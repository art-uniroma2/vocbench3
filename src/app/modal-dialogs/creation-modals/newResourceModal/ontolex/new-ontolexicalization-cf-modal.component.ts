import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTURIResource, RDFResourceRolesEnum } from "../../../../models/ARTResources";
import { CustomFormValue } from "../../../../models/CustomForms";
import { OntoLex } from "../../../../models/Vocabulary";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { BasicModalServices } from "../../../basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../../browsing-modals/browsing-modals.service";
import { AbstractCustomConstructorModal } from "../abstract-custom-constructor-modal";

@Component({
  selector: "new-ontolexicalization-cf-modal",
  templateUrl: "./new-ontolexicalization-cf-modal.component.html",
  standalone: false
})
export class NewOntoLexicalizationCfModalComponent extends AbstractCustomConstructorModal {
  @Input() title: string;
  @Input() lexicalizationProp: ARTURIResource; //(OntoLex.senses | OntoLex.denotes | Ontolex.isDenotedBy)
  @Input() clsChangeable: boolean = true;

  //standard form
  linkedResource: string;

  createPlainCheck: boolean = true;
  createSenseCheck: boolean = true;

  pickerRoles: RDFResourceRolesEnum[] = [RDFResourceRolesEnum.cls, RDFResourceRolesEnum.individual, RDFResourceRolesEnum.property,
  RDFResourceRolesEnum.concept, RDFResourceRolesEnum.conceptScheme, RDFResourceRolesEnum.skosCollection];

  /**
   * true if the modal should allow to link a lexical entry to a reference (show the reference input field),
   * false if it should allow to link a resource to a lexical entry (show the lexical entry input field).
   */
  linkToReference: boolean;
  /**
   * true if the modal should allow to create a LexicalSense (show the "create plain" checkbox),
   * false if it should allow to create a plain lexicalization (show the "create sense" checkbox).
   */
  createSense: boolean;

  constructor(public activeModal: NgbActiveModal, cfService: CustomFormsServices,
    basicModals: BasicModalServices, browsingModals: BrowsingModalServices) {
    super(cfService, basicModals, browsingModals);
  }

  ngOnInit() {
    this.resourceClass = OntoLex.lexicalSense;

    if (this.lexicalizationProp.equals(OntoLex.isDenotedBy)) {
      this.linkToReference = false;
      this.createSense = false;
    } else if (this.lexicalizationProp.equals(OntoLex.sense)) {
      this.linkToReference = true;
      this.createSense = true;
    } else if (this.lexicalizationProp.equals(OntoLex.denotes)) {
      this.linkToReference = true;
      this.createSense = false;
    }

    this.selectCustomForm();
  }

  changeClass() {
    this.changeClassWithRoot(OntoLex.lexicalSense);
  }

  pickLexicalEntry() {
    this.browsingModals.browseLexicalEntryList({ key: "DATA.ACTIONS.SELECT_LEXICAL_ENTRY" }).then(
      (lexEntry: ARTURIResource) => {
        this.linkedResource = lexEntry.getURI();
      }
    );
  }

  updateLinkedRes(res: ARTURIResource) {
    if (res != null) {
      this.linkedResource = res.getURI();
    } else {
      this.linkedResource = null;
    }
  }

  isStandardFormDataValid(): boolean {
    return (this.linkedResource != null && this.linkedResource.trim() != "");
  }

  okImpl() {
    let entryMap: any = this.collectCustomFormData();

    let returnedData: NewOntoLexicalizationCfModalReturnData = {
      linkedResource: new ARTURIResource(this.linkedResource),
      createPlain: this.createPlainCheck,
      createSense: this.createSenseCheck,
      cls: this.resourceClass,
      cfValue: null
    };
    //set class only if not the default
    if (!this.resourceClass.equals(OntoLex.lexicalSense)) {
      returnedData.cls = this.resourceClass;
    }
    //set cfValue only if not null and only if it's creating a sense (that is a reified lexicalizaion, CF doesn't make sense for a plain lexicalization)
    if (this.createSense && this.customFormId != null && entryMap != null) {
      returnedData.cfValue = new CustomFormValue(this.customFormId, entryMap);
    }
    this.activeModal.close(returnedData);
  }

  cancel() {
    this.activeModal.dismiss();
  }

}

export class NewOntoLexicalizationCfModalReturnData {
  linkedResource: ARTURIResource; //lexicalEntry or reference
  createPlain: boolean;
  createSense: boolean;
  cls: ARTURIResource;
  cfValue: CustomFormValue;
}