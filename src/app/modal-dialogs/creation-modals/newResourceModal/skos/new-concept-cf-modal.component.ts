import { ChangeDetectorRef, Component, Input, ViewChild } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LangPickerComponent } from 'src/app/widget/pickers/lang-picker/lang-picker.component';
import { ARTLiteral, ARTURIResource } from "../../../../models/ARTResources";
import { CustomFormValue } from "../../../../models/CustomForms";
import { SKOS } from "../../../../models/Vocabulary";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { VBContext } from "../../../../utils/VBContext";
import { BasicModalServices } from "../../../basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../../browsing-modals/browsing-modals.service";
import { AbstractCustomConstructorModal } from "../abstract-custom-constructor-modal";

@Component({
  selector: "new-concept-cf-modal",
  templateUrl: "./new-concept-cf-modal.component.html",
  standalone: false
})
export class NewConceptCfModalComponent extends AbstractCustomConstructorModal {

  @ViewChild(LangPickerComponent, { static: false }) langPicker: LangPickerComponent;

  @Input() title: string = "Modal title";
  @Input() broader: ARTURIResource;
  @Input() schemes: ARTURIResource[]; //in standard form
  @Input() cls: ARTURIResource;
  @Input() clsChangeable: boolean = true;
  @Input() lang: string; //in standard form

  broaderProp: ARTURIResource = SKOS.broader;

  //standard form
  label: string;
  uri: string;

  constructor(public activeModal: NgbActiveModal, private resourceService: ResourcesServices, private changeDetectorRef: ChangeDetectorRef,
    cfService: CustomFormsServices, basicModals: BasicModalServices, browsingModals: BrowsingModalServices) {
    super(cfService, basicModals, browsingModals);
  }

  ngOnInit() {
    this.resourceClass = this.cls ? this.cls : SKOS.concept;
    this.selectCustomForm();

    if (this.broader) {
      let broaderPropUri = VBContext.getWorkingProjectCtx().getProjectPreferences().conceptTreePreferences.baseBroaderProp;
      if (broaderPropUri != SKOS.broader.getURI()) {
        this.resourceService.getResourceDescription(new ARTURIResource(broaderPropUri)).subscribe(
          res => {
            this.broaderProp = res;
          }
        );
      }
    }
  }

  ngAfterViewInit() {
    this.changeDetectorRef.detectChanges();
  }

  onSchemesChanged(schemes: ARTURIResource[]) {
    this.schemes = schemes;
  }

  changeClass() {
    this.changeClassWithRoot(SKOS.concept);
  }

  changeBroaderProp() {
    this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }, [SKOS.broader]).then(
      (selectedProp: ARTURIResource) => {
        this.broaderProp = selectedProp;
      },
      () => { }
    );
  }

  isStandardFormDataValid(): boolean {
    return (this.label != undefined && this.label.trim() != "" && this.lang != null &&
      this.schemes != null && this.schemes.length > 0);
  }

  okImpl() {
    let entryMap: any = this.collectCustomFormData();

    let returnedData: NewConceptCfModalReturnData = {
      uriResource: null,
      label: new ARTLiteral(this.label, null, this.lang),
      cls: this.resourceClass,
      broaderProp: null,
      schemes: this.schemes,
      cfValue: null
    };
    //Set URI only if localName is not empty
    if (this.uri != null && this.uri.trim() != "") {
      returnedData.uriResource = new ARTURIResource(this.uri);
    }
    //set broaderProp only if not the default
    if (!this.broaderProp.equals(SKOS.broader)) {
      returnedData.broaderProp = this.broaderProp;
    }
    //set cfValue only if not null
    if (this.customFormId != null && entryMap != null) {
      returnedData.cfValue = new CustomFormValue(this.customFormId, entryMap);
    }

    this.activeModal.close(returnedData);
  }

  cancel() {
    this.activeModal.dismiss();
  }

}

export class NewConceptCfModalReturnData {
  uriResource: ARTURIResource;
  label: ARTLiteral;
  cls: ARTURIResource;
  broaderProp: ARTURIResource;
  schemes: ARTURIResource[];
  cfValue: CustomFormValue;
}