import { Component, EventEmitter, Input, Output, SimpleChanges } from "@angular/core";
import { ARTURIResource } from "../../../../models/ARTResources";
import { SkosServices } from "../../../../services/skos.service";
import { ResourceUtils } from "../../../../utils/ResourceUtils";
import { VBContext } from "../../../../utils/VBContext";
import { BrowsingModalServices } from "../../../browsing-modals/browsing-modals.service";

@Component({
  selector: "scheme-selection",
  templateUrl: "./scheme-selection.component.html",
  standalone: false
})
export class SchemeSelectionComponent {
  @Input() concept: ARTURIResource; //useful to limit the schemeList to the only schemes of a concept
  @Input() schemes: ARTURIResource[]; //to force the initialization of schemes
  @Output() update = new EventEmitter<ARTURIResource[]>();

  addBtnImgSrc = "./assets/images/icons/actions/conceptScheme_create.png";

  collapsed: boolean = false;

  schemeList: ARTURIResource[] = [];
  selectedScheme: ARTURIResource;

  constructor(private skosService: SkosServices, private browsingModals: BrowsingModalServices) { }

  initSchemeList() {
    this.schemeList = [];
    if (this.concept == null) {
      /**
       * Init schemeList with all the active schemes (retrieve them from the getAllSchemes result since the 
       * active_schemes preference contains just the URIs, not all the info (show, role...))
       */
      this.skosService.getAllSchemes().subscribe(
        schemes => {
          if (this.schemes == null) {
            this.schemes = VBContext.getWorkingProjectCtx().getProjectPreferences().activeSchemes;
          }
          for (const scheme of this.schemes) {
            for (const s of schemes) {
              if (scheme.equals(s)) {
                this.schemeList.push(s);
                break;
              }
            }
          }
          this.update.emit(this.schemeList);
        }
      );
    } else {
      //init schemeList with the schemes of the given concept (broader of the new creating)
      this.skosService.getSchemesOfConcept(this.concept).subscribe(
        schemes => {
          this.schemeList = schemes;
          this.update.emit(this.schemeList);
        }
      );
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['concept']) {
      this.initSchemeList();
    }
  }

  addScheme() {
    this.browsingModals.browseSchemeList({ key: "DATA.ACTIONS.ADD_SCHEME" }).then(
      (scheme: any) => {
        //add the chosen scheme only if not already in list
        if (!ResourceUtils.containsNode(this.schemeList, scheme)) {
          this.schemeList.push(scheme);
          this.update.emit(this.schemeList);
        }
      },
      () => { }
    );
  }

  removeScheme(scheme: ARTURIResource) {
    this.schemeList.splice(this.schemeList.indexOf(scheme), 1);
    this.selectedScheme = null;
    this.update.emit(this.schemeList);
  }

}