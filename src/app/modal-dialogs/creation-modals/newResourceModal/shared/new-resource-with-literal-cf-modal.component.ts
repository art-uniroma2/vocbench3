import { ChangeDetectorRef, Component, Input, ViewChild } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LangPickerComponent } from 'src/app/widget/pickers/lang-picker/lang-picker.component';
import { ARTLiteral, ARTURIResource } from "../../../../models/ARTResources";
import { CustomFormValue } from "../../../../models/CustomForms";
import { LanguageConstraint } from "../../../../models/LanguagesCountries";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { BasicModalServices } from "../../../basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../../browsing-modals/browsing-modals.service";
import { AbstractCustomConstructorModal } from "../abstract-custom-constructor-modal";

@Component({
    selector: "new-resource-lit-cf-modal",
    templateUrl: "./new-resource-with-literal-cf-modal.component.html",
    standalone: false
})
export class NewResourceWithLiteralCfModalComponent extends AbstractCustomConstructorModal {
    @ViewChild(LangPickerComponent, { static: false }) langPicker: LangPickerComponent;

    @Input() title: string;
    @Input() cls: ARTURIResource; //class that this modal is creating an instance
    @Input() clsChangeable: boolean = true;
    @Input() literalLabel: string = "Label";
    @Input() langConstraints: LanguageConstraint = { constrain: false, locale: true };

    //standard form
    label: string;
    @Input() lang: string;
    uri: string;

    constructor(public activeModal: NgbActiveModal, cfService: CustomFormsServices,
        basicModals: BasicModalServices, browsingModals: BrowsingModalServices, private changeDetectorRef: ChangeDetectorRef) {
        super(cfService, basicModals, browsingModals);
    }

    ngOnInit() {
        this.resourceClass = this.cls;
        this.selectCustomForm();
    }

    ngAfterViewInit() {
        this.changeDetectorRef.detectChanges();
    }

    changeClass() {
        this.changeClassWithRoot(this.cls);
    }

    isStandardFormDataValid(): boolean {
        return (this.label != undefined && this.label.trim() != "" && this.lang != null);
    }

    okImpl() {
        let entryMap: any = this.collectCustomFormData();

        let returnedData: NewResourceWithLiteralCfModalReturnData = {
            uriResource: null,
            literal: new ARTLiteral(this.label, null, this.lang),
            cls: this.resourceClass,
            cfValue: null
        };
        //Set URI only if localName is not empty
        if (this.uri != null && this.uri.trim() != "") {
            returnedData.uriResource = new ARTURIResource(this.uri);
        }
        //set cfValue only if not null
        if (this.customFormId != null && entryMap != null) {
            returnedData.cfValue = new CustomFormValue(this.customFormId, entryMap);
        }
        this.activeModal.close(returnedData);
    }

    cancel() {
        this.activeModal.dismiss();
    }

}

export class NewResourceWithLiteralCfModalReturnData {
    uriResource: ARTURIResource;
    literal: ARTLiteral;
    cls: ARTURIResource;
    cfValue: CustomFormValue;
}