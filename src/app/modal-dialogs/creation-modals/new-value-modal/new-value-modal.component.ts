import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTNode } from "../../../models/ARTResources";

@Component({
  selector: "new-value-modal",
  templateUrl: "./new-value-modal.component.html",
  standalone: false
})
export class NewValueModalComponent {

  @Input() title: string = 'Create new value';
  @Input() editable: boolean = false;

  value: ARTNode;

  constructor(public activeModal: NgbActiveModal) { }

  updateValue(value: ARTNode) {
    this.value = value;
  }

  ok() {
    this.activeModal.close(this.value);
  }

  cancel() {
    this.activeModal.dismiss();
  }

}