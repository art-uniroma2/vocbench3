import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: "datetime-picker-modal",
  templateUrl: "./datetime-picker-modal.component.html",
  standalone: false
})
export class DatetimePickerModalComponent {
  @Input() title: string;
  @Input() date: Date;

  constructor(public activeModal: NgbActiveModal) { }

  ok() {
    this.activeModal.close(this.date);
  }

  cancel() {
    this.activeModal.dismiss();
  }

}