import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ResourcePickerConfig } from "src/app/widget/pickers/value-picker/resource-picker.component";
import { ARTResource, ARTURIResource } from "../../../models/ARTResources";

@Component({
    selector: "resource-picker-modal",
    templateUrl: "./resource-picker-modal.component.html",
    standalone: false
})
export class ResourcePickerModalComponent {
    @Input() title: string;
    @Input() config: ResourcePickerConfig;
    @Input() editable: boolean;

    resource: ARTResource;

    constructor(public activeModal: NgbActiveModal) {}

    updateResource(res: ARTURIResource) {
        this.resource = res;
    }

    ok() {
        this.activeModal.close(this.resource);
    }

    cancel() {
        this.activeModal.dismiss();
    }

}