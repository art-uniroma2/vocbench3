import { Component, ElementRef, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTURIResource } from '../../../models/ARTResources';
import { AbstractStructureModal } from "../AbstractStructureModal";

@Component({
  selector: "instance-list-modal",
  templateUrl: "./instance-list-modal.component.html",
  standalone: false
})
export class InstanceListModalComponent extends AbstractStructureModal {
  @Input() cls: ARTURIResource;

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef) {
    super(activeModal, elementRef);
  }

}