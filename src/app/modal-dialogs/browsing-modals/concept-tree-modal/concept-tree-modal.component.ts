import { Component, ElementRef, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTURIResource } from '../../../models/ARTResources';
import { AbstractStructureModal } from "../AbstractStructureModal";

@Component({
  selector: "concept-tree-modal",
  templateUrl: "./concept-tree-modal.component.html",
  standalone: false
})
export class ConceptTreeModalComponent extends AbstractStructureModal {
  @Input() schemes: ARTURIResource[];
  @Input() schemeChangeable: boolean = false;

  selectedConcept: ARTURIResource;

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef) {
    super(activeModal, elementRef);
  }


}