import { Component, ElementRef } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractStructureModal } from "../AbstractStructureModal";

@Component({
  selector: "lexicon-list-modal",
  templateUrl: "./lexicon-list-modal.component.html",
  standalone: false
})
export class LexiconListModalComponent extends AbstractStructureModal {

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef) {
    super(activeModal, elementRef);
  }


}