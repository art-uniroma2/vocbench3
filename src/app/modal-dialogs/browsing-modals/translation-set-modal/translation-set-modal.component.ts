import { Component, ElementRef, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractStructureModal } from "../AbstractStructureModal";

@Component({
  selector: "translation-set-modal",
  templateUrl: "./translation-set-modal.component.html",
  standalone: false
})
export class TranslationSetModalComponent extends AbstractStructureModal {

  @Input() editable: boolean = false;
  @Input() deletable: boolean = false;
  @Input() allowMultiselection: boolean = false;

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef) {
    super(activeModal, elementRef);
  }


}