import { Component, ElementRef, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTURIResource } from '../../../models/ARTResources';
import { AbstractStructureModal } from "../AbstractStructureModal";

@Component({
  selector: "class-tree-modal",
  templateUrl: "./class-tree-modal.component.html",
  standalone: false
})
export class ClassTreeModalComponent extends AbstractStructureModal {
  @Input() roots?: ARTURIResource[];

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef) {
    super(activeModal, elementRef);
  }

}