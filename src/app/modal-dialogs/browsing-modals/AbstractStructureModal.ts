import { Directive, ElementRef, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTResource } from 'src/app/models/ARTResources';
import { NodeSelectEvent } from 'src/app/structures/abstractNode';
import { UIUtils } from 'src/app/utils/UIUtils';
import { ProjectContext } from 'src/app/utils/VBContext';

@Directive()
export class AbstractStructureModal {

  @Input() title: string;
  @Input() projectCtx: ProjectContext;

  selectedNode: ARTResource;

  constructor(protected activeModal: NgbActiveModal, protected elementRef: ElementRef) { }

  ngAfterViewInit() {
    UIUtils.setFullSizeModal(this.elementRef);
  }

  onNodeSelected(event?: NodeSelectEvent) {
    this.selectedNode = event?.value as ARTResource;
  }

  isOkEnabled() {
    return this.selectedNode != null;
  }

  ok() {
    this.activeModal.close(this.selectedNode);
  }

  cancel() {
    this.activeModal.dismiss();
  }

}
