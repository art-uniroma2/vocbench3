import { Component, ElementRef } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractStructureModal } from "../AbstractStructureModal";

@Component({
  selector: "datatype-list-modal",
  templateUrl: "./datatype-list-modal.component.html",
  standalone: false
})
export class DatatypeListModalComponent extends AbstractStructureModal {

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef) {
    super(activeModal, elementRef);
  }


}