import { Component, ElementRef } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractStructureModal } from "../AbstractStructureModal";

@Component({
  selector: "collection-tree-modal",
  templateUrl: "./collection-tree-modal.component.html",
  standalone: false
})
export class CollectionTreeModalComponent extends AbstractStructureModal {

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef) {
    super(activeModal, elementRef);
  }

}