import { Component, ElementRef, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTURIResource, RDFResourceRolesEnum } from '../../../models/ARTResources';
import { AbstractStructureModal } from "../AbstractStructureModal";

@Component({
  selector: "property-tree-modal",
  templateUrl: "./property-tree-modal.component.html",
  standalone: false
})
export class PropertyTreeModalComponent extends AbstractStructureModal {

  @Input() rootProperties: ARTURIResource[];
  @Input() resource: ARTURIResource;
  @Input() type: RDFResourceRolesEnum;

  domainRes: ARTURIResource;

  showAll: boolean = false;

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef) {
    super(activeModal, elementRef);
  }

  ngOnInit() {
    this.domainRes = this.resource;
  }

  /**
   * When the checkbox "select all properties" changes status
   * Resets the selectedProperty and update the domainRes that represents 
   * the resource which its type should be the domain of the properties in the tree
   */
  onShowAllChanged() {
    this.selectedNode = null;
    if (this.showAll) {
      this.domainRes = null;
    } else {
      this.domainRes = this.resource;
    }
  }


}