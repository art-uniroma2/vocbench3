import { Component, ElementRef } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractStructureModal } from "../AbstractStructureModal";

@Component({
  selector: "lexical-sense-list-modal",
  templateUrl: "./lexical-sense-list-modal.component.html",
  standalone: false
})
export class LexicalSenseListModalComponent extends AbstractStructureModal {

  multiselection: boolean = false;

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef) {
    super(activeModal, elementRef);
  }

}