import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { TextOrTranslation, TranslationUtils } from 'src/app/utils/TranslationUtils';
import { ARTURIResource, RDFResourceRolesEnum } from "../../models/ARTResources";
import { ProjectContext } from '../../utils/VBContext';
import { ModalOptions } from '../Modals';
import { ClassIndividualTreeModalComponent } from "./class-individual-tree-modal/class-individual-tree-modal.component";
import { ClassTreeModalComponent } from "./class-tree-modal/class-tree-modal.component";
import { CollectionTreeModalComponent } from "./collection-tree-modal/collection-tree-modal.component";
import { ConceptTreeModalComponent } from "./concept-tree-modal/concept-tree-modal.component";
import { DatatypeListModalComponent } from './datatype-list-modal/datatype-list-modal.component';
import { InstanceListModalComponent } from "./instance-list-modal/instance-list-modal.component";
import { LexicalEntryListModalComponent } from './lexical-entry-list-modal/lexical-entry-list-modal.component';
import { LexicalSenseListModalComponent } from './lexical-sense-list-modal/lexical-sense-list-modal.component';
import { LexiconListModalComponent } from './lexicon-list-modal/lexicon-list-modal.component';
import { PropertyTreeModalComponent } from "./property-tree-modal/property-tree-modal.component";
import { SchemeListModalComponent } from "./scheme-list-modal/scheme-list-modal.component";
import { TranslationSetModalComponent } from './translation-set-modal/translation-set-modal.component';

/**
 * Service to open browsing modals, namely the modal that contains trees (concept, class, property) or list (instances).
 */
@Injectable()
export class BrowsingModalServices {

  constructor(private modalService: NgbModal, private translateService: TranslateService) { }

  /**
   * Opens a modal to browse the class tree
   * @param title the title of the modal
   * @return if the modal closes with ok returns a promise containing the selected class
   */
  browseClassTree(title: TextOrTranslation, roots?: ARTURIResource[], projectCtx?: ProjectContext): Promise<ARTURIResource> {
    let _options: ModalOptions = new ModalOptions();
    const modalRef: NgbModalRef = this.modalService.open(ClassTreeModalComponent, _options);
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    if (roots != null) modalRef.componentInstance.roots = roots;
    if (projectCtx != null) modalRef.componentInstance.projectCtx = projectCtx;
    return modalRef.result;
  }

  /**
   * Opens a modal to browse the class tree and select an individual of a class
   * @param title the title of the modal
   * @param classes (optional) tells the admitted type of the individual to pick
   * @return if the modal closes with ok returns a promise containing the selected individual
   */
  browseClassIndividualTree(title: TextOrTranslation, classes?: ARTURIResource[], projectCtx?: ProjectContext): Promise<ARTURIResource> {
    let _options: ModalOptions = new ModalOptions('lg');
    const modalRef: NgbModalRef = this.modalService.open(ClassIndividualTreeModalComponent, _options);
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    if (classes != null) modalRef.componentInstance.classes = classes;
    if (projectCtx != null) modalRef.componentInstance.projectCtx = projectCtx;
    return modalRef.result;
  }

  /**
   * Opens a modal to browse the concept tree
   * @param title the title of the modal
   * @param cls the class of the instance to browse
   * @return if the modal closes with ok returns a promise containing the selected instance
   */
  browseInstanceList(title: TextOrTranslation, cls: ARTURIResource): Promise<ARTURIResource> {
    let _options: ModalOptions = new ModalOptions();
    const modalRef: NgbModalRef = this.modalService.open(InstanceListModalComponent, _options);
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    modalRef.componentInstance.cls = cls;
    return modalRef.result;
  }

  /**
   * Opens a modal to browse the concept tree
   * @param title the title of the modal
   * @param scheme the scheme of the concept tree. If not provided the modal contains a tree in no-scheme mode
   * @param schemeChangeable if true a menu is shown and the user can browse not only the selected scheme
   * @return if the modal closes with ok returns a promise containing the selected concept
   */
  browseConceptTree(title: TextOrTranslation, schemes?: ARTURIResource[], schemeChangeable?: boolean, projectCtx?: ProjectContext): Promise<ARTURIResource> {
    let _options: ModalOptions = new ModalOptions();
    const modalRef: NgbModalRef = this.modalService.open(ConceptTreeModalComponent, _options);
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    if (schemes != null) modalRef.componentInstance.schemes = schemes;
    if (schemeChangeable != null) modalRef.componentInstance.schemeChangeable = schemeChangeable;
    if (projectCtx != null) modalRef.componentInstance.projectCtx = projectCtx;
    return modalRef.result;
  }

  /**
   * Opens a modal to browse the collection tree
   * @param title the title of the modal
   * @return if the modal closes with ok returns a promise containing the selected collection
   */
  browseCollectionTree(title: TextOrTranslation, projectCtx?: ProjectContext): Promise<ARTURIResource> {
    let _options: ModalOptions = new ModalOptions();
    const modalRef: NgbModalRef = this.modalService.open(CollectionTreeModalComponent, _options);
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    if (projectCtx != null) modalRef.componentInstance.projectCtx = projectCtx;
    return modalRef.result;
  }

  /**
   * Opens a modal to browse the scheme list
   * @param title the title of the modal
   * @return if the modal closes with ok returns a promise containing the selected scheme
   */
  browseSchemeList(title: TextOrTranslation, projectCtx?: ProjectContext): Promise<ARTURIResource> {
    let _options: ModalOptions = new ModalOptions();
    const modalRef: NgbModalRef = this.modalService.open(SchemeListModalComponent, _options);
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    if (projectCtx != null) modalRef.componentInstance.projectCtx = projectCtx;
    return modalRef.result;
  }

  /**
   * Opens a modal to browse the property tree
   * @param title the title of the modal
   * @param rootProperties optional , if provided the tree is build with these properties as roots
   * @param resource optional, if provided the returned propertyTree contains 
   * @param type optional, tells the type of properties to show in the tree
   * just the properties that have as domain the type of the resource 
   * @return if the modal closes with ok returns a promise containing the selected property
   */
  browsePropertyTree(title: TextOrTranslation, rootProperties?: ARTURIResource[], resource?: ARTURIResource, type?: RDFResourceRolesEnum, projectCtx?: ProjectContext): Promise<ARTURIResource> {
    let _options: ModalOptions = new ModalOptions();
    const modalRef: NgbModalRef = this.modalService.open(PropertyTreeModalComponent, _options);
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    if (rootProperties != null) modalRef.componentInstance.rootProperties = rootProperties;
    if (resource != null) modalRef.componentInstance.resource = resource;
    if (type != null) modalRef.componentInstance.type = type;
    if (projectCtx != null) modalRef.componentInstance.projectCtx = projectCtx;
    return modalRef.result;
  }

  /**
   * 
   * @param title 
   * @param lexicon if not provided, get the current active
   * @param lexiconChangeable 
   * @param allowMultiselection
   */
  browseLexicalEntryList(title: TextOrTranslation, lexicon?: ARTURIResource, lexiconChangeable?: boolean, editable?: boolean, deletable?: boolean,
    allowMultiselection?: boolean, projectCtx?: ProjectContext) {
    let _options: ModalOptions = new ModalOptions();
    const modalRef: NgbModalRef = this.modalService.open(LexicalEntryListModalComponent, _options);
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    if (lexicon != null) modalRef.componentInstance.lexicon = lexicon;
    if (lexiconChangeable != null) modalRef.componentInstance.lexiconChangeable = lexiconChangeable;
    if (editable != null) modalRef.componentInstance.editable = editable;
    if (deletable != null) modalRef.componentInstance.deletable = deletable;
    if (allowMultiselection != null) modalRef.componentInstance.allowMultiselection = allowMultiselection;
    if (projectCtx != null) modalRef.componentInstance.projectCtx = projectCtx;
    return modalRef.result;
  }

  /**
   * 
   * @param title 
   */
  browseLexiconList(title: TextOrTranslation, projectCtx?: ProjectContext): Promise<ARTURIResource> {
    let _options: ModalOptions = new ModalOptions();
    const modalRef: NgbModalRef = this.modalService.open(LexiconListModalComponent, _options);
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    if (projectCtx != null) modalRef.componentInstance.projectCtx = projectCtx;
    return modalRef.result;
  }

  browseLexicalSense(title: TextOrTranslation, projectCtx?: ProjectContext): Promise<ARTURIResource> {
    let _options: ModalOptions = new ModalOptions();
    const modalRef: NgbModalRef = this.modalService.open(LexicalSenseListModalComponent, _options);
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    if (projectCtx != null) modalRef.componentInstance.projectCtx = projectCtx;
    return modalRef.result;
  }

  browseTranslationSet(title: TextOrTranslation, editable?: boolean, deletable?: boolean, allowMultiselection?: boolean, projectCtx?: ProjectContext): Promise<ARTURIResource> {
    let _options: ModalOptions = new ModalOptions();
    const modalRef: NgbModalRef = this.modalService.open(TranslationSetModalComponent, _options);
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    if (editable != null) modalRef.componentInstance.editable = editable;
    if (deletable != null) modalRef.componentInstance.deletable = deletable;
    if (allowMultiselection != null) modalRef.componentInstance.allowMultiselection = allowMultiselection;
    if (projectCtx != null) modalRef.componentInstance.projectCtx = projectCtx;
    return modalRef.result;
  }

  /**
   * Opens a modal to browse the datatype list
   * @param title the title of the modal
   * @return if the modal closes with ok returns a promise containing the selected datatype
   */
  browseDatatypeList(title: TextOrTranslation): Promise<ARTURIResource> {
    let _options: ModalOptions = new ModalOptions();
    const modalRef: NgbModalRef = this.modalService.open(DatatypeListModalComponent, _options);
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText(title, this.translateService);
    return modalRef.result;
  }

}