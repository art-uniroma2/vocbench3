import { Component, ElementRef, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTURIResource } from '../../../models/ARTResources';
import { AbstractStructureModal } from "../AbstractStructureModal";

@Component({
  selector: "class-individual-tree-modal",
  templateUrl: "./class-individual-tree-modal.component.html",
  standalone: false
})
export class ClassIndividualTreeModalComponent extends AbstractStructureModal {
  @Input() classes: ARTURIResource[];

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef) {
    super(activeModal, elementRef);
  }

}