import { Component, ElementRef, Input } from "@angular/core";
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from "@ngx-translate/core";
import { ARTURIResource } from '../../../models/ARTResources';
import { ResourcesServices } from "../../../services/resources.service";
import { STRequestOptions } from "../../../utils/HttpManager";
import { VBContext } from "../../../utils/VBContext";
import { AbstractStructureModal } from "../AbstractStructureModal";
import { LexiconListModalComponent } from "../lexicon-list-modal/lexicon-list-modal.component";

@Component({
  selector: "lexical-entry-list-modal",
  templateUrl: "./lexical-entry-list-modal.component.html",
  standalone: false
})
export class LexicalEntryListModalComponent extends AbstractStructureModal {

  @Input() lexicon: ARTURIResource;
  @Input() lexiconChangeable: boolean = false;
  @Input() editable: boolean = false;
  @Input() deletable: boolean = false;
  @Input() allowMultiselection: boolean = false;

  activeLexicon: ARTURIResource;
  checkedResources: ARTURIResource[] = [];

  multiselection: boolean = false;

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef, private modalService: NgbModal, private resourceService: ResourcesServices,
    private translateService: TranslateService) {
    super(activeModal, elementRef);
  }

  ngOnInit() {
    this.activeLexicon = this.lexicon;
    if (this.activeLexicon == null) { //if no lexicon has been "forced", set the current active lexicon
      let activeLexiconProp = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().activeLexicon;
      if (activeLexiconProp != null) {
        this.resourceService.getResourceDescription(activeLexiconProp, STRequestOptions.getRequestOptions(this.projectCtx)).subscribe(
          lex => {
            this.activeLexicon = lex;
          }
        );
      }
    }
  }

  changeLexicon() {
    //cannot use BrowsingModalServices since it would cause a circular dependency (this modal is opened by BrowsingModalServices and has the same injected)
    const modalRef: NgbModalRef = this.modalService.open(LexiconListModalComponent);
    modalRef.componentInstance.title = this.translateService.instant("DATA.ACTIONS.SELECT_LEXICON");
    modalRef.result.then(
      (lexicon: ARTURIResource) => {
        this.activeLexicon = lexicon;
      },
      () => { }
    );
  }

  isOkEnabled() {
    if (this.multiselection) {
      return this.checkedResources.length > 0;
    } else {
      return this.selectedNode != null;
    }
  }

  ok() {
    let returnValue: any = this.multiselection ? this.checkedResources : this.selectedNode; //ARTURIResource or ARTURIResource (multiselection on)
    this.activeModal.close(returnValue);
  }

  cancel() {
    this.activeModal.dismiss();
  }

}