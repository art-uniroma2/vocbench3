import { Component, ElementRef } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractStructureModal } from "../AbstractStructureModal";

@Component({
  selector: "scheme-list-modal",
  templateUrl: "./scheme-list-modal.component.html",
  standalone: false
})
export class SchemeListModalComponent extends AbstractStructureModal {

  constructor(activeModal: NgbActiveModal, elementRef: ElementRef) {
    super(activeModal, elementRef);
  }

}