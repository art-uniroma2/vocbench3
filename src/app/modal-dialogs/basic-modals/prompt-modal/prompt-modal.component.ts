import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: "prompt-modal",
  templateUrl: "./prompt-modal.component.html",
  standalone: false
})
export class PromptModalComponent {

  @Input() title: string;
  @Input() label: { value: string, tooltip?: string };
  @Input() message: string;
  @Input() value: string;
  @Input() hideNo: boolean = false;
  @Input() inputOptional: boolean = false;
  @Input() inputSanitized: boolean = false;

  constructor(public activeModal: NgbActiveModal) { }

  onEnter(event: Event) {
    if (event instanceof KeyboardEvent) {
      if (!event.shiftKey && !event.altKey && !event.ctrlKey) { //when enter is not in combo with shift (which insert a newline)
        if (this.isInputValid()) { //only when the input value is the only one
          event.preventDefault();
          this.ok();
        }
      }
    }
  }

  isInputValid(): boolean {
    return (this.value != undefined && this.value.trim() != "");
  }

  ok() {
    if (this.inputOptional || this.isInputValid()) {
      this.activeModal.close(this.value);
    }
  }

  close() {
    this.activeModal.dismiss();
  }

}