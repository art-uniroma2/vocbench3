import { ARTURIResource } from "./ARTResources";
import { DataPanel, DataPanelLabelSetting } from "./DataStructure";
import { Language } from "./LanguagesCountries";
import { AccessLevel, Project, ProjectVisibility, ProjectVisualization } from "./Project";
import { ResViewSection } from "./ResourceView";
import { Sheet2RdfSettings } from './Sheet2RDF';
import { OWL, RDFS, SKOS } from "./Vocabulary";

/**
 * Names of the property of the Settings (at every levels: system, project, user, project-user)
 */
export enum SettingsEnum {
  activeLexicon = "activeLexicon",
  activeSchemes = "activeSchemes",
  authService = "authService",
  classTree = "classTree",
  conceptTree = "conceptTree",
  customTree = "customTree",
  dataPanelLabelMappings = "dataPanelLabelMappings",
  defaultConceptType = "defaultConceptType",
  defaultLexEntryType = "defaultLexEntryType",
  editingLanguage = "editingLanguage",
  emailVerification = "emailVerification",
  experimentalFeaturesEnabled = "experimentalFeaturesEnabled",
  filterValueLanguages = "filterValueLanguages",
  graphViewPartitionFilter = "graphViewPartitionFilter",
  hiddenDataPanelsOntolex = "hiddenDataPanelsOntolex",
  hiddenDataPanelsRdfsOwl = "hiddenDataPanelsRdfsOwl",
  hiddenDataPanelsSkos = "hiddenDataPanelsSkos",
  hideLiteralGraphNodes = "hideLiteralGraphNodes",
  homeContent = "homeContent",
  instanceList = "instanceList",
  labelClashMode = "labelClashMode",
  languages = "languages",
  lexEntryList = "lexEntryList",
  mail = "mail",
  maxRowsTablePreview = "maxRowsTablePreview",
  notificationsStatus = "notificationsStatus",
  preload = "preload",
  privacyStatementAvailable = "privacyStatementAvailable",
  projectCreation = "projectCreation",
  projectVisualization = "projectVisualization",
  projectRendering = "projectRendering",
  projectTheme = "projectTheme",
  remoteConfigs = "remoteConfigs",
  renderingClassTree = "renderingClassTree",
  renderingCollectionTree = "renderingCollectionTree",
  renderingConceptTree = "renderingConceptTree",
  renderingDatatypeList = "renderingDatatypeList",
  renderingInstanceList = "renderingInstanceList",
  renderingLexEntryList = "renderingLexEntryList",
  renderingLexiconList = "renderingLexiconList",
  renderingPropertyTree = "renderingPropertyTree",
  renderingSchemeList = "renderingSchemeList",
  resourceView = "resourceView",
  resViewPartitionFilter = "resViewPartitionFilter",
  resViewPredLabelMappings = "resViewPredLabelMappings",
  resViewSectionsCustomization = "resViewSectionsCustomization",
  searchSettings = "searchSettings",
  sheet2rdfSettings = "sheet2rdfSettings",
  showFlags = "showFlags",
  timeMachineEnabled = "timeMachineEnabled",
}

export class PreferencesUtils {
  /**
   * Merge the default preferences object with the one returned by the Settings.getSettings() service
   * Useful to keep the default values to those properties not set and thus not returned by the above service.
   * @param localPref 
   * @param settingsProperty 
   */
  public static mergePreference(localPref: any, settingsProperty: any) {
    Object.keys(settingsProperty).forEach(prop => {
      if (settingsProperty[prop] != null) {
        localPref[prop] = settingsProperty[prop];
      }
    });
  }
}

export class ResourceViewPreference {
  syncTabs: boolean = false; //in tabbed mode allows to keep sync'd the resource in the active tab with the same resource in the tree/list
  defaultConceptType: ResourceViewType = ResourceViewType.resourceView; //tells the RV type to be open by default for concepts
  lastConceptType: ResourceViewType;
  defaultLexEntryType: ResourceViewType = ResourceViewType.resourceView; //tells the RV type to be open by default for lexEntry
  lastLexEntryType: ResourceViewType;
  displayImg: boolean = false;
  rendering: boolean = true;
  inference: boolean = false;
  showDeprecated: boolean = true;
  showDatatypeBadge: boolean = false;
  forceSearchModeForAddPropertyValue: boolean = false;
  sortByRendering: boolean = false; //use the same rendering language order for sorting value in lexicalizations section
}

export enum ResourceViewType { //used for set a default type of resource view for concepts
  resourceView = "resourceView",
  termView = "termView",
  lexicographerView = "lexicographerView",
  sourceCode = "sourceCode"
}

export class ResourceViewProjectSettings {
  customSections: { [key: string]: CustomSection } = {}; //map name -> CustomSection
  templates: ResViewTemplate = {}; //map role -> sections
}

export interface RoleSectionsMap {
  [key: string]: ResViewSection[]
}

export class CustomSection {
  matchedProperties: string[];
}

export class PredicateLabelSettings {
  enabled: boolean;
  mappings: { [lang: string]: PredLabelMapping } = {}; // map { lang -> { pred -> label } }
}
export interface PredLabelMapping { [pred: string]: string }

export interface ResViewSectionsCustomization {
  [lang: string]: ResViewSectionInfoMapping // lang -> section -> label+desc
}
export interface ResViewSectionInfoMapping {
  [section: string]: ResViewSectionDisplayInfo
}
export interface ResViewSectionDisplayInfo {
  label?: string;
  description?: string;
}

export class SearchSettings {
  stringMatchMode: SearchMode = SearchMode.contains;
  useURI: boolean = false;
  useLocalName: boolean = true;
  useNotes: boolean = false;
  restrictLang: boolean = false;
  languages: string[] = [];
  includeLocales: boolean = false;
  useAutocompletion: boolean = false;
  restrictActiveScheme: boolean = true;
  extendToAllIndividuals: boolean = false;
}

export enum SearchMode {
  startsWith = "startsWith",
  contains = "contains",
  endsWith = "endsWith",
  exact = "exact",
  fuzzy = "fuzzy"
}

export enum StatusFilter {
  NOT_DEPRECATED = "NOT_DEPRECATED",
  ONLY_DEPRECATED = "ONLY_DEPRECATED",
  UNDER_VALIDATION = "UNDER_VALIDATION",
  UNDER_VALIDATION_FOR_DEPRECATION = "UNDER_VALIDATION_FOR_DEPRECATION",
  ANYTHING = "ANYTHING",
}

export class ClassTreePreference {
  rootClass: string; //NT serialization of root class
  filter: ClassTreeFilter = { enabled: true, map: {} };
  showInstancesNumber: boolean;

  constructor(project: Project) {
    let modelType = project.getModelType();
    this.rootClass = modelType == RDFS.uri ? RDFS.resource.toNT() : OWL.thing.toNT();
    this.showInstancesNumber = modelType == RDFS.uri || modelType == OWL.uri;
  }
}
export class ClassTreeFilter {
  enabled: boolean = true;
  map: { [key: string]: string[] } = {}; //map where keys are the URIs of a class and the values are the URIs of the subClasses to filter out
}

export class ConceptTreePreference {
  baseBroaderProp: string = SKOS.broader.getURI();
  broaderProps: string[] = [];
  narrowerProps: string[] = [];
  includeSubProps: boolean = true; //tells if the hierarchy should consider
  syncInverse: boolean = true; //tells if the narrower/broader properties should be synced with their inverse
  visualization: ConceptTreeVisualizationMode = ConceptTreeVisualizationMode.hierarchyBased;
  multischemeMode: MultischemeMode = MultischemeMode.or;
  safeToGoLimit: number = 1000;
}

/**
 * checksum: string - it is a representation of the request params (it could be a concat of the params serialization)
 * safe: boolean tells if the tree/list is safe to be initialized, namely if the amount of elements (root/items) are under a safety limit
 */
export interface SafeToGoMap { [checksum: string]: SafeToGo }
export interface SafeToGo { safe: boolean, count?: number }

export enum ConceptTreeVisualizationMode {
  searchBased = "searchBased",
  hierarchyBased = "hierarchyBased"
}

export enum MultischemeMode { //tells if the multi-scheme are considered in AND or in OR when browsing the concept tree
  and = "and",
  or = "or"
}

export enum PrefLabelClashMode {
  forbid = "forbid", //always forbid
  warning = "warning", //warn user about the clash and allows to force
  allow = "allow" //always allow
}

export class LexicalEntryListPreference {
  visualization: LexEntryVisualizationMode = LexEntryVisualizationMode.indexBased;
  indexLength: number = 1;
  safeToGoLimit: number = 1000;
}

export enum LexEntryVisualizationMode {
  searchBased = "searchBased",
  indexBased = "indexBased"
}

export class InstanceListPreference {
  visualization: InstanceListVisualizationMode = InstanceListVisualizationMode.standard;
  safeToGoLimit: number = 1000;
  includeNonDirect: boolean;
}

export class CustomTreeSettings {
  enabled?: boolean;
  rootSelection?: CustomTreeRootSelection;
  roots?: string[];
  type?: string;
  includeSubtype?: boolean;
  hierarchicalProperty: string;
  includeSubProp?: boolean;
  inverseHierarchyDirection?: boolean;
}
export enum CustomTreeRootSelection {
  all = "all",
  with_child_only = "with_child_only",
  enumeration = "enumeration",
}

export enum InstanceListVisualizationMode {
  searchBased = "searchBased",
  standard = "standard"
}

export class ValueFilterLanguages {
  languages: string[];
  enabled: boolean;
}


export interface ResViewTemplate {
  [role: string]: ResViewSection[]; //map resource type => included sections
}

export class SectionFilterPreference {
  [role: string]: ResViewSection[]; //role is a RDFResourceRoleEnum, values are only the hidden sections
}

export enum NotificationStatus {
  no_notifications = "no_notifications",
  in_app_only = "in_app_only",
  email_instant = "email_instant",
  email_daily_digest = "email_daily_digest",
}

/**
 * Class that represents the user settings (preferences) of a Project 
 */
export class ProjectUserSettings {
  renderingLanguagesPreference: string[] = []; //languages that user has assigned for project (and ordered according his preferences)

  editingLanguage: string; //default editing language
  filterValueLang: ValueFilterLanguages; //languages visible in resource description (e.g. in ResourceView, Graph,...)

  activeSchemes: ARTURIResource[] = [];
  activeLexicon: ARTURIResource;
  showFlags: boolean = true;
  projectThemeId: number = null;

  hiddenDataPanels: DataPanel[] = []; //list of hidden panels

  classTreePreferences: ClassTreePreference;
  instanceListPreferences: InstanceListPreference;
  conceptTreePreferences: ConceptTreePreference;
  lexEntryListPreferences: LexicalEntryListPreference;
  customTreeSettings: CustomTreeSettings;

  resViewPreferences: ResourceViewPreference;
  resViewPartitionFilter: SectionFilterPreference = {};

  //graph preferences
  graphViewSectionFilter: SectionFilterPreference;
  hideLiteralGraphNodes: boolean = true;

  searchSettings: SearchSettings;

  sheet2RdfSettings: Sheet2RdfSettings;

  notificationStatus: NotificationStatus = NotificationStatus.no_notifications;

  renderingClassTree: boolean;
  renderingCollectionTree: boolean;
  renderingConceptTree: boolean;
  renderingDatatypeList: boolean;
  renderingInstanceList: boolean;
  renderingLexEntryList: boolean;
  renderingLexiconList: boolean;
  renderingPropertyTree: boolean;
  renderingSchemeList: boolean;
}

export class UserSettings {
  projectVisualization: ProjectVisualization = new ProjectVisualization();
  projectRendering: boolean = false;
}

/**
 * Class that represents the settings of a project (user indipendent)
 */
export class ProjectSettings {
  projectLanguagesSetting: Language[] = []; //all available languages in a project (settings)
  prefLabelClashMode: PrefLabelClashMode = PrefLabelClashMode.forbid;
  resourceView: ResourceViewProjectSettings;
  resViewPredLabelMappings: PredicateLabelSettings;
  resViewSectionsCustomization: ResViewSectionsCustomization;
  dataPanelLabelMappings: DataPanelLabelSetting;
  timeMachineEnabled: boolean = true;
}

/**
 * Class that represents the global application settings
 */
export class SystemSettings {
  experimentalFeaturesEnabled: boolean = false;
  privacyStatementAvailable: boolean = false;
  showFlags: boolean = true;
  homeContent: string;
  languages: Language[];
  emailVerification: boolean = false;
  authService: AuthServiceMode = AuthServiceMode.Default;
}

export class ProjectCreationPreferences {
  aclUniversalAccessDefault: AccessLevel = null;
  openAtStartUpDefault: boolean = false;
  projectVisibilityDefault: ProjectVisibility;
}

export enum AuthServiceMode {
  Default = "Default",
  SAML = "SAML",
  OAuth2 = "OAuth2"
}