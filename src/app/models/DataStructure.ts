import { OntoLex, OWL, RDFS, SKOS } from './Vocabulary';

export enum DataPanel {
    cls = "cls",
    concept = "concept",
    skosCollection = "skosCollection",
    conceptScheme = "conceptScheme",
    property = "property",
    limeLexicon = "limeLexicon",
    ontolexLexicalEntry = "ontolexLexicalEntry",
    dataRange = "dataRange",
    custom = "custom"
}

export interface DataPanelLabelSetting {
    [lang: string]: DataPanelLabelMap
}
export interface DataPanelLabelMap {
    [panel: string]: string
}

export class DataStructureUtils {

    static readonly modelPanelsMap: { [key: string]: DataPanel[] } = {
        [RDFS.uri]: [DataPanel.cls, DataPanel.property, DataPanel.dataRange],
        [OWL.uri]: [DataPanel.cls, DataPanel.property, DataPanel.dataRange],
        [SKOS.uri]: [DataPanel.cls, DataPanel.concept, DataPanel.conceptScheme, 
            DataPanel.skosCollection, DataPanel.property, DataPanel.dataRange],
        [OntoLex.uri]: [DataPanel.cls, DataPanel.concept, DataPanel.conceptScheme, 
            DataPanel.skosCollection, DataPanel.property, DataPanel.limeLexicon, 
            DataPanel.ontolexLexicalEntry, DataPanel.dataRange],
    };

    static readonly panelsPriority: { [key: string]: DataPanel[] } = {
        [RDFS.uri]: [DataPanel.cls, DataPanel.property],
        [OWL.uri]: [DataPanel.cls, DataPanel.property],
        [SKOS.uri]: [DataPanel.concept, DataPanel.conceptScheme, DataPanel.skosCollection],
        [OntoLex.uri]: [DataPanel.limeLexicon, DataPanel.ontolexLexicalEntry, DataPanel.concept, 
            DataPanel.conceptScheme]
    };
}