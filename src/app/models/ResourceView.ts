import { ARTResource, ARTURIResource } from "./ARTResources";
import { VersionInfo } from "./History";
import { Decomp, OntoLex, OWL, RDF, RDFS, SKOS, SKOSXL } from "./Vocabulary";

export enum ResViewSection {
  broaders = "broaders",
  classaxioms = "classaxioms",
  collections = "collections",
  constituents = "constituents",
  datatypeDefinitions = "datatypeDefinitions",
  denotations = "denotations",
  disjointProperties = "disjointProperties",
  domains = "domains",
  equivalentProperties = "equivalentProperties",
  evokedLexicalConcepts = "evokedLexicalConcepts",
  facets = "facets",
  formRepresentations = "formRepresentations",
  imports = "imports",
  labelRelations = "labelRelations",
  lexicalizations = "lexicalizations",
  lexicalForms = "lexicalForms",
  lexicalSenses = "lexicalSenses",
  members = "members",
  membersOrdered = "membersOrdered",
  notes = "notes",
  properties = "properties",
  ranges = "ranges",
  rdfsMembers = "rdfsMembers",
  schemes = "schemes",
  subPropertyChains = "subPropertyChains",
  subterms = "subterms",
  superproperties = "superproperties",
  topconceptof = "topconceptof",
  types = "types"
}

export class ResViewUtils {

  /**
   * sections where add manually functionality is enabled.
   * Note: if a section is added, remember to change the implementation of checkTypeCompliantForManualAdd in the renderer.
   */
  public static addManuallySection: ResViewSection[] = [
    ResViewSection.broaders,
    ResViewSection.classaxioms,
    ResViewSection.constituents,
    ResViewSection.disjointProperties,
    ResViewSection.domains,
    ResViewSection.equivalentProperties,
    ResViewSection.facets,
    ResViewSection.formRepresentations,
    ResViewSection.imports,
    ResViewSection.labelRelations,
    ResViewSection.members,
    ResViewSection.notes,
    ResViewSection.properties,
    ResViewSection.ranges,
    ResViewSection.rdfsMembers,
    ResViewSection.schemes,
    ResViewSection.subterms,
    ResViewSection.superproperties,
    ResViewSection.topconceptof,
    ResViewSection.types
  ];

  /**
   * sections where add external resource functionality is enabled.
   */
  public static addExternalResourceSection: ResViewSection[] = [
    ResViewSection.broaders,
    ResViewSection.classaxioms,
    ResViewSection.disjointProperties,
    ResViewSection.domains,
    ResViewSection.equivalentProperties,
    ResViewSection.facets,
    ResViewSection.labelRelations,
    ResViewSection.members,
    ResViewSection.properties, //how to determine if range is literal?
    ResViewSection.ranges,
    ResViewSection.schemes,
    ResViewSection.superproperties,
    ResViewSection.topconceptof,
    ResViewSection.types
  ];

  /**
   * sections where change property is enabled.
   */
  public static changePropertySection: ResViewSection[] = [
    ResViewSection.types,
    ResViewSection.classaxioms,
    ResViewSection.topconceptof,
    ResViewSection.schemes,
    ResViewSection.broaders,
    ResViewSection.superproperties,
    ResViewSection.equivalentProperties,
    ResViewSection.disjointProperties,
    ResViewSection.subPropertyChains,
    ResViewSection.subterms,
    ResViewSection.domains,
    ResViewSection.ranges,
    // ResViewSection.facets,
    ResViewSection.datatypeDefinitions,
    ResViewSection.lexicalizations,
    ResViewSection.lexicalForms,
    ResViewSection.lexicalSenses,
    ResViewSection.denotations,
    ResViewSection.evokedLexicalConcepts,
    ResViewSection.notes,
    ResViewSection.members,
    // ResViewSection.membersOrdered,
    ResViewSection.labelRelations,
    // ResViewSection.formRepresentations,
    // ResViewSection.imports,
    ResViewSection.constituents,
    ResViewSection.rdfsMembers,
    ResViewSection.properties
  ];

  /**
   * The sections ordered according how they are shown in the RV
   */
  public static orderedResourceViewSections: ResViewSection[] = [
    ResViewSection.types,
    ResViewSection.classaxioms,
    ResViewSection.topconceptof,
    ResViewSection.schemes,
    ResViewSection.broaders,
    ResViewSection.superproperties,
    ResViewSection.equivalentProperties,
    ResViewSection.disjointProperties,
    ResViewSection.subPropertyChains,
    ResViewSection.subterms,
    ResViewSection.domains,
    ResViewSection.ranges,
    ResViewSection.facets,
    ResViewSection.datatypeDefinitions,
    ResViewSection.lexicalizations,
    ResViewSection.lexicalForms,
    ResViewSection.lexicalSenses,
    ResViewSection.denotations,
    ResViewSection.evokedLexicalConcepts,
    ResViewSection.notes,
    ResViewSection.members,
    ResViewSection.membersOrdered,
    ResViewSection.labelRelations,
    ResViewSection.formRepresentations,
    ResViewSection.imports,
    ResViewSection.constituents,
    ResViewSection.rdfsMembers,
    ResViewSection.properties
  ];

  /**
   * Returns the root properties of a given section.
   * Root properties are those properties prompted when the user is enriching a resource from a ResView section
   */
  public static getSectionRootProperties(section: ResViewSection): ARTURIResource[] {
    if (section == ResViewSection.broaders) {
      return [SKOS.broader];
    } else if (section == ResViewSection.classaxioms) {
      return [RDFS.subClassOf, OWL.equivalentClass, OWL.disjointWith, OWL.complementOf, OWL.intersectionOf, OWL.unionOf, OWL.oneOf];
    } else if (section == ResViewSection.constituents) {
      return [Decomp.constituent];
    } else if (section == ResViewSection.datatypeDefinitions) {
      return [OWL.equivalentClass]; //set, but not used
    } else if (section == ResViewSection.denotations) {
      return [OntoLex.denotes];
    } else if (section == ResViewSection.disjointProperties) {
      return [OWL.propertyDisjointWith];
    } else if (section == ResViewSection.domains) {
      return [RDFS.domain];
    } else if (section == ResViewSection.equivalentProperties) {
      return [OWL.equivalentProperty];
    } else if (section == ResViewSection.evokedLexicalConcepts) {
      return [OntoLex.evokes];
    } else if (section == ResViewSection.facets) {
      return [OWL.inverseOf];
    } else if (section == ResViewSection.formRepresentations) {
      return [OntoLex.representation];
    } else if (section == ResViewSection.imports) {
      return [OWL.imports];
    } else if (section == ResViewSection.labelRelations) {
      return [SKOSXL.labelRelation];
    } else if (section == ResViewSection.lexicalForms) {
      return [OntoLex.otherForm, OntoLex.canonicalForm];
    } else if (section == ResViewSection.lexicalSenses) {
      return [OntoLex.sense];
    } else if (section == ResViewSection.lexicalizations) {
      return []; //retrieved from server
    } else if (section == ResViewSection.members) {
      return [SKOS.member];
    } else if (section == ResViewSection.membersOrdered) {
      return [SKOS.memberList]; //provided but not used in the RV section renderer
    } else if (section == ResViewSection.notes) {
      return [SKOS.note];
    } else if (section == ResViewSection.properties) {
      return []; //retrieved from server
    } else if (section == ResViewSection.ranges) {
      return [RDFS.range];
    } else if (section == ResViewSection.rdfsMembers) {
      return [RDFS.member];
    } else if (section == ResViewSection.schemes) {
      return [SKOS.inScheme];
    } else if (section == ResViewSection.subPropertyChains) {
      return [OWL.propertyChainAxiom];
    } else if (section == ResViewSection.subterms) {
      return [Decomp.subterm];
    } else if (section == ResViewSection.superproperties) {
      return [RDFS.subPropertyOf];
    } else if (section == ResViewSection.topconceptof) {
      return [SKOS.topConceptOf];
    } else if (section == ResViewSection.types) {
      return [RDF.type];
    } else {
      return []; //for unknown or custom sections
    }
  }

  /**
   * Returns the known properties of the given section.
   * The known properties are those properties that are described in the section and for which exists dedicated add/remove services
   * (e.g. rdfs:label, skos(xl):pref/alt/hiddenLabel have dedicated service in lexicalizations section).
   * The known properties are used only in Multi-root section renderer in order to know if the section is able to handle the add
   * and delete operations.
   */
  public static getSectionKnownProperties(section: ResViewSection): ARTURIResource[] {
    if (section == ResViewSection.lexicalizations) {
      return [
        RDFS.label, SKOS.prefLabel, SKOS.altLabel, SKOS.hiddenLabel,
        SKOSXL.prefLabel, SKOSXL.altLabel, SKOSXL.hiddenLabel, OntoLex.isDenotedBy
      ];
    } else {
      return this.getSectionRootProperties(section);
    }
  }

}

export class PropertyFacet {
  name: string;
  value: boolean;
  explicit: boolean;
}

export enum PropertyFacetsEnum {
  symmetric = "symmetric",
  asymmetric = "asymmetric",
  functional = "functional",
  inverseFunctional = "inverseFunctional",
  reflexive = "reflexive",
  irreflexive = "irreflexive",
  transitive = "transitive"
}

export enum AddAction {
  default = "default",
  manually = "manually",
  remote = "remote"
}

export class ResourceViewCtx {
  versions: VersionInfo[]; //caches the versions for sharing them among different ResView (no need to initialize them at each RV)
}

export class ValueClickEvent {
  ctrl?: boolean; //to open in a new tabset
  shift?: boolean; //to open in the side tabset
  double?: boolean; //to open in a new tabset
  value: ARTResource;

  constructor(value: ARTResource, event?: MouseEvent, double?: boolean) {
    this.value = value;
    this.ctrl = event?.metaKey || event?.ctrlKey;
    this.shift = event?.shiftKey;
    this.double = double;
  }
}