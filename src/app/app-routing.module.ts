import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SystemConfigurationComponent } from './administration/system-configuration/systemConfigurationComponent';
import { AlignmentValidationComponent } from './alignment/alignmentValidation/alignment-validation.component';
import { CollaborationComponent } from './collaboration/collaborationComponent';
import { ExportDataComponent } from './config/dataManagement/exportData/export-data.component';
import { LoadDataComponent } from './config/dataManagement/loadData/loadDataComponent';
import { RefactorComponent } from './config/dataManagement/refactor/refactor.component';
import { VersioningComponent } from './config/dataManagement/versioning/versioningComponent';
import { CustomFormConfigComponent } from './custom-forms/custom-form-conf.component';
import { CustomServiceRouterComponent } from './custom-services/customServiceRouterComponent';
import { CustomFormViewPageComponent } from './custom-views/custom-form-view-page.component';
import { DataComponent } from './data/data.component';
import { EdoalComponent } from './edoal/edoal.component';
import { HistoryComponent } from './history-validation/history.component';
import { ValidationComponent } from './history-validation/validation.component';
import { HomeComponent } from './home.component';
import { MetadataRegistryComponent } from './metadata/metadataRegistry/metadataRegistryComponent';
import { MetadataVocabulariesComponent } from './metadata/metadataVocabularies/metadata-vocabularies.component';
import { NamespacesAndImportsComponent } from './metadata/namespacesAndImports/namespaces-and-imports.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { VocbenchPreferencesComponent } from './preferences/vocbenchPreferencesComponent';
import { CreateProjectComponent } from './project/create-project/create-project.component';
import { ProjectComponent } from './project/project.component';
import { ResourceMetadataComponent } from './resource-metadata/resource-metadata.component';
import { Sheet2RdfComponent } from './sheet2rdf/sheet2rdf.component';
import { SkosDiffingComponent } from './skos-diffing/skos-diffing.component';
import { SparqlComponent } from './sparql/sparql-component.component';
import { RegistrationComponent } from './user/registration.component';
import { ResetPasswordComponent } from './user/reset-password.component';
import { UserActionsComponent } from './user/userActionsComponent';
import { UserProfileComponent } from './user/user-profile.component';
import { adminGuard, authGuard, pmGuard, projectGuard, superUserGuard } from './utils/CanActivateGuards';
import { UserResolver } from './utils/UserResolver';

const routes: Routes = [
  { path: "", redirectTo: "/Home", pathMatch: "full" },
  { path: "Home", component: HomeComponent, resolve: { user: UserResolver } },
  // // route config of navigation bar
  { path: "Projects", component: ProjectComponent, canActivate: [adminGuard] },
  { path: "Projects/CreateProject", component: CreateProjectComponent, canActivate: [superUserGuard] },
  { path: "Data", component: DataComponent, canActivate: [authGuard, projectGuard] },
  { path: "Edoal", component: EdoalComponent, canActivate: [authGuard, projectGuard] },
  { path: "Sparql", component: SparqlComponent, canActivate: [authGuard, projectGuard] },
  { path: "Vocabularies", component: MetadataVocabulariesComponent, canActivate: [authGuard, projectGuard] },
  { path: "Imports", component: NamespacesAndImportsComponent, canActivate: [authGuard, projectGuard] },
  { path: "MetadataRegistry", component: MetadataRegistryComponent, canActivate: [authGuard, projectGuard] },
  { path: "History", component: HistoryComponent, canActivate: [authGuard, projectGuard] },
  { path: "Validation", component: ValidationComponent, canActivate: [authGuard, projectGuard] },
  { path: "AlignmentValidation", component: AlignmentValidationComponent, canActivate: [authGuard, projectGuard] },
  { path: "Sheet2RDF", component: Sheet2RdfComponent, canActivate: [authGuard, projectGuard] },
  { path: "SkosDiffing", component: SkosDiffingComponent, canActivate: [authGuard, projectGuard] },
  { path: "Collaboration", component: CollaborationComponent, canActivate: [authGuard, projectGuard] },
  { path: "CustomForm", component: CustomFormConfigComponent, canActivate: [authGuard, projectGuard] },
  { path: "CustomFormView", component: CustomFormViewPageComponent, canActivate: [authGuard, projectGuard] },
  { path: "CustomServices", component: CustomServiceRouterComponent, canActivate: [authGuard, projectGuard] },
  { path: "Notifications", component: NotificationsComponent, canActivate: [authGuard, projectGuard] },
  { path: "ResourceMetadata", component: ResourceMetadataComponent, canActivate: [pmGuard, projectGuard] },
  { path: "LoadData", component: LoadDataComponent, canActivate: [authGuard, projectGuard] },
  { path: "ExportData", component: ExportDataComponent, canActivate: [authGuard, projectGuard] },
  { path: "Refactor", component: RefactorComponent, canActivate: [authGuard, projectGuard] },
  { path: "Versioning", component: VersioningComponent, canActivate: [authGuard, projectGuard] },
  { path: "Sysconfig", component: SystemConfigurationComponent, canActivate: [authGuard] },
  { path: "Registration/:firstAccess", component: RegistrationComponent }, //param firstAccess 1 if no user registered
  { path: "Profile", component: UserProfileComponent, canActivate: [authGuard] },
  { path: "Preferences", component: VocbenchPreferencesComponent, canActivate: [authGuard, projectGuard] },
  { path: "ResetPassword/:token", component: ResetPasswordComponent },
  { path: "UserActions", component: UserActionsComponent },
  //lazy loading of module with child route
  {
    path: "Administration",
    loadChildren: () => import('./modules/administrationModule').then(m => m.AdministrationModule),
    canLoad: [authGuard]
  },
  {
    path: "Icv",
    loadChildren: () => import('./modules/icvModule').then(m => m.IcvModule),
    canLoad: [authGuard, projectGuard]
  },
  {
    path: "Sepia",
    loadChildren: () => import('./sep/sepModule').then(m => m.SepModule),
    canLoad: [authGuard, projectGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
