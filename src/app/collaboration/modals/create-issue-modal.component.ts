import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Settings } from "../../models/Plugins";
import { CollaborationServices } from "../../services/collaboration.service";

@Component({
    selector: "create-issue-modal",
    templateUrl: "./create-issue-modal.component.html",
    standalone: false
})
export class CreateIssueModalComponent {

    form: Settings;

    constructor(public activeModal: NgbActiveModal, private collaborationService: CollaborationServices) { }

    ngOnInit() {
        this.collaborationService.getIssueCreationForm().subscribe(
            form => {
                this.form = form;
            }
        );
    }

    isOkClickable(): boolean {
        return this.form && !this.form.requireConfiguration();
    }

    ok() {
        let formMap = this.form.getPropertiesAsMap();
        this.activeModal.close(formMap);
    }

    cancel() {
        this.activeModal.dismiss();
    }

}