import { Component, ElementRef, ViewChild } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { CollaborationServices } from "../../services/collaboration.service";
import { UIUtils } from "../../utils/UIUtils";
import { VBCollaboration } from "../../utils/VBCollaboration";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { finalize } from "rxjs";

@Component({
  selector: "collaboration-proj-modal",
  templateUrl: "./collaboration-project-modal.component.html",
  standalone: false
})
export class CollaborationProjectModalComponent {

  @ViewChild('blockDiv', { static: true }) blockDivElement: ElementRef;

  headers: string[];
  projects: any[] = [];
  selectedProject: any;

  constructor(public activeModal: NgbActiveModal, private collaborationService: CollaborationServices,
    private vbCollaboration: VBCollaboration, private basicModals: BasicModalServices) {
  }

  ngOnInit() {
    this.initProjectList();
  }

  private initProjectList() {
    UIUtils.startLoadingDiv(this.blockDivElement.nativeElement);
    this.collaborationService.listProjects().subscribe({
      next: resp => {
        UIUtils.stopLoadingDiv(this.blockDivElement.nativeElement);
        this.headers = resp.headers;
        this.projects = resp.projects;
      },
      error: (err: Error) => {
        if (err.name.endsWith("ConnectException")) {
          this.basicModals.alert({ key: "STATUS.ERROR" }, { key: "MESSAGES.CANNOT_RETRIEVE_ISSUES_CONNECTION_FAILED" }, ModalType.error, err.name + " " + err.message);
        } else if (err.name.endsWith("CollaborationBackendException")) {
          this.basicModals.alert({ key: "STATUS.ERROR" }, { key: "MESSAGES.CANNOT_RETRIEVE_ISSUES_LOGIN_FAILED" }, ModalType.error, err.stack);
        }
        this.vbCollaboration.setWorking(false);
        this.activeModal.dismiss();
      }
    });
  }

  createProject() {
    let projectProps: { [key: string]: string } = {};
    this.headers.forEach((h: string) => {
      projectProps[h] = null;
    });
    this.basicModals.promptProperties({ key: "COMMONS.ACTIONS.CREATE_PROJECT" }, projectProps, false).then(
      props => {
        this.collaborationService.createProject(props).subscribe(
          () => {
            this.initProjectList();
          }
        );
      },
      () => { }
    );
  }

  selectProject(p: { id: string, key: string, name: string }) {
    this.selectedProject = p;
  }


  ok() {
    UIUtils.startLoadingDiv(this.blockDivElement.nativeElement);
    this.collaborationService.assignProject(this.selectedProject).pipe(
      finalize(() => {
        UIUtils.stopLoadingDiv(this.blockDivElement.nativeElement);
      })
    ).subscribe(
      () => {
        this.activeModal.close();
      }
    );
  }

  cancel() {
    this.activeModal.dismiss();
  }

}