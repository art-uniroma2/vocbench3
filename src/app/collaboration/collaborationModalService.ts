import { Injectable } from '@angular/core';
import { CollaborationProjSettingsModalComponent } from "./modals/collaboration-proj-settings-modal.component";
import { CollaborationUserSettingsModalComponent } from "./modals/collaboration-user-settings-modal.component";
import { CollaborationProjectModalComponent } from "./modals/collaboration-project-modal.component";
import { CreateIssueModalComponent } from "./modals/create-issue-modal.component";
import { IssueListModalComponent } from "./issue-list-modal.component";
import { Issue } from '../models/Collaboration';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalOptions } from '../modal-dialogs/Modals';

/**
 * Service to open modals that allow to create a classes list or instances list
 */
@Injectable()
export class CollaborationModalServices {

    constructor(private modalService: NgbModal) { }

    /**
     * Opens a modal to list the collaboration system issues and select one
     * @return if the modal closes with ok returns an Issue
     */
    openIssueList(): Promise<Issue> {
        return this.modalService.open(IssueListModalComponent, new ModalOptions()).result;
    }

    /**
     * Opens a modal to edit the collaboration system project settings
     */
    editCollaborationProjectSettings() {
        return this.modalService.open(CollaborationProjSettingsModalComponent, new ModalOptions()).result;
    }

    /**
     * Opens a modal to edit the collaboration system user settings
     */
    editCollaborationUserSettings() {
        return this.modalService.open(CollaborationUserSettingsModalComponent, new ModalOptions()).result;
    }

    /**
     * Opens a modal to create or assign a collaboration project
     */
    editCollaborationProject() {
        return this.modalService.open(CollaborationProjectModalComponent, new ModalOptions()).result;
    }

    /**
     * 
     */
    createIssue() {
        return this.modalService.open(CreateIssueModalComponent, new ModalOptions()).result;
    }

}