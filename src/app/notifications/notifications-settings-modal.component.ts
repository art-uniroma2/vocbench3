import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: "notifications-settings-modal",
    templateUrl: "./notifications-settings-modal.component.html",
    standalone: false
})
export class NotificationSettingsModalComponent {

    constructor(public activeModal: NgbActiveModal) {}

    ok() {
        this.activeModal.close();
    }

}