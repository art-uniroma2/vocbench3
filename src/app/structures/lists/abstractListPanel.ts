import { ChangeDetectorRef, Directive } from '@angular/core';
import { GraphModalServices } from "../../graph/modals/graph-modal.service";
import { ARTURIResource, RDFResourceRolesEnum } from "../../models/ARTResources";
import { CustomFormsServices } from "../../services/custom-forms.service";
import { ResourcesServices } from "../../services/resources.service";
import { ActionDescription, RoleActionResolver } from "../../utils/RoleActionResolver";
import { VBEventHandler } from "../../utils/VBEventHandler";
import { VBProperties } from "../../utils/VBProperties";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { SharedModalServices } from '../../modal-dialogs/shared-modals/shared-modals.service';
import { AbstractPanel } from "../abstractPanel";
import { MultiSubjectEnrichmentHelper } from "../multiSubjectEnrichmentHelper";

@Directive()
export abstract class AbstractListPanel extends AbstractPanel {

  /**
   * VIEWCHILD, INPUTS / OUTPUTS
   */

  /**
   * ATTRIBUTES
   */

  /**
   * CONSTRUCTOR
   */
  constructor(
    cfService: CustomFormsServices,
    resourceService: ResourcesServices,
    basicModals: BasicModalServices,
    sharedModals: SharedModalServices,
    graphModals: GraphModalServices,
    eventHandler: VBEventHandler,
    vbProp: VBProperties,
    actionResolver: RoleActionResolver,
    multiEnrichment: MultiSubjectEnrichmentHelper,
    cdRef: ChangeDetectorRef
  ) {
    super(cfService, resourceService, basicModals, sharedModals, graphModals, eventHandler, vbProp, actionResolver, multiEnrichment, cdRef);
  }

  /**
   * METHODS
   */

  executeAction(act: ActionDescription, role?: RDFResourceRolesEnum) {
    act.function(this.getActionContext(role), this.selectedNode).subscribe(
      () => {
        if (act.conditions.post.deselectOnComplete) {
          this.selectedNode = null;
        }
      }
    );
  }

  abstract openAt(node: ARTURIResource): void;

}