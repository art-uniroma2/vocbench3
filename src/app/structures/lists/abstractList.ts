import { ChangeDetectorRef, Directive, QueryList } from "@angular/core";
import { ARTResource, ARTURIResource, ResAttribute } from "../../models/ARTResources";
import { VBEventHandler } from "../../utils/VBEventHandler";
import { AbstractStruct } from "../abstractStruct";
import { AbstractListNode } from "./abstractListNode";

@Directive()
export abstract class AbstractList extends AbstractStruct {

  abstract viewChildrenNode: QueryList<AbstractListNode>;

  protected pendingSearchRes: ARTURIResource; //searched resource that is waiting to be selected once the list is initialized

  /**
   * ATTRIBUTES
   */

  /**
   * CONSTRUCTOR
   */
  protected changeDetectorRef: ChangeDetectorRef;
  constructor(eventHandler: VBEventHandler, changeDetectorRef: ChangeDetectorRef) {
    super(eventHandler);
    this.changeDetectorRef = changeDetectorRef;
    this.eventSubscriptions.push(this.eventHandler.resourceCreatedUndoneEvent.subscribe(
      (node: ARTURIResource) => this.onResourceCreatedUndone(node)
    ));
  }

  /**
   * METHODS
   */

  init() {
    this.setInitialStatus();
    this.initImpl();
  }

  abstract initImpl(): void;

  /**
   * type of the node param depends on the list implementation
   * (for example in instance list it is an object with individual and its class, in scheme list it is simply the scheme)
   */
  // abstract selectNode(node: any): void;
  abstract onListNodeCreated(node: ARTURIResource): void;
  abstract onListNodeDeleted(node: ARTURIResource): void;
  abstract onResourceCreatedUndone(node: ARTResource): void;

  openListAt(node: ARTURIResource) {
    this.ensureNodeVisibility(node);
    this.changeDetectorRef.detectChanges(); //wait that the children node is rendered (in case the openPages has been increased)
    let childrenNodeComponent = this.viewChildrenNode.toArray();
    for (const c of childrenNodeComponent) {
      if (c.node.equals(node)) {
        c.ensureVisible();
        if (!c.node.getAdditionalProperty(ResAttribute.SELECTED)) {
          c.selectNode();
        }
        break;
      }
    }
  }

  ensureNodeVisibility(resource: ARTURIResource) {
    for (let i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i].equals(resource)) {
        if (i >= this.nodeLimit) {
          //update nodeLimit so that node at index i is within the range
          let scrollStep: number = ((i - this.nodeLimit) / this.increaseRate) + 1;
          this.nodeLimit += this.increaseRate * scrollStep;
        }
        this.pendingSearchRes = null; //if there was any pending search, reset it
        return; //node found and visible
      }
    }
    //if this code is reached, the node is not found (so probably it is waiting that the list is initialized)
    this.pendingSearchRes = resource;
  }

}