import { ChangeDetectorRef, Component, QueryList, ViewChildren } from "@angular/core";
import { finalize } from "rxjs";
import { ARTResource, ARTURIResource, RDFResourceRolesEnum, ResAttribute } from "../../../models/ARTResources";
import { Project } from "../../../models/Project";
import { SemanticTurkey } from "../../../models/Vocabulary";
import { SkosServices } from "../../../services/skos.service";
import { AuthorizationEvaluator } from "../../../utils/AuthorizationEvaluator";
import { STRequestOptions } from "../../../utils/HttpManager";
import { ResourceUtils, SortAttribute } from "../../../utils/ResourceUtils";
import { TreeListContext } from "../../../utils/UIUtils";
import { VBActionsEnum } from "../../../utils/VBActions";
import { VBContext } from "../../../utils/VBContext";
import { VBEventHandler } from "../../../utils/VBEventHandler";
import { VBProperties } from "../../../utils/VBProperties";
import { AbstractList } from "../abstractList";
import { SchemeListNodeComponent } from "./scheme-list-node.component";
import { NodeSelectEvent } from "../../abstractNode";

@Component({
  selector: "scheme-list",
  templateUrl: "./scheme-list.component.html",
  host: { class: "treeListComponent" },
  standalone: false
})
export class SchemeListComponent extends AbstractList {

  @ViewChildren(SchemeListNodeComponent) viewChildrenNode: QueryList<SchemeListNodeComponent>;

  structRole = RDFResourceRolesEnum.conceptScheme;

  constructor(private skosService: SkosServices, private vbProp: VBProperties, eventHandler: VBEventHandler, changeDetectorRef: ChangeDetectorRef) {
    super(eventHandler, changeDetectorRef);
    this.eventSubscriptions.push(eventHandler.schemeCreatedEvent.subscribe((node: ARTURIResource) => this.onListNodeCreated(node)));
    this.eventSubscriptions.push(eventHandler.schemeDeletedEvent.subscribe((node: ARTURIResource) => this.onListNodeDeleted(node)));
    this.eventSubscriptions.push(eventHandler.schemeDeletedUndoneEvent.subscribe((node: ARTURIResource) => this.onDeletedUndo(node)));
    //handler when active schemes is changed programmatically when a searched concept belong to a non active scheme
    this.eventSubscriptions.push(eventHandler.schemeChangedEvent.subscribe(
      (data: { schemes: ARTURIResource[], project: Project }) => {
        if (VBContext.getWorkingProjectCtx(this.projectCtx).getProject().getName() == data.project.getName()) {
          this.onSchemeChanged(data.schemes);
        }
      })
    );
  }

  ngOnInit() {
    if (!AuthorizationEvaluator.isAuthorized(VBActionsEnum.skosGetSchemes)) {
      this.unauthorized = true;
      return;
    }
    this.init();
  }

  initImpl() {
    this.loading = true;
    this.skosService.getAllSchemes(this.showDeprecated, STRequestOptions.getRequestOptions(this.projectCtx)).pipe(
      finalize(() => { this.loading = false; })
    ).subscribe(
      schemes => {
        //sort by show if rendering is active, uri otherwise
        ResourceUtils.sortResources(schemes, this.rendering ? SortAttribute.show : SortAttribute.value);
        for (const s of schemes) {
          let active: boolean = ResourceUtils.containsNode(VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().activeSchemes, s);
          s['checked'] = active;
          this.nodes.push(s);
        }
      }
    );
  }

  onListNodeCreated(node: ARTURIResource) {
    this.nodes.unshift(node);
    if (this.context == TreeListContext.addPropValue) {
      this.nodeSelected.emit(new NodeSelectEvent(node));
    }
  }

  onListNodeDeleted(node: ARTURIResource) {
    for (let i = 0; i < this.nodes.length; i++) { //Update the schemeList
      if (this.nodes[i].equals(node)) {
        if (VBContext.getWorkingProject().isValidationEnabled()) {
          //replace the resource instead of simply change the graphs, so that the rdfResource detect the change
          let stagedRes: ARTURIResource = this.nodes[i].clone() as ARTURIResource;
          stagedRes.setGraphs([new ARTURIResource(SemanticTurkey.stagingRemoveGraph + VBContext.getWorkingProject().getBaseURI())]);
          stagedRes.setAdditionalProperty(ResAttribute.EXPLICIT, false);
          stagedRes.setAdditionalProperty(ResAttribute.SELECTED, false);
          this.nodes[i] = stagedRes;
        } else {
          this.nodes.splice(i, 1);
        }
        break;
      }
    }
    //update the activeSchemes if the deleted was active
    if (ResourceUtils.containsNode(VBContext.getWorkingProjectCtx().getProjectPreferences().activeSchemes, node)) {
      this.updateActiveSchemesPref();
    }
    this.selectedNode = null;
  }

  onResourceCreatedUndone(node: ARTResource) {
    for (let i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i].equals(node)) {
        this.nodes.splice(i, 1);
        break;
      }
    }
    //if "undoned" the creation of an active scheme, remove it from the active schemes
    if (ResourceUtils.containsNode(VBContext.getWorkingProjectCtx().getProjectPreferences().activeSchemes, node)) {
      this.updateActiveSchemesPref();
    }
  }

  onDeletedUndo(node: ARTURIResource) {
    this.nodes.push(node);
  }

  //@Override
  ensureNodeVisibility(resource: ARTURIResource) {
    for (let i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i].equals(resource)) {
        if (i >= this.nodeLimit) {
          //update nodeLimit so that node at index i is within the range
          let scrollStep: number = ((i - this.nodeLimit) / this.increaseRate) + 1;
          this.nodeLimit += this.increaseRate * scrollStep;
        }
        break;
      }
    }
  }

  public activateAllScheme() {
    for (let n of this.nodes) {
      n['checked'] = true;
    }
    this.updateActiveSchemesPref();
  }

  public deactivateAllScheme() {
    for (let n of this.nodes) {
      n['checked'] = false;
    }
    this.updateActiveSchemesPref();
  }

  updateActiveSchemesPref() {
    this.vbProp.setActiveSchemes(VBContext.getWorkingProjectCtx(this.projectCtx), this.collectCheckedSchemes()).subscribe();
  }

  /**
   * Collects all the schemes checked
   */
  private collectCheckedSchemes(): ARTURIResource[] {
    //collect all the active scheme
    let activeSchemes: ARTURIResource[] = [];
    for (const n of this.nodes) {
      if (n['checked']) {
        activeSchemes.push(n as ARTURIResource);
      }
    }
    return activeSchemes;
  }


  private onSchemeChanged(schemes: ARTURIResource[]) {
    this.nodes.forEach(s => { s['checked'] = ResourceUtils.containsNode(schemes, s); });
  }

}


// class SchemeListItem {
//   public checked: boolean;
//   public scheme: ARTURIResource;
// }