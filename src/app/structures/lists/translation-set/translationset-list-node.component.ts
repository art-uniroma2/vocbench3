import { Component } from "@angular/core";
import { VBEventHandler } from "../../../utils/VBEventHandler";
import { AbstractListNode } from "../abstractListNode";

@Component({
    selector: "translationset-list-node",
    templateUrl: "./translationset-list-node.component.html",
    standalone: false
})
export class TranslationSetListNodeComponent extends AbstractListNode {

    constructor(eventHandler: VBEventHandler) {
        super(eventHandler);
    }

}