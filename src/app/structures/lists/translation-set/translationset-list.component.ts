import { ChangeDetectorRef, Component, QueryList, ViewChildren } from "@angular/core";
import { finalize } from "rxjs";
import { OntoLexLemonServices } from "src/app/services/ontolex-lemon.service";
import { ARTResource, ARTURIResource, RDFResourceRolesEnum, ResAttribute } from "../../../models/ARTResources";
import { SemanticTurkey } from "../../../models/Vocabulary";
import { AuthorizationEvaluator } from "../../../utils/AuthorizationEvaluator";
import { ResourceUtils, SortAttribute } from "../../../utils/ResourceUtils";
import { TreeListContext } from "../../../utils/UIUtils";
import { VBActionsEnum } from "../../../utils/VBActions";
import { VBContext } from "../../../utils/VBContext";
import { VBEventHandler } from "../../../utils/VBEventHandler";
import { AbstractList } from "../abstractList";
import { TranslationSetListNodeComponent } from "./translationset-list-node.component";
import { NodeSelectEvent } from "../../abstractNode";

@Component({
  selector: "translationset-list",
  templateUrl: "./translationset-list.component.html",
  host: { class: "treeListComponent" },
  standalone: false
})
export class TranslationSetListComponent extends AbstractList {

  @ViewChildren(TranslationSetListNodeComponent) viewChildrenNode: QueryList<TranslationSetListNodeComponent>;

  structRole = RDFResourceRolesEnum.vartransTranslationSet;

  constructor(private ontolexService: OntoLexLemonServices, eventHandler: VBEventHandler, changeDetectorRef: ChangeDetectorRef) {
    super(eventHandler, changeDetectorRef);
    this.eventSubscriptions.push(eventHandler.translationSetCreatedEvent.subscribe((node: ARTURIResource) => this.onListNodeCreated(node)));
    this.eventSubscriptions.push(eventHandler.translationSetDeletedEvent.subscribe((node: ARTURIResource) => this.onListNodeDeleted(node)));
    this.eventSubscriptions.push(eventHandler.translationSetDeletedUndoneEvent.subscribe((node: ARTURIResource) => this.onDeletedUndo(node)));
  }

  ngOnInit() {
    if (!AuthorizationEvaluator.isAuthorized(VBActionsEnum.datatypesGetDatatype)) {
      this.unauthorized = true;
      return;
    }
    this.init();
  }

  initImpl() {
    this.loading = true;
    this.ontolexService.getTranslationSets(this.showDeprecated).pipe(
      finalize(() => { this.loading = false; })
    ).subscribe(
      sets => {
        ResourceUtils.sortResources(sets, this.rendering ? SortAttribute.show : SortAttribute.value);
        this.nodes = sets.filter(s => s instanceof ARTURIResource);
      }
    );
  }

  onListNodeCreated(node: ARTURIResource) {
    this.nodes.unshift(node);
    if (this.context == TreeListContext.addPropValue) {
      this.nodeSelected.emit(new NodeSelectEvent(node));
    }
  }

  onListNodeDeleted(node: ARTURIResource) {
    for (let i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i].equals(node)) {
        if (VBContext.getWorkingProject().isValidationEnabled()) {
          //replace the resource instead of simply change the graphs, so that the rdfResource detect the change
          let stagedRes: ARTURIResource = this.nodes[i].clone() as ARTURIResource;
          stagedRes.setGraphs([new ARTURIResource(SemanticTurkey.stagingRemoveGraph + VBContext.getWorkingProject().getBaseURI())]);
          stagedRes.setAdditionalProperty(ResAttribute.EXPLICIT, false);
          stagedRes.setAdditionalProperty(ResAttribute.SELECTED, false);
          this.nodes[i] = stagedRes;
        } else {
          this.nodes.splice(i, 1);
        }
        break;
      }
    }
  }

  onResourceCreatedUndone(node: ARTResource) {
    for (let i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i].equals(node)) {
        this.nodes.splice(i, 1);
        break;
      }
    }
  }

  onDeletedUndo(node: ARTURIResource) {
    this.nodes.push(node);
  }

}