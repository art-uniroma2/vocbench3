import { ChangeDetectorRef, Component, EventEmitter, Input, Output, QueryList, SimpleChanges, ViewChildren } from "@angular/core";
import { Observable, of } from "rxjs";
import { finalize, mergeMap } from "rxjs/operators";
import { ARTResource, ARTURIResource, RDFResourceRolesEnum, ResAttribute } from "../../../models/ARTResources";
import { InstanceListPreference, InstanceListVisualizationMode, SafeToGo } from "../../../models/Properties";
import { SemanticTurkey } from "../../../models/Vocabulary";
import { ClassesServices } from "../../../services/classes.service";
import { AuthorizationEvaluator } from "../../../utils/AuthorizationEvaluator";
import { STRequestOptions } from "../../../utils/HttpManager";
import { ResourceUtils, SortAttribute } from "../../../utils/ResourceUtils";
import { TreeListContext } from "../../../utils/UIUtils";
import { VBActionsEnum } from "../../../utils/VBActions";
import { VBContext } from "../../../utils/VBContext";
import { InstanceDeleteUndoData, VBEventHandler } from "../../../utils/VBEventHandler";
import { AbstractList } from "../abstractList";
import { InstanceListNodeComponent } from "./instance-list-node.component";
import { NodeSelectEvent } from "../../abstractNode";

@Component({
  selector: "instance-list",
  templateUrl: "./instance-list.component.html",
  host: { class: "treeListComponent" },
  standalone: false
})
export class InstanceListComponent extends AbstractList {
  @Input() cls: ARTURIResource;
  @Input() forceVisualizationMode: InstanceListVisualizationMode; //if not null, forces visualization mode regardles of configuration
  @Output() switchMode = new EventEmitter<InstanceListVisualizationMode>(); //requires to the parent panel to switch mode

  //InstanceListNodeComponent children of this Component (useful to select the instance during the search)
  @ViewChildren(InstanceListNodeComponent) viewChildrenNode: QueryList<InstanceListNodeComponent>;

  private pendingSearchCls: ARTURIResource; //class of a searched instance that is waiting to be selected once the list is initialized

  visualizationMode: InstanceListVisualizationMode;//this could be changed dynamically, so each time it is used, get it again from preferences

  structRole = RDFResourceRolesEnum.individual;

  translationParam: { count: number, safeToGoLimit: number };

  InstanceListVisualizationMode = InstanceListVisualizationMode;

  constructor(private clsService: ClassesServices, eventHandler: VBEventHandler, changeDetectorRef: ChangeDetectorRef) {
    super(eventHandler, changeDetectorRef);
    this.eventSubscriptions.push(eventHandler.instanceDeletedEvent.subscribe(
      (data: { instance: ARTURIResource, cls: ARTURIResource }) => {
        if (this.cls == null) return;
        if (data.cls.equals(this.cls)) this.onListNodeDeleted(data.instance);
      }
    ));
    this.eventSubscriptions.push(eventHandler.instanceCreatedEvent.subscribe(
      (data: { instance: ARTResource, cls: ARTResource }) => {
        if (this.cls == null) return;
        if (data.cls.equals(this.cls)) this.onListNodeCreated(data.instance as ARTURIResource);
      }
    ));
    this.eventSubscriptions.push(eventHandler.typeRemovedEvent.subscribe(
      (data: { resource: ARTResource, type: ARTResource }) => this.onTypeRemoved(data.resource, data.type as ARTURIResource)));
    this.eventSubscriptions.push(eventHandler.instanceDeletedUndoneEvent.subscribe(
      (data: InstanceDeleteUndoData) => {
        if (this.cls == null) return;
        if (data.types.some(t => t.equals(this.cls))) {
          this.nodes.push(data.resource);
        }
      }
    ));
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['cls']) {
      this.init();
    }
  }

  initImpl() {
    if (!AuthorizationEvaluator.isAuthorized(VBActionsEnum.classesGetInstances)) {
      this.unauthorized = true;
      return;
    }

    let instListPref: InstanceListPreference = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().instanceListPreferences;

    this.visualizationMode = this.forceVisualizationMode ? this.forceVisualizationMode : instListPref.visualization;
    if (this.cls != null) { //class provided => init list
      if (this.visualizationMode == InstanceListVisualizationMode.standard) {
        this.checkInitializationSafe().subscribe(
          () => {
            if (this.safeToGo.safe) {
              this.loading = true;
              this.clsService.getInstances(this.cls, instListPref.includeNonDirect, this.showDeprecated, STRequestOptions.getRequestOptions(this.projectCtx)).pipe(
                finalize(() => { this.loading = false; })
              ).subscribe(
                instances => {
                  //sort by show if rendering is active, uri otherwise
                  ResourceUtils.sortResources(instances, this.rendering ? SortAttribute.show : SortAttribute.value);
                  this.nodes = instances;
                  this.resumePendingSearch();
                }
              );
            }
          }
        );
      } else { //search based
        //reset to empty list and check for pending search
        this.forceList(null);
        this.resumePendingSearch();
      }
    } else { //class not provided, reset the instance list
      this.setInitialStatus();
      //prevent ExpressionChangedAfterItHasBeenCheckedError on isOpenGraphEnabled('dataOriented') in the parent panel
      this.changeDetectorRef.detectChanges();
    }
  }

  private resumePendingSearch() {
    // if there is some pending search where the class is same class which instance are currently described
    if (
      this.pendingSearchRes &&
      (
        (this.pendingSearchCls && this.pendingSearchCls.equals(this.cls)) ||
        !this.pendingSearchCls //null if already checked that the pendingSearchCls is the current (see selectSearchedInstance)
      )
    ) {
      if (VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().instanceListPreferences.visualization == InstanceListVisualizationMode.standard) {
        this.openListAt(this.pendingSearchRes); //standard mode => simply open list (focus searched res)
      } else { //search mode => set the pending searched resource as only element of the list and then focus it
        this.forceList([this.pendingSearchRes]);
        this.changeDetectorRef.detectChanges();
        this.openListAt(this.pendingSearchRes);
      }
    }
  }

  /**
   * Perform a check in order to prevent the initialization of the structure with too many elements
   * Return true if the initialization is safe or if the user agreed to init the structure anyway
   */
  private checkInitializationSafe(): Observable<void> {
    let instListPreference: InstanceListPreference = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().instanceListPreferences;
    if (this.safeToGoLimit != instListPreference.safeToGoLimit) {
      this.safeToGoMap = {}; //limit changed, safetiness checks invalidated => reset the map
    }
    this.safeToGoLimit = instListPreference.safeToGoLimit;

    let checksum = this.getInitRequestChecksum();

    let safeness: SafeToGo = this.safeToGoMap[checksum];
    if (safeness != null) { //found safeness in cache
      this.safeToGo = safeness;
      this.translationParam = { count: this.safeToGo.count, safeToGoLimit: this.safeToGoLimit };
      return of(null);
    } else { //never initialized => count
      this.loading = true;
      return this.getNumberOfInstances(this.cls).pipe(
        finalize(() => { this.loading = false; }),
        mergeMap(count => {
          safeness = { safe: count < this.safeToGoLimit, count: count };
          this.safeToGoMap[checksum] = safeness; //cache the safeness
          this.safeToGo = safeness;
          this.translationParam = { count: this.safeToGo.count, safeToGoLimit: this.safeToGoLimit };
          return of(null);
        })
      );
    }
  }

  private getInitRequestChecksum() {
    let checksum = "cls:" + this.cls.toNT() + 
      "&showDeprecated:" + this.showDeprecated;
    return checksum;
  }

  /**
   * Forces the safeness of the structure even if it was reported as not safe, then re initialize it
   */
  forceSafeness() {
    this.safeToGo = { safe: true };
    let checksum = this.getInitRequestChecksum();
    this.safeToGoMap[checksum] = this.safeToGo;
    this.initImpl();
  }

  /**
   * Returns the number of instances of the given class. Useful when the user select a class in order to check if there 
   * are too many instances.
   * @param cls 
   */
  private getNumberOfInstances(cls: ARTURIResource, includeDeprecated?: boolean): Observable<number> {
    if (VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().classTreePreferences.showInstancesNumber) { //if num inst are already computed when building the tree...
      return of(this.cls.getAdditionalProperty(ResAttribute.NUM_INST));
    } else { //otherwise call a service
      let includeNonDirect = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().instanceListPreferences.includeNonDirect;
      return this.clsService.getNumberOfInstances(cls, includeNonDirect, includeDeprecated, STRequestOptions.getRequestOptions(this.projectCtx));
    }
  }

  public forceList(list: ARTURIResource[]) {
    this.safeToGo = { safe: true }; //prevent the list not showing if a previous standard initialization set the safeToGo to false
    this.setInitialStatus();
    this.nodes = list;
  }

  //EVENT LISTENERS
  onListNodeDeleted(node: ARTURIResource) {
    for (let i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i].equals(node)) {
        if (VBContext.getWorkingProject().isValidationEnabled()) {
          //replace the resource instead of simply change the graphs, so that the rdfResource detect the change
          let stagedRes: ARTURIResource = this.nodes[i].clone();
          stagedRes.setGraphs([new ARTURIResource(SemanticTurkey.stagingRemoveGraph + VBContext.getWorkingProject().getBaseURI())]);
          stagedRes.setAdditionalProperty(ResAttribute.EXPLICIT, false);
          stagedRes.setAdditionalProperty(ResAttribute.SELECTED, false);
          this.nodes[i] = stagedRes;
        } else {
          this.nodes.splice(i, 1);
        }
        break;
      }
    }
  }

  onResourceCreatedUndone(node: ARTResource) {
    for (let i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i].equals(node)) {
        this.nodes.splice(i, 1);
        break;
      }
    }
  }

  onListNodeCreated(node: ARTURIResource) {
    this.nodes.unshift(node);
    if (this.context == TreeListContext.addPropValue) {
      // this.selectNode(node);
      this.nodeSelected.emit(new NodeSelectEvent(node));
    }
  }

  private onTypeRemoved(instance: ARTResource, cls: ARTURIResource) {
    //check of cls not undefined is required if instance list has never been initialized with an @Input class
    if (this.cls && this.cls.equals(cls)) {
      for (let i = 0; i < this.nodes.length; i++) {
        if (this.nodes[i].equals(instance)) {
          this.nodes.splice(i, 1);
          break;
        }
      }
    }
  }

}