import { ChangeDetectorRef, Component, EventEmitter, Input, Output, QueryList, SimpleChanges, ViewChildren } from "@angular/core";
import { Observable, of } from "rxjs";
import { finalize, mergeMap } from 'rxjs/operators';
import { ARTResource, ARTURIResource, RDFResourceRolesEnum, ResAttribute } from "../../../models/ARTResources";
import { LexEntryVisualizationMode, LexicalEntryListPreference, SafeToGo } from "../../../models/Properties";
import { SemanticTurkey } from "../../../models/Vocabulary";
import { OntoLexLemonServices } from "../../../services/ontolex-lemon.service";
import { AuthorizationEvaluator } from "../../../utils/AuthorizationEvaluator";
import { STRequestOptions } from "../../../utils/HttpManager";
import { ResourceUtils, SortAttribute } from "../../../utils/ResourceUtils";
import { TreeListContext } from "../../../utils/UIUtils";
import { VBActionsEnum } from "../../../utils/VBActions";
import { VBContext } from "../../../utils/VBContext";
import { LexEntryDeleteUndoData, VBEventHandler } from "../../../utils/VBEventHandler";
import { NodeSelectEvent } from "../../abstractNode";
import { AbstractList } from "../abstractList";
import { LexicalEntryListNodeComponent } from "./lexical-entry-list-node.component";

@Component({
  selector: "lexical-entry-list",
  templateUrl: "./lexical-entry-list.component.html",
  host: { class: "treeListComponent" },
  standalone: false
})
export class LexicalEntryListComponent extends AbstractList {

  @ViewChildren(LexicalEntryListNodeComponent) viewChildrenNode: QueryList<LexicalEntryListNodeComponent>;

  @Input() index: string; //initial letter of the entries to show
  @Input() lexicon: ARTURIResource;
  @Input() forceVisualizationMode: LexEntryVisualizationMode; //if not null, forces visualization mode regardles of configuration
  @Output() switchMode = new EventEmitter<LexEntryVisualizationMode>(); //requires to the parent panel to switch mode
  @Output() changeIndexLength = new EventEmitter<number>(); //requires to the parent to change the index length

  structRole = RDFResourceRolesEnum.ontolexLexicalEntry;

  visualizationMode: LexEntryVisualizationMode; //this could be changed dynamically, so each time it is used, get it again from preferences

  unsafeIndexOneChar: boolean; //true if in case of safeToGo = false, the current index is 1-char

  translationParam: { count: number, safeToGoLimit: number };

  LexEntryVisualizationMode = LexEntryVisualizationMode;

  constructor(private ontolexService: OntoLexLemonServices, eventHandler: VBEventHandler, changeDetectorRef: ChangeDetectorRef) {
    super(eventHandler, changeDetectorRef);

    this.eventSubscriptions.push(eventHandler.lexicalEntryCreatedEvent.subscribe(
      (data: { entry: ARTURIResource, lexicon: ARTURIResource }) => {
        if (data.lexicon.equals(this.lexicon)) this.onListNodeCreated(data.entry);
      }
    ));
    //here there is no need to check for the index (leading char of the entry) since if the entry uri is not found it is not deleted
    this.eventSubscriptions.push(eventHandler.lexicalEntryDeletedEvent.subscribe(
      (lexEntry: ARTURIResource) => this.onListNodeDeleted(lexEntry)));
    this.eventSubscriptions.push(eventHandler.lexEntryDeletedUndoneEvent.subscribe(
      (data: LexEntryDeleteUndoData) => this.onEntryDeletedUndone(data)
    ));
  }

  ngOnInit() {
    if (!AuthorizationEvaluator.isAuthorized(VBActionsEnum.ontolexGetLexicalEntry)) {
      this.unauthorized = true;
      return;
    }
    this.init();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['index'] && !changes['index'].firstChange || changes['lexicon'] && !changes['lexicon'].firstChange) {
      this.init();
    }
  }

  initImpl() {
    this.visualizationMode = this.forceVisualizationMode ? this.forceVisualizationMode : VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().lexEntryListPreferences.visualization;
    if (this.visualizationMode == LexEntryVisualizationMode.indexBased && this.index != undefined) {
      this.checkInitializationSafe().subscribe(
        () => {
          if (this.safeToGo.safe) {
            this.loading = true;
            this.ontolexService.getLexicalEntriesByAlphabeticIndex(this.index, this.lexicon, this.showDeprecated, STRequestOptions.getRequestOptions(this.projectCtx)).pipe(
              finalize(() => { this.loading = false; })
            ).subscribe(
              entries => {
                //sort by show if rendering is active, uri otherwise
                ResourceUtils.sortResources(entries, this.rendering ? SortAttribute.show : SortAttribute.value);
                this.nodes = entries;
                if (this.pendingSearchRes) {
                  this.openListAt(this.pendingSearchRes);
                }
              }
            );
          }
        }
      );
    } else if (this.visualizationMode == LexEntryVisualizationMode.searchBased) {
      //reset list to empty
      this.forceList(null);
    }
  }

  /**
   * Forces the safeness of the structure even if it was reported as not safe, then re initialize it
   */
  forceSafeness() {
    this.safeToGo = { safe: true };
    let checksum = this.getInitRequestChecksum();
    this.safeToGoMap[checksum] = this.safeToGo;
    this.initImpl();
  }

  /**
   * Perform a check in order to prevent the initialization of the structure with too many elements.
   * Update the safeToGo flag and the cached map
   */
  private checkInitializationSafe(): Observable<void> {
    let lexEntryListPreference: LexicalEntryListPreference = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().lexEntryListPreferences;
    if (this.safeToGoLimit != lexEntryListPreference.safeToGoLimit) {
      this.safeToGoMap = {}; //limit changed, safetiness checks invalidated => reset the map
    }
    this.safeToGoLimit = lexEntryListPreference.safeToGoLimit;
    this.unsafeIndexOneChar = lexEntryListPreference.indexLength == 1;

    let checksum = this.getInitRequestChecksum();

    let safeness: SafeToGo = this.safeToGoMap[checksum];
    if (safeness != null) { //found safeness in cache
      this.safeToGo = safeness;
      this.translationParam = { count: this.safeToGo.count, safeToGoLimit: this.safeToGoLimit };
      return of(null);
    } else { //never initialized/cahced => count
      this.loading = true;
      return this.ontolexService.countLexicalEntriesByAlphabeticIndex(this.index, this.lexicon, this.showDeprecated, STRequestOptions.getRequestOptions(this.projectCtx)).pipe(
        finalize(() => { this.loading = false; }),
        mergeMap(count => {
          safeness = { safe: count < this.safeToGoLimit, count: count };
          this.safeToGoMap[checksum] = safeness; //cache the safeness
          this.safeToGo = safeness;
          this.translationParam = { count: this.safeToGo.count, safeToGoLimit: this.safeToGoLimit };
          return of(null);
        })
      );
    }
  }

  private getInitRequestChecksum() {
    let checksum = "lexicon:" + ((this.lexicon != null) ? this.lexicon.toNT() : null) +
      "&index:" + this.index +
      "&showDeprecated:" + this.showDeprecated;
    return checksum;
  }

  public forceList(list: ARTURIResource[]) {
    this.safeToGo = { safe: true }; //prevent the list not showing if a previous index-based initialization set the safeToGo to false
    this.setInitialStatus();
    this.nodes = list;
  }

  onListNodeCreated(node: ARTURIResource) {
    if (VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().lexEntryListPreferences.visualization == LexEntryVisualizationMode.indexBased) {
      if (node.getShow().toLocaleLowerCase().startsWith(this.index.toLocaleLowerCase())) {
        this.nodes.unshift(node);
        if (this.context == TreeListContext.addPropValue) {
          this.nodeSelected.emit(new NodeSelectEvent(node));
        }
      }
    }
  }

  onListNodeDeleted(node: ARTURIResource) {
    for (let i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i].equals(node)) {
        if (VBContext.getWorkingProject().isValidationEnabled()) {
          //replace the resource instead of simply change the graphs, so that the rdfResource detect the change
          let stagedRes: ARTURIResource = this.nodes[i].clone() as ARTURIResource;
          stagedRes.setGraphs([new ARTURIResource(SemanticTurkey.stagingRemoveGraph + VBContext.getWorkingProject().getBaseURI())]);
          stagedRes.setAdditionalProperty(ResAttribute.EXPLICIT, false);
          stagedRes.setAdditionalProperty(ResAttribute.SELECTED, false);
          this.nodes[i] = stagedRes;
        } else {
          this.nodes.splice(i, 1);
        }
        break;
      }
    }
  }

  onResourceCreatedUndone(node: ARTResource) {
    for (let i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i].equals(node)) {
        this.nodes.splice(i, 1);
        break;
      }
    }
  }

  onEntryDeletedUndone(data: { resource: ARTURIResource, lexicons: ARTURIResource[] }) {
    if (data.lexicons.some(l => l.equals(this.lexicon))) {
      this.ontolexService.getLexicalEntryIndex(data.resource).subscribe(
        index => {
          let toAdd: boolean;
          if (this.index.length == 1) {
            toAdd = this.index.toLocaleUpperCase() == index[0].toLocaleUpperCase();
          } else if (this.index.length == 2) {
            toAdd = index.toLocaleUpperCase() == this.index.toLocaleUpperCase();
          }
          if (toAdd) {
            this.nodes.push(data.resource);
          }
        }
      );
    }
  }

}