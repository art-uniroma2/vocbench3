import { ChangeDetectorRef, Component, QueryList, ViewChildren } from "@angular/core";
import { finalize } from "rxjs";
import { ARTResource, ARTURIResource, RDFResourceRolesEnum, ResAttribute } from "../../../models/ARTResources";
import { Project } from "../../../models/Project";
import { SemanticTurkey } from "../../../models/Vocabulary";
import { OntoLexLemonServices } from "../../../services/ontolex-lemon.service";
import { AuthorizationEvaluator } from "../../../utils/AuthorizationEvaluator";
import { STRequestOptions } from "../../../utils/HttpManager";
import { ResourceUtils, SortAttribute } from "../../../utils/ResourceUtils";
import { TreeListContext } from "../../../utils/UIUtils";
import { VBActionsEnum } from "../../../utils/VBActions";
import { VBContext } from "../../../utils/VBContext";
import { VBEventHandler } from "../../../utils/VBEventHandler";
import { VBProperties } from "../../../utils/VBProperties";
import { NodeSelectEvent } from "../../abstractNode";
import { AbstractList } from "../abstractList";
import { LexiconListNodeComponent } from "./lexicon-list-node.component";

@Component({
  selector: "lexicon-list",
  templateUrl: "./lexicon-list.component.html",
  host: { class: "treeListComponent" },
  standalone: false
})
export class LexiconListComponent extends AbstractList {

  @ViewChildren(LexiconListNodeComponent) viewChildrenNode: QueryList<LexiconListNodeComponent>;

  structRole = RDFResourceRolesEnum.limeLexicon;

  list: ARTURIResource[];

  activeLexicon: ARTURIResource;

  constructor(private ontolexService: OntoLexLemonServices, private vbProp: VBProperties, eventHandler: VBEventHandler, changeDetectorRef: ChangeDetectorRef) {
    super(eventHandler, changeDetectorRef);
    this.eventSubscriptions.push(eventHandler.lexiconCreatedEvent.subscribe((node: ARTURIResource) => this.onListNodeCreated(node)));
    this.eventSubscriptions.push(eventHandler.lexiconDeletedEvent.subscribe((node: ARTURIResource) => this.onListNodeDeleted(node)));
    this.eventSubscriptions.push(eventHandler.lexiconDeletedUndoneEvent.subscribe((node: ARTURIResource) => this.onDeletedUndo(node)));
    //handler when active lexicon is changed programmatically when a searched entry belong to a non active lexicon
    this.eventSubscriptions.push(eventHandler.lexiconChangedEvent.subscribe(
      (data: { lexicon: ARTURIResource, project: Project }) => {
        if (data.project.getName() == VBContext.getWorkingProjectCtx(this.projectCtx).getProject().getName()) {
          if (data.lexicon != null) {
            this.activeLexicon = this.nodes.find(n => n.equals(data.lexicon)) as ARTURIResource;
          } else {
            this.activeLexicon = null;
          }
        }
      })
    );
  }

  ngOnInit() {
    if (!AuthorizationEvaluator.isAuthorized(VBActionsEnum.ontolexGetLexicon)) {
      this.unauthorized = true;
      return;
    }
    this.init();
  }

  initImpl() {
    this.loading = true;
    this.ontolexService.getLexicons(this.showDeprecated, STRequestOptions.getRequestOptions(this.projectCtx)).pipe(
      finalize(() => { this.loading = false; })
    ).subscribe(
      lexicons => {
        //sort by show if rendering is active, uri otherwise
        ResourceUtils.sortResources(lexicons, this.rendering ? SortAttribute.show : SortAttribute.value);
        this.nodes = lexicons;

        let activeLexicon = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().activeLexicon;
        if (activeLexicon != null) {
          this.activeLexicon = this.nodes.find(l => l.equals(activeLexicon)) as ARTURIResource;
        } else if (activeLexicon == null && this.nodes.length == 1) { //if no lexicon is set and there is only one lexicon, automatically activate it
          this.toggleLexicon(this.nodes[0] as ARTURIResource);
        }
      }
    );
  }

  onListNodeCreated(node: ARTURIResource) {
    this.nodes.unshift(node);
    if (this.context == TreeListContext.addPropValue) {
      this.nodeSelected.emit(new NodeSelectEvent(node));
    }
    if (this.context == TreeListContext.dataPanel && this.nodes.length == 1) {
      this.toggleLexicon(this.nodes[0] as ARTURIResource);
    }
  }

  onListNodeDeleted(node: ARTURIResource) {
    for (let i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i].equals(node)) {
        if (VBContext.getWorkingProject().isValidationEnabled()) {
          //replace the resource instead of simply change the graphs, so that the rdfResource detect the change
          let stagedRes: ARTURIResource = this.nodes[i].clone() as ARTURIResource;
          stagedRes.setGraphs([new ARTURIResource(SemanticTurkey.stagingRemoveGraph + VBContext.getWorkingProject().getBaseURI())]);
          stagedRes.setAdditionalProperty(ResAttribute.EXPLICIT, false);
          stagedRes.setAdditionalProperty(ResAttribute.SELECTED, false);
          this.nodes[i] = stagedRes;
        } else {
          this.nodes.splice(i, 1);
        }
        break;
      }
    }
  }

  onResourceCreatedUndone(node: ARTResource) {
    for (let i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i].equals(node)) {
        this.nodes.splice(i, 1);
        break;
      }
    }
  }

  onDeletedUndo(node: ARTURIResource) {
    this.nodes.push(node);
  }

  toggleLexicon(lexicon: ARTURIResource) {
    if (this.activeLexicon == lexicon) {
      this.activeLexicon = null;
    } else {
      this.activeLexicon = lexicon;
    }
    this.vbProp.setActiveLexicon(VBContext.getWorkingProjectCtx(this.projectCtx), this.activeLexicon).subscribe();
  }

}