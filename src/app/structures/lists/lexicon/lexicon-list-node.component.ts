import { Component } from "@angular/core";
import { AbstractListNode } from "../abstractListNode";
import { VBEventHandler } from "../../../utils/VBEventHandler";

@Component({
    selector: "lexicon-list-node",
    templateUrl: "./lexicon-list-node.component.html",
    standalone: false
})
export class LexiconListNodeComponent extends AbstractListNode {

    constructor(eventHandler: VBEventHandler) {
        super(eventHandler);
    }

}