import { ChangeDetectorRef, Component, Input, QueryList, ViewChildren } from "@angular/core";
import { finalize, Observable } from "rxjs";
import { ARTResource, ARTURIResource, RDFResourceRolesEnum, ResAttribute } from "../../../models/ARTResources";
import { SemanticTurkey } from "../../../models/Vocabulary";
import { DatatypesServices } from "../../../services/datatypes.service";
import { AuthorizationEvaluator } from "../../../utils/AuthorizationEvaluator";
import { STRequestOptions } from "../../../utils/HttpManager";
import { ResourceUtils, SortAttribute } from "../../../utils/ResourceUtils";
import { TreeListContext } from "../../../utils/UIUtils";
import { VBActionsEnum } from "../../../utils/VBActions";
import { VBContext } from "../../../utils/VBContext";
import { VBEventHandler } from "../../../utils/VBEventHandler";
import { AbstractList } from "../abstractList";
import { DatatypeListNodeComponent } from "./datatype-list-node.component";
import { NodeSelectEvent } from "../../abstractNode";

@Component({
  selector: "datatype-list",
  templateUrl: "./datatype-list.component.html",
  host: { class: "treeListComponent" },
  standalone: false
})
export class DatatypeListComponent extends AbstractList {

  @Input() full: boolean = false; //if true show all the datatypes (also the owl2 that are not declared as rdfs:Datatype)

  @ViewChildren(DatatypeListNodeComponent) viewChildrenNode: QueryList<DatatypeListNodeComponent>;

  structRole = RDFResourceRolesEnum.dataRange;

  constructor(private datatypeService: DatatypesServices, eventHandler: VBEventHandler, changeDetectorRef: ChangeDetectorRef) {
    super(eventHandler, changeDetectorRef);
    this.eventSubscriptions.push(eventHandler.datatypeCreatedEvent.subscribe((node: ARTURIResource) => this.onListNodeCreated(node)));
    this.eventSubscriptions.push(eventHandler.datatypeDeletedEvent.subscribe((node: ARTURIResource) => this.onListNodeDeleted(node)));
    this.eventSubscriptions.push(eventHandler.datatypeDeletedUndoneEvent.subscribe((node: ARTURIResource) => this.onDeletedUndo(node)));
  }

  ngOnInit() {
    if (!AuthorizationEvaluator.isAuthorized(VBActionsEnum.datatypesGetDatatype)) {
      this.unauthorized = true;
      return;
    }
    this.init();
  }

  initImpl() {
    let initFn: Observable<ARTURIResource[]>;
    if (this.full) {
      initFn = this.datatypeService.getDatatypes(STRequestOptions.getRequestOptions(this.projectCtx));
    } else {
      initFn = this.datatypeService.getDeclaredDatatypes(this.showDeprecated, STRequestOptions.getRequestOptions(this.projectCtx));
    }

    this.loading = true;
    initFn.pipe(
      finalize(() => { this.loading = false; })
    ).subscribe(
      datatypes => {
        //sort by show if rendering is active, uri otherwise
        ResourceUtils.sortResources(datatypes, this.rendering ? SortAttribute.show : SortAttribute.value);
        this.nodes = datatypes;
      }
    );
  }

  onListNodeCreated(node: ARTURIResource) {
    this.nodes.unshift(node);
    if (this.context == TreeListContext.addPropValue) {
      // this.selectNode(node);
      this.nodeSelected.emit(new NodeSelectEvent(node));
    }
  }

  onListNodeDeleted(node: ARTURIResource) {
    for (let i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i].equals(node)) {
        if (VBContext.getWorkingProject().isValidationEnabled()) {
          //replace the resource instead of simply change the graphs, so that the rdfResource detect the change
          let stagedRes: ARTURIResource = this.nodes[i].clone() as ARTURIResource;
          stagedRes.setGraphs([new ARTURIResource(SemanticTurkey.stagingRemoveGraph + VBContext.getWorkingProject().getBaseURI())]);
          stagedRes.setAdditionalProperty(ResAttribute.EXPLICIT, false);
          stagedRes.setAdditionalProperty(ResAttribute.SELECTED, false);
          this.nodes[i] = stagedRes;
        } else {
          this.nodes.splice(i, 1);
        }
        break;
      }
    }
  }

  onResourceCreatedUndone(node: ARTResource) {
    for (let i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i].equals(node)) {
        this.nodes.splice(i, 1);
        break;
      }
    }
  }

  onDeletedUndo(node: ARTURIResource) {
    this.nodes.push(node);
  }

}