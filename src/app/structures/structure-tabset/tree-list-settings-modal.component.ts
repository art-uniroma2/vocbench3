import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { concat, from, Observable, of } from 'rxjs';
import { toArray } from 'rxjs/operators';
import { DataPanel } from 'src/app/models/DataStructure';
import { ExtensionPointID, Scope } from 'src/app/models/Plugins';
import { CustomTreeRootSelection, CustomTreeSettings, SettingsEnum } from 'src/app/models/Properties';
import { SettingsServices } from 'src/app/services/settings.service';
import { STRequestOptions } from 'src/app/utils/HttpManager';
import { ProjectContext, VBContext } from 'src/app/utils/VBContext';
import { BasicModalServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { VBProperties } from "../../utils/VBProperties";

@Component({
  selector: "tree-list-settings-modal",
  templateUrl: "./tree-list-settings-modal.component.html",
  standalone: false
})
export class TreeListSettingsModalComponent {

  @Input() projectCtx: ProjectContext;

  isEdoal: boolean;

  showDeprecated: boolean;

  customTreeSettings: CustomTreeSettings;

  hiddenDataPanels: DataPanel[];

  constructor(public activeModal: NgbActiveModal, private basicModals: BasicModalServices, private settingsService: SettingsServices, private vbProp: VBProperties) { }

  ngOnInit() {
    this.showDeprecated = this.vbProp.getShowDeprecated();

    this.customTreeSettings = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().customTreeSettings;

    this.hiddenDataPanels = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().hiddenDataPanels;
  }

  onSettingsReset() {
    let requestsOpt = new STRequestOptions({ ctxProject: VBContext.getWorkingProjectCtx(this.projectCtx).getProject() });
    this.basicModals.confirm({ key: "STATUS.WARNING" }, { key: "MESSAGES.CONFIG_RESTORE_PROJECT_DEFAULT_CONFIRM" }, ModalType.warning).then(
      () => {
        let settings: CustomTreeSettings = null;
        this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.customTree, settings, requestsOpt).subscribe(
          () => {
            //reinitialize so exploit the fallback to default (if any) mechanism
            this.settingsService.getSettings(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, requestsOpt).subscribe(
              settings => {
                this.customTreeSettings = settings.getPropertyValue(SettingsEnum.customTree);
                VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().customTreeSettings = settings.getPropertyValue(SettingsEnum.customTree);
                this.basicModals.alert({ key: "STATUS.OPERATION_DONE" }, { key: "MESSAGES.CONFIG_PROJECT_DEFAULT_RESTORED" });
              }
            );
          }
        );
      },
      () => { }
    );
  }

  ok() {
    this.isCustomTreeSettingsValid().subscribe(
      valid => {
        if (valid) {
          //both settings are valid => submit changes
          this.submitChanges();
        }
      }
    );
  }


  private isCustomTreeSettingsValid(): Observable<boolean> {
    if (this.customTreeSettings.enabled) {
      //check if CTree enabled but incomplete (hierarchical prop not provided, or root selection set to enumeration but no roots provided)
      if (
        (this.customTreeSettings.hierarchicalProperty == null) ||
        (this.customTreeSettings.rootSelection == CustomTreeRootSelection.enumeration && (!this.customTreeSettings.roots || this.customTreeSettings.roots.length == 0))
      ) {
        this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "DATA.CUSTOM_TREE.MESSAGES.INCOMPLETE_CONFIGURATION" }, ModalType.warning);
        return of(false);
      }
      //check if resource type is not provided (default to rdfs:Resource) and root selection set to all
      if (this.customTreeSettings.type == null && this.customTreeSettings.rootSelection == CustomTreeRootSelection.all) {
        return from(
          this.basicModals.confirm({ key: "STATUS.WARNING" }, { key: "DATA.CUSTOM_TREE.MESSAGES.NO_TYPE_ALL_ROOTS_WARN" }, ModalType.warning).then(
            () => {
              return true;
            },
            () => {
              return false;
            }
          )
        );
      } else {
        return of(true);
      }
    } else {
      return of(true);
    }
  }

  private submitChanges() {
    let updateSettingsFn: Observable<any>[] = [];
    updateSettingsFn.push(this.vbProp.setHiddenDataPanels(this.projectCtx, this.hiddenDataPanels));
    updateSettingsFn.push(this.vbProp.setCustomTreeSettings(this.projectCtx, this.customTreeSettings));
    concat(...updateSettingsFn).pipe(
      toArray()
    ).subscribe(
      () => {
        this.vbProp.setShowDeprecated(this.showDeprecated);
        this.activeModal.close();
      }
    );
  }

  cancel() {
    this.activeModal.dismiss();
  }

}