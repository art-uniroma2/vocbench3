import { ChangeDetectorRef, Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from "@ngx-translate/core";
import { DataPanel, DataPanelLabelSetting, DataStructureUtils } from 'src/app/models/DataStructure';
import { CustomTreeSettings } from 'src/app/models/Properties';
import { ResourceUtils } from "src/app/utils/ResourceUtils";
import { TranslationUtils } from "src/app/utils/TranslationUtils";
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { ARTResource, ARTURIResource, RDFResourceRolesEnum } from "../../models/ARTResources";
import { AuthorizationEvaluator } from "../../utils/AuthorizationEvaluator";
import { TreeListContext } from "../../utils/UIUtils";
import { VBActionsEnum } from "../../utils/VBActions";
import { ProjectContext, VBContext } from "../../utils/VBContext";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { SharedModalServices } from "../../modal-dialogs/shared-modals/shared-modals.service";
import { DatatypeListPanelComponent } from "../lists/datatype/datatype-list-panel.component";
import { LexicalEntryListPanelComponent } from "../lists/lexical-entry/lexical-entry-list-panel.component";
import { LexiconListPanelComponent } from "../lists/lexicon/lexicon-list-panel.component";
import { SchemeListPanelComponent } from "../lists/scheme/scheme-list-panel.component";
import { ClassIndividualTreePanelComponent } from "../trees/class/class-individual-tree-panel.component";
import { CollectionTreePanelComponent } from "../trees/collection/collection-tree-panel.component";
import { ConceptTreePanelComponent } from "../trees/concept/concept-tree-panel.component";
import { PropertyTreePanelComponent } from "../trees/property/property-tree-panel.component";
import { TreeListSettingsModalComponent } from "./tree-list-settings-modal.component";
import { NodeSelectEvent } from "../abstractNode";

@Component({
  selector: "structure-tabset",
  templateUrl: "./structure-tabset.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class TabsetPanelComponent {

  @Input() projectCtx: ProjectContext;
  @Input() readonly: boolean = false;
  @Input() editable: boolean = true;
  @Output() nodeSelected = new EventEmitter<NodeSelectEvent|null>();

  @ViewChild(ConceptTreePanelComponent) viewChildConceptPanel: ConceptTreePanelComponent;
  @ViewChild(CollectionTreePanelComponent) viewChildCollectionPanel: CollectionTreePanelComponent;
  @ViewChild(SchemeListPanelComponent) viewChildSchemePanel: SchemeListPanelComponent;
  @ViewChild(PropertyTreePanelComponent) viewChildPropertyPanel: PropertyTreePanelComponent;
  @ViewChild(ClassIndividualTreePanelComponent) viewChildClsIndPanel: ClassIndividualTreePanelComponent;
  @ViewChild(LexiconListPanelComponent) viewChildLexiconPanel: LexiconListPanelComponent;
  @ViewChild(LexicalEntryListPanelComponent) viewChildLexialEntryPanel: LexicalEntryListPanelComponent;
  @ViewChild(DatatypeListPanelComponent) viewChildDatatypePanel: DatatypeListPanelComponent;

  DataPanel = DataPanel; //workaround for using enum in template

  context: TreeListContext = TreeListContext.dataPanel;

  selectedResource: ARTResource;

  availablePanels: DataPanel[];
  tabs: { panel: DataPanel, label: string }[];
  activeTab: DataPanel;
  allowMultiselection: boolean = true;

  constructor(private modalService: NgbModal, private basicModals: BasicModalServices, private sharedModals: SharedModalServices,
    private translate: TranslateService, private changeDetectorRef: ChangeDetectorRef
  ) {
    this.translate.onLangChange.subscribe(() => this.initTabsLabels());
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.initTabs();

    //init active tab
    if (this.activeTab != null) {
      if (!this.tabs.some(tab => tab.panel == this.activeTab)) {
        this.activeTab = null;
      }
    }
    if (this.activeTab == null) {
      let model = VBContext.getWorkingProjectCtx(this.projectCtx).getProject().getModelType();
      let tabsPriority: DataPanel[] = DataStructureUtils.panelsPriority[model];
      for (let t of tabsPriority) {
        if (this.tabs.some(tab => tab.panel == t)) {
          this.activeTab = t;
          break;
        }
      }
      if (this.activeTab == null && this.tabs.length > 0) {
        this.activeTab = this.tabs[0].panel;
      }
    }

    if (this.readonly == false) { //if readonly is false (or not provided as @Input)
      this.readonly = VBContext.getContextVersion() != null; //if it is working on an old dump version, disable the updates
    }
    if (this.readonly || !this.editable) {
      this.allowMultiselection = false;
    }
  }

  onNodeSelected(event?: NodeSelectEvent) {
    this.nodeSelected.emit(event);
  }

  syncResource(resource: ARTResource, allowTabChange?: boolean) {
    if (resource instanceof ARTURIResource) { //in the trees/lists are visible only IRI resources, so allow to sync only ARTURIResource
      let role: RDFResourceRolesEnum = resource.getRole();

      let tabToActivate: DataPanel;
      if (ResourceUtils.roleSubsumes(RDFResourceRolesEnum.property, role)) {
        tabToActivate = DataPanel.property;
      } else if (ResourceUtils.roleSubsumes(RDFResourceRolesEnum.skosCollection, role)) {
        tabToActivate = DataPanel.skosCollection;
      } else if (role == RDFResourceRolesEnum.individual) {
        tabToActivate = DataPanel.cls;
      } else if (
        role == RDFResourceRolesEnum.cls || role == RDFResourceRolesEnum.concept || role == RDFResourceRolesEnum.conceptScheme ||
        role == RDFResourceRolesEnum.limeLexicon || role == RDFResourceRolesEnum.ontolexLexicalEntry || role == RDFResourceRolesEnum.dataRange
      ) {
        tabToActivate = role as unknown as DataPanel; //here I can force cast since the roles in the if are overlapped with DataPanel
      }

      if (allowTabChange && this.activeTab != tabToActivate && this.isTabVisible(tabToActivate)) { //if the tab needs to be changed and the target tab is visible
        this.activeTab = tabToActivate;
      }
      //sync the resource in the tree/list only if the resource has the same role of the tree/list currently active
      if (this.activeTab == tabToActivate) {
        if (role == RDFResourceRolesEnum.concept) {
          this.viewChildConceptPanel.openTreeAt(resource);
        } else if (role == RDFResourceRolesEnum.conceptScheme) {
          this.viewChildSchemePanel.openAt(resource);
        } else if (role == RDFResourceRolesEnum.skosCollection || role == RDFResourceRolesEnum.skosOrderedCollection) {
          this.viewChildCollectionPanel.openTreeAt(resource);
        } else if (role == RDFResourceRolesEnum.property || role == RDFResourceRolesEnum.annotationProperty ||
          role == RDFResourceRolesEnum.datatypeProperty || role == RDFResourceRolesEnum.objectProperty ||
          role == RDFResourceRolesEnum.ontologyProperty) {
          this.viewChildPropertyPanel.openTreeAt(resource);
        } else if (role == RDFResourceRolesEnum.cls) {
          this.viewChildClsIndPanel.openClassTreeAt(resource);
        } else if (role == RDFResourceRolesEnum.limeLexicon) {
          this.viewChildLexiconPanel.openAt(resource);
        } else if (role == RDFResourceRolesEnum.ontolexLexicalEntry) {
          this.viewChildLexialEntryPanel.openAt(resource);
        } else if (role == RDFResourceRolesEnum.dataRange) {
          this.viewChildDatatypePanel.openAt(resource);
        }
      }
    }
  }

  //TAB HANDLER

  private initTabs() {
    //init all data panels
    let allPanels: DataPanel[] = [DataPanel.cls, DataPanel.concept, DataPanel.conceptScheme, DataPanel.skosCollection,
    DataPanel.property, DataPanel.limeLexicon, DataPanel.ontolexLexicalEntry, DataPanel.dataRange];

    let model = VBContext.getWorkingProjectCtx(this.projectCtx).getProject().getModelType();
    let panelFilter: DataPanel[] = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().hiddenDataPanels;

    /*
    Filter panels according:
    - model compatibility
    - filter setting
    - authorization
     */
    this.availablePanels = allPanels.filter(p => {
      //check if tab is compliant with model
      let modelCompliant = DataStructureUtils.modelPanelsMap[model].includes(p);
      //check if tab is not hidden by the setting
      let notHidden = !panelFilter.includes(p);
      //check authorization
      let authorized = this.isTabAuthorized(p);
      return modelCompliant && notHidden && authorized;
    });

    //if a custom tree is configured, add the Custom tab
    let ctSettings: CustomTreeSettings = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().customTreeSettings;
    if (ctSettings.enabled) {
      this.availablePanels.push(DataPanel.custom);
    }

    this.tabs = this.availablePanels.map(t => {
      return { panel: t, label: "" };
    });

    this.initTabsLabels();
  }

  private initTabsLabels() {
    let currentLang = this.translate.currentLang;
    let panelLabelsMap: DataPanelLabelSetting = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectSettings().dataPanelLabelMappings;
    this.tabs.forEach(t => {
      if (panelLabelsMap?.[currentLang]?.[t.panel] != null) {
        t.label = panelLabelsMap[currentLang][t.panel];
      } else {
        t.label = this.translate.instant(TranslationUtils.dataPanelTranslationMap[t.panel]);
      }
    });
  }

  private isTabAuthorized(tab: DataPanel): boolean {
    if (tab == DataPanel.cls) {
      return AuthorizationEvaluator.isAuthorized(VBActionsEnum.classesGetClassTaxonomy);
    } else if (tab == DataPanel.concept) {
      return AuthorizationEvaluator.isAuthorized(VBActionsEnum.skosGetConceptTaxonomy);
    } else if (tab == DataPanel.conceptScheme) {
      return AuthorizationEvaluator.isAuthorized(VBActionsEnum.skosGetSchemes);
    } else if (tab == DataPanel.skosCollection) {
      return AuthorizationEvaluator.isAuthorized(VBActionsEnum.skosGetCollectionTaxonomy);
    } else if (tab == DataPanel.property) {
      return AuthorizationEvaluator.isAuthorized(VBActionsEnum.propertiesGetPropertyTaxonomy);
    } else if (tab == DataPanel.limeLexicon) {
      return AuthorizationEvaluator.isAuthorized(VBActionsEnum.ontolexGetLexicon);
    } else if (tab == DataPanel.ontolexLexicalEntry) {
      return AuthorizationEvaluator.isAuthorized(VBActionsEnum.ontolexGetLexicalEntry);
    } else if (tab == DataPanel.dataRange) {
      return AuthorizationEvaluator.isAuthorized(VBActionsEnum.datatypesGetDatatype);
    } else if (tab == DataPanel.custom) {
      return AuthorizationEvaluator.isAuthorized(VBActionsEnum.customTreeGetTaxonomy);
    } else {
      return false;
    }
  }

  isTabVisible(tab: DataPanel): boolean {
    return this.availablePanels.includes(tab);
  }

  selectTab(tabName: DataPanel) {
    this.activeTab = tabName;
  }

  //Focus the panel and select the searched resource after an advanced search
  onAdvancedSearch(resource: ARTResource) {
    if (resource instanceof ARTURIResource) {
      let role = resource.getRole();

      let tabToActivate: DataPanel;
      if (ResourceUtils.roleSubsumes(RDFResourceRolesEnum.property, role)) {
        tabToActivate = DataPanel.property;
      } else if (ResourceUtils.roleSubsumes(RDFResourceRolesEnum.skosCollection, role)) {
        tabToActivate = DataPanel.skosCollection;
      } else if (role == RDFResourceRolesEnum.individual) {
        tabToActivate = DataPanel.cls;
      } else if (
        role == RDFResourceRolesEnum.cls || role == RDFResourceRolesEnum.concept || role == RDFResourceRolesEnum.conceptScheme ||
        role == RDFResourceRolesEnum.limeLexicon || role == RDFResourceRolesEnum.ontolexLexicalEntry || role == RDFResourceRolesEnum.dataRange
      ) {
        tabToActivate = role as unknown as DataPanel; //here I can force cast since the roles in the if are overlapped with DataPanel
      } else {
        this.sharedModals.openResourceView(resource, false);
      }

      if (tabToActivate != null) {
        if (this.isTabVisible(tabToActivate)) { //if the tab is visible => activate the tab and select the resource in list/tree
          this.activeTab = tabToActivate;
          //wait the update of the UI after the change of the tab
          this.changeDetectorRef.detectChanges();
          if (tabToActivate == DataPanel.property) {
            this.viewChildPropertyPanel.openTreeAt(resource);
          } else if (tabToActivate == DataPanel.cls) {
            this.viewChildClsIndPanel.selectSearchedResource(resource);
          } else if (tabToActivate == DataPanel.concept) {
            this.viewChildConceptPanel.selectSearchedResource(resource);
          } else if (tabToActivate == DataPanel.conceptScheme) {
            this.viewChildSchemePanel.openAt(resource);
          } else if (tabToActivate == DataPanel.limeLexicon) {
            this.viewChildLexiconPanel.openAt(resource);
          } else if (tabToActivate == DataPanel.ontolexLexicalEntry) {
            this.viewChildLexialEntryPanel.selectAdvancedSearchedResource(resource);
          } else if (tabToActivate == DataPanel.skosCollection) {
            this.viewChildCollectionPanel.openTreeAt(resource);
          } else if (tabToActivate == DataPanel.dataRange) {
            this.viewChildDatatypePanel.openAt(resource);
          }
        } else { //if not visible, open resource in modal
          this.basicModals.alert({ key: "SEARCH.SEARCH" }, { key: "MESSAGES.RESOURCE_NOT_FOCUSABLE_RES_VIEW_MODAL", params: { resource: resource.getShow() } }, ModalType.warning).then(
            () => {
              this.sharedModals.openResourceView(resource, false);
            }
          );
        }
      }
    } else { //BNode are not in trees or lists => open in modal
      this.basicModals.alert({ key: "SEARCH.SEARCH" }, { key: "MESSAGES.RESOURCE_NOT_FOCUSABLE_RES_VIEW_MODAL", params: { resource: resource.getShow() } }, ModalType.warning).then(
        () => {
          this.sharedModals.openResourceView(resource, false);
        }
      );
    }
  }

  openSettings() {
    const modalRef: NgbModalRef = this.modalService.open(TreeListSettingsModalComponent, new ModalOptions());
    modalRef.componentInstance.projectCtx = VBContext.getWorkingProjectCtx(this.projectCtx);
    modalRef.result.then(
      () => {
        this.init();
      },
      () => { }
    );
  }

}