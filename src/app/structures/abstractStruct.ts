import { Directive, ElementRef, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { Subscription } from "rxjs";
import { ARTResource, ARTURIResource, RDFResourceRolesEnum, ResAttribute } from "../models/ARTResources";
import { SafeToGo, SafeToGoMap } from "../models/Properties";
import { ResourceUtils } from "../utils/ResourceUtils";
import { TreeListContext } from "../utils/UIUtils";
import { ProjectContext } from "../utils/VBContext";
import { VBEventHandler } from "../utils/VBEventHandler";
import { NodeSelectEvent } from "./abstractNode";

@Directive()
export abstract class AbstractStruct {

  @Input() rendering: boolean = true; //if true the nodes in the list should be rendered with the show, with the qname otherwise
  @Input() multiselection: boolean = false; //if true enabled the selection of multiple resources via checkboxes
  @Input() showDeprecated: boolean = true;
  @Input() context: TreeListContext;
  @Input() projectCtx: ProjectContext;
  @Output() nodeSelected = new EventEmitter<NodeSelectEvent | null>();
  @Output() nodeChecked = new EventEmitter<ARTURIResource[]>();

  @ViewChild('scrollableContainer') scrollableElement: ElementRef;

  /**
   * ATTRIBUTES
   */

  abstract structRole: RDFResourceRolesEnum; //declare the type of resources in the panel

  unauthorized: boolean = false; //true if user is not authorized to access the tree/list

  nodes: ARTURIResource[];

  eventSubscriptions: Subscription[] = [];
  selectedNode: ARTURIResource;
  checkedNodes: ARTURIResource[] = [];

  //safe to go (used by concept tree, instance list, lex-entry list)
  safeToGoLimit: number;
  safeToGo: SafeToGo = { safe: true };
  safeToGoMap: SafeToGoMap = {};

  loading: boolean;

  /**
   * CONSTRUCTOR
   */
  protected eventHandler: VBEventHandler;
  constructor(eventHandler: VBEventHandler) {
    this.eventHandler = eventHandler;
    this.eventSubscriptions.push(eventHandler.refreshDataBroadcastEvent.subscribe(() => this.init()));
    this.eventSubscriptions.push(eventHandler.refreshTreeListEvent.subscribe((roles: RDFResourceRolesEnum[]) => {
      if (roles.indexOf(this.structRole) != -1) this.init();
    }));
    this.eventSubscriptions.push(eventHandler.resourceUpdatedEvent.subscribe(
      (res: ARTResource) => {
        if (res instanceof ARTURIResource && res.equals(this.selectedNode)) {
          this.selectedNode = res;
          this.selectedNode.setAdditionalProperty(ResAttribute.SELECTED, true); //restore the selected attr
        }
      }
    ));
  }

  /**
   * METHODS
   */

  ngOnDestroy() {
    this.eventSubscriptions.forEach(s => s.unsubscribe());
  }

  abstract init(): void;

  setInitialStatus() {
    this.nodes = [];
    this.selectedNode = null;
    this.nodeSelected.emit(null);
    this.checkedNodes = [];
    this.nodeChecked.emit(this.checkedNodes);
    this.nodeLimit = this.initialNodes;
    this.unauthorized = false;
  }

  onNodeSelected(event?: NodeSelectEvent) {
    if (this.selectedNode != undefined) {
      this.selectedNode.deleteAdditionalProperty(ResAttribute.SELECTED);
    }
    this.selectedNode = event?.value.asURIResource();
    this.selectedNode.setAdditionalProperty(ResAttribute.SELECTED, true);
    this.nodeSelected.emit(event);
  }


  onNodeChecked(event: { node: ARTURIResource, checked: boolean }) {
    if (event.checked) {
      this.checkedNodes.push(event.node);
    } else {
      let nodeIdx: number = ResourceUtils.indexOfNode(this.checkedNodes, event.node);
      if (nodeIdx != -1) {
        this.checkedNodes.splice(nodeIdx, 1);
      }
    }
    this.nodeChecked.emit(this.checkedNodes);
  }

  //Root limitation management
  initialNodes: number = 150;
  nodeLimit: number = this.initialNodes;
  increaseRate: number = this.initialNodes / 5;
  onScroll() {
    let scrollElement: HTMLElement = this.scrollableElement.nativeElement;
    // if (scrollElement.scrollTop === (scrollElement.scrollHeight - scrollElement.offsetHeight)) {
    //consider a little buffer of 2px in order to prevent potential problems (see https://stackoverflow.com/a/32283147/5805661)
    if (Math.abs(scrollElement.scrollHeight - scrollElement.offsetHeight - scrollElement.scrollTop) < 2) {
      //bottom reached => increase max range if there are more roots to show
      if (this.nodeLimit < this.nodes.length) {
        this.nodeLimit += this.increaseRate;
      }
    }
  }

}