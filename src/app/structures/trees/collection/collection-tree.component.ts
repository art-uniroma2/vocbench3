import { ChangeDetectorRef, Component, QueryList, ViewChildren } from "@angular/core";
import { finalize } from "rxjs";
import { ARTURIResource, RDFResourceRolesEnum } from "../../../models/ARTResources";
import { SearchServices } from "../../../services/search.service";
import { SkosServices } from "../../../services/skos.service";
import { AuthorizationEvaluator } from "../../../utils/AuthorizationEvaluator";
import { STRequestOptions } from "../../../utils/HttpManager";
import { ResourceUtils, SortAttribute } from "../../../utils/ResourceUtils";
import { TreeListContext } from "../../../utils/UIUtils";
import { VBActionsEnum } from "../../../utils/VBActions";
import { TreeNodeDeleteUndoData, VBEventHandler } from "../../../utils/VBEventHandler";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { SharedModalServices } from "../../../modal-dialogs/shared-modals/shared-modals.service";
import { AbstractTree } from "../abstractTree";
import { CollectionTreeNodeComponent } from "./collection-tree-node.component";

@Component({
  selector: "collection-tree",
  templateUrl: "./collection-tree.component.html",
  host: { class: "treeListComponent" },
  standalone: false
})
export class CollectionTreeComponent extends AbstractTree {
  //CollectionTreeNodeComponent children of this Component (useful to open tree during the search)
  @ViewChildren(CollectionTreeNodeComponent) viewChildrenNode: QueryList<CollectionTreeNodeComponent>;

  structRole = RDFResourceRolesEnum.skosCollection;

  constructor(private skosService: SkosServices, private searchService: SearchServices,
    eventHandler: VBEventHandler, basicModals: BasicModalServices, sharedModals: SharedModalServices, changeDetectorRef: ChangeDetectorRef) {

    super(eventHandler, basicModals, sharedModals, changeDetectorRef);

    this.eventSubscriptions.push(eventHandler.rootCollectionCreatedEvent.subscribe(
      (newColl: ARTURIResource) => this.onRootCollectionCreated(newColl)));
    this.eventSubscriptions.push(eventHandler.collectionDeletedEvent.subscribe(
      (deletedCollection: ARTURIResource) => this.onTreeNodeDeleted(deletedCollection)));
    this.eventSubscriptions.push(eventHandler.nestedCollectionAddedEvent.subscribe(
      (data: any) => this.onNestedCollectionAdded(data.nested, data.container)));
    this.eventSubscriptions.push(eventHandler.nestedCollectionAddedFirstEvent.subscribe(
      (data: any) => this.onNestedCollectionAdded(data.nested, data.container)));
    this.eventSubscriptions.push(eventHandler.nestedCollectionAddedLastEvent.subscribe(
      (data: any) => this.onNestedCollectionAdded(data.nested, data.container)));
    this.eventSubscriptions.push(eventHandler.nestedCollectionAddedInPositionEvent.subscribe(
      (data: any) => this.onNestedCollectionAdded(data.nested, data.container)));
    this.eventSubscriptions.push(eventHandler.collectionDeletedUndoneEvent.subscribe(
      (data: TreeNodeDeleteUndoData) => this.onDeleteUndo(data)));
  }

  initImpl() {
    if (!AuthorizationEvaluator.isAuthorized(VBActionsEnum.skosGetCollectionTaxonomy)) {
      this.unauthorized = true;
      return;
    }

    this.loading = true;
    this.skosService.getRootCollections(this.showDeprecated, STRequestOptions.getRequestOptions(this.projectCtx)).pipe(
      finalize(() => { this.loading = false; })
    ).subscribe(
      rootColl => {
        //sort by show if rendering is active, uri otherwise
        ResourceUtils.sortResources(rootColl, this.rendering ? SortAttribute.show : SortAttribute.value);
        this.nodes = rootColl;
      },
    );
  }

  openTreeAt(node: ARTURIResource) {
    this.searchService.getPathFromRoot(node, RDFResourceRolesEnum.skosCollection, null, null, null, null, null, null,
      STRequestOptions.getRequestOptions(this.projectCtx)).subscribe(
        path => {
          if (path.length == 0) {
            this.onTreeNodeNotFound(node);
            return;
          }
          //open tree from root to node
          this.openRoot(path);
        }
      );
  }

  //EVENT LISTENERS

  private onRootCollectionCreated(collection: ARTURIResource) {
    this.nodes.unshift(collection);
    if (this.context == TreeListContext.addPropValue) {
      this.openRoot([collection]);
    }
  }

  private onNestedCollectionAdded(nested: ARTURIResource, container: ARTURIResource) {
    //if the nested was a root collection, then remove it from root (since it is no more a root by definition)
    for (let i = 0; i < this.nodes.length; i++) {
      if (this.nodes[i].equals(nested)) {
        this.nodes.splice(i, 1);
        break;
      }
    }
    void container; //unused in this case
  }

  private onDeleteUndo(data: TreeNodeDeleteUndoData) {
    if (data.parents.length == 0) {
      this.onRootCollectionCreated(data.resource);
    }
  }

}