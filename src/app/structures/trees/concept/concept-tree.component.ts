import { ChangeDetectorRef, Component, EventEmitter, Input, Output, QueryList, SimpleChanges, ViewChildren } from "@angular/core";
import { Observable, of } from "rxjs";
import { finalize, mergeMap } from 'rxjs/operators';
import { ARTURIResource, RDFResourceRolesEnum } from "../../../models/ARTResources";
import { ConceptTreePreference, ConceptTreeVisualizationMode, MultischemeMode, SafeToGo } from "../../../models/Properties";
import { SearchServices } from "../../../services/search.service";
import { SkosServices } from "../../../services/skos.service";
import { AuthorizationEvaluator } from "../../../utils/AuthorizationEvaluator";
import { STRequestOptions } from "../../../utils/HttpManager";
import { ResourceUtils, SortAttribute } from "../../../utils/ResourceUtils";
import { TreeListContext } from "../../../utils/UIUtils";
import { VBActionsEnum } from "../../../utils/VBActions";
import { VBContext } from "../../../utils/VBContext";
import { ConceptDeleteUndoData, VBEventHandler } from "../../../utils/VBEventHandler";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { SharedModalServices } from "../../../modal-dialogs/shared-modals/shared-modals.service";
import { AbstractTree } from "../abstractTree";
import { ConceptTreeNodeComponent } from "./concept-tree-node.component";

@Component({
  selector: "concept-tree",
  templateUrl: "./concept-tree.component.html",
  host: { class: "treeListComponent" },
  standalone: false
})
export class ConceptTreeComponent extends AbstractTree {
  @Input() schemes: ARTURIResource[];
  @Input() forceVisualizationMode: ConceptTreeVisualizationMode; //if not null, forces visualization mode regardles of configuration
  @Output() conceptRemovedFromScheme = new EventEmitter<ARTURIResource>();//used to report a concept removed from a scheme only when the scheme is the one used in the current concept tree
  @Output() switchMode = new EventEmitter<ConceptTreeVisualizationMode>(); //requires to the parent panel to switch mode


  //ConceptTreeNodeComponent children of this Component (useful to open tree during the search)
  @ViewChildren(ConceptTreeNodeComponent) viewChildrenNode: QueryList<ConceptTreeNodeComponent>;

  structRole = RDFResourceRolesEnum.concept;

  visualizationMode: ConceptTreeVisualizationMode; //this could be changed dynamically, so each time it is used, get it again from preferences

  ConceptTreeVisualizationMode = ConceptTreeVisualizationMode;

  translationParam: { count: number, safeToGoLimit: number };

  /*
   * when the tree is initialized multiple time in a short amount of time (e.g. when the scheme is changed and then immediately changed again)
   * it might happened that the first initialization request takes more time than the last, so the response of the first is reveived after
   * the last. In this way, the tree is initialized with the wrong response (the first instead of the last).
   * This variable is useful in order to keep trace of the last request and to take in account only the related response ignoring the others.
   */
  private lastInitTimestamp: number;

  constructor(private skosService: SkosServices, private searchService: SearchServices,
    eventHandler: VBEventHandler, basicModals: BasicModalServices, sharedModals: SharedModalServices, changeDetectorRef: ChangeDetectorRef) {
    super(eventHandler, basicModals, sharedModals, changeDetectorRef);
    this.eventSubscriptions.push(eventHandler.topConceptCreatedEvent.subscribe(
      (data: { concept: ARTURIResource, schemes: ARTURIResource[] }) => this.onTopConceptCreated(data.concept, data.schemes)));
    this.eventSubscriptions.push(eventHandler.conceptDeletedEvent.subscribe(
      (deletedConcept: ARTURIResource) => this.onTreeNodeDeleted(deletedConcept)));
    // this.eventSubscriptions.push(eventHandler.conceptRemovedFromSchemeEvent.subscribe(
    //   (data: any) => this.onConceptRemovedFromScheme(data.concept, data.scheme)));
    // this.eventSubscriptions.push(eventHandler.conceptRemovedAsTopConceptEvent.subscribe(
    //   (data: any) => this.onConceptRemovedFromScheme(data.concept, data.scheme)));
    this.eventSubscriptions.push(eventHandler.multischemeModeChangedEvent.subscribe(
      () => this.init() //multischeme mode changed => reinit tree
    ));
    this.eventSubscriptions.push(eventHandler.conceptDeletedUndoneEvent.subscribe(
      (data: ConceptDeleteUndoData) => this.onDeleteUndo(data)));
  }

  /**
   * Listener on changes of @Input scheme. When it changes, update the tree
   */
  ngOnChanges(changes: SimpleChanges) {
    if (changes['schemes']) {
      this.init();
    }
  }

  initImpl() {
    if (!AuthorizationEvaluator.isAuthorized(VBActionsEnum.skosGetConceptTaxonomy)) {
      this.unauthorized = true;
      return;
    }

    let conceptTreePreference: ConceptTreePreference = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().conceptTreePreferences;
    this.visualizationMode = this.forceVisualizationMode ? this.forceVisualizationMode : conceptTreePreference.visualization;

    if (this.visualizationMode == ConceptTreeVisualizationMode.hierarchyBased) {
      this.lastInitTimestamp = new Date().getTime();
      this.checkInitializationSafe().subscribe({
        next: () => {
          if (this.safeToGo.safe) {
            let conceptTreePreference: ConceptTreePreference = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().conceptTreePreferences;
            let broaderProps: ARTURIResource[] = conceptTreePreference.broaderProps.map((prop: string) => new ARTURIResource(prop));
            let narrowerProps: ARTURIResource[] = conceptTreePreference.narrowerProps.map((prop: string) => new ARTURIResource(prop));
            let includeSubProps: boolean = conceptTreePreference.includeSubProps;
            this.loading = true;
            this.skosService.getTopConcepts(this.lastInitTimestamp, this.schemes, conceptTreePreference.multischemeMode,
              broaderProps, narrowerProps, includeSubProps, this.showDeprecated, STRequestOptions.getRequestOptions(this.projectCtx)).pipe(
                finalize(() => { this.loading = false; })
              ).subscribe({
                next: data => {
                  if (data.timestamp != this.lastInitTimestamp) { //the response is not about the last getTopConcepts request
                    return; //=> ignore it
                  }
                  let topConcepts = data.concepts;
                  //sort by show if rendering is active, uri otherwise
                  ResourceUtils.sortResources(topConcepts, this.rendering ? SortAttribute.show : SortAttribute.value);
                  this.nodes = topConcepts;
                  if (this.pendingSearchPath) {
                    this.openRoot(this.pendingSearchPath);
                  }
                },
                error: (err: Error) => {
                  this.missingSchemeErrHandler(err);
                }
              });
          }
        },
        error: (err: Error) => {
          this.missingSchemeErrHandler(err);
        }
      });
    } else if (this.visualizationMode == ConceptTreeVisualizationMode.searchBased) {
      //reset list to empty
      this.forceList(null);
    }
  }

  private missingSchemeErrHandler(err: Error) {
    /*
    normally, deleted schemes are removed immediately from active scheme preference. 
    Anyway, when validation is enabled, the scheme deletion doesn't update immediately the preference
    (since the scheme still exists, it is simply marked as deprecated), the scheme is deleted only when the scheme deletion action
    is validated. So the active scheme remains pending. Here I handle the exception thrown in that case and emit the related event
    */
    if (err.name.endsWith("MethodConstraintViolationException")) {
      let msg = err.message; // something like: resource '[http://data.europa.eu/xsp/cn2020/conceptScheme_4d4e846a]' does not exist in the current dataset
      //isolate resource IRI
      let resIRI = msg.substring(msg.indexOf("[") + 1, msg.indexOf("]"));
      if (ResourceUtils.testIRI(resIRI)) {
        this.eventHandler.schemeDeletedEvent.emit(new ARTURIResource(resIRI));
      }
    }
  }

  /**
   * Forces the safeness of the structure even if it was reported as not safe, then re initialize it
   */
  forceSafeness() {
    this.safeToGo.safe = true;
    let checksum = this.getInitRequestChecksum();
    this.safeToGoMap[checksum] = this.safeToGo;
    this.initImpl();
  }

  /**
   * Perform a check in order to prevent the initialization of the structure with too many elements
   * Return true if the initialization is safe or if the user agreed to init the structure anyway
   */
  private checkInitializationSafe(): Observable<void> {
    let conceptTreePreference: ConceptTreePreference = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().conceptTreePreferences;
    let multischemeMode: MultischemeMode = conceptTreePreference.multischemeMode;
    let broaderProps: ARTURIResource[] = conceptTreePreference.broaderProps.map((prop: string) => new ARTURIResource(prop));
    let narrowerProps: ARTURIResource[] = conceptTreePreference.narrowerProps.map((prop: string) => new ARTURIResource(prop));
    let includeSubProps: boolean = conceptTreePreference.includeSubProps;

    if (this.safeToGoLimit != conceptTreePreference.safeToGoLimit) {
      this.safeToGoMap = {}; //limit changed, safetiness checks invalidated => reset the map
    }
    this.safeToGoLimit = conceptTreePreference.safeToGoLimit;

    let checksum = this.getInitRequestChecksum();

    let safeness: SafeToGo = this.safeToGoMap[checksum];
    if (safeness != null) { //found safeness in cache
      this.safeToGo = safeness;
      this.translationParam = { count: this.safeToGo.count, safeToGoLimit: this.safeToGoLimit };
      return of(null);
    } else { //never initialized => count
      this.loading = true;
      return this.skosService.countTopConcepts(this.lastInitTimestamp, this.schemes, multischemeMode, broaderProps, narrowerProps, includeSubProps, this.showDeprecated, STRequestOptions.getRequestOptions(this.projectCtx)).pipe(
        finalize(() => { this.loading = false; }),
        mergeMap(data => {
          let safeness = { safe: data.count < this.safeToGoLimit, count: data.count };
          this.safeToGoMap[checksum] = safeness; //cache the safeness
          if (data.timestamp != this.lastInitTimestamp) { //a newest request has been performed => stop this initialization
            return of(null);
          }
          this.safeToGo = safeness;
          this.translationParam = { count: this.safeToGo.count, safeToGoLimit: this.safeToGoLimit };
          return of(null);
        })
      );
    }
  }

  private getInitRequestChecksum() {
    let conceptTreePreference: ConceptTreePreference = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().conceptTreePreferences;
    let multischemeMode: MultischemeMode = conceptTreePreference.multischemeMode;
    let broaderProps: ARTURIResource[] = conceptTreePreference.broaderProps.map((prop: string) => new ARTURIResource(prop));
    let narrowerProps: ARTURIResource[] = conceptTreePreference.narrowerProps.map((prop: string) => new ARTURIResource(prop));
    let includeSubProps: boolean = conceptTreePreference.includeSubProps;
    let checksum = "schemes:" + this.schemes?.map(s => s.toNT()).join(",") +
      "&multischemeMode:" + multischemeMode +
      "&broaderProps:" + broaderProps +
      "&narrowerProps:" + narrowerProps +
      "&includeSubProps:" + includeSubProps + 
      "&showDeprecated:" + this.showDeprecated;
    return checksum;
  }

  public forceList(list: ARTURIResource[]) {
    this.safeToGo = { safe: true }; //prevent the list not showing if a previous hierarchy initialization set the safeToGo to false
    this.setInitialStatus();
    this.nodes = list;
  }

  openTreeAt(node: ARTURIResource) {
    let conceptTreePreference: ConceptTreePreference = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().conceptTreePreferences;
    let multischemeMode: MultischemeMode = conceptTreePreference.multischemeMode;
    let broaderProps: ARTURIResource[] = conceptTreePreference.broaderProps.map((prop: string) => new ARTURIResource(prop));
    let narrowerProps: ARTURIResource[] = conceptTreePreference.narrowerProps.map((prop: string) => new ARTURIResource(prop));
    let includeSubProps: boolean = conceptTreePreference.includeSubProps;
    this.searchService.getPathFromRoot(node, RDFResourceRolesEnum.concept, this.schemes, multischemeMode, broaderProps, narrowerProps, includeSubProps, null,
      STRequestOptions.getRequestOptions(this.projectCtx)).subscribe(
        path => {
          if (path.length == 0) {
            this.onTreeNodeNotFound(node);
            return;
          }
          //open tree from root to node
          this.openRoot(path);
        }
      );
  }

  //EVENT LISTENERS

  private onTopConceptCreated(concept: ARTURIResource, schemes: ARTURIResource[]) {
    if (this.schemes == undefined) { //in no-scheme mode add to the root if it isn't already in
      if (!ResourceUtils.containsNode(this.nodes, concept)) {
        this.nodes.unshift(concept);
        if (this.context == TreeListContext.addPropValue) {
          this.openRoot([concept]);
        }
      }
    } else { //otherwise add the top concept only if it is added in a scheme currently active in the tree
      if (this.schemes != null) {
        for (const s of schemes) {
          if (ResourceUtils.containsNode(this.schemes, s)) {
            this.nodes.unshift(concept);
            if (this.context == TreeListContext.addPropValue) {
              this.openRoot([concept]);
            }
            break;
          }
        }
      }
    }
  }

  /**
   * TODO: a concept is removed from a scheme, but I don't know if it is still in another scheme active.
   * E.g.
   * Schemes: S1, S2 (both visible)
   * Concept: C (in both S1 and S2)
   * C is removed from S1 but it should be still visible because is in S2 but I have not this info here. 
   * I just know that "concept" has been removed from "scheme".
   * Decide what to do, at the moment a refresh is required in order to see the updated tree
   */
  // private onConceptRemovedFromScheme(concept: ARTURIResource, scheme: ARTURIResource) {
  //   if (this.scheme != undefined && this.scheme.equals(scheme)) {
  //     for (let i = 0; i < this.nodes.length; i++) {
  //       if (this.nodes[i].equals(concept)) {
  //         this.nodes.splice(i, 1);
  //         this.conceptRemovedFromScheme.emit(concept);
  //         break;
  //       }
  //     }
  //   }
  // }

  private onDeleteUndo(data: ConceptDeleteUndoData) {
    if (data.parents.length > 0) return; //has broaders, so the concept to restore is not a top concept
    if (this.schemes == null || this.schemes.length == 0) { //no scheme mode => no check on scheme, simply add to roots
      this.nodes.push(data.resource);
    } else {
      let visible: boolean;
      let multischemeMode = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().conceptTreePreferences.multischemeMode;
      if (multischemeMode == MultischemeMode.or) { //concept to restore is visible if it belongs to at least one active scheme
        data.schemes.forEach(s => {
          if (this.schemes.some(activeSc => activeSc.equals(s))) {
            visible = true;
          }
        });
      } else { //mode AND, visible if belongs to every active scheme
        visible = true;
        this.schemes.forEach(actSc => {
          if (!data.schemes.some(s => s.equals(actSc))) {
            visible = false; //there is an active scheme which concept doesn't belong
          }
        });
      }
      if (visible) {
        this.nodes.push(data.resource);
      }
    }
  }

}