import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTNode, ARTURIResource } from 'src/app/models/ARTResources';

/**
 * Modal that allows to choose among a set of rdfResource
 */
@Component({
  selector: "off-scheme-search-resolution-modal",
  templateUrl: "./off-scheme-search-resolution-modal.component.html",
  standalone: false
})
export class OffSchemeSearchResolutionModalComponent {
  @Input() title: string;
  @Input() rendering: boolean = true;
  @Input() schemes: ARTURIResource[];
  /*
  0 schemes:
  - switch no-scheme mode
  - open searched resource in dialog
  1+ scheme:
  - switch to selected scheme
  - open searched resource in dialog
   */

  message: string;
  selectedScheme: ARTURIResource;
  selectedResources: ARTURIResource[];

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
    if (this.schemes.length == 1) {
      this.selectedScheme = this.schemes[0];
      this.selectedResources = [this.selectedScheme];
    }
  }

  onResourceSelected(resources: ARTNode[]) {
    if (resources.length > 0) {
      this.selectedScheme = resources[0] as ARTURIResource;
    }
  }

  changeSchemes() {
    this.activeModal.close({
      switchScheme: true,
      scheme: this.selectedScheme //null in case of no-scheme mode
    });
  }

  openInDialog() {
    this.activeModal.close({ switchScheme: false });
  }

  cancel() {
    this.activeModal.dismiss();
  }
}

export interface OffSchemeSearchResolutionModalReturn {
  switchScheme: boolean; //true => switch to no-scheme or activate selected scheme; false => open in dialog
  scheme?: ARTURIResource; //scheme to add/switch to (null for switching to no-scheme)
}