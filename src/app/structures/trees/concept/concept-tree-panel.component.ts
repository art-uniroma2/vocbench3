import { ChangeDetectorRef, Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from "@ngx-translate/core";
import { finalize, Observable, of } from 'rxjs';
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { SharedModalServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { GraphModalServices } from "../../../graph/modals/graph-modal.service";
import { ARTURIResource, RDFResourceRolesEnum, ResAttribute } from "../../../models/ARTResources";
import { Project } from "../../../models/Project";
import { ConceptTreeVisualizationMode, ProjectUserSettings, SearchSettings } from "../../../models/Properties";
import { CustomFormsServices } from "../../../services/custom-forms.service";
import { ResourcesServices } from "../../../services/resources.service";
import { SearchServices } from "../../../services/search.service";
import { SkosServices } from "../../../services/skos.service";
import { AuthorizationEvaluator } from "../../../utils/AuthorizationEvaluator";
import { STRequestOptions } from "../../../utils/HttpManager";
import { ResourceUtils, SortAttribute } from "../../../utils/ResourceUtils";
import { ActionDescription, RoleActionResolver } from "../../../utils/RoleActionResolver";
import { TreeListContext } from "../../../utils/UIUtils";
import { VBActionFunctionCtx, VBActionsEnum } from "../../../utils/VBActions";
import { VBContext } from "../../../utils/VBContext";
import { VBEventHandler } from "../../../utils/VBEventHandler";
import { VBProperties } from "../../../utils/VBProperties";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../../modal-dialogs/browsing-modals/browsing-modals.service";
import { MultiSubjectEnrichmentHelper } from "../../multiSubjectEnrichmentHelper";
import { AbstractTreePanel } from "../abstractTreePanel";
import { AddToSchemeModalComponent } from "./add-to-scheme-modal.component";
import { ConceptTreeSettingsModalComponent } from "./concept-tree-settings-modal.component";
import { ConceptTreeComponent } from "./concept-tree.component";
import { OffSchemeSearchResolutionModalComponent, OffSchemeSearchResolutionModalReturn } from './off-scheme-search-resolution-modal.component';

@Component({
  selector: "concept-tree-panel",
  templateUrl: "./concept-tree-panel.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class ConceptTreePanelComponent extends AbstractTreePanel {

  @Input() schemes: ARTURIResource[]; //if set the concept tree is initialized with this scheme, otherwise with the scheme from VB context
  @Input() schemeChangeable: boolean = false; //if true, above the tree is shown a menu to select a scheme
  @Input() forceVisualizationMode: ConceptTreeVisualizationMode; //if not null, forces visualization mode regardles of configuration
  @Output() schemeChanged = new EventEmitter<ARTURIResource[]>(); //when dynamic scheme is changed (inform parent component in order to eventually reset a previous concept selection)

  @ViewChild(ConceptTreeComponent) viewChildTree: ConceptTreeComponent;

  panelRole: RDFResourceRolesEnum = RDFResourceRolesEnum.concept;

  private modelType: string;

  workingSchemes: ARTURIResource[];//keep track of the selected scheme: could be assigned throught @Input scheme or scheme selection
  //(useful expecially when schemeChangeable is true so the changes don't effect the scheme in context)

  visualizationMode: ConceptTreeVisualizationMode;//this could be changed dynamically, so each time it is used, get it again from preferences

  //for visualization searchBased
  lastSearch: string;

  constructor(private skosService: SkosServices,
    private searchService: SearchServices,
    private browsingModals: BrowsingModalServices,
    private modalService: NgbModal,
    private translateService: TranslateService,
    cfService: CustomFormsServices,
    resourceService: ResourcesServices,
    basicModals: BasicModalServices,
    sharedModals: SharedModalServices,
    graphModals: GraphModalServices,
    eventHandler: VBEventHandler,
    vbProp: VBProperties,
    actionResolver: RoleActionResolver,
    multiEnrichment: MultiSubjectEnrichmentHelper,
    cdRef: ChangeDetectorRef
  ) {
    super(cfService, resourceService, basicModals, sharedModals, graphModals, eventHandler, vbProp, actionResolver, multiEnrichment, cdRef);

    this.eventSubscriptions.push(eventHandler.schemeChangedEvent.subscribe(
      (data: { schemes: ARTURIResource[], project: Project }) => {
        if (VBContext.getWorkingProjectCtx(this.projectCtx).getProject().getName() == data.project.getName()) {
          this.onSchemeChanged(data.schemes);
        }
      })
    );
  }

  ngOnInit() {
    super.ngOnInit();

    this.visualizationMode = this.getVisualizationMode();

    this.modelType = VBContext.getWorkingProjectCtx(this.projectCtx).getProject().getModelType();

    //Initialize working schemes
    if (this.schemes === undefined) { //if @Input is not provided at all, get the scheme from the preferences
      this.workingSchemes = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().activeSchemes;
    } else { //if @Input schemes is provided (it could be null => no scheme-mode), initialize the tree with this scheme
      this.workingSchemes = this.schemes;
    }
  }

  //top bar commands handlers

  getActionContext(): VBActionFunctionCtx {
    let metaClass: ARTURIResource = ResourceUtils.convertRoleToClass(this.panelRole, this.modelType);
    let actionCtx: VBActionFunctionCtx = { metaClass: metaClass, treeListComponent: this.viewChildTree, schemes: this.workingSchemes };
    return actionCtx;
  }


  //@Override
  isActionDisabled(action: ActionDescription) {
    //In addition to the cross-panel conditions, in this case the create actions are disabled if the panel is in no-scheme mode
    return super.isActionDisabled(action) || (action.editType == "C" && this.isNoSchemeMode());
  }

  refresh() {
    this.visualizationMode = this.getVisualizationMode();
    if (this.visualizationMode == ConceptTreeVisualizationMode.hierarchyBased) {
      //in index based visualization reinit the list
      this.viewChildTree.init();
    } else if (this.visualizationMode == ConceptTreeVisualizationMode.searchBased) {
      //in search based visualization repeat the search
      if (this.lastSearch != undefined) {
        this.doSearch(this.lastSearch);
      }
    }
  }

  isNoSchemeMode() {
    return this.workingSchemes.length == 0;
  }

  changeSchemeSelection() {
    this.skosService.getAllSchemes(null, STRequestOptions.getRequestOptions(this.projectCtx)).subscribe(
      schemes => {
        this.sharedModals.selectResource({ key: "DATA.ACTIONS.SELECT_SCHEME" }, null, schemes, this.rendering, true, true, this.workingSchemes).then(
          (schemes: ARTURIResource[]) => {
            this.workingSchemes = schemes;
            this.schemeChanged.emit(schemes);
          },
          () => { }
        );
      },
      () => { }
    );
  }

  //search handlers

  doSearch(searchedText: string) {
    this.lastSearch = searchedText;

    let projPref: ProjectUserSettings = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences();
    let searchSettings: SearchSettings = projPref.searchSettings;
    let searchLangs: string[];
    let includeLocales: boolean;
    if (searchSettings.restrictLang) {
      searchLangs = searchSettings.languages;
      includeLocales = searchSettings.includeLocales;
    }
    let searchingScheme: ARTURIResource[] = [];
    if (searchSettings.restrictActiveScheme) {
      searchingScheme = this.workingSchemes;
    }

    this.viewChildTree.loading = true;
    this.searchService.searchResource(searchedText, [RDFResourceRolesEnum.concept], searchSettings.useLocalName, searchSettings.useURI,
      searchSettings.useNotes, searchSettings.stringMatchMode, searchLangs, includeLocales, searchingScheme,
      projPref.conceptTreePreferences.multischemeMode, STRequestOptions.getRequestOptions(this.projectCtx)).pipe(
        finalize(() => { this.viewChildTree.loading = false; })
      ).subscribe(
        searchResult => {
          ResourceUtils.sortResources(searchResult, this.rendering ? SortAttribute.show : SortAttribute.value);
          this.visualizationMode = this.getVisualizationMode();
          if (this.visualizationMode == ConceptTreeVisualizationMode.hierarchyBased) {
            if (searchResult.length == 0) {
              this.basicModals.alert({ key: "SEARCH.SEARCH" }, { key: "MESSAGES.NO_RESULTS_FOUND_FOR", params: { text: searchedText } }, ModalType.warning);
            } else { //1 or more results
              if (searchResult.length == 1) {
                this.selectSearchedResource(searchResult[0]);
              } else { //multiple results, ask the user which one select
                this.sharedModals.selectResource({ key: "SEARCH.SEARCH" }, { key: "MESSAGES.TOT_RESULTS_FOUND", params: { count: searchResult.length } }, searchResult, this.rendering).then(
                  (selectedResources: ARTURIResource[]) => {
                    this.selectSearchedResource(selectedResources[0]);
                  },
                  () => { }
                );
              }
            }
          } else { //searchBased
            if (searchResult.length == 0) {
              this.basicModals.alert({ key: "SEARCH.SEARCH" }, { key: "MESSAGES.NO_RESULTS_FOUND_FOR", params: { text: searchedText } }, ModalType.warning);
            }
            this.viewChildTree.forceList(searchResult);
          }
        }
      );
  }

  /**
   * This method could be invoked by:
   * - the current component in doSearch only in hierarchy-based mode
   * - the parent component after an advanced search, so it should takes in account also the search-based mode
   * @param resource 
   */
  public selectSearchedResource(resource: ARTURIResource) {
    this.getSearchedConceptSchemes(resource).subscribe(
      schemes => {
        let isInActiveSchemes: boolean = false;
        if (this.workingSchemes.length == 0) { //no scheme mode -> searched concept should be visible
          isInActiveSchemes = true;
        } else {
          for (const s of schemes) {
            if (ResourceUtils.containsNode(this.workingSchemes, s)) {
              isInActiveSchemes = true;
              break;
            }
          }
        }
        if (isInActiveSchemes) {
          this.selectResourceVisualizationModeAware(resource);
        } else {
          if (schemes.length == 0) { //searched concept doesn't belong to any scheme => ask switch to no-scheme mode
            this.resolveOffSchemeSearch(schemes).then(
              (data: OffSchemeSearchResolutionModalReturn) => {
                if (data.switchScheme) {
                  this.vbProp.setActiveSchemes(VBContext.getWorkingProjectCtx(this.projectCtx), []).subscribe();
                  this.cdRef.detectChanges();
                  this.selectResourceVisualizationModeAware(resource);
                } else {
                  this.sharedModals.openResourceView(resource, false, this.projectCtx);
                }
              },
              () => { }
            );
          } else {
            this.resourceService.getResourcesInfo(schemes).subscribe(
              schemes => {
                this.resolveOffSchemeSearch(schemes).then(
                  (data: OffSchemeSearchResolutionModalReturn) => {
                    if (data.switchScheme) {
                      if (this.context == TreeListContext.addPropValue) {
                        //update active scheme only here, so outside the "add" operation everything stays unchanged 
                        this.workingSchemes = this.workingSchemes.slice().concat(data.scheme); //slice so workingScheme is a new object and triggers ngOnChanges in ConceptTreeComponent
                      } else {
                        this.workingSchemes = this.workingSchemes.concat(data.scheme);
                        this.vbProp.setActiveSchemes(VBContext.getWorkingProjectCtx(this.projectCtx), this.workingSchemes).subscribe(); //update the active schemes
                      }
                      this.cdRef.detectChanges();
                      this.selectResourceVisualizationModeAware(resource);
                    } else {
                      this.sharedModals.openResourceView(resource, false, this.projectCtx);
                    }
                  },
                  () => { }
                );
              }
            );
          }
        }
      }
    );
  }

  private resolveOffSchemeSearch(schemes: ARTURIResource[]) {
    const modalRef: NgbModalRef = this.modalService.open(OffSchemeSearchResolutionModalComponent, new ModalOptions());
    modalRef.componentInstance.title = this.translateService.instant("SEARCH.SEARCH");
    modalRef.componentInstance.rendering = this.rendering;
    modalRef.componentInstance.schemes = schemes;
    return modalRef.result;
  }

  private selectResourceVisualizationModeAware(resource: ARTURIResource) {
    this.visualizationMode = this.getVisualizationMode();
    if (this.visualizationMode == ConceptTreeVisualizationMode.hierarchyBased) {
      this.openTreeAt(resource);
    } else {
      this.viewChildTree.forceList([resource]);
      this.cdRef.detectChanges();
      this.viewChildTree.openRoot([resource]);
    }
  }

  /**
   * Schemes of a searched concept could be retrieved from a "schemes" attribute (if searched by a "ordinary" search), or from
   * invoking a specific service (if the "schemes" attr is not present when searched by advanced search)
   */
  private getSearchedConceptSchemes(concept: ARTURIResource): Observable<ARTURIResource[]> {
    let schemes: ARTURIResource[] = concept.getAdditionalProperty(ResAttribute.SCHEMES);
    if (schemes == null) {
      return this.skosService.getSchemesOfConcept(concept);
    } else {
      return of(schemes);
    }
  }

  openTreeAt(resource: ARTURIResource) {
    this.viewChildTree.openTreeAt(resource);
  }

  settings() {
    const modalRef: NgbModalRef = this.modalService.open(ConceptTreeSettingsModalComponent, new ModalOptions());
    return modalRef.result.then(
      () => {
        this.viewChildTree.init();
      },
      () => { }
    );
  }

  onSwitchMode(mode: ConceptTreeVisualizationMode) {
    if (this.forceVisualizationMode) {
      /*
      no need to update the stored pref since the view has been switched (probably throught the link "Switch to hierarchical visualization mode")
      when the structure was in a "forced" visualization mode. This means that the actual preference was ignored (in favor of the forced one), 
      thus needs to remain as it was.
      */
      return;
    }
    let concTreePrefs = VBContext.getWorkingProjectCtx().getProjectPreferences().conceptTreePreferences;
    concTreePrefs.visualization = mode;
    this.vbProp.setConceptTreePreferences(concTreePrefs).subscribe();
    this.viewChildTree.init();
  }

  isAddToSchemeEnabled() {
    return this.selectedNode != null && this.isContextDataPanel() && this.editable &&
      AuthorizationEvaluator.isAuthorized(VBActionsEnum.skosAddMultipleToScheme);
  }
  addToScheme() {
    this.browsingModals.browseSchemeList({ key: "DATA.ACTIONS.SELECT_SCHEME" }).then(
      scheme => {
        const modalRef: NgbModalRef = this.modalService.open(AddToSchemeModalComponent, new ModalOptions());
        modalRef.componentInstance.concept = this.selectedNode;
        modalRef.componentInstance.scheme = scheme;
        return modalRef.result;
      },
      () => { }
    );
  }

  private getVisualizationMode() {
    return this.forceVisualizationMode ? this.forceVisualizationMode : VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().conceptTreePreferences.visualization;
  }

  //EVENT LISTENERS

  //when a concept is removed from a scheme, it should be still visible in res view,
  //but no more selected in the tree if it was in the current scheme 
  onConceptRemovedFromScheme(concept: ARTURIResource) {
    this.selectedNode = null;
    void concept;
  }

  private onSchemeChanged(schemes: ARTURIResource[]) {
    this.workingSchemes = schemes;
    //in case of visualization search based reset the list (in case of hierarchy the tree is refreshed following ngOnChanges on @Input schemes)
    this.visualizationMode = this.getVisualizationMode();
    if (this.visualizationMode == ConceptTreeVisualizationMode.searchBased && this.lastSearch != null) {
      this.viewChildTree.init();
    }
  }

}