import { ChangeDetectorRef, Component, QueryList, ViewChildren } from "@angular/core";
import { finalize } from "rxjs";
import { CustomTreeSettings } from 'src/app/models/Properties';
import { CustomTreesServices } from 'src/app/services/custom-trees.service';
import { VBContext } from 'src/app/utils/VBContext';
import { ARTURIResource, RDFResourceRolesEnum } from "../../../models/ARTResources";
import { SearchServices } from "../../../services/search.service";
import { ResourceUtils, SortAttribute } from "../../../utils/ResourceUtils";
import { VBEventHandler } from "../../../utils/VBEventHandler";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { SharedModalServices } from "../../../modal-dialogs/shared-modals/shared-modals.service";
import { AbstractTree } from "../abstractTree";
import { CustomTreeNodeComponent } from './custom-tree-node.component';

@Component({
  selector: "custom-tree",
  templateUrl: "./custom-tree.component.html",
  host: { class: "treeListComponent" },
  standalone: false
})
export class CustomTreeComponent extends AbstractTree {

  @ViewChildren(CustomTreeNodeComponent) viewChildrenNode: QueryList<CustomTreeNodeComponent>;

  structRole = RDFResourceRolesEnum.undetermined;

  constructor(private customTreeService: CustomTreesServices, private searchService: SearchServices,
    eventHandler: VBEventHandler, basicModals: BasicModalServices, sharedModals: SharedModalServices, changeDetectorRef: ChangeDetectorRef) {
    super(eventHandler, basicModals, sharedModals, changeDetectorRef);

    this.eventSubscriptions.push(this.eventHandler.customTreeSettingsChangedEvent.subscribe(
      () => this.init()
    ));
  }

  initImpl() {
    //sort by show if rendering is active, uri otherwise
    let orderAttribute: SortAttribute = this.rendering ? SortAttribute.show : SortAttribute.value;

    let ctSettings: CustomTreeSettings = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().customTreeSettings;

    //check necessary since customTreeSettingsChangedEvent might be handled before the tabset in Data is updated (so CTree might be still present but no config available)
    if (!ctSettings.enabled) return;

    this.loading = true;
    this.customTreeService.getRoots(this.showDeprecated).pipe(
      finalize(() => { this.loading = false; })
    ).subscribe(
      roots => {
        ResourceUtils.sortResources(roots, orderAttribute);
        this.nodes = roots.filter(r => r instanceof ARTURIResource);
      }
    );
  }

  openTreeAt(_node: ARTURIResource) {
  }

}