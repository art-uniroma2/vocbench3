import { ChangeDetectorRef, Component, ViewChild } from "@angular/core";
import { SharedModalServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { GraphModalServices } from "../../../graph/modals/graph-modal.service";
import { ARTURIResource, RDFResourceRolesEnum } from "../../../models/ARTResources";
import { CustomFormsServices } from "../../../services/custom-forms.service";
import { ResourcesServices } from "../../../services/resources.service";
import { RoleActionResolver } from "../../../utils/RoleActionResolver";
import { VBActionFunctionCtx } from "../../../utils/VBActions";
import { VBEventHandler } from "../../../utils/VBEventHandler";
import { VBProperties } from "../../../utils/VBProperties";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { MultiSubjectEnrichmentHelper } from "../../multiSubjectEnrichmentHelper";
import { AbstractTreePanel } from "../abstractTreePanel";
import { CustomTreeComponent } from './custom-tree.component';

@Component({
  selector: "custom-tree-panel",
  templateUrl: "./custom-tree-panel.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class CustomTreePanelComponent extends AbstractTreePanel {

  @ViewChild(CustomTreeComponent) viewChildTree: CustomTreeComponent;

  panelRole: RDFResourceRolesEnum = RDFResourceRolesEnum.undetermined;

  constructor(cfService: CustomFormsServices,
    resourceService: ResourcesServices,
    basicModals: BasicModalServices,
    sharedModals: SharedModalServices,
    graphModals: GraphModalServices,
    eventHandler: VBEventHandler,
    vbProp: VBProperties,
    actionResolver: RoleActionResolver,
    multiEnrichment: MultiSubjectEnrichmentHelper,
    cdRef: ChangeDetectorRef
  ) {
    super(cfService, resourceService, basicModals, sharedModals, graphModals, eventHandler, vbProp, actionResolver, multiEnrichment, cdRef);
  }


  getActionContext(_role?: RDFResourceRolesEnum): VBActionFunctionCtx {
    return {
      metaClass: null,
      treeListComponent: this.viewChildTree
    };
  }

  refresh() {
    this.viewChildTree.init();
  }

  //search handlers

  doSearch(_searchedText: string) {
  }

  openTreeAt(resource: ARTURIResource) {
    this.viewChildTree.openTreeAt(resource);
  }

}