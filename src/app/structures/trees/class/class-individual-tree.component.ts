import { Component, EventEmitter, Input, Output, SimpleChanges, ViewChild } from "@angular/core";
import { ConceptTreeVisualizationMode, InstanceListVisualizationMode, LexEntryVisualizationMode } from 'src/app/models/Properties';
import { ARTURIResource } from "../../../models/ARTResources";
import { TreeListContext } from "../../../utils/UIUtils";
import { ProjectContext, VBContext } from "../../../utils/VBContext";
import { ClassTreePanelComponent } from "./class-tree-panel.component";
import { NodeSelectEvent } from "../../abstractNode";

@Component({
  selector: "class-individual-tree",
  templateUrl: "./class-individual-tree.component.html",
  standalone: false
})
export class ClassIndividualTreeComponent {

  @Input() context: TreeListContext;
  @Input() projectCtx: ProjectContext;
  @Input() roots: ARTURIResource[]; //roots of the class three
  @Input() schemes: ARTURIResource[]; //scheme to use in case the class selected is skos:Concept
  @Input() editable: boolean = true; //used only in right panel (instance/concept)
  @Input() deletable: boolean = true; //used only in right panel (instance/concept)
  @Input() allowMultiselection: boolean = false; //tells if the multiselection is allowed in the instance list panel
  @Input() forceVisualizationMode: ConceptTreeVisualizationMode | LexEntryVisualizationMode | InstanceListVisualizationMode;
  @Output() nodeSelected = new EventEmitter<NodeSelectEvent|null>();//when an instance or a concept is selected
  @Output() nodeChecked = new EventEmitter<ARTURIResource[]>();
  @Output() multiselectionStatus = new EventEmitter<boolean>(); //emitted when the multiselection changes status (activated/deactivated)
  /*in the future I might need an Output for selected class. In case, change nodeSelected in instanceSelected and
  create classSelected Output. (Memo: nodeSelected is to maintain the same Output of the other tree components)*/

  @ViewChild(ClassTreePanelComponent, { static: true }) classTreePanelChild: ClassTreePanelComponent;

  selectedClass: ARTURIResource = null; //the class selected from class tree
  currentSchemes: ARTURIResource[];//the scheme selecte in the concept tree (only if selected class is skos:Concept)
  selectedInstance: ARTURIResource; //the instance (or concept) selected in the instance list (or concept tree)

  ngOnInit() {
    if (this.schemes === undefined) { //if @Input scheme is not provided at all, get it from project preference
      this.currentSchemes = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().activeSchemes;
    } else { //if @Input scheme is provided (it could be null => no scheme-mode), initialize the tree with this scheme
      this.currentSchemes = this.schemes;
    }
    if (this.context == undefined) { //if not overwritten from a parent component (e.g. addPropertyValueModal), set its default
      this.context = TreeListContext.clsIndTree;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['roots']) { //when roots changes, deselect eventals class and instance selected
      this.selectedClass = null;
      this.selectedInstance = null;
    }
  }

  /**
   * Listener to the event nodeSelected thrown by the class-tree. Updates the selectedClass
   */
  onTreeClassSelected(event?: NodeSelectEvent) {
    let cls = event?.value;
    if (this.selectedClass == undefined || (this.selectedClass != undefined && !this.selectedClass.equals(cls))) {
      this.selectedInstance = null; //reset the instance only if selected class changes
      this.nodeSelected.emit(null);
    }
    this.selectedClass = cls;
  }

  /**
   * Listener to click on element in the instance list. Updates the selectedInstance
   */
  onInstanceSelected(event?: NodeSelectEvent) {
    this.selectedInstance = event?.value;
    this.nodeSelected.emit(event);
  }

  onNodeChecked(nodes: ARTURIResource[]) {
    this.nodeChecked.emit(nodes);
  }

  /**
   * Listener to schemeChanged event emitted by concept-tree when range class is skos:Concept.
   */
  onConceptTreeSchemeChange() {
    this.selectedInstance = null;
    this.nodeSelected.emit(new NodeSelectEvent(this.selectedInstance));
  }

  /**
   * Listener to lexiconChanged event emitted by lexical-entry-list when range class is ontolex:LexicalEntry.
   */
  onLexEntryLexiconChange() {
    this.selectedInstance = null;
    this.nodeSelected.emit(new NodeSelectEvent(this.selectedInstance));
  }

  onMultiselectionChange(multiselection: boolean) {
    this.multiselectionStatus.emit(multiselection);
  }

}