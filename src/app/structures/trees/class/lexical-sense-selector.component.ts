import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ProjectContext } from "src/app/utils/VBContext";
import { ARTURIResource } from "../../../models/ARTResources";
import { OntoLexLemonServices } from "../../../services/ontolex-lemon.service";
import { NodeSelectEvent } from "../../abstractNode";

@Component({
  selector: "lexical-sense-selector",
  templateUrl: "./lexical-sense-selector.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class LexicalSenseSelectorComponent {
  @Input() projectCtx: ProjectContext;
  @Output() nodeSelected = new EventEmitter<NodeSelectEvent | null>();

  private selectedLexicalEntry: ARTURIResource;

  lexicalSenses: ARTURIResource[];
  private selectedSense: ARTURIResource;

  constructor(private ontolexService: OntoLexLemonServices) { }

  onLexEntrySelected(event?: NodeSelectEvent) {
    this.selectedLexicalEntry = event?.value;
    this.onLexicalSenseSelected(null);
    if (this.selectedLexicalEntry != null) {
      this.ontolexService.getLexicalEntrySenses(this.selectedLexicalEntry).subscribe(
        senses => {
          this.lexicalSenses = senses;
        }
      );
    }
  }

  onLexEntryLexiconChange() {
    this.selectedLexicalEntry = null;
    this.onLexicalSenseSelected(null);
  }

  onIndexChanged() {
    this.selectedLexicalEntry = null;
    this.onLexicalSenseSelected(null);
  }

  onLexicalSenseSelected(sense: ARTURIResource) {
    this.selectedSense = sense;
    this.nodeSelected.emit(new NodeSelectEvent(sense));
  }

}