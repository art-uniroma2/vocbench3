import { ChangeDetectorRef, Component, Input, QueryList, SimpleChanges, ViewChildren } from "@angular/core";
import { finalize } from "rxjs";
import { ARTURIResource, RDFResourceRolesEnum } from "../../../models/ARTResources";
import { ClassesServices } from "../../../services/classes.service";
import { SearchServices } from "../../../services/search.service";
import { AuthorizationEvaluator } from "../../../utils/AuthorizationEvaluator";
import { STRequestOptions } from "../../../utils/HttpManager";
import { NTriplesUtil, ResourceUtils, SortAttribute } from "../../../utils/ResourceUtils";
import { VBActionsEnum } from "../../../utils/VBActions";
import { VBContext } from "../../../utils/VBContext";
import { VBEventHandler } from "../../../utils/VBEventHandler";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { SharedModalServices } from "../../../modal-dialogs/shared-modals/shared-modals.service";
import { AbstractTree } from "../abstractTree";
import { ClassTreeNodeComponent } from "./class-tree-node.component";

@Component({
  selector: "class-tree",
  templateUrl: "./class-tree.component.html",
  host: { class: "treeListComponent" },
  standalone: false
})
export class ClassTreeComponent extends AbstractTree {
  @Input() roots: ARTURIResource[];
  @Input() selectionOnInit: ARTURIResource; //specifies a resource to select after the tree init
  @Input() filterEnabled: boolean = false;

  //ClassTreeNodeComponent children of this Component (useful to open tree during the search)
  @ViewChildren(ClassTreeNodeComponent) viewChildrenNode: QueryList<ClassTreeNodeComponent>;

  structRole = RDFResourceRolesEnum.cls;

  constructor(private clsService: ClassesServices, private searchService: SearchServices,
    eventHandler: VBEventHandler, basicModals: BasicModalServices, sharedModals: SharedModalServices, changeDetectorRef: ChangeDetectorRef) {
    super(eventHandler, basicModals, sharedModals, changeDetectorRef);
    this.eventSubscriptions.push(eventHandler.classDeletedEvent.subscribe(
      (cls: ARTURIResource) => this.onTreeNodeDeleted(cls)));
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['roots']) {
      this.init();
    }
  }

  initImpl() {
    if (!AuthorizationEvaluator.isAuthorized(VBActionsEnum.classesGetClassTaxonomy)) {
      this.unauthorized = true;
      return;
    }

    let clsTreeRoots: ARTURIResource[] = this.roots;
    if (clsTreeRoots == undefined || clsTreeRoots.length == 0) {
      clsTreeRoots = [NTriplesUtil.parseURI(VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().classTreePreferences.rootClass)];
    }

    this.loading = true;
    this.clsService.getClassesInfo(clsTreeRoots, STRequestOptions.getRequestOptions(this.projectCtx)).pipe(
      finalize(() => { this.loading = false; })
    ).subscribe(
      roots => {
        //sort by show if rendering is active, uri otherwise
        ResourceUtils.sortResources(roots, this.rendering ? SortAttribute.show : SortAttribute.value);
        this.nodes = this.nodes.concat(roots);
        this.nodes = this.nodes.filter(n => this.showDeprecated || !n.isDeprecated());
        if (this.selectionOnInit != null) {
          this.openTreeAt(this.selectionOnInit);
        }
      });
  }

  openTreeAt(node: ARTURIResource) {
    let rootForPath: ARTURIResource;
    if (this.roots == undefined || this.roots.length == 0) {
      rootForPath = NTriplesUtil.parseURI(VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().classTreePreferences.rootClass);
    } else if (this.roots != undefined && this.roots.length == 1) {
      rootForPath = this.roots[0];
    }
    this.searchService.getPathFromRoot(node, RDFResourceRolesEnum.cls, null, null, null, null, null, rootForPath,
      STRequestOptions.getRequestOptions(this.projectCtx)).subscribe(
        path => {
          if (path.length == 0) {
            this.onTreeNodeNotFound(node);
            return;
          }
          this.openRoot(path);
        }
      );
  }

}