import { ChangeDetectorRef, Directive } from '@angular/core';
import { GraphModalServices } from "../../graph/modals/graph-modal.service";
import { ARTURIResource, RDFResourceRolesEnum, ResAttribute } from "../../models/ARTResources";
import { CustomFormsServices } from "../../services/custom-forms.service";
import { ResourcesServices } from "../../services/resources.service";
import { ActionDescription, RoleActionResolver } from "../../utils/RoleActionResolver";
import { VBEventHandler } from "../../utils/VBEventHandler";
import { VBProperties } from "../../utils/VBProperties";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { ModalType } from '../../modal-dialogs/Modals';
import { SharedModalServices } from '../../modal-dialogs/shared-modals/shared-modals.service';
import { AbstractPanel } from "../abstractPanel";
import { MultiSubjectEnrichmentHelper } from "../multiSubjectEnrichmentHelper";

@Directive()
export abstract class AbstractTreePanel extends AbstractPanel {

  /**
   * VIEWCHILD, INPUTS / OUTPUTS
   */

  /**
   * ATTRIBUTES
   */



  /**
   * CONSTRUCTOR
   */
  constructor(
    cfService: CustomFormsServices,
    resourceService: ResourcesServices,
    basicModals: BasicModalServices,
    sharedModals: SharedModalServices,
    graphModals: GraphModalServices,
    eventHandler: VBEventHandler,
    vbProp: VBProperties,
    actionResolver: RoleActionResolver,
    multiEnrichment: MultiSubjectEnrichmentHelper,
    cdRef: ChangeDetectorRef
  ) {
    super(cfService, resourceService, basicModals, sharedModals, graphModals, eventHandler, vbProp, actionResolver, multiEnrichment, cdRef);
  }

  /**
   * METHODS
   */

  // abstract createRoot(role?: RDFResourceRolesEnum): void;
  // abstract createChild(role?: RDFResourceRolesEnum): void;

  executeAction(act: ActionDescription, role?: RDFResourceRolesEnum) {
    if (act.conditions.pre.selectionRequired && act.conditions.pre.childlessRequired && this.selectedNode.getAdditionalProperty(ResAttribute.MORE)) {
      this.basicModals.alert({ key: "STATUS.OPERATION_DENIED" }, { key: "MESSAGES.OPERATION_DENIED_ON_NODE_WITH_CHILDREN" }, ModalType.warning);
      return;
    }
    act.function(this.getActionContext(role), this.selectedNode).subscribe(
      () => {
        if (act.conditions.post.deselectOnComplete) {
          this.selectedNode = null;
        }
      }
    );
  }


  abstract openTreeAt(node: ARTURIResource): void;

  //the following determines if the create button is disabled in the UI. It could be overriden in the extending components
  // isCreateChildDisabled(): boolean {
  //     return (!this.selectedNode || this.readonly || !AuthorizationEvaluator.Tree.isCreateAuthorized(this.panelRole));
  // }

}