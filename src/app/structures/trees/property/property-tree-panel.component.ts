import { ChangeDetectorRef, Component, Input, ViewChild } from "@angular/core";
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { SharedModalServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { GraphModalServices } from "../../../graph/modals/graph-modal.service";
import { ARTResource, ARTURIResource, RDFResourceRolesEnum } from "../../../models/ARTResources";
import { SearchSettings } from "../../../models/Properties";
import { OWL, RDF } from "../../../models/Vocabulary";
import { CustomFormsServices } from "../../../services/custom-forms.service";
import { ResourcesServices } from "../../../services/resources.service";
import { SearchServices } from "../../../services/search.service";
import { STRequestOptions } from "../../../utils/HttpManager";
import { ResourceUtils, SortAttribute } from "../../../utils/ResourceUtils";
import { RoleActionResolver } from "../../../utils/RoleActionResolver";
import { VBActionFunctionCtx } from "../../../utils/VBActions";
import { VBContext } from "../../../utils/VBContext";
import { VBEventHandler } from "../../../utils/VBEventHandler";
import { VBProperties } from "../../../utils/VBProperties";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { MultiSubjectEnrichmentHelper } from "../../multiSubjectEnrichmentHelper";
import { AbstractTreePanel } from "../abstractTreePanel";
import { PropertyTreeComponent } from "./property-tree.component";

@Component({
  selector: "property-tree-panel",
  templateUrl: "./property-tree-panel.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class PropertyTreePanelComponent extends AbstractTreePanel {
  @Input() resource: ARTResource;//provide to show just the properties with domain the type of the resource
  @Input() type: RDFResourceRolesEnum; //tells the type of the property to show in the tree
  @Input() roots: ARTURIResource[]; //in case the roots are provided to the component instead of being retrieved from server

  @ViewChild(PropertyTreeComponent) viewChildTree: PropertyTreeComponent;

  panelRole: RDFResourceRolesEnum = RDFResourceRolesEnum.property;
  rendering: boolean = false; //override the value in AbstractPanel

  constructor(private searchService: SearchServices,
    cfService: CustomFormsServices,
    resourceService: ResourcesServices,
    basicModals: BasicModalServices,
    sharedModals: SharedModalServices,
    graphModals: GraphModalServices,
    eventHandler: VBEventHandler,
    vbProp: VBProperties,
    actionResolver: RoleActionResolver,
    multiEnrichment: MultiSubjectEnrichmentHelper,
    cdRef: ChangeDetectorRef
  ) {
    super(cfService, resourceService, basicModals, sharedModals, graphModals, eventHandler, vbProp, actionResolver, multiEnrichment, cdRef);
  }


  getActionContext(role?: RDFResourceRolesEnum): VBActionFunctionCtx {
    let metaClass: ARTURIResource = role ? this.convertRoleToClass(role) : this.convertRoleToClass(this.selectedNode.getRole());
    let actionCtx: VBActionFunctionCtx = { metaClass: metaClass, treeListComponent: this.viewChildTree };
    return actionCtx;
  }

  refresh() {
    this.viewChildTree.init();
  }

  //search handlers

  doSearch(searchedText: string) {
    let searchSettings: SearchSettings = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().searchSettings;
    let searchLangs: string[];
    let includeLocales: boolean;
    if (searchSettings.restrictLang) {
      searchLangs = searchSettings.languages;
      includeLocales = searchSettings.includeLocales;
    }
    this.viewChildTree.loading = true;
    this.searchService.searchResource(searchedText, [RDFResourceRolesEnum.property], searchSettings.useLocalName,
      searchSettings.useURI, searchSettings.useNotes, searchSettings.stringMatchMode, searchLangs, includeLocales, null, null,
      STRequestOptions.getRequestOptions(this.projectCtx)).subscribe(
        searchResult => {
          this.viewChildTree.loading = false;
          if (searchResult.length == 0) {
            this.basicModals.alert({ key: "SEARCH.SEARCH" }, { key: "MESSAGES.NO_RESULTS_FOUND_FOR", params: { text: searchedText } }, ModalType.warning);
          } else { //1 or more results
            if (searchResult.length == 1) {
              this.openTreeAt(searchResult[0]);
            } else { //multiple results, ask the user which one select
              ResourceUtils.sortResources(searchResult, this.rendering ? SortAttribute.show : SortAttribute.value);
              this.sharedModals.selectResource({ key: "SEARCH.SEARCH" }, { key: "MESSAGES.TOT_RESULTS_FOUND", params: { count: searchResult.length } }, searchResult, this.rendering).then(
                (selectedResources: ARTURIResource[]) => {
                  this.openTreeAt(selectedResources[0]);
                },
                () => { }
              );
            }
          }
        }
      );
  }

  openTreeAt(resource: ARTURIResource) {
    this.viewChildTree.openTreeAt(resource);
  }

  //I don't know why, if I move this in ResourceUtils I get a strange error
  private convertRoleToClass(role: RDFResourceRolesEnum): ARTURIResource {
    let roleClass: ARTURIResource;
    if (role == RDFResourceRolesEnum.property) {
      roleClass = RDF.property;
    } else if (role == RDFResourceRolesEnum.datatypeProperty) {
      roleClass = OWL.datatypeProperty;
    } else if (role == RDFResourceRolesEnum.objectProperty) {
      roleClass = OWL.objectProperty;
    } else if (role == RDFResourceRolesEnum.annotationProperty) {
      roleClass = OWL.annotationProperty;
    } else if (role == RDFResourceRolesEnum.ontologyProperty) {
      roleClass = OWL.ontologyProperty;
    }
    return roleClass;
  }

}