import { ChangeDetectorRef, Component, Input, QueryList, SimpleChanges, ViewChildren } from "@angular/core";
import { finalize, Observable } from 'rxjs';
import { ARTResource, ARTURIResource, RDFResourceRolesEnum } from "../../../models/ARTResources";
import { RDFS } from "../../../models/Vocabulary";
import { PropertyServices } from "../../../services/properties.service";
import { SearchServices } from "../../../services/search.service";
import { AuthorizationEvaluator } from "../../../utils/AuthorizationEvaluator";
import { STRequestOptions } from "../../../utils/HttpManager";
import { ResourceUtils, SortAttribute } from "../../../utils/ResourceUtils";
import { TreeListContext } from "../../../utils/UIUtils";
import { VBActionsEnum } from "../../../utils/VBActions";
import { TreeNodeDeleteUndoData, VBEventHandler } from "../../../utils/VBEventHandler";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { SharedModalServices } from "../../../modal-dialogs/shared-modals/shared-modals.service";
import { AbstractTree } from "../abstractTree";
import { PropertyTreeNodeComponent } from "./property-tree-node.component";

@Component({
  selector: "property-tree",
  templateUrl: "./property-tree.component.html",
  host: { class: "treeListComponent" },
  standalone: false
})
export class PropertyTreeComponent extends AbstractTree {
  @Input() resource: ARTResource;//provided to show just the properties with domain the type of the resource
  @Input() type: RDFResourceRolesEnum; //tells the type of the property to show in the tree
  @Input() roots: ARTURIResource[]; //in case the roots are provided to the component instead of being retrieved from server

  //PropertyTreeNodeComponent children of this Component (useful to open tree during the search)
  @ViewChildren(PropertyTreeNodeComponent) viewChildrenNode: QueryList<PropertyTreeNodeComponent>;

  structRole = RDFResourceRolesEnum.property;

  constructor(private propertyService: PropertyServices, private searchService: SearchServices,
    eventHandler: VBEventHandler, basicModals: BasicModalServices, sharedModals: SharedModalServices, changeDetectorRef: ChangeDetectorRef) {

    super(eventHandler, basicModals, sharedModals, changeDetectorRef);

    this.eventSubscriptions.push(eventHandler.topPropertyCreatedEvent.subscribe(
      (node: ARTURIResource) => this.onTopPropertyCreated(node)));
    this.eventSubscriptions.push(eventHandler.propertyDeletedEvent.subscribe(
      (property: ARTURIResource) => this.onTreeNodeDeleted(property)));
    this.eventSubscriptions.push(eventHandler.propertyDeletedUndoneEvent.subscribe(
      (data: TreeNodeDeleteUndoData) => this.onDeleteUndo(data)));
  }

  /**
   * Called when @Input resource changes, reinitialize the tree
   */
  ngOnChanges(changes: SimpleChanges) {
    if (changes['resource'] || changes['roots']) {
      this.init();
    }
  }

  initImpl() {
    if (!AuthorizationEvaluator.isAuthorized(VBActionsEnum.propertiesGetPropertyTaxonomy)) {
      this.unauthorized = true;
      return;
    }

    //sort by show if rendering is active, uri otherwise
    let orderAttribute: SortAttribute = this.rendering ? SortAttribute.show : SortAttribute.value;

    /* different cases:
     * - roots provided as Input: tree is build rootet on these properties
     * - roots not provided, Input resource provided: tree roots are those properties that has types of this resource as domain
     * - type provided: initialize tree just for the given property type 
     * - no Input provided: tree roots retrieved from server without restrinction
     */
    if (this.roots) {
      this.loading = true;
      this.propertyService.getPropertiesInfo(this.roots, STRequestOptions.getRequestOptions(this.projectCtx)).pipe(
        finalize(() => { this.loading = false; })
      ).subscribe(
        props => {
          ResourceUtils.sortResources(props, orderAttribute);
          this.nodes = props;
        }
      );
    } else if (this.resource) {
      this.loading = true;
      this.propertyService.getRelevantPropertiesForResource(this.resource, STRequestOptions.getRequestOptions(this.projectCtx)).pipe(
        finalize(() => { this.loading = false; })
      ).subscribe(
        relevantProps => {
          //add (hardcode) rdfs:comment if not present
          let commentFound: boolean = false;
          relevantProps.forEach(p => {
            if (p.equals(RDFS.comment)) {
              commentFound = true;
            }
          });
          if (!commentFound) {
            relevantProps.push(RDFS.comment);
          }
          this.loading = true;
          this.propertyService.getPropertiesInfo(relevantProps, STRequestOptions.getRequestOptions(this.projectCtx)).pipe(
            finalize(() => { this.loading = false; })
          ).subscribe(
            props => {
              ResourceUtils.sortResources(props, orderAttribute);
              this.nodes = props;
            },
          );
        }
      );
    } else {
      let initTreeFn: Observable<ARTURIResource[]> = this.propertyService.getTopProperties(this.showDeprecated, STRequestOptions.getRequestOptions(this.projectCtx));
      if (this.type == RDFResourceRolesEnum.objectProperty) {
        initTreeFn = this.propertyService.getTopObjectProperties(this.showDeprecated, STRequestOptions.getRequestOptions(this.projectCtx));
      } else if (this.type == RDFResourceRolesEnum.annotationProperty) {
        initTreeFn = this.propertyService.getTopAnnotationProperties(this.showDeprecated, STRequestOptions.getRequestOptions(this.projectCtx));
      } else if (this.type == RDFResourceRolesEnum.datatypeProperty) {
        initTreeFn = this.propertyService.getTopDatatypeProperties(this.showDeprecated, STRequestOptions.getRequestOptions(this.projectCtx));
      } else if (this.type == RDFResourceRolesEnum.ontologyProperty) {
        initTreeFn = this.propertyService.getTopOntologyProperties(this.showDeprecated, STRequestOptions.getRequestOptions(this.projectCtx));
      }
      this.loading = true;
      initTreeFn.pipe(
        finalize(() => { this.loading = false; })
      ).subscribe(
        props => {
          ResourceUtils.sortResources(props, orderAttribute);
          this.nodes = props;
        }
      );
    }
  }

  openTreeAt(node: ARTURIResource) {
    this.searchService.getPathFromRoot(node, RDFResourceRolesEnum.property, null, null, null, null, null, null,
      STRequestOptions.getRequestOptions(this.projectCtx)).subscribe(
        path => {
          if (path.length == 0) {
            this.onTreeNodeNotFound(node);
            return;
          }
          //open tree from root to node
          this.openRoot(path);
        }
      );
  }

  //EVENT LISTENERS

  private onTopPropertyCreated(property: ARTURIResource) {
    this.nodes.unshift(property);
    if (this.context == TreeListContext.addPropValue) {
      this.openRoot([property]);
    }
  }

  private onDeleteUndo(data: TreeNodeDeleteUndoData) {
    if (data.parents.length == 0) {
      this.onTopPropertyCreated(data.resource);
    }
  }

}