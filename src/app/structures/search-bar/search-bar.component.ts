import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AutocompleteComponent } from 'angular-ng-autocomplete';
import { debounceTime, Subject, Subscription } from "rxjs";
import { STRequestOptions } from 'src/app/utils/HttpManager';
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { ARTResource, ARTURIResource, RDFResourceRolesEnum } from "../../models/ARTResources";
import { SearchMode, SearchSettings } from "../../models/Properties";
import { SearchServices } from "../../services/search.service";
import { TreeListContext } from "../../utils/UIUtils";
import { ProjectContext, VBContext } from "../../utils/VBContext";
import { VBProperties } from "../../utils/VBProperties";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { AdvancedSearchModalComponent } from "./advanced-search-modal.component";
import { CustomSearchModalComponent } from "./custom-search-modal.component";
import { LoadCustomSearchModalComponent } from "./load-custom-search-modal.component";
import { SearchSettingsModalComponent } from './search-settings-modal.component';

@Component({
  selector: "search-bar",
  templateUrl: "./search-bar.component.html",
  styleUrls: ["./search-bar.component.css"],
  standalone: false
})
export class SearchBarComponent {

  @ViewChild('inputField', { static: false }) private inputField: ElementRef;
  @ViewChild('autocompleter', { static: false }) private autocompleter: AutocompleteComponent;

  @Input() role: RDFResourceRolesEnum; //tells the role of the panel where the search bar is placed (usefull for customizing the settings)
  @Input() disabled: boolean = false;
  @Input() cls: ARTURIResource; //useful where search-bar is in the instance list panel
  @Input() schemes: ARTURIResource[]; //useful where search-bar is in the concept tree panel
  @Input() context: TreeListContext;
  @Input() projectCtx: ProjectContext;
  @Input() focusOnInit: boolean;
  @Output() search: EventEmitter<string> = new EventEmitter<string>();
  @Output() advancedSearch: EventEmitter<ARTResource> = new EventEmitter<ARTResource>();

  //search mode startsWith/contains/endsWith
  stringMatchModes: { labelTranslationKey: string, value: SearchMode, symbol: string }[] = [
    { labelTranslationKey: "SEARCH.SETTINGS.STARTS_WITH", value: SearchMode.startsWith, symbol: "α.." },
    { labelTranslationKey: "SEARCH.SETTINGS.CONTAINS", value: SearchMode.contains, symbol: ".α." },
    { labelTranslationKey: "SEARCH.SETTINGS.ENDS_WITH", value: SearchMode.endsWith, symbol: "..α" },
    { labelTranslationKey: "SEARCH.SETTINGS.EXACT", value: SearchMode.exact, symbol: "α" },
    { labelTranslationKey: "SEARCH.SETTINGS.FUZZY", value: SearchMode.fuzzy, symbol: "~α" }
  ];

  searchSettings: SearchSettings;
  searchStr: string;

  private eventSubscriptions: Subscription[] = [];

  constructor(
    private searchService: SearchServices,
    private modalService: NgbModal,
    private vbProperties: VBProperties,
    private basicModals: BasicModalServices
  ) {
    this.eventSubscriptions.push(
      this.inputChangeSubject.pipe(debounceTime(500)).subscribe(() => {
        this.getAutocompleterSuggestions();
      })
    );
  }

  ngOnInit() {
    this.searchSettings = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().searchSettings;
  }

  ngAfterViewInit() {
    if (this.focusOnInit) {
      if (this.inputField) {
        this.inputField.nativeElement.focus();
      } else if (this.autocompleter) {
        this.autocompleter.focus();
      }
    }
  }

  ngOnDestroy() {
    this.eventSubscriptions.forEach(s => s.unsubscribe());
  }

  doSearch() {
    if (this.autocompleter != null) { //in case using autocompleter, close the suggestions
      this.autocompleter.close();
    }
    if (this.searchStr != undefined && this.searchStr.trim() != "") {
      this.search.emit(this.searchStr);
    } else {
      this.basicModals.alert({ key: "SEARCH.SEARCH" }, { key: "MESSAGES.ENTER_VALID_STRING_TO_SEARCH" }, ModalType.warning);
    }
  }

  editSettings() {
    const modalRef: NgbModalRef = this.modalService.open(SearchSettingsModalComponent, new ModalOptions());
    modalRef.componentInstance.role = this.role;
    modalRef.componentInstance.structureCtx = this.context;
    modalRef.componentInstance.projectCtx = this.projectCtx;
    modalRef.result.then(
      () => {
        //search settings might changed, update searchSettings var
        this.searchSettings = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().searchSettings;
      }
    );
  }

  /**
   * Advanced and Custom search are available only if the panel is in the data page and works on the current project, not a contextual one
   */
  showOtherSearch(): boolean {
    return this.context == TreeListContext.dataPanel && this.projectCtx == null;
  }

  doAdvancedSearch() {
    const modalRef: NgbModalRef = this.modalService.open(AdvancedSearchModalComponent, new ModalOptions('lg'));
    modalRef.result.then(
      (resource: ARTResource) => {
        this.advancedSearch.emit(resource);
      },
      () => { }
    );
  }

  customSearch() {
    const modalRef: NgbModalRef = this.modalService.open(LoadCustomSearchModalComponent, new ModalOptions());
    modalRef.result.then(
      customSearchRef => {
        const modalRef: NgbModalRef = this.modalService.open(CustomSearchModalComponent, new ModalOptions());
        modalRef.componentInstance.searchParameterizationReference = customSearchRef;
        modalRef.result.then(
          (resource: ARTResource) => {
            //exploit the same event (and related handler) of advanced search
            this.advancedSearch.emit(resource);
          },
          () => { }
        );
      },
      () => { }
    );
  }

  updateSearchMode(mode: SearchMode, event: Event) {
    event.stopPropagation();
    this.searchSettings.stringMatchMode = mode;
    this.vbProperties.setSearchSettings(VBContext.getWorkingProjectCtx(this.projectCtx), this.searchSettings);
  }

  /**
   * AUTOCOMPLETER STUFF
   */
  private inputChangeSubject: Subject<void> = new Subject<void>();

  completerData: string[];
  completerLimit: number = 30;
  completerLimitExceeded: boolean = false;
  isCompleterLoading: boolean;

  private lastCompleterKey: string;

  onAutocompleteChange() {
    this.inputChangeSubject.next();
  }

  getAutocompleterSuggestions() {
    if (this.searchStr.trim() == "" || this.searchStr.length < 3) {
      this.completerData = [];
      return;
    }
    /* ng-autocomplete component emit a inputChanged event even when user press any other key (e.g. move the caret) and the string doesn't change.
    I need to perform this check in order to prevent search repetition */
    if (this.lastCompleterKey == this.searchStr) {
      return;
    }
    this.lastCompleterKey = this.searchStr;

    this.isCompleterLoading = true;
    let langsParam: string[];
    let includeLocales: boolean;
    if (this.searchSettings.restrictLang) {
      langsParam = this.searchSettings.languages;
      includeLocales = this.searchSettings.includeLocales;
    }
    let schemesParam: ARTURIResource[];
    if (this.searchSettings.restrictActiveScheme) {
      schemesParam = this.schemes;
    }
    let clsParam: ARTURIResource;
    if (this.role == RDFResourceRolesEnum.individual && !this.searchSettings.extendToAllIndividuals) {
      clsParam = this.cls;
    }
    let concTreePref = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().conceptTreePreferences;
    this.searchService.searchStringList(this.searchStr, [this.role], this.searchSettings.useLocalName, this.searchSettings.stringMatchMode,
      langsParam, includeLocales, schemesParam, concTreePref.multischemeMode, clsParam, STRequestOptions.getRequestOptions(this.projectCtx)).subscribe(
        (results: string[]) => {
          results.sort();
          this.completerLimitExceeded = results.length > this.completerLimit;
          if (this.completerLimitExceeded) {
            this.completerData = results.slice(0, this.completerLimit);
          } else {
            this.completerData = results;
          }
          this.isCompleterLoading = false;
        }
      );
  }

}