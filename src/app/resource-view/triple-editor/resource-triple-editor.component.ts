import { Component, EventEmitter, Input, Output } from "@angular/core";
import { finalize, Subscription } from "rxjs";
import { RDFFormat } from "src/app/models/RDFFormat";
import { ExportServices } from "src/app/services/export.service";
import { LocalStorageManager } from 'src/app/utils/LocalStorageManager';
import { VBEventHandler } from "src/app/utils/VBEventHandler";
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { ARTResource } from "../../models/ARTResources";
import { ResourcesServices } from "../../services/resources.service";
import { AuthorizationEvaluator } from "../../utils/AuthorizationEvaluator";
import { VBActionsEnum } from "../../utils/VBActions";
import { VBContext } from "../../utils/VBContext";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";

@Component({
  selector: "resource-triple-editor",
  templateUrl: "./resource-triple-editor.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class ResourceTripleEditorComponent {

  @Input() resource: ARTResource;
  @Input() readonly: boolean;
  @Input() pendingValidation: boolean;
  @Output() update = new EventEmitter<ARTResource>();

  editAuthorized: boolean;
  description: string;

  rdfFormats: RDFFormat[];
  format: RDFFormat;

  loading: boolean;

  private eventSubscriptions: Subscription[] = [];

  constructor(private resourcesService: ResourcesServices, private exportService: ExportServices, private basicModals: BasicModalServices, private eventHandler: VBEventHandler) {
    this.eventSubscriptions.push(this.eventHandler.resourceUpdatedEvent.subscribe(
      (resource: ARTResource) => this.onResourceUpdated(resource)
    ));
  }

  ngOnInit() {
    //editor disabled if user has no permission to edit
    this.editAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.resourcesUpdateResourceTriplesDescription);

    let formatNameCookie: string = LocalStorageManager.getItem(LocalStorageManager.RES_VIEW_CODE_FORMAT) || "Turtle";

    this.exportService.getOutputFormats().subscribe(
      formats => {
        this.rdfFormats = formats.filter(f =>
          f.name == "JSON-LD" ||
          f.name == "N-Triples" ||
          f.name == "RDF/XML" ||
          f.name == "TriG" ||
          f.name == "TriX" ||
          f.name == "Turtle"
        );
        //init format according cookie
        for (let f of this.rdfFormats) {
          if (f.name == formatNameCookie) {
            this.format = f;
          }
        }
        if (this.format == null) { //in case it has not been set (e.g. if cookie-stored format was not valid)
          //select Turtle as default
          this.format = this.rdfFormats.find(f => f.name == "Turtle");
        }
        this.initDescription();
      }
    );


  }

  ngOnDestroy() {
    this.eventSubscriptions.forEach(s => s.unsubscribe());
  }

  initDescription() {
    //reinit the descriptions so that when initDescription is invoked after applyChanges, onDescriptionChange is triggered 
    this.description = null;

    this.loading = true;
    this.resourcesService.getOutgoingTriples(this.resource, this.format).pipe(
      finalize(() => { this.loading = false; })
    ).subscribe(
      triples => {
        this.description = triples;
      }
    );
  }

  onFormatChange() {
    LocalStorageManager.setItem(LocalStorageManager.RES_VIEW_CODE_FORMAT, this.format.name);
    this.initDescription();
  }

  isApplyEnabled(): boolean {
    /* encountered too much problems comparing description with a pristine copy (initialized at the initialization of the resource description)
    so, just check that the description is not empty. Allows to apply changes even if description is not changed */
    return this.description != null && this.description.trim() != "";
  }

  applyChanges() {
    if (VBContext.getWorkingProject().isValidationEnabled()) {
      this.basicModals.alertCheckCookie({ key: "RESOURCE_VIEW.CODE_EDITOR.CODE_EDITOR" }, { key: "MESSAGES.CODE_EDITOR_VALIDATION_IGNORED_WARN" },
        LocalStorageManager.WARNING_CODE_CHANGE_VALIDATION).then(
          () => {
            this.applyChangesImpl();
          }
        );
    } else {
      this.applyChangesImpl();
    }
  }

  private applyChangesImpl() {
    this.loading = true;
    this.resourcesService.updateResourceTriplesDescription(this.resource, this.description, this.format).pipe(
      finalize(() => { this.loading = false; })
    ).subscribe({
      next: () => {
        this.update.emit(this.resource);
        this.initDescription();
      },
      error: (err: Error) => {
        if (err.name.endsWith("IllegalArgumentException")) {
          this.basicModals.alert({ key: "STATUS.OPERATION_DENIED" }, { key: "MESSAGES.CANNOT_EDIT_DIFFERENT_RESOURCE_CODE" }, ModalType.warning);
        }
      }
    });
  }

  private onResourceUpdated(resource: ARTResource) {
    if (this.resource.equals(resource)) {
      this.initDescription();
    }
  }

}