import { ChangeDetectorRef, Component, EventEmitter, Input, Output, SimpleChanges, ViewChild } from "@angular/core";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin, Observable, Subscription } from "rxjs";
import { finalize, map } from 'rxjs/operators';
import { GraphModalServices } from 'src/app/graph/modals/graph-modal.service';
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { CollaborationModalServices } from "../../collaboration/collaborationModalService";
import { AccessMethod, ARTNode, ARTPredicateObjects, ARTResource, ARTURIResource, LocalResourcePosition, RDFResourceRolesEnum, RemoteResourcePosition, ResAttribute, ResourcePosition } from "../../models/ARTResources";
import { Issue } from "../../models/Collaboration";
import { VersionInfo } from "../../models/History";
import { Project } from "../../models/Project";
import { NotificationStatus, ProjectUserSettings, ResourceViewProjectSettings } from "../../models/Properties";
import { PropertyFacet, ResViewSection, ValueClickEvent } from "../../models/ResourceView";
import { CollaborationServices } from "../../services/collaboration.service";
import { MetadataRegistryServices } from "../../services/metadata-registry.service";
import { NotificationServices } from "../../services/notification.service";
import { ResourcesServices } from "../../services/resources.service";
import { ResourceViewServices } from "../../services/resource-view.service";
import { VersionsServices } from "../../services/versions.service";
import { AuthorizationEvaluator, CRUDEnum, ResourceViewAuthEvaluator } from "../../utils/AuthorizationEvaluator";
import { Deserializer } from "../../utils/Deserializer";
import { HttpServiceContext } from "../../utils/HttpManager";
import { ResourceUtils } from "../../utils/ResourceUtils";
import { VBActionsEnum } from "../../utils/VBActions";
import { VBCollaboration } from "../../utils/VBCollaboration";
import { ProjectContext, VBContext } from "../../utils/VBContext";
import { VBEventHandler } from "../../utils/VBEventHandler";
import { VBProperties } from "../../utils/VBProperties";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { AbstractResourceView } from "./abstractResourceView";
import { ResourceViewHelper } from './resViewHelper';
import { CollectionsSectionRendererComponent } from './section-renderer/impl/collections-section-renderer.component';
import { MultiActionFunction, MultiActionType, MultipleActionHelper } from "./section-renderer/multipleActionHelper";
import { TimeMachineModalComponent } from "./time-machine/time-machine-modal.component";

@Component({
  selector: "resource-view-editor",
  templateUrl: "./resource-view-editor.component.html",
  host: { class: "vbox" },
  styleUrls: ['./resource-view-editor.component.css'],
  standalone: false
})
export class ResourceViewEditorComponent extends AbstractResourceView {
  @Input() resource: ARTResource;
  @Input() readonly: boolean = false;
  @Input() disabled: boolean = false; //disable all the interaction, also those available in readonly (e.g. rendering, value-filter, inference...)
  @Input() inModal: boolean;
  @Input() projectCtx: ProjectContext;
  @Input() atTime: Date; //java.time.ZonedDateTime

  @Output() valueClick = new EventEmitter<ValueClickEvent>();
  @Output() update = new EventEmitter<ARTResource>(); //(useful to notify resourceViewTabbed that resource is updated)
  @Output() renderingChanged = new EventEmitter<boolean>();

  @ViewChild(CollectionsSectionRendererComponent) collectionSection: CollectionsSectionRendererComponent;

  private viewInitialized: boolean = false; //in order to wait blockDiv to be ready

  private eventSubscriptions: Subscription[] = [];

  unauthorizedResource: boolean = false; //true if user has no capabilities for accessing the current resource (capability for invoking getResourceView)
  unknownHost: boolean = false; //tells if the resource view of the current resource failed to be fetched due to a UnknownHostException
  unexistingResource: boolean = false; //tells if the requested resource does not exist (empty description)

  resourcePosition: ResourcePosition;
  resourcePositionDetails: string;
  localCurrentProject: boolean; //in case of local resource position, tells if the resource belongs to the current project (local resource could also belong to local project which is not the current accessed)
  accessMethod: AccessMethod;
  AccessMethod = AccessMethod;

  //when an external resource is not found, it is possible to specify a different position for resolving it
  availablePosition: ResourcePosition[];
  forcedPosition: ResourcePosition; //position forced by user (if more positions are available)

  //sections
  private resViewResponse: any = null; //to store the getResourceView response and avoid to repeat the request when user switches on/off inference
  resViewSections: { [key: string]: ARTPredicateObjects[] } = {
    [ResViewSection.broaders]: null,
    [ResViewSection.classaxioms]: null,
    [ResViewSection.collections]: null,
    [ResViewSection.constituents]: null,
    [ResViewSection.datatypeDefinitions]: null,
    [ResViewSection.denotations]: null,
    [ResViewSection.disjointProperties]: null,
    [ResViewSection.domains]: null,
    [ResViewSection.equivalentProperties]: null,
    [ResViewSection.evokedLexicalConcepts]: null,
    [ResViewSection.facets]: null,
    [ResViewSection.formRepresentations]: null,
    [ResViewSection.imports]: null,
    [ResViewSection.labelRelations]: null,
    [ResViewSection.lexicalForms]: null,
    [ResViewSection.lexicalSenses]: null,
    [ResViewSection.lexicalizations]: null,
    [ResViewSection.members]: null,
    [ResViewSection.membersOrdered]: null,
    [ResViewSection.notes]: null,
    [ResViewSection.properties]: null,
    [ResViewSection.ranges]: null,
    [ResViewSection.rdfsMembers]: null,
    [ResViewSection.schemes]: null,
    [ResViewSection.subPropertyChains]: null,
    [ResViewSection.subterms]: null,
    [ResViewSection.superproperties]: null,
    [ResViewSection.topconceptof]: null,
    [ResViewSection.types]: null,
  };
  customSections: ResViewSection[];
  sectionOrder: { [key: string]: number }; //map sections->position

  propertyFacets: PropertyFacet[] = null;

  //top bar buttons

  private showInferredPristine: boolean = false; //useful to decide whether repeat the getResourceView request once the includeInferred changes
  showInferred: boolean = false;

  rendering: boolean = true; //tells if the resource shown inside the sections should be rendered

  valueFilterLangEnabled: boolean;

  collaborationAvailable: boolean = false;
  issuesStruct: { btnClass: string; issues: Issue[] } = {
    btnClass: "", issues: null
  };

  //time machine/versioning
  timeActionsEnabled: boolean = false; //tells if the "clock" icon (for versioning and time machine) should be visible 
  //hidden if the @Input() projectCtx or atTime != null, namely RV showing external resource, or showing a resource at a different time)
  timeMachineAvailable: boolean = false; //tells if the conditions for the time machine are satisfied (history enabled)
  versionList: VersionInfo[];
  activeVersion: VersionInfo;

  notificationsAvailable: boolean = false;
  isWatching: boolean;

  isGraphAuthorized: boolean;
  isOpenDataGraphEnabled: boolean;
  isOpenModelGraphEnabled: boolean;

  settingsAvailable: boolean = true;

  promptedAddress: string;

  loading: boolean;

  private pendingGetResViewSubscription: Subscription; //for unsubscribing from pending RV initialization in case input resource changes

  constructor(resViewService: ResourceViewServices, modalService: NgbModal,
    private versionService: VersionsServices, private resourcesService: ResourcesServices, private collaborationService: CollaborationServices,
    private metadataRegistryService: MetadataRegistryServices, private notificationsService: NotificationServices,
    private eventHandler: VBEventHandler, private vbProp: VBProperties, private vbCollaboration: VBCollaboration,
    private basicModals: BasicModalServices, private graphModals: GraphModalServices, private collabModals: CollaborationModalServices,
    private changeDetectorRef: ChangeDetectorRef) {
    super(resViewService, modalService);
    this.eventSubscriptions.push(this.eventHandler.resourceRenamedEvent.subscribe(
      (data: any) => this.onResourceRenamed(data.oldResource, data.newResource)
    ));
    this.eventSubscriptions.push(this.eventHandler.resourceDeprecatedEvent.subscribe(
      (resource: ARTResource) => this.onResourceUpdated(resource)
    ));
    this.eventSubscriptions.push(this.eventHandler.collaborationSystemStatusChanged.subscribe(
      () => this.onCollaborationSystemStatusChange()
    ));
    this.eventSubscriptions.push(this.eventHandler.notificationStatusChangedEvent.subscribe(
      () => this.initNotificationsAvailable()
    ));
    this.eventSubscriptions.push(this.eventHandler.resourceUpdatedEvent.subscribe(
      (resource: ARTResource) => this.onResourceUpdated(resource)
    ));
  }

  ngOnChanges(changes: SimpleChanges) {
    let projPref: ProjectUserSettings = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences();
    this.showInferred = projPref.resViewPreferences.inference;
    this.rendering = projPref.resViewPreferences.rendering;
    this.renderingChanged.emit(this.rendering);
    this.valueFilterLangEnabled = projPref.filterValueLang.enabled;

    this.isGraphAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.graphRead);
    this.isOpenDataGraphEnabled = this.isGraphAuthorized;
    this.isOpenModelGraphEnabled = this.isGraphAuthorized && this.resource.getRole() == RDFResourceRolesEnum.cls && this.resource instanceof ARTURIResource;

    if (changes['resource'] && changes['resource'].currentValue) {
      //if not the first change, avoid to refresh res view if resource is not changed
      if (!changes['resource'].firstChange) {
        let prevRes: ARTResource = changes['resource'].previousValue;
        if (prevRes.equals(this.resource)) { //res not changed
          return;
        } else {
          this.forcedPosition = null;
        }
      }
      if (this.viewInitialized) {
        this.buildResourceView(this.resource);//refresh resource view when Input resource changes
      }
    }
    if (changes['atTime'] && changes['atTime'].currentValue && !changes['atTime'].firstChange) {
      this.buildResourceView(this.resource);
    }
  }

  ngOnInit() {
    this.activeVersion = VBContext.getContextVersion(); //set globally from Versioning page
    this.readonly = this.readonly || (this.activeVersion != null || HttpServiceContext.getContextVersion() != null) || this.atTime != null; //if the RV is working on an old dump version, disable the updates

    this.initVersions();
  }

  ngAfterViewInit() {
    this.viewInitialized = true;
    this.buildResourceView(this.resource);
  }

  ngOnDestroy() {
    this.eventSubscriptions.forEach(s => s.unsubscribe());
    this.pendingGetResViewSubscription?.unsubscribe();
  }

  /**
   * Perform the getResourceView request and build the resource view.
   * Called when
   * - a resource is selected for the first time in a tree
   * - the selected resource changes (not in tab mode where every resource selected opens a new tab,
   *   but in splitted mode when the RV is the same and simply changes the selected resource to describe)
   * - the resource is renamed, so it needs to refresh
   * - some section has performed a change and emits an update event (which invokes this method, see template)
   */
  public buildResourceView(res: ARTResource) {

    this.pendingGetResViewSubscription?.unsubscribe();

    this.unauthorizedResource = false;
    if (this.resource.getRole() != RDFResourceRolesEnum.mention && this.resource.getRole() != RDFResourceRolesEnum.individual) {
      this.unauthorizedResource = !AuthorizationEvaluator.isAuthorized(VBActionsEnum.resourceViewGetResourceView, this.resource);
    }
    this.disabled = this.unauthorizedResource;
    if (this.unauthorizedResource) return;

    this.showInferredPristine = this.showInferred;
    if (this.activeVersion != null) {
      HttpServiceContext.setContextVersion(this.activeVersion); //set temprorarly version
    }
    let getResViewFn: Observable<any> = this.resViewService.getResourceView(res, this.showInferred, this.forcedPosition);
    if (this.atTime != null) {
      getResViewFn = this.resViewService.getResourceViewAtTime(res, this.atTime); //cast safe since atTime is provided only if res is IRI
    }

    //reset all...
    for (let s of Object.keys(this.resViewSections)) {
      this.resViewSections[s] = null;
    }
    // this.resourcePosition = null;
    // this.accessMethod = null;
    // this.localCurrentProject = false;

    this.loading = true;
    this.pendingGetResViewSubscription = getResViewFn.pipe(
      finalize(() => {
        this.loading = false;
      })
    ).subscribe({
      next: stResp => {
        HttpServiceContext.removeContextVersion();
        this.resViewResponse = stResp;

        this.resource = Deserializer.createRDFResource(this.resViewResponse.resource);
        this.update.emit(this.resource);

        this.initResourcePosition();
        this.fillSections();
        this.fillLazySections();
        this.unknownHost = false;

        this.timeActionsEnabled = !this.inModal && this.projectCtx == null && this.atTime == null && this.resource.getAdditionalProperty(ResAttribute.EXPLICIT);
        this.settingsAvailable = !this.inModal; //settings available only if RV is shown in data page (not in modal)
        this.initNotificationsAvailable();
      },
      error: (err: Error) => {
        if (err.name.endsWith("UnknownHostException")) {
          this.unknownHost = true;
        }
      }
    });


    this.updateCollaborationStatus();
    if (this.collaborationAvailable) {
      this.initCollaboration();
    }

    this.changeDetectorRef.detectChanges();
  }

  private initResourcePosition() {
    this.resourcePosition = ResourcePosition.deserialize(this.resource.getAdditionalProperty(ResAttribute.RESOURCE_POSITION));

    if (
      this.resource.getRole() == RDFResourceRolesEnum.mention && //mention is also the default role (assigned when nature is empty)
      !this.resourcePosition.isLocal() //so for setting readonly to true, check also if the res position is not local
    ) {
      this.readonly = true;
    }

    this.accessMethod = this.resource.getAdditionalProperty(ResAttribute.ACCESS_METHOD);

    if (this.resourcePosition instanceof LocalResourcePosition) {
      this.localCurrentProject = this.resourcePosition.project == VBContext.getWorkingProject().getName();
      this.resourcePositionDetails = this.resourcePosition.project;
    } else if (this.resourcePosition instanceof RemoteResourcePosition) {
      this.metadataRegistryService.getDatasetMetadata(this.resourcePosition.datasetMetadata).subscribe(
        metadata => {
          if (metadata.title != null) {
            this.resourcePositionDetails = metadata.title + ", " + metadata.uriSpace;
          } else {
            this.resourcePositionDetails = metadata.uriSpace;
          }
        }
      );
    } //else is unknown => the UI gives the possibility to discover the dataset
  }

  /**
   * Fill all the sections of the RV. This not requires that the RV description is fetched again from server,
   * in fact if the user switches on/off the value filter, there's no need to perform a new request.
   */
  private fillSections() {
    if (VBContext.getWorkingProject().isValidationEnabled()) {
      this.pendingValidation.emit(ResourceUtils.isResourceInStaging(this.resource));
    }

    // this.resourcePosition = ResourcePosition.deserialize(this.resource.getAdditionalProperty(ResAttribute.RESOURCE_POSITION));
    // if (
    //   this.resource.getRole() == RDFResourceRolesEnum.mention && //mention is also the default role (assigned when nature is empty)
    //   !this.resourcePosition.isLocal() //so for setting readonly to true, check also if the res position is not local
    // ) {
    //   this.readonly = true;
    // }

    //time machine available on local IRI resource and in projects with history enabled (no need to check projectCtx, since if it is provided, clock button is already hidden via timeActionsEnabled)
    this.timeMachineAvailable = this.resource.isURIResource() && this.resourcePosition.isLocal() &&
      VBContext.getWorkingProject().isHistoryEnabled() &&
      VBContext.getWorkingProjectCtx().getProjectSettings().timeMachineEnabled;

    // if (this.resourcePosition instanceof LocalResourcePosition) {
    //   this.resourcePositionLocalProj = this.resourcePosition.project == VBContext.getWorkingProject().getName();
    //   this.resourcePositionDetails = this.resourcePosition.project;
    // } else if (this.resourcePosition instanceof RemoteResourcePosition) {
    //   this.metadataRegistryService.getDatasetMetadata(this.resourcePosition.datasetMetadata).subscribe(
    //     metadata => {
    //       if (metadata.title != null) {
    //         this.resourcePositionDetails = metadata.title + ", " + metadata.uriSpace;
    //       } else {
    //         this.resourcePositionDetails = metadata.uriSpace;
    //       }
    //     }
    //   );
    // } //else is unknown => the UI gives the possibility to discover the dataset

    //list of section filtered out for the role of the current described resource
    let sectionFilter: ResViewSection[] = VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPartitionFilter[this.resource.getRole()];
    if (sectionFilter == null) {
      sectionFilter = []; //to prevent error later (in sectionFilter.indexOf(section))
    }

    this.resViewSections[ResViewSection.broaders] = this.initSection(ResViewSection.broaders, sectionFilter, true);
    this.resViewSections[ResViewSection.classaxioms] = this.initSection(ResViewSection.classaxioms, sectionFilter, true);
    this.resViewSections[ResViewSection.collections] = this.initSection(ResViewSection.collections, sectionFilter, true);
    this.resViewSections[ResViewSection.constituents] = this.initSection(ResViewSection.constituents, sectionFilter, false); //ordered server-side
    this.resViewSections[ResViewSection.datatypeDefinitions] = this.initSection(ResViewSection.datatypeDefinitions, sectionFilter, true);
    this.resViewSections[ResViewSection.denotations] = this.initSection(ResViewSection.denotations, sectionFilter, true);
    this.resViewSections[ResViewSection.disjointProperties] = this.initSection(ResViewSection.disjointProperties, sectionFilter, true);
    this.resViewSections[ResViewSection.domains] = this.initSection(ResViewSection.domains, sectionFilter, true);
    this.resViewSections[ResViewSection.equivalentProperties] = this.initSection(ResViewSection.equivalentProperties, sectionFilter, true);
    this.resViewSections[ResViewSection.evokedLexicalConcepts] = this.initSection(ResViewSection.evokedLexicalConcepts, sectionFilter, true);
    this.resViewSections[ResViewSection.facets] = this.initFacetsSection(ResViewSection.facets, sectionFilter);//dedicated initialization
    this.resViewSections[ResViewSection.formRepresentations] = this.initSection(ResViewSection.formRepresentations, sectionFilter, true);
    this.resViewSections[ResViewSection.imports] = this.initSection(ResViewSection.imports, sectionFilter, true);
    this.resViewSections[ResViewSection.labelRelations] = this.initSection(ResViewSection.labelRelations, sectionFilter, true);
    this.resViewSections[ResViewSection.lexicalForms] = this.initSection(ResViewSection.lexicalForms, sectionFilter, true);
    this.resViewSections[ResViewSection.lexicalSenses] = this.initSection(ResViewSection.lexicalSenses, sectionFilter, true);
    this.resViewSections[ResViewSection.lexicalizations] = this.initSection(ResViewSection.lexicalizations, sectionFilter, false); //the sort is performed in the section according the language
    this.resViewSections[ResViewSection.members] = this.initSection(ResViewSection.members, sectionFilter, true);
    this.resViewSections[ResViewSection.membersOrdered] = this.initOrderedMembersSection(ResViewSection.membersOrdered, sectionFilter);//dedicated initialization
    this.resViewSections[ResViewSection.notes] = this.initSection(ResViewSection.notes, sectionFilter, true);
    this.resViewSections[ResViewSection.properties] = this.initSection(ResViewSection.properties, sectionFilter, true);
    this.resViewSections[ResViewSection.ranges] = this.initSection(ResViewSection.ranges, sectionFilter, true);
    this.resViewSections[ResViewSection.rdfsMembers] = this.initSection(ResViewSection.rdfsMembers, sectionFilter, false); //ordered server-side
    this.resViewSections[ResViewSection.schemes] = this.initSection(ResViewSection.schemes, sectionFilter, true);
    this.resViewSections[ResViewSection.subterms] = this.initSection(ResViewSection.subterms, sectionFilter, true);
    this.resViewSections[ResViewSection.subPropertyChains] = this.initSection(ResViewSection.subPropertyChains, sectionFilter, true);
    this.resViewSections[ResViewSection.superproperties] = this.initSection(ResViewSection.superproperties, sectionFilter, true);
    this.resViewSections[ResViewSection.topconceptof] = this.initSection(ResViewSection.topconceptof, sectionFilter, true);
    this.resViewSections[ResViewSection.types] = this.initSection(ResViewSection.types, sectionFilter, true);

    let rvSettings: ResourceViewProjectSettings = VBContext.getWorkingProjectCtx().getProjectSettings().resourceView;
    this.sectionOrder = {};
    if (rvSettings.templates) {
      let templateForRole: ResViewSection[] = rvSettings.templates[this.resource.getRole()];
      if (templateForRole != null) {
        for (let i = 0; i < templateForRole.length; i++) {
          this.sectionOrder[templateForRole[i]] = i;
        }
      }
    }
    if (Object.keys(this.sectionOrder).length == 0) { //template not provided => init a default
      [
        ResViewSection.types, ResViewSection.classaxioms, ResViewSection.topconceptof, ResViewSection.schemes,
        ResViewSection.broaders, ResViewSection.superproperties, ResViewSection.equivalentProperties,
        ResViewSection.disjointProperties, ResViewSection.subPropertyChains, ResViewSection.constituents,
        ResViewSection.subterms, ResViewSection.domains, ResViewSection.ranges, ResViewSection.facets,
        ResViewSection.datatypeDefinitions, ResViewSection.lexicalizations, ResViewSection.lexicalForms,
        ResViewSection.lexicalSenses, ResViewSection.denotations, ResViewSection.evokedLexicalConcepts,
        ResViewSection.notes, ResViewSection.members, ResViewSection.membersOrdered, ResViewSection.labelRelations,
        ResViewSection.formRepresentations, ResViewSection.imports,
        ResViewSection.collections, ResViewSection.rdfsMembers, ResViewSection.properties
      ].forEach((section, idx) => {
        this.sectionOrder[section] = idx;
      });
    }
    if (rvSettings.customSections) {
      this.customSections = Object.keys(rvSettings.customSections) as ResViewSection[];
      this.customSections.forEach(section => {
        this.resViewSections[section] = this.initSection(section as ResViewSection, sectionFilter, true);
      });
    }

    this.resolveForeignURI();

    if (
      this.resource.getRole() == RDFResourceRolesEnum.mention &&
      //these sections are always returned, even when resource is not defined, so I need to check also if length == 0
      (!this.resViewResponse[ResViewSection.lexicalizations] || this.resViewResponse[ResViewSection.lexicalizations].length == 0) &&
      (!this.resViewResponse[ResViewSection.properties] || this.resViewResponse[ResViewSection.properties].length == 0) &&
      (!this.resViewResponse[ResViewSection.types] || this.resViewResponse[ResViewSection.types].length == 0) &&
      //sections optional
      !this.resViewResponse[ResViewSection.broaders] &&
      !this.resViewResponse[ResViewSection.classaxioms] &&
      !this.resViewResponse[ResViewSection.constituents] &&
      !this.resViewResponse[ResViewSection.datatypeDefinitions] &&
      !this.resViewResponse[ResViewSection.denotations] &&
      !this.resViewResponse[ResViewSection.disjointProperties] &&
      !this.resViewResponse[ResViewSection.domains] &&
      !this.resViewResponse[ResViewSection.equivalentProperties] &&
      !this.resViewResponse[ResViewSection.evokedLexicalConcepts] &&
      !this.resViewResponse[ResViewSection.facets] &&
      !this.resViewResponse[ResViewSection.formRepresentations] &&
      !this.resViewResponse[ResViewSection.imports] &&
      !this.resViewResponse[ResViewSection.labelRelations] &&
      !this.resViewResponse[ResViewSection.lexicalForms] &&
      !this.resViewResponse[ResViewSection.lexicalSenses] &&
      !this.resViewResponse[ResViewSection.members] &&
      !this.resViewResponse[ResViewSection.membersOrdered] &&
      !this.resViewResponse[ResViewSection.notes] &&
      !this.resViewResponse[ResViewSection.ranges] &&
      !this.resViewResponse[ResViewSection.rdfsMembers] &&
      !this.resViewResponse[ResViewSection.subPropertyChains] &&
      !this.resViewResponse[ResViewSection.schemes] &&
      !this.resViewResponse[ResViewSection.subterms] &&
      !this.resViewResponse[ResViewSection.superproperties] &&
      !this.resViewResponse[ResViewSection.topconceptof]
    ) {
      this.unexistingResource = true;
      if (this.resourcePosition instanceof LocalResourcePosition && this.resourcePosition.project != VBContext.getWorkingProject().getName() && this.resource instanceof ARTURIResource) {
        this.metadataRegistryService.findDatasets(this.resource).subscribe(
          positions => {
            this.availablePosition = positions;
          }
        );
      }
    } else {
      this.unexistingResource = false;
    }
  }

  /**
   * Initializes the poList of a section:
   * - verifies if the section should be showed
   * - deserializes the response
   * - filters (eventually) the object list
   * - sorts the object list
   * @param section 
   * @param sectionFilter 
   * @param sort 
   */
  private initSection(section: ResViewSection, sectionFilter: ResViewSection[], sort: boolean): ARTPredicateObjects[] {
    //The poList is parsed only if the section is present in the response.
    let sectionJson: any = this.resViewResponse[section];
    if (sectionJson != null) {
      if (Array.isArray(sectionJson)) {
        let poList = Deserializer.createPredicateObjectsList(sectionJson);

        //if the there is a value under validation emit a pendingValidation event
        if (VBContext.getWorkingProject().isValidationEnabled()) {
          if (poList.some(po => po.getObjects().some(obj => ResourceUtils.isTripleInStaging(obj)))) {
            this.pendingValidation.emit(true);
          }
        }

        /** If:
         * - the Read is authorized
         * - the section is not filtered (in the preference)
         * it is processed and returned
         */
        if (ResourceViewAuthEvaluator.isAuthorized(section, CRUDEnum.R, this.resource) && sectionFilter.indexOf(section) == -1) {
          this.filterPredObjList(poList);
          if (sort) {
            ResourceViewHelper.sortObjects(poList, this.rendering);
          }
          ResourceViewHelper.resolveImg(poList);
          return poList;
        } else {
          return null;
        }
      } else { // lazy section
        if (ResourceViewAuthEvaluator.isAuthorized(section, CRUDEnum.R, this.resource) && sectionFilter.indexOf(section) == -1) {
          return [];
        } else {
          return null;
        }
      }
    } else {
      return null;
    }
  }

  private fillLazySections() {
    //currently collections is the only lazy section
    if (this.collectionSection) {
      this.collectionSection.refreshLazyData();
    }
  }

  /**
   * The response of the facets section is different from the others, so initializes it in a dedicated method.
   * 
   * @param section this could be omitted since this method is used just for facets section, 
   *  but it is provided anyway in order to be aligned with the initSection method
   * @param sectionFilter 
   */
  private initFacetsSection(section: ResViewSection, sectionFilter: ResViewSection[]): ARTPredicateObjects[] {
    let poList: ARTPredicateObjects[]; //poList of inverseof 
    let facetsSectionJson: any = this.resViewResponse[section];
    if (
      facetsSectionJson != null &&
      ResourceViewAuthEvaluator.isAuthorized(section, CRUDEnum.R, this.resource) &&
      sectionFilter.indexOf(section) == -1
    ) {
      this.propertyFacets = [];
      for (let facetName in facetsSectionJson) {
        if (facetName == "inverseOf") continue;
        this.propertyFacets.push({
          name: facetName,
          value: facetsSectionJson[facetName].value,
          explicit: facetsSectionJson[facetName].explicit
        });
      }
      //parse inverseOf section in facets
      poList = Deserializer.createPredicateObjectsList(facetsSectionJson.inverseOf);
      this.filterPredObjList(poList);
      ResourceViewHelper.sortObjects(poList, this.rendering);
    }
    return poList;
  }

  /**
   * The response of the membersOrdered section is slightly different from the others, so initializes it in a dedicated method.
   * 
   * @param section this could be omitted since this method is used just for membersOrdered section,
   *  but it is provided anyway in order to be aligned with the initSection method
   * @param sectionFilter
   */
  private initOrderedMembersSection(section: ResViewSection, sectionFilter: ResViewSection[]): ARTPredicateObjects[] {
    let poList: ARTPredicateObjects[];
    let sectionJson: any = this.resViewResponse[section];
    if (
      sectionJson != null &&
      ResourceViewAuthEvaluator.isAuthorized(section, CRUDEnum.R, this.resource) &&
      sectionFilter.indexOf(ResViewSection.membersOrdered) == -1
    ) {
      poList = Deserializer.createPredicateObjectsList(sectionJson);
      //the "explicit" attribute for the collection members is not declared => set the attribute based on the explicit of the collection
      for (const po of poList) { //for each pred-obj-list
        let objects = po.getObjects();
        for (const obj of objects) { //for each collection (member list, should be just 1)
          if (obj.getAdditionalProperty(ResAttribute.EXPLICIT)) { //set member explicit only if collection is explicit
            let members: ARTResource[] = obj.getAdditionalProperty(ResAttribute.MEMBERS);
            for (const m of members) {
              m.setAdditionalProperty(ResAttribute.EXPLICIT, true);
            }
          }
        }
      }
      this.filterPredObjList(poList);
      ResourceViewHelper.sortObjects(poList, this.rendering);
    }
    return poList;
  }

  private filterPredObjList(predObjList: ARTPredicateObjects[]) {
    ResourceViewHelper.filterInferredFromPredObjList(predObjList, this.projectCtx);
    ResourceViewHelper.filterValueLanguageFromPrefObjList(predObjList, this.projectCtx);
    ResourceViewHelper.filterDeprecatedValues(predObjList, this.projectCtx);
  }


  /**
   * Resolves the foreign IRIs of all sections.
   * This method iterates over the values (objects in the sections' poList) and in case there are foreign IRIs (IRI with role mention),
   * tries to resolve them. The foreign IRIs could represent:
   * - resources in local projects (position local)
   * - resources in remote projects (position remote; currently left unresolved)
   * - URL images to display (position unknown)
   * @param pol 
   */
  private resolveForeignURI() {
    if (!AuthorizationEvaluator.isAuthorized(VBActionsEnum.resourcesGetResourcePosition)) {
      return; //if not authorized to get the resource position, do not allow foreign uri resolution
    }

    let pol: ARTPredicateObjects[] = [];
    for (let section in this.resViewSections) {
      let predObjList = this.resViewSections[section];
      if (predObjList && predObjList.length > 0) {
        predObjList.forEach(po => pol.push(po));
      }
    }

    //collect foreign IRIs, namely IRI values with role mention
    let foreignResources: ARTURIResource[] = [];
    pol.forEach(po => {
      po.getObjects().forEach(o => {
        if (o instanceof ARTURIResource && o.getRole() == RDFResourceRolesEnum.mention) { //if it is a URI mention
          foreignResources.push(o);
        }
      });
    });
    if (foreignResources.length > 0) {
      //retrieve the position of the foreign resources
      this.resourcesService.getResourcesPosition(foreignResources).subscribe(
        positionMap => {
          //collect the foreign resources grouped by project
          // let candidateImageURLs: string[] = []; //list of foreign IRIs which position is unknown
          let localProjectResourcesMap: { [projectName: string]: string[] } = {}; //for each local project -> list of IRI to resolve
          for (let resIri in positionMap) {
            let position = positionMap[resIri];
            if (position.isLocal()) {
              let projectName = (position as LocalResourcePosition).project;
              if (projectName != VBContext.getWorkingProject().getName()) {
                if (localProjectResourcesMap[projectName] == null) {
                  localProjectResourcesMap[projectName] = [resIri];
                } else {
                  localProjectResourcesMap[projectName].push(resIri);
                }
              }
            } else if (position.isRemote()) {
              //at the moment nothing to do
            }
          }
          /**
           * Resolves the local project foreign URIs
           */
          //prepare the service invocations for resolving the foreign URIs. One invocation for each project
          let resolveResourcesFn: Observable<void>[] = [];
          let resolvedResources: ARTURIResource[] = [];
          for (let projectName in localProjectResourcesMap) {
            let resources: ARTURIResource[] = localProjectResourcesMap[projectName].map(r => new ARTURIResource(r));
            HttpServiceContext.setContextProject(new Project(projectName));
            resolveResourcesFn.push(
              this.resourcesService.getResourcesInfo(resources).pipe(
                finalize(() => HttpServiceContext.removeContextProject()),
                map(resources => {
                  resolvedResources = resolvedResources.concat(resources);
                })
              )
            );
          }
          //invoke all the services, then (the resolvedResources array is filled) search and replace the values in the poList
          forkJoin(resolveResourcesFn).subscribe(
            () => {
              pol.forEach(po => {
                po.getObjects().forEach((o: ARTNode, index: number, array: ARTNode[]) => {
                  if (o instanceof ARTURIResource && o.getRole() == RDFResourceRolesEnum.mention) {
                    let resolved: ARTURIResource = resolvedResources.find(r => r.equals(o)); //search the values among the resolved ones
                    if (resolved != null) { //if found, replace
                      //replace the entire value (instead of just the attributes) for triggering the ngOnChanges in the rdf-resource component
                      let resToReplace: ARTURIResource = array[index].clone() as ARTURIResource;
                      resToReplace.setShow(resolved.getShow());
                      resToReplace.setRole(resolved.getRole());
                      resToReplace.setNature(resolved.getNature());
                      array[index] = resToReplace; //replace
                    }
                  }
                });
              });
            }
          );
        }
      );
    }
  }

  /**
   * HEADING BUTTONS HANDLERS
   */

  switchInferred() {
    this.showInferred = !this.showInferred;
    let pref = VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPreferences;
    pref.inference = this.showInferred;
    this.vbProp.setResourceViewPreferences(pref).subscribe(
      () => {
        if (!this.showInferredPristine) { //resource view has been initialized with showInferred to false, so repeat the request
          this.buildResourceView(this.resource);
        } else { //resource view has been initialized with showInferred to true, so there's no need to repeat the request
          this.fillSections();
        }
      }
    );
  }

  switchRendering() {
    this.rendering = !this.rendering;
    let pref = VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPreferences;
    pref.rendering = this.rendering;
    this.vbProp.setResourceViewPreferences(pref).subscribe(
      () => {
        this.renderingChanged.emit(this.rendering); //useful to reflect the rendering of the resource in the tab (in case the RV is in a tab)
      }
    );
  }

  switchValueFilterLang() {
    this.valueFilterLangEnabled = !this.valueFilterLangEnabled;
    //update the preference
    let valueFilterLangPref = VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().filterValueLang;
    valueFilterLangPref.enabled = this.valueFilterLangEnabled;
    this.vbProp.setValueFilterLanguages(valueFilterLangPref).subscribe(
      () => {
        //update the RV
        this.fillSections();
      }
    );
  }

  openDataGraph() {
    this.graphModals.openDataGraph(this.resource, this.rendering);
  }

  openModelGraph() {
    this.graphModals.openModelGraph(this.resource, this.rendering);
  }

  //TIME ACTIONS

  initVersions() {
    this.versionList = VBContext.getWorkingProjectCtx(this.projectCtx).resViewCtx.versions;
    if (this.versionList == null && AuthorizationEvaluator.isAuthorized(VBActionsEnum.versionsGetVersions)) {
      this.versionService.getVersions().subscribe(
        versions => {
          this.versionList = versions;
          VBContext.getWorkingProjectCtx(this.projectCtx).resViewCtx.versions = this.versionList; //cache/share the version into the RV ctx
          //update the active version
          if (this.activeVersion != null) {
            this.activeVersion = this.versionList.find(v => v.versionId == this.activeVersion.versionId);
          }
        }
      );
    }
  }

  switchToVersion(version?: VersionInfo) {
    if (this.activeVersion != version) {
      this.activeVersion = version;
      this.buildResourceView(this.resource);
    }
    //resView is readonly if one of the temp version and the context version are not null
    this.readonly = this.activeVersion != null || VBContext.getContextVersion() != null;
  }

  timeMachine() {
    const modalRef: NgbModalRef = this.modalService.open(TimeMachineModalComponent, new ModalOptions('full'));
    modalRef.componentInstance.resource = this.resource;
  }

  //NOTIFICATIONS HANDLERS
  private initNotificationsAvailable() {
    //notifications available only if the ResView is about an IRI of the current project and if notifications are activated
    this.notificationsAvailable = this.projectCtx == null && this.resource instanceof ARTURIResource && this.resource.getAdditionalProperty(ResAttribute.EXPLICIT) &&
      VBContext.getWorkingProjectCtx(this.projectCtx).getProjectPreferences().notificationStatus != NotificationStatus.no_notifications;
    if (this.notificationsAvailable) { //in case notification are active => init the status of watching
      this.initNotificationsStatus();
    }
  }
  private initNotificationsStatus() {
    this.notificationsService.isWatching(this.resource).subscribe(
      isWatching => {
        this.isWatching = isWatching;
      }
    );
  }
  changeNotificationStatus() {
    let notificationFn: Observable<void>;
    if (this.isWatching) {
      notificationFn = this.notificationsService.stopWatching(this.resource);
    } else {
      notificationFn = this.notificationsService.startWatching(this.resource);
    }
    notificationFn.subscribe(
      () => {
        this.isWatching = !this.isWatching;
      }
    );
  }

  assertInferredStatements() {
    let assertFn: MultiActionFunction[] = [];
    for (let p in this.resViewSections) {
      if (p == ResViewSection.lexicalizations) continue; //lexicalizations not assertable
      let poList: ARTPredicateObjects[] = this.resViewSections[p];
      if (poList == null) continue; //predicate object list null for the current resource (section not foreseen for the resource role)
      poList.forEach((predObjs: ARTPredicateObjects) => {
        predObjs.getObjects().forEach((obj: ARTNode) => {
          if (ResourceUtils.isTripleInferred(obj)) {
            assertFn.push({
              function: this.resourcesService.addValue(this.resource, predObjs.getPredicate(), obj),
              value: obj
            });
          }
        });
      });
    }

    if (assertFn.length == 0) {
      this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "MESSAGES.NO_INFERRED_STATEMENTS_TO_ASSERT" }, ModalType.warning);
    } else {
      let onComplete = () => { //when the assert of all the statements is completed, stop the loading and rebuild the ResView
        this.loading = false;
        this.buildResourceView(this.resource);
      };
      this.loading = true;
      MultipleActionHelper.executeActions(assertFn, MultiActionType.addition, this.basicModals, null, null, onComplete);
    }
  }

  // COLLABORATION SYSTEM HANDLERS

  private initCollaboration() {
    this.collaborationService.listIssuesAssignedToResource(this.resource).subscribe({
      next: issues => {
        this.issuesStruct = {
          btnClass: "",
          issues: null
        };
        if (issues.length > 0) {
          /* Iterate over the issues and add the classes for styling the button of the collaboration system menu
           * - black (no class applied) if there are no issues
           * - green (.done-issues) if there are only closed issues
           * - blue (.todo-issues) if there is at least one open issue
           * - orange (.in-progress-issues) if there is at least in-progress issues and no todo
           * - red (.generic-issues) if there are at least one issue but no status is known
           */
          for (let i of issues) {
            if (i.getStatusId() == '10000') {
              this.issuesStruct.btnClass = "todo-issues";
              break;
            } else if (i.getStatusId() == '3') {
              this.issuesStruct.btnClass = "in-progress-issues";
            } else if (i.getStatusId() == '10001') {
              if (this.issuesStruct.btnClass != "in-progress-issues") {
                this.issuesStruct.btnClass = "done-issues";
              }
            }
          }
          if (this.issuesStruct.btnClass == "") { //there are issue but with statusId not known
            this.issuesStruct.btnClass = "generic-issues";
          }
          this.issuesStruct.issues = issues;
        }
      },
      error: (err: Error) => {
        if (this.collaborationAvailable) {
          this.basicModals.alert({ key: "STATUS.ERROR" }, { key: "MESSAGES.COLLABORATION_SYS_CONFIGURED_BUT_NOT_WORKING" }, ModalType.error, err.stack);
          this.vbCollaboration.setWorking(false);
        }
      }
    });
  }

  createIssue() {
    this.collabModals.createIssue().then(
      formMap => {
        this.loading = true;
        this.collaborationService.createIssue(this.resource, formMap).pipe(
          finalize(() => { this.loading = false; })
        ).subscribe(
          () => {
            this.initCollaboration();
          }
        );
      },
      () => { }
    );
  }

  assignToIssue() {
    this.collabModals.openIssueList().then(
      issue => {
        this.loading = true;
        this.collaborationService.assignResourceToIssue(issue.getKey(), this.resource).pipe(
          finalize(() => { this.loading = false; })
        ).subscribe(
          () => {
            this.initCollaboration();
          },
        );
      },
      () => { }
    );
  }

  unassignIssue(issue: Issue) {
    this.collaborationService.removeResourceFromIssue(issue.getId(), this.resource).subscribe( //cast is safe (cs available only for IRI)
      () => {
        this.initCollaboration();
      }
    );
  }

  private onCollaborationSystemStatusChange() {
    this.updateCollaborationStatus();
    if (this.collaborationAvailable) { //status changed, now CS is available => refresh issues lists
      this.initCollaboration();
    }
  }

  private updateCollaborationStatus() {
    this.collaborationAvailable = this.resource instanceof ARTURIResource && this.vbCollaboration.isWorking() && this.vbCollaboration.isActive() && this.projectCtx == null;
  }

  //Status bar

  describeAddress() {
    if (ResourceUtils.testIRI(this.promptedAddress)) {
      this.resource = new ARTURIResource(this.promptedAddress);
      this.buildResourceView(this.resource);
    } else {
      this.basicModals.alert({ key: "STATUS.ERROR" }, { key: "MESSAGES.INVALID_IRI", params: { iri: this.promptedAddress } }, ModalType.warning);
    }
  }

  discoverDataset() {
    this.loading = true;
    this.metadataRegistryService.discoverDataset(this.resource).pipe(
      finalize(() => { this.loading = false; })
    ).subscribe(
      () => {
        this.buildResourceView(this.resource);
      }
    );
  }

  //resource not found - force position

  forcePosition(position: ResourcePosition) {
    this.forcedPosition = position;
    this.buildResourceView(this.resource);
  }

  /**
   * EVENT LISTENERS
   */

  onValueClick(event: ValueClickEvent) {
    this.valueClick.emit(event);
  }

  private onResourceRenamed(oldResource: ARTURIResource, newResource: ARTURIResource) {
    if (this.resource instanceof ARTURIResource) { //rename affect only URIResource
      if (this.resource.equals(oldResource)) {
        (this.resource).setURI(newResource.getURI());
        this.buildResourceView(this.resource); //refresh the resource view in order to update the panel rdf-resource
      }
    }
  }

  private onResourceUpdated(resource: ARTResource) {
    if (this.resource.equals(resource)) {
      this.buildResourceView(this.resource);
    }
  }

}