import { Component, ElementRef, Input, SimpleChanges, ViewChild } from "@angular/core";
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { ToastService } from "src/app/widget/toast/toast.service";
import { ARTBNode, ARTResource, ARTURIResource, ResAttribute } from "../../models/ARTResources";
import { RefactorServices } from "../../services/refactor.service";
import { AuthorizationEvaluator } from "../../utils/AuthorizationEvaluator";
import { ResourceUtils } from "../../utils/ResourceUtils";
import { VBActionsEnum } from "../../utils/VBActions";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";

@Component({
  selector: "resource-rename",
  templateUrl: "./resource-rename.component.html",
  standalone: false
})
export class ResourceRenameComponent {

  @Input() resource: ARTResource;
  @Input() readonly: boolean;

  @ViewChild('localrenameinput') localRenameInput: ElementRef;
  @ViewChild('totalrenameinput') totalRenameInput: ElementRef;

  iriResource: ARTURIResource;
  bNode: ARTBNode;

  renameDisabled: boolean = true;

  localName: string;
  pristineNamespace: string;
  pristineLocalName: string;

  renameLocked = true;
  namespaceLocked = true;

  constructor(private refactorService: RefactorServices, private basicModals: BasicModalServices, private toastService: ToastService) { }

  ngOnInit() {
    this.initResource();
  }

  private initResource() {
    if (this.resource instanceof ARTURIResource) {
      this.iriResource = this.resource;
      this.localName = this.iriResource.getLocalName();
    } else {
      this.bNode = this.resource as ARTBNode;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['resource'] && changes['resource'].currentValue) {
      this.initResource();
      //rename disabled if resource is not explicit || resView is readOnly || user has not the authorization || resource is to validate
      this.renameDisabled = (!this.resource.getAdditionalProperty(ResAttribute.EXPLICIT) || this.readonly ||
        !AuthorizationEvaluator.isAuthorized(VBActionsEnum.refactorChangeResourceUri, this.resource) ||
        ResourceUtils.isResourceInStaging(this.resource));
    }
  }

  /** 
   * Enable and focus the input text to rename the resource 
   */
  startRename() {
    this.localRenameInput.nativeElement.focus();
    this.renameLocked = false;
  }

  /**
   * Cancel the renaming of the resource and restore the original UI
   */
  cancelRename() {
    //here I can cast resource to ARTURIResource (since rename is enabled only for ARTURIResource and not for ARTBNode)
    this.localName = (this.resource as ARTURIResource).getLocalName(); //restore the local name
    this.renameLocked = true;
    this.namespaceLocked = true;
  }

  blockNamespace() {
    this.namespaceLocked = !this.namespaceLocked;
  }

  /**
   * Apply the renaming of the resource and restore the original UI
   */
  renameResource() {
    let newUri: string;
    //here I can cast resource to ARTURIResource (since rename is enabled only for ARTURIResource and not for ARTBNode)
    if (this.namespaceLocked) { //just the namespace has changed
      if (this.localName.trim() == "") {
        this.basicModals.alert({ key: "STATUS.INVALID_VALUE" }, { key: "MESSAGES.INVALID_LOCAL_NAME" }, ModalType.warning);
        this.cancelRename();
        return;
      }
      newUri = (this.resource as ARTURIResource).getBaseURI() + this.localName;
    } else { //complete renaming (ns + localName)
      newUri = this.totalRenameInput.nativeElement.value;
      if (newUri.trim() == "") {
        this.basicModals.alert({ key: "STATUS.INVALID_VALUE" }, { key: "MESSAGES.INVALID_IRI", params: { iri: newUri } }, ModalType.warning);
        this.cancelRename();
        return;
      }
    }
    if ((this.resource as ARTURIResource).getURI() != newUri) { //if the uri has changed
      let toRes = new ARTURIResource(newUri);
      this.refactorService.changeResourceURI(this.resource as ARTURIResource, toRes).subscribe({
        next: () => {
          this.renameLocked = true;
          this.namespaceLocked = true;
        },
        error: () => {
          this.cancelRename();
        }
      });
    } else {
      this.cancelRename();
    }
  }

  /**
   * Since URI of URIResource is splitted in namespace and localName, this method allows
   * to copy the complete URI in the clipboard.
   */
  copyToClipboard() {
    //this method is called by clicking on a button visible only if the resource is a URIResource, so cast is safe
    navigator.clipboard.writeText((this.resource as ARTURIResource).getURI()).then(
      () => {
        this.toastService.show({ key: "STATUS.OPERATION_DONE" }, { key: "MESSAGES.RESOURCE_URI_COPIED_IN_CLIPBOARD" });
      },
      () => { }
    );
  }

}