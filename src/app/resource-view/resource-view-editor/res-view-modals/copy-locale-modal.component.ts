import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTNode } from "../../../models/ARTResources";
import { Language } from "../../../models/LanguagesCountries";

@Component({
    selector: "copy-locale-modal",
    templateUrl: "./copy-locale-modal.component.html",
    standalone: false
})
export class CopyLocalesModalComponent {
    @Input() value: ARTNode;
    @Input() localesInput: Language[];

    locales: { locale: Language; checked: boolean; }[] = [];

    constructor(public activeModal: NgbActiveModal) {}

    ngOnInit() {
        this.localesInput.forEach(l => {
            this.locales.push({ locale: l, checked: true });
        });
    }

    /**
     * Ok disabled if none of the locales is checked
     */
    isOkDisabled() {
        for (const l of this.locales) {
            if (l.checked) return false;
        }
        return true;
    }

    ok() {
        let returnData: Language[] = [];
        this.locales.forEach(l => {
            if (l.checked) {
                returnData.push(l.locale);
            }
        });
        this.activeModal.close(returnData);
    }

    cancel() {
        this.activeModal.dismiss();
    }

}
