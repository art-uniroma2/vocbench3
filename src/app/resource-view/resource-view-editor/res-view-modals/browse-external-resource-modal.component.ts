import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin, Observable } from "rxjs";
import { map } from 'rxjs/operators';
import { ExtensionPointID, Scope } from "src/app/models/Plugins";
import { SettingsEnum } from "src/app/models/Properties";
import { SettingsServices } from "src/app/services/settings.service";
import { LocalStorageManager } from 'src/app/utils/LocalStorageManager';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { ARTURIResource, RDFResourceRolesEnum } from "../../../models/ARTResources";
import { Project } from "../../../models/Project";
import { RDFS, SKOS } from "../../../models/Vocabulary";
import { ProjectServices } from "../../../services/projects.service";
import { PropertyServices, RangeType } from "../../../services/properties.service";
import { HttpServiceContext, STRequestOptions } from "../../../utils/HttpManager";
import { ProjectContext, VBContext } from "../../../utils/VBContext";
import { VBProperties } from "../../../utils/VBProperties";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../../modal-dialogs/browsing-modals/browsing-modals.service";
import { NodeSelectEvent } from "src/app/structures/abstractNode";

/**
 * This modal is used to browse and select a resource belonging to an external project.
 * This is used when aligning a resource, or when enriching a property for a resource 
 * (only in this case a property is passed to the modal)
 */

@Component({
  selector: "browse-external-resource-modal",
  templateUrl: "./browse-external-resource-modal.component.html",
  standalone: false
})
export class BrowseExternalResourceModalComponent {
  @Input() title: string;
  @Input() property: ARTURIResource;
  @Input() propChangeable: boolean = true;

  private rootProperty: ARTURIResource; //root property of the section that invoked this modal
  enrichingProperty: ARTURIResource;

  projectList: Project[] = [];
  project: Project;
  remoteProjCtx: ProjectContext;
  schemes: ARTURIResource[]; //scheme to explore in case target project is skos(xl)
  private remoteResource: ARTURIResource;

  activeView: RDFResourceRolesEnum;

  constructor(public activeModal: NgbActiveModal, public projService: ProjectServices,
    private settingsService: SettingsServices, private propService: PropertyServices, private vbProp: VBProperties,
    private basicModals: BasicModalServices, private browsingModals: BrowsingModalServices) {
  }

  ngOnInit() {
    this.rootProperty = this.property;
    this.enrichingProperty = this.rootProperty;

    if (this.enrichingProperty != null) {
      this.checkPropertyRangeResource(this.enrichingProperty).subscribe(
        allowsResource => {
          if (!allowsResource) {
            this.basicModals.alert({ key: "STATUS.ERROR" }, { key: "MESSAGES.CANNOT_ENRICH_PROPERTY_WITH_REMOTE_RES", params: { property: this.enrichingProperty.getShow() } },
              ModalType.warning);
            this.cancel();
          }
        }
      );
    }

    this.projService.listProjects(VBContext.getWorkingProject(), true).subscribe(
      projects => {
        //keep only the projects different from the current
        for (const p of projects) {
          if (p.isOpen() && p.getName() != VBContext.getWorkingProject().getName()) {
            this.projectList.push(p);
          }
        }
        this.restoreLastProject();
      }
    );
  }

  private restoreLastProject() {
    let lastExploredProject: string = LocalStorageManager.getItem(LocalStorageManager.ALIGNMENT_LAST_EXPLORED_PROJECT);
    if (lastExploredProject != null) {
      this.projectList.forEach((p: Project) => {
        if (p.getName() == lastExploredProject) {
          this.project = p;
        }
      });
      if (this.project != null) { //last explored project found among the available
        this.onProjectChange();
      }
    }
  }

  private restoreLastType() {
    let lastChosenType: string = LocalStorageManager.getItem(LocalStorageManager.ALIGNMENT_LAST_CHOSEN_TYPE);
    if (lastChosenType != null) {
      this.activeView = lastChosenType as RDFResourceRolesEnum;
    }
  }

  onProjectChange() {
    HttpServiceContext.removeContextProject();
    HttpServiceContext.setContextProject(this.project);
    LocalStorageManager.setItem(LocalStorageManager.ALIGNMENT_LAST_EXPLORED_PROJECT, this.project.getName());

    this.remoteProjCtx = new ProjectContext(this.project);
    let initProjectCtxFn: Observable<void>[] = [
      this.vbProp.initProjectUserBindings(this.remoteProjCtx),
      this.vbProp.initUserProjectPreferences(this.remoteProjCtx),
      this.vbProp.initProjectSettings(this.remoteProjCtx)
    ];
    forkJoin(initProjectCtxFn).subscribe(
      () => {
        this.activeView = null;
        this.remoteResource = null;
        if (this.isProjectSKOS()) {
          let reqOptions: STRequestOptions = new STRequestOptions({ ctxProject: this.project });
          this.settingsService.getSettings(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, reqOptions).subscribe(
            settings => {
              let activeSchemes: string[] = settings.getPropertyValue(SettingsEnum.activeSchemes);
              this.schemes = (activeSchemes != null) ? activeSchemes.map(s => new ARTURIResource(s)) : null;
              this.restoreLastType();
            }
          );
        } else {
          this.restoreLastType();
        }
      }
    );
  }

  onAlignTypeChanged() {
    LocalStorageManager.setItem(LocalStorageManager.ALIGNMENT_LAST_CHOSEN_TYPE, this.activeView);
    this.remoteResource = null;
  }

  changeProperty() {
    this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }, [this.rootProperty]).then(
      (selectedProp: ARTURIResource) => {
        if (!this.enrichingProperty.equals(selectedProp)) {
          this.checkPropertyRangeResource(selectedProp).subscribe(
            allowsResource => {
              if (allowsResource) {
                this.enrichingProperty = selectedProp;
              } else {
                this.basicModals.alert({ key: "STATUS.ERROR" }, { key: "MESSAGES.CANNOT_ENRICH_PROPERTY_WITH_REMOTE_RES", params: { property: selectedProp.getShow() } },
                  ModalType.warning);
              }
            }
          );
        }
      },
      () => { }
    );
  }

  /**
   * Used for checking that an enriching property allows resources as value (not literal).
   */
  private checkPropertyRangeResource(property: ARTURIResource): Observable<boolean> {
    return this.propService.getRange(property).pipe(
      map(range => {
        let ranges = range.ranges;
        if (range != undefined) {
          let rangeCollection: ARTURIResource[] = ranges.rangeCollection ? ranges.rangeCollection.resources : null;
          if (
            ranges.type == RangeType.literal ||
            (rangeCollection != null && rangeCollection.length == 1 && rangeCollection[0].equals(RDFS.literal))
          ) { //range literal
            return false;
          }
        }
        return true;
      })
    );
  }

  /**
   * Listener called when a resource of a tree is selected
   */
  onNodeSelected(event?: NodeSelectEvent) {
    this.remoteResource = event?.value;
  }

  /**
   * Listener called when it's aligning concept and the scheme in the concept tree is changed
   */
  onConceptTreeSchemeChange() {
    this.remoteResource = null;
  }

  isProjectSKOS(): boolean {
    return this.project.getModelType() == SKOS.uri;
  }

  isOkClickable(): boolean {
    return this.remoteResource != undefined;
  }

  ok() {
    HttpServiceContext.removeContextProject();
    this.activeModal.close({
      resource: this.remoteResource,
      property: this.enrichingProperty
    });
  }

  cancel() {
    HttpServiceContext.removeContextProject();
    this.activeModal.dismiss();
  }

}


export class BrowseExternalResourceModalReturnData {
  resource: ARTURIResource;
  property: ARTURIResource;
}