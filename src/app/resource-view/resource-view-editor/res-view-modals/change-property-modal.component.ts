import { Component, ElementRef, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { from, Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { ResViewSection, ResViewUtils } from 'src/app/models/ResourceView';
import { PropertyServices, RangeResponse } from 'src/app/services/properties.service';
import { ResourcesServices } from 'src/app/services/resources.service';
import { ResourceViewServices } from 'src/app/services/resource-view.service';
import { AuthorizationEvaluator } from 'src/app/utils/AuthorizationEvaluator';
import { UIUtils } from 'src/app/utils/UIUtils';
import { VBActionsEnum } from 'src/app/utils/VBActions';
import { VBContext } from 'src/app/utils/VBContext';
import { BasicModalServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { ARTNode, ARTResource, ARTURIResource, RDFResourceRolesEnum } from "../../../models/ARTResources";
import { NodeSelectEvent } from "src/app/structures/abstractNode";

@Component({
  selector: "change-prop-modal",
  templateUrl: "./change-property-modal.component.html",
  standalone: false
})
export class ChangePropertyModalComponent {
  @Input() section: ResViewSection;
  @Input() subject: ARTResource;
  @Input() property: ARTURIResource;
  @Input() value: ARTNode;

  ready: boolean;

  sectionProperties: ARTURIResource[];
  roots: ARTURIResource[];

  subjResForPropTree: ARTResource; //in other properties section, the property roots are retrieved directly in property-tree based on the resource provided in Input

  showAll: boolean = false;
  copy: boolean = false;
  targetProp: ARTURIResource;

  isShowAllAuthorized: boolean;

  constructor(public activeModal: NgbActiveModal, private elementRef: ElementRef,
    private propertyService: PropertyServices, private resourceService: ResourcesServices, private resViewService: ResourceViewServices,
    private basicModals: BasicModalServices) { }

  ngOnInit() {
    //the possibility to add/update to properties external the section is allowed only to those who have capability for invoking add/update triples services
    this.isShowAllAuthorized =
      AuthorizationEvaluator.isAuthorized(VBActionsEnum.resourcesAddValue, this.subject) &&
      AuthorizationEvaluator.isAuthorized(VBActionsEnum.resourcesUpdateTripleValue, this.subject);

    this.initSectionProperties().subscribe(
      () => {
        this.roots = this.sectionProperties;
        if (this.section == ResViewSection.properties) {
          this.subjResForPropTree = this.subject;
        }
        this.ready = true;
      }
    );
  }

  ngAfterViewInit() {
    UIUtils.setFullSizeModal(this.elementRef);
  }

  private initSectionProperties(): Observable<void> {
    if (this.section == ResViewSection.lexicalizations) {
      return this.resViewService.getLexicalizationProperties(this.subject).pipe(
        map(props => {
          this.sectionProperties = props;
        })
      );
    } else if (this.section == ResViewSection.properties) {
      return of(null);
    } else if (this.section in ResViewSection) { //any other known section
      this.sectionProperties = ResViewUtils.getSectionRootProperties(this.section);
      return of(null);
    } else { //custom section
      let predicatesIRI: string[] = VBContext.getWorkingProjectCtx().getProjectSettings().resourceView.customSections[this.section].matchedProperties;
      this.sectionProperties = predicatesIRI.map(p => new ARTURIResource(p, null, RDFResourceRolesEnum.property));
      return of(null);
    }
  }

  onShowAllChanged() {
    this.targetProp = null;
    if (this.showAll) {
      this.roots = null;
      if (this.section == ResViewSection.properties) {
        this.subjResForPropTree = null;
      }
    } else {
      if (this.section == ResViewSection.properties) {
        this.subjResForPropTree = this.subject;
      } else {
        this.roots = this.sectionProperties;
      }
    }
  }

  onNodeSelected(event?: NodeSelectEvent) {
    this.targetProp = event?.value;
  }

  doChecks(): Observable<boolean> {
    return this.propertyService.getRange(this.targetProp).pipe(
      mergeMap(range => {
        if (!RangeResponse.isRangeCompliant(range, this.value)) {
          return from(
            this.basicModals.confirm({ key: "STATUS.WARNING" }, { key: "MESSAGES.VALUE_TYPE_PROPERTY_RANGE_INCONSISTENT_CONFIRM", params: { property: this.targetProp.getShow() } }, ModalType.warning).then(
              () => {
                return true;
              },
              () => {
                return false;
              }
            )
          );
        } else {
          return of(true);
        }
      })
    );
  }

  /**
   * Ok disabled if none of the locales is checked
   */
  isOkDisabled() {
    return !this.targetProp;
  }

  ok() {
    this.doChecks().subscribe(
      ok => {
        if (ok) {
          if (this.copy) {
            if (this.showAll) {
              //cannot guarantee that the target property is handled by the same section where the modal has been invoked
              // => execute the "generic" add service
              this.resourceService.addValue(this.subject, this.targetProp, this.value).subscribe(
                () => {
                  this.activeModal.close();
                }
              );
            } else { //still in the same section
              //leave the add to the RV section implementation
              let data: ChangePropertyData = {
                newProperty: this.targetProp,
                value: this.value,
                copy: true
              };
              this.activeModal.close(data);
            }
          } else { //move to another / change property
            if (this.showAll) {
              //cannot guarantee that the target property is handled by the same section where the modal has been invoked
              // => execute the "generic" update service
              this.resourceService.updateTriplePredicate(this.subject, this.property, this.targetProp, this.value).subscribe(
                () => {
                  this.activeModal.close();
                }
              );
            } else { //still in the same section
              //leave the update to the RV section implementation
              let data: ChangePropertyData = {
                newProperty: this.targetProp,
                value: this.value
              };
              this.activeModal.close(data);
            }
          }
        }
      }
    );
  }

  cancel() {
    this.activeModal.dismiss();
  }

}


export interface ChangePropertyData {
  newProperty: ARTURIResource;
  value: ARTNode;
  copy?: boolean;
}