import { Component, ElementRef, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTURIResource, ResAttribute } from '../../../models/ARTResources';
import { UIUtils } from "../../../utils/UIUtils";
import { NodeSelectEvent } from "src/app/structures/abstractNode";

@Component({
  selector: "constituent-list-creator-modal",
  templateUrl: "./constituent-list-creator-modal.component.html",
  standalone: false
})
export class ConstituentListCreatorModalComponent {
  @Input() title: string;

  // private constituentCls: ARTURIResource = Decomp.component;

  selectedConstituentSource: ARTURIResource;
  selectedConstituentTarget: ARTURIResource;

  list: ARTURIResource[] = [];

  ordered: boolean = true;

  constructor(public activeModal: NgbActiveModal, private elementRef: ElementRef) { }

  ngAfterViewInit() {
    UIUtils.setFullSizeModal(this.elementRef);
  }

  onLexicalEntrySelected(event?: NodeSelectEvent) {
    this.selectedConstituentSource = event?.value;
  }

  addConstituentToList() {
    let constituent: ARTURIResource = this.selectedConstituentSource.clone();
    constituent.deleteAdditionalProperty(ResAttribute.SELECTED);
    this.list.push(constituent);
  }

  removeConstituentFromList() {
    this.list.splice(this.list.indexOf(this.selectedConstituentTarget), 1);
    this.selectedConstituentTarget = null;
  }

  moveUp() {
    let idx = this.list.indexOf(this.selectedConstituentTarget);
    this.list.splice(idx - 1, 0, this.list.splice(idx, 1)[0]);
  }
  moveDown() {
    let idx = this.list.indexOf(this.selectedConstituentTarget);
    this.list.splice(idx + 1, 0, this.list.splice(idx, 1)[0]);
  }

  isFirstInList(): boolean {
    return this.list[0] == this.selectedConstituentTarget;
  }
  isLastInList(): boolean {
    return this.list[this.list.length - 1] == this.selectedConstituentTarget;
  }

  ok() {
    this.activeModal.close({ list: this.list, ordered: this.ordered });
  }

  cancel() {
    this.activeModal.dismiss();
  }

}

export class ConstituentListCreatorModalReturnData {
  list: ARTURIResource[];
  ordered: boolean;
}