import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTURIResource, RDFResourceRolesEnum } from "../../../models/ARTResources";
import { Decomp, RDF } from "../../../models/Vocabulary";
import { BrowsingModalServices } from "../../../modal-dialogs/browsing-modals/browsing-modals.service";
import { NodeSelectEvent } from "src/app/structures/abstractNode";

@Component({
  selector: "rdfs-members-modal",
  templateUrl: "./rdfs-members-modal.component.html",
  standalone: false
})
export class RdfsMembersModalComponent {
  @Input() property: ARTURIResource;
  @Input() propChangeable: boolean = true;

  private constituentCls: ARTURIResource = Decomp.component;

  rootProperty: ARTURIResource; //root property of the section that invoked this modal
  enrichingProperty: ARTURIResource;

  rdfN: ARTURIResource;
  memberN: number = 1;

  selectedConstituent: ARTURIResource;

  constructor(public activeModal: NgbActiveModal, private browsingModals: BrowsingModalServices) { }

  ngOnInit() {
    this.updateRdfNProp();

    this.rootProperty = this.property;
    this.enrichingProperty = this.rdfN;
  }

  updateRdfNProp() {
    this.rdfN = new ARTURIResource(RDF.namespace + "_" + this.memberN, "rdf:_" + this.memberN, RDFResourceRolesEnum.objectProperty);
    this.enrichingProperty = this.rdfN;
  }

  onConstituentSelected(event?: NodeSelectEvent) {
    this.selectedConstituent = event?.value;
  }

  changeProperty() {
    this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }, [this.rootProperty]).then(
      (selectedProp: any) => {
        if (!this.enrichingProperty.equals(selectedProp)) {
          this.enrichingProperty = selectedProp;
        }
      },
      () => { }
    );
  }

  ok() {
    this.activeModal.close({ property: this.enrichingProperty, value: this.selectedConstituent });
  }

  cancel() {
    this.activeModal.dismiss();
  }

}

export class RdfsMembersModalReturnData {
  property: ARTURIResource;
  value: ARTURIResource;
}
