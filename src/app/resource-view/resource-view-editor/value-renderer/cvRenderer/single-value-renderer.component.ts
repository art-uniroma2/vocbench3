import { Component, Input } from "@angular/core";
import { ARTNode, ARTResource } from "src/app/models/ARTResources";
import { AbstractSingleValueView, AdvSingleValueView, CustomViewRenderedValue } from "src/app/models/CustomViews";
import { CustomViewsServices } from "src/app/services/custom-views.service";
import { ResourcesServices } from "src/app/services/resources.service";
import { AbstractSingleViewRendererComponent } from "./abstractSingleViewRenderer";

@Component({
  selector: "single-value-renderer",
  templateUrl: "./single-value-renderer.component.html",
  styles: [`
    :host {
        display: block;
    }
    .multivalue {
        outline: 1px solid #eee;
        border-radius: 5px;
        margin-right: 5px;
    }
  `],
  standalone: false
})
export class SingleValueRendererComponent extends AbstractSingleViewRendererComponent {

  @Input() view: AbstractSingleValueView;


  constructor(private resourceService: ResourcesServices, private cvService: CustomViewsServices) {
    super();
  }

  ngOnInit() {
    super.ngOnInit();
    this.view.values.forEach(v => {
      if (v.resource instanceof ARTResource) {
        //getResourcesInfo instead of getResourceDescription since the latter requires that the provided resource is locally defined (so prevent error in case of external reference)
        this.resourceService.getResourcesInfo([v.resource]).subscribe(
          (annRes: ARTResource[]) => {
            v.resource = annRes.find(r => r.equals(v.resource));
          }
        );
      }
    });
  }

  protected processInput(): void {
    //Nothing to do
  }

  onUpdate(data: { old: ARTNode, new: ARTNode }) {
    let pivots: Map<string, ARTNode> = new Map<string, ARTNode>();
    if (this.view instanceof AdvSingleValueView) {
      let valueToUpdate = this.view.values.find(v => v.resource.equals(data.old));
      for (let pivotName in valueToUpdate.pivots) {
        pivots.set(pivotName, valueToUpdate.pivots[pivotName]);
      }
    }
    this.cvService.updateSingleValueData(this.subject, this.predicate, data.old, data.new, pivots).subscribe(
      () => {
        this.update.emit();
      }
    );
  }

  deleteSingleValue(value: CustomViewRenderedValue) {
    if (this.view.values.length == 1) {
      //the delete is on the only value of the reified resource/CV-value, so delete the whole resource
      this.deleteHandler();
    } else {
      //the delete is on a specific value of the CV-value, delete only the specific one
      let pivots: Map<string, ARTNode> = new Map<string, ARTNode>();
      if (this.view instanceof AdvSingleValueView) {
        for (let pivotName in value.pivots) {
          pivots.set(pivotName, value.pivots[pivotName]);
        }
      }
      this.cvService.deleteSingleValueData(this.subject, this.predicate, value.resource, pivots).subscribe(
        () => {
          this.update.emit();
        }
      );

    }
  }


}