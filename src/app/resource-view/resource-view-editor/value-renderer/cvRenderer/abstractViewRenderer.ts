import { Directive, EventEmitter, Input, Output } from "@angular/core";
import { ARTNode, ARTResource, ARTURIResource } from "src/app/models/ARTResources";
import { ResViewSection, ValueClickEvent } from "src/app/models/ResourceView";
import { CRUDEnum, ResourceViewAuthEvaluator } from "src/app/utils/AuthorizationEvaluator";

@Directive()
export abstract class AbstractViewRendererComponent {

  @Input() subject: ARTResource;
  @Input() predicate: ARTURIResource;
  @Input() section: ResViewSection;
  @Input() readonly: boolean;
  @Input() rendering: boolean;

  @Output() valueClick = new EventEmitter<ValueClickEvent>();
  @Output() update = new EventEmitter<void>();
  @Output() delete = new EventEmitter<ARTNode>();

  editAuthorized: boolean = true;
  deleteAuthorized: boolean = true;


  constructor() { }

  ngOnInit() {
    this.initActionStatus();
  }

  protected initActionStatus(): void {
    this.editAuthorized = ResourceViewAuthEvaluator.isAuthorized(this.section, CRUDEnum.U, this.subject) && !this.readonly;
    this.deleteAuthorized = ResourceViewAuthEvaluator.isAuthorized(this.section, CRUDEnum.D, this.subject) && !this.readonly;
  }

  /**
   * normalizes the input data in a format compliant for the view renderer
   */
  protected abstract processInput(): void;


  protected abstract deleteHandler(arg?: any): void;


}