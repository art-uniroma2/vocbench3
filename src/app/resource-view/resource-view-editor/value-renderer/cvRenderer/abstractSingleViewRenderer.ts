import { Directive, Input } from "@angular/core";
import { ARTNode, ARTResource } from "src/app/models/ARTResources";
import { AbstractView } from "src/app/models/CustomViews";
import { ResourceUtils } from 'src/app/utils/ResourceUtils';
import { VBContext } from 'src/app/utils/VBContext';
import { AbstractViewRendererComponent } from "./abstractViewRenderer";
import { ValueClickEvent } from "src/app/models/ResourceView";

@Directive()
export abstract class AbstractSingleViewRendererComponent extends AbstractViewRendererComponent {

  @Input() view: AbstractView;

  isInferred: boolean = false;

  ngOnInit() {
    super.ngOnInit();

    //copied from EditableResourceComponent
    this.isInferred = ResourceUtils.isTripleInferred(this.view.resource);

    let inWorkingGraph: boolean = true;
    if (this.view.resource.getTripleGraphs() != null) {
      /*
      in some cases, the resource described by the view may not contain the graphs 
      (because no match between object of the pred-obj list and the described resource of CV has been found)
      so check if the resource being described is defined in the working graph only if the triple graphs are set
      */
      ResourceUtils.containsNode(this.view.resource.getTripleGraphs(), VBContext.getActualWorkingGraph());
    }

    this.readonly = (
      (!this.isInferred && !inWorkingGraph) || //neither in the working graph nor in inference graph
      this.readonly ||
      ResourceUtils.isTripleInStaging(this.view.resource)
    );
  }


  deleteHandler() {
    this.delete.emit(this.view.resource);
  }

  /**
 * Emit a doubleClick event on the resource described in the CV (unless otherwise specified, e.g. a single data of a chart)
 * @param res 
 */
  onDoubleClick(res?: ARTNode) {
    if (res == null) {
      res = this.view.resource;
    }
    if (res instanceof ARTResource) {
      this.valueClick.emit(new ValueClickEvent(res, null, true));
    }
  }

  onClick(event: MouseEvent) {
    if (this.view.resource instanceof ARTResource) {
      this.valueClick.emit(new ValueClickEvent(this.view.resource, event));
    }
  }

  onValueClick(event: ValueClickEvent) {
    this.valueClick.emit(event);
  }

}