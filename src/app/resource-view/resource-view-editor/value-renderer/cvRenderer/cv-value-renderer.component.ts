import { Component, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { CustomViewRenderedValue, UpdateMode } from "src/app/models/CustomViews";
import { ResViewSection, ValueClickEvent } from "src/app/models/ResourceView";
import { CRUDEnum, ResourceViewAuthEvaluator } from "src/app/utils/AuthorizationEvaluator";
import { NTriplesUtil, ResourceUtils } from "src/app/utils/ResourceUtils";
import { TranslationUtils } from 'src/app/utils/TranslationUtils';
import { BasicModalServices } from "src/app/modal-dialogs/basic-modals/basic-modals.service";
import { NewValueModalComponent } from 'src/app/modal-dialogs/creation-modals/new-value-modal/new-value-modal.component';
import { ModalOptions, ModalType } from "src/app/modal-dialogs/Modals";
import { LiteralPickerComponent } from "src/app/widget/pickers/value-picker/literal-picker.component";
import { ResourcePickerComponent, ResourcePickerConfig } from "src/app/widget/pickers/value-picker/resource-picker.component";
import { ARTLiteral, ARTNode, ARTResource, ARTURIResource, RDFResourceRolesEnum, RDFTypesEnum } from "../../../../models/ARTResources";

@Component({
  selector: "cv-value-renderer",
  templateUrl: "./cv-value-renderer.component.html",
  styles: [`
        ::ng-deep .renderingText { word-break: break-word } 
    `],
  standalone: false
})
export class CvValueRendererComponent {

  @Input() readonly: boolean;
  @Input() rendering: boolean;
  @Input() subject: ARTResource; //described resource
  @Input() value: CustomViewRenderedValue;
  @Input() section: ResViewSection;

  /* useful for render the edit/delete actions in different way:
  - in single value: both edit and delete are visible in a dropdown menu
  - in vector: only edit is visible, the deletion of single value of a table is not allowed (currently) since I don't know the property which the value is attached to
  */
  @Input() context: CvValueRendererCtx = "single_value";

  //this is an hack for using the resource and literal pickers logic inside this component (I added the picker components in the template as hidden elements)
  @ViewChild(ResourcePickerComponent) resPicker: ResourcePickerComponent;
  resPickerConf: ResourcePickerConfig;

  @ViewChild(LiteralPickerComponent) litPicker: LiteralPickerComponent;
  litPickerDatatypes: ARTURIResource[];

  @Output() update = new EventEmitter<{ old: ARTNode, new: ARTNode }>(); //a change has been done => request to update the RV
  @Output() delete = new EventEmitter<void>(); //require a deletion of the current value to the parent component
  @Output() valueClick = new EventEmitter<ValueClickEvent>();

  editInProgress: boolean = false;
  resourceStringValue: string;
  resourceStringValuePristine: string;

  editAuthorized: boolean = true;
  deleteAuthorized: boolean = true;

  constructor(private basicModals: BasicModalServices, private modalService: NgbModal, private translateService: TranslateService) { }

  ngOnInit() {
    if (this.value.updateInfo.updateMode == UpdateMode.picker) {
      if (this.value.updateInfo.valueType == RDFTypesEnum.resource && this.value.updateInfo.classes != null) {
        this.resPickerConf = { roles: [RDFResourceRolesEnum.individual], classes: this.value.updateInfo.classes.map(c => NTriplesUtil.parseURI(c)) };
      } else if (this.value.updateInfo.valueType == RDFTypesEnum.literal && this.value.updateInfo.datatype != null) {
        this.litPickerDatatypes = [NTriplesUtil.parseURI(this.value.updateInfo.datatype)];
      }
    }

    if (this.value.resource) {
      this.readonly = this.readonly || ResourceUtils.isTripleInStaging(this.value.resource);
    }

    //edit authorized if update mode is provided and edit capabilities are granted
    this.editAuthorized = this.value.updateInfo.updateMode != UpdateMode.none &&
      ResourceViewAuthEvaluator.isAuthorized(this.section, CRUDEnum.U, this.subject, this.value.resource) && !this.readonly;
    this.deleteAuthorized = ResourceViewAuthEvaluator.isAuthorized(this.section, CRUDEnum.D, this.subject, this.value.resource) && !this.readonly;
  }

  edit() {
    if (this.value.updateInfo.updateMode == UpdateMode.inline) {
      this.editInline();
    } else if (this.value.updateInfo.updateMode == UpdateMode.picker) {
      if (this.value.updateInfo.valueType == RDFTypesEnum.resource) {
        this.resPicker.pickLocalResource();
      } else if (this.value.updateInfo.valueType == RDFTypesEnum.literal) {
        this.litPicker.pickLiteral();
      } else { //valueType null => any type
        this.selectValue();
      }
    } else { //widget (cannot be "none", since this option disables edit button)
      this.editInline(); //this edit is temporarly, what to do here?
    }
  }

  selectValue() {
    const modalRef: NgbModalRef = this.modalService.open(NewValueModalComponent, new ModalOptions());
    modalRef.componentInstance.title = TranslationUtils.getTranslatedText({ key: "COMMONS.ACTIONS.EDIT" }, this.translateService);
    modalRef.componentInstance.editable = true;
    modalRef.result.then(
      value => {
        this.update.emit({ old: this.value.resource, new: value });
      },
      () => { }
    );
  }

  editInline() {
    this.editInProgress = true;
    this.resourceStringValue = this.value.resource.toNT();//default string value (in the follow if-else override it eventually)
    this.resourceStringValuePristine = this.resourceStringValue;
  }
  confirmEdit() {
    if (this.resourceStringValue != this.resourceStringValuePristine) { //apply edit only if the representation is changed
      try {
        let newValue: ARTNode = NTriplesUtil.parseNode(this.resourceStringValue);
        this.update.emit({ old: this.value.resource, new: newValue });
      } catch {
        this.basicModals.alert({ key: "STATUS.INVALID_VALUE" }, { key: this.resourceStringValue + " is not a valid NT value" }, ModalType.warning);
      }
    }
  }
  cancelEdit() {
    this.editInProgress = false;
  }

  onResourcePicked(res: ARTResource) {
    this.update.emit({ old: this.value.resource, new: res });
  }
  onLiteralPicked(lit: ARTLiteral) {
    this.update.emit({ old: this.value.resource, new: lit });
  }

  deleteHandler() {
    this.delete.emit();
  }

  valueDblClick() {
    if (this.value.resource instanceof ARTResource) {
      this.valueClick.emit(new ValueClickEvent(this.value.resource, null, true ));
    }
  }

  onValueClick(event: ValueClickEvent) {
    this.valueClick.emit(event);
  }

  onLinkClicked(linkRes: ARTURIResource) {
    if (this.value.resource instanceof ARTResource) {
      let vcEvent = new ValueClickEvent(linkRes);
      vcEvent.ctrl = true;
      this.valueClick.emit(vcEvent);
    }
  }

}

export type CvValueRendererCtx = "single_value" | "vector";