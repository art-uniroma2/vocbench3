import { Directive, EventEmitter, Input, Output } from "@angular/core";
import { ARTNode, ARTResource, ARTURIResource } from "../../../models/ARTResources";
import { ResViewSection, ValueClickEvent } from "../../../models/ResourceView";

@Directive()
export class AbstractResViewResource {
  @Input() subject: ARTResource; //subject of the triple which the "resource" represents the object (the resource represented in the RV)
  @Input() predicate: ARTURIResource; //property of the triple which the "resource" represents the object
  @Input() resource: ARTNode; //resource shown in the component. Represents the object of a triple shown in a ResourceView section
  @Input() rendering: boolean;
  @Input() readonly: boolean;
  @Input() section: ResViewSection;

  @Output() delete = new EventEmitter();
  @Output() valueClick = new EventEmitter<ValueClickEvent>();

  deleteValue() {
    this.delete.emit();
  }

  onDblClick() {
    if (this.resource instanceof ARTResource) {
      this.valueClick.emit(new ValueClickEvent(this.resource, null, true));
    }
  }
}