import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Output, ViewChild } from "@angular/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { InferenceExplanationModalComponent } from "src/app/icv/owlConsistencyViolations/inference-explanation-modal.component";
import { Triple } from "src/app/models/Shared";
import { RefactorServices } from "src/app/services/refactor.service";
import { VBEventHandler } from 'src/app/utils/VBEventHandler';
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { ManchesterExprModalReturnData } from "src/app/modal-dialogs/shared-modals/manchester-expr-modal/manchester-expr-modal.component";
import { ARTBNode, ARTLiteral, ARTNode, ARTResource, ARTURIResource, RDFResourceRolesEnum, RDFTypesEnum, ResAttribute, ShowInterpretation } from "../../../models/ARTResources";
import { Language, Languages } from "../../../models/LanguagesCountries";
import { ResViewSection, ResViewUtils, ValueClickEvent } from "../../../models/ResourceView";
import { RDF, RDFS, SKOS, SKOSXL } from "../../../models/Vocabulary";
import { ManchesterServices } from "../../../services/manchester.service";
import { PropertyServices } from "../../../services/properties.service";
import { ResourcesServices } from "../../../services/resources.service";
import { SkosServices } from "../../../services/skos.service";
import { AuthorizationEvaluator, CRUDEnum, ResourceViewAuthEvaluator } from "../../../utils/AuthorizationEvaluator";
import { DatatypeValidator } from "../../../utils/DatatypeValidator";
import { NTriplesUtil, ResourceUtils } from "../../../utils/ResourceUtils";
import { VBActionsEnum } from "../../../utils/VBActions";
import { VBContext } from "../../../utils/VBContext";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../../modal-dialogs/browsing-modals/browsing-modals.service";
import { CreationModalServices } from "../../../modal-dialogs/creation-modals/creation-modals.service";
import { SharedModalServices } from "../../../modal-dialogs/shared-modals/shared-modals.service";
import { ChangePropertyData } from '../res-view-modals/change-property-modal.component';
import { ResViewModalServices } from "../res-view-modals/resViewModalServices";
import { AbstractResViewResource } from "./abstractResViewResource";

@Component({
  selector: "editable-resource",
  templateUrl: "./editable-resource.component.html",
  host: { class: "d-flex" },
  standalone: false
})
export class EditableResourceComponent extends AbstractResViewResource {

  @ViewChild('editingField', { static: false }) private editingField: ElementRef;

  @Output() update = new EventEmitter(); //fire a request to update the entire ResView
  @Output() edit = new EventEmitter(); //fire a request for the renderer to edit the value
  @Output() changeProp = new EventEmitter<ChangePropertyData>(); //fire a request for the renderer to change the property of the value
  @Output() copyLocale = new EventEmitter<Language[]>(); //fire a request for the renderer to copy the value to different locales

  //useful to perform a check on the type of the edited value.
  //The check isn't too deep, it just distinguishes between resource, literal or any (undetermined)
  private rangeType: RDFTypesEnum;
  private ranges: {
    type: string,
    rangeCollection: {
      resources: ARTURIResource[],
      dataRanges: (ARTLiteral[])[]
    }
  }; //stores response.ranges of getRange service

  isImage: boolean = false;

  editActionScenario: EditActionScenarioEnum = EditActionScenarioEnum.default;

  //actions authorizations
  editMenuDisabled: boolean = false;
  editAuthorized: boolean = false;
  bulkEditAuthorized: boolean = false;
  deleteAuthorized: boolean = false;
  bulkDeleteAuthorized: boolean = false;
  spawnFromLabelAuthorized: boolean = false;
  moveLabelAuthorized: boolean = false;
  assertAuthorized: boolean = false;
  copyLocalesAuthorized: boolean = false;

  changePropertyAvailable: boolean;

  isInferred: boolean = false;
  isXLabelMenuItemAvailable: boolean = false;
  isReplaceMenuItemAvailable: boolean = true;
  isBulkActionMenuItemAvailable: boolean = true;
  copyLocalesAction: { available: boolean, locales: Language[] } = { available: false, locales: [] };

  isRepoGDB: boolean = false;

  editInProgress: boolean = false;
  bulkEditInProgress: boolean = false;
  editLiteralInProgress: boolean = false;
  private resourceStringValuePristine: string;
  resourceStringValue: string; //editable representation of the resource

  constructor(private resourcesService: ResourcesServices, private propService: PropertyServices,
    private manchesterService: ManchesterServices, private refactorService: RefactorServices, private skosService: SkosServices,
    private basicModals: BasicModalServices, private sharedModals: SharedModalServices,
    private creationModals: CreationModalServices, private browsingModals: BrowsingModalServices,
    private rvModalService: ResViewModalServices, private dtValidator: DatatypeValidator, private eventHandler: VBEventHandler,
    private translateService: TranslateService, private modalService: NgbModal, private cdRef: ChangeDetectorRef) {
    super();
  }

  ngOnInit() {
    let displayImg: boolean = VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPreferences.displayImg;
    if (displayImg && this.resource.getAdditionalProperty(ResAttribute.IS_IMAGE)) {
      this.isImage = true;
    }

    /**
     * Initializes the editActionScenario: this tells how to handle the "edit" action.
     * For details see the comments written to the enum definitions of the EditActionScenarioEnum.
     */
    if (this.resource instanceof ARTLiteral && this.resource.getLang() != null) {
      this.editActionScenario = EditActionScenarioEnum.langTaggedLiteral;
    } else if (this.resource instanceof ARTLiteral && this.resource.getDatatype() != null) {
      this.editActionScenario = EditActionScenarioEnum.typedLiteral;
    } else if (this.resource instanceof ARTResource && this.resource.getRole() == RDFResourceRolesEnum.xLabel) {
      this.editActionScenario = EditActionScenarioEnum.xLabel;
    } else if (
      //in datatypeDefinitions and subPropertyChains sections the edit is delegated to the SectionRenderer.
      this.section == ResViewSection.datatypeDefinitions || this.section == ResViewSection.subPropertyChains ||
      //in facets (precisely inverseOf in property facets) the edit of ObjectProperty expression is delegated to the SectionRenderer
      (this.section == ResViewSection.facets && this.resource.isBNode() && this.resource.getAdditionalProperty(ResAttribute.SHOW_INTERPR) == ShowInterpretation.ope) ||
      (this.section == ResViewSection.classaxioms && this.resource.isBNode() && this.resource.getAdditionalProperty(ResAttribute.SHOW_INTERPR) == ShowInterpretation.list)
    ) {
      this.editActionScenario = EditActionScenarioEnum.section;
    } else if (this.section == ResViewSection.classaxioms && this.resource.isBNode() && this.resource.getAdditionalProperty(ResAttribute.SHOW_INTERPR) == ShowInterpretation.descr) {
      this.editActionScenario = EditActionScenarioEnum.manchesterDescr;
    }

    /**
     * Determines if the menu items about xlabels should be visible.
     * Visible only if:
     * the subject is a concept, the object is a xLabel and if it is in the lexicalizations section
     * (so avoid "spawn new concept..." from xLabel in labelRelation section of an xLabel ResView)
     */
    this.isXLabelMenuItemAvailable = (
      this.section == ResViewSection.lexicalizations &&
      this.subject.getRole() == RDFResourceRolesEnum.concept &&
      this.resource.isResource() && (this.resource as ARTResource).getRole() == RDFResourceRolesEnum.xLabel
    );

    /**
     * Determines if the menu item "Replace with existing resource" should be visible.
     * Visible:
     * in classaxioms section if object is an IRI
     * in any sections (except from classaxioms, datatypeFacets, subPropertyChains) if the object is a resource
    */
    this.isReplaceMenuItemAvailable = (
      (this.section == ResViewSection.classaxioms && this.resource.isURIResource()) ||
      (this.section != ResViewSection.subPropertyChains && this.section != ResViewSection.classaxioms &&
        this.section != ResViewSection.datatypeDefinitions && this.resource.isResource())
    );

    /**
     * Bulk edit items are available only if those section where it's safe to update/remove directly the PO in the SPO triple
     */
    this.isBulkActionMenuItemAvailable = (
      this.section == ResViewSection.broaders ||
      (this.section == ResViewSection.classaxioms && this.resource instanceof ARTURIResource) ||
      // this.section == ResViewSection.constituents ||
      // this.section == ResViewSection.denotations ||
      this.section == ResViewSection.disjointProperties ||
      (this.section == ResViewSection.domains && this.resource instanceof ARTURIResource) ||
      this.section == ResViewSection.equivalentProperties ||
      // this.section == ResViewSection.evokedLexicalConcepts ||
      // this.section == ResViewSection.facets ||
      // this.section == ResViewSection.imports ||
      this.section == ResViewSection.labelRelations ||
      // this.section == ResViewSection.lexicalForms ||
      // this.section == ResViewSection.lexicalSenses ||
      (this.section == ResViewSection.lexicalizations && this.resource instanceof ARTLiteral) ||
      this.section == ResViewSection.members ||
      // this.section == ResViewSection.membersOrdered ||
      this.section == ResViewSection.notes ||
      (this.section == ResViewSection.properties && this.resource instanceof ARTLiteral) ||
      (this.section == ResViewSection.ranges && this.resource instanceof ARTURIResource) ||
      // this.section == ResViewSection.rdfsMembers ||
      this.section == ResViewSection.schemes ||
      // this.section == ResViewSection.subPropertyChains ||
      this.section == ResViewSection.subterms ||
      this.section == ResViewSection.superproperties ||
      this.section == ResViewSection.topconceptof ||
      this.section == ResViewSection.types
    );

    this.isInferred = ResourceUtils.isTripleInferred(this.resource);

    if (this.isInferred) {
      this.isRepoGDB = VBContext.getWorkingProjectCtx().getRepoBackend().startsWith("graphdb:") || VBContext.getWorkingProjectCtx().getRepoBackend().startsWith("owlim:");
    }

    //init actions authorization
    let inWorkingGraph: boolean = ResourceUtils.containsNode(this.resource.getTripleGraphs(), VBContext.getActualWorkingGraph());
    this.editMenuDisabled = (
      (!this.isInferred && !inWorkingGraph) || //neither in the working graph nor in inference graph
      // (!this.resource.getAdditionalProperty(ResAttribute.EXPLICIT)) || 
      this.readonly ||
      ResourceUtils.isTripleInStaging(this.resource)
    );
    this.editAuthorized = ResourceViewAuthEvaluator.isAuthorized(this.section, CRUDEnum.U, this.subject, this.resource);
    this.deleteAuthorized = ResourceViewAuthEvaluator.isAuthorized(this.section, CRUDEnum.D, this.subject, this.resource);
    this.spawnFromLabelAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.refactorSpawnNewConceptFromLabel, null, this.resource);
    this.moveLabelAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.refactorMoveXLabelToResource, this.subject, this.resource);
    this.assertAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.resourcesAddValue, this.subject);
    this.copyLocalesAuthorized = ResourceViewAuthEvaluator.isAuthorized(this.section, CRUDEnum.C, this.subject, this.resource);
    //bulk actions visible in every section exept: subPropertyChains that 
    this.bulkEditAuthorized = this.editAuthorized && AuthorizationEvaluator.isAuthorized(VBActionsEnum.resourcesUpdatePredicateObject);
    this.bulkDeleteAuthorized = this.deleteAuthorized && AuthorizationEvaluator.isAuthorized(VBActionsEnum.resourcesRemovePredicateObject);

    this.changePropertyAvailable = ResViewUtils.changePropertySection.includes(this.section);

    /**
     * Copy to locales option is available only in lexicalizations, notes and properties sections
     * (currenlty only for simplicity, I will see later if it can be enabled also in other sections) if:
     * - in lexicalizations if the value is a plain literal or an xlabel with a language
     * - in notes if the value is a plain literal with a language
     * - in proeprties if the value is a plain literal with a language
     */
    if (
      this.resource.getAdditionalProperty(ResAttribute.LANG) != null && (
        (this.section == ResViewSection.properties && this.editActionScenario == EditActionScenarioEnum.langTaggedLiteral) || //plain in notes
        (this.section == ResViewSection.notes && this.editActionScenario == EditActionScenarioEnum.langTaggedLiteral) || //plain in notes
        (this.section == ResViewSection.lexicalizations && //plain or xlabel in lexicalizations
          (this.editActionScenario == EditActionScenarioEnum.langTaggedLiteral || this.editActionScenario == EditActionScenarioEnum.xLabel)
        )
      )) {
      let projectLangs: Language[] = VBContext.getWorkingProjectCtx().getProjectSettings().projectLanguagesSetting;
      let userAssignedLangs: string[] = VBContext.getProjectUserBinding().getLanguages();
      let intersection: Language[];
      if (userAssignedLangs.length == 0 && VBContext.getLoggedUser().isAdmin()) {
        intersection = projectLangs; //admin with no lang assigned => assign all project langs
      } else {
        intersection = projectLangs.filter((pl: Language) => {
          return userAssignedLangs.find(ul => ul.toLocaleLowerCase() == pl.tag.toLocaleLowerCase());
        });
      }
      let locales = Languages.getLocales(intersection, this.resource.getAdditionalProperty(ResAttribute.LANG));
      this.copyLocalesAction = {
        available: locales.length > 0,
        locales: locales
      };
    }

  }

  //======== "edit" HANDLER ========

  editLiteral() {
    if (this.editActionScenario == EditActionScenarioEnum.xLabel) {
      this.resourceStringValue = this.resource.getShow();
    } else if (this.editActionScenario == EditActionScenarioEnum.typedLiteral || this.editActionScenario == EditActionScenarioEnum.langTaggedLiteral) {
      this.resourceStringValue = (this.resource as ARTLiteral).getValue();
      if ((this.resource as ARTLiteral).getDatatype() == RDF.html.getURI()) {
        this.creationModals.newTypedLiteral({ key: "COMMONS.ACTIONS.EDIT_X", params: { x: this.predicate.getShow() } }, this.predicate, this.resource as ARTLiteral, [RDF.html], null, false, true).then(
          (literals: ARTLiteral[]) => {
            this.updateTriple(this.subject, this.predicate, this.resource, literals[0]);
          },
          () => { }
        );
        return;
      }
    }
    this.resourceStringValuePristine = this.resourceStringValue;
    this.editLiteralInProgress = true;
    this.focusEditingField();
  }

  editValue() {
    if (this.editActionScenario == EditActionScenarioEnum.section) {
      this.edit.emit();
    } else if (this.editActionScenario == EditActionScenarioEnum.manchesterDescr) {
      this.sharedModals.manchesterExpression({ key: "DATA.ACTIONS.EDIT_MANCHESTER_EXPRESSION" }, this.resource.getShow()).then(
        (data: ManchesterExprModalReturnData) => {
          if (data.expression == this.resource.getShow()) return; //if expression didn't change, don't do nothing
          this.manchesterService.updateExpression(data.expression, this.resource as ARTBNode, data.skipSemCheck).subscribe(
            () => { this.update.emit(); }
          );
        },
        () => { }
      );
    } else { //default
      //special case: resource is a data range => don't edit inline dataranges, but open the editor instead
      if (this.resource instanceof ARTBNode && this.resource.getRole() == RDFResourceRolesEnum.dataRange) {
        this.rvModalService.editDataRange(this.resource).then(
          () => { this.update.emit(); },
          () => { }
        );
      }
      else {
        if (this.rangeType == null) { //check to avoid repeating of getRange in case it's not the first time that user edits the value
          this.propService.getRange(this.predicate).subscribe(
            range => {
              this.ranges = range.ranges;
              /**
              * special case: if range is literal and has restriction (datarange), allow to edit only with datarange
              */
              if (
                this.ranges != null && this.ranges.type == RDFTypesEnum.literal &&
                this.ranges.rangeCollection != null && this.ranges.rangeCollection.dataRanges != null
              ) {
                this.creationModals.newTypedLiteral({ key: "COMMONS.ACTIONS.EDIT_X", params: { x: this.predicate.getShow() } }, this.predicate, null,
                  this.ranges.rangeCollection.resources, this.ranges.rangeCollection.dataRanges, false, true).then(
                    (literals: ARTLiteral[]) => {
                      this.updateTriple(this.subject, this.predicate, this.resource, literals[0]);
                    },
                    () => { }
                  );
              } else {
                this.computeResourceStringValue();
                this.editInProgress = true;
                this.focusEditingField();
              }
            }
          );
        } else {
          this.computeResourceStringValue();
          this.editInProgress = true;
          this.focusEditingField();
        }
      }
    }
  }

  bulkEdit() {
    this.computeResourceStringValue();
    this.bulkEditInProgress = true;
    this.focusEditingField();
  }

  private computeResourceStringValue() {
    this.resourceStringValue = this.resource.toNT();//default string value (in the follow if-else override it eventually)
    if (this.ranges != null) { //check to avoid error in case the property has custom ranges that replace the "classic" range
      let type: string = this.ranges.type;
      if (type == RDFTypesEnum.resource) {
        this.rangeType = RDFTypesEnum.resource;
        // special case: if user is editing an xLabel, the widget should allow to edit the literal form, not the uri
        if (this.resource instanceof ARTResource && this.resource.getRole() == RDFResourceRolesEnum.xLabel) {
          let literalForm: ARTLiteral = new ARTLiteral(
            this.resource.getShow(), null, this.resource.getAdditionalProperty(ResAttribute.LANG));
          this.resourceStringValue = literalForm.toNT();
        }
        //special case: if user is editing a class restriction, the widget should allow to edit the manchester expression
        else if (this.resource instanceof ARTBNode && this.resource.getAdditionalProperty(ResAttribute.SHOW_INTERPR) != null) {
          this.resourceStringValue = this.resource.getShow();
        }
      } else if (type.toLowerCase().includes("literal")) {
        this.rangeType = RDFTypesEnum.literal;
      } else {
        this.rangeType = RDFTypesEnum.undetermined; //default
      }
      this.resourceStringValuePristine = this.resourceStringValue;
    }
  }

  confirmEdit() {
    if (this.resourceStringValue != this.resourceStringValuePristine) { //apply edit only if the representation is changed
      if (this.editLiteralInProgress) {
        if (this.editActionScenario == EditActionScenarioEnum.langTaggedLiteral) {
          let newValue: ARTLiteral = new ARTLiteral(this.resourceStringValue, null, (this.resource as ARTLiteral).getLang());
          if (this.section == ResViewSection.lexicalizations) {
            this.updateLexicalization(this.subject, this.predicate, this.resource as ARTLiteral, newValue);
          } else if (this.section == ResViewSection.notes) {
            this.updateNote(this.subject, this.predicate, this.resource as ARTLiteral, newValue);
          } else {
            this.updateTriple(this.subject, this.predicate, this.resource, newValue);
          }
        } else if (this.editActionScenario == EditActionScenarioEnum.typedLiteral) {
          let newValue: ARTLiteral = new ARTLiteral(this.resourceStringValue, (this.resource as ARTLiteral).getDatatype(), null);
          if (!this.isTypedLiteralValid(newValue)) {
            this.basicModals.alert({ key: "STATUS.INVALID_VALUE" }, { key: "MESSAGES.INVALID_VALUE_FOR_DATATYPE", params: { value: newValue.getValue(), datatype: newValue.getDatatype() } },
              ModalType.warning);
            this.cancelEdit();
            return;
          }
          this.updateTriple(this.subject, this.predicate, this.resource, newValue);
        } else if (this.editActionScenario == EditActionScenarioEnum.xLabel) {
          let oldLitForm: ARTLiteral = new ARTLiteral(this.resource.getShow(), null, this.resource.getAdditionalProperty(ResAttribute.LANG));
          let newValue: ARTLiteral = new ARTLiteral(this.resourceStringValue, null, this.resource.getAdditionalProperty(ResAttribute.LANG));
          if (this.section == ResViewSection.lexicalizations) {
            this.updateLexicalization(this.resource as ARTResource, SKOSXL.literalForm, oldLitForm, newValue);
          } else {
            this.updateTriple(this.resource as ARTResource, SKOSXL.literalForm, oldLitForm, newValue);
          }
        }
      } else if (this.bulkEditInProgress) {
        try {
          let newValue: ARTNode = this.parseEditedValue();
          //check consistency of the new value
          if (this.isPropertyRangeInconsistentWithNewValue(newValue)) {
            this.basicModals.confirm({ key: "STATUS.WARNING" }, { key: "MESSAGES.VALUE_TYPE_PROPERTY_RANGE_INCONSISTENT_CONFIRM", params: { property: this.predicate.getShow() } },
              ModalType.warning).then(
                () => {
                  this.resourcesService.updatePredicateObject(this.predicate, this.resource, newValue).subscribe(
                    () => this.update.emit()
                  );
                },
                () => { this.cancelEdit(); }
              );
          } else {
            this.basicModals.confirm({ key: "STATUS.WARNING" }, { key: "MESSAGES.BULK_EDIT_CONFIRM" }).then(
              () => {
                this.resourcesService.updatePredicateObject(this.predicate, this.resource, newValue).subscribe(
                  () => this.update.emit()
                );
              },
              () => {
                this.cancelEdit();
              }
            );
          }
        } catch (err) {
          if (err instanceof Error) {
            this.basicModals.alert({ key: "STATUS.WARNING" }, err.message, ModalType.warning);
          } else {
            console.error("An unknown error occurred", err);
          }
          this.cancelEdit();
        }
      } else if (this.editInProgress) {
        try {
          let newValue: ARTNode = this.parseEditedValue();
          //check consistency of the new value
          if (this.isPropertyRangeInconsistentWithNewValue(newValue)) {
            this.basicModals.confirm({ key: "STATUS.WARNING" }, { key: "MESSAGES.VALUE_TYPE_PROPERTY_RANGE_INCONSISTENT_CONFIRM", params: { property: this.predicate.getShow() } },
              ModalType.warning).then(
                () => { this.updateTriple(this.subject, this.predicate, this.resource, newValue); },
                () => { this.cancelEdit(); }
              );
          } else {
            if (this.section == ResViewSection.notes) {
              this.updateNote(this.subject, this.predicate, this.resource as ARTLiteral, newValue);
            } else {
              this.updateTriple(this.subject, this.predicate, this.resource, newValue);
            }
          }
        } catch (err) {
          if (err instanceof Error) {
            this.basicModals.alert({ key: "STATUS.WARNING" }, err.message, ModalType.warning);
          } else {
            console.error("An unknown error occurred", err);
          }
          this.cancelEdit();
        }
      }
    } else {
      this.cancelEdit();
    }
  }

  private parseEditedValue(): ARTNode {
    let newValue: ARTNode;
    //parse the string typed by the user
    if (this.resourceStringValue.startsWith("<") && this.resourceStringValue.endsWith(">")) { //uri
      newValue = NTriplesUtil.parseURI(this.resourceStringValue);
    } else if (this.resourceStringValue.startsWith("_:")) { //bnode
      newValue = NTriplesUtil.parseBNode(this.resourceStringValue);
    } else if (this.resourceStringValue.startsWith("\"")) { //literal
      newValue = NTriplesUtil.parseLiteral(this.resourceStringValue);
      let litValue: ARTLiteral = newValue as ARTLiteral;
      if (litValue.getDatatype() != null && !this.isTypedLiteralValid(litValue)) {
        throw new Error(litValue.getValue() + " is not a valid value for the given datatype: " + litValue.getDatatype());
      }
    } else if (ResourceUtils.isQName(this.resourceStringValue, VBContext.getPrefixMappings())) { //qname
      newValue = ResourceUtils.parseQName(this.resourceStringValue, VBContext.getPrefixMappings());
    } else {
      throw new Error("Not a valid N-Triples representation: " + this.resourceStringValue);
    }
    return newValue;
  }

  private isPropertyRangeInconsistentWithNewValue(newValue: ARTNode): boolean {
    if (this.rangeType == RDFTypesEnum.literal && !newValue.isLiteral()) {
      return true;
    } else if (this.rangeType == RDFTypesEnum.resource) {
      /**
       * special case: if range of property is resource, it is still compliant with literal newValue in case 
       * in rangeCollection there is rdfs:Literal
       */
      if (ResourceUtils.containsNode(this.ranges.rangeCollection.resources, RDFS.literal) && newValue.isLiteral()) {
        return false;
      }
      if (!newValue.isResource()) {
        return true;
      }
    }
    return false;
  }

  private updateTriple(subject: ARTResource, predicate: ARTURIResource, oldValue: ARTNode, newValue: ARTNode) {
    if (oldValue.equals(newValue)) return;
    this.resourcesService.updateTripleValue(subject, predicate, oldValue, newValue).subscribe(
      () => {
        this.cancelEdit();
        /** Event propagated to the resView that refreshes.
         * I cannot simply update the rdf-resource since the URI of the resource
         * in the predicate objects list stored in the section render is still the same */
        this.update.emit();
      }
    );
  }

  /**
   * This method handles the edit of a value in the notes section.
   * For the moment this is a "workaround" to allow a more specific authorization check with the note edit
   * (which has a dedicated service, more specific than the general updateTriple).
   * In the future (if more specific/dedicated services will be provided for the triples update) 
   * it could be handled differently, namely an edit event is propagated up to the renderer that handles ad hoc the edit.
   * @param subject 
   * @param predicate 
   * @param oldValue 
   * @param newValue 
   */
  private updateNote(subject: ARTResource, predicate: ARTURIResource, oldValue: ARTNode, newValue: ARTNode) {
    this.skosService.updateNote(subject, predicate, oldValue, newValue).subscribe(
      () => {
        this.cancelEdit();
        this.update.emit();
      }
    );
  }

  /**
   * This method handles the edit of a value (EditScenario xlabel or langTaggedLiteral) in the lexicalizations section.
   * For the moment this is a "workaround" to allow a more specific authorization check with the lexicalization edit
   * (which has a dedicated service, more specific than the general updateTriple).
   * In the future (if more specific/dedicated services will be provided for the triples update) 
   * it could be handled differently, namely an edit event is propagated up to the renderer that handles ad hoc the edit.
   * @param subject 
   * @param predicate 
   * @param oldValue 
   * @param newValue 
   */
  private updateLexicalization(subject: ARTResource, predicate: ARTURIResource, oldValue: ARTLiteral, newValue: ARTLiteral) {
    this.resourcesService.updateLexicalization(subject, predicate, oldValue, newValue).subscribe(
      () => {
        this.resourcesService.getResourceDescription(this.subject).subscribe(
          updatedRes => {
            this.eventHandler.resourceLexicalizationUpdatedEvent.emit({ oldResource: this.subject, newResource: updatedRes });
          }
        );
        this.cancelEdit();
        this.update.emit();
      }
    );
  }

  cancelEdit() {
    this.editInProgress = false;
    this.bulkEditInProgress = false;
    this.editLiteralInProgress = false;
  }

  private isTypedLiteralValid(literal: ARTLiteral): boolean {
    let dt: ARTURIResource = new ARTURIResource(literal.getDatatype());
    let valid = this.dtValidator.isValid(literal, dt);
    return valid;
  }

  private focusEditingField() {
    this.cdRef.detectChanges();
    this.editingField.nativeElement.focus();
  }

  //====== "Replace with existing resource" HANDLER =====

  replace() {
    this.rvModalService.addPropertyValue({ key: "COMMONS.ACTIONS.REPLACE" }, this.subject, this.predicate, false, null, false).then(
      (data: any) => {
        this.updateTriple(this.subject, this.predicate, this.resource, data.value[0]);
      },
      () => { }
    );
  }

  //====== "Spawn new concept from this xLabel" HANDLER =====

  spawnNewConceptWithLabel() {
    //here I can cast resource since this method is called only on object with role "xLabel" that are ARTResource
    this.creationModals.newConceptFromLabel({ key: "RESOURCE_VIEW.ACTIONS.SPAWN_CONCEPT_FROM_XLABEL" }, this.resource as ARTResource, SKOS.concept, true, this.subject as ARTURIResource).then(
      data => {
        let oldConcept: ARTURIResource = this.subject as ARTURIResource;
        this.refactorService.spawnNewConceptFromLabel(this.resource as ARTResource, data.schemes, oldConcept,
          data.uriResource, data.broader, data.cfValue).subscribe(
            () => {
              this.update.emit();
            }
          );
      },
      () => { }
    );
  }

  //====== "Move xLabel to another concept" HANDLER =====

  moveLabelToConcept() {
    this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_LEXICALIZATION_PROPERTY" }, [SKOSXL.prefLabel, SKOSXL.altLabel, SKOSXL.hiddenLabel]).then(
      predicate => {
        let activeSchemes: ARTURIResource[] = VBContext.getWorkingProjectCtx().getProjectPreferences().activeSchemes;
        this.browsingModals.browseConceptTree({ key: "DATA.ACTIONS.SELECT_CONCEPT" }, activeSchemes, false).then(
          newConcept => {
            this.refactorService.moveXLabelToResource(this.subject, predicate, this.resource as ARTResource, newConcept).subscribe({
              next: () => {
                this.update.emit();
              },
              error: (err: Error) => {
                if (err.name.endsWith("PrefPrefLabelClashException")) {
                  let msg = err.message + " " + this.translateService.instant("MESSAGES.FORCE_OPERATION_CONFIRM");
                  this.basicModals.confirm({ key: "STATUS.OPERATION_DENIED" }, msg, ModalType.warning).then(
                    () => {
                      this.refactorService.moveXLabelToResource(this.subject, predicate, this.resource as ARTResource, newConcept, true).subscribe(
                        () => {
                          this.update.emit();
                        }
                      );
                    },
                    () => { }
                  );
                }
              }
            });
          },
          () => { }
        );
      },
      () => { }
    );
  }

  //====== "Assert/Explain inferred statement" HANDLER =====

  assertInferred() {
    this.resourcesService.addValue(this.subject, this.predicate, this.resource).subscribe(
      () => {
        this.update.emit();
      }
    );
  }

  explainInferred() {
    const modalRef: NgbModalRef = this.modalService.open(InferenceExplanationModalComponent, new ModalOptions('lg'));
    let triple: Triple = {
      subject: this.subject,
      predicate: this.predicate,
      object: this.resource,
      tripleScope: this.resource.getAdditionalProperty(ResAttribute.TRIPLE_SCOPE),
      graphs: this.resource.getTripleGraphs()
    };
    modalRef.componentInstance.triple = triple;
  }

  //====== "Copy value to other locales" HANDLER =====

  moveToProperty() {
    this.rvModalService.changeValueProperty(this.section, this.subject, this.predicate, this.resource).then(
      (data: ChangePropertyData) => {
        /*
        If dialog returns nothing, it means that the add/update operation has been performed by it, 
        otherwise, if it returns data, it means that it leaves the update operation to the section
        */
        if (data != null) {
          this.changeProp.emit(data);
        } else {
          this.update.emit();
        }
      },
      () => { }
    );
  }

  //====== "Copy value to other locales" HANDLER =====

  copyLocales() {
    this.rvModalService.copyLocale(this.resource, this.copyLocalesAction.locales).then(
      locales => {
        this.copyLocale.emit(locales);
      },
      () => { }
    );
  }

  //====== "Delete" HANDLER =====

  bulkDelete() {
    this.basicModals.confirm({ key: "STATUS.WARNING" }, { key: "MESSAGES.BULK_DELETE_CONFIRM" }).then(
      () => {
        this.resourcesService.removePredicateObject(this.predicate, this.resource).subscribe(
          () => {
            this.update.emit();
          }
        );
      },
      () => { }
    );
  }

  //==============================

  onImgDblClick() {
    if ((this.resource as ARTURIResource).getRole() == RDFResourceRolesEnum.mention) {
      window.open(this.resource.getNominalValue(), '_blank').focus();
    } else {
      this.onDblClick();
    }
  }
  onImgClick(event: MouseEvent) {
    //shift/ctrl + click on image, handle it as double click
    if (event.ctrlKey || event.metaKey || event.shiftKey) {
      this.onImgDblClick();
    }
  }

  onDblClick() {
    if (this.resource instanceof ARTResource) {
      super.onDblClick();
    } else {
      if (!this.editMenuDisabled) { //menu disabled if value not editable
        this.editLiteral();
      }
    }
  }

  onValueClick(event: ValueClickEvent) {
    this.valueClick.emit(event);
  }

  onKeydown(event: KeyboardEvent) {
    if (event.key == "Escape") {
      this.cancelEdit();
    } else if (event.key == "Enter") {
      if (event.shiftKey || event.altKey || event.ctrlKey) {
        //newline, don't do nothing, the new line is automatically added
      } else {
        this.confirmEdit();
      }
    }
  }

}


/**
 * Enum that tells how to handle the "edit" action.
 */
enum EditActionScenarioEnum {
  xLabel = "xLabel", //edit should allow to change the literal form
  langTaggedLiteral = "langTaggedLiteral", //edit should allow to change the content of the literal without langTag
  typedLiteral = "typedLiteral", //edit should allow to change the content of the literal without datatype
  manchesterDescr = "manchesterDescr", //class axiom description expressed in manchester => edit handled through a manchester editor modal
  section = "section", //edit should be handled ad hoc by the section (an event is emitted) which should implements an editHandler method
  default = "default" //edit should allow to edit the NT form (iri/bnode/...)
}