import { ARTNode, ARTPredicateObjects, ARTResource, ARTURIResource, ResAttribute } from 'src/app/models/ARTResources';
import { ValueFilterLanguages } from 'src/app/models/Properties';
import { SemanticTurkey } from 'src/app/models/Vocabulary';
import { ResourceUtils, SortAttribute } from 'src/app/utils/ResourceUtils';
import { ProjectContext, VBContext } from 'src/app/utils/VBContext';

export class ResourceViewHelper {

  static filterInferredFromPredObjList(predObjList: ARTPredicateObjects[], projectCtx: ProjectContext) {
    let showInferred = VBContext.getWorkingProjectCtx(projectCtx).getProjectPreferences().resViewPreferences.inference;
    if (!showInferred) {
      for (let i = 0; i < predObjList.length; i++) {
        let objList: ARTNode[] = predObjList[i].getObjects();
        for (let j = 0; j < objList.length; j++) {
          let objGraphs: ARTURIResource[] = objList[j].getTripleGraphs();
          if (ResourceUtils.containsNode(objGraphs, new ARTURIResource(SemanticTurkey.inferenceGraph))) {
            objList.splice(j, 1);
            j--;
          }
        }
        //after filtering the objects list, if the predicate has no more objects, remove it from predObjList
        if (objList.length == 0) {
          predObjList.splice(i, 1);
          i--;
        }
      }
    }
  }

  static filterValueLanguageFromPrefObjList(predObjList: ARTPredicateObjects[], projectCtx: ProjectContext) {
    let filterValueLangPref: ValueFilterLanguages = VBContext.getWorkingProjectCtx(projectCtx).getProjectPreferences().filterValueLang;
    if (filterValueLangPref.languages.length == 0 || !filterValueLangPref.enabled) return;
    for (let i = 0; i < predObjList.length; i++) {
      let objList: ARTNode[] = predObjList[i].getObjects();
      for (let j = 0; j < objList.length; j++) {
        let lang = objList[j].getAdditionalProperty(ResAttribute.LANG);
        //remove the object if it has a language not in the languages list of the filter
        if (lang != null && filterValueLangPref.languages.indexOf(lang) == -1) {
          objList.splice(j, 1);
          j--;
        }
      }
      //after filtering the objects list, if the predicate has no more objects, remove it from predObjList
      if (objList.length == 0) {
        predObjList.splice(i, 1);
        i--;
      }
    }
  }

  static filterDeprecatedValues(predObjList: ARTPredicateObjects[], projectCtx: ProjectContext) {
    let showDeprecated = VBContext.getWorkingProjectCtx(projectCtx).getProjectPreferences().resViewPreferences.showDeprecated;
    if (!showDeprecated) {
      for (let i = 0; i < predObjList.length; i++) {
        let objList: ARTNode[] = predObjList[i].getObjects();
        for (let j = 0; j < objList.length; j++) {
          let obj = objList[j];
          if (obj instanceof ARTResource && obj.isDeprecated()) {
            //remove the object if it is deprecated
            objList.splice(j, 1);
            j--;
          }
        }
        //after filtering the objects list, if the predicate has no more objects, remove it from predObjList
        if (objList.length == 0) {
          predObjList.splice(i, 1);
          i--;
        }
      }
    }
  }

  static sortObjects(predObjList: ARTPredicateObjects[], rendering: boolean) {
    //sort by show if rendering is active, uri otherwise
    let attribute: SortAttribute = rendering ? SortAttribute.show : SortAttribute.value;
    for (const po of predObjList) {
      let objList: ARTNode[] = po.getObjects();
      ResourceUtils.sortResources(objList, attribute);
    }
  }

  static resolveImg(predObjLists: ARTPredicateObjects[]) {
    let candidateImageURLs: string[] = [];
    //sort by show if rendering is active, uri otherwise
    for (let poList of predObjLists) {
      let objList: ARTNode[] = poList.getObjects();
      for (let obj of objList) {
        if (obj instanceof ARTURIResource) {
          let resIri = obj.getURI();
          let url = new URL(resIri); //image URL could have query params appended, getting just pathname of URL resolves the issue
          if ((/\.(eps|gif|heic|heif|jpe|jpg|jpeg|tif|tiff|png|svg|svgz|webp)$/i).test(url.pathname)) {
            candidateImageURLs.push(resIri); //is a candidate only if satisfies the regex
          }
        }
      }
    }
    candidateImageURLs.forEach(url => {
      let i = new Image();
      i.onload = () => { //if succesfully loaded => it is an image => search and replace the values in the poList
        predObjLists.forEach(po => {
          po.getObjects().forEach((o: ARTNode, index: number, array: ARTNode[]) => {
            if (o instanceof ARTURIResource && o.getURI() == url) {
              let resToReplace: ARTURIResource = array[index].clone() as ARTURIResource;
              resToReplace.setAdditionalProperty(ResAttribute.IS_IMAGE, true);
              array[index] = resToReplace;
            }
          });
        });
      };
      i.src = url;
    });
  }

}