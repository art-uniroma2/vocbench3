import { Directive, EventEmitter, Input, Output, TemplateRef } from "@angular/core";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalOptions } from 'src/app/modal-dialogs/Modals';
import { ResourceViewServices } from "../../services/resource-view.service";
import { ResViewSettingsModalComponent } from "../res-view-settings/res-view-settings-modal.component";

@Directive()
export abstract class AbstractResourceView {

  @Input() historyControl: TemplateRef<any>;

  //inform the parent container that the resource in RV contains values to validate, so prevent usage of Code editor  
  @Output() pendingValidation = new EventEmitter<boolean>();

  protected resViewService: ResourceViewServices;
  protected modalService: NgbModal;
  constructor(resViewService: ResourceViewServices, modalService: NgbModal) {
    this.resViewService = resViewService;
    this.modalService = modalService;
  }

  /**
   * Opens a modal that allows to edit the resource view settings
   */
  openSettings() {
    const modalRef: NgbModalRef = this.modalService.open(ResViewSettingsModalComponent, new ModalOptions('lg'));
    return modalRef.result;
  }

}