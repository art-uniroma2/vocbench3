import { Directive } from "@angular/core";
import { PropertyServices } from "src/app/services/properties.service";
import { CreationModalServices } from "src/app/modal-dialogs/creation-modals/creation-modals.service";
import { ARTURIResource } from "../../../models/ARTResources";
import { ResViewUtils } from "../../../models/ResourceView";
import { CustomFormsServices } from "../../../services/custom-forms.service";
import { ResourcesServices } from "../../../services/resources.service";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { ResViewModalServices } from "../res-view-modals/resViewModalServices";
import { SectionRenderer } from "./sectionRenderer";
import { TranslateService } from "@ngx-translate/core";

@Directive()
export abstract class SectionRendererMultiRoot extends SectionRenderer {

  /**
   * ATTRIBUTES
   */

  constructor(
    resourcesService: ResourcesServices,
    propService: PropertyServices,
    cfService: CustomFormsServices,
    basicModals: BasicModalServices,
    creationModals: CreationModalServices,
    resViewModals: ResViewModalServices,
    translate: TranslateService
  ) {
    super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
  }

  /**
   * Root properties described in the section.
   * Note that this differs from wellKnownProperties from because this should only contains root properties
   * (those properties that has no super properties among the known properties) not all the known properties
   * (e.g. rdfs:label, skos(xl):pref/alt/hiddenLabel for lexicalizations section)
   */
  protected rootProperties: ARTURIResource[];
  /**
   * Properties described in the section for which exists dedicated add/remove services
   * (e.g. rdfs:label, skos(xl):pref/alt/hiddenLabel for lexicalizations section)
   */
  protected knownProperties: ARTURIResource[];

  /**
   * METHODS
   */

  ngOnInit() {
    super.ngOnInit();
    this.rootProperties = ResViewUtils.getSectionRootProperties(this.section);
    this.knownProperties = ResViewUtils.getSectionKnownProperties(this.section);
  }

  //used in removePredicateObject to know if the removing object is about a well known property
  isKnownProperty(predicate: ARTURIResource): boolean {
    for (const p of this.knownProperties) {
      if (p.equals(predicate)) {
        return true;
      }
    }
    return false;
  }

}