import { Directive } from "@angular/core";
import { Observable, of } from "rxjs";
import { ARTURIResource } from "../../../models/ARTResources";
import { ResViewUtils } from "../../../models/ResourceView";
import { CustomFormsServices } from "../../../services/custom-forms.service";
import { PropertyServices } from "../../../services/properties.service";
import { ResourcesServices } from "../../../services/resources.service";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { CreationModalServices } from "../../../modal-dialogs/creation-modals/creation-modals.service";
import { ResViewModalServices } from "../res-view-modals/resViewModalServices";
import { SectionRenderer } from "./sectionRenderer";
import { TranslateService } from "@ngx-translate/core";

@Directive()
export abstract class SectionRenderSingleRoot extends SectionRenderer {

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices, 
        translate: TranslateService
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    /**
     * ATTRIBUTES
     */

    /**
     * Root property described in the section
     */
    protected rootProperty: ARTURIResource;

    /**
     * METHODS
     */

    ngOnInit() {
        super.ngOnInit();
        this.rootProperty = ResViewUtils.getSectionRootProperties(this.section)[0];
    }

    getPredicateToEnrich(): Observable<ARTURIResource> {
        return of(this.rootProperty);
    }

}