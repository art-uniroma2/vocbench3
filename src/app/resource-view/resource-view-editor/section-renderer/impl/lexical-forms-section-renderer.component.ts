import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { from, Observable, of } from 'rxjs';
import { PropertyServices } from "src/app/services/properties.service";
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { ARTNode, ARTResource, ARTURIResource } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { OntoLex } from "../../../../models/Vocabulary";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { OntoLexLemonServices } from "../../../../services/ontolex-lemon.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../../../modal-dialogs/browsing-modals/browsing-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { NewResourceWithLiteralCfModalReturnData } from "../../../../modal-dialogs/creation-modals/newResourceModal/shared/new-resource-with-literal-cf-modal.component";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { SectionRendererMultiRoot } from "../sectionRendererMultiRoot";

@Component({
    selector: "lexical-forms-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class LexicalFormsSectionRendererComponent extends SectionRendererMultiRoot {

    section = ResViewSection.lexicalForms;
    addBtnImgSrc = "./assets/images/icons/actions/objectProperty_create.png";

    private lexiconLang: string; //cache the language of the lexicon

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
        private ontolexService: OntoLexLemonServices, 
        private browsingModals: BrowsingModalServices
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    add(predicate: ARTURIResource, _propChangeable: boolean) {
        if (!this.isKnownProperty(predicate)) {
            this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "MESSAGES.UNHANDLED_LEXICAL_FORM_PROPERTY", params: { property: predicate.getShow() } }, ModalType.warning);
            return;
        }

        this.getLexiconLang().subscribe(
            lang => {
                this.lexiconLang = lang;
                this.creationModals.newResourceWithLiteralCf({ key: "COMMONS.ACTIONS.CREATE_X", params: { x: predicate.getShow() } }, OntoLex.form, true, "Written rep", this.lexiconLang, { constrain: true, locale: true }).then(
                    (data: NewResourceWithLiteralCfModalReturnData) => {
                        if (predicate.equals(OntoLex.canonicalForm)) {
                            this.ontolexService.setCanonicalForm(this.resource as ARTURIResource, data.literal, data.uriResource, data.cfValue).subscribe(
                                () => this.update.emit(null)
                            );
                        } else if (predicate.equals(OntoLex.otherForm)) {
                            this.ontolexService.addOtherForm(this.resource as ARTURIResource, data.literal, data.uriResource, data.cfValue).subscribe(
                                () => this.update.emit(null)
                            );
                        }
                    },
                    () => { }
                );
            }
        );
    }

    private getLexiconLang(): Observable<string> {
        if (this.lexiconLang == null) {
            return this.ontolexService.getLexicalEntryLanguage(this.resource as ARTURIResource);
        } else {
            return of(this.lexiconLang);
        }
    }


    getPredicateToEnrich(): Observable<ARTURIResource> {
        return from(
            this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }, this.rootProperties).then(
                (selectedProp: any) => {
                    return selectedProp;
                },
                () => { }
            )
        );
    }

    checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
        return of(value instanceof ARTURIResource);
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        this.getRemoveFunction(predicate, object).subscribe(
            () => this.update.emit()
        );
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        return this.ontolexService.removeForm(this.resource, predicate, object as ARTResource);
    }

}