import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from "rxjs";
import { ARTNode, ARTURIResource } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { SkosServices } from "../../../../services/skos.service";
import { VBEventHandler } from "../../../../utils/VBEventHandler";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { MultiActionFunction } from "../multipleActionHelper";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
    selector: "top-concepts-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class TopConceptsSectionRendererComponent extends SectionRenderSingleRoot {

    section = ResViewSection.topconceptof;
    addBtnImgSrc = "./assets/images/icons/actions/conceptScheme_create.png";

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
        private skosService: SkosServices,
        private eventHandler: VBEventHandler
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    //add as top concept
    add(predicate: ARTURIResource, propChangeable: boolean) {
        this.resViewModals.addPropertyValue({ key: "DATA.ACTIONS.SET_AS_TOP_CONCEPT" }, this.resource, predicate, propChangeable).then(
            (data: any) => {
                let prop: ARTURIResource = data.property;
                let values: ARTURIResource[] = data.value;
                let addFunctions: MultiActionFunction[] = [];
                values.forEach((v: ARTURIResource) => {
                    addFunctions.push({
                        function: this.skosService.addTopConcept(this.resource as ARTURIResource, v, prop),
                        value: v
                    });
                });
                this.addMultiple(addFunctions);
            },
            () => { }
        );
    }

    checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
        return of(value instanceof ARTURIResource);
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        this.getRemoveFunction(predicate, object).subscribe(
            () => {
                if (this.rootProperty.equals(predicate)) {
                    //=> emits conceptRemovedAsTopConceptEvent cause it has not been fired by the generic service (removeValue)
                    this.eventHandler.conceptRemovedAsTopConceptEvent.emit({ concept: this.resource as ARTURIResource, scheme: object as ARTURIResource });
                }
                this.update.emit();
            }
        );
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        if (this.rootProperty.equals(predicate)) { //removing skos:topConceptOf relation
            return this.skosService.removeTopConcept(this.resource as ARTURIResource, object as ARTURIResource);
        } else { //predicate is some subProperty of skos:topConceptOf
            return this.resourcesService.removeValue(this.resource, predicate, object);
        }
    }

}