import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { forkJoin, Observable, of } from "rxjs";
import { ARTBNode, ARTLiteral, ARTNode, ARTURIResource, RDFResourceRolesEnum } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { ManchesterServices } from "../../../../services/manchester.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { AddPropertyValueModalReturnData } from "../../res-view-modals/add-property-value-modal.component";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { MultiActionFunction } from "../multipleActionHelper";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
    selector: "ranges-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class RangesSectionRendererComponent extends SectionRenderSingleRoot {

    section = ResViewSection.ranges;
    addBtnImgSrc = "./assets/images/icons/actions/cls_create.png";

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
        private manchService: ManchesterServices
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    add(predicate: ARTURIResource, propChangeable: boolean) {
        this.resViewModals.addPropertyValue({ key: "DATA.ACTIONS.ADD_RANGE" }, this.resource, predicate, propChangeable).then(
            (data: AddPropertyValueModalReturnData) => {
                let prop: ARTURIResource = data.property;
                let value: any = data.value; //value can be a class, manchester Expression, or a datatype (if resource is a datatype prop)
                /** If the rerource is a datatype property, value could be a:
                 * - datatype (ARTURIResource[])
                 * - datarange (ARTLiteral[])
                 */
                if (this.resource.getRole() == RDFResourceRolesEnum.datatypeProperty) {
                    if (value instanceof Array) {
                        let addFunctions: MultiActionFunction[] = [];

                        if (value[0] instanceof ARTLiteral) { //datarange
                            this.propService.setDataRange(this.resource as ARTURIResource, value).subscribe(
                                () => this.update.emit(null)
                            );
                        } else { //instance of ARTURIResource => datatype
                            if (prop.equals(this.rootProperty)) { //it's using rdfs:range
                                value.forEach((v: ARTURIResource) => {
                                    addFunctions.push({
                                        function: this.propService.addPropertyRange(this.resource as ARTURIResource, v),
                                        value: v
                                    });
                                });
                            } else { //it's using a subProperty of rdfs:range
                                value.forEach((v: ARTURIResource) => {
                                    addFunctions.push({
                                        function: this.resourcesService.addValue(this.resource, prop, v),
                                        value: v
                                    });
                                });
                            }
                            this.addMultiple(addFunctions);
                        }
                    }
                }
                /** Otherwise, if the resource is a object/annotation/ontologyProperty, value could be a:
                 * - resource
                 * - manchester expression
                 */
                else {
                    if (typeof value == "string") {
                        this.manchService.createRestriction(this.resource as ARTURIResource, prop, value, data.skipSemCheck).subscribe(
                            () => this.update.emit(null)
                        );
                    } else { //value is ARTURIResource[] (class(es) selected from the tree)
                        let addFunctions: MultiActionFunction[] = [];
                        if (prop.equals(this.rootProperty)) { //it's using rdfs:range
                            value.forEach((v: ARTURIResource) => {
                                addFunctions.push({
                                    function: this.propService.addPropertyRange(this.resource as ARTURIResource, v),
                                    value: v
                                });
                            });
                        } else { //it's using a subProperty of rdfs:range
                            value.forEach((v: ARTURIResource) => {
                                addFunctions.push({
                                    function: this.resourcesService.addValue(this.resource, prop, v),
                                    value: v
                                });
                            });
                        }
                        this.addMultiple(addFunctions);
                    }
                }
            },
            () => { }
        );
    }

    checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
        if (predicate.getRole() == RDFResourceRolesEnum.datatypeProperty) { //datatype property accepts literal as value
            return of(value instanceof ARTLiteral);
        } else {
            return of(value instanceof ARTURIResource);
        }
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        /**
         * An object in this section could be:
         * - Class (type: URI, role: cls)
         * - Datatype (type: URI, role: individual)
         * - Manchester expression (type: BNode, role: cls)
         * - DataRange (type: BNode, role: dataRange)
         */
        if (object instanceof ARTBNode) { //object is manchExpr or dataRange
            if (object.getRole() == RDFResourceRolesEnum.dataRange) {
                this.propService.removeDataranges(this.resource as ARTURIResource, object).subscribe(
                  () => this.update.emit(null)
                );
            } else { //role cls => manchester expression or simple class
                this.manchService.isClassAxiom(object).subscribe(
                    isClassAxiom => {
                        if (isClassAxiom) {
                            this.manchService.removeExpression(this.resource as ARTURIResource, predicate, object).subscribe(
                              () => this.update.emit(null)
                            );
                        } else {
                            this.getRemoveFunction(predicate, object).subscribe(
                              () => this.update.emit()
                            );
                        }
                    }
                );
            }
        } else { //object instanceof ARTURIResource => object is class or datatype
            this.getRemoveFunction(predicate, object).subscribe(
                () => this.update.emit()
            );
        }
    }

    //@override
    removeAllValues(predicate: ARTURIResource) {
        for (const po of this.poList) {
            let objList: ARTNode[] = po.getObjects();
            //collects all the suspicious class axioms, namely the range that are BNode
            let suspClassAxioms: ARTBNode[] = [];
            let notClassAxioms: ARTURIResource[] = [];
            for (const object of objList) {
                if (object instanceof ARTBNode) {
                    suspClassAxioms.push(object);
                } else {
                    notClassAxioms.push(object as ARTURIResource);
                }
            }
            if (suspClassAxioms.length > 0) { //there is at least a bnode, check if it is a class axiom
                //collects the functions to do the checks
                let isClassAxiomFnArray: any[] = [];
                for (const a of suspClassAxioms) {
                    isClassAxiomFnArray.push(this.manchService.isClassAxiom(a));
                }
                let removeFnArray: any[] = [];
                //collects the remove function for all the not class axioms ranges
                for (const a of notClassAxioms) {
                    removeFnArray.push(this.getRemoveFunction(predicate, a));
                }
                //collects remove function for all the suspicious class axioms ranges
                forkJoin(isClassAxiomFnArray).subscribe(
                    results => {
                        for (let j = 0; j < results.length; j++) {
                            if (results[j]) { //is class axiom
                                removeFnArray.push(this.manchService.removeExpression(this.resource as ARTURIResource, predicate, suspClassAxioms[j]));
                            } else { //not a class axiom
                                removeFnArray.push(this.getRemoveFunction(predicate, suspClassAxioms[j]));
                            }
                        }
                        this.removeAllRicursively(removeFnArray);
                    }
                );
            } else { //all range are IRI, there's no need to check for class axioms
                let removeFnArray: any[] = [];
                for (const o of objList) {
                    removeFnArray.push(this.getRemoveFunction(predicate, o));
                }
                this.removeAllRicursively(removeFnArray);
            }
        }
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        if (this.rootProperty.equals(predicate)) { //removing rdfs:range relation
            return this.propService.removePropertyRange(this.resource as ARTURIResource, object as ARTURIResource);
        } else { //removing subProperty of rdfs:range
            return this.resourcesService.removeValue(this.resource, predicate, object);
        }
    }

}