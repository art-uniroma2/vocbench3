import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from 'rxjs';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { ARTNode, ARTURIResource, RDFResourceRolesEnum } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { RdfsMembersModalReturnData } from "../../res-view-modals/rdfs-members-modal.component";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
    selector: "rdfs-members-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class RdfsMembersSectionRendererComponent extends SectionRenderSingleRoot {

    section = ResViewSection.rdfsMembers;
    addBtnImgSrc = "./assets/images/icons/actions/property_create.png";

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    add(predicate: ARTURIResource, propChangeable: boolean) {
        if (this.resource.getRole() == RDFResourceRolesEnum.ontolexLexicalEntry) {
            this.basicModals.confirm({ key: "DATA.ACTIONS.ADD_MEMBER" }, { key: "MESSAGES.EDIT_RDF_MEMBER_WARN_CONFIRM" }, ModalType.warning).then(
                () => {
                    this.resViewModals.addRdfsMembers(predicate, propChangeable).then(
                        (data: RdfsMembersModalReturnData) => {
                            let prop: ARTURIResource = data.property;
                            let member: ARTURIResource = data.value;
                            this.resourcesService.addValue(this.resource, prop, member).subscribe(
                                () => {
                                    this.update.emit(null);
                                }
                            );
                        },
                        () => { }
                    );
                },
                () => { }
            );
        }
    }

    checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
        return of(value instanceof ARTURIResource);
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        //warning message only when deleting members of lexical entry
        if (this.resource.getRole() == RDFResourceRolesEnum.ontolexLexicalEntry) {
            this.basicModals.confirm({ key: "COMMONS.ACTIONS.DELETE_MEMBER" }, { key: "MESSAGES.DELETE_RDF_MEMBER_WARN_CONFIRM" }, ModalType.warning).then(
                () => {
                    this.getRemoveFunction(predicate, object).subscribe(
                        () => {
                            this.update.emit();
                        }
                    );
                },
                () => { }
            );
        } else {
            this.getRemoveFunction(predicate, object).subscribe(
                () => {
                    this.update.emit();
                }
            );
        }
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        return this.resourcesService.removeValue(this.resource, predicate, object);
    }

}