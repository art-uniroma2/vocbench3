import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { from, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { PrefLabelClashMode } from "src/app/models/Properties";
import { VBContext } from "src/app/utils/VBContext";
import { ARTLiteral, ARTNode, ARTURIResource } from "../../../../models/ARTResources";
import { Language } from "../../../../models/LanguagesCountries";
import { ResViewSection } from "../../../../models/ResourceView";
import { RDFS, SKOS, SKOSXL } from "../../../../models/Vocabulary";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { PropertyServices, RangeResponse } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../../../modal-dialogs/browsing-modals/browsing-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { NewXLabelModalReturnData } from "../../../../modal-dialogs/creation-modals/newResourceModal/skos/new-xlabel-modal.component";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { LexicalizationEnrichmentHelper } from "../lexicalizationEnrichmentHelper";
import { MultiActionError, MultiActionFunction } from "../multipleActionHelper";
import { EnrichmentType, PropertyEnrichmentHelper, PropertyEnrichmentInfo } from "../propertyEnrichmentHelper";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
    selector: "properties-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class PropertiesSectionRendererComponent extends SectionRenderSingleRoot {

    section = ResViewSection.properties;
    addBtnImgSrc = "./assets/images/icons/actions/property_create.png";

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
        private browsingModals: BrowsingModalServices,
        private lexicalizationEnrichmentHelper: LexicalizationEnrichmentHelper
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
        this.sectionCollapsed = (this.poList.length > 5);
    }

    add(predicate: ARTURIResource) {
        /**
         * Lexicalization properties should not be enriched from this section, it should be used the proper LexicalizationSectionRenderer instead.
         * Anyway, here the lexicalization properties are handled ad hoc and not by the enrichProperty method in order to
         * be handled in the same way of LexicalizationSectionRenderer
         */
        if (
            predicate.equals(SKOSXL.prefLabel) || predicate.equals(SKOSXL.altLabel) || predicate.equals(SKOSXL.hiddenLabel) ||
            predicate.equals(SKOS.prefLabel) || predicate.equals(SKOS.altLabel) || predicate.equals(SKOS.hiddenLabel) ||
            predicate.equals(RDFS.label)
        ) {
            PropertyEnrichmentHelper.getPropertyEnrichmentInfo(predicate, this.propService, this.basicModals).subscribe(
                (data: PropertyEnrichmentInfo) => {
                    if (data.type == null) { //range selection canceled
                        return;
                    } else if (data.type == EnrichmentType.customForm) { //if a custom form has been defined, use it
                        this.enrichWithCustomForm(predicate, data.form);
                    } else { //otherwise (default case, where type is "resource" and rangeCollection is [skosxl:Label]) use the proper enrichment service
                        this.enrichWithLabel(predicate);
                    }
                }
            );
        } else {
            this.enrichProperty(predicate);
        }
    }

    private enrichWithLabel(predicate: ARTURIResource) {
        if (predicate.equals(SKOSXL.prefLabel) || predicate.equals(SKOSXL.altLabel) || predicate.equals(SKOSXL.hiddenLabel)) { //SKOSXL
            let prefLabelPred: boolean = predicate.equals(SKOSXL.prefLabel);
            this.creationModals.newXLabel({ key: "COMMONS.ACTIONS.ADD_X", params: { x: predicate.getShow() } }, null, null, null, null, null, { enabled: true, allowSameLang: !prefLabelPred }).then(
                (data: NewXLabelModalReturnData) => {
                    this.addMultipleLabelValues(predicate, data.labels, data.cls);
                },
                () => { }
            );
        } else { //SKOS or RDFS
            let prefLabelPred: boolean = predicate.equals(SKOS.prefLabel);
            this.creationModals.newPlainLiteral({ key: "COMMONS.ACTIONS.ADD_X", params: { x: predicate.getShow() } }, null, null, null, null, null, { enabled: true, allowSameLang: !prefLabelPred }).then(
                (labels: ARTLiteral[]) => {
                    this.addMultipleLabelValues(predicate, labels);
                },
                () => { }
            );
        }
    }

    /**
     * This method is copied from the LexicalizationSectionRenderer.
     * It is replicated here, for the same reasons explained in this.add(), for handling the enrichment of the lexicalization properties.
     */
    private addMultipleLabelValues(predicate: ARTURIResource, labels: ARTLiteral[], cls?: ARTURIResource) {
        let addFunctions: MultiActionFunction[] = [];
        let errorHandler: (errors: MultiActionError[]) => void;

        //SKOS or SKOSXL lexicalization predicates
        let prefLabelClashMode = VBContext.getWorkingProjectCtx().getProjectSettings().prefLabelClashMode;
        let checkClash: boolean = prefLabelClashMode != PrefLabelClashMode.allow; //enable the clash checks only if not "allow" ("forbid"|"warning")
        if (
            predicate.equals(SKOSXL.prefLabel) || predicate.equals(SKOSXL.altLabel) || predicate.equals(SKOSXL.hiddenLabel) ||
            predicate.equals(SKOS.prefLabel) || predicate.equals(SKOS.altLabel) || predicate.equals(SKOS.hiddenLabel)
        ) {
            labels.forEach((label: ARTLiteral) => {
                addFunctions.push({
                    function: this.lexicalizationEnrichmentHelper.getSkosSkosxlAddLabelFn(this.resource as ARTURIResource, predicate, label, cls, checkClash, checkClash, false),
                    value: label
                });
            });
            errorHandler = (errors: MultiActionError[]) => {
                if (errors.length == 1) { //if only one error, try to handle it
                    let err: MultiActionError = errors[0];
                    if (
                        ((err.error.name.endsWith("PrefPrefLabelClashException") || err.error.name.endsWith("PrefAltLabelClashException")) && prefLabelClashMode == PrefLabelClashMode.warning) ||
                        err.error.name.endsWith("BlacklistForbiddendException")
                    ) {
                        this.lexicalizationEnrichmentHelper.handleForceAddLexicalizationError(err.error, this.resource as ARTURIResource, predicate, err.value as ARTLiteral, cls, checkClash, checkClash, false, { eventEmitter: this.update });
                    } else { //other error that cannot be handled with a "force action"
                        this.handleSingleMultiAddError(err);
                    }
                } else {
                    this.handleMultipleMultiAddError(errors);
                }
            };
        } else { //rdfs:label (or maybe a custom property) for which doens't exist a dedicated service
            labels.forEach((label: ARTLiteral) => {
                addFunctions.push({
                    function: this.resourcesService.addValue(this.resource, predicate, label),
                    value: label
                });
            });
        }
        this.addMultiple(addFunctions, errorHandler);
    }

    getPredicateToEnrich(): Observable<ARTURIResource> {
        return from(
            this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }, null, this.resource as ARTURIResource).then(
                selectedProp => {
                    return selectedProp;
                },
                () => { return null; }
            )
        );
    }

    checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
        return this.propService.getRange(predicate).pipe(
            mergeMap(range => {
                return of(RangeResponse.isRangeCompliant(range, value));
            })
        );
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        this.getRemoveFunction(predicate, object).subscribe(
            () => this.update.emit(null)
        );
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        return this.resourcesService.removeValue(this.resource, predicate, object);
    }

    copyLocaleHandler(predicate: ARTURIResource, eventData: { value: ARTNode, locales: Language[] }) {
        let addFunctions: MultiActionFunction[] = [];
        //this function is the handler of an event invoked in properties only when the value is a plain literal, so the cast is safe
        let value: ARTLiteral = eventData.value as ARTLiteral;
        eventData.locales.forEach(l => {
            let newValue: ARTLiteral = new ARTLiteral(value.getValue(), null, l.tag);
            addFunctions.push({
                function: this.resourcesService.addValue(this.resource, predicate, newValue),
                value: newValue
            });
        });
        this.addMultiple(addFunctions);
    }

}