import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { from, Observable, of } from 'rxjs';
import { mergeMap } from "rxjs/operators";
import { CreationModalServices } from "src/app/modal-dialogs/creation-modals/creation-modals.service";
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { ARTBNode, ARTNode, ARTResource, ARTURIResource } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { OWL, RDFS } from "../../../../models/Vocabulary";
import { ClassesServices } from "../../../../services/classes.service";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { ManchesterServices } from "../../../../services/manchester.service";
import { PropertyServices, RangeResponse } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from '../../../../modal-dialogs/browsing-modals/browsing-modals.service';
import { AddPropertyValueModalReturnData } from "../../res-view-modals/add-property-value-modal.component";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { MultiActionFunction } from "../multipleActionHelper";
import { SectionRendererMultiRoot } from "../sectionRendererMultiRoot";

@Component({
    selector: "class-axiom-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class ClassAxiomSectionRendererComponent extends SectionRendererMultiRoot {

    section = ResViewSection.classaxioms;
    addBtnImgSrc = "./assets/images/icons/actions/cls_create.png";

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
        private clsService: ClassesServices, 
        private manchService: ManchesterServices,
        private browsingModals: BrowsingModalServices
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    /**
     * Based on the property opens the proper dialog to enrich it
     * oneOf opens a modal to create a list of instances
     * intersectionOf and unionOf opens a modal to create a list of classes (or expression)
     * subClassOf, equivalentClass, disjointWith, complementOf asks the user if choose an existing class
     * (then opens a class tree modal) or to create a manchester expression (then opens a prompt modal) 
     */
    add(predicate: ARTURIResource) {
        if (!this.isKnownProperty(predicate)) {
            this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "MESSAGES.UNHANDLED_AXIOM_PROPERTY", params: { property: predicate.getShow() } }, ModalType.warning);
            return;
        }

        //if the predicate is oneOf open a modal to create an instance list, otherwise ask the user to make a further decision
        if (predicate.equals(OWL.oneOf)) {
            this.createInstanceList(predicate);
        } else if (predicate.equals(OWL.intersectionOf) || predicate.equals(OWL.unionOf)) {
            this.createClassList(predicate);
        } else { //rdfs:subClassOf, owl:equivalentClass, owl:disjointWith, owl:complementOf
            //ask the user to choose to add an existing class or to add a class expression
            this.resViewModals.addPropertyValue({ key: "COMMONS.ACTIONS.ADD_X", params: { x: predicate.getShow() } }, this.resource, predicate, false).then(
                (data: AddPropertyValueModalReturnData) => {
                    let value: any = data.value; //value can be a class or a manchester Expression
                    if (typeof value == "string") {
                        this.manchService.createRestriction(this.resource as ARTURIResource, predicate, value, data.skipSemCheck).subscribe(
                            () => this.update.emit(null)
                        );
                    } else { //value is an ARTURIResource[] (class(es) selected from the tree)
                        let values: ARTURIResource[] = data.value;
                        let addFunctions: MultiActionFunction[] = [];

                        if (predicate.equals(RDFS.subClassOf)) {
                            values.forEach((v: ARTURIResource) => {
                                addFunctions.push({
                                    function: this.clsService.addSuperCls(this.resource as ARTURIResource, v),
                                    value: v
                                });
                            });
                        } else {
                            values.forEach((v: ARTURIResource) => {
                                addFunctions.push({
                                    function: this.resourcesService.addValue(this.resource, predicate, v),
                                    value: v
                                });
                            });
                        }
                        this.addMultiple(addFunctions);
                    }
                },
                () => { }
            );
        }
    }

    getPredicateToEnrich(): Observable<ARTURIResource> {
        return from(
            this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }, this.rootProperties).then(
                (selectedProp: any) => {
                    return selectedProp;
                },
                () => { }
            )
        );
    }

    checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
        return this.propService.getRange(predicate).pipe(
            mergeMap(range => {
                return of(RangeResponse.isRangeCompliant(range, value));
            })
        );
    }

    editHandler(predicate: ARTURIResource, object: ARTNode) {
        /* 
        in this section, this handler can be triggered only when user edits a list bNode representing a unionOf or intersectionOf list
        (see editable-resource component, the edit event is emitted only in such case).
        Handle such scenario by recreating a new list
        */
       //this check is actually unnecessary since if the predicate is not one of these, editHandler would never be triggered from editable-resource, but better to be careful
        if (predicate.equals(OWL.unionOf) || predicate.equals(OWL.intersectionOf)) { 
            this.resViewModals.createClassList({ key: "COMMONS.ACTIONS.EDIT_X", params: { x: predicate.getShow() } }).then(
                (classes: ARTResource[]) => {
                    //now, in order to update, remove the old expression and add the new one
                    let removeFn: Observable<void> = this.getRemoveFunctionImpl(predicate, object);
                    let addFn: Observable<void>;
                    if (predicate.equals(OWL.intersectionOf)) {
                        addFn = this.clsService.addIntersectionOf(this.resource as ARTURIResource, classes);
                    } else if (predicate.equals(OWL.unionOf)) {
                        addFn = this.clsService.addUnionOf(this.resource as ARTURIResource, classes);
                    }
                    removeFn.subscribe(
                        () => {
                            addFn.subscribe(
                                () => this.update.emit()
                            );
                        }
                    );
                },
                () => { }
            );
        }

    }

    /**
     * Opens a modal to create a class list.
     * Called to enrich intersectionOf and unionOf
     */
    private createClassList(predicate: ARTURIResource) {
        this.resViewModals.createClassList({ key: "COMMONS.ACTIONS.ADD_X", params: { x: predicate.getShow() } }).then(
            (classes: ARTResource[]) => {
                if (predicate.equals(OWL.intersectionOf)) {
                    this.clsService.addIntersectionOf(this.resource as ARTURIResource, classes).subscribe(
                        () => this.update.emit(null)
                    );
                } else if (predicate.equals(OWL.unionOf)) {
                    this.clsService.addUnionOf(this.resource as ARTURIResource, classes).subscribe(
                      () => this.update.emit(null)
                    );
                }
            },
            () => { }
        );
    }

    /**
     * Opens a modal to create an instance list
     * Called to enrich oneOf
     */
    private createInstanceList(predicate: ARTURIResource) {
        this.resViewModals.createInstanceList({ key: "COMMONS.ACTIONS.ADD_X", params: { x: predicate.getShow() } }).then(
            (instances: any) => {
                this.clsService.addOneOf(this.resource as ARTURIResource, instances).subscribe(
                    () => this.update.emit(null)
                );
            },
            () => { }
        );
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        this.getRemoveFunction(predicate, object).subscribe(
          () => this.update.emit(null)
        );
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        if (this.isKnownProperty(predicate)) { //if it is removing a value about a root property, call the specific method
            if (predicate.equals(RDFS.subClassOf)) {
                if (object.isBNode()) {
                    return this.manchService.removeExpression(this.resource as ARTURIResource, predicate, object);
                } else {
                    return this.clsService.removeSuperCls(this.resource as ARTURIResource, object as ARTURIResource);
                }
            } else if (predicate.equals(OWL.equivalentClass) || predicate.equals(OWL.disjointWith) || predicate.equals(OWL.complementOf)) {
                if (object.isBNode()) {
                    return this.manchService.removeExpression(this.resource as ARTURIResource, predicate, object);
                } else {
                    return this.resourcesService.removeValue(this.resource as ARTURIResource, predicate, object);
                }
            } else if (predicate.equals(OWL.intersectionOf)) {
                return this.clsService.removeIntersectionOf(this.resource as ARTURIResource, object);
            } else if (predicate.equals(OWL.unionOf)) {
                return this.clsService.removeUnionOf(this.resource as ARTURIResource, object as ARTBNode);
            } else if (predicate.equals(OWL.oneOf)) {
                return this.clsService.removeOneOf(this.resource as ARTURIResource, object as ARTBNode);
            } else { //not one of the known property (should never happen)
                return this.resourcesService.removeValue(this.resource, predicate, object);
            }
        } else { //predicate is some subProperty of a root property
            return this.resourcesService.removeValue(this.resource, predicate, object);
        }
    }

}