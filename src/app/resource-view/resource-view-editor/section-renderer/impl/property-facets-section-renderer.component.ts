import { Component, Input } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from "rxjs";
import { ARTNode, ARTURIResource, ResAttribute } from "../../../../models/ARTResources";
import { AddAction, PropertyFacet, PropertyFacetsEnum, ResViewSection } from "../../../../models/ResourceView";
import { OWL, RDF } from "../../../../models/Vocabulary";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { CRUDEnum, ResourceViewAuthEvaluator } from "../../../../utils/AuthorizationEvaluator";
import { ResourceUtils } from "../../../../utils/ResourceUtils";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { AddPropertyValueModalReturnData } from "../../res-view-modals/add-property-value-modal.component";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { MultiActionFunction } from "../multipleActionHelper";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
  selector: "property-facets-renderer",
  templateUrl: "./property-facets-section-renderer.component.html",
  standalone: false
})
export class PropertyFacetsSectionRendererComponent extends SectionRenderSingleRoot {

  @Input() facets: PropertyFacet[];

  AddAction = AddAction; //workaround for using enum in template

  section = ResViewSection.facets;
  addBtnImgSrc = "./assets/images/icons/actions/property_create.png";

  constructor(
    resourcesService: ResourcesServices,
    propService: PropertyServices,
    cfService: CustomFormsServices,
    basicModals: BasicModalServices,
    creationModals: CreationModalServices,
    resViewModals: ResViewModalServices,
    translate: TranslateService,
  ) {
    super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  add(predicate: ARTURIResource, propChangeable: boolean) {
    this.resViewModals.addPropertyValue({ key: "DATA.ACTIONS.ADD_INVERSE_PROPERTY" }, this.resource, predicate, propChangeable).then(
      (data: AddPropertyValueModalReturnData) => {
        let prop: ARTURIResource = data.property;
        let inverse: boolean = data.inverseProperty;
        let values: ARTURIResource[] = data.value;

        let addFunctions: MultiActionFunction[] = [];
        values.forEach((v: ARTURIResource) => {
          addFunctions.push({
            function: this.propService.addInverseProperty(this.resource as ARTURIResource, v, prop, inverse),
            value: v
          });
        });
        this.addMultiple(addFunctions);
      },
      () => { }
    );
  }

  // addExternalValue() {
  //   //not allowed by this section, so left empty
  // }

  checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
    return of(value instanceof ARTURIResource);
  }

  removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
    this.getRemoveFunction(predicate, object).subscribe(
      () => this.update.emit(null)
    );
  }

  getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
    return this.propService.removeInverseProperty(this.resource as ARTURIResource, object as ARTURIResource, predicate);
  }

  changeFacet(facet: PropertyFacet) {
    if (facet.name == PropertyFacetsEnum.symmetric) {
      this.setPropertyFacet(OWL.symmetricProperty, facet.value);
    } else if (facet.name == PropertyFacetsEnum.asymmetric) {
      this.setPropertyFacet(OWL.asymmetricProperty, facet.value);
    } else if (facet.name == PropertyFacetsEnum.functional) {
      this.setPropertyFacet(OWL.functionalProperty, facet.value);
    } else if (facet.name == PropertyFacetsEnum.inverseFunctional) {
      this.setPropertyFacet(OWL.inverseFunctionalProperty, facet.value);
    } else if (facet.name == PropertyFacetsEnum.reflexive) {
      this.setPropertyFacet(OWL.reflexiveProperty, facet.value);
    } else if (facet.name == PropertyFacetsEnum.irreflexive) {
      this.setPropertyFacet(OWL.irreflexiveProperty, facet.value);
    } else if (facet.name == PropertyFacetsEnum.transitive) {
      this.setPropertyFacet(OWL.transitiveProperty, facet.value);
    }
  }

  private setPropertyFacet(propertyClass: ARTURIResource, value: boolean) {
    if (value) {
      this.resourcesService.addValue(this.resource, RDF.type, propertyClass).subscribe(
        () => this.update.emit(null)
      );
    } else {
      this.resourcesService.removeValue(this.resource, RDF.type, propertyClass).subscribe(
        () => this.update.emit(null)
      );
    }
  }

  isChangeFacetDisabled(facet: PropertyFacet) {
    return (
      !facet.explicit ||
      (!this.resource.getAdditionalProperty(ResAttribute.EXPLICIT) && !ResourceUtils.isResourceInStagingAdd(this.resource)) ||
      this.readonly || !ResourceViewAuthEvaluator.isAuthorized(this.section, CRUDEnum.U, this.resource)
    );
  }

  /**
   * In this section, the edit is delegated only for the ObjectProperty expression (manchester) inside the inverseOf pred-objects panel
   */
  editHandler(predicate: ARTURIResource, object: ARTNode) {
    this.resViewModals.addPropertyValue({ key: "DATA.ACTIONS.SET_INVERSE_PROPERTY" }, this.resource, predicate, false).then(
      (data: AddPropertyValueModalReturnData) => {
        //first remove the previous value
        this.getRemoveFunctionImpl(predicate, object).subscribe(
          () => {
            //then add the new value
            let prop: ARTURIResource = data.property;
            let inverse: boolean = data.inverseProperty;
            let values: ARTURIResource[] = data.value;
            let addFunctions: MultiActionFunction[] = [];
            values.forEach((v: ARTURIResource) => {
              addFunctions.push({
                function: this.propService.addInverseProperty(this.resource as ARTURIResource, v, prop, inverse),
                value: v
              });
            });
            this.addMultiple(addFunctions);
          }
        );
      },
      () => { }
    );
  }

}