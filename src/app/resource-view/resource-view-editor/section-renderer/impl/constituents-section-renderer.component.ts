import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from 'rxjs';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { ARTNode, ARTURIResource } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { OntoLexLemonServices } from "../../../../services/ontolex-lemon.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { ConstituentListCreatorModalReturnData } from "../../res-view-modals/constituent-list-creator-modal.component";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
    selector: "constituents-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class ConstituentsSectionRendererComponent extends SectionRenderSingleRoot {

    section = ResViewSection.constituents;
    addBtnImgSrc = "./assets/images/icons/actions/objectProperty_create.png";

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
        private ontolexService: OntoLexLemonServices
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    add(_predicate: ARTURIResource, _propChangeable: boolean) {
        this.resViewModals.createConstituentList({ key: "DATA.ACTIONS.CREATE_CONSTITUENTS_LIST" }).then(
            (data: ConstituentListCreatorModalReturnData) => {
                this.ontolexService.setLexicalEntryConstituents(this.resource as ARTURIResource, data.list, data.ordered).subscribe(
                    () => {
                        this.update.emit();
                    }
                );
            },
            () => { }
        );
    }

    checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
        return of(value instanceof ARTURIResource);
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        this.basicModals.confirm({ key: "DATA.ACTIONS.DELETE_CONSTITUENT" }, { key: "MESSAGES.DELETE_CONSTITUENT_WARN_CONFIRM" }, ModalType.warning).then(
            () => {
                this.getRemoveFunction(predicate, object).subscribe(
                    () => {
                        this.update.emit();
                    }
                );
            },
            () => { }
        );
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        return this.resourcesService.removeValue(this.resource, predicate, object);
    }

}