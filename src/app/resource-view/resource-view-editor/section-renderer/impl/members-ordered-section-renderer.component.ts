import { Component, SimpleChanges } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from "rxjs";
import { ResourceUtils } from 'src/app/utils/ResourceUtils';
import { ARTNode, ARTResource, ARTURIResource, ResAttribute } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { SKOS } from "../../../../models/Vocabulary";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { SkosServices } from "../../../../services/skos.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { MultiActionFunction } from "../multipleActionHelper";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
  selector: "members-ordered-renderer",
  templateUrl: "./members-ordered-section-renderer.component.html",
  standalone: false
})
export class MembersOrderedSectionRendererComponent extends SectionRenderSingleRoot {

  section = ResViewSection.membersOrdered;
  addBtnImgSrc = "./assets/images/icons/actions/skosCollection_create.png";

  selectedMember: ARTResource;

  constructor(
    resourcesService: ResourcesServices,
    propService: PropertyServices,
    cfService: CustomFormsServices,
    basicModals: BasicModalServices,
    creationModals: CreationModalServices,
    resViewModals: ResViewModalServices,
    translate: TranslateService,
    private skosService: SkosServices
  ) {
    super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
  }

  ngOnInit() {
    super.ngOnInit();
    let skosMemberList = this.poList.find(poList => poList.getPredicate().equals(SKOS.memberList));
    if (skosMemberList) {
      /* 
      From https://art-uniroma2.atlassian.net/browse/ST-1674
      it should not be possible to edit an ordered collection, if either 
          - the tripleScope of the triple with predicate skos:memberList
          - the tripleScope of each member concept are staged
      */
      let memberListTripleStaged: boolean = skosMemberList.getObjects().some(o => ResourceUtils.isTripleInStaging(o));
      let allMembersStaged: boolean = skosMemberList.getObjects().some(o => {
        let members: ARTResource[] = o.getAdditionalProperty(ResAttribute.MEMBERS);
        return members.length != 0 && !members.some(m => !ResourceUtils.isResourceInStaging(m)); //doesn't exist any member not in staging (=> all members in staging)
      });
      this.addDisabled = this.addDisabled || memberListTripleStaged || allMembersStaged;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    super.ngOnChanges(changes);
    if (changes['poList']) {
      this.ngOnInit(); //re-trigger the check on auth (it can happen that a member list now has staged members)
    }
  }

  selectMember(member: ARTResource) {
    if (this.selectedMember == member) {
      this.selectedMember = null;
    } else {
      this.selectedMember = member;
    }
  }

  /**
   * Needed to be implemented since this Component extends SectionRenderSingleRoot, but not used.
   * Use addFirst addLast addBefore and addAfter instead
   */
  add() { }

  //not used since this section doesn't allow manual add operation
  checkTypeCompliantForManualAdd(_predicate: ARTURIResource, _value: ARTNode): Observable<boolean> {
    return of(true);
  }

  /**
   * Adds a first member to an ordered collection 
   */
  addFirst(predicate?: ARTURIResource) {
    let membersProperty = predicate ? predicate : SKOS.member;
    this.resViewModals.addPropertyValue({ key: "DATA.ACTIONS.ADD_MEMBER" }, this.resource, membersProperty, false, null, false).then(
      (data: any) => {
        let values: ARTResource[] = data.value;
        let addFunctions: MultiActionFunction[] = [{
          function: this.skosService.addFirstToOrderedCollection(this.resource, values[0]),
          value: values[0]
        }];
        this.addMultiple(addFunctions);
      },
      () => { }
    );
  }

  /**
   * Adds a last member to an ordered collection 
   */
  addLast(predicate?: ARTURIResource) {
    let membersProperty = predicate ? predicate : SKOS.member;
    this.resViewModals.addPropertyValue({ key: "DATA.ACTIONS.ADD_MEMBER" }, this.resource, membersProperty, false, null, false).then(
      (data: any) => {
        let values: ARTResource[] = data.value;
        let addFunctions: MultiActionFunction[] = [{
          function: this.skosService.addLastToOrderedCollection(this.resource, values[0]),
          value: values[0]
        }];
        this.addMultiple(addFunctions);
      },
      () => { }
    );
  }

  /**
   * Adds a member in a given position to an ordered collection 
   */
  addBefore(predicate?: ARTURIResource) {
    let membersProperty = predicate ? predicate : SKOS.member;
    this.resViewModals.addPropertyValue({ key: "DATA.ACTIONS.ADD_MEMBER" }, this.resource, membersProperty, false, null, false).then(
      (data: any) => {
        let position = parseInt((this.selectedMember.getAdditionalProperty(ResAttribute.INDEX)).getValue());
        let values: ARTResource[] = data.value;
        let addFunctions: MultiActionFunction[] = [{
          function: this.skosService.addInPositionToOrderedCollection(this.resource, values[0], position),
          value: values[0]
        }];
        this.addMultiple(addFunctions);
      },
      () => { }
    );
  }

  /**
   * Adds a member in a given position to an ordered collection 
   */
  addAfter(predicate?: ARTURIResource) {
    let membersProperty = predicate ? predicate : SKOS.member;
    this.resViewModals.addPropertyValue({ key: "DATA.ACTIONS.ADD_MEMBER" }, this.resource, membersProperty, false, null, false).then(
      (data: any) => {
        let position = parseInt((this.selectedMember.getAdditionalProperty(ResAttribute.INDEX)).getValue()) + 1;
        let values: ARTResource[] = data.value;
        let addFunctions: MultiActionFunction[] = [{
          function: this.skosService.addInPositionToOrderedCollection(this.resource, values[0], position),
          value: values[0]
        }];
        this.addMultiple(addFunctions);
      },
      () => { }
    );
  }

  // This is called only when the user remove the whole member list, not single member
  removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
    this.getRemoveFunction(predicate, object).subscribe(
      () => this.update.emit(null)
    );
  }

  getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
    return this.resourcesService.removeValue(this.resource, predicate, object);
  }


  removeMember(member: ARTResource) {
    this.skosService.removeFromOrderedCollection(this.resource, member).subscribe(
      () => this.update.emit(null)
    );
  }

}