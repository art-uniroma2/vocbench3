import { Component } from "@angular/core";
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from "rxjs";
import { ImportFromLocalProjectData, ImportFromWebData } from 'src/app/metadata/namespacesAndImports/import-ontology-modal.component';
import { DatatypeValidator } from "src/app/utils/DatatypeValidator";
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { ImportFromDatasetCatalogModalReturnData } from "../../../../metadata/namespacesAndImports/import-from-dataset-catalog-modal.component";
import { ARTNode, ARTURIResource } from "../../../../models/ARTResources";
import { ImportAction, ImportSource } from "../../../../models/Metadata";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { MetadataServices } from "../../../../services/metadata.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { SharedModalServices } from "../../../../modal-dialogs/shared-modals/shared-modals.service";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
  selector: "imports-renderer",
  templateUrl: "./imports-section-renderer.component.html",
  standalone: false
})
export class ImportsSectionRendererComponent extends SectionRenderSingleRoot {

  section = ResViewSection.imports;
  addBtnImgSrc = "./assets/images/icons/actions/ontologyProperty_create.png";

  constructor(
    resourcesService: ResourcesServices,
    propService: PropertyServices,
    cfService: CustomFormsServices,
    basicModals: BasicModalServices,
    creationModals: CreationModalServices,
    resViewModals: ResViewModalServices,
    translate: TranslateService,
    private metadataService: MetadataServices,
    private dtValidator: DatatypeValidator,
    private sharedModals: SharedModalServices,
    private translateService: TranslateService
  ) {
    super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
  }

  ngOnInit() {
    super.ngOnInit();
  }

  /**
   * Needed to be implemented since this Component extends SectionRenderSingleRoot, but not used.
   * Use importFrom...() methods instead
   */
  add() { }

  /**
   * The following importFrom...() methods are mostly copied from namespacesAndImportsComponent
   */

  /**
   * Opens a modal to import an ontology from web,
   * once done refreshes the imports list and the namespace prefix mapping
   */
  importFromWeb() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.IMPORT_FROM_WEB" }, ImportSource.fromWeb, ImportAction.import).then(
      (data: ImportFromWebData) => {
        let importFn: Observable<any> = this.metadataService.addFromWeb(data.baseURI, data.transitiveImportAllowance, data.altURL, data.rdfFormat);
        if (data.mirrorFile != null) {
          importFn = this.metadataService.addFromWebToMirror(data.baseURI, data.mirrorFile, data.transitiveImportAllowance, data.altURL, data.rdfFormat);
        }
        importFn.subscribe({
          next: () => {
            this.onImportsChange();
          },
          error: (err: Error) => { this.handleImportFromWebFail(err); }
        });
      },
      () => { }
    );
  }


  /**
   * Opens a modal to import an ontology from a local file and copies it to a mirror file,
   * once done refreshes the imports list and the namespace prefix mapping
   */
  importFromLocalFile() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.IMPORT_FROM_LOCAL_FILE" }, ImportSource.fromLocalFile, ImportAction.import).then(
      (data: any) => {
        this.metadataService.addFromLocalFile(data.baseURI, data.localFile, data.mirrorFile, data.transitiveImportAllowance).subscribe(
          () => {
            this.onImportsChange();
          }
        );
      },
      () => { }
    );
  }

  /**
   * Opens a modal to import an ontology from a mirror file,
   * once done refreshes the imports list and the namespace prefix mapping
   */
  importFromOntologyMirror() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.IMPORT_FROM_ONTOLOGY_MIRROR" }, ImportSource.fromOntologyMirror, ImportAction.import).then(
      (data: any) => {
        this.metadataService.addFromMirror(data.mirror.baseURI, data.mirror.file, data.transitiveImportAllowance).subscribe(
          () => {
            this.onImportsChange();
          }
        );
      },
      () => { }
    );
  }

  /**
   * Opens a modal to import an ontology from the dataset catalog. This uses the addFromWeb import.
   * Once done refreshes the imports list and the namespace prefix mapping
   */
  importFromDatasetCatalog() {
    this.sharedModals.importFromDatasetCatalog({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.IMPORT_FROM_DATASET_CATALOG" }).then(
      (data: ImportFromDatasetCatalogModalReturnData) => {
        this.metadataService.addFromWeb(data.ontologyIRI, data.transitiveImportAllowance, data.dataDump, data.rdfFormat).subscribe({
          next: () => {
            this.onImportsChange();
          },
          error: (err: Error) => { this.handleImportFromWebFail(err); }
        });
      },
      () => { }
    );
  }

  /**
   * Opens a modal to import an ontology from a local file and copies it to a mirror file,
   * once done refreshes the imports list and the namespace prefix mapping
   */
  importFromLocalProject() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.IMPORT_FROM_LOCAL_PROJECT" }, ImportSource.fromLocalProject, ImportAction.import).then(
      (data: ImportFromLocalProjectData) => {
        this.metadataService.addFromLocalProject(data.project.getName(), data.transitiveImportAllowance).subscribe(
          () => {
            this.onImportsChange();
          }
        );
      },
      () => { }
    );
  }

  checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
    return of(value instanceof ARTURIResource);
  }

  removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
    this.getRemoveFunction(predicate, object).subscribe(
      () => {
        this.onImportsChange();
      }
    );
  }

  getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
    return this.metadataService.removeImport(object.getNominalValue());
  }


  private handleImportFromWebFail(err: Error) {
    let msg = this.translateService.instant("METADATA.NAMESPACES_AND_IMPORTS.MESSAGES.UNABLE_TO_IMPORT_ONTOLOGY") + ":\n" + err.message;
    this.basicModals.alert({ key: "STATUS.WARNING" }, msg, ModalType.warning);
  }

  private onImportsChange() {
    //import might have added/removed DT restrictions, so update them
    this.dtValidator.initDatatypeRestrictions().subscribe();
    this.update.emit();
  }

}