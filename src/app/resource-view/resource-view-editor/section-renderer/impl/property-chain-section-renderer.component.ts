import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from "rxjs";
import { ARTBNode, ARTNode, ARTResource, ARTURIResource } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { PropertyListCreatorModalReturnData } from "../../res-view-modals/property-chain-creator-modal.component";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
    selector: "property-chain-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class PropertyChainRendererComponent extends SectionRenderSingleRoot {

    section = ResViewSection.subPropertyChains;
    addBtnImgSrc = "./assets/images/icons/actions/property_create.png";

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    add(predicate: ARTURIResource, propChangeable: boolean) {
        this.resViewModals.createPropertyChain({key: "DATA.ACTIONS.CREATE_PROPERTY_CHAIN"}, predicate, propChangeable).then(
            (data: PropertyListCreatorModalReturnData) => {
                let prop: ARTURIResource = data.property;
                let chain: string[] = data.chain;
                this.propService.addPropertyChainAxiom(this.resource as ARTURIResource, chain.join(","), prop).subscribe(
                    () => this.update.emit(null)
                );
            },
            () => {}
        );
    }

    editHandler(predicate: ARTURIResource, object: ARTNode) {
        //here I can force the cast to ARTBNode since I am sure that all the object handled in this section are Bnode
        this.resViewModals.createPropertyChain({key: "DATA.ACTIONS.CREATE_PROPERTY_CHAIN"}, predicate, false, object as ARTBNode).then(
            (data: PropertyListCreatorModalReturnData) => {
                let chain: string[] = data.chain;
                this.propService.updatePropertyChainAxiom(this.resource as ARTURIResource, object as ARTResource, chain.join(","), predicate).subscribe(
                    () => this.update.emit(null)
                );
            },
            () => {}
        );
    }

    //not used since this section doesn't allow manual add operation
    checkTypeCompliantForManualAdd(_predicate: ARTURIResource, _value: ARTNode): Observable<boolean> {
        return of(true);
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        this.getRemoveFunction(predicate, object).subscribe(
            () => {
                this.update.emit(null);
            }
        );
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        return this.propService.removePropertyChainAxiom(this.resource as ARTURIResource, object as ARTResource, predicate);
    }

}