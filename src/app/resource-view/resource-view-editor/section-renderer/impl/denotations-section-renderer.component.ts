import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from "rxjs";
import { ARTNode, ARTResource, ARTURIResource } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { OntoLexLemonServices } from "../../../../services/ontolex-lemon.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { NewOntoLexicalizationCfModalReturnData } from "../../../../modal-dialogs/creation-modals/newResourceModal/ontolex/new-ontolexicalization-cf-modal.component";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";


@Component({
    selector: "denotations-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class DenotationsSectionRendererComponent extends SectionRenderSingleRoot {

    section = ResViewSection.denotations;
    addBtnImgSrc = "./assets/images/icons/actions/objectProperty_create.png";

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
        private ontolexService: OntoLexLemonServices
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    add(predicate: ARTURIResource, _propChangeable: boolean) {
        if (predicate.equals(this.rootProperty)) {
            this.creationModals.newOntoLexicalizationCf({ key: "DATA.ACTIONS.ADD_DENOTATION" }, this.rootProperty, false).then(
                (data: NewOntoLexicalizationCfModalReturnData) => {
                    this.ontolexService.addLexicalization(this.resource, data.linkedResource, data.createPlain, data.createSense, data.cls, data.cfValue).subscribe(
                        () => {
                            this.update.emit();
                        }
                    );
                },
                () => { }
            );
        } else {
            this.enrichProperty(predicate);
        }
    }

    //not used since this section doesn't allow manual add operation
    checkTypeCompliantForManualAdd(_predicate: ARTURIResource, _value: ARTNode): Observable<boolean> {
        return of(true);
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        this.getRemoveFunction(predicate, object).subscribe(
            () => {
                this.update.emit(null);
            }
        );
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        return this.ontolexService.removeLexicalization(this.resource, object as ARTResource);
    }

}