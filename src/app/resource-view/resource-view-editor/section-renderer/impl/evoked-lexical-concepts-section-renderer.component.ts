import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from "rxjs";
import { OntoLexLemonServices } from "src/app/services/ontolex-lemon.service";
import { NewConceptualizationCfModalReturnData } from "src/app/modal-dialogs/creation-modals/newResourceModal/ontolex/new-conceptualization-cf-modal.component";
import { ARTNode, ARTResource, ARTURIResource } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";


@Component({
    selector: "evoked-lexical-concepts-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class EvokedLexicalConceptsSectionRendererComponent extends SectionRenderSingleRoot {

    section = ResViewSection.evokedLexicalConcepts;
    addBtnImgSrc = "./assets/images/icons/actions/objectProperty_create.png";

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
        private ontolexService: OntoLexLemonServices
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    add(predicate: ARTURIResource, _propChangeable: boolean) {
        if (predicate.equals(this.rootProperty)) {
            this.creationModals.newConceptualizationCf({ key: "DATA.ACTIONS.ADD_CONCEPTUALIZATION" }, false, false).then(
                (data: NewConceptualizationCfModalReturnData) => {
                    this.ontolexService.addConceptualization(this.resource, data.linkedResource, true, data.createSense, data.cls, data.cfValue).subscribe(
                        () => {
                            this.update.emit();
                        }
                    );
                },
                () => { }
            );
        } else {
            this.enrichProperty(predicate);
        }
    }

    //not used since this section doesn't allow manual add operation
    checkTypeCompliantForManualAdd(_predicate: ARTURIResource, _value: ARTNode): Observable<boolean> {
        return of(true);
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        this.getRemoveFunction(predicate, object).subscribe(
            () => {
                this.update.emit(null);
            }
        );
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        return this.ontolexService.removeConceptualization(this.resource, object as ARTResource);
    }

}