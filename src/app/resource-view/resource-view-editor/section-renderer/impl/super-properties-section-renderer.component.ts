import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from "rxjs";
import { ARTNode, ARTURIResource } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { AddPropertyValueModalReturnData } from "../../res-view-modals/add-property-value-modal.component";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { MultiActionFunction } from "../multipleActionHelper";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
    selector: "superproperties-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class SuperPropertiesSectionRendererComponent extends SectionRenderSingleRoot {

    section = ResViewSection.superproperties;
    addBtnImgSrc = "./assets/images/icons/actions/property_create.png";

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    add(predicate: ARTURIResource, propChangeable: boolean) {
        this.resViewModals.addPropertyValue({ key: "DATA.ACTIONS.ADD_SUPERPROPERTY" }, this.resource, predicate, propChangeable).then(
            (data: AddPropertyValueModalReturnData) => {
                let prop: ARTURIResource = data.property;
                let inverse: boolean = data.inverseProperty;
                let values: ARTURIResource[] = data.value;

                let addFunctions: MultiActionFunction[] = [];
                values.forEach((v: ARTURIResource) => {
                    addFunctions.push({
                        function: this.propService.addSuperProperty(this.resource as ARTURIResource, v, prop, inverse),
                        value: v
                    });
                });
                this.addMultiple(addFunctions);
            },
            () => { }
        );
    }

    checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
        return of(value instanceof ARTURIResource);
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        this.getRemoveFunction(predicate, object).subscribe(
            () => {
                this.update.emit(null);
            }
        );
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        return this.propService.removeSuperProperty(this.resource as ARTURIResource, object as ARTURIResource, predicate);
    }

}