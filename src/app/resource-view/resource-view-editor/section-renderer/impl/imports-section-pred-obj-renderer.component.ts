import { Component, EventEmitter, Output } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { CustomViewsServices } from "src/app/services/custom-views.service";
import { PredicateObjectsRendererComponent } from "../predicate-objects-renderer.component";

@Component({
    selector: "import-pred-obj-renderer",
    templateUrl: "./imports-section-pred-obj-renderer.component.html",
    styles: [`
        :host {
            display: block;
            margin-bottom: 4px;
        }
    `],
    standalone: false
})
export class ImportSectionPredObjRendererComponent extends PredicateObjectsRendererComponent {

    /**
     * INPUTS / OUTPUTS
     */
    @Output() importFromWeb = new EventEmitter();
    @Output() importFromLocalFile = new EventEmitter();
    @Output() importFromOntologyMirror = new EventEmitter();
    @Output() importFromDatasetCatalog = new EventEmitter();
    @Output() importFromLocalProject = new EventEmitter();

    /**
     * ATTRIBUTES
     */


    constructor(cvService: CustomViewsServices, translate: TranslateService) {
        super(cvService, translate);
    }

}