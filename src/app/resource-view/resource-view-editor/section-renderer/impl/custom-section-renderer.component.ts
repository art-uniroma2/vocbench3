import { Component, Input } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { from, Observable, of } from "rxjs";
import { CreationModalServices } from "src/app/modal-dialogs/creation-modals/creation-modals.service";
import { ARTNode, ARTURIResource, RDFResourceRolesEnum } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { VBContext } from "../../../../utils/VBContext";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../../../modal-dialogs/browsing-modals/browsing-modals.service";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { SectionRendererMultiRoot } from "../sectionRendererMultiRoot";

@Component({
    selector: "custom-section-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class CustomSectionRendererComponent extends SectionRendererMultiRoot {

    @Input() section: ResViewSection;
    addBtnImgSrc = "./assets/images/icons/actions/property_create.png";

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
        private browsingModals: BrowsingModalServices
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    //@Override
    getPredicateToEnrich(): Observable<ARTURIResource> {
        let predicatesIRI: string[] = VBContext.getWorkingProjectCtx().getProjectSettings().resourceView.customSections[this.section].matchedProperties;
        this.rootProperties = predicatesIRI.map(p => new ARTURIResource(p, null, RDFResourceRolesEnum.property));
        if (this.rootProperties.length == 1) { //just one property => return it
            return of(this.rootProperties[0]);
        } else {
            return from(
                this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }, this.rootProperties).then(
                    (selectedProp: ARTURIResource) => {
                        return selectedProp;
                    },
                    () => {
                        return null;
                    }
                )
            );
        }
    }

    add(predicate: ARTURIResource, _propChangeable: boolean) {
        this.enrichProperty(predicate);
    }

    checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
        return of(value instanceof ARTURIResource);
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        this.getRemoveFunction(predicate, object).subscribe(
            () => {
                this.update.emit(null);
            }
        );
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        return this.resourcesService.removeValue(this.resource, predicate, object);
    }

}