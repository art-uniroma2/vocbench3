import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { from, Observable, of } from "rxjs";
import { ARTLiteral, ARTNode, ARTURIResource } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { OntoLexLemonServices } from "../../../../services/ontolex-lemon.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../../../modal-dialogs/browsing-modals/browsing-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { MultiActionFunction } from "../multipleActionHelper";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
    selector: "form-representations-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class FormRepresentationsSectionRendererComponent extends SectionRenderSingleRoot {

    section = ResViewSection.formRepresentations;
    addBtnImgSrc = "./assets/images/icons/actions/datatypeProperty_create.png";

    private lexiconLang: string; //cache the language of the lexicon

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
        private browsingModals: BrowsingModalServices, 
        private ontolexService: OntoLexLemonServices
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    //add as top concept
    add(predicate: ARTURIResource, _propChangeable: boolean) {
        this.getLexiconLang().subscribe(
            lang => {
                this.lexiconLang = lang;
                this.creationModals.newPlainLiteral({ key: "COMMONS.ACTIONS.ADD_X", params: { x: predicate.getShow() } }, null, false, this.lexiconLang, false, { constrain: true, locale: true }, { enabled: true, allowSameLang: false }).then(
                    (literals: ARTLiteral[]) => {
                        let addFunctions: MultiActionFunction[] = [];
                        literals.forEach((literal: ARTLiteral) => {
                            addFunctions.push({
                                function: this.ontolexService.addFormRepresentation(this.resource, literal, predicate),
                                value: literal
                            });
                        });
                        this.addMultiple(addFunctions);
                    },
                    () => { }
                );
            }
        );
    }

    private getLexiconLang(): Observable<string> {
        if (this.lexiconLang == null) {
            return this.ontolexService.getFormLanguage(this.resource);
        } else {
            return of(this.lexiconLang);
        }
    }

    getPredicateToEnrich(): Observable<ARTURIResource> {
        return from(
            this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }, [this.rootProperty]).then(
                selectedProp => {
                    return selectedProp;
                },
                () => { return null; }
            )
        );
    }

    checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
        return of(value instanceof ARTLiteral);
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        this.getRemoveFunction(predicate, object).subscribe(
            () => {
                this.update.emit(null);
            }
        );
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        return this.ontolexService.removeFormRepresentation(this.resource, object as ARTLiteral, predicate);
    }

}