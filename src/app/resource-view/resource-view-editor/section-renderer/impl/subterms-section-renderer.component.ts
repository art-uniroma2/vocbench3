import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from "rxjs";
import { ARTNode, ARTURIResource } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { OntoLexLemonServices } from "../../../../services/ontolex-lemon.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../../../modal-dialogs/browsing-modals/browsing-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { MultiActionFunction } from "../multipleActionHelper";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
    selector: "subterms-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class SubtermsSectionRendererComponent extends SectionRenderSingleRoot {

    section = ResViewSection.subterms;
    addBtnImgSrc = "./assets/images/icons/actions/objectProperty_create.png";

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
        private browsingModals: BrowsingModalServices, 
        private ontolexService: OntoLexLemonServices) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    add(predicate: ARTURIResource, _propChangeable: boolean) {
        this.browsingModals.browseLexicalEntryList({ key: "DATA.ACTIONS.ADD_SUBTERM" }, null, true, true, false, true).then(
            (values: ARTURIResource[] | ARTURIResource) => { //array if multiple selected, one value otherwise
                let addFunctions: MultiActionFunction[] = [];
                if (values instanceof ARTURIResource) {
                    values = [values];
                }
                values.forEach((v: ARTURIResource) => {
                    addFunctions.push({
                        function: this.ontolexService.addSubterm(this.resource as ARTURIResource, v, predicate),
                        value: v
                    });
                });
                this.addMultiple(addFunctions);
            },
            () => { }
        );
    }

    checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
        return of(value instanceof ARTURIResource);
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        this.getRemoveFunction(predicate, object).subscribe(
            () => {
                this.update.emit();
            }
        );
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        return this.ontolexService.removeSubterm(this.resource as ARTURIResource, object as ARTURIResource, predicate);
    }

}