import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from "rxjs";
import { ARTBNode, ARTNode, ARTURIResource } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { DatatypesServices } from "../../../../services/datatypes.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
    selector: "datatype-definition-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class DatatypeDefinitionSectionRendererComponent extends SectionRenderSingleRoot {

    section = ResViewSection.datatypeDefinitions;
    addBtnImgSrc = "./assets/images/icons/actions/property_create.png";

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
        private datatypeService: DatatypesServices
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    add() {
        this.resViewModals.setDatatypeFacets({ key: "DATA.ACTIONS.SET_DATATYPE_RESTRICTIONS" }, this.resource as ARTURIResource).then(
            () => {
                this.update.emit();
            },
            () => { }
        );
    }

    editHandler(predicate: ARTURIResource, object: ARTNode) {
        //here I can force the cast to ARTBNode since I am sure that all the object handled in this section are Bnode
        this.resViewModals.setDatatypeFacets({ key: "DATA.ACTIONS.EDIT_DATATYPE_RESTRICTIONS" }, this.resource as ARTURIResource, object as ARTBNode).then(
            () => {
                this.update.emit();
            },
            () => { }
        );
    }


    checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
        return of(value instanceof ARTURIResource);
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        this.getRemoveFunction(predicate, object).subscribe(
            () => {
                this.update.emit();
            }
        );
    }

    getRemoveFunctionImpl(_predicate: ARTURIResource, _object: ARTNode): Observable<any> {
        return this.datatypeService.deleteDatatypeRestriction(this.resource as ARTURIResource);
    }

}