import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from "rxjs";
import { ARTNode, ARTURIResource } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { SkosServices } from "../../../../services/skos.service";
import { VBEventHandler } from "../../../../utils/VBEventHandler";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { MultiActionFunction } from "../multipleActionHelper";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
    selector: "schemes-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class SchemesSectionRendererComponent extends SectionRenderSingleRoot {

    section = ResViewSection.schemes;
    addBtnImgSrc = "./assets/images/icons/actions/conceptScheme_create.png";

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
        private skosService: SkosServices,
        private eventHandler: VBEventHandler
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    //add as top concept
    add(predicate: ARTURIResource, propChangeable: boolean) {
        this.resViewModals.addPropertyValue({ key: "DATA.ACTIONS.ADD_TO_SCHEME" }, this.resource, predicate, propChangeable).then(
            (data: any) => {
                let prop: ARTURIResource = data.property;
                let values: ARTURIResource[] = data.value;
                let addFunctions: MultiActionFunction[] = [];

                if (prop.equals(this.rootProperty)) { //it's adding a concept to a scheme with skos:inScheme
                    values.forEach((v: ARTURIResource) => {
                        addFunctions.push({
                            function: this.skosService.addConceptToScheme(this.resource as ARTURIResource, v),
                            value: v
                        });
                    });
                } else { //it's enriching a subProperty of skos:inScheme
                    values.forEach((v: ARTURIResource) => {
                        addFunctions.push({
                            function: this.resourcesService.addValue(this.resource, prop, v),
                            value: v
                        });
                    });
                }
                this.addMultiple(addFunctions);
            },
            () => { }
        );
    }

    checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
        return of(value instanceof ARTURIResource);
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        this.getRemoveFunction(predicate, object).subscribe(
            () => {
                if (!this.rootProperty.equals(predicate)) {
                    //=> emits conceptRemovedFromSchemeEvent cause it has not been fired by the generic service (removeValue)
                    this.eventHandler.conceptRemovedFromSchemeEvent.emit({ concept: this.resource as ARTURIResource, scheme: object as ARTURIResource });
                }
                this.update.emit(null);
            }
        );
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        if (this.rootProperty.equals(predicate)) { // removing a skos:inScheme relation
            return this.skosService.removeConceptFromScheme(this.resource as ARTURIResource, object as ARTURIResource);
        } else { //predicate is some subProperty of skos:inScheme
            return this.resourcesService.removeValue(this.resource, predicate, object);
        }
    }

}