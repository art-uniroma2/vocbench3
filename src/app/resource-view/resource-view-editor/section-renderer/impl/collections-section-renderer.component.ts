import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from "rxjs";
import { Deserializer } from 'src/app/utils/Deserializer';
import { HttpServiceContext } from 'src/app/utils/HttpManager';
import { VBContext } from 'src/app/utils/VBContext';
import { ARTNode, ARTURIResource } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { SkosServices } from "../../../../services/skos.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { ResourceViewHelper } from '../../resViewHelper';
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
    selector: "collections-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class CollectionsSectionRendererComponent extends SectionRenderSingleRoot {

    section = ResViewSection.collections;
    addBtnImgSrc = null;

    sectionCollapsed = true; //lazy section

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
        private skosService: SkosServices
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    /**
     * Public method to let the ResView refresh this laxy section (when the whole RV is refreshed) if it has been already expanded
     */
    refreshLazyData() {
        if (!this.sectionCollapsed) {
            this.initLazyData();
        }
    }

    private initLazyData() {
        let activeVersion = VBContext.getContextVersion(); //set globally from Versioning page
        if (activeVersion != null) {
            HttpServiceContext.setContextVersion(activeVersion); //set temprorarly version
        }
        this.skosService.getCollectionsForConcept(this.resource).subscribe(
            sectionJson => {
                HttpServiceContext.removeContextVersion();
                let poList = Deserializer.createPredicateObjectsList(sectionJson);
                ResourceViewHelper.filterInferredFromPredObjList(poList, this.projectCtx);
                ResourceViewHelper.filterValueLanguageFromPrefObjList(poList, this.projectCtx);
                ResourceViewHelper.filterDeprecatedValues(poList, this.projectCtx);
                ResourceViewHelper.sortObjects(poList, this.rendering);
                this.poList = poList;
            }
        );

    }

    /**
     * Following methods are not used, every action is disabled in this section
     */

    add(_predicate: ARTURIResource, _propChangeable: boolean) {
    }

    checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
        return of(value instanceof ARTURIResource);
    }

    removePredicateObject(_predicate: ARTURIResource, _object: ARTNode) { }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        return this.resourcesService.removeValue(this.resource, predicate, object);
    }


    /**
     * @Override
     */
    toggleCollapsed() {
        super.toggleCollapsed();
        if (!this.sectionCollapsed) { //opened
            this.initLazyData();
        }
    }

}