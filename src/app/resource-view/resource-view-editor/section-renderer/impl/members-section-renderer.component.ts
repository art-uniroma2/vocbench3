import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from "rxjs";
import { map } from 'rxjs/operators';
import { ARTNode, ARTResource, ARTURIResource, RDFResourceRolesEnum } from "../../../../models/ARTResources";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { SkosServices } from "../../../../services/skos.service";
import { VBEventHandler } from "../../../../utils/VBEventHandler";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { MultiActionFunction } from "../multipleActionHelper";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
    selector: "members-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class MembersSectionRendererComponent extends SectionRenderSingleRoot {

    section = ResViewSection.members;
    addBtnImgSrc = "./assets/images/icons/actions/skosCollection_create.png";

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
        private skosService: SkosServices, 
        private eventHandler: VBEventHandler
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    /**
     * Adds a member in a collection (unordered)
     */
    add(predicate: ARTURIResource, propChangeable: boolean) {
        this.resViewModals.addPropertyValue({key: "DATA.ACTIONS.ADD_MEMBER"}, this.resource, predicate, propChangeable).then(
            (data: any) => {
                let prop: ARTURIResource = data.property;
                let values: ARTURIResource[] = data.value;
                let addFunctions: MultiActionFunction[] = [];

                if (prop.equals(this.rootProperty)) { //it's using skos:member
                    values.forEach((v: ARTURIResource) => {
                        addFunctions.push({
                            function: this.skosService.addToCollection(this.resource, v),
                            value: v
                        });
                    });
                } else { //it's enriching a subProperty of skos:member
                    values.forEach((v: ARTURIResource) => {
                        addFunctions.push({
                            function: this.resourcesService.addValue(this.resource, prop, v).pipe(
                                map(() => {
                                    if (v.getRole() == RDFResourceRolesEnum.skosCollection || v.getRole() == RDFResourceRolesEnum.skosOrderedCollection) {
                                        this.eventHandler.nestedCollectionAddedEvent.emit({ nested: v, container: this.resource });
                                    }
                                })
                            ),
                            value: v
                        });
                    });
                }
                this.addMultiple(addFunctions);
            },
            () => { }
        );
    }

    checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean> {
        return of(value instanceof ARTURIResource);
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        this.getRemoveFunction(predicate, object).subscribe(
            () => this.update.emit()
        );
    }

    removeAllValues(predicate: ARTURIResource) {
        let removeFnArray: any[] = [];
        for (const po of this.poList) {
            let objList: ARTNode[] = po.getObjects();
            for (const o of objList) {
                removeFnArray.push(this.getRemoveFunction(predicate, o));
            }
        }
        this.removeAllRicursively(removeFnArray);
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        if (this.rootProperty.equals(predicate)) { //removing skos:member relation
            return this.skosService.removeFromCollection(this.resource, object as ARTResource);
        } else { //predicate is some subProperty of skos:member
            return this.resourcesService.removeValue(this.resource, predicate, object);
        }
    }

}