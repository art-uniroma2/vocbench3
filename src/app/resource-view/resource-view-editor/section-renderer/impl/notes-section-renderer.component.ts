import { Component } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { from, Observable, of } from "rxjs";
import { ARTLiteral, ARTNode, ARTResource, ARTURIResource } from "../../../../models/ARTResources";
import { CustomFormValue } from "../../../../models/CustomForms";
import { Language } from "../../../../models/LanguagesCountries";
import { ResViewSection } from "../../../../models/ResourceView";
import { CustomFormsServices } from "../../../../services/custom-forms.service";
import { PropertyServices } from "../../../../services/properties.service";
import { ResourcesServices } from "../../../../services/resources.service";
import { SkosServices } from "../../../../services/skos.service";
import { BasicModalServices } from "../../../../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../../../modal-dialogs/browsing-modals/browsing-modals.service";
import { CreationModalServices } from "../../../../modal-dialogs/creation-modals/creation-modals.service";
import { ResViewModalServices } from "../../res-view-modals/resViewModalServices";
import { MultiActionFunction } from "../multipleActionHelper";
import { SectionRenderSingleRoot } from "../sectionRendererSingleRoot";

@Component({
    selector: "notes-renderer",
    templateUrl: "../sectionRenderer.html",
    standalone: false
})
export class NotesSectionRendererComponent extends SectionRenderSingleRoot {

    section = ResViewSection.notes;
    addBtnImgSrc = "./assets/images/icons/actions/annotationProperty_create.png";

    constructor(
        resourcesService: ResourcesServices, 
        propService: PropertyServices, 
        cfService: CustomFormsServices,
        basicModals: BasicModalServices, 
        creationModals: CreationModalServices, 
        resViewModals: ResViewModalServices,
        translate: TranslateService,
        private browsingModals: BrowsingModalServices,
        private skosService: SkosServices
    ) {
        super(resourcesService, propService, cfService, basicModals, creationModals, resViewModals, translate);
    }

    ngOnInit() {
        super.ngOnInit();
    }

    add(predicate: ARTURIResource) {
        this.enrichProperty(predicate);
    }

    getPredicateToEnrich(): Observable<ARTURIResource> {
        return from(
            this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }, [this.rootProperty]).then(
                selectedProp => {
                    return selectedProp;
                },
                () => { return null; }
            )
        );
    }

    checkTypeCompliantForManualAdd(_predicate: ARTURIResource, _value: ARTNode): Observable<boolean> {
        return of(true); //notes accept all kinds of values
    }


    copyLocaleHandler(predicate: ARTURIResource, eventData: { value: ARTNode, locales: Language[] }) {
        let addFunctions: MultiActionFunction[] = [];
        //this function is the handler of an event invoked in notes only when the value is a plain literal, so the cast is safe
        let value: ARTLiteral = eventData.value as ARTLiteral;
        eventData.locales.forEach(l => {
            let note: ARTLiteral = new ARTLiteral(value.getValue(), null, l.tag);
            addFunctions.push({
                function: this.getAddSectionAware(this.resource, predicate, note),
                value: note
            });
        });
        this.addMultiple(addFunctions);
    }

    //@Override
    getAddSectionAware(resource: ARTResource, predicate: ARTURIResource, value: ARTNode | CustomFormValue): Observable<void> {
        return this.skosService.addNote(resource, predicate, value);
    }

    //@Override
    getChangePropertySectionAware(resource: ARTResource, predicate: ARTURIResource, newPredicate: ARTURIResource, value: ARTNode): Observable<void> {
        return this.skosService.updateNoteProperty(resource, predicate, newPredicate, value);
    }

    //@Override
    getAddPropertySectionAware(resource: ARTResource, predicate: ARTURIResource, value: ARTNode): Observable<void> {
        return this.skosService.addNote(resource, predicate, value);
    }

    removePredicateObject(predicate: ARTURIResource, object: ARTNode) {
        this.getRemoveFunctionImpl(predicate, object).subscribe(
            () => this.update.emit(null)
        );
    }

    getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any> {
        return this.skosService.removeNote(this.resource, predicate, object);
    }

}