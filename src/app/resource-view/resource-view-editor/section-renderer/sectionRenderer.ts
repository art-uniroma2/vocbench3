import { Directive, EventEmitter, Input, Output, SimpleChanges } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Observable } from 'rxjs';
import { CustomForm, CustomFormValue } from "src/app/models/CustomForms";
import { PredicateCustomView } from "src/app/models/CustomViews";
import { Language } from "src/app/models/LanguagesCountries";
import { ResViewSectionsCustomization } from "src/app/models/Properties";
import { PropertyServices } from "src/app/services/properties.service";
import { TranslationUtils } from "src/app/utils/TranslationUtils";
import { ProjectContext, VBContext } from 'src/app/utils/VBContext';
import { CreationModalServices } from "src/app/modal-dialogs/creation-modals/creation-modals.service";
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { ARTLiteral, ARTNode, ARTPredicateObjects, ARTResource, ARTURIResource, ResAttribute } from "../../../models/ARTResources";
import { AddAction, ResViewSection, ResViewUtils, ValueClickEvent } from "../../../models/ResourceView";
import { CustomFormsServices } from "../../../services/custom-forms.service";
import { ResourcesServices } from "../../../services/resources.service";
import { CRUDEnum, ResourceViewAuthEvaluator } from "../../../utils/AuthorizationEvaluator";
import { ResourceUtils } from "../../../utils/ResourceUtils";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { AddPropertyValueModalReturnData } from "../res-view-modals/add-property-value-modal.component";
import { BrowseExternalResourceModalReturnData } from "../res-view-modals/browse-external-resource-modal.component";
import { ChangePropertyData } from '../res-view-modals/change-property-modal.component';
import { ResViewModalServices } from "../res-view-modals/resViewModalServices";
import { MultiActionError, MultiActionFunction, MultiActionType, MultipleActionHelper } from "./multipleActionHelper";
import { EnrichmentType, PropertyEnrichmentHelper, PropertyEnrichmentInfo } from "./propertyEnrichmentHelper";

@Directive()
export abstract class SectionRenderer {

  /**
   * INPUTS / OUTPUTS
   */

  @Input() poList: ARTPredicateObjects[];
  @Input() resource: ARTResource; //resource described
  @Input() rendering: boolean;
  @Input() readonly: boolean;
  @Input() projectCtx: ProjectContext;
  @Output() update = new EventEmitter(); //something changed in this section. Tells to ResView to update
  @Output() valueClick = new EventEmitter<ValueClickEvent>();

  AddAction = AddAction;

  protected predCustomView: PredicateCustomView;

  protected resourcesService: ResourcesServices;
  protected propService: PropertyServices;
  protected cfService: CustomFormsServices;
  protected basicModals: BasicModalServices;
  protected creationModals: CreationModalServices;
  protected resViewModals: ResViewModalServices;
  private translate: TranslateService;

  constructor(
    resourcesService: ResourcesServices,
    propService: PropertyServices,
    cfService: CustomFormsServices,
    basicModals: BasicModalServices,
    creationModals: CreationModalServices,
    resViewModals: ResViewModalServices,
    translate: TranslateService
  ) {
    this.resourcesService = resourcesService;
    this.propService = propService;
    this.cfService = cfService;
    this.basicModals = basicModals;
    this.creationModals = creationModals;
    this.resViewModals = resViewModals;
    this.translate = translate;

    this.translate.onLangChange.subscribe(() => {
      this.initSectionDisplayInfo();
    });
  }

  /**
   * ATTRIBUTES
   */

  abstract section: ResViewSection;

  /**
   * to handle section collapsed/expanded
   */
  sectionCollapsed: boolean = false;

  /**
   * to enabled/disable the add button
   */
  addAuthorized: boolean = true;

  /**
   * Label + description of the section
   */
  sectionTitle: string;
  sectionDescription: string;
  /**
   * Src of the "add" icon placed on the groupPanel outline.
   * This is specific of a section.
   */
  abstract addBtnImgSrc: string;
  /**
   * Title show on mouseover on the "add" icon placed on the groupPanel outline.
   * This is specific of a section.
   */
  addTitleTranslationKey: string;

  /**
   * Action authorization
   */
  addDisabled: boolean = false;
  addExteranlResourceAllowed: boolean = false;
  addManuallyAllowed: boolean = false;

  showCustomView: boolean = true;

  /**
   * METHODS
   */

  ngOnInit() {
    this.addTitleTranslationKey = TranslationUtils.getResViewSectionAddBtnTranslationKey(this.section);

    this.initSectionDisplayInfo();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['resource'] || changes['readonly']) {
      this.initActionsStatus();
    }
  }

  private initSectionDisplayInfo() {
    //reset info (to prevent them holding the old value in a different language, in case onLangChange was fired)
    this.sectionTitle = null;
    this.sectionDescription = null;
    let sectionCustomization: ResViewSectionsCustomization = VBContext.getWorkingProjectCtx().getProjectSettings().resViewSectionsCustomization;
    if (sectionCustomization?.[this.translate.currentLang]?.[this.section]) {
      let title = sectionCustomization[this.translate.currentLang][this.section].label;
      let descr = sectionCustomization[this.translate.currentLang][this.section].description;
      if (title != null && title != "") {
        this.sectionTitle = title;
      }
      if (descr != null && descr != "") {
        this.sectionDescription = descr;
      }
    }
    if (this.sectionTitle == null) { //if no custom title, apply the default
      this.sectionTitle = this.translate.instant(TranslationUtils.getResViewSectionTranslationKey(this.section));
    }
  }

  initActionsStatus() {
    /**
     * Add is disabled if one of them is true
     * - resource is not explicit (e.g. imported or inferred) but not in staging add at the same time (addition in staging add is allowed)
     * - ResView is working in readonly mode
     * - user not authorized
     */
    this.addDisabled = !this.resource.getAdditionalProperty(ResAttribute.EXPLICIT) && !ResourceUtils.isResourceInStagingAdd(this.resource) ||
      this.readonly || !ResourceViewAuthEvaluator.isAuthorized(this.section, CRUDEnum.C, this.resource);
    this.addExteranlResourceAllowed = ResViewUtils.addExternalResourceSection.indexOf(this.section) != -1;
    this.addManuallyAllowed = !(this.section in ResViewSection) || ResViewUtils.addManuallySection.indexOf(this.section) != -1; //custom section OR add manually foreseen

  }

  /**
   * Listener of add event fired by "+" or "add value (manually)" buttons (manually parameter true)
   */
  addHandler(predicate?: ARTURIResource, action?: AddAction) { //manually becomes type: "default"|manuelly|remote
    if (!predicate) {
      this.getPredicateToEnrich().subscribe(
        predicate => {
          if (predicate) { //if not canceled
            if (action == AddAction.manually) {
              this.addManually(predicate, true);
            } else if (action == AddAction.remote) {
              this.addExternal(predicate, true);
            } else {
              this.add(predicate, true);
            }
          }
        }
      );
    } else {
      if (action == AddAction.manually) {
        this.addManually(predicate, false);
      } else if (action == AddAction.remote) {
        this.addExternal(predicate, false);
      } else {
        this.add(predicate, false);
      }
    }
  }

  /**
   * Should allow to enrich a property by opening a modal and selecting a value.
   * It can get an optional parameter "property".
   * This is fired when the add button is clicked (the one placed on the groupPanel outline) without property parameter,
   * or hen the "+" button of a specific property panel is clicked (placed in the subPanel heading) with the property provided.
   * If property is provided (add fired from specific property panel) the modal won't allow to change it allowing so
   * to enrich just that property, otherwise, if property is not provided (add fired from the generic section panel),
   * the modal allow to change property to enrich.
   * @param predicate property to enrich.
   */
  abstract add(predicate: ARTURIResource, propChangeable: boolean): void;

  protected addMultiple(addFunctions: MultiActionFunction[], errorHandler?: (errors: MultiActionError[]) => void, errors?: MultiActionError[]) {
    MultipleActionHelper.executeActions(addFunctions, MultiActionType.addition, this.basicModals, errorHandler, errors, () => this.update.emit());
  }
  protected handleSingleMultiAddError(error: MultiActionError) {
    MultipleActionHelper.handleSingleMultiActionError(error, MultiActionType.addition, this.basicModals);
  }
  protected handleMultipleMultiAddError(errors: MultiActionError[]) {
    MultipleActionHelper.handleMultipleMultiActionError(errors, MultiActionType.addition, this.basicModals);
  }

  /**
   * Implementation of addManually with the predicate provided
   * @param predicate 
   */
  addManually(predicate: ARTURIResource, propChangeable: boolean) {
    this.resViewModals.addManualValue(predicate, propChangeable).then(
      data => {
        let property: ARTURIResource = data.property;
        let value: ARTNode = data.value;
        this.checkTypeCompliantForManualAdd(property, value).subscribe(
          compliant => {
            if (compliant) { //value type compliant with predicate range
              this.resourcesService.addValue(this.resource, property, value).subscribe(
                () => this.update.emit()
              );
            } else { //value type not compliant with predicate range
              this.basicModals.confirm({ key: "STATUS.WARNING" }, { key: "MESSAGES.VALUE_TYPE_PROPERTY_RANGE_INCONSISTENT_CONFIRM", params: { property: property.getShow() } },
                ModalType.warning).then(
                  () => {
                    this.resourcesService.addValue(this.resource, property, value).subscribe(
                      () => this.update.emit()
                    );
                  },
                  () => { }
                );
            }
          }
        );
      },
      () => { }
    );
  }

  private addExternal(predicate: ARTURIResource, propChangeable: boolean) {
    this.resViewModals.browseExternalResource({ key: "COMMONS.ACTIONS.SELECT_EXTERNAL_RESOURCE" }, predicate, propChangeable).then(
      (data: BrowseExternalResourceModalReturnData) => {
        this.resourcesService.addValue(this.resource, data.property, data.resource).subscribe(
          () => this.update.emit()
        );
      },
      () => { }
    );
  }

  protected enrichProperty(predicate: ARTURIResource) {
    PropertyEnrichmentHelper.getPropertyEnrichmentInfo(predicate, this.propService, this.basicModals).subscribe(
      (data: PropertyEnrichmentInfo) => {
        if (data.type == EnrichmentType.resource) {
          this.enrichWithResource(predicate);
        } else if (data.type == EnrichmentType.literal) {
          this.enrichWithTypedLiteral(predicate, data.allowedDatatypes, data.dataRanges);
        } else if (data.type == EnrichmentType.customForm) {
          this.enrichWithCustomForm(predicate, data.form);
        }
      }
    );
  }

  protected enrichWithCustomForm(predicate: ARTURIResource, form: CustomForm) {
    this.resViewModals.enrichCustomForm({ key: "COMMONS.ACTIONS.ADD_X", params: { x: predicate.getShow() } }, form.getId()).then(
      (cfValue: CustomFormValue) => {
        this.getAddSectionAware(this.resource, predicate, cfValue).subscribe(() => this.update.emit());
      },
      () => { }
    );
  }

  /**
   * Opens a newTypedLiteral modal to enrich the predicate with a typed literal value 
   */
  protected enrichWithTypedLiteral(predicate: ARTURIResource, allowedDatatypes?: ARTURIResource[], dataRanges?: (ARTLiteral[])[]) {
    this.creationModals.newTypedLiteral({ key: "COMMONS.ACTIONS.ADD_X", params: { x: predicate.getShow() } }, predicate, null, allowedDatatypes, dataRanges, true, true).then(
      (literals: ARTLiteral[]) => {
        let addFunctions: MultiActionFunction[] = [];
        literals.forEach((l: ARTLiteral) => {
          addFunctions.push({
            function: this.getAddSectionAware(this.resource, predicate, l),
            value: l
          });
        });
        this.addMultiple(addFunctions);
      },
      () => { }
    );
  }

  /**
   * Opens a modal to enrich the predicate with a resource 
   */
  protected enrichWithResource(predicate: ARTURIResource) {
    this.resViewModals.addPropertyValue({ key: "COMMONS.ACTIONS.ADD_X", params: { x: predicate.getShow() } }, this.resource, predicate, false).then(
      (data: AddPropertyValueModalReturnData) => {
        let prop: ARTURIResource = data.property;
        let values: ARTURIResource[] = data.value;
        let addFunctions: MultiActionFunction[] = [];
        values.forEach((v: ARTURIResource) => {
          addFunctions.push({
            function: this.getAddSectionAware(this.resource, prop, v),
            value: v
          });
        });
        this.addMultiple(addFunctions);
      },
      () => { }
    );
  }

  /**
   * This represents the specific section implementation for the add. 
   * By default it is Resource.addValue(...), but it could be overriden in a section if it has a specific implementation 
   * (like in notes section for which exists the addNote service that accept a SpecialValue as value)
   * @param resource
   * @param predicate 
   * @param value 
   */
  protected getAddSectionAware(resource: ARTResource, predicate: ARTURIResource, value: ARTNode | CustomFormValue): Observable<void> {
    return this.resourcesService.addValue(resource, predicate, value);
  }

  /**
   * This represents the specific section implementation for the update value property. 
   * By default it is Resource.updateTriplePredicate(...), but it could be overriden in a section if it has a specific implementation 
   * (like in notes and lexicalizations)
   * @param resource 
   * @param predicate 
   * @param newPredicate 
   * @param value 
   */
  protected getChangePropertySectionAware(resource: ARTResource, predicate: ARTURIResource, newPredicate: ARTURIResource, value: ARTNode): Observable<void> {
    return this.resourcesService.updateTriplePredicate(resource, predicate, newPredicate, value);
  }

  /**
   * This represents the specific section implementation for the add property to value. 
   * By default it is Resource.addValue(...), but it could be overriden in a section if it has a specific implementation 
   * (like in notes and lexicalizations)
   * @param resource
   * @param predicate 
   * @param value 
   */
  protected getAddPropertySectionAware(resource: ARTResource, predicate: ARTURIResource, value: ARTNode): Observable<void> {
    return this.resourcesService.addValue(resource, predicate, value);
  }

  /**
   * Returns the predicate to enrich in case of an add operation is fired from the generic section
   */
  abstract getPredicateToEnrich(): Observable<ARTURIResource>;

  /**
   * Tells if a value nature is compliant with the range of a predicate. Useful to check if a manually 
   * provided value is ok.
   * @param predicate 
   * @param value 
   */
  abstract checkTypeCompliantForManualAdd(predicate: ARTURIResource, value: ARTNode): Observable<boolean>;

  /**
   * Listener of remove event. If object is passed with the event remove just that object, otherwise remove all the values
   * @param predicate 
   * @param object 
   */
  protected removeHandler(predicate: ARTURIResource, object?: ARTNode) {
    if (object == null) {
      this.basicModals.confirm({ key: "COMMONS.ACTIONS.DELETE_ALL_VALUES" }, { key: "MESSAGES.DELETE_ALL_VALUES_CONFIRM", params: { property: predicate.getShow() } }, ModalType.warning).then(
        () => this.removeAllValues(predicate),
        () => { }
      );
    } else {
      this.removePredicateObject(predicate, object);
    }
  }
  /**
   * Removes all the objects of the predicate-objects list
   * @param predicate 
   */
  removeAllValues(predicate: ARTURIResource) {
    let deleteFunctions: MultiActionFunction[] = [];
    this.poList.forEach(po => {
      if (po.getPredicate().equals(predicate)) {
        let values: ARTNode[] = po.getObjects();
        values.forEach((v: ARTNode) => {
          deleteFunctions.push({
            function: this.getRemoveFunction(predicate, v),
            value: v
          });
        });
        MultipleActionHelper.executeActions(deleteFunctions, MultiActionType.deletion, this.basicModals, null, null, () => this.update.emit());
      }
    });
  }
  /**
   * Removes an object related to the given predicate.
   * This is fired when the "-" button is clicked (near an object).
   */
  abstract removePredicateObject(predicate: ARTURIResource, object: ARTNode): void;
  /**
   * Based on the predicate and the object, returns the remove function to invoke
   * @param predicate 
   * @param object 
   */
  protected getRemoveFunction(predicate: ARTURIResource, object: ARTNode): Observable<any> {
    if (
      predicate.getAdditionalProperty(ResAttribute.HAS_CUSTOM_RANGE) && object.isResource() &&
      !object.getAdditionalProperty(ResAttribute.NOT_REIFIED)
    ) {
      return this.cfService.removeReifiedResource(this.resource, predicate, object);
    } else {
      return this.getRemoveFunctionImpl(predicate, object);
    }
  }
  /**
   * Implementation of the getRemoveFunction() for the section
   */
  abstract getRemoveFunctionImpl(predicate: ARTURIResource, object: ARTNode): Observable<any>;
  /**
   * Calls all the remove functions collected in removeAllValues
   * @param removeFnArray 
   */
  protected removeAllRicursively(removeFnArray: any[]) {
    if (removeFnArray.length == 0) return;
    removeFnArray[0].subscribe(
      () => {
        removeFnArray.shift();
        if (removeFnArray.length > 0) {
          this.removeAllRicursively(removeFnArray);
        } else {
          this.update.emit();
        }
      }
    );
  }

  /**
   * Customizes the behaviour to the edit action (fired from the editable-resource).
   * Note: in order to trigger this method, in the EditableResourceComponent it is necessary to change the
   * editActionScenario to "section" in order to deletage the handling to the related section
   * @param predicate 
   * @param object 
   */
  editHandler(_predicate: ARTURIResource, _object: ARTNode): void {
    /**
     * normally don't do nothing, only the sections that require an ad hoc edit of the value (like subPropertyChain) need to
     * override the method
     */
  }

  /**
   * Handler of "copy value to other locales" action, to implement only where foreseen
   */
  copyLocaleHandler(_predicate: ARTURIResource, _eventData: { value: ARTNode, locales: Language[] }) { }

  /**
   * Handle the event to change property fired by editable-resource
   * @param property 
   * @param eventData 
   */
  onChangeProperty(property: ARTURIResource, eventData: ChangePropertyData) {
    let actionFn: Observable<void>;
    if (eventData.copy) { // user requested a copy, so execute the add action
      actionFn = this.getAddPropertySectionAware(this.resource, eventData.newProperty, eventData.value);
    } else { //user requested a move to another property, so execute the update action
      //execute the implementation provided in the section
      actionFn = this.getChangePropertySectionAware(this.resource, property, eventData.newProperty, eventData.value);
    }
    actionFn.subscribe(
      () => {
        this.update.emit();
      }
    );
  }

  /**
   * When the object is edited or replaced requires update of res view
   */
  protected onObjectUpdate() {
    this.update.emit();
  }

  onValueClick(event: ValueClickEvent) {
    this.valueClick.emit(event);
  }

  /**
   * Tells if the given object need to be rendered as reifiedResource or as simple rdfResource.
   * A resource should be rendered as reifiedResource if the predicate has custom range and the object
   * is an ARTBNode or an ARTURIResource (so a reifiable object). Otherwise, if the object is a literal
   * or the predicate has no custom range, the object should be rendered as simple rdfResource
   * @param object object of the predicate object list to render in view.
   */
  protected renderAsReified(predicate: ARTURIResource, object: ARTNode): boolean {
    return (predicate.getAdditionalProperty(ResAttribute.HAS_CUSTOM_RANGE) && object.isResource());
  }

  toggleCollapsed() {
    this.sectionCollapsed = !this.sectionCollapsed;
  }

}