import { Component, EventEmitter, Input, Output, SimpleChanges } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { CustomViewData } from "src/app/models/CustomViews";
import { PredicateLabelSettings } from "src/app/models/Properties";
import { CustomViewsServices } from "src/app/services/custom-views.service";
import { VBContext } from "src/app/utils/VBContext";
import { ARTNode, ARTPredicateObjects, ARTResource, ARTURIResource, ResAttribute } from "../../../models/ARTResources";
import { Language } from "../../../models/LanguagesCountries";
import { AddAction, ResViewSection, ResViewUtils, ValueClickEvent } from "../../../models/ResourceView";
import { CRUDEnum, ResourceViewAuthEvaluator } from "../../../utils/AuthorizationEvaluator";
import { ResourceUtils } from "../../../utils/ResourceUtils";
import { ChangePropertyData } from '../res-view-modals/change-property-modal.component';

@Component({
  selector: "pred-obj-renderer",
  templateUrl: "./predicate-objects-renderer.component.html",
  styles: [`
        :host {
            display: block;
            margin-bottom: 4px;
        }
    `],
  standalone: false
})
export class PredicateObjectsRendererComponent {

  /**
   * INPUTS / OUTPUTS
   */

  @Input() predObj: ARTPredicateObjects;
  @Input() resource: ARTResource; //resource described
  @Input() readonly: boolean;
  @Input() rendering: boolean;
  @Input() section: ResViewSection;
  @Output() add: EventEmitter<AddAction> = new EventEmitter<AddAction>();
  @Output() remove: EventEmitter<ARTNode> = new EventEmitter<ARTNode>(); //if the event doesn't contain the node, it means "delete all"
  @Output() edit: EventEmitter<ARTNode> = new EventEmitter<ARTNode>(); //require the parent section renderer to edit the value
  @Output() changeProp = new EventEmitter<ChangePropertyData>(); //require the parent section renderer to update the property
  @Output() update = new EventEmitter();
  @Output() copyLocale = new EventEmitter<{ value: ARTNode, locales: Language[] }>(); //forward the event copyLocale from editable-resource
  @Output() valueClick = new EventEmitter<ValueClickEvent>();

  /**
   * ATTRIBUTES
   */

  predicate: ARTURIResource;
  predicateRendering: string;
  /*
  The actual object list of the input predObj.
  Important: the view must iterate over this so that the foreign URIs resolved asyncronously in ResourceViewEditorComponent are automatically 
  and properly displayed when the resolution is completed
  */
  objects: ARTNode[];
  objectsBackup: ARTNode[]; //a copy of the original objects list, used to backup when CV are toggled on/off

  showCustomView: boolean = true;
  customView: CustomViewData;

  addDisabled: boolean = false;
  deleteAllDisabled: boolean = false;
  actionMenuDisabled: boolean = false;

  addManuallyAllowed: boolean = false;
  addExteranlResourceAllowed: boolean = false;


  protected cvService: CustomViewsServices;
  protected translate: TranslateService;
  constructor(cvService: CustomViewsServices, translate: TranslateService) {
    this.cvService = cvService;
    this.translate = translate;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['resource'] || changes['readonly']) {
      this.initActionsStatus();
    }
    if (changes['predObj']) {
      this.predicate = this.predObj.getPredicate();
      this.initPredicateRendering();

      this.objectsBackup = this.predObj.getObjects().slice();
      this.objects = this.predObj.getObjects();

      if (this.predicate.getAdditionalProperty(ResAttribute.CUSTOM_VIEW_MODEL) != null) { //predicate has a CV
        this.cvService.getViewData(this.resource, this.predicate).subscribe( //retrieve data
          (cvData: CustomViewData) => {
            if (cvData.data.length > 0) { //if data is retrieved correctly, store it and remove the pred-obj
              this.customView = cvData;
              // here I need to filter out from objects those values covered by any CustomView
              this.objects = this.objects.filter(o => //keep object if
                !this.customView.data.some(d => //there is no CV that...
                  d.resource.equals(o) //describe such object
                )
              );
            }
          }
        );
      }
    }
  }

  private initPredicateRendering() {
    let lang = this.translate.currentLang;
    let rendering: string;
    let resViewPredLabelMappings: PredicateLabelSettings = VBContext.getWorkingProjectCtx().getProjectSettings().resViewPredLabelMappings;
    if (resViewPredLabelMappings && resViewPredLabelMappings.enabled && resViewPredLabelMappings.mappings[lang]) {
      //settings defined, enabled and available in the active lang
      rendering = resViewPredLabelMappings.mappings[lang][this.predicate.getURI()];
    }
    if (rendering == null) {
      rendering = this.predicate.getShow();
    }
    this.predicateRendering = rendering;
  }

  private initActionsStatus() {
    /**
     * Add is disabled if one of them is true
     * - resource is not explicit (e.g. imported or inferred) but not in staging add at the same time (addition in staging add is allowed)
     * - ResView is working in readonly mode
     * - user not authorized
     */
    this.addDisabled = !this.resource.getAdditionalProperty(ResAttribute.EXPLICIT) && !ResourceUtils.isResourceInStagingAdd(this.resource) ||
      this.readonly || !ResourceViewAuthEvaluator.isAuthorized(this.section, CRUDEnum.C, this.resource);

    /**
     * "Delete all values" is disabled if one of them is true
     * - subject resource is not explicit (e.g. imported, inferred, in staging)
     * - subject resource is in a staging status (staging-add or staging-remove)
     * - ResView is working in readonly mode
     * - user not authorized to delete operation on subject resource
     * - delete operation is not allowed on one of the objects (e.g. if user has no language capability on one of them)
     */
    this.deleteAllDisabled = !this.resource.getAdditionalProperty(ResAttribute.EXPLICIT) ||
      ResourceUtils.isResourceInStaging(this.resource) ||
      this.readonly || !ResourceViewAuthEvaluator.isAuthorized(this.section, CRUDEnum.D, this.resource);

    if (!this.deleteAllDisabled) { //if checks on subject resource are passed, performs checks on the objects
      for (let o of this.predObj.getObjects()) {
        if (!ResourceViewAuthEvaluator.isAuthorized(this.section, CRUDEnum.D, this.resource, o)) {
          this.deleteAllDisabled = true;
          break;
        }
      }
    }

    //menu disabled if all of its action are disabled
    this.actionMenuDisabled = this.addDisabled && this.deleteAllDisabled;

    this.addManuallyAllowed = !(this.section in ResViewSection) || ResViewUtils.addManuallySection.indexOf(this.section) != -1; //custom section OR add manually foreseen
    this.addExteranlResourceAllowed = ResViewUtils.addExternalResourceSection.indexOf(this.section) != -1;
  }

  toggleCustomView() {
    this.showCustomView = !this.showCustomView;
    if (this.showCustomView) { //if CV enabled, remove the resource covered by CV from objects list
      for (let i = this.objects.length - 1; i >= 0; i--) {
        if (this.customView.data.some(d => d.resource.equals(this.objects[i]))) {
          this.objects.splice(i, 1);
        }
      }
    } else { //if CV disabled, restore all the objects
      this.objects = this.objectsBackup.slice();
    }
  }

  /**
   * METHODS
   */

  /**
   * Should allow to enrich a property by opening a modal and selecting a value.
   * This is fired when the add button is clicked (the one placed on the groupPanel outline) without property parameter,
   * or hen the "+" button of a specific property panel is clicked (placed in the subPanel heading) with the property provided.
   * If property is provided (add fired from specific property panel) the modal won't allow to change it allowing so
   * to enrich just that property, otherwise, if property is not provided (add fired from the generic section panel),
   * the modal allow to change property to enrich.
   * @param predicate property to enrich.
   */
  addValue() {
    this.add.emit(AddAction.default);
  }
  addManually() {
    this.add.emit(AddAction.manually);
  }
  addExternalValue() {
    this.add.emit(AddAction.remote);
  }

  /**
   * Removes an object related to the given predicate.
   * This is fired when the "-" button is clicked (near an object).
   */
  removeValue(object: ARTNode) {
    this.remove.emit(object);
  }
  removeAllValues() {
    this.remove.emit();
  }

  /**
   * Events forwarding
   */

  onEdit(object: ARTNode) {
    this.edit.emit(object);
  }
  onUpdate() {
    this.update.emit();
  }
  onChangeProperty(data: ChangePropertyData) {
    this.changeProp.emit(data);
  }
  onCopyLocale(locales: Language[], obj: ARTNode) {
    this.copyLocale.emit({ value: obj, locales: locales });
  }

  onClick(obj: ARTNode, event: MouseEvent) {
    event.stopPropagation();
    if (obj instanceof ARTResource) {
      this.valueClick.emit(new ValueClickEvent(obj, event));
    }
  }

  onDblClick(obj: ARTNode) {
    if (obj instanceof ARTResource) {
      this.valueClick.emit(new ValueClickEvent(obj, null, true));
    }
  }

  onValueClick(event: ValueClickEvent) {
    this.valueClick.emit(event);
  }

  /**
   * Paging handlers
   */
  private pagingLimit: number = 10;
  private limitActive: boolean = true;
  showObject(index: number) {
    return !this.limitActive || (index < this.pagingLimit);
  }
  showAllButton() {
    return this.limitActive && (this.pagingLimit < this.predObj.getObjects().length);
  }
  showAll() {
    this.limitActive = false;
  }


}