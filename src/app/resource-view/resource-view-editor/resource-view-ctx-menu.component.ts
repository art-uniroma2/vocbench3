import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalOptions } from 'src/app/modal-dialogs/Modals';
import { ResourceAlignmentModalComponent } from "../../alignment/resourceAlignment/resource-alignment-modal.component";
import { ARTResource, ResAttribute } from "../../models/ARTResources";
import { SKOS } from "../../models/Vocabulary";
import { AlignmentServices } from "../../services/alignment.service";
import { RefactorServices } from "../../services/refactor.service";
import { ResourcesServices } from "../../services/resources.service";
import { AuthorizationEvaluator } from "../../utils/AuthorizationEvaluator";
import { ResourceUtils } from "../../utils/ResourceUtils";
import { VBActionsEnum } from "../../utils/VBActions";
import { ProjectContext, VBContext } from "../../utils/VBContext";
import { CreationModalServices } from "../../modal-dialogs/creation-modals/creation-modals.service";

@Component({
    selector: "res-view-menu",
    templateUrl: "./resource-view-ctx-menu.component.html",
    standalone: false
})
export class ResourceViewContextMenuComponent {

    @Input() resource: ARTResource;
    @Input() readonly: boolean;
    @Input() rendering: boolean;
    @Input() showInferred: boolean;
    @Input() projectCtx: ProjectContext;
    @Output() update = new EventEmitter();
    @Output() assertInferred = new EventEmitter();

    isSetDeprecatedDisabled: boolean;
    isAlignDisabled: boolean;
    isSpawnFromLabelDisabled: boolean;
    isAssertInferredDisabled: boolean;

    isHistoryEnabled: boolean;
    isValidationEnabled: boolean;

    constructor(private alignServices: AlignmentServices, private refactorService: RefactorServices, private resourcesService: ResourcesServices,
        private creationModals: CreationModalServices, private modalService: NgbModal,
        private router: Router) { }

    ngOnInit() {
        //init the menu items
        this.isSetDeprecatedDisabled = (
            (!this.resource.getAdditionalProperty(ResAttribute.EXPLICIT) && !ResourceUtils.isResourceInStagingAdd(this.resource)) ||
            !AuthorizationEvaluator.isAuthorized(VBActionsEnum.resourcesSetDeprecated, this.resource)
        );
        this.isAlignDisabled = (
            (!this.resource.getAdditionalProperty(ResAttribute.EXPLICIT) && !ResourceUtils.isResourceInStagingAdd(this.resource)) ||
            !AuthorizationEvaluator.isAuthorized(VBActionsEnum.alignmentAddAlignment, this.resource)
        );
        this.isSpawnFromLabelDisabled = (
            !this.resource.getAdditionalProperty(ResAttribute.EXPLICIT) ||
            !AuthorizationEvaluator.isAuthorized(VBActionsEnum.refactorSpawnNewConceptFromLabel, null, this.resource)
        );
        this.isAssertInferredDisabled = (
            !this.resource.getAdditionalProperty(ResAttribute.EXPLICIT) ||
            !AuthorizationEvaluator.isAuthorized(VBActionsEnum.resourcesAddValue, this.resource)
        );

        this.isHistoryEnabled = VBContext.getWorkingProjectCtx(this.projectCtx).getProject().isHistoryEnabled();
        this.isValidationEnabled = VBContext.getWorkingProjectCtx(this.projectCtx).getProject().isValidationEnabled();
    }

    alignResource() {
        this.openAlignmentModal().then(
            (data: any) => {
                this.alignServices.addAlignment(this.resource, data.property, data.object).subscribe(
                    () => {
                        this.update.emit(this.resource);
                    }
                );
            },
            () => { }
        );
    }

    /**
     * Opens a modal to create an alignment.
     * @return an object containing "property" and "object", namely the mapping property and the 
     * aligned object
     */
    private openAlignmentModal() {
        const modalRef: NgbModalRef = this.modalService.open(ResourceAlignmentModalComponent, new ModalOptions());
        modalRef.componentInstance.resource = this.resource; //cast since alignment is available only for URI
        return modalRef.result;
    }

    spawnNewConceptWithLabel() {
        this.creationModals.newConceptFromLabel({ key: "RESOURCE_VIEW.ACTIONS.SPAWN_CONCEPT_FROM_XLABEL" }, this.resource, SKOS.concept).then(
            data => {
                //from the resView of the xLabel I don't know the concept to which it belongs, 
                //so oldConcept in spawnNewConceptFromLabel request is null and lets the server find the oldConcept
                this.refactorService.spawnNewConceptFromLabel(this.resource, data.schemes, null,
                    data.uriResource, data.broader, data.cfValue).subscribe(
                        () => {
                            this.update.emit();
                        }
                    );
            },
            () => { }
        );
    }

    assertInferredStmts() {
        this.assertInferred.emit();
    }

    /**
     * Useful to enable menu item only for URIResource
     */
    isURIResource() {
        return this.resource.isURIResource();
    }

    setAsDeprecated() {
        this.resourcesService.setDeprecated(this.resource).subscribe();
    }


    goToResourceHistory() {
        this.router.navigate(['/History'], {state: { resId: this.resource.getNominalValue() }});
    }

    goToResourceValidation() {
        this.router.navigate(['/Validation'], {state: { resId: this.resource.getNominalValue() }});
    }

}