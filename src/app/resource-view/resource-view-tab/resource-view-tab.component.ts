import { Component, EventEmitter, Input, Output, SimpleChanges, ViewChild } from '@angular/core';
import { NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import { ARTResource } from 'src/app/models/ARTResources';
import { ValueClickEvent } from 'src/app/models/ResourceView';

@Component({
  selector: 'resource-view-tab',
  templateUrl: './resource-view-tab.component.html',
  host: { class: "vbox" },
  standalone: false
})
export class ResourceViewTabComponent {

  @Input() resource: ARTResource;
  @Output() update = new EventEmitter<ARTResource>(); //(useful to notify parent tab that resource is updated)
  @Output() valueClick = new EventEmitter<ValueClickEvent>();
  @Output() historyChange = new EventEmitter<ARTResource>();

  activeIdx: number = -1;
  resourceHistory: ARTResource[] = [];

  ngOnChanges(changes: SimpleChanges) {
    if (changes['resource']) {
      /*
      Prevent issue like:
      - resource-view get res description, parse the AnnotatedValue and emit update event
      - resource-view-tab forward update event
      - resource-view-tabset get the AnnotatedValue from event and update stored tab resource
      - updated tab resource is detected in ngOnChanges as new resource, even if it's the same (simply annotated)
      */
      if (this.activeIdx != -1) {
        let prevRes: ARTResource = this.resourceHistory[this.activeIdx];
        if (prevRes.equals(this.resource)) {
          return;
        }
      }

      //remove the rest part of the history and append the new resourse at the end
      //e.g. r0,r1,r2,r3 in the history, current idx is 1 (r1 active res), rN is added => resulting history must be r0, r1 + rN
      this.resourceHistory = this.resourceHistory.slice(0, this.activeIdx+1);
      this.resourceHistory.push(this.resource);
      this.activeIdx = this.resourceHistory.length - 1; //activate new added tab

      //no more than 10 elements in the history
      if (this.resourceHistory.length > 10) {
        this.resourceHistory.shift(); // Remove the first element
      }
    }
  }

  goToResInHistory(res: ARTResource) {
    this.activeIdx = this.resourceHistory.findIndex(r => r.equals(res));
    this.onActiveIndexChange();
  }

  previousResource() {
    this.activeIdx--;
    this.onActiveIndexChange();
  }

  nextResource() {
    this.activeIdx++;
    this.onActiveIndexChange();
  }

  /**
   * When resource of history changes, emit update event so to update also the selector in the tabset
   */
  private onActiveIndexChange() {
    this.historyChange.emit(this.resourceHistory[this.activeIdx]);
  }

  /**
   * Removes a resource from history.
   * To use when a resource is deleted from tree/list and then needs to be removed from history
   * @param resource 
   */
  removeFromHistory(resource: ARTResource) {
    let idx = this.resourceHistory.findIndex(r => r.equals(resource));
    if (idx != -1) {
      this.resourceHistory.splice(idx, 1);
    }
  }

  /*
  Handlers of browsing buttons (back/next).
  Here distinguish between short and long click and emulates the behavior of web browsers:
  - short click: go to the previous/next resource
  - long click: open a dropdown to show history:
    - left => previous resources in history
    - right => next resources in history
  */

  @ViewChild("leftDrop") leftDrop: NgbDropdown;
  @ViewChild("rightDrop") rightDrop: NgbDropdown;

  private pressTimer: any;
  private isLongPress = false;

  onMouseDown(direction: BrowseDir) {
    this.leftDrop.close();
    this.rightDrop.close();
    this.isLongPress = false;
    this.pressTimer = setTimeout(() => {
      this.isLongPress = true;
      this.onLongPress(direction);
    }, 800);
  }
  onMouseUp(direction: BrowseDir) {
    clearTimeout(this.pressTimer);
    if (!this.isLongPress) {
      this.onClick(direction);
    }
  }
  onClick(direction: BrowseDir) {
    if (direction == 'left') {
      this.previousResource();
    } else {
      this.nextResource();
    }
  }
  onLongPress(direction: BrowseDir) {
    if (direction == 'left') {
      this.leftDrop.open();
    } else {
      this.rightDrop.open();
    }
  }


  /*
   * Forward of events from resource-view child
   */

  onValueClick(event: ValueClickEvent) {
    this.valueClick.emit(event);
  }

  onUpdate(event: ARTResource) {
    this.update.emit(event);
  }

}


type BrowseDir = "left" | "right"