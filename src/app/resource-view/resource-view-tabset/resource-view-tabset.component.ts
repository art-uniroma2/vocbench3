import { ChangeDetectorRef, Component, EventEmitter, Output, QueryList, ViewChildren } from "@angular/core";
import { Subscription } from "rxjs";
import { ValueClickEvent } from "src/app/models/ResourceView";
import { ResourceUtils } from "src/app/utils/ResourceUtils";
import { VBEventHandler } from "src/app/utils/VBEventHandler";
import { ModalType } from "src/app/modal-dialogs/Modals";
import { BasicModalServices } from "src/app/modal-dialogs/basic-modals/basic-modals.service";
import { ARTResource, ARTURIResource } from "../../models/ARTResources";
import { ResourceViewTabComponent } from "../resource-view-tab/resource-view-tab.component";

@Component({
  selector: "resource-view-tabset",
  templateUrl: "./resource-view-tabset.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class ResourceViewTabsetComponent {

  //tells to the parent component that there are no more RV open
  @Output() empty = new EventEmitter<void>();
  @Output() valueClick = new EventEmitter<ValueClickEvent>();
  @Output() tabSelect = new EventEmitter<ARTResource>(); //emits event when a tab is selected, useful to keep in sync tabbed ResView and trees/lists

  @ViewChildren(ResourceViewTabComponent) rvTabComponents: QueryList<ResourceViewTabComponent>;

  tabs: Tab[] = [];

  activeTab: Tab;

  eventSubscriptions: Subscription[] = [];


  constructor(
    private eventHandler: VBEventHandler,
    private basicModals: BasicModalServices,
    private cdRef: ChangeDetectorRef
  ) {
    this.eventSubscriptions.push(
      this.eventHandler.refreshDataBroadcastEvent.subscribe(() => {
        this.closeAll();
      })
    );
  }



  ngOnDestroy() {
    this.eventSubscriptions.forEach(s => s.unsubscribe());
  }

  selectResource(resource: ARTResource, newTab?: boolean) {
    //search for a tab describing the given resource
    let tab = this.tabs.find(t => t.resource.equals(resource));
    if (tab != null) { //resource already open in a tab => activate it
      this.activeTab = tab;
    } else { //resource not yet open in a tab => open it
      if (newTab || this.tabs.length == 0) {
        this.addTab(resource);
      } else {
        this.activeTab.resource = resource;
      }
    }
  }

  private addTab(resource: ARTResource) {
    let tab: Tab = { resource: resource };
    this.tabs.push(tab);

    this.cdRef.detectChanges();
    this.activeTab = tab;
  }

  /**
   * Select and activate a tab
   * @param tab 
   * @param emitSelect if true emits a select event
   */
  private selectTab(tab: Tab) {
    this.activeTab = tab;
    this.tabSelect.emit(tab.resource);
  }

  closeTab(tab: Tab) {
    //remove the tab to close
    let idxTabToClose: number = this.tabs.indexOf(tab);
    this.tabs.splice(idxTabToClose, 1);
    if (this.tabs.length == 0) { //no more tabs
      this.empty.emit();
    } else { //other tabs present => active another tab
      if (tab == this.activeTab) { //closed tab is the active one
        //select the previous tab, or the following in case the closed tab was the first one
        if (idxTabToClose > 0) {
          this.selectTab(this.tabs[idxTabToClose - 1]);
        } else {
          this.selectTab(this.tabs[idxTabToClose]);
        }
      }
    }
  }

  closeAll() {
    this.tabs = [];
    this.empty.emit();
  }

  promptAddress() {
    this.basicModals.prompt({ key: 'RESOURCE_VIEW.ACTIONS.OPEN_RES_IN_NEW_TAB' }, { value: 'IRI' }).then(
      iri => {
        if (ResourceUtils.testIRI(iri)) {
          this.addTab(new ARTURIResource(iri));
        } else {
          this.cdRef.detectChanges();
          this.basicModals.alert({ key: "STATUS.ERROR" }, { key: "MESSAGES.INVALID_IRI", params: { iri: iri } }, ModalType.warning);
        }
      },
      () => { }
    );
  }

  deleteResource(res: ARTResource) {
    let tab = this.tabs.find(t => t.resource.equals(res));
    if (tab != null) {
      this.closeTab(tab);
    }
    //update history of tabs (if deleted resource is in tab history)
    this.cdRef.detectChanges();
    this.rvTabComponents.toArray().forEach(rvTabComp => {
      rvTabComp.removeFromHistory(res);
    });
  }

  /**
   * When changes on resource view make change the show of the resource, update the resource of the tab
   * so that the header of the tab shows the updated resource.
   */
  onResourceUpdate(resource: ARTResource, tab: Tab) {
    tab.resource = resource;
  }

  /**
   * When in a tab the active resource (in history) changes, emit tabSelect to (optionally) sync with tree/list
   * @param resource 
   */
  onHistoryChange(resource: ARTResource) {
    this.tabSelect.emit(resource);
  }

  onValueClick(event: ValueClickEvent) {
    if (event.shift) {
      //shift+click => propagate the event to be handled in the parent (eventually open RV in the side tabset)
      this.valueClick.emit(event);
    } else if (event.ctrl) {
      //ctrl+click => open internally to this tabset
      this.selectResource(event.value, true);
    } else if (event.double) {
      //double click => open in the same tabset
      this.selectResource(event.value, false);
    }
  }

  onTabMouseDown(event: MouseEvent, tab: Tab) {
    if (event.button == 1) { //wheel/middle click => close tab
      this.closeTab(tab);
    } else if (event.shiftKey) { //shift click on tab => emit event so tabset move tab to side tabset
      this.valueClick.emit(new ValueClickEvent(tab.resource, event));
      if (this.activeTab != tab) {
        //shift + click on unfocused tab => move and close it
        event.stopImmediatePropagation(); //stop propagation to prevent ngb nav to autofocus the tab
        event.preventDefault(); //to prevent opening SV on a new browser window
        this.closeTab(tab);
      }
    } else {
      this.selectTab(tab); //simple click => select/focus tab
    }
  }

}


interface Tab {
  resource: ARTResource;
}