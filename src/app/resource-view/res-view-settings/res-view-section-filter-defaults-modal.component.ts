import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin } from "rxjs";
import { map } from "rxjs/operators";
import { ExtensionPointID, Scope, Settings } from "src/app/models/Plugins";
import { ResViewTemplate, SectionFilterPreference, SettingsEnum } from "src/app/models/Properties";
import { VBContext } from "src/app/utils/VBContext";
import { VBProperties } from "src/app/utils/VBProperties";
import { SettingsServices } from "../../services/settings.service";

@Component({
    selector: "res-view-section-filter-defaults-modal",
    templateUrl: "./res-view-section-filter-defaults-modal.component.html",
    standalone: false
})
export class ResViewSectionFilterDefaultsModalComponent {

    template: ResViewTemplate; //template on which base the sections filter

    userDefault: SectionFilterPreference;
    projectDefault: SectionFilterPreference;

    scopes: Scope[];
    activeScope: Scope;

    private restoreProjDefaultOpt: string = "COMMONS.ACTIONS.RESTORE_PROJ_DEFAULT";
    private applyUserDefaultOpt: string = "COMMONS.ACTIONS.APPLY_USER_DEFAULT";
    multiActionOpts: string[] = [this.restoreProjDefaultOpt, this.applyUserDefaultOpt];
    selectedMultiActionOpt: string = this.multiActionOpts[0];

    constructor(
        public activeModal: NgbActiveModal, 
        private settingsService: SettingsServices, 
        private vbProp: VBProperties
    ) { }

    ngOnInit() {

        this.template = VBContext.getWorkingProjectCtx().getProjectSettings().resourceView.templates;

        let initProjDefaults = this.settingsService.getSettingsDefault(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, Scope.PROJECT).pipe(
            map((setting: Settings) => {
                this.projectDefault = setting.getPropertyValue(SettingsEnum.resViewPartitionFilter);
            })
        );
        let initUserDefaults = this.settingsService.getSettingsDefault(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, Scope.USER).pipe(
            map((setting: Settings) => {
                this.userDefault = setting.getPropertyValue(SettingsEnum.resViewPartitionFilter);
            })
        );
        forkJoin([initProjDefaults, initUserDefaults]).subscribe(
            () => {
                this.scopes = [];
                if (this.projectDefault) {
                    this.scopes.push(Scope.PROJECT);
                }
                if (this.userDefault) {
                    this.scopes.push(Scope.USER);
                }
                if (this.scopes.length > 0) {
                    this.activeScope = this.scopes[0];
                }
            }
        );
    }

    /**
     * Action perfomed when only one default is available
     */
    restoreDefault() {
        if (this.scopes[0] == Scope.PROJECT) {
            //restore project default
            this.restoreProjDefault();
        } else { //USER
            //apply user default
            this.applyUserDefault();
        }
    }

    applyAction() {
        if (this.selectedMultiActionOpt == this.restoreProjDefaultOpt) {
            this.restoreProjDefault();
        } else if (this.selectedMultiActionOpt == this.applyUserDefaultOpt) {
            this.applyUserDefault();
        }
    }

    restoreProjDefault() {
        this.vbProp.setResViewSectionFilter(null).subscribe(
            () => {
                this.ok();
            }
        );
    }

    applyUserDefault() {
        this.vbProp.setResViewSectionFilter(this.userDefault).subscribe(
            () => {
                this.ok();
            }
        );
    }


    ok() {
        this.activeModal.close();
    }

    cancel() {
        this.activeModal.dismiss();
    }


}