import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ResViewTemplate, SectionFilterPreference } from "../../models/Properties";
import { LocalStorageManager } from "src/app/utils/LocalStorageManager";

@Component({
  selector: "res-view-settings-modal",
  templateUrl: "./res-view-settings-modal.component.html",
  standalone: false
})
export class ResViewSettingsModalComponent {

  sectionsFilter: SectionFilterPreference;
  template: ResViewTemplate; //template on which base the sections filter

  showGuidelines: boolean = true;

  constructor(public activeModal: NgbActiveModal) { }

  closeGuideline(dontShowAgain: boolean) {
    this.showGuidelines = false;
    if (dontShowAgain) {
      LocalStorageManager.setItem(LocalStorageManager.RES_VIEW_GUIDELINES_DISMISSED, true);
    }
  }

  ok() {
    this.activeModal.close();
  }

}