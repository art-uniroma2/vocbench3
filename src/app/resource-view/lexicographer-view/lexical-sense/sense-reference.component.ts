import { Component, Input } from "@angular/core";
import { SenseReference } from "src/app/models/LexicographerView";

@Component({
    selector: "sense-ref",
    templateUrl: "./sense-reference.component.html",
    host: { class: "d-block" },
    standalone: false
})
export class SenseReferenceComponent {
    @Input() ref: SenseReference;

    constructor() {}

}