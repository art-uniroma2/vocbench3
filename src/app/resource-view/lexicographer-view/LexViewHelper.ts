import { Injectable } from "@angular/core";
import { from, Observable, of } from "rxjs";
import { map, mergeMap } from "rxjs/operators";
import { ARTResource, ARTURIResource } from "src/app/models/ARTResources";
import { OntoLexLemonServices } from "src/app/services/ontolex-lemon.service";
import { ConstituentListCreatorModalReturnData } from "../resource-view-editor/res-view-modals/constituent-list-creator-modal.component";
import { ResViewModalServices } from "../resource-view-editor/res-view-modals/resViewModalServices";

/**
 * Contains methods shared in the lex view
 */
@Injectable()
export class LexViewHelper {

  constructor(private ontolexService: OntoLexLemonServices, private resViewModals: ResViewModalServices) { }

  setConstituents(sourceEntry: ARTResource): Observable<boolean> {
    return from(
      this.resViewModals.createConstituentList({ key: "DATA.ACTIONS.CREATE_CONSTITUENTS_LIST" }).then(
        (data: ConstituentListCreatorModalReturnData) => {
          return this.ontolexService.setLexicalEntryConstituents(sourceEntry as ARTURIResource, data.list, data.ordered).pipe(
            map(() => true)
          );
        },
        () => of(false)
      )
    ).pipe( //for flattening Observable<Observable<boolean>> returned above
      mergeMap(done => done)
    );
  }

}