import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ARTResource, ARTURIResource } from "src/app/models/ARTResources";
import { EntryReference, LexicalEntry, LexicalResourceUtils } from "src/app/models/LexicographerView";
import { OntoLexLemonServices } from "src/app/services/ontolex-lemon.service";
import { AuthorizationEvaluator } from "src/app/utils/AuthorizationEvaluator";
import { VBActionsEnum } from "src/app/utils/VBActions";

@Component({
    selector: "subterm",
    templateUrl: "./subterm.component.html",
    host: { class: "d-block" },
    standalone: false
})
export class SubtermComponent {
    @Input() readonly: boolean = false;
    @Input() entry: LexicalEntry;
    @Input() subterm: EntryReference;
    @Output() dblclickObj: EventEmitter<ARTResource> = new EventEmitter<ARTResource>();
    @Output() update = new EventEmitter<void>(); //something changed, request to update

    deleteAuthorized: boolean;

    constructor(private ontolexService: OntoLexLemonServices) { }

    ngOnInit() {
        this.deleteAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.ontolexRemoveSubterm) && !this.readonly && !LexicalResourceUtils.isTripleInStaging(this.subterm);
    }

    delete() {
        this.ontolexService.removeSubterm(this.entry.id as ARTURIResource, this.subterm.id as ARTURIResource).subscribe(
            () => {
                this.update.emit();
            }
        );
    }

    onDblClick() {
        this.dblclickObj.emit(this.subterm.id);
    }

}