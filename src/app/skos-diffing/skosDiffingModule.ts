import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../modules/sharedModule';
import { CreateDiffingTaskModalComponent } from './modals/create-diffing-task-modal.component';
import { SkosDiffingComponent } from './skos-diffing.component';

@NgModule({
    imports: [
        CommonModule,
        DragDropModule,
        FormsModule,
        SharedModule,
        TranslateModule
    ],
    declarations: [
        CreateDiffingTaskModalComponent,
        SkosDiffingComponent,
    ],
    exports: [SkosDiffingComponent],
    providers: []
})
export class SkosDiffingModule { }