import { Component } from "@angular/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import FileSaver from "file-saver";
import { CustomService } from "src/app/models/CustomService";
import { InvokableReporter, ServiceInvocationDefinition } from "src/app/models/InvokableReporter";
import { LocalStorageManager } from "src/app/utils/LocalStorageManager";
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { Reference } from "../../models/Configuration";
import { InvokableReportersServices } from "../../services/invokable-reporters.service";
import { AuthorizationEvaluator } from "../../utils/AuthorizationEvaluator";
import { VBActionsEnum } from "../../utils/VBActions";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { ImportInvokableReporterModalComponent } from "./modals/import-invokable-reporter-modal.component";
import { InvokableReporterModalServices } from "./modals/invokableReporterModalServices";

@Component({
    selector: "invokable-reporters-component",
    templateUrl: "./invokable-reporters-page.component.html",
    host: { class: "hbox" },
    standalone: false
})
export class InvokableReportersPageComponent {

    reporters: Reference[];
    selectedReporter: Reference;

    createReporterAuthorized: boolean;
    deleteReporterAuthorized: boolean;

    constructor(
        private invokableReporterService: InvokableReportersServices,
        private invokableReporterModals: InvokableReporterModalServices,
        private basicModals: BasicModalServices,
        private modalService: NgbModal,
        private translateService: TranslateService,
    ) { }

    ngOnInit() {
        this.createReporterAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.invokableReporterCreate);
        this.deleteReporterAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.invokableReporterDelete);

        this.initReporters();
    }

    private initReporters() {
        this.invokableReporterService.getInvokableReporterIdentifiers().subscribe(
            references => {
                this.reporters = references;
            }
        );
    }

    selectReporter(reporter: Reference) {
        this.selectedReporter = reporter;
    }

    createReporter() {
        this.invokableReporterModals.openInvokableReporterEditor({ key: "INVOKABLE_REPORTERS.ACTIONS.CREATE_INVOKABLE_REPORT" }, this.reporters).then(
            () => {
                this.initReporters();
            },
            () => { }
        );
    }

    deleteReporter() {
        this.basicModals.confirm({ key: "INVOKABLE_REPORTERS.ACTIONS.DELETE_INVOKABLE_REPORT" }, { key: "INVOKABLE_REPORTERS.MESSAGES.DELETE_INVOKABLE_REPORT_CONFIRM" }, ModalType.warning).then(
            () => {
                this.invokableReporterService.deleteInvokableReporter(this.selectedReporter.relativeReference).subscribe(
                    () => {
                        this.initReporters();
                    }
                );
            }
        );
    }

    exportInvokableReporter() {
        //before exporting, inform user if reporter uses custom services
        this.invokableReporterService.getInvokableReporter(this.selectedReporter.relativeReference).subscribe(
            (reporter: InvokableReporter) => {
                let sections: ServiceInvocationDefinition[] = reporter.getPropertyValue("sections");
                let customServices: string[] = sections.filter(s => s.extensionPath == CustomService.CUSTOM_SERVICE_PATH).map(s => s.service + "/" + s.operation);
                if (customServices.length > 0) {
                    let msg = this.translateService.instant("INVOKABLE_REPORTERS.MESSAGES.WARN_EXPORT_WITH_CUSTOM_SERVICES");
                    msg += "\n\n" + customServices.map(s => "- " + s).join(";\n");
                    this.basicModals.confirmCheckCookie({ key: "STATUS.WARNING" }, msg, LocalStorageManager.WARNING_INVOKABLE_REPORTER_WITH_CUSTOM_SERVICE, ModalType.warning).then(
                        () => {
                            this.exportImpl();
                        },
                        () => {}
                    );
                } else {
                    this.exportImpl();
                }
            }
        );
    }

    private exportImpl() {
        this.invokableReporterService.exportInvokableReporter(this.selectedReporter.relativeReference).subscribe(
            blob => {
                FileSaver.saveAs(blob, this.selectedReporter.identifier + ".cfg");
            }
        );
    }

    importInvokableReporter() {
        const modalRef: NgbModalRef = this.modalService.open(ImportInvokableReporterModalComponent, new ModalOptions());
        return modalRef.result.then(
            () => {
                this.initReporters();
            },
            () => { }
        );
    }

}