import { Component } from "@angular/core";
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { CustomServiceServices } from "../../services/custom-service.service";
import { AuthorizationEvaluator } from "../../utils/AuthorizationEvaluator";
import { VBActionsEnum } from "../../utils/VBActions";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { CustomServiceModalServices } from "./modals/customServiceModalServices";
import FileSaver from "file-saver";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { ImportCustomServiceModalComponent } from "./modals/import-custom-service-modal.component";

@Component({
    selector: "custom-services-component",
    templateUrl: "./custom-services-page.component.html",
    host: { class: "hbox" },
    standalone: false
})
export class CustomServicesPageComponent {

    serviceIds: string[];
    selectedServiceId: string;

    createServiceAuthorized: boolean;
    deleteServiceAuthorized: boolean;

    constructor(
        private customServService: CustomServiceServices, 
        private basicModals: BasicModalServices,
        private customServiceModals: CustomServiceModalServices,
        private modalService: NgbModal
    ) { }

    ngOnInit() {
        this.createServiceAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.customServiceCreate);
        this.deleteServiceAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.customServiceDelete);

        this.initServices();
    }

    initServices() {
        this.customServService.getCustomServiceIdentifiers().subscribe(
            ids => {
                this.serviceIds = ids;
            }
        );
    }

    selectService(id: string) {
        if (this.selectedServiceId != id) {
            this.selectedServiceId = id;
        }
    }


    createService() {
        this.customServiceModals.openCustomServiceEditor({ key: "CUSTOM_SERVICES.ACTIONS.CREATE_CUSTOM_SERVICE" }).then(
            () => {
                this.initServices();
            },
            () => { }
        );
    }

    deleteService() {
        this.basicModals.confirm({ key: "CUSTOM_SERVICES.ACTIONS.DELETE_CUSTOM_SERVICE" }, { key: "MESSAGES.DELETE_CUSTOM_SERVICE_CONFIRM" }, ModalType.warning).then(
            () => {
                this.customServService.deleteCustomService(this.selectedServiceId).subscribe(
                    () => {
                        this.selectedServiceId = null;
                        this.initServices();
                    }
                );
            }
        );
    }

    exportCustomService() {
        this.customServService.exportCustomService(this.selectedServiceId).subscribe(
            blob => {
                FileSaver.saveAs(blob, this.selectedServiceId + ".cfg");
            }
        );
    }

    importCustomService() {
        const modalRef: NgbModalRef = this.modalService.open(ImportCustomServiceModalComponent, new ModalOptions());
        return modalRef.result.then(
            () => {
                this.initServices();
            },
            () => {}
        );
    }

    reload() {
        this.customServService.reloadCustomServices().subscribe(
            () => {
                this.initServices();
            }
        );
    }

}