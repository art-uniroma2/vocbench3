import { Component, EventEmitter, Input, Output } from "@angular/core";
import { RDFResourceRolesEnum } from "../models/ARTResources";
import { Language } from "../models/LanguagesCountries";
import { OWL, SKOS } from "../models/Vocabulary";
import { UIUtils } from "../utils/UIUtils";
import { VBContext } from "../utils/VBContext";

@Component({
  selector: "icv-config-panel",
  templateUrl: "./icv-config-panel.component.html",
  styles: ['.configPanel { max-height: 170px }'],
  standalone: false
})
export class IcvConfigPanelComponent {

  @Input() checkRoles: boolean = false;
  @Input() checkLanguages: boolean = false;

  @Output() rolesChange = new EventEmitter<RDFResourceRolesEnum[]>();
  @Output() langsChange = new EventEmitter<string[]>();

  configPanelOpen: boolean = true;

  roles: RoleCheckItem[];
  languages: LanguageCheckItem[];

  constructor() { }

  ngOnInit() {
    if (this.checkLanguages) {
      this.languages = [];
      let projectLangs = VBContext.getWorkingProjectCtx().getProjectSettings().projectLanguagesSetting;
      projectLangs.forEach(pl => {
        this.languages.push({ lang: pl, check: pl.mandatory });
      });
      this.onLangsChange();
    }

    if (this.checkRoles) {
      let modelType: string = VBContext.getWorkingProject().getModelType();
      this.roles = [
        { role: RDFResourceRolesEnum.concept, show: "Concept", img: UIUtils.getRoleImageSrc(RDFResourceRolesEnum.concept), check: modelType == SKOS.uri },
        { role: RDFResourceRolesEnum.conceptScheme, show: "ConceptScheme", img: UIUtils.getRoleImageSrc(RDFResourceRolesEnum.conceptScheme), check: modelType == SKOS.uri },
        { role: RDFResourceRolesEnum.skosCollection, show: "Collection", img: UIUtils.getRoleImageSrc(RDFResourceRolesEnum.skosCollection), check: false },
        { role: RDFResourceRolesEnum.cls, show: "Class", img: UIUtils.getRoleImageSrc(RDFResourceRolesEnum.cls), check: modelType == OWL.uri },
        { role: RDFResourceRolesEnum.individual, show: "Individual", img: UIUtils.getRoleImageSrc(RDFResourceRolesEnum.individual), check: false },
        { role: RDFResourceRolesEnum.property, show: "Property", img: UIUtils.getRoleImageSrc(RDFResourceRolesEnum.property), check: false }
      ];
      this.onRolesChange();
    }
  }

  checkAllRoles(check: boolean) {
    this.roles.forEach(r => {
      r.check = check;
    });
    this.onRolesChange();
  }

  checkAllLangs(check: boolean) {
    this.languages.forEach(l => {
      l.check = check;
    });
    this.onLangsChange();
  }

  checkAllLangsMandatory() {
    this.languages.forEach(l => {
      l.check = l.lang.mandatory;
    });
    this.onLangsChange();
  }

  onLangsChange() {
    let langsParam: string[] = [];
    for (const l of this.languages) {
      if (l.check) {
        langsParam.push(l.lang.tag);
      }
    }
    this.langsChange.emit(langsParam);
  }

  onRolesChange() {
    let rolesParam: RDFResourceRolesEnum[] = [];
    for (const r of this.roles) {
      if (r.check) {
        rolesParam.push(r.role);
      }
    }
    this.rolesChange.emit(rolesParam);
  }

}

class LanguageCheckItem {
  lang: Language;
  check: boolean;
}

class RoleCheckItem {
  role: RDFResourceRolesEnum;
  show: string;
  img: string;
  check: boolean;
}