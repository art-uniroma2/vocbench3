import { Component } from "@angular/core";
import { AbstractIcvComponent } from "../abstractIcvComponent";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { SharedModalServices } from "../../modal-dialogs/shared-modals/shared-modals.service";
import { ARTURIResource } from "../../models/ARTResources";
import { UIUtils } from "../../utils/UIUtils";
import { IcvServices } from "../../services/icv.service";

@Component({
    selector: "invalid-uri-component",
    templateUrl: "./invalidUriComponent.html",
    host: { class: "pageComponent" },
    standalone: false
})
export class InvalidUriComponent extends AbstractIcvComponent {

    checkRoles: boolean = false;
    checkLanguages: boolean = false;

    brokenRecordList: ARTURIResource[];

    constructor(private icvService: IcvServices, basicModals: BasicModalServices, sharedModals: SharedModalServices) {
        super(basicModals, sharedModals);
    }

    /**
     * Run the check
     */
    executeIcv() {
        UIUtils.startLoadingDiv(document.getElementById("blockDivIcv"));
        this.icvService.listLocalInvalidURIs().subscribe(
            resources => {
                UIUtils.stopLoadingDiv(document.getElementById("blockDivIcv"));
                this.brokenRecordList = resources;
                this.initPaging(this.brokenRecordList);
            }
        );
    }

}