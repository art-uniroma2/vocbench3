import { Component } from "@angular/core";
import { ARTResource } from "../../models/ARTResources";
import { IcvServices } from "../../services/icv.service";
import { UIUtils } from "../../utils/UIUtils";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { SharedModalServices } from "../../modal-dialogs/shared-modals/shared-modals.service";
import { AbstractIcvComponent } from "../abstractIcvComponent";

@Component({
    selector: "disjoint-exact-match-component",
    templateUrl: "./disjointExactMatchConceptComponent.html",
    host: { class: "pageComponent" },
    standalone: false
})
export class DisjointExactMatchConceptComponent extends AbstractIcvComponent {

    checkLanguages = false;
    checkRoles = false;
    brokenRecordList: ARTResource[];

    constructor(private icvService: IcvServices, basicModals: BasicModalServices, sharedModals: SharedModalServices) {
        super(basicModals, sharedModals);
    }

    /**
     * Run the check
     */
    executeIcv() {
        UIUtils.startLoadingDiv(document.getElementById("blockDivIcv"));
        this.icvService.listConceptsExactMatchDisjoint().subscribe(
            resources => {
                UIUtils.stopLoadingDiv(document.getElementById("blockDivIcv"));
                this.brokenRecordList = resources;

                this.initPaging(this.brokenRecordList);
            }
        );
    
    }

}