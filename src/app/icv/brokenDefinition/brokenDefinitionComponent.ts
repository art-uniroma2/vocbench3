import { Component } from "@angular/core";
import { AbstractIcvComponent } from "../abstractIcvComponent";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { SharedModalServices } from "../../modal-dialogs/shared-modals/shared-modals.service";
import { BrowsingModalServices } from "../../modal-dialogs/browsing-modals/browsing-modals.service";
import { ARTURIResource } from "../../models/ARTResources";
import { SKOS } from "../../models/Vocabulary";
import { IcvServices } from "../../services/icv.service";
import { UIUtils } from "../../utils/UIUtils";

@Component({
    selector: "broken-definition-component",
    templateUrl: "./brokenDefinitionComponent.html",
    host: { class: "pageComponent" },
    standalone: false
})
export class BrokenDefinitionComponent extends AbstractIcvComponent {

    checkLanguages = false;
    checkRoles = true;

    noteProperty: ARTURIResource = SKOS.note;

    brokenRecordList: { subject: ARTURIResource, predicate: ARTURIResource, object: ARTURIResource }[];

    constructor(private icvService: IcvServices, private browsingModals: BrowsingModalServices,
        basicModals: BasicModalServices, sharedModals: SharedModalServices) {
        super(basicModals, sharedModals);
    }

    /**
     * Run the check
     */
    executeIcv() {
        UIUtils.startLoadingDiv(document.getElementById("blockDivIcv"));
        this.icvService.listBrokenDefinitions(this.rolesToCheck, this.noteProperty).subscribe(
            records => {
                UIUtils.stopLoadingDiv(document.getElementById("blockDivIcv"));
                this.brokenRecordList = records;
                this.initPaging(this.brokenRecordList);
            }
        );
    }

    changeProperty() {
        this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }, [SKOS.note]).then(
            property => {
                this.noteProperty = property;
            },
            () => { }
        );
    }

}