import { Component } from "@angular/core";
import { AbstractIcvComponent } from "../abstractIcvComponent";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { SharedModalServices } from "../../modal-dialogs/shared-modals/shared-modals.service";
import { ARTURIResource } from "../../models/ARTResources";
import { IcvServices } from "../../services/icv.service";
import { UIUtils } from "../../utils/UIUtils";

@Component({
    selector: "cyclic-concept-component",
    templateUrl: "./cyclicConceptComponent.html",
    host: { class: "pageComponent" },
    standalone: false
})
export class CyclicConceptComponent extends AbstractIcvComponent {

    checkLanguages = false;
    checkRoles = false;
    brokenRecordList: ARTURIResource[][];

    constructor(private icvService: IcvServices, basicModals: BasicModalServices, sharedModals: SharedModalServices) {
        super(basicModals, sharedModals);
    }

    /**
     * Run the check
     */
    executeIcv() {
        UIUtils.startLoadingDiv(document.getElementById("blockDivIcv"));
        this.icvService.listConceptsHierarchicalCycles().subscribe(
            cycles => {
                UIUtils.stopLoadingDiv(document.getElementById("blockDivIcv"));
                this.brokenRecordList = cycles;
                this.initPaging(this.brokenRecordList);
            }
        );
    }

}