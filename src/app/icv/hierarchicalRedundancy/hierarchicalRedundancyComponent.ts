import { Component } from "@angular/core";
import { AbstractIcvComponent } from "../abstractIcvComponent";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { SharedModalServices } from "../../modal-dialogs/shared-modals/shared-modals.service";
import { ARTURIResource } from "../../models/ARTResources";
import { IcvServices } from "../../services/icv.service";
import { UIUtils } from "../../utils/UIUtils";

@Component({
    selector: "hierarchical-redundancy-component",
    templateUrl: "./hierarchicalRedundancyComponent.html",
    host: { class: "pageComponent" },
    standalone: false
})
export class HierarchicalRedundancyComponent extends AbstractIcvComponent {

    checkLanguages = false;
    checkRoles = false;

    sameScheme: boolean = true;

    brokenRecordList: { subject: ARTURIResource, predicate: ARTURIResource, object: ARTURIResource }[];

    constructor(private icvService: IcvServices, basicModals: BasicModalServices, sharedModals: SharedModalServices) {
        super(basicModals, sharedModals);
    }

    /**
     * Run the check
     */
    executeIcv() {
        UIUtils.startLoadingDiv(document.getElementById("blockDivIcv"));
        this.icvService.listConceptsHierarchicalRedundancies(this.sameScheme).subscribe(
            redundancies => {
                UIUtils.stopLoadingDiv(document.getElementById("blockDivIcv"));
                this.brokenRecordList = redundancies;
                this.initPaging(this.brokenRecordList);
            }
        );
    }

}