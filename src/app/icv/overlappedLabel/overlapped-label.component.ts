import { Component } from "@angular/core";
import { ARTLiteral, ARTResource, ARTURIResource, RDFResourceRolesEnum } from "../../models/ARTResources";
import { IcvServices } from "../../services/icv.service";
import { Deserializer } from "../../utils/Deserializer";
import { UIUtils } from "../../utils/UIUtils";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { SharedModalServices } from "../../modal-dialogs/shared-modals/shared-modals.service";
import { AbstractIcvComponent } from "../abstractIcvComponent";

@Component({
    selector: "overlapped-label",
    templateUrl: "./overlapped-label.component.html",
    host: { class: "pageComponent" },
    standalone: false
})
export class OverlappedLabelComponent extends AbstractIcvComponent {

    checkLanguages = false;
    checkRoles = true;
    brokenRecordList: BrokendRecord[];

    constructor(private icvService: IcvServices, basicModals: BasicModalServices, sharedModals: SharedModalServices) {
        super(basicModals, sharedModals);
    }

    /**
     * Run the check
     */
    executeIcv() {
        UIUtils.startLoadingDiv(document.getElementById("blockDivIcv"));
        this.icvService.listResourcesWithOverlappedLabels(this.rolesToCheck).subscribe(
            resources => {
                UIUtils.stopLoadingDiv(document.getElementById("blockDivIcv"));
                this.brokenRecordList = [];
                resources.forEach(r => {
                    
                    let labelAttr: string = r.getAdditionalProperty("label");
                    let labelRes: ARTLiteral = Deserializer.createLiteral(labelAttr);

                    //look for an existing entry for the same resource-label pair
                    let entry = this.brokenRecordList.find(e => e.resource.equals(r) && labelRes.equals(e.label));
                    if (!entry) { //add the entry only if not yet in
                        entry = { resource: r, label: labelRes };
                        this.brokenRecordList.push(entry);
                    }

                    //if xlabel attr is present, create and add also the resource for the xLabel
                    let xlabelAttr = r.getAdditionalProperty("xlabel");
                    if (xlabelAttr) {
                        let xLabelRes: ARTURIResource = new ARTURIResource(xlabelAttr, null, RDFResourceRolesEnum.xLabel);
                        if (entry.xlabels == null) {
                            entry.xlabels = [];
                        }
                        entry.xlabels.push(xLabelRes);
                    }
                });
                this.initPaging(this.brokenRecordList);
            }
        );
    
    }

}

interface BrokendRecord {
    resource: ARTResource; //resource with overlapped labels
    label: ARTLiteral; //literal label
    xlabels?: ARTResource[]; //(only in SKOSXL) list of the xlabel resources
}