import { Component } from "@angular/core";
import { ARTURIResource } from "../../models/ARTResources";
import { IcvServices } from "../../services/icv.service";
import { SkosServices } from "../../services/skos.service";
import { UIUtils } from "../../utils/UIUtils";
import { SharedModalServices } from "../../modal-dialogs/shared-modals/shared-modals.service";

@Component({
    selector: "top-concept-with-broader",
    templateUrl: "./top-concept-with-broader.component.html",
    host: { class: "pageComponent" },
    standalone: false
})
export class TopConceptWithBroaderComponent {

    brokenRecordList: {concept: ARTURIResource, scheme: ARTURIResource}[];

    constructor(private icvService: IcvServices, private skosService: SkosServices, private sharedModals: SharedModalServices) { }

    /**
     * Run the check
     */
    runIcv() {
        UIUtils.startLoadingDiv(document.getElementById("blockDivIcv"));
        this.icvService.listTopConceptsWithBroader().subscribe(
            records => {
                this.brokenRecordList = records;
                UIUtils.stopLoadingDiv(document.getElementById("blockDivIcv"));
            }
        );
    }

    removeBroaders(record: any) {
        let concept = record.concept;
        let scheme = record.scheme;
        this.icvService.removeBroadersToConcept(concept, scheme).subscribe(
            () => {
                this.runIcv();
            }
        );
    }

    removeAsTopConceptOf(record: any) {
        let concept = record.concept;
        let scheme = record.scheme;
        this.skosService.removeTopConcept(concept, scheme).subscribe(
            () => {
                this.runIcv();
            }
        );
    }

    removeBroadersToAll() {
        this.icvService.removeBroadersToAllConcepts().subscribe(
            () => {
                this.runIcv();
            }
        );
    }

    removeAllAsTopConceptOf() {
        this.icvService.removeAllAsTopConceptsWithBroader().subscribe(
            () => {
                this.runIcv();
            }
        );
    }

    onResourceClick(res: ARTURIResource) {
        this.sharedModals.openResourceView(res, false);
    }

}