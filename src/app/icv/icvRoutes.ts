import { RouterModule, Routes } from '@angular/router';
import { authGuard, projectGuard } from "../utils/CanActivateGuards";
import { BrokenAlignmentComponent } from "./broken-alignment/broken-alignment.component";
import { BrokenDefinitionComponent } from "./brokenDefinition/brokenDefinitionComponent";
import { ConflictualLabelComponent } from "./conflictual-label/conflictual-label.component";
import { CyclicConceptComponent } from "./cyclicConcept/cyclicConceptComponent";
import { DanglingConceptComponent } from "./dangling-concept/dangling-concept.component";
import { DanglingXLabelComponent } from "./dangling-xlabel/dangling-xlabel.component";
import { DisjointExactMatchConceptComponent } from "./disjointExactMatchConcept/disjointExactMatchConceptComponent";
import { DisjointRelatedConceptComponent } from "./disjointRelatedConcept/disjointRelatedConceptComponent";
import { ExtraSpaceLabelComponent } from "./extraSpaceLabel/extraSpaceLabelComponent";
import { HierarchicalRedundancyComponent } from "./hierarchicalRedundancy/hierarchicalRedundancyComponent";
import { IcvListComponent } from "./icvListComponent";
import { InvalidUriComponent } from "./invalidUri/invalidUriComponent";
import { MultiplePrefLabelComponent } from "./multiplePrefLabel/multiplePrefLabelComponent";
import { NoLabelResourceComponent } from "./no-label-resource/no-label-resource.component";
import { NoSchemeConceptComponent } from "./no-scheme-concept/no-scheme-concept.component";
import { NoDefinitionResourceComponent } from "./noDefinitionResource/noDefinitionResourceComponent";
import { NoLangLabelComponent } from "./noLangLabel/noLangLabelComponent";
import { NoMandatoryLabelComponent } from "./noMandatoryLabel/noMandatoryLabelComponent";
import { NoTopConceptSchemeComponent } from "./noTopConceptScheme/no-top-concept-scheme.component";
import { OnlyAltLabelResourceComponent } from "./onlyAltLabelResource/onlyAltLabelResourceComponent";
import { OverlappedLabelComponent } from "./overlappedLabel/overlapped-label.component";
import { OwlConsistencyViolationsComponent } from './owlConsistencyViolations/owlConsistencyViolationsComponent';
import { TopConceptWithBroaderComponent } from "./top-concept-with-broader/top-concept-with-broader.component";

export const routes: Routes = [
  {
    path: "", component: IcvListComponent, canActivate: [authGuard, projectGuard], children: [
      { path: "DanglingConcept", component: DanglingConceptComponent, canActivate: [authGuard, projectGuard] },
      { path: "NoSchemeConcept", component: NoSchemeConceptComponent, canActivate: [authGuard, projectGuard] },
      { path: "NoTopConceptScheme", component: NoTopConceptSchemeComponent, canActivate: [authGuard, projectGuard] },
      { path: "TopConceptWithBroader", component: TopConceptWithBroaderComponent, canActivate: [authGuard, projectGuard] },
      { path: "DisjointRelatedConcept", component: DisjointRelatedConceptComponent, canActivate: [authGuard, projectGuard] },
      { path: "DisjointExactMatchConcept", component: DisjointExactMatchConceptComponent, canActivate: [authGuard, projectGuard] },
      { path: "HierarchicalRedundancy", component: HierarchicalRedundancyComponent, canActivate: [authGuard, projectGuard] },
      { path: "HierarchicalCycle", component: CyclicConceptComponent, canActivate: [authGuard, projectGuard] },
      { path: "NoLabelResource", component: NoLabelResourceComponent, canActivate: [authGuard, projectGuard] },
      { path: "OnlyAltLabelResource", component: OnlyAltLabelResourceComponent, canActivate: [authGuard, projectGuard] },
      { path: "OverlappedLabelResource", component: OverlappedLabelComponent, canActivate: [authGuard, projectGuard] },
      { path: "OwlViolations", component: OwlConsistencyViolationsComponent, canActivate: [authGuard, projectGuard] },
      { path: "ConflictualLabelResource", component: ConflictualLabelComponent, canActivate: [authGuard, projectGuard] },
      { path: "NoLangLabelResource", component: NoLangLabelComponent, canActivate: [authGuard, projectGuard] },
      { path: "ExtraSpaceLabelResource", component: ExtraSpaceLabelComponent, canActivate: [authGuard, projectGuard] },
      { path: "NoMandatoryLabelResource", component: NoMandatoryLabelComponent, canActivate: [authGuard, projectGuard] },
      { path: "MutliplePrefLabelResource", component: MultiplePrefLabelComponent, canActivate: [authGuard, projectGuard] },
      { path: "DanglingXLabel", component: DanglingXLabelComponent, canActivate: [authGuard, projectGuard] },
      { path: "NoDefinitionResource", component: NoDefinitionResourceComponent, canActivate: [authGuard, projectGuard] },
      { path: "BrokenAlignment", component: BrokenAlignmentComponent, canActivate: [authGuard, projectGuard] },
      { path: "BrokenDefinition", component: BrokenDefinitionComponent, canActivate: [authGuard, projectGuard] },
      { path: "InvalidURI", component: InvalidUriComponent, canActivate: [authGuard, projectGuard] }
    ]
  },
];

export const icvRouting = RouterModule.forChild(routes);