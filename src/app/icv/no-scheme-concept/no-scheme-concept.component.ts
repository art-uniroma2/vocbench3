import { Component } from "@angular/core";
import { forkJoin } from 'rxjs';
import { ARTURIResource } from "../../models/ARTResources";
import { IcvServices } from "../../services/icv.service";
import { SkosServices } from "../../services/skos.service";
import { UIUtils } from "../../utils/UIUtils";
import { BrowsingModalServices } from "../../modal-dialogs/browsing-modals/browsing-modals.service";
import { SharedModalServices } from "../../modal-dialogs/shared-modals/shared-modals.service";

@Component({
  selector: "no-scheme-concept",
  templateUrl: "./no-scheme-concept.component.html",
  host: { class: "pageComponent" },
  standalone: false
})
export class NoSchemeConceptComponent {

  brokenConceptList: ARTURIResource[];

  hideAlert: boolean;

  constructor(private icvService: IcvServices, private skosService: SkosServices, private browsingModals: BrowsingModalServices,
    private sharedModals: SharedModalServices) { }

  /**
   * Run the check
   */
  runIcv() {
    UIUtils.startLoadingDiv(document.getElementById("blockDivIcv"));
    this.icvService.listConceptsWithNoScheme().subscribe(
      concepts => {
        this.brokenConceptList = concepts;
        UIUtils.stopLoadingDiv(document.getElementById("blockDivIcv"));
      }
    );
  }

  /**
   * Fixes concept by adding it to a scheme 
   */
  addToScheme(concept: ARTURIResource) {
    this.browsingModals.browseSchemeList({ key: "DATA.ACTIONS.SELECT_SCHEME" }).then(
      (scheme: any) => {
        this.skosService.addConceptToScheme(concept, scheme).subscribe(
          () => {
            this.runIcv();
          }
        );
      },
      () => { }
    );
  }

  /**
   * Fixes concepts by adding them all to a scheme
   */
  addAllToScheme() {
    this.browsingModals.browseSchemeList({ key: "DATA.ACTIONS.SELECT_SCHEME" }).then(
      (scheme: any) => {
        this.icvService.addAllConceptsToScheme(scheme).subscribe(
          () => {
            this.runIcv();
          }
        );
      },
      () => { }
    );
  }

  /**
   * Fixes concept by deleting it 
   */
  deleteConcept(concept: ARTURIResource) {
    this.skosService.deleteConcept(concept).subscribe(
      () => {
        this.runIcv();
      }
    );
  }

  /**
   * Fixes concepts by deleting them all 
   */
  deleteAllConcept() {
    let deleteConcFnArray: any[] = [];
    deleteConcFnArray = this.brokenConceptList.map((conc) => this.skosService.deleteConcept(conc));
    //call the collected functions and subscribe when all are completed
    forkJoin(deleteConcFnArray).subscribe(
      () => {
        this.runIcv();
      }
    );
  }

  onResourceClick(res: ARTURIResource) {
    this.sharedModals.openResourceView(res, false);
  }

}