import { Component } from "@angular/core";
import { SharedModalServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { ARTResource, ARTURIResource } from "../../models/ARTResources";
import { SKOSXL } from "../../models/Vocabulary";
import { ClassesServices } from "../../services/classes.service";
import { IcvServices } from "../../services/icv.service";
import { UIUtils } from "../../utils/UIUtils";
import { VBContext } from "../../utils/VBContext";
import { BrowsingModalServices } from "../../modal-dialogs/browsing-modals/browsing-modals.service";
import { finalize } from "rxjs";

@Component({
    selector: "dangling-xlabel",
    templateUrl: "./dangling-xlabel.component.html",
    host: { class: "pageComponent" },
    standalone: false
})
export class DanglingXLabelComponent {

    brokenLabelList: ARTResource[];

    constructor(private icvService: IcvServices, private classesService: ClassesServices,
        private browsingModals: BrowsingModalServices, private sharedModals: SharedModalServices) { }

    /**
     * Run the check
     */
    runIcv() {
        UIUtils.startLoadingDiv(document.getElementById("blockDivIcv"));
        this.icvService.listDanglingXLabels().pipe(
          finalize(() => {
            UIUtils.stopLoadingDiv(document.getElementById("blockDivIcv"));
          })
        ).subscribe(
            labels => {
                this.brokenLabelList = labels;
            }
        );
    }

    /**
     * Deletes the given xlabel
     */
    deleteLabel(xlabel: ARTResource) {
        this.classesService.deleteInstance(xlabel, SKOSXL.label).subscribe(
            () => {
                this.runIcv();
            }
        );
    }

    /**
     * Ask the user to choose the relation whith which link the xlabel, then ask for the concept to which
     * set the dangling xlabel.
     */
    assignLabel(xlabel: ARTResource) {
        //as pref alt or hidden?
        let predOpts = [SKOSXL.prefLabel, SKOSXL.altLabel, SKOSXL.hiddenLabel];
        this.sharedModals.selectResource({ key: "DATA.ACTIONS.SELECT_LEXICALIZATION_PROPERTY" }, null, predOpts).then(
            (selectedPred: ARTURIResource[]) => {
                let activeSchemes: ARTURIResource[] = VBContext.getWorkingProjectCtx().getProjectPreferences().activeSchemes;
                this.browsingModals.browseConceptTree({ key: "DATA.ACTIONS.SELECT_CONCEPT" }, activeSchemes, true).then(
                    (concept: any) => {
                        this.icvService.setDanglingXLabel(concept, selectedPred[0], xlabel).subscribe(
                            () => {
                                this.runIcv();
                            }
                        );
                    },
                    () => { }
                );
            },
            () => { }
        );
    }

    /**
     * Quick fix. Deletes all dangling xLabel.
     */
    deleteAllLabel() {
        this.icvService.deleteAllDanglingXLabel().subscribe(
            () => {
                this.runIcv();
            }
        );
    }

}