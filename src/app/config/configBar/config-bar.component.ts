import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import FileSaver from 'file-saver';
import { forkJoin } from 'rxjs';
import { defaultIfEmpty } from 'rxjs/operators';
import { ARTResource, ARTURIResource } from "src/app/models/ARTResources";
import { OntoLex, SKOS } from 'src/app/models/Vocabulary';
import { ExportServices } from "src/app/services/export.service";
import { ShaclBatchValidationModalComponent } from "src/app/shacl/shacl-batch-validation-modal.component";
import { UndoHandler } from "src/app/undo/undoHandler";
import { Cookie } from 'src/app/utils/Cookie';
import { HttpManager } from "src/app/utils/HttpManager";
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { ToastService } from "src/app/widget/toast/toast.service";
import { VersionInfo } from "../../models/History";
import { Project, ProjectLabelCtx } from "../../models/Project";
import { AdministrationServices } from "../../services/administration.service";
import { InputOutputServices } from "../../services/input-output.service";
import { ShaclServices } from "../../services/shacl.service";
import { LoadShapesModalComponent } from "../../shacl/load-shapes-modal.component";
import { AuthorizationEvaluator } from "../../utils/AuthorizationEvaluator";
import { UIUtils } from "../../utils/UIUtils";
import { VBActionsEnum } from "../../utils/VBActions";
import { VBContext } from "../../utils/VBContext";
import { VBProperties } from "../../utils/VBProperties";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { SharedModalServices } from "../../modal-dialogs/shared-modals/shared-modals.service";
import { ChangeWGraphModalComponent } from "./change-wgraph-modal.component";

@Component({
  selector: "config-bar",
  templateUrl: "./config-bar.component.html",
  standalone: false
})
export class ConfigBarComponent {

  currentProject: Project;

  privacyStatementAvailable: boolean = false;
  shaclEnabled: boolean = false;

  translateLangs: string[];
  translateLang: string;

  loadDataAuthorized: boolean;
  exportDataAuthorized: boolean;
  clearDataAuthorized: boolean;
  versioningAuthorized: boolean;
  wgraphAuthorized: boolean;
  loadShapesAuthorized: boolean;
  exportShapesAuthorized: boolean;
  clearShapesAuthorized: boolean;
  shaclBatchValidationAuthorized: boolean;

  constructor(private exportServices: ExportServices, private inOutService: InputOutputServices, private administrationService: AdministrationServices,
    private shaclService: ShaclServices, private vbProp: VBProperties, private undoHandler: UndoHandler,
    private basicModals: BasicModalServices, private sharedModals: SharedModalServices, private modalService: NgbModal,
    private toastService: ToastService, private translate: TranslateService, private route: Router) {
  }

  ngOnInit() {
    this.translateLangs = this.translate.getLangs();
    this.translateLang = this.translate.currentLang;
  }

  /**
   * returns true if a project is open. Useful to enable/disable navbar links
   */
  isProjectAccessed(): boolean {
    this.currentProject = VBContext.getWorkingProject();
    return VBContext.getWorkingProject() != undefined;
  }

  /**
   * Returns true if the user is logged (an authentication token is stored).
   */
  isUserLogged(): boolean {
    return VBContext.isLoggedIn();
  }

  undo() {
    this.undoHandler.handle();
  }

  /* ===============================
   * Project selection
   * =============================== */

  /**
   * Returns the current version of the project
   */
  getCtxVersion(): VersionInfo {
    return VBContext.getContextVersion();
  }

  /**
   * Returns the current write graph of the project 
   */
  getCtxWGraph(): ARTResource {
    return VBContext.getContextWGraph();
  }

  /**
   * Opens a modal that allows to change project among the open
   */
  changeProject() {
    this.sharedModals.changeProject();
  }

  /* ===============================
   * About menu
   * =============================== */

  /**
   * Initializes privacyStatementAvailable. This methods is invoked each time the "About VocBench" menu is open.
   * This is necessary since if privacyStatementAvailable is initialized in ngOnInit(), the system setting migth still not retrieved
   * (in AppComponent.ngOnInit())
   */
  onAboutMenuOpen() {
    this.privacyStatementAvailable = this.vbProp.isPrivacyStatementAvailable();
  }

  downloadPrivacyStatement() {
    this.administrationService.downloadPrivacyStatement().subscribe();
  }

  copyWebApiUrl() {
    let baseUrl = HttpManager.getServerHost() + "/" + HttpManager.serverpath + "/" + HttpManager.groupId + "/" + HttpManager.artifactId + "/";
    navigator.clipboard.writeText(baseUrl).then(
      () => {
        this.toastService.show(null, { key: "APP.TOP_BAR.ABOUT_MENU.WEB_API_COPIED" });
      },
      () => { }
    );
  }

  /* ===============================
   * Global data management
   * =============================== */

  /**
   * Invoke the initialization of the authorizations of the global data management menu items.
   * This is performed each time the menu is open in order to prevent repeated checks even when not necessary
   */
  onGlobalDataManagementMenuOpen() {
    this.initAuth();
    this.shaclEnabled = VBContext.getWorkingProject().isShaclEnabled();
  }

  clearData() {
    this.basicModals.confirm({ key: "COMMONS.ACTIONS.CLEAR_DATA" }, { key: "MESSAGES.CLEAR_DATA_CONFIRM" }, ModalType.warning).then(
      () => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.inOutService.clearData().subscribe(
          () => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            this.basicModals.alert({ key: "COMMONS.ACTIONS.CLEAR_DATA" }, { key: "MESSAGES.DATA_CLEARED" });

            //according the model, reset schemes and/or lexicon in order to prevent error when re-init the concept-tree/lexEntry-list
            let clearStoredInfoFn = [];
            if (this.currentProject.getModelType() == SKOS.uri || this.currentProject.getModelType() == OntoLex.uri) {
              clearStoredInfoFn.push(this.vbProp.setActiveSchemes(VBContext.getWorkingProjectCtx(), []));
            }
            if (this.currentProject.getModelType() == OntoLex.uri) {
              clearStoredInfoFn.push(this.vbProp.setActiveLexicon(VBContext.getWorkingProjectCtx(), null));
            }

            forkJoin(clearStoredInfoFn)
              .pipe(defaultIfEmpty(null)) //needed since if clearStoredInfoFn is empty (namely no action after clear data), the handler of subscribe is not executed
              .subscribe(
                () => {
                  //simulate the project change in order to force the destroy of all the Route
                  VBContext.setProjectChanged(true);
                  //redirect to the home in order to prevent any kind of error related to not existing resource
                  this.route.navigate(["/Home"]);
                }
              );
          }
        );
      },
      () => { }
    );
  }

  changeWGraph() {
    const modalRef: NgbModalRef = this.modalService.open(ChangeWGraphModalComponent, new ModalOptions());
    modalRef.result.then(
      (graph: ARTURIResource) => {
        if (VBContext.getWorkingProject()?.getBaseURI() == graph.getNominalValue()) {
          graph = null;
        }
        VBContext.setContextWGraph(graph);
        VBContext.setProjectChanged(true); //changing wgraph is equivalent to changing projectv
        //redirect to the home in order to reset views that were based on the old wgraph
        this.route.navigate(["/Home"]);
      },
      () => { }
    );
  }

  loadShaclShapes() {
    this.modalService.open(LoadShapesModalComponent, new ModalOptions());
  }

  exportShaclShapes() {
    this.shaclService.exportShapes().subscribe(
      blob => {
        FileSaver.saveAs(blob, "shapes.ttl");
      }
    );
  }

  clearShaclShapes() {
    this.basicModals.confirm({ key: "SHACL.CLEAR_SHACL_SHAPES" }, "This operation will delete all the SHACL shapes stored in the project. Are you sure to proceed?",
      ModalType.warning).then(
        () => {
          UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
          this.shaclService.clearShapes().subscribe(
            () => {
              UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
              this.basicModals.alert({ key: "SHACL.CLEAR_SHACL_SHAPES" }, { key: "MESSAGES.SHACL_SHAPES_CLEARED" });
            }
          );
        },
        () => { }
      );
  }

  batchShaclValidation() {
    this.modalService.open(ShaclBatchValidationModalComponent, new ModalOptions());
  }

  onTranslateLangChanged() {
    this.translate.use(this.translateLang);
    Cookie.setTranslationLangCookie(this.translateLang, { path: "/" });
    ProjectLabelCtx.language = this.translateLang;
  }

  /* ===============================
   * AUTH
   * =============================== */

  /**
   * Initializes the authorizations of the global data management menu items
   */
  private initAuth() {
    this.loadDataAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.inputOutputLoadData);
    this.exportDataAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.exportExport);
    this.clearDataAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.inputOutputClearData);
    this.versioningAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.versionsGetVersions);
    this.wgraphAuthorized = AuthorizationEvaluator.isGaolAuthorized('auth(rdf(graph), "U").'); //goal written directly since this has no corresponding action, just changes wgraph in VBContext
    this.loadShapesAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.shaclLoadShapes);
    this.exportShapesAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.shaclExportShapes);
    this.clearShapesAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.shaclClearShapes);
    this.shaclBatchValidationAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.shaclBatchValidation);
  }

}