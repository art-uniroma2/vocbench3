import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTURIResource } from "src/app/models/ARTResources";
import { ExportServices } from "src/app/services/export.service";
import { ResourceUtils } from "src/app/utils/ResourceUtils";
import { VBContext } from "src/app/utils/VBContext";
import { BasicModalServices } from "src/app/modal-dialogs/basic-modals/basic-modals.service";
import { ModalType } from "src/app/modal-dialogs/Modals";

@Component({
  selector: "change-wgraph-modal",
  templateUrl: "./change-wgraph-modal.component.html",
  standalone: false
})
export class ChangeWGraphModalComponent {

  graphs: ARTURIResource[];
  selectedGraph: ARTURIResource;

  baseUri: string;

  customGraph: string;
  useCustomGraph: boolean;

  constructor(
    public activeModal: NgbActiveModal,
    private basicModals: BasicModalServices,
    private exportServices: ExportServices,
  ) { }

  ngOnInit() {
    this.baseUri = VBContext.getWorkingProject().getBaseURI();
    this.selectedGraph = VBContext.getContextWGraph();
    if (!this.selectedGraph) { //no working graph => the default one is the baseURI
      this.selectedGraph = new ARTURIResource(this.baseUri);
    }
    this.exportServices.getNamedGraphs().subscribe(
      graphs => {
        graphs.sort((a, b) => (a.getURI() === this.baseUri ? -1 : b.getURI() === this.baseUri ? 1 : 0));
        this.graphs = graphs;
      }
    );
  }

  selectGraph(graph: ARTURIResource) {
    if (graph.equals(this.selectedGraph)) {
      this.selectedGraph = null;
    } else {
      this.selectedGraph = graph;
    }
    this.useCustomGraph = false;
  }

  onDblClick(graph: ARTURIResource) {
    this.selectedGraph = graph;
    this.ok();
  }

  onUseCustomGraphChange() {
    if (this.useCustomGraph) {
      this.selectedGraph = null;
    }
  }

  isOkEnabled() {
    /*
    ok clickable if:
    - "use custom graph" checked AND a custom graph string has been provide
    - "use custom graph" not checked AND a named graph is selected
    */
    return (this.useCustomGraph && this.customGraph != null && this.customGraph.trim() != "") || (!this.useCustomGraph && this.selectedGraph);
  }

  ok() {
    if (this.useCustomGraph) {
      if (!ResourceUtils.testIRI(this.customGraph)) {
        this.basicModals.alert({ key: "STATUS.INVALID_VALUE" }, { key: "MESSAGES.INVALID_IRI", params: { iri: this.customGraph } }, ModalType.warning);
        return;
      }
    }
    this.activeModal.close(this.selectedGraph);
  }

  cancel() {
    this.activeModal.dismiss();
  }
}