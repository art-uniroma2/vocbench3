import { Component } from "@angular/core";
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { finalize, Subscription } from "rxjs";
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { Configuration } from "../../../models/Configuration";
import { Bucket, DatasetDescription, DatasetSearchFacets, DatasetSearchResult, DownloadDescription, FacetAggregation, SearchResultsPage, SelectionMode } from "../../../models/Metadata";
import { ConfigurableExtensionFactory, ExtensionFactory, ExtensionPointID, PluginSpecification } from "../../../models/Plugins";
import { DatasetCatalogsServices } from "../../../services/dataset-catalogs.service";
import { ExtensionsServices } from "../../../services/extensions.service";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { DataDumpSelectorModalComponent } from "./data-dump-selector-modal.component";

@Component({
  selector: "dataset-catalog-modal",
  templateUrl: "./datasetCatalogModal.html",
  styles: [`
    .searchDatasetResult { 
        margin-bottom: 3px;
        padding: 3px 6px;
        border: 1px solid #ddd;
        border-radius: 5px;
    }`
  ],
  standalone: false
})
export class DatasetCatalogModalComponent {

  extensions: ConfigurableExtensionFactory[];
  selectedExtension: ConfigurableExtensionFactory;
  private extensionConfig: Configuration;

  query: string;
  private lastQuery: string;
  lastSearchFacets: { [facetName: string]: { facetDisplayName?: string; items: { [itemName: string]: { itemDisplayName?: string } } } } = {};

  searchDatasetResult: SearchResultsPage<DatasetSearchResult>;
  selectedDataset: DatasetSearchResult;
  selectedDatasetDescription: DatasetDescription;

  page: number = 0;
  totPage: number;

  searching: boolean;
  describingDataset: boolean;

  private describeDatasetSubscription: Subscription;

  constructor(public activeModal: NgbActiveModal, private metadataRepositoryService: DatasetCatalogsServices,
    private extensionService: ExtensionsServices, private modalService: NgbModal, private basicModals: BasicModalServices) {
  }

  ngOnInit() {
    //retrieves the list of extPoints (LOV and LODCloud)
    this.extensionService.getExtensions(ExtensionPointID.DATASET_CATALOG_CONNECTOR_ID).subscribe(
      extensions => {
        this.extensions = extensions as ConfigurableExtensionFactory[];
      }
    );
  }

  ngOnDestroy() {
    this.describeDatasetSubscription?.unsubscribe();
  }

  onExtensionChange(selectedExtension: ExtensionFactory) {
    this.selectedExtension = selectedExtension as ConfigurableExtensionFactory;
    this.extensionConfig = null;
    this.clearResults();
  }

  onExtensionConfigUpdated(config: Configuration) {
    this.extensionConfig = config;
    this.clearResults();
  }

  private clearResults() {
    this.searchDatasetResult = null;
    this.selectedDataset = null;
    this.selectedDatasetDescription = null;
  }

  onKeydown(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.searchDataset();
    }
  }

  searchDataset() {
    if (!this.query || this.query.trim() == "") {
      return;
    }
    this.lastQuery = this.query;
    this.lastSearchFacets = {};
    this.page = 0;
    this.totPage = 0;
    this.executeSearchDataset();
  }

  private executeSearchDataset() {
    let connectorSpec = this.buildConnectorSpecification();
    if (!connectorSpec) return;

    let facets: DatasetSearchFacets = {};
    for (let facetName of Object.keys(this.lastSearchFacets)) {
      let items = Object.keys(this.lastSearchFacets[facetName].items);
      if (items.length != 0) {
        facets[facetName] = items;
      }
    }
    this.searching = true;
    this.metadataRepositoryService.searchDataset(connectorSpec, this.lastQuery, facets, this.page).pipe(
      finalize(() => { this.searching = false; })
    ).subscribe(
      (results: SearchResultsPage<DatasetSearchResult>) => {
        this.searchDatasetResult = results;
        this.selectedDatasetDescription = null;
        this.totPage = Math.floor(this.searchDatasetResult.totalResults / this.searchDatasetResult.pageSize);
        if (this.searchDatasetResult.totalResults % this.searchDatasetResult.pageSize != 0) {
          this.totPage++;
        }
      }
    );
  }

  selectDataset(dataset: DatasetSearchResult) {
    let connectorSpec = this.buildConnectorSpecification();
    if (!connectorSpec) return;

    this.selectedDataset = dataset;

    this.describeDatasetSubscription?.unsubscribe();

    this.describingDataset = true;
    this.describeDatasetSubscription = this.metadataRepositoryService.describeDataset(connectorSpec, this.selectedDataset.id).pipe(
      finalize(() => {
        this.describingDataset = false;
      })
    ).subscribe(
      datasetDescription => {
        this.selectedDatasetDescription = datasetDescription;
      }
    );
  }

  requireConfigurationConnector() {
    if (this.extensionConfig != null) {
      return this.extensionConfig.requireConfiguration();
    }
    return false;
  }

  private buildConnectorSpecification(): PluginSpecification {
    let connectorSpec: PluginSpecification = { factoryId: this.selectedExtension.id };
    if (this.extensionConfig != null) {
      if (this.requireConfigurationConnector()) {
        this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "MESSAGES.CATALOG_CONNECTOR_NOT_CONFIGURED" }, ModalType.warning);
        return null;
      }
      if (this.extensionConfig != null) {
        connectorSpec.configType = this.extensionConfig.type;
        connectorSpec.configuration = this.extensionConfig.getPropertiesAsMap();
      }
    }
    return connectorSpec;
  }

  prevPage() {
    this.page--;
    this.executeSearchDataset();
  }
  nextPage() {
    this.page++;
    this.executeSearchDataset();
  }

  toggleFacet(facet: FacetAggregation, bucket: Bucket) {
    let searchFacet = this.lastSearchFacets[facet.name];
    if (!searchFacet) {
      searchFacet = {
        facetDisplayName: facet.displayName,
        items: {}
      };
      this.lastSearchFacets[facet.name] = searchFacet;
    }

    let selectionMode = this.searchDatasetResult.facetAggregations.find(agg => agg.name == facet.name).selectionMode;

    if (selectionMode == SelectionMode.disabled) return;

    if (selectionMode == SelectionMode.single) {
      Object.keys(searchFacet.items).forEach(element => {
        if (element != bucket.name) {
          delete searchFacet.items[element];
        }
      });
    }

    if (searchFacet.items[bucket.name]) {
      delete searchFacet.items[bucket.name];
    } else {
      searchFacet.items[bucket.name] = { itemDisplayName: bucket.displayName };
    }

    this.executeSearchDataset();
  }

  private selectDataDump(): Promise<DownloadDescription> {
    const modalRef: NgbModalRef = this.modalService.open(DataDumpSelectorModalComponent, new ModalOptions('lg'));
    modalRef.componentInstance.message = "The selected dataset catalog has multiple data dumps. Please select the one to use from this list.";
    modalRef.componentInstance.dataDumps = this.selectedDatasetDescription.dataDumps;
    return modalRef.result;
  }

  ok() {
    if (this.selectedDatasetDescription.dataDumps.length == 0) {
      this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "The selected dataset catalog has no data dump, so it cannot be used for retrieving data." }, ModalType.warning);
      return;
      // let returnData: DatasetCatalogModalReturnData = {
      //     connectorId: this.selectedExtension.id,
      //     dataset: this.selectedDatasetDescription,
      //     dataDump: null
      // };
      // this.activeModal.close(returnData);
    } else if (this.selectedDatasetDescription.dataDumps.length == 1) {
      let returnData: DatasetCatalogModalReturnData = {
        connectorId: this.selectedExtension.id,
        dataset: this.selectedDatasetDescription,
        dataDump: this.selectedDatasetDescription.dataDumps[0]
      };
      this.activeModal.close(returnData);
    } else { //multiple data dumps
      this.selectDataDump().then(
        dump => {
          let returnData: DatasetCatalogModalReturnData = {
            connectorId: this.selectedExtension.id,
            dataset: this.selectedDatasetDescription,
            dataDump: dump
          };
          this.activeModal.close(returnData);
        },
        () => { }
      );
    }
  }

  cancel() {
    this.activeModal.dismiss();
  }

}

export class DatasetCatalogModalReturnData {
  connectorId: string;
  dataset: DatasetDescription;
  dataDump: DownloadDescription;
}