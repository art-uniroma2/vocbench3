import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTURIResource } from "../../../../models/ARTResources";

@Component({
    selector: "filter-graphs-modal",
    templateUrl: "./filter-graphs-modal.component.html",
    standalone: false
})
export class FilterGraphsModalComponent {
    @Input() graphs: {checked: boolean, graph: ARTURIResource}[];


    constructor(public activeModal: NgbActiveModal) {}

    // graph panel handlers
    areAllGraphDeselected(): boolean {
        for (const g of this.graphs) {
            if (g.checked) {
                return false;
            }
        }
        return true;
    }


    ok() {
        this.activeModal.close();
    }

}