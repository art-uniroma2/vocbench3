import { Component } from "@angular/core";
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { SKOS, SKOSXL } from "../../../models/Vocabulary";
import { RefactorServices } from "../../../services/refactor.service";
import { AuthorizationEvaluator } from "../../../utils/AuthorizationEvaluator";
import { UIUtils } from "../../../utils/UIUtils";
import { VBActionsEnum } from "../../../utils/VBActions";
import { VBContext } from "../../../utils/VBContext";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { finalize } from "rxjs";

@Component({
  selector: "refactor",
  templateUrl: "./refactor.component.html",
  host: { class: "pageComponent" },
  standalone: false
})
export class RefactorComponent {

  private lexicalizationModel: string; //RDFS, SKOS, SKOS-XL

  reifyNotes: boolean = false; //used in skos->skoxl
  flattenNotes: boolean = false; //used in skosxl->skos

  constructor(private refactorService: RefactorServices, private basicModals: BasicModalServices) { }

  ngOnInit() {
    this.lexicalizationModel = VBContext.getWorkingProject().getLexicalizationModelType();
  }

  skosToSkosxlEnabled(): boolean {
    return this.lexicalizationModel == SKOSXL.uri;
  }

  skosxlToSkosEnabled(): boolean {
    return (this.lexicalizationModel == SKOSXL.uri || this.lexicalizationModel == SKOS.uri);
  }

  skosToSkosxl() {
    this.basicModals.confirm({ key: "DATA_MANAGEMENT.REFACTOR.SKOS_TO_SKOSXL" }, { key: "MESSAGES.LONG_PROCESS_WARN_CONFIRM" }, ModalType.warning).then(
      () => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.refactorService.SKOStoSKOSXL(this.reifyNotes).pipe(
          finalize(() => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
          })
        ).subscribe(
          () => {
            this.basicModals.alert({ key: "DATA_MANAGEMENT.REFACTOR.REFACTOR" }, { key: "MESSAGES.REFACTORING_COMPLETED" });
          }
        );
      },
      () => { }
    );
  }

  skosxlToSkos() {
    this.basicModals.confirm({ key: "DATA_MANAGEMENT.REFACTOR.SKOSXL_TO_SKOS" }, { key: "MESSAGES.LONG_PROCESS_WARN_CONFIRM" }, ModalType.warning).then(
      () => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.refactorService.SKOSXLtoSKOS(this.flattenNotes).pipe(
          finalize(() => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
          })
        ).subscribe(
          () => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            this.basicModals.alert({ key: "DATA_MANAGEMENT.REFACTOR.REFACTOR" }, { key: "MESSAGES.REFACTORING_COMPLETED" });
          }
        );
      },
      () => { }
    );
  }

  //TODO: some event in order to destroy the data component
  migrateData() {
    this.basicModals.confirm({ key: "COMMONS.ACTIONS.MIGRATE_DATA_TO_BASEURI_GRAPH" }, { key: "MESSAGES.LONG_PROCESS_WARN_CONFIRM" }, ModalType.warning).then(
      () => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.refactorService.migrateDefaultGraphToBaseURIGraph().subscribe(
          () => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            this.basicModals.alert({ key: "DATA_MANAGEMENT.REFACTOR.REFACTOR" }, { key: "MESSAGES.REFACTORING_COMPLETED" });
          }
        );
      },
      () => { }
    );
  }

  //Authorizations
  isMigrateAuthorized(): boolean {
    return AuthorizationEvaluator.isAuthorized(VBActionsEnum.refactorMigrateToBaseUriGraph);
  }
  isSkosToSkosxlAuthorized(): boolean {
    return AuthorizationEvaluator.isAuthorized(VBActionsEnum.refactorSkosToSkosxl);
  }
  isSkoxlToSkosAuthorized(): boolean {
    return AuthorizationEvaluator.isAuthorized(VBActionsEnum.refactorSkosxlToSkos);
  }

}