import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalStorageManager } from 'src/app/utils/LocalStorageManager';
import { VBContext } from "src/app/utils/VBContext";
import { VBProperties } from 'src/app/utils/VBProperties';
import { ProjectColumnId, ProjectTableColumnStruct, ProjectUtils, ProjectVisualization } from "../../models/Project";

@Component({
    selector: "proj-table-conf-modal",
    templateUrl: "./project-table-config-modal.component.html",
    standalone: false
})
export class ProjectTableConfigModalComponent {

    projectVisualization: ProjectVisualization;
    private projVisualizationChanged: boolean;

    isAdmin: boolean;

    columns: ProjectTableColumnStruct[] = [];
    selectedColumn: ProjectTableColumnStruct;

    constructor(public activeModal: NgbActiveModal, private vbProp: VBProperties) { }

    ngOnInit() {
        this.isAdmin = VBContext.getLoggedUser().isAdmin();

        this.projectVisualization = VBContext.getUserSettings().projectVisualization;

        if (this.isAdmin) {
            this.initColumnTable();
        }
    }

    onProjVisualizationChanged() {
        this.projVisualizationChanged = true;
    }

    private initColumnTable() {
        this.columns = ProjectUtils.getDefaultProjectTableColumns();
        let customOrder: ProjectColumnId[] = this.getCustomColumnsSetting(); //this setting contains the (ordered) comma separated IDs of the columns to show

        //update the show
        this.columns.forEach(defCol => {
            if (customOrder.indexOf(defCol.id) == -1) { //the column is not present in the custom order (hidden)
                defCol.show = false;
            }
        });

        //sort the columns according the custom order
        this.columns.sort((c1, c2) => {
            let idx1 = customOrder.indexOf(c1.id);
            if (idx1 == -1) idx1 = 99;
            let idx2 = customOrder.indexOf(c2.id);
            if (idx2 == -1) idx2 = 99;
            if (idx1 > idx2) return 1;
            else if (idx1 < idx2) return -1;
            else return 0;
        });
    }

    private getCustomColumnsSetting(): ProjectColumnId[] {
        let columnOrder: ProjectColumnId[] = ProjectUtils.defaultTableOrder;
        let colOrderCookie = LocalStorageManager.getItem(LocalStorageManager.PROJECT_TABLE_ORDER); //this cookie contains the (ordered) comma separated IDs of the columns to show
        if (colOrderCookie != null) {
            columnOrder = colOrderCookie.split(",") as ProjectColumnId[];
        }
        return columnOrder;
    }

    selectColumn(col: ProjectTableColumnStruct) {
        if (this.selectedColumn == col) {
            this.selectedColumn = null;
        } else {
            this.selectedColumn = col;
        }
    }

    moveUp() {
        let idx = this.columns.indexOf(this.selectedColumn);
        this.columns.splice(idx - 1, 0, this.columns.splice(idx, 1)[0]);
    }

    moveDown() {
        let idx = this.columns.indexOf(this.selectedColumn);
        this.columns.splice(idx + 1, 0, this.columns.splice(idx, 1)[0]);
    }

    reset() {
        LocalStorageManager.deleteItem(LocalStorageManager.PROJECT_TABLE_ORDER);
        this.initColumnTable();
    }

    ok() {
        if (this.projVisualizationChanged) {
            this.vbProp.setProjectVisualization(this.projectVisualization).subscribe();
        }

        let oldColumnsCookie = LocalStorageManager.getItem(LocalStorageManager.PROJECT_TABLE_ORDER);
        let newColumnCookie = this.columns.filter(c => c.show).map(c => c.id).join(",");
        if (this.isAdmin && oldColumnsCookie != newColumnCookie) {
            LocalStorageManager.setItem(LocalStorageManager.PROJECT_TABLE_ORDER, newColumnCookie);
        }

        if (this.projVisualizationChanged || oldColumnsCookie != newColumnCookie) { 
            //close if something changed
            this.activeModal.close();
        } else { //if nothing changed, simply dismiss the modal
            this.activeModal.dismiss();
        }
    }

}