import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Project } from "src/app/models/Project";
import { ProjectServices } from "src/app/services/projects.service";
import { VBContext } from 'src/app/utils/VBContext';
import { VBProperties } from 'src/app/utils/VBProperties';

@Component({
  selector: "project-list-panel",
  templateUrl: "./project-list-panel.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class ProjectListPanelComponent {

  @Input() consumer: Project;
  @Input() onlyOpen: boolean;

  @Output() projectSelected: EventEmitter<Project> = new EventEmitter<Project>();

  projects: Project[];
  selectedProject: Project;

  rendering: boolean;

  nameFilter: string = "";

  constructor(private projectService: ProjectServices, private vbProp: VBProperties) { }

  ngOnInit() {
    this.rendering = VBContext.getUserSettings().projectRendering;

    this.projectService.listProjects(this.consumer, this.onlyOpen).subscribe(
      projects => {
        this.projects = projects;
      }
    );
  }

  switchRendering() {
    this.rendering = !this.rendering;
    this.vbProp.setProjectRendering(this.rendering).subscribe();
  }

  selectProject(project: Project) {
    this.selectedProject = project;
    this.projectSelected.emit(this.selectedProject);
  }

  isProjectVisible(project: Project): boolean {
    return this.nameFilter == "" || project.getName(this.rendering).toLocaleLowerCase().includes(this.nameFilter.toLocaleLowerCase());
  }

}
