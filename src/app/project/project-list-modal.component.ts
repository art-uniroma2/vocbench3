import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from "@ngx-translate/core";
import { finalize } from 'rxjs/operators';
import { Project, ProjectViewMode } from '../models/Project';
import { MetadataServices } from "../services/metadata.service";
import { ProjectServices } from "../services/projects.service";
import { UserServices } from "../services/user.service";
import { DatatypeValidator } from "../utils/DatatypeValidator";
import { VBCollaboration } from '../utils/VBCollaboration';
import { VBContext } from "../utils/VBContext";
import { VBProperties } from '../utils/VBProperties';
import { AbstractProjectComponent } from "./abstractProjectComponent";

@Component({
  selector: "project-list-modal",
  templateUrl: "./project-list-modal.component.html",
  standalone: false
})
export class ProjectListModalComponent extends AbstractProjectComponent {

  selectedProject: Project;

  accessing: boolean;
  isSuperUser: boolean;

  constructor(projectService: ProjectServices, userService: UserServices, metadataService: MetadataServices,
    vbCollaboration: VBCollaboration, vbProp: VBProperties, dtValidator: DatatypeValidator, modalService: NgbModal,
    translateService: TranslateService, private activeModal: NgbActiveModal, private router: Router) {
    super(projectService, userService, metadataService, vbCollaboration, vbProp, dtValidator, modalService, translateService);
  }

  ngOnInit() {
    super.ngOnInit();
    this.isSuperUser = VBContext.getLoggedUser().isSuperUser();
  }

  getListProjectsFn() {
    return this.projectService.listProjects(null, true);
  }

  getRetrieveProjectsBagsFn(bagOfFacet: string) {
    return this.projectService.retrieveProjects(bagOfFacet, null, true);
  }

  selectProject(project: Project) {
    this.selectedProject = project;
  }

  changeVisualizationMode(mode: ProjectViewMode) {
    this.projectVisualization.mode = mode;
  }

  createProject() {
    this.router.navigate(["/Projects/CreateProject"]);
    this.cancel();
  }

  ok() {
    this.accessing = true;
    this.accessProject(this.selectedProject)
      .pipe(finalize(() => this.accessing = false))
      .subscribe(
        () => {
          this.activeModal.close();
          this.router.navigate(['/Home']);
        }
      );
  }

  cancel() {
    this.activeModal.dismiss();
  }

}