import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from "@ngx-translate/core";
import { from, Observable } from "rxjs";
import { finalize, map } from 'rxjs/operators';
import { Settings } from "../models/Plugins";
import { ExceptionDAO, Project, ProjectColumnId, ProjectTableColumnStruct, ProjectUtils, ProjectViewMode, ProjectVisibility, RemoteRepositorySummary, RepositorySummary } from '../models/Project';
import { MapleServices } from "../services/maple.service";
import { MetadataServices } from "../services/metadata.service";
import { ProjectServices } from "../services/projects.service";
import { RepositoriesServices } from "../services/repositories.service";
import { UserServices } from "../services/user.service";
import { DatatypeValidator } from "../utils/DatatypeValidator";
import { HttpServiceContext } from "../utils/HttpManager";
import { LocalStorageManager } from '../utils/LocalStorageManager';
import { UIUtils } from "../utils/UIUtils";
import { VBCollaboration } from '../utils/VBCollaboration';
import { VBContext } from '../utils/VBContext';
import { VBProperties } from '../utils/VBProperties';
import { BasicModalServices } from "../modal-dialogs/basic-modals/basic-modals.service";
import { ModalOptions, ModalType } from '../modal-dialogs/Modals';
import { LocalizedMap } from "../widget/localized-editor/localized-editor.component";
import { PluginSettingsHandler } from "../modal-dialogs/shared-modals/plugin-config-modal/plugin-config-modal.component";
import { SharedModalServices } from "../modal-dialogs/shared-modals/shared-modals.service";
import { ToastService } from "../widget/toast/toast.service";
import { AbstractProjectComponent } from "./abstractProjectComponent";
import { OpenAllProjReportModalComponent } from "./open-projects-report-modal.component";
import { ACLEditorModalComponent } from "./projectACL/acl-editor-modal.component";
import { ProjectACLModalComponent } from "./projectACL/project-acl-modal.component";
import { ProjectPropertiesModalComponent } from "./project-properties-modal.component";
import { ProjSettingsEditorModalComponent } from "./project-settings-editor/project-settings-editor-modal.component";
import { DeleteRemoteRepoModalComponent } from "./remoteRepositories/delete-remote-repo-modal.component";
import { DeleteRepositoryReportModalComponent } from "./remoteRepositories/delete-repository-report-modal.component";
import { RemoteRepoEditorModalComponent } from "./remoteRepositories/remote-repo-editor-modal.component";

@Component({
  selector: "project-component",
  templateUrl: "./project.component.html",
  host: { class: "pageComponent" },
  standalone: false
})
export class ProjectComponent extends AbstractProjectComponent {

  columnIDs: ProjectColumnId[] = [ProjectColumnId.accessed, ProjectColumnId.history, ProjectColumnId.lexicalization, ProjectColumnId.location, ProjectColumnId.model, ProjectColumnId.name, ProjectColumnId.open, ProjectColumnId.validation];
  columnOrder: { [id: string]: { show: string, flex: number, order: number } };

  constructor(projectService: ProjectServices, userService: UserServices, metadataService: MetadataServices,
    vbCollaboration: VBCollaboration, vbProp: VBProperties, dtValidator: DatatypeValidator, modalService: NgbModal, translateService: TranslateService,
    private repositoriesService: RepositoriesServices, private mapleService: MapleServices,
    private basicModals: BasicModalServices, private sharedModals: SharedModalServices, private toastService: ToastService, private router: Router) {
    super(projectService, userService, metadataService, vbCollaboration, vbProp, dtValidator, modalService, translateService);
  }

  //@Override the one in the abstract parent since it needs to initialize the column order first
  initProjects() {
    //init column order
    this.columnOrder = {};
    let columns: ProjectTableColumnStruct[] = ProjectUtils.getDefaultProjectTableColumns();
    let customOrder: ProjectColumnId[] = this.getCustomColumnsSetting(); //this setting contains the (ordered) IDs of the columns to show
    customOrder.forEach((colId: ProjectColumnId, idx: number) => {
      let colStruct: ProjectTableColumnStruct = columns.find(c => c.id == colId); //retrieve the column struct
      this.columnOrder[colId] = { show: colStruct.translationKey, order: idx, flex: colStruct.flex };
    });

    super.initProjects();
  }

  getListProjectsFn() {
    return this.projectService.listProjects();
  }

  getRetrieveProjectsBagsFn(bagOfFacet: string) {
    return this.projectService.retrieveProjects(bagOfFacet);
  }

  openOrCloseProject(project: Project) {
    if (project.isOpen()) {
      this.closeProject(project);
    } else {
      this.openProject(project);
    }
  }

  private openProject(project: Project) {
    UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
    this.projectService.accessProject(project)
      .pipe(finalize(() => UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen)))
      .subscribe({
        next: () => {
          project.setOpen(true);
          UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
          this.accessProject(project)
            .pipe(finalize(() => UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen)))
            .subscribe();
        },
        error: (err: Error) => {
          this.projectService.handleMissingChangetrackierSailError(err, this.basicModals);
        }
      });
  }

  openAll() {
    UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
    this.projectService.accessAllProjects()
      .pipe(finalize(() => UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen)))
      .subscribe(
        (report: { [key: string]: ExceptionDAO }) => {
          this.openAllRespHandler(report);
        }
      );
  }

  openAllStartup() {
    UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
    this.projectService.accessAllProjects(null, null, null, true)
      .pipe(finalize(() => UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen)))
      .subscribe(
        (report: { [key: string]: ExceptionDAO }) => {
          this.openAllRespHandler(report);
        }
      );
  }

  openAllRespHandler(report: { [key: string]: ExceptionDAO }) {
    if (Object.keys(report).length != 0) {
      const modalRef: NgbModalRef = this.modalService.open(OpenAllProjReportModalComponent, new ModalOptions('lg'));
      modalRef.componentInstance.report = report;
      modalRef.result.then(() => {
        this.initProjects();
      });
    } else {
      this.initProjects();
    }
  }

  /**
   * Calls the proper service in order to disconnect from the given project.
   */
  private closeProject(project: Project) {
    UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
    this.projectService.disconnectFromProject(project)
      .pipe(finalize(() => UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen)))
      .subscribe(
        () => {
          project.setOpen(false);
        }
      );
  }

  closeAll() {
    UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
    this.projectService.disconnectFromAllProjects()
      .pipe(finalize(() => UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen)))
      .subscribe(
        () => {
          this.initProjects();
        }
      );
  }

  activateProject(project: Project) {
    let workingProj = VBContext.getWorkingProject();
    if (workingProj == undefined || workingProj.getName() != project.getName()) {
      UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
      this.accessProject(project)
        .pipe(finalize(() => UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen)))
        .subscribe();
    }
  }

  /**
   * Redirects to the create project page
   */
  createProject() {
    this.router.navigate(["/Projects/CreateProject"]);
  }

  deleteProject(project: Project) {
    if (project.isOpen()) {
      this.basicModals.alert({ key: "COMMONS.ACTIONS.DELETE_PROJECT" }, { key: "MESSAGES.PROJECT_OPEN_CLOSE_AND_RETRY" }, ModalType.warning);
    } else {
      this.basicModals.confirm({ key: "COMMONS.ACTIONS.DELETE_PROJECT" }, { key: "MESSAGES.DELETE_PROJECT_CONFIRM", params: { project: project.getName() } }, ModalType.warning).then(
        () => {
          //retrieve the remote repositories referenced by the deleting project (this must be done before the deletion in order to prevent errors)
          this.projectService.getRepositories(project, true).subscribe(
            (repositories: RepositorySummary[]) => {
              this.deleteImpl(project).subscribe( //delete the project
                () => {
                  if (repositories.length > 0) { //if the deleted project was linked with remote repositories proceed with the deletion
                    this.deleteRemoteRepo(project, repositories);
                  }
                }
              );
            }
          );
        },
        () => { }
      );
    }
  }

  private deleteImpl(project: Project): Observable<void> {
    return this.projectService.deleteProject(project).pipe(
      map(() => {
        this.initProjects();
      })
    );
  }

  private deleteRemoteRepo(deletedProject: Project, repositories: RepositorySummary[]) {
    this.selectRemoteRepoToDelete(deletedProject, repositories).subscribe( //ask to the user which repo delete
      (deletingRepositories: RemoteRepositorySummary[]) => {
        if (deletingRepositories.length > 0) {
          UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
          this.repositoriesService.deleteRemoteRepositories(deletingRepositories).subscribe( //delete them
            (exceptions: ExceptionDAO[]) => {
              UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
              if (exceptions.some(e => e != null)) { //some deletion has failed => show the report
                const modalRef: NgbModalRef = this.modalService.open(DeleteRepositoryReportModalComponent, new ModalOptions('lg'));
                modalRef.componentInstance.deletingRepositories = deletingRepositories;
                modalRef.componentInstance.exceptions = exceptions;
              }
            }
          );
        }
      }
    );
  }

  private selectRemoteRepoToDelete(project: Project, repositories: RepositorySummary[]): Observable<RemoteRepositorySummary[]> {
    const modalRef: NgbModalRef = this.modalService.open(DeleteRemoteRepoModalComponent, new ModalOptions());
    modalRef.componentInstance.project = project;
    modalRef.componentInstance.repositories = repositories;
    return from(
      modalRef.result.then(
        repos => {
          return repos;
        }
      )
    );
  }

  /**
   * Opens a modal to show the properties of the selected project
   */
  openPropertyModal(project: Project) {
    const modalRef: NgbModalRef = this.modalService.open(ProjectPropertiesModalComponent, new ModalOptions('lg'));
    modalRef.componentInstance.project = project;
    return modalRef.result;
  }

  openACLModal() {
    let projCount: number = 0;
    if (this.projectVisualization.mode == ProjectViewMode.list) {
      projCount = this.projectList.length;
    } else {
      this.projectDirs.forEach(dir => { projCount += dir.projects.length; });
    }
    if (projCount > 50) {
      this.basicModals.confirm({ key: "PROJECTS.ACL.ACL_MATRIX" }, { key: "MESSAGES.ACL_TOO_MUCH_PROJ_CONFIRM", params: { projCount: projCount } }, ModalType.warning).then(
        () => { //confirmed
          this.openACLMatrix();
        },
        () => { }
      );
    } else {
      this.openACLMatrix();
    }
  }

  private openACLMatrix() {
    this.modalService.open(ProjectACLModalComponent, new ModalOptions('full'));
  }

  editACL(project: Project) {
    const modalRef: NgbModalRef = this.modalService.open(ACLEditorModalComponent, new ModalOptions('sm'));
    modalRef.componentInstance.project = project;
    return modalRef.result;
  }

  /** 
   * Opens a modal to edit the remote repositories credentials
   */
  editRemoteRepoCredential(project: Project) {
    if (project.isOpen()) {
      this.basicModals.alert({ key: "STATUS.OPERATION_DENIED" }, { key: "MESSAGES.CANNOT_EDIT_OPEN_PROJECT_CREDENTIALS" }, ModalType.warning);
      return;
    }
    const modalRef: NgbModalRef = this.modalService.open(RemoteRepoEditorModalComponent, new ModalOptions());
    modalRef.componentInstance.project = project;
  }

  editLabels(project: Project) {
    let projectLabels = project.getLabels();
    let localizeMap: LocalizedMap = new Map();
    for (let lang in projectLabels) {
      localizeMap.set(lang, projectLabels[lang]);
    }
    this.sharedModals.localizedEditor({ key: "PROJECTS.ACTIONS.EDIT_LABELS" }, localizeMap, true).then(
      (newLocalizedMap: LocalizedMap) => {
        let newProjLabels: { [lang: string]: string } = {};
        newLocalizedMap.forEach((label, lang) => {
          newProjLabels[lang] = label;
        });
        this.projectService.setProjectLabels(project, newProjLabels).subscribe(
          () => {
            project.setLabels(newProjLabels);
          }
        );
      },
      () => { }
    );
  }

  editDescription(project: Project) {
    this.basicModals.prompt({ key: "MODELS.PROJECT.DESCRIPTION" }, { value: "Description" }, null, project.getDescription(), true).then(
      descr => {
        if (descr.trim() == "") {
          descr = null;
        }
        this.projectService.setProjectProperty(project, "description", descr).subscribe(
          () => {
            project.setDescription(descr);
          }
        );
      },
      () => { }
    );
  }

  editFacets(project: Project) {
    this.sharedModals.configurePlugin(project.getFacets()).then(facets => {
      this.projectService.setProjectFacets(project, facets).subscribe(
        () => {
          project.setFacets(facets); //update facets in project
          if (this.projectVisualization.mode == ProjectViewMode.facet) {
            this.initProjects();
          }
        }
      );
    }, () => { });
  }

  editCustomFacetsSchema() {
    let handler: PluginSettingsHandler = (facets: Settings) => this.projectService.setCustomProjectFacetsSchema(facets);
    this.projectService.getCustomProjectFacetsSchema().subscribe(facetsSchema => {
      this.sharedModals.configurePlugin(facetsSchema, { key: "PROJECTS.CONFIG.FACETS_SCHEMA_DEFINITION" }, handler).then(
        () => { //changed settings
          this.initProjects();
        },
        () => { } //nothing changed
      );
    });
  }

  editSettings(project: Project) {
    const modalRef: NgbModalRef = this.modalService.open(ProjSettingsEditorModalComponent, new ModalOptions('lg'));
    modalRef.componentInstance.project = project;
  }

  profileProject(project: Project) {
    HttpServiceContext.setContextProject(project);
    this.mapleService.checkProjectMetadataAvailability().pipe(
      finalize(() => HttpServiceContext.removeContextProject())
    ).subscribe(
      available => {
        let msgKey = available ? "MESSAGES.PROFILE_PROJECT_REFRESH_CONFIRM" : "MESSAGES.PROFILE_PROJECT_CONFIRM";
        this.basicModals.confirm({ key: "COMMONS.ACTIONS.PROFILE_PROJECT" }, { key: msgKey, params: { project: project.getName() } }, ModalType.warning).then(
          () => {
            this.profileProjectImpl(project);
          },
          () => { }
        );
      }
    );
  }
  private profileProjectImpl(project: Project) {
    HttpServiceContext.setContextProject(project);
    this.mapleService.profileProject().pipe(
      finalize(() => HttpServiceContext.removeContextProject())
    ).subscribe(
      () => {
        this.toastService.show({ key: "STATUS.OPERATION_DONE" }, { key: "MESSAGES.PROFILE_PROJECT_COMPLETED", params: { project: project.getName() } });
      }
    );
  }


  toggleOpenAtStartup(project: Project) {
    project.setOpenAtStartup(!project.getOpenAtStartup());
    this.projectService.setOpenAtStartup(project, project.getOpenAtStartup()).subscribe();
  }

  toggleReadOnly(project: Project) {
    project.setReadOnly(!project.isReadOnly());
    this.projectService.setReadOnly(project, project.isReadOnly()).subscribe();
  }

  togglePubliclyAccessible(project: Project) {
    let visibility = project.getVisibility() == ProjectVisibility.PUBLIC ? ProjectVisibility.AUTHORIZED : ProjectVisibility.PUBLIC;
    project.setVisibility(visibility);
    this.projectService.setVisibility(project, visibility).subscribe();
  }
  /**
   * COOKIE MANAGEMENT
   */

  private getCustomColumnsSetting(): ProjectColumnId[] {
    let columnOrder: ProjectColumnId[] = ProjectUtils.defaultTableOrder;
    let colOrderCookie = LocalStorageManager.getItem(LocalStorageManager.PROJECT_TABLE_ORDER); //this cookie contains the (ordered) comma separated IDs of the columns to show
    if (colOrderCookie != null) {
      columnOrder = colOrderCookie.split(",") as ProjectColumnId[];
    }
    return columnOrder;
  }

}