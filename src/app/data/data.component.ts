import { ChangeDetectorRef, Component, ViewChild } from "@angular/core";
import { Subscription } from "rxjs";
import { ARTResource, ARTURIResource } from "../models/ARTResources";
import { ResourceViewTabsetComponent } from "../resource-view/resource-view-tabset/resource-view-tabset.component";
import { NodeSelectEvent } from "../structures/abstractNode";
import { TabsetPanelComponent } from "../structures/structure-tabset/structure-tabset.component";
import { VBContext } from "../utils/VBContext";
import { VBEventHandler } from "../utils/VBEventHandler";
import { ValueClickEvent } from "../models/ResourceView";

@Component({
  selector: "data",
  templateUrl: "./data.component.html",
  host: {
    class: "pageComponent",
  },
  standalone: false
})
export class DataComponent {

  @ViewChild(TabsetPanelComponent) treePanelChild: TabsetPanelComponent;

  @ViewChild("mainTabset") viewChildMainRvTabset: ResourceViewTabsetComponent;
  @ViewChild("sideTabset") viewChildSideRvTabset: ResourceViewTabsetComponent;

  selectedResource: ARTResource = null;
  sideResource: ARTResource = null;
  lastTabResource: ARTResource; //the resource of the last selected tab (main or side, no matter)

  showPromptAddress: boolean;
  address: string;

  private sync: boolean = false; //tells if ResView and tree/list need to be keep synced

  private eventSubscriptions: Subscription[] = [];

  constructor(private eventHandler: VBEventHandler, private cdRef: ChangeDetectorRef) {
    this.eventSubscriptions.push(this.eventHandler.resourceCreatedUndoneEvent.subscribe(
      (res: ARTURIResource) => this.onNodeDeleted(res)));
    this.eventSubscriptions.push(this.eventHandler.datatypeDeletedEvent.subscribe(
      (deletedRes: ARTURIResource) => this.onNodeDeleted(deletedRes)));
    this.eventSubscriptions.push(this.eventHandler.classDeletedEvent.subscribe(
      (deletedRes: ARTURIResource) => this.onNodeDeleted(deletedRes)));
    this.eventSubscriptions.push(this.eventHandler.collectionDeletedEvent.subscribe(
      (deletedRes: ARTURIResource) => this.onNodeDeleted(deletedRes)));
    this.eventSubscriptions.push(this.eventHandler.conceptDeletedEvent.subscribe(
      (deletedRes: ARTURIResource) => this.onNodeDeleted(deletedRes)));
    this.eventSubscriptions.push(this.eventHandler.instanceDeletedEvent.subscribe(
      (data: { instance: ARTURIResource, cls: ARTURIResource }) => this.onNodeDeleted(data.instance)));
    this.eventSubscriptions.push(this.eventHandler.lexicalEntryDeletedEvent.subscribe(
      (deletedRes: ARTURIResource) => this.onNodeDeleted(deletedRes)));
    this.eventSubscriptions.push(this.eventHandler.lexiconDeletedEvent.subscribe(
      (deletedRes: ARTURIResource) => this.onNodeDeleted(deletedRes)));
    this.eventSubscriptions.push(this.eventHandler.propertyDeletedEvent.subscribe(
      (deletedRes: ARTURIResource) => this.onNodeDeleted(deletedRes)));
    this.eventSubscriptions.push(this.eventHandler.schemeDeletedEvent.subscribe(
      (deletedRes: ARTURIResource) => this.onNodeDeleted(deletedRes)));
    this.eventSubscriptions.push(this.eventHandler.resViewTabSyncChangedEvent.subscribe(
      (sync: boolean) => { this.sync = sync; }
    ));
  }

  ngOnInit() {
    this.sync = VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPreferences.syncTabs;
  }

  ngOnDestroy() {
    this.eventSubscriptions.forEach(s => s.unsubscribe());
  }

  onValueClick(event: ValueClickEvent) {
    if (event.shift) {
      this.sideResource = event.value;
      this.cdRef.detectChanges();
      this.viewChildSideRvTabset.selectResource(this.sideResource, event.ctrl);
    }
  }

  onNodeSelected(event?: NodeSelectEvent) {
    if (event == null) return;

    /*
    this handles a corner case
    - sync is enabled, so each time a tab is activated, its resource needs to be synced in the tree/list
    - this causes a new NodeSelectedEvent to be fired from the tree/list when the synced resource is focused/selected
    - such event, which have no key modificator (shift/ctrl), causes the resource to be open in the main tab
    - if the tab originally selected belongs to the side tabset, it will result in having the same resource open in main and side tabset
    */
    if (this.sync && event.value.equals(this.lastTabResource)) return;

    //if shift+click and resource in the main tabset is already selected => open resource in side tabset
    if (event.shift && this.selectedResource != null) {
      this.sideResource = event.value;
      this.cdRef.detectChanges();
      this.viewChildSideRvTabset.selectResource(this.sideResource, event.ctrl);
    } else {
      this.selectedResource = event.value;
      this.cdRef.detectChanges();
      this.viewChildMainRvTabset.selectResource(this.selectedResource, event.ctrl);
    }
    
  }

  private onNodeDeleted(node: ARTResource) {
    if (!VBContext.getWorkingProject().isValidationEnabled()) {
      if (this.viewChildMainRvTabset != null) {
        this.viewChildMainRvTabset.deleteResource(node);
      }
      if (this.viewChildSideRvTabset != null) {
        this.viewChildSideRvTabset.deleteResource(node);
      }
    }
  }

  //when a tab selected, if sync is enabled, select it in tree/list
  onTabSelected(resource: ARTResource) {
    if (this.sync) {
      this.lastTabResource = resource;
      this.treePanelChild.syncResource(resource);
    }
  }

  onMainTabsetEmpty() {
    this.selectedResource = null;
    this.cdRef.detectChanges();
  }

  onSideTabsetEmpty() {
    this.sideResource = null;
    this.cdRef.detectChanges();
  }

}