import { Injectable } from '@angular/core';
import { ARTURIResource } from '../models/ARTResources';
import { CommitInfo } from '../models/History';
import { ClassesServices } from '../services/classes.service';
import { IndividualsServices } from '../services/individuals.service';
import { OntoLexLemonServices } from '../services/ontolex-lemon.service';
import { PropertyServices } from '../services/properties.service';
import { ResourcesServices } from '../services/resources.service';
import { SkosServices } from '../services/skos.service';
import { UndoServices } from '../services/undo.service';
import { VBContext } from '../utils/VBContext';
import { VBEventHandler } from '../utils/VBEventHandler';
import { BasicModalServices } from '../modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from '../modal-dialogs/Modals';
import { ToastOpt } from '../widget/toast/Toasts';
import { ToastService } from '../widget/toast/toast.service';

@Injectable()
export class UndoHandler {

  constructor(private undoService: UndoServices, private resourceService: ResourcesServices, private individualService: IndividualsServices,
    private ontolexService: OntoLexLemonServices, private skosService: SkosServices, private classService: ClassesServices, private propertyService: PropertyServices,
    private basicModals: BasicModalServices, private toastService: ToastService, private eventHandler: VBEventHandler) { }

  handle() {
    let project = VBContext.getWorkingProject();

    //if no project is accessed, or the accessed one has undo not enabled, do nothing
    if (project == null || !project.isUndoEnabled()) return;

    //perform UNDO only if active element is not a textarea or input (with type text)
    let activeEl: Element = document.activeElement;
    let tagName: string = activeEl.tagName.toLowerCase();
    if (tagName != "textarea" && (tagName != "input" || activeEl.getAttribute("type") != "text")) {
      this.undoService.undo().subscribe({
        next: (commit: CommitInfo) => {
          this.eventHandler.operationUndoneEvent.emit(commit);
          let operation: string = commit.operation.getShow();
          this.toastService.show({ key: "UNDO.OPERATION_UNDONE" }, { key: "UNDO.OPERATION_UNDONE_INFO", params: { operation: operation } });
          this.restoreOldStatus(commit);
        },
        error: (error: Error) => {
          if (error.name.endsWith("RepositoryException") &&
            error.message.includes("Empty undo stack") ||
            error.message.includes("The performer of the last operation does not match the agent for whom undo has been requested")
          ) { //empty undo stack or different user
            let sailExc: string = "SailException: "; //after this exception starts the message
            let errorMsg: string = error.message.substring(error.message.indexOf(sailExc) + sailExc.length);
            this.toastService.show({ key: "STATUS.WARNING" }, errorMsg, new ToastOpt({ toastClass: "bg-warning", textClass: "text-body" }));
          } else {
            let errorMsg = error.message != null ? error.message : "Unknown response from the server";
            let errorDetails = error.stack;
            this.basicModals.alert({ key: "STATUS.ERROR" }, errorMsg, ModalType.error, errorDetails);
          }
        }
      });
    }
  }

  private restoreOldStatus(commit: CommitInfo) {
    let operationId: string = commit.operation.getURI();
    if (commit.created != null) {
      commit.created.forEach(r => {
        if (r instanceof ARTURIResource) {
          //validation doesn't affect this event, the created resource will be removed
          this.eventHandler.resourceCreatedUndoneEvent.emit(r);
        }
      });
    }
    if (commit.deleted != null) {
      if (VBContext.getWorkingProjectCtx().getProject().isValidationEnabled()) {
        //if validation enabled, undone of a delete operation is equivalent to an update, since the staged-del graph is removed => emit resource updated
        commit.deleted.forEach(r => {
          this.resourceService.getResourceDescription(r).subscribe(
            res => this.eventHandler.resourceUpdatedEvent.emit(res)
          );
        });
      } else { //in case of validation disabled emit a dedicated event for each structure
        commit.deleted.forEach(r => {
          if (operationId.endsWith("Classes/deleteClass")) {
            if (r instanceof ARTURIResource) {
              this.classService.getSuperClasses(r).subscribe(
                superClasses => {
                  this.resourceService.getResourceDescription(r).subscribe(
                    res => this.eventHandler.classDeletedUndoneEvent.emit({ resource: res, parents: superClasses })
                  );
                }
              );
            }
          } else if (operationId.endsWith("Classes/deleteInstance")) {
            if (r instanceof ARTURIResource) {
              this.individualService.getNamedTypes(r).subscribe(
                (types: ARTURIResource[]) => {
                  this.resourceService.getResourceDescription(r).subscribe(
                    res => this.eventHandler.instanceDeletedUndoneEvent.emit({ resource: res, types: types })
                  );
                }
              );
            }
          } else if (operationId.endsWith("Datatypes/deleteDatatype")) {
            this.resourceService.getResourceDescription(r).subscribe(
              res => this.eventHandler.datatypeDeletedUndoneEvent.emit(res as ARTURIResource)
            );
          } else if (operationId.endsWith("OntoLexLemon/deleteLexicalEntry")) {
            this.ontolexService.getLexicalEntryLexicons(r as ARTURIResource).subscribe(
              (lexicons: ARTURIResource[]) => {
                this.resourceService.getResourceDescription(r).subscribe(
                  res => this.eventHandler.lexEntryDeletedUndoneEvent.emit({ resource: res as ARTURIResource, lexicons: lexicons })
                );
              }
            );
          } else if (operationId.endsWith("OntoLexLemon/deleteLexicon")) {
            this.resourceService.getResourceDescription(r).subscribe(
              res => this.eventHandler.lexiconDeletedUndoneEvent.emit(res as ARTURIResource)
            );
          } else if (operationId.endsWith("OntoLexLemon/deleteTranslationSet")) {
            this.resourceService.getResourceDescription(r).subscribe(
              res => this.eventHandler.translationSetDeletedUndoneEvent.emit(res as ARTURIResource)
            );
          } else if (operationId.endsWith("Properties/deleteProperty")) {
            this.propertyService.getSuperProperties(r as ARTURIResource).subscribe(
              superProps => {
                this.resourceService.getResourceDescription(r).subscribe(
                  res => this.eventHandler.propertyDeletedUndoneEvent.emit({ resource: res as ARTURIResource, parents: superProps })
                );
              }
            );
          } else if (operationId.endsWith("SKOS/deleteCollection")) {
            this.skosService.getSuperCollections(r as ARTURIResource).subscribe(
              superColls => {
                this.resourceService.getResourceDescription(r).subscribe(
                  res => this.eventHandler.collectionDeletedUndoneEvent.emit({ resource: res as ARTURIResource, parents: superColls })
                );
              }
            );
          } else if (operationId.endsWith("SKOS/deleteConcept")) {
            this.skosService.getSchemesOfConcept(r as ARTURIResource).subscribe(
              (schemes: ARTURIResource[]) => {
                let concTreePref = VBContext.getWorkingProjectCtx().getProjectPreferences().conceptTreePreferences;
                let broaderProps: ARTURIResource[] = concTreePref.broaderProps.map((prop: string) => new ARTURIResource(prop));
                let narrowerProps: ARTURIResource[] = concTreePref.narrowerProps.map((prop: string) => new ARTURIResource(prop));
                this.skosService.getBroaderConcepts(r as ARTURIResource, schemes, concTreePref.multischemeMode, broaderProps, narrowerProps, concTreePref.includeSubProps).subscribe(
                  (broaders: ARTURIResource[]) => {
                    this.resourceService.getResourceDescription(r).subscribe(
                      res => this.eventHandler.conceptDeletedUndoneEvent.emit({ resource: res as ARTURIResource, schemes: schemes, parents: broaders })
                    );

                  }
                );
              }
            );
          } else if (operationId.endsWith("SKOS/deleteConceptScheme")) {
            this.resourceService.getResourceDescription(r).subscribe(
              res => this.eventHandler.schemeDeletedUndoneEvent.emit(res as ARTURIResource)
            );
          }
        });
      }
    }
    if (commit.modified != null) {
      commit.modified.forEach(r => {
        //need to retrieve the resource description in order to restore the annotated value into tree/list
        this.resourceService.getResourceDescription(r).subscribe(
          res => this.eventHandler.resourceUpdatedEvent.emit(res)
        );
      });
    }

  }

}