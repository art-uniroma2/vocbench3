import { Component, ElementRef, ViewChild } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ShaclServices } from "../services/shacl.service";
import { UIUtils } from "../utils/UIUtils";

@Component({
    selector: "shacl-batch-validation-modal",
    templateUrl: "./shacl-batch-validation-modal.component.html",
    standalone: false
})
export class ShaclBatchValidationModalComponent {

    @ViewChild('blockingDiv', { static: true }) private blockingDivElement: ElementRef;

    validationOnCommitEnabled: boolean;

    validationResult: string;

    constructor(public activeModal: NgbActiveModal, private shaclService: ShaclServices) {
    }

    startValidation() {
        UIUtils.startLoadingDiv(this.blockingDivElement.nativeElement);
        this.shaclService.batchValidation().subscribe(
            result => {
                UIUtils.stopLoadingDiv(this.blockingDivElement.nativeElement);
                this.validationResult = result;
            }
        );
    }

    ok() {
        this.activeModal.close();
    }

    cancel() {
        this.activeModal.dismiss();
    }

}