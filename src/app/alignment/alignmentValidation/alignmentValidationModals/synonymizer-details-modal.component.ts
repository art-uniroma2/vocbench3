import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ConceptualizationSet, Lexicon } from "../../../models/Maple";
import { ResolvedSynonymizer } from './create-remote-alignment-task-modal.component';

@Component({
    selector: "synonymizer-details-modal",
    templateUrl: "./synonymizer-details-modal.component.html",
    standalone: false
})
export class SynonymizerDetailsModalComponent {
    @Input() synonymizer: ResolvedSynonymizer;

    lexicon: Lexicon;
    conceptualizationSet: ConceptualizationSet;

    constructor(public activeModal: NgbActiveModal) { }

    ngOnInit() {
        this.lexicon = this.synonymizer.lexiconDataset;
        this.conceptualizationSet = this.synonymizer.conceptualizationSetDataset;
    }

    ok() {
        this.activeModal.close();
    }
    
}