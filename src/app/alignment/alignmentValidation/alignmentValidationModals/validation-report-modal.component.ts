import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: "validation-report-modal",
    templateUrl: "./validation-report-modal.component.html",
    standalone: false
})
export class ValidationReportModalComponent {
    @Input() report: any[]; //collection of object with entity1, entity2, property and action
    
    constructor(public activeModal: NgbActiveModal) {}
    
    ok() {
        this.activeModal.close();
    }

    cancel() {
        this.activeModal.dismiss();
    }
}