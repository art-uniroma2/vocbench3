import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTURIResource } from "../../models/ARTResources";

@Component({
    selector: "assisted-search-result-modal",
    templateUrl: "./assisted-search-result-modal.component.html",
    standalone: false
})
export class AssistedSearchResultModalComponent {
    @Input() title: string;
    @Input() resourceList: ARTURIResource[];

    resourceSelected: ARTURIResource;

    constructor(public activeModal: NgbActiveModal) {}

    ok() {
        this.activeModal.close(this.resourceSelected);
    }

    cancel() {
        this.activeModal.dismiss();
    }
}