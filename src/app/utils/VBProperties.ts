import { Injectable } from '@angular/core';
import { forkJoin, Observable, of, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { ARTResource, ARTURIResource, RDFResourceRolesEnum } from '../models/ARTResources';
import { DataPanel } from '../models/DataStructure';
import { Language, Languages } from '../models/LanguagesCountries';
import { ExtensionPointID, Scope } from '../models/Plugins';
import { ProjectLabelCtx, ProjectVisualization } from '../models/Project';
import { AuthServiceMode, ClassTreePreference, ConceptTreePreference, CustomTreeSettings, InstanceListPreference, LexicalEntryListPreference, NotificationStatus, PredicateLabelSettings, PreferencesUtils, PrefLabelClashMode, ProjectUserSettings, ProjectSettings, ResourceViewPreference, ResourceViewProjectSettings, SearchMode, SearchSettings, SectionFilterPreference, SettingsEnum, SystemSettings, UserSettings, ValueFilterLanguages } from '../models/Properties';
import { ResViewSection } from '../models/ResourceView';
import { Sheet2RdfSettings } from '../models/Sheet2RDF';
import { OntoLex, OWL, RDFS, SKOS } from '../models/Vocabulary';
import { AdministrationServices } from '../services/administration.service';
import { SettingsServices } from '../services/settings.service';
import { VBEventHandler } from '../utils/VBEventHandler';
import { STRequestOptions } from './HttpManager';
import { LocalStorageManager } from './LocalStorageManager';
import { ProjectContext, VBContext } from './VBContext';

@Injectable()
export class VBProperties {

  private eventSubscriptions: Subscription[] = [];

  constructor(private adminService: AdministrationServices, private settingsService: SettingsServices,
    private eventHandler: VBEventHandler) {
    this.eventSubscriptions.push(eventHandler.resourceRenamedEvent.subscribe(
      (data: { oldResource: ARTResource, newResource: ARTResource }) => this.onResourceRenamed(data.oldResource, data.newResource)
    ));
  }

  ngOnDestroy() {
    this.eventSubscriptions.forEach(s => s.unsubscribe());
  }

  /* =============================
  ========= PREFERENCES ==========
  ============================= */

  /**
   * To call each time the user change project
   * @param projectCtx
   */
  initUserProjectPreferences(projectCtx: ProjectContext): Observable<any> {
    let getPUSettingsCore = this.settingsService.getSettings(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, STRequestOptions.getRequestOptions(projectCtx)).pipe(
      map(settings => {
        let puSettings: ProjectUserSettings = projectCtx.getProjectPreferences();

        let activeSchemes: ARTURIResource[] = [];
        let schemesStr: string[] = settings.getPropertyValue(SettingsEnum.activeSchemes);
        if (schemesStr != null) {
          schemesStr.forEach(s => activeSchemes.push(new ARTURIResource(s, null, RDFResourceRolesEnum.conceptScheme)));
        }
        puSettings.activeSchemes = activeSchemes;

        let activeLexicon: ARTURIResource;
        let activeLexiconStr: string = settings.getPropertyValue(SettingsEnum.activeLexicon);
        if (activeLexiconStr != null) {
          activeLexicon = new ARTURIResource(activeLexiconStr, null, RDFResourceRolesEnum.limeLexicon);
        }
        puSettings.activeLexicon = activeLexicon;

        puSettings.showFlags = settings.getPropertyValue(SettingsEnum.showFlags);

        let projectThemeId = settings.getPropertyValue(SettingsEnum.projectTheme);
        puSettings.projectThemeId = projectThemeId;
        if (projectCtx.getProject().getName() == VBContext.getWorkingProject().getName()) {
          //change the them only if the PU settings are initialized for the current project
          //(so, not if they are init before accessing an external project, like in resource-picker)
          this.eventHandler.themeChangedEvent.emit(puSettings.projectThemeId);
        }

        //languages 
        puSettings.editingLanguage = settings.getPropertyValue(SettingsEnum.editingLanguage);

        let filterValueLangPref = settings.getPropertyValue(SettingsEnum.filterValueLanguages);
        if (filterValueLangPref == null) {
          puSettings.filterValueLang = { languages: [], enabled: false }; //default
        } else {
          puSettings.filterValueLang = filterValueLangPref;
        }

        //resource view preferences
        puSettings.resViewPreferences = new ResourceViewPreference();
        let resViewSetting: ResourceViewPreference = settings.getPropertyValue(SettingsEnum.resourceView);
        if (resViewSetting != null) {
          PreferencesUtils.mergePreference(puSettings.resViewPreferences, resViewSetting);
        }
        this.initResourceViewPreferenceCookie(puSettings); //fill the preferences also with those stored as cookie

        puSettings.resViewPartitionFilter = settings.getPropertyValue(SettingsEnum.resViewPartitionFilter, {});

        //graph preferences
        let graphSectionFilterPref = settings.getPropertyValue(SettingsEnum.graphViewPartitionFilter);
        if (graphSectionFilterPref == null) { //initialize with the only lexicalization section for each role
          graphSectionFilterPref = {};
          for (let role in RDFResourceRolesEnum) {
            graphSectionFilterPref[role] = [ResViewSection.lexicalizations];
          }
        }
        puSettings.graphViewSectionFilter = graphSectionFilterPref;

        puSettings.hideLiteralGraphNodes = settings.getPropertyValue(SettingsEnum.hideLiteralGraphNodes);

        //cls tree preferences
        puSettings.classTreePreferences = new ClassTreePreference(projectCtx.getProject());
        let clsTreeSettings: ClassTreePreference = settings.getPropertyValue(SettingsEnum.classTree);
        if (clsTreeSettings != null) {
          PreferencesUtils.mergePreference(puSettings.classTreePreferences, clsTreeSettings);
        }

        //instance list preferences
        puSettings.instanceListPreferences = new InstanceListPreference();
        let instListSetting: InstanceListPreference = settings.getPropertyValue(SettingsEnum.instanceList);
        if (instListSetting != null) {
          PreferencesUtils.mergePreference(puSettings.instanceListPreferences, instListSetting);
        }


        //concept tree preferences
        puSettings.conceptTreePreferences = new ConceptTreePreference();
        let concTreeSetting: ConceptTreePreference = settings.getPropertyValue(SettingsEnum.conceptTree);
        if (concTreeSetting != null) {
          PreferencesUtils.mergePreference(puSettings.conceptTreePreferences, concTreeSetting);
        }


        //lexical entry list preferences
        puSettings.lexEntryListPreferences = new LexicalEntryListPreference();
        let lexEntrySetting: LexicalEntryListPreference = settings.getPropertyValue(SettingsEnum.lexEntryList);
        if (lexEntrySetting != null) {
          PreferencesUtils.mergePreference(puSettings.lexEntryListPreferences, lexEntrySetting);
        }

        //custom tree
        puSettings.customTreeSettings = new CustomTreeSettings();
        let cTreeSetting = settings.getPropertyValue(SettingsEnum.customTree);
        if (cTreeSetting != null) {
          PreferencesUtils.mergePreference(puSettings.customTreeSettings, cTreeSetting);
        }

        //search settings
        puSettings.searchSettings = new SearchSettings();
        let searchSettings: SearchSettings = settings.getPropertyValue(SettingsEnum.searchSettings);
        if (searchSettings != null) {
          PreferencesUtils.mergePreference(puSettings.searchSettings, searchSettings);
        }
        this.initSearchSettingsCookie(puSettings); //other settings stored in cookies

        //s2rdf
        puSettings.sheet2RdfSettings = new Sheet2RdfSettings();
        let s2rdfSettings: Sheet2RdfSettings = settings.getPropertyValue(SettingsEnum.sheet2rdfSettings);
        if (s2rdfSettings != null) {
          PreferencesUtils.mergePreference(puSettings.sheet2RdfSettings, s2rdfSettings);
        }

        //notifications
        let notificationStatusSetting = settings.getPropertyValue(SettingsEnum.notificationsStatus);
        if (notificationStatusSetting != null) {
          puSettings.notificationStatus = notificationStatusSetting;
        }

        //data panels filter
        let model = projectCtx.getProject().getModelType();
        let hiddenDataPanels: DataPanel[];
        if (model == SKOS.uri) {
          hiddenDataPanels = settings.getPropertyValue(SettingsEnum.hiddenDataPanelsSkos);
        } else if (model == OntoLex.uri) {
          hiddenDataPanels = settings.getPropertyValue(SettingsEnum.hiddenDataPanelsOntolex);
        } else if (model == OWL.uri || model == RDFS.uri) {
          hiddenDataPanels = settings.getPropertyValue(SettingsEnum.hiddenDataPanelsRdfsOwl);
        }
        if (hiddenDataPanels != null) {
          puSettings.hiddenDataPanels = hiddenDataPanels;
        }

        //structures rendering
        puSettings.renderingClassTree = settings.getPropertyValue(SettingsEnum.renderingClassTree);
        puSettings.renderingCollectionTree = settings.getPropertyValue(SettingsEnum.renderingCollectionTree);
        puSettings.renderingConceptTree = settings.getPropertyValue(SettingsEnum.renderingConceptTree);
        puSettings.renderingDatatypeList = settings.getPropertyValue(SettingsEnum.renderingDatatypeList);
        puSettings.renderingInstanceList = settings.getPropertyValue(SettingsEnum.renderingInstanceList);
        puSettings.renderingLexEntryList = settings.getPropertyValue(SettingsEnum.renderingLexEntryList);
        puSettings.renderingLexiconList = settings.getPropertyValue(SettingsEnum.renderingLexiconList);
        puSettings.renderingPropertyTree = settings.getPropertyValue(SettingsEnum.renderingPropertyTree);
        puSettings.renderingSchemeList = settings.getPropertyValue(SettingsEnum.renderingSchemeList);

      })
    );
    // this is called separately since it is about a different plugin
    let getPUSettingsRenderingEngine = this.settingsService.getSettings(ExtensionPointID.RENDERING_ENGINE_ID, Scope.PROJECT_USER, STRequestOptions.getRequestOptions(projectCtx)).pipe(
      map(settings => {
        projectCtx.getProjectPreferences().renderingLanguagesPreference = settings.getPropertyValue(SettingsEnum.languages).split(",");
      })
    );
    return forkJoin([
      getPUSettingsRenderingEngine,
      getPUSettingsCore,
    ]);
  }

  setActiveSchemes(projectCtx: ProjectContext, schemes: ARTURIResource[]): Observable<void> {
    if (schemes == null) {
      projectCtx.getProjectPreferences().activeSchemes = [];
    } else {
      projectCtx.getProjectPreferences().activeSchemes = schemes;
    }
    let schemesPropValue: string[] = schemes.map(s => s.toNT());
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.activeSchemes, schemesPropValue, new STRequestOptions({ ctxProject: projectCtx.getProject() })).pipe(
      map(() => {
        this.eventHandler.schemeChangedEvent.emit({ schemes: schemes, project: projectCtx.getProject() });
      })
    );
  }

  setActiveLexicon(projectCtx: ProjectContext, lexicon: ARTURIResource): Observable<void> {
    projectCtx.getProjectPreferences().activeLexicon = lexicon;
    let lexiconUri: string = lexicon != null ? lexicon.toNT() : null;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.activeLexicon, lexiconUri, new STRequestOptions({ ctxProject: projectCtx.getProject() })).pipe(
      map(() => {
        this.eventHandler.lexiconChangedEvent.emit({ lexicon: lexicon, project: projectCtx.getProject() });
      })
    );
  }


  getShowFlags(): boolean {
    if (VBContext.getWorkingProjectCtx() != null) {
      return VBContext.getWorkingProjectCtx().getProjectPreferences().showFlags;
    } else {
      return VBContext.getSystemSettings().showFlags;
    }
  }
  setShowFlags(show: boolean): Observable<void> {
    VBContext.getWorkingProjectCtx().getProjectPreferences().showFlags = show;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.showFlags, show).pipe(
      map(() => {
        this.eventHandler.showFlagChangedEvent.emit(show);
      })
    );
  }

  setProjectTheme(theme: number): Observable<void> {
    VBContext.getWorkingProjectCtx().getProjectPreferences().projectThemeId = theme;
    let value = (theme == 0) ? null : theme + ""; //theme 0 is the default one, so remove the preference
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.projectTheme, value).pipe(
      map(() => {
        this.eventHandler.themeChangedEvent.emit(theme);
      })
    );
  }

  setRenderingLanguagesPreference(languages: string[]): Observable<void> {
    let value: string = (languages.length == 0) ? null : languages.join(",");
    VBContext.getWorkingProjectCtx().getProjectPreferences().renderingLanguagesPreference = languages;
    return this.settingsService.storeSetting(ExtensionPointID.RENDERING_ENGINE_ID, Scope.PROJECT_USER, SettingsEnum.languages, value);
  }

  setEditingLanguage(lang: string): Observable<void> {
    VBContext.getWorkingProjectCtx().getProjectPreferences().editingLanguage = lang;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.editingLanguage, lang);
  }

  setValueFilterLanguages(filter: ValueFilterLanguages): Observable<void> {
    VBContext.getWorkingProjectCtx().getProjectPreferences().filterValueLang = filter;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.filterValueLanguages, filter);
  }

  setNotificationStatus(status: NotificationStatus): Observable<void> {
    VBContext.getWorkingProjectCtx().getProjectPreferences().notificationStatus = status;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.notificationsStatus, status).pipe(
      map(() => {
        this.eventHandler.notificationStatusChangedEvent.emit();
      })
    );
  }

  setHiddenDataPanels(projectCtx: ProjectContext, panels: DataPanel[]): Observable<void> {
    projectCtx.getProjectPreferences().hiddenDataPanels = panels;
    let model = projectCtx.getProject().getModelType();
    let settingName: SettingsEnum;
    if (model == SKOS.uri) {
      settingName = SettingsEnum.hiddenDataPanelsSkos;
    } else if (model == OWL.uri || model == RDFS.uri) {
      settingName = SettingsEnum.hiddenDataPanelsRdfsOwl;
    } else if (model == OntoLex.uri) { //project.getModelType() == OntoLex.uri
      settingName = SettingsEnum.hiddenDataPanelsOntolex;
    }
    if (settingName != null) { //it may be left empty if project is Edoal (should not happens though)
      return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, settingName, panels, new STRequestOptions({ ctxProject: projectCtx.getProject() }));
    } else {
      return of(null); //should never happen
    }
  }

  //class tree settings
  setClassTreePreferences(clsTreePref: ClassTreePreference): Observable<void> {
    VBContext.getWorkingProjectCtx().getProjectPreferences().classTreePreferences = clsTreePref;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.classTree, clsTreePref);
  }

  //instance list settings
  setInstanceListPreferences(instListPref: InstanceListPreference): Observable<void> {
    VBContext.getWorkingProjectCtx().getProjectPreferences().instanceListPreferences = instListPref;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.instanceList, instListPref);
  }

  //concept tree settings
  setConceptTreePreferences(concTreePrefs: ConceptTreePreference): Observable<void> {
    VBContext.getWorkingProjectCtx().getProjectPreferences().conceptTreePreferences = concTreePrefs;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.conceptTree, concTreePrefs);
  }

  //lex entry list settings
  setLexicalEntryListPreferences(lexEntryListPrefs: LexicalEntryListPreference): Observable<void> {
    VBContext.getWorkingProjectCtx().getProjectPreferences().lexEntryListPreferences = lexEntryListPrefs;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.lexEntryList, lexEntryListPrefs);
  }

  setCustomTreeSettings(projectCtx: ProjectContext, customTreeSettings: CustomTreeSettings): Observable<void> {
    projectCtx.getProjectPreferences().customTreeSettings = customTreeSettings;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.customTree, customTreeSettings, new STRequestOptions({ ctxProject: projectCtx.getProject() })).pipe(
      map(() => {
        this.eventHandler.customTreeSettingsChangedEvent.emit();
      })
    );
  }

  setStructureRendering(projectCtx: ProjectContext, panelRole: RDFResourceRolesEnum, rendering: boolean): Observable<void> {
    let settingEnum: SettingsEnum;
    if (panelRole == RDFResourceRolesEnum.cls) {
      projectCtx.getProjectPreferences().renderingClassTree = rendering;
      settingEnum = SettingsEnum.renderingClassTree;
    } else if (panelRole == RDFResourceRolesEnum.skosCollection) {
      projectCtx.getProjectPreferences().renderingCollectionTree = rendering;
      settingEnum = SettingsEnum.renderingCollectionTree;
    } else if (panelRole == RDFResourceRolesEnum.concept) {
      projectCtx.getProjectPreferences().renderingConceptTree = rendering;
      settingEnum = SettingsEnum.renderingConceptTree;
    } else if (panelRole == RDFResourceRolesEnum.dataRange) {
      projectCtx.getProjectPreferences().renderingDatatypeList = rendering;
      settingEnum = SettingsEnum.renderingDatatypeList;
    } else if (panelRole == RDFResourceRolesEnum.individual) {
      projectCtx.getProjectPreferences().renderingInstanceList = rendering;
      settingEnum = SettingsEnum.renderingInstanceList;
    } else if (panelRole == RDFResourceRolesEnum.ontolexLexicalEntry) {
      projectCtx.getProjectPreferences().renderingLexEntryList = rendering;
      settingEnum = SettingsEnum.renderingLexEntryList;
    } else if (panelRole == RDFResourceRolesEnum.limeLexicon) {
      projectCtx.getProjectPreferences().renderingLexiconList = rendering;
      settingEnum = SettingsEnum.renderingLexiconList;
    } else if (panelRole == RDFResourceRolesEnum.property) {
      projectCtx.getProjectPreferences().renderingPropertyTree = rendering;
      settingEnum = SettingsEnum.renderingPropertyTree;
    } else if (panelRole == RDFResourceRolesEnum.conceptScheme) {
      projectCtx.getProjectPreferences().renderingSchemeList = rendering;
      settingEnum = SettingsEnum.renderingSchemeList;
    }
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, settingEnum, rendering, new STRequestOptions({ ctxProject: projectCtx.getProject() }));
  }

  //Res view settings
  setResourceViewPreferences(resViewPrefs: ResourceViewPreference): Observable<void> {
    VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPreferences = resViewPrefs;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.resourceView, resViewPrefs);
  }

  refreshResourceViewSectionFilter(): Observable<void> { //refreshed the cached rv section filter
    return this.settingsService.getSettings(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER).pipe(
      map(settings => {
        VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPartitionFilter = settings.getPropertyValue(SettingsEnum.resViewPartitionFilter, {});
      })
    );
  }

  setResViewSectionFilter(setting: SectionFilterPreference) {
    VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPartitionFilter = setting;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.resViewPartitionFilter, setting);
  }

  //Graph settings
  setGraphViewSectionFilter(pref: SectionFilterPreference): Observable<void> {
    VBContext.getWorkingProjectCtx().getProjectPreferences().graphViewSectionFilter = pref;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.graphViewPartitionFilter, pref);
  }
  setHideLiteralGraphNodes(hide: boolean): Observable<void> {
    VBContext.getWorkingProjectCtx().getProjectPreferences().hideLiteralGraphNodes = hide;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.hideLiteralGraphNodes, hide);
  }

  /* =============================
  =========== BINDINGS ===========
  ============================= */

  initProjectUserBindings(projectCtx: ProjectContext): Observable<void> {
    return this.adminService.getProjectUserBinding(projectCtx.getProject()).pipe(
      map(pub => {
        projectCtx.setProjectUserBinding(pub);
      })
    );
  }


  /* =============================
  =========== SETTINGS ===========
  ============================= */

  /*  --- SYSTEM --- */

  initStartupSystemSettings(): Observable<void> {
    return this.settingsService.getStartupSettings().pipe(
      map(settings => {
        let systemSettings: SystemSettings = new SystemSettings();
        systemSettings.experimentalFeaturesEnabled = settings.getPropertyValue(SettingsEnum.experimentalFeaturesEnabled);
        systemSettings.privacyStatementAvailable = settings.getPropertyValue(SettingsEnum.privacyStatementAvailable);
        systemSettings.showFlags = settings.getPropertyValue(SettingsEnum.showFlags);
        systemSettings.homeContent = settings.getPropertyValue(SettingsEnum.homeContent);
        systemSettings.emailVerification = settings.getPropertyValue(SettingsEnum.emailVerification);
        let systemLanguages: Language[] = settings.getPropertyValue(SettingsEnum.languages);
        Languages.sortLanguages(systemLanguages);
        systemSettings.languages = systemLanguages;
        let authServiceValue = settings.getPropertyValue(SettingsEnum.authService);
        if (authServiceValue in AuthServiceMode) {
          systemSettings.authService = authServiceValue;
        }
        VBContext.setSystemSettings(systemSettings);
      })
    );
  }

  setExperimentalFeaturesEnabled(enabled: boolean): Observable<void> {
    VBContext.getSystemSettings().experimentalFeaturesEnabled = enabled;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.SYSTEM, SettingsEnum.experimentalFeaturesEnabled, enabled);
  }

  setEmailVerification(enabled: boolean): Observable<void> {
    VBContext.getSystemSettings().emailVerification = enabled;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.SYSTEM, SettingsEnum.emailVerification, enabled);
  }

  setHomeContent(homeContent: string): Observable<void> {
    VBContext.getSystemSettings().homeContent = homeContent;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.SYSTEM, SettingsEnum.homeContent, homeContent);
  }

  isPrivacyStatementAvailable(): boolean {
    return VBContext.getSystemSettings().privacyStatementAvailable;
  }

  /*  --- PROJECT --- */

  initProjectSettings(projectCtx: ProjectContext): Observable<any> {
    let projectSettings: ProjectSettings = projectCtx.getProjectSettings();
    return this.settingsService.getSettings(ExtensionPointID.ST_CORE_ID, Scope.PROJECT, STRequestOptions.getRequestOptions(projectCtx)).pipe(
      map(settings => {
        let langsValue: Language[] = settings.getPropertyValue(SettingsEnum.languages);
        projectSettings.projectLanguagesSetting = langsValue;
        Languages.sortLanguages(projectSettings.projectLanguagesSetting);

        let labelClashModeValue = settings.getPropertyValue(SettingsEnum.labelClashMode);
        if (labelClashModeValue != null && labelClashModeValue in PrefLabelClashMode) { //if not null and valid enum
          projectSettings.prefLabelClashMode = labelClashModeValue;
        }

        projectSettings.resourceView = new ResourceViewProjectSettings();
        let rvSettings: ResourceViewProjectSettings = settings.getPropertyValue(SettingsEnum.resourceView);
        if (rvSettings != null) {
          PreferencesUtils.mergePreference(projectSettings.resourceView, rvSettings);
        }

        projectSettings.resViewPredLabelMappings = new PredicateLabelSettings();
        let predLabelSettings: PredicateLabelSettings = settings.getPropertyValue(SettingsEnum.resViewPredLabelMappings);
        if (predLabelSettings != null) {
          PreferencesUtils.mergePreference(projectSettings.resViewPredLabelMappings, predLabelSettings);
        }

        projectSettings.resViewSectionsCustomization = settings.getPropertyValue(SettingsEnum.resViewSectionsCustomization);

        projectSettings.dataPanelLabelMappings = settings.getPropertyValue(SettingsEnum.dataPanelLabelMappings);

        projectSettings.timeMachineEnabled = settings.getPropertyValue(SettingsEnum.timeMachineEnabled);
      })
    );
  }

  /*  --- USER --- */

  initUserSettings(): Observable<any> {
    return this.settingsService.getSettings(ExtensionPointID.ST_CORE_ID, Scope.USER).pipe(
      map(settings => {
        let userSettings = new UserSettings();

        let projectVisualization: ProjectVisualization = settings.getPropertyValue(SettingsEnum.projectVisualization, new ProjectVisualization());
        userSettings.projectVisualization = projectVisualization;

        let rendering: boolean = settings.getPropertyValue(SettingsEnum.projectRendering, false);
        userSettings.projectRendering = rendering;
        ProjectLabelCtx.renderingEnabled = rendering;

        VBContext.setUserSettings(userSettings);
      })
    );
  }

  setProjectVisualization(projectVisualization: ProjectVisualization): Observable<void> {
    VBContext.getUserSettings().projectVisualization = projectVisualization;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.USER, SettingsEnum.projectVisualization, projectVisualization);
  }

  setProjectRendering(rendering: boolean): Observable<void> {
    VBContext.getUserSettings().projectRendering = rendering;
    //update rendering option also in ProjectLabelCtx (why? read explanation in comment on ProjectLabelCtx class)
    ProjectLabelCtx.renderingEnabled = rendering;
    return this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.USER, SettingsEnum.projectRendering, rendering);
  }


  /*
  PREFERENCES IN LOCAL STORAGE
  */

  /**
   * Some resource view preference are stored in the browser LocalStorage, not server-side preference. Here they are initialized
   * @param projectPreferences 
   */
  initResourceViewPreferenceCookie(projectPreferences: ProjectUserSettings) {
    let rvPrefs: ResourceViewPreference = projectPreferences.resViewPreferences;
    //tab sync
    let syncTabsCookie: string = LocalStorageManager.getItem(LocalStorageManager.RES_VIEW_TAB_SYNCED);
    if (syncTabsCookie != null) {
      rvPrefs.syncTabs = syncTabsCookie == "true"; //default false
    }
    //display img
    let displayImgCookie = LocalStorageManager.getItem(LocalStorageManager.RES_VIEW_DISPLAY_IMG);
    if (displayImgCookie != null) {
      rvPrefs.displayImg = displayImgCookie == "true"; //default false
    }
    //show deprecated
    let showDeprecatedCookie = LocalStorageManager.getItem(LocalStorageManager.RES_VIEW_SHOW_DEPRECATED);
    if (showDeprecatedCookie != null) {
      rvPrefs.showDeprecated = showDeprecatedCookie != "false"; //default true
    }
    let sortByRenderingCookie = LocalStorageManager.getItem(LocalStorageManager.RES_VIEW_SORT_BY_RENDERING);
    if (sortByRenderingCookie != null) {
      rvPrefs.sortByRendering = sortByRenderingCookie == "true";
    }
    let forceSearchModeForAddPropertyValueValue = LocalStorageManager.getItem(LocalStorageManager.RES_VIEW_FORCE_SEARCHMODE_FOR_ADD_PROPERTY_VALUE);
    if (forceSearchModeForAddPropertyValueValue != null) {
      rvPrefs.forceSearchModeForAddPropertyValue = forceSearchModeForAddPropertyValueValue == "true"; //default false
    }
  }

  /**
   * ResView Tab sync
   */
  setResourceViewTabSync(sync: boolean) {
    LocalStorageManager.setItem(LocalStorageManager.RES_VIEW_TAB_SYNCED, sync + "");
    VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPreferences.syncTabs = sync;
  }

  /**
   * ResView display img
   */
  setResourceViewDisplayImg(display: boolean) {
    LocalStorageManager.setItem(LocalStorageManager.RES_VIEW_DISPLAY_IMG, display + "");
    VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPreferences.displayImg = display;
  }

  /**
   * ResView show deprecated linked resources
   * @param display 
   */
  setShowDeprecatedInResView(show: boolean) {
    LocalStorageManager.setItem(LocalStorageManager.RES_VIEW_SHOW_DEPRECATED, show + "");
    VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPreferences.showDeprecated = show;
  }

  setSortByRendering(sort: boolean) {
    LocalStorageManager.setItem(LocalStorageManager.RES_VIEW_SORT_BY_RENDERING, sort + "");
    VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPreferences.sortByRendering = sort;
  }

  setForceSearchModeForAddPropertyValue(force: boolean) {
    LocalStorageManager.setItem(LocalStorageManager.RES_VIEW_FORCE_SEARCHMODE_FOR_ADD_PROPERTY_VALUE, force + "");
    VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPreferences.forceSearchModeForAddPropertyValue = force;
  }

  /**
   * Tree/list Show deprecated
   */
  setShowDeprecated(showDeprecated: boolean) {
    LocalStorageManager.setItem(LocalStorageManager.SHOW_DEPRECATED, showDeprecated + "");
    this.eventHandler.showDeprecatedChangedEvent.emit(showDeprecated);
  }
  getShowDeprecated(): boolean {
    let cookieValue: string = LocalStorageManager.getItem(LocalStorageManager.SHOW_DEPRECATED);
    return cookieValue != "false"; //default true
  }


  private initSearchSettingsCookie(preferences: ProjectUserSettings) {
    let searchModeCookie: string = LocalStorageManager.getItem(LocalStorageManager.SEARCH_STRING_MATCH_MODE);
    if (searchModeCookie != null) {
      preferences.searchSettings.stringMatchMode = searchModeCookie as SearchMode;
    }
    let useUriCookie: string = LocalStorageManager.getItem(LocalStorageManager.SEARCH_USE_URI);
    if (useUriCookie != null) {
      preferences.searchSettings.useURI = useUriCookie == "true";
    }
    let useLocalNameCookie: string = LocalStorageManager.getItem(LocalStorageManager.SEARCH_USE_LOCAL_NAME);
    if (useLocalNameCookie != null) {
      preferences.searchSettings.useLocalName = useLocalNameCookie == "true";
    }
    let useNotesCookie: string = LocalStorageManager.getItem(LocalStorageManager.SEARCH_USE_NOTES);
    if (useNotesCookie != null) {
      preferences.searchSettings.useNotes = useNotesCookie == "true";
    }
    let restrictSchemesCookie: string = LocalStorageManager.getItem(LocalStorageManager.SEARCH_CONCEPT_SCHEME_RESTRICTION);
    if (restrictSchemesCookie != null) {
      preferences.searchSettings.restrictActiveScheme = restrictSchemesCookie == "true";
    }
    let extendAllIndividualsCookie: string = LocalStorageManager.getItem(LocalStorageManager.SEARCH_EXTEND_ALL_INDIVIDUALS);
    if (restrictSchemesCookie != null) {
      preferences.searchSettings.extendToAllIndividuals = extendAllIndividualsCookie == "true";
    }
  }

  setSearchSettings(projectCtx: ProjectContext, settings: SearchSettings) {
    let oldSearchSettings: SearchSettings = projectCtx.getProjectPreferences().searchSettings;

    LocalStorageManager.setItem(LocalStorageManager.SEARCH_STRING_MATCH_MODE, settings.stringMatchMode);
    LocalStorageManager.setItem(LocalStorageManager.SEARCH_USE_URI, settings.useURI + "");
    LocalStorageManager.setItem(LocalStorageManager.SEARCH_USE_LOCAL_NAME, settings.useLocalName + "");
    LocalStorageManager.setItem(LocalStorageManager.SEARCH_USE_NOTES, settings.useNotes + "");
    LocalStorageManager.setItem(LocalStorageManager.SEARCH_CONCEPT_SCHEME_RESTRICTION, settings.restrictActiveScheme + "");
    LocalStorageManager.setItem(LocalStorageManager.SEARCH_EXTEND_ALL_INDIVIDUALS, settings.extendToAllIndividuals + "");

    let changed: boolean = oldSearchSettings.languages != settings.languages ||
      oldSearchSettings.languages != settings.languages ||
      oldSearchSettings.restrictLang != settings.restrictLang ||
      oldSearchSettings.includeLocales != settings.includeLocales ||
      oldSearchSettings.useAutocompletion != settings.useAutocompletion;

    if (changed) {
      //the properties stored as cookie (e.g. useURI, useLocalName, ...) will be simply ignored server side, so I can pass here the whole searchSettings object
      this.settingsService.storeSetting(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, SettingsEnum.searchSettings, settings,
        new STRequestOptions({ ctxProject: projectCtx.getProject() })).subscribe();
    }
    projectCtx.getProjectPreferences().searchSettings = settings;
    this.eventHandler.searchPrefsUpdatedEvent.emit(projectCtx.getProject());
  }

  //EVENT HANDLER
  /**
   * In case of resource renamed, check if the resource is a current active scheme, in case update the preference
   * @param oldResource 
   * @param newResource 
   */
  private onResourceRenamed(oldResource: ARTResource, newResource: ARTResource) {
    let activeSchemes: ARTURIResource[] = VBContext.getWorkingProjectCtx().getProjectPreferences().activeSchemes;
    for (const s of activeSchemes) {
      if (s.equals(oldResource)) {
        s.setURI(newResource.getNominalValue());
        this.setActiveSchemes(VBContext.getWorkingProjectCtx(), activeSchemes).subscribe();
        break;
      }
    }
  }

}