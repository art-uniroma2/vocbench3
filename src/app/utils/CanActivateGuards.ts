import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserServices } from '../services/user.service';
import { AuthorizationEvaluator } from './AuthorizationEvaluator';
import { VBActionsEnum } from './VBActions';
import { VBContext } from './VBContext';

/**
 * Guard that prevents accessing page to not logged user
 */
export const authGuard = () => {
  const router = inject(Router);
  const userService = inject(UserServices);

  if (VBContext.isLoggedIn()) {
    return of(true);
  } else {
    if (VBContext.getSystemSettings().authService === "OAuth2") { // Avoid following the getUser in case of authMode = “OAuth2”
      return of(false);
    }
    /* if there is no user in vbCtx it doesn't mean that the user is not logged, in fact,
    if the user refresh the page, VBContext is reinitialized and then userLogged is reset to null.
    Here try to retrieve from server the logged user. */
    return userService.getUser().pipe(
      map(user => { //request completed succesfully, set the user in the context and return true
        if (user) { //getUser returned the logged user
          return true;
        } else { //no logged user, getUser returned null
          router.navigate(['/Home']);
          return false;
        }
      })
    );
  }
};

/**
 * Guard that prevents accessing page to not "normal" user (allows only to admin)
 */
export const adminGuard = () => {
  const router = inject(Router);
  const userService = inject(UserServices);

  if (VBContext.isLoggedIn()) {
    return of(VBContext.getLoggedUser().isAdmin());
  } else {
    /* if there is no user in vbCtx it doesn't mean that the user is not logged, in fact,
    if the user refresh the page, VBContext is reinitialized and then userLogged is reset to null.
    Here try to retrieve from server the logged user. */
    return userService.getUser().pipe(
      map(user => { //request completed succesfully, set the user in the context and return true
        if (user) { //getUser returned the logged user
          return user.isAdmin();
        } else { //no logged user, getUser returned null
          router.navigate(['/Home']);
          return false;
        }
      })
    );
  }
};


export const superUserGuard = () => {
  const router = inject(Router);
  const userService = inject(UserServices);

  if (VBContext.isLoggedIn()) {
    return of(VBContext.getLoggedUser().isSuperUser() || VBContext.getLoggedUser().isAdmin());
  } else {
    /* if there is no user in vbCtx it doesn't mean that the user is not logged, in fact,
    if the user refresh the page, VBContext is reinitialized and then userLogged is reset to null.
    Here try to retrieve from server the logged user. */
    return userService.getUser().pipe(
      map(user => { //request completed succesfully, set the user in the context and return true
        if (user) { //getUser returned the logged user
          return user.isSuperUser() || user.isAdmin();
        } else { //no logged user, getUser returned null
          router.navigate(['/Home']);
          return false;
        }
      })
    );
  }
};

export const pmGuard = () => {
  if (VBContext.isLoggedIn()) {
    return of(AuthorizationEvaluator.isAuthorized(VBActionsEnum.administrationProjectManagement));
  } else {
    return of(false);
  }
};



/**
 * Guard that prevents accessing page whit no project accessed
 */
export const projectGuard = () => {
  const router = inject(Router);

  if (VBContext.getWorkingProject() != undefined) {
    return of(true);
  } else {
    router.navigate(['/Home']);
    return of(false);
  }
};
