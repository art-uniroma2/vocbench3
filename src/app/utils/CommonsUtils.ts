export class CommonsUtils {

    static getCryptoStrongRandomString(length: number) {
        let charset = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        let result: string = "";
        if (window.crypto && window.crypto.getRandomValues) {
            let values = new Uint32Array(length);
            window.crypto.getRandomValues(values);
            for (let i = 0; i < length; i++) {
                result += charset[values[i] % charset.length];
            }
            return result;
        }
        else throw new Error("Your browser can't generate secure random numbers");
    }

    static getCryptoStrongRandomInt(min: number, max: number) {
        const array = new Uint32Array(1);
        window.crypto.getRandomValues(array);
        const range = max - min + 1;
        const maxRange = 2 ** 32 - 1;
        const num = array[0] / maxRange;
        return Math.floor(num * range) + min;
    }

    static cryptoRandom(): number {
        const array = new Uint32Array(1);
        window.crypto.getRandomValues(array);
        const maxRange = 2 ** 32 - 1;
        return array[0] / maxRange;
    }


}