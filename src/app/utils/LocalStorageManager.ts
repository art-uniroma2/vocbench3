import { Project } from "../models/Project";
import { User } from "../models/User";

export class LocalStorageManager {

    public static MANCH_EXPR_SKIP_SEMANTIC_CHECK = "manchester.skip_semantic_check";

    public static SHOW_DEPRECATED = "tree_list.show_deprecated";

    public static ALIGNMENT_VALIDATION_ALIGNMENT_PER_PAGE = "alignment_validation.alignment_per_page";
    public static ALIGNMENT_VALIDATION_RELATION_SHOW = "alignment_validation.relation_show";
    public static ALIGNMENT_VALIDATION_SHOW_CONFIDENCE = "alignment_validation.show_confidence";
    public static ALIGNMENT_VALIDATION_REJECTED_ALIGNMENT_ACTION = "alignment_validation.rejected_alignment_action";
    public static ALIGNMENT_VALIDATION_RENDERING = "alignment_validation.rendering";

    public static ALIGNMENT_LAST_EXPLORED_PROJECT = "alignment.last_project";
    public static ALIGNMENT_LAST_CHOSEN_TYPE = "alignment.last_type";

    public static LEX_ENTRY_LAST_INDEX = "structures.lex_entry.last_index";

    public static RES_VIEW_CODE_FORMAT = "resource_view.code_format";
    public static RES_VIEW_DISPLAY_IMG = "resource_view.display_img";
    public static RES_VIEW_FORCE_SEARCHMODE_FOR_ADD_PROPERTY_VALUE = "resource_view.force_searchmode_for_add_property_value";
    public static RES_VIEW_GUIDELINES_DISMISSED = "resview.guidelines_dismissed";
    public static RES_VIEW_SHOW_DEPRECATED = "resource_view.show_deprecated";
    public static RES_VIEW_SORT_BY_RENDERING = "resource_view.sort_by_rendering";
    public static RES_VIEW_TAB_SYNCED = "resource_view.tab_synced";

    public static SEARCH_STRING_MATCH_MODE = "search.string_match_mode";
    public static SEARCH_USE_URI = "search.use_uri";
    public static SEARCH_USE_LOCAL_NAME = "search.use_local_name";
    public static SEARCH_USE_NOTES = "search.use_notes";
    public static SEARCH_CONCEPT_SCHEME_RESTRICTION = "search.restrict_active_schemes";
    public static SEARCH_EXTEND_ALL_INDIVIDUALS = "search.extends_all_inividuals";

    public static PROJECT_TABLE_ORDER = "project.table_col_order";
    public static PROJECT_COLLAPSED_DIRS = "project.collapsed_dirs"; //comma separated names of collapsed directories

    public static WARNING_ADMIN_CHANGE_USER_TYPE = "warnings.administration.change_user_type";
    public static WARNING_ALIGN_VALIDATION_CHANGE_REL = "warnings.alignment_validation.change_relation";
    public static WARNING_CODE_CHANGE_VALIDATION = "warnings.resource_view.code_change_validation";
    public static WARNING_CUSTOM_ROOT = "warnings.ui.tree.cls.customroot";
    public static WARNING_INVOKABLE_REPORTER_WITH_CUSTOM_SERVICE = "warnings.invokable_reporter.reporter_with_custom_service";


    public static getItem(name: string, project?: Project, user?: User): string {
        return localStorage.getItem(LocalStorageManager.getKey(name, project, user));
    }

    public static setItem(name: string, value: any, project?: Project, user?: User) {
        if (value == null) {
            LocalStorageManager.deleteItem(name, project, user);
            return;
        }
        localStorage.setItem(LocalStorageManager.getKey(name, project, user), value);
    }

    /**
     * Removes specified Cookie
     */
    public static deleteItem(name: string, project?: Project, user?: User) {
        localStorage.removeItem(LocalStorageManager.getKey(name, project, user));
    }

    private static getKey(name: string, project?: Project, user?: User) {
        if (project) {
            name += ".P." + project.getName();
        }
        if (user) {
            name += ".U." + user.getIri().getURI();
        }
        return name;
    }

}