import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { VBContext } from 'src/app/utils/VBContext';
import { ModalOptions } from 'src/app/modal-dialogs/Modals';
import { Docset, SepConfiguration, SepExtPoints, SepFMObject, SepFMObjectType } from '../Sep';
import { SepiaServices } from '../sepiaServices';
import { SepSettingsModalComponent } from '../sep-settings/sep-settings-modal.component';

@Component({
  selector: "sep-component",
  templateUrl: "./sep.component.html",
  host: {
    class: "pageComponent",
  },
  standalone: false
})
export class SepComponent implements OnInit {

  isAdmin: boolean;
  configured: boolean;

  selectedDocset: Docset;
  selectedFile: SepFMObject;
  configurations: SepConfiguration[] = [];

  protected readonly SepFMObjectType = SepFMObjectType;

  constructor(
    private sepiaService: SepiaServices,
    private modalService: NgbModal,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.isAdmin = VBContext.getLoggedUser().isAdmin();
    this.initSepiaSettings();
  }

  initSepiaSettings() {
    this.sepiaService.getEndpointUrl().subscribe(
      (endpoint: string) => {
        if (endpoint) {
          this.configured = true;
          // this.sepService.setApiUrl(endpoint);
          this.initConfigurations();
        }
      }
    );
  }

  initConfigurations() {
    this.sepiaService.listConfigurations(undefined, SepExtPoints.FileManager).subscribe(
      configs => {
        this.configurations = configs;
      }
    );
  }

  onDocsetSelected(docset: Docset) {
    if (docset) {
      docset.configuration = this.configurations.find(conf => conf.id == docset.configurationId);
    }
    this.selectedDocset = docset;
    this.selectedFile = null;
  }

  onFileSelected(file: SepFMObject) {
    if (file.type == SepFMObjectType.DIRECTORY) {
      this.sepiaService.findPath(file.id, this.selectedDocset.extensionId, this.selectedDocset.configurationId).subscribe(
        path => {
          file['fullPath'] = path;
          this.selectedFile = null;
          this.cd.detectChanges();
          this.selectedFile = file;
        }
      );
    } else {
      this.selectedFile = file;
    }
  }


  settings() {
    const modalRef: NgbModalRef = this.modalService.open(SepSettingsModalComponent, new ModalOptions());
    modalRef.result.then(
      () => {
        this.initSepiaSettings();
      },
      () => { }
    );
  }

}
