import { Component, EventEmitter, Input, Output, SimpleChanges, ViewChild } from "@angular/core";
import { Docset, SepFMObject } from '../Sep';
import { SepiaServices } from "../sepiaServices";
import { FileListComponent } from './file-list.component';

@Component({
    selector: "file-list-panel",
    templateUrl: "./file-list-panel.component.html",
    host: { class: "vbox" },
    standalone: false
})
export class FileListPanelComponent {

    @ViewChild(FileListComponent) viewChildList: FileListComponent;

    @Output() nodeSelected = new EventEmitter<SepFMObject>();
    @Input() docset: Docset;

    files: SepFMObject[];

    selectedFile: SepFMObject;

    constructor(
        private sepiaServices: SepiaServices
    ) {}

    ngOnChanges(changes: SimpleChanges) {
        if (changes['docset']) {
            this.init();
        }
    }

    init() {
        //set initial status
        this.files = [];
        if (this.docset) {
            this.sepiaServices.findDocset(this.docset.id).subscribe(
                        docset => {
                            this.files = docset.content;
                        }
                    );
        }
    }

    refresh() {
        //this.viewChildList.init();
        this.init();
    }

    onNodeSelected(file: SepFMObject) {
        this.selectedFile = file;
        this.nodeSelected.emit(this.selectedFile);
    }

}
