import { Component, Input } from "@angular/core";
import { SepFMObject, SepFMObjectType } from '../Sep';

@Component({
  selector: "file-details",
  templateUrl: "./file-details.component.html",
  host: { class: "vbox" },
  standalone: false
})
export class FileDetailsComponent {

  @Input() file: SepFMObject;

  protected readonly SepFMObjectType = SepFMObjectType;
}
