import { Component, EventEmitter, Input, Output, SimpleChanges } from "@angular/core";
import { SepFMObject, SepFMObjectType } from '../Sep';

@Component({
  selector: "file-list",
  templateUrl: "./file-list.component.html",
  host: { class: "treeListComponent" },
  standalone: false
})
export class FileListComponent {

  @Output() nodeSelected = new EventEmitter<SepFMObject>();

  @Input() files: SepFMObject[];

  loading: boolean;

  selectedFile: SepFMObject;

  protected readonly SepFMObjectType = SepFMObjectType;

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['files']) {
      this.init();
    }
  }

  init() {
    //set initial status
    this.selectedFile = null;
    for (let f of this.files) {
      if (f.type == SepFMObjectType.DIRECTORY) {
        f.name += "/";
      }
    }
  }


  onNodeSelected(file: SepFMObject) {
    this.selectedFile = file;
    this.nodeSelected.emit(this.selectedFile);
  }

}
