export class SepExtPoints {
  static FileManager = "it.devistar.sepia.filemanager.FileManager";
}

export interface Docset {
  configurationId: string;
  extensionClass: string;
  extensionId: string;
  id: string;
  name: string;
  content: SepFMObject[];
  contentIds?: string[];
  contentSize?: number;
  userId: string;
  //UI fields
  configuration?: SepConfiguration;
}

export interface SepConfiguration {
  id: string;
  name: string;
  userId: string;
  extensionPointClasses: string[];
  extensionId: string;
  configData: any;
}

export interface SepFMObject {
  name: string;
  type: SepFMObjectType;
  path: string;
  contentType: string;
  createdAt: Date;
  id: string;
  score: number;
  size: number;
  updatedAt: Date;
}

export enum SepFMObjectType {
  FILE = "FILE",
  DIRECTORY = "DIRECTORY"
}
