import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Settings } from "../models/Plugins";
import { HttpManager, STRequestOptions, STRequestParams } from "../utils/HttpManager";
import { Docset, SepConfiguration, SepFMObject } from "./Sep";

@Injectable()
export class SepiaServices {

    private serviceName = "Sepia";

    constructor(private httpMgr: HttpManager) { }

    getEndpointUrl(): Observable<string> {
        let params: STRequestParams = {};
        return this.httpMgr.doGet(this.serviceName, "getEndpointUrl", params);
    }

    getSettings(): Observable<Settings> {
        let params: STRequestParams = {};
        return this.httpMgr.doGet(this.serviceName, "getSettings", params).pipe(
            map(stResp => {
                if (stResp) {
                    return Settings.parse(stResp);
                } else {
                    return stResp;
                }
            })
        );
    }

    storeSettings(endpointURL: string): Observable<void> {
        let params: STRequestParams = {
            endpointURL: endpointURL
        };
        return this.httpMgr.doPost(this.serviceName, "storeSettings", params);
    }


    listConfigurations(extensionId?: string, extensionPointClass?: string): Observable<SepConfiguration[]> {
        let params: STRequestParams = {
            extensionId: extensionId,
            extensionPointClass: extensionPointClass
        };
        return this.httpMgr.doGet(this.serviceName, "listConfigurations", params);
    }

    /* DOCSETS */

    listDocsets(): Observable<Docset[]> {
        let params: STRequestParams = {};
        return this.httpMgr.doGet(this.serviceName, "listDocsets", params);
    }

    findDocset(id: string): Observable<Docset> {
        let params: STRequestParams = {
            id: id,
        };
        return this.httpMgr.doGet(this.serviceName, "findDocset", params);
    }

    createDocset(name: string, extensionId: string, configurationId: string, contentIds: string[]): Observable<void> {
        let params: STRequestParams = {
            name: name,
            extensionId: extensionId,
            configurationId: configurationId,
            contentIds: contentIds
        };
        return this.httpMgr.doPost(this.serviceName, "createDocset", params);
    }

    deleteDocset(id: string): Observable<void> {
        let params: STRequestParams = {
            id: id,
        };
        return this.httpMgr.doPost(this.serviceName, "deleteDocset", params);
    }

    updateDocset(docset: Docset): Observable<Docset> {
        let params: STRequestParams = {
            docset: JSON.stringify(docset),
        };
        return this.httpMgr.doPost(this.serviceName, "updateDocset", params);
    }

    /**
     * Checks if the current `DataManager` instance supports `PathFinder` capabilities.
     *
     * @returns {Observable<boolean>} An observable that emits `true` if the `DataManager`
     * supports `PathFinder` capabilities, or `false` otherwise.
     */
    hasPathFinderCapability(): Observable<boolean> {
        let params: STRequestParams = {};
        return this.httpMgr.doGet(this.serviceName, "hasPathFinderCapability", params);
    }

    /* FILE MANAGER */

    listFiles(extensionId: string, configurationId: string, dir?: string): Observable<SepFMObject[]> {
        let params: STRequestParams = {
            extensionId: extensionId,
            configurationId: configurationId,
            dir: dir
        };
        return this.httpMgr.doGet(this.serviceName, "listFiles", params);
    }

    createDir(extensionId: string, configurationId: string, dir: string): Observable<void> {
        let params: STRequestParams = {
            extensionId: extensionId,
            configurationId: configurationId,
            dir: dir,
        };
        return this.httpMgr.doPost(this.serviceName, "createDir", params);
    }

    deleteFileOrDirectory(id: string, extensionId: string, configurationId: string): Observable<void> {
        let params: STRequestParams = {
            id: id,
            extensionId: extensionId,
            configurationId: configurationId,
        };
        return this.httpMgr.doPost(this.serviceName, "deleteFileOrDirectory", params);

    }

    createFile(extensionId: string, configurationId: string, path: string, file: File, overwrite?: boolean): Observable<void> {
        let data: STRequestParams = {
            extensionId: extensionId,
            configurationId: configurationId,
            path: path,
            file: file,
            overwrite: overwrite
        };
        let options: STRequestOptions = new STRequestOptions({
            errorHandlers: [
                { className: 'it.uniroma2.art.semanticturkey.services.core.Sepia.SepiaBackendException', action: 'skip' },
            ]
        });
        return this.httpMgr.uploadFile(this.serviceName, "createFile", data, options);
    }

    search(text: string, extensionId: string, configurationId: string, dir: string): Observable<SepFMObject[]> {
        let params: STRequestParams = {
            text: text,
            extensionId: extensionId,
            configurationId: configurationId,
            dir: dir
        };
        return this.httpMgr.doGet(this.serviceName, "search", params);
    }

    findPath(id: string, extensionId: string, configurationId: string): Observable<string> {
        let params: STRequestParams = {
            id: id,
            extensionId: extensionId,
            configurationId: configurationId,
        };
        return this.httpMgr.doGet(this.serviceName, "findPath", params).pipe(
            map(path => {
                //replace \ with / (probably, according the OS, findPath returns \ as separator on Windows, and / on Mac/Linux)
                path = path.replace(/\\/g, '/');
                //remove the leading "/" (server returns path with leading / (e.g. "/foo"), but listFiles doesn't want the /)
                path = path.substring(1);
                return path;

            })
        );
    }



}