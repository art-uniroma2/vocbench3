import { Pipe, PipeTransform } from '@angular/core';
import { filesize } from "filesize";

@Pipe({
  name: 'filesize',
  standalone: false
})
export class FilesizePipe implements PipeTransform {

  transform(value: number, ..._args: unknown[]): any {
    return filesize(value, { base: 2, standard: "jedec" });
  }

}
