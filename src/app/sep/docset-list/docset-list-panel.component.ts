import { Component, EventEmitter, Output, ViewChild } from "@angular/core";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { TranslationUtils } from '../../utils/TranslationUtils';
import { BasicModalServices } from '../../modal-dialogs/basic-modals/basic-modals.service';
import { CreateDocsetModalComponent } from '../create-docset-modal/create-docset-modal.component';
import { Docset, SepFMObject } from '../Sep';
import { SepBrowserModalComponent } from '../sep-browser/sep-browser-modal.component';
import { DocsetListComponent } from './docset-list.component';
import { SepiaServices } from "../sepiaServices";

@Component({
    selector: "docset-list-panel",
    templateUrl: "./docset-list-panel.component.html",
    host: { class: "vbox" },
    standalone: false
})
export class DocsetListPanelComponent {

    @ViewChild(DocsetListComponent) viewChildList: DocsetListComponent;

    @Output() nodeSelected = new EventEmitter<Docset>();


    selectedDocset: Docset;

    constructor(
        private sepiaService: SepiaServices, 
        private basicModals: BasicModalServices, 
        private modalService: NgbModal, 
        private translateService: TranslateService
    ) {}

    refresh() {
        this.viewChildList.init();
    }

    onNodeSelected(docset: Docset) {
        this.selectedDocset = docset;
        this.nodeSelected.emit(this.selectedDocset);
    }

    createDocset() {
        const modalRef: NgbModalRef = this.modalService.open(CreateDocsetModalComponent, new ModalOptions('lg'));
        modalRef.componentInstance.title = TranslationUtils.getTranslatedText({ key: "Create Docset" }, this.translateService);
        modalRef.result.then(
            () => {
                this.refresh();
            },
            () => {}
        );
    }


    editDocset() {
        if(this.selectedDocset) {
            this.sepiaService.findDocset(this.selectedDocset.id).subscribe(
                docset => {
                    const modalRef: NgbModalRef = this.modalService.open(SepBrowserModalComponent, new ModalOptions('lg'));
                    modalRef.componentInstance.title = TranslationUtils.getTranslatedText({key: docset.name}, this.translateService);
                    modalRef.componentInstance.selectedConfigIdOnInit = docset.configurationId;
                    modalRef.componentInstance.selectedEntriesOnInit = docset.content;
                    modalRef.componentInstance.multiselection = true;
                    modalRef.result.then(
                        (entries: SepFMObject[]) => {
                            docset.contentIds = entries.map(r => r.id);
                            this.sepiaService.updateDocset(docset).subscribe(docset => {
                                //this.files = docset.resources;
                                this.selectedDocset.contentSize = docset.content.length;
                                this.refresh();
                                //this.docsetUpdated.emit(docset);
                            });
                        },
                        () => {
                        }
                    );
                }
            );
        }
    }

    deleteDocset() {
        this.basicModals.confirm({ key: "COMMONS.ACTIONS.DELETE_DOCSET" }, { key: "SEPIA.MESSAGES.DELETE_DOCSET_CONFIRM", params: { docset: this.selectedDocset.name } }, ModalType.warning).then(
            () => {
                this.sepiaService.deleteDocset(this.selectedDocset.id).subscribe(
                    () => {
                        this.refresh();
                    }
                );
            },
            () => { }
        );
    }

}
