import { Component, EventEmitter, Output } from "@angular/core";
import { finalize } from 'rxjs/operators';
import { Docset } from '../Sep';
import { SepiaServices } from "../sepiaServices";

@Component({
    selector: "docset-list",
    templateUrl: "./docset-list.component.html",
    host: { class: "treeListComponent" },
    standalone: false
})
export class DocsetListComponent {

    @Output() nodeSelected = new EventEmitter<Docset>();

    loading: boolean;

    docsets: Docset[];
    selectedDocset: Docset;

    constructor(
        private sepiaService: SepiaServices
    ) { }

    ngOnInit() {
        this.init();
    }

    init() {
        this.docsets = null;
        //this.onNodeSelected(null);
        this.loading = true;
        this.sepiaService.listDocsets()
            .pipe(
                finalize(() => { this.loading = false; })
            ).subscribe(
                (docsets: Docset[]) => {
                    this.docsets = docsets;
                    if (this.selectedDocset) {
                        const docsetToSelect = this.docsets.find(doc => doc.id === this.selectedDocset.id);
                        if (docsetToSelect) {
                            this.onNodeSelected(docsetToSelect);
                        }
                        else this.onNodeSelected(null);
                    } else {
                        this.onNodeSelected(null);
                    }
                }
            );
    }

    onNodeSelected(docset: Docset) {
        this.selectedDocset = docset;
        this.nodeSelected.emit(this.selectedDocset);
    }

}
