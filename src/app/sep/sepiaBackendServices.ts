import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpServiceContext } from '../utils/HttpManager';
import { STError } from '../utils/STResponseUtils';
import { UIUtils } from '../utils/UIUtils';
import { VBContext } from '../utils/VBContext';
import { VBEventHandler } from '../utils/VBEventHandler';
import { BasicModalServices } from '../modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from '../modal-dialogs/Modals';
import { Docset, SepConfiguration, SepFMObject } from './Sep';

@Injectable()
export class SepiaBackendServices {

  /*
  This service class implemented direct API calls to Sepia backend.
  These API invokations are no more used in Sepia dashboard (in favour of SepiaServices invokations, where requests are proxied by ST).
  Anyway, I preferred to keep them here as backup of for future usage
  */

  private apiUrl: string;

  constructor(private httpClient: HttpClient, private router: Router, private basicModals: BasicModalServices, private eventHandler: VBEventHandler) { }

  setApiUrl(apiUrl: string) {
    if (!apiUrl.endsWith("/")) {
      apiUrl += "/";
    }
    this.apiUrl = apiUrl;
  }


  listConfigurations(extensionId?: string, extensionPointClass?: string): Observable<SepConfiguration[]> {
    let params = new HttpParams();
    if (extensionId) params = params.append('extensionId', extensionId);
    if (extensionPointClass) params = params.append('extensionPointClass', extensionPointClass);
    const options = {
      params,
      responseType: 'json' as const // Use 'as const' to let TypeScript knows that you really do mean to use a constant string type
    };
    return this.httpClient.get<SepConfiguration[]>(this.apiUrl + "configurations", options).pipe(
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  /* DOCSETS */

  listDocsets(): Observable<Docset[]> {
    let params: HttpParams = new HttpParams();
    return this.httpClient.get<Docset[]>(this.apiUrl + "docsets", { params: params }).pipe(
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  findDocset(id: string): Observable<Docset> {
    let params: HttpParams = new HttpParams();
    return this.httpClient.get<Docset>(this.apiUrl + "docsets/" + id, { params: params }).pipe(
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  createDocset(name: string, extensionId: string, configurationId: string, contentIds: string[]): Observable<void> {
    let params: HttpParams = new HttpParams();
    let reqBody = {
      name: name,
      extensionId: extensionId,
      configurationId: configurationId,
      contentIds: contentIds,
    };
    return this.httpClient.post<void>(this.apiUrl + "docsets", reqBody, { params: params }).pipe(
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  deleteDocset(id: string): Observable<void> {
    return this.httpClient.delete<void>(this.apiUrl + "docsets/" + id).pipe(
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  updateDocset(docset: Docset): Observable<Docset> {
    return this.httpClient.patch<Docset>(this.apiUrl + "docsets", docset, { responseType: 'json' }).pipe(
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  /**
   * Checks if the current `DataManager` instance supports `PathFinder` capabilities.
   *
   * @returns {Observable<boolean>} An observable that emits `true` if the `DataManager`
   * supports `PathFinder` capabilities, or `false` otherwise.
   */
  hasPathFinderCapability(): Observable<boolean> {
    return this.httpClient.get<boolean>(this.apiUrl + "docsets/hasPathFinderCapability").pipe(
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  /* FILE MANAGER */

  listFiles(extensionId: string, configurationId: string, dir?: string): Observable<SepFMObject[]> {
    let params: HttpParams = new HttpParams();
    params = params.set("extensionId", extensionId);
    params = params.set("configurationId", configurationId);
    if (dir) {
      params = params.set("dir", dir);
    }
    return this.httpClient.get<SepFMObject[]>(this.apiUrl + "file-manager/files", { params: params }).pipe(
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  createDir(extensionId: string, configurationId: string, dir: string): Observable<void> {
    let params = new HttpParams();
    params = params.set("extensionId", extensionId);
    params = params.set("configurationId", configurationId);
    params = params.set("dir", dir);
    return this.httpClient.post<void>(this.apiUrl + "file-manager/files/createDirectory", null, { params: params }).pipe(
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  deleteFileOrDirectory(id: string, extensionId: string, configurationId: string): Observable<void> {
    let params = new HttpParams();
    params = params.set("extensionId", extensionId);
    params = params.set("configurationId", configurationId);
    return this.httpClient.delete<void>(this.apiUrl + "file-manager/files/" + id, { params: params }).pipe(
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  createFile(extensionId: string, configurationId: string, path: string, file: File, overwrite?: boolean): Observable<void> {
    let params = new HttpParams();
    params = params.set("extensionId", extensionId);
    params = params.set("configurationId", configurationId);
    params = params.set("path", path);
    if (overwrite) {
      params = params.set("overwrite", overwrite + "");
    }
    const formData = new FormData();
    formData.append('file', file);
    return this.httpClient.post<void>(this.apiUrl + "file-manager/files", formData, { params: params }).pipe(
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  search(text: string, extensionId: string, configurationId: string, dir: string): Observable<SepFMObject[]> {
    let params = new HttpParams();
    params = params.set("text", text);
    params = params.set("extensionId", extensionId);
    params = params.set("configurationId", configurationId);
    params = params.set("dir", dir);
    return this.httpClient.get<SepFMObject[]>(this.apiUrl + "file-manager/files/search", { params: params }).pipe(
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  findPath(id: string, extensionId: string, configurationId: string): Observable<string> {
    let params = new HttpParams();
    params = params.set("extensionId", extensionId);
    params = params.set("configurationId", configurationId);
    return this.httpClient.get<string>(this.apiUrl + "file-manager/files/" + id + "/path", { params: params, responseType: 'text' as 'json' }).pipe(
      map(path => {
        //replace \ with / (probably, according the OS, findPath returns \ as separator on Windows, and / on Mac/Linux)
        path = path.replace(/\\/g, '/');
        //remove the leading "/" (server returns path with leading / (e.g. "/foo"), but listFiles doesn't want the /)
        path = path.substring(1);
        return path;

      }),
      catchError(error => {
        return this.handleError(error);
      })
    );
  }



  /**
   * Handler for error in requests to ST server. Called in catch clause of get/post requests.
   * This handler returns an Error with a name and message. It is eventually useful in a Component that calls a service that returns
   * an error, so that it can recognize (through the name) the error (Exception) and eventually catch it and show a proper alert.
   * @param err error catched in catch clause (is a Response in case the error is a 401 || 403 response or if the server doesn't respond)
   */
  private handleError(err: HttpErrorResponse) {
    let error: STError = new STError();
    if (err instanceof HttpErrorResponse) { //error thrown by the angular HttpClient get() or post()
      if (err.error instanceof ErrorEvent) { //A client-side or network error occurred
        let errorMsg = "An error occurred:" + err.error.message;
        this.basicModals.alert({ key: "STATUS.ERROR" }, errorMsg, ModalType.error);
        error.name = "Client Error";
        error.message = errorMsg;
      } else { //The backend returned an unsuccessful response code. The response body may contain clues as to what went wrong.
        let errorMsg: string;
        if (!err.ok && err.status == 0 && err.statusText == "Unknown Error") { //attribute of error response in case of no backend response
          errorMsg = "Connection with Sepia server backend (" + this.apiUrl + ") has failed; please check your internet connection";
          this.basicModals.alert({ key: "STATUS.ERROR" }, errorMsg, ModalType.error);
          error.name = "ConnectionError";
          error.message = errorMsg;
        } else { //backend error response
          let status: number = err.status;
          if (status == 401 || status == 403) { //user did a not authorized requests or is not logged in
            if (err.error instanceof ArrayBuffer) {
              errorMsg = String.fromCharCode(...new Uint8Array(err.error));
            } else {
              errorMsg = err.error;
            }
            error.name = "UnauthorizedRequestError";
            error.message = err.message;

            this.basicModals.alert({ key: "STATUS.ERROR" }, errorMsg, ModalType.error).then(
              () => {
                if (err.status == 401) { ////in case user is not logged at all, reset context and redirect to home
                  VBContext.resetContext();
                  HttpServiceContext.resetContext();
                  this.eventHandler.themeChangedEvent.emit();
                  this.router.navigate(['/Sep']);
                }
              },
              () => { }
            );
          } else { //Sepia server error (e.g. out of memory)
            let errorJson: any;
            try {
              errorJson = JSON.parse(err.error);
            } catch {
              errorJson = err.error;
            }
            let errorMsg = (errorJson.message != null ? errorJson.message : "Unknown response from the server") + " (status: " + err.status + ")";
            this.basicModals.alert({ key: "STATUS.ERROR" }, errorMsg, ModalType.error);
            error.name = "ServerError";
            error.message = errorMsg;
          }
        }
      }
    }
    UIUtils.stopAllLoadingDiv();
    return throwError(error);
  }


}
