import { Component, Input } from "@angular/core";
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { ModalOptions } from 'src/app/modal-dialogs/Modals';
import { TranslationUtils } from '../../utils/TranslationUtils';
import { SepConfiguration, SepExtPoints, SepFMObject } from '../Sep';
import { SepBrowserModalComponent } from '../sep-browser/sep-browser-modal.component';
import { SepiaServices } from "../sepiaServices";

@Component({
    selector: "create-docset-modal",
    templateUrl: "./create-docset-modal.component.html",
    standalone: false
})
export class CreateDocsetModalComponent {
    @Input() title: string;

    name: string;
    configurations: SepConfiguration[];
    selectedConfiguration: SepConfiguration;

    content: SepFMObject[];

    constructor(public activeModal: NgbActiveModal, 
        private sepiaServices: SepiaServices,
        private modalService: NgbModal, private translateService: TranslateService
    ) { }

    ngOnInit() {
        this.sepiaServices.listConfigurations(undefined, SepExtPoints.FileManager).subscribe(
            configs => {
                this.configurations = configs;
                if (this.configurations.length > 0) {
                    this.selectedConfiguration = this.configurations[0];
                }
            }
        );
    }

    browseFileManager() {
        const modalRef: NgbModalRef = this.modalService.open(SepBrowserModalComponent, new ModalOptions('lg'));
        modalRef.componentInstance.title = TranslationUtils.getTranslatedText({ key: "File Browser" }, this.translateService);
        modalRef.componentInstance.selectedConfigIdOnInit = this.selectedConfiguration.id;
        modalRef.componentInstance.selectedEntriesOnInit = this.content;
        modalRef.componentInstance.multiselection = true;
        modalRef.result.then(
            (entries: SepFMObject[]) => {
                this.content = entries;
            },
            () => { }
        );
    }


    ok() {
        this.sepiaServices.createDocset(this.name, this.selectedConfiguration.extensionId, this.selectedConfiguration.id, this.content.map(r => r.id)).subscribe(
            () => {
                this.activeModal.close();
            }
        );

    }

    cancel() {
        this.activeModal.dismiss();
    }

    onConfigurationChanged() {
        this.content = [];
    }
}
