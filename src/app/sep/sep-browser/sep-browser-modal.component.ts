import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SepFMObject } from '../Sep';

@Component({
    selector: "sep-browser-modal",
    templateUrl: "./sep-browser-modal.component.html",
    standalone: false
})
export class SepBrowserModalComponent {
    @Input() title: string;
    @Input() selectedConfigIdOnInit: string;
    @Input() selectedEntriesOnInit: SepFMObject[];
    @Input() multiselection: boolean;
    @Input() readonly: boolean;
    @Input() rootPath: string = '';
    @Input() showDetails: boolean = true;

    selectedEntries: SepFMObject[] = []; //id of the checked entries


    constructor(public activeModal: NgbActiveModal) {
    }


    ok() {
        this.activeModal.close(this.selectedEntries);
    }

    cancel() {
        this.activeModal.dismiss();
    }

    onEntriesChanged(entries: SepFMObject[]) {
        this.selectedEntries = entries;

    }
}
