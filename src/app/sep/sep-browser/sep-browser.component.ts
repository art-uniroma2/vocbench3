import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from "@ngx-translate/core";
import { forkJoin, from, Observable, of } from 'rxjs';
import { catchError, finalize, map, mergeMap } from 'rxjs/operators';
import { BasicModalServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { TranslationUtils } from '../../utils/TranslationUtils';
import { SepConfiguration, SepExtPoints, SepFMObject, SepFMObjectType } from '../Sep';
import { SepiaServices } from "../sepiaServices";
import { SepBrowserSearchResultsModalComponent } from "./sep-browser-search-results-modal.component";
import { STError } from "src/app/utils/STResponseUtils";

@Component({
    selector: "sep-browser",
    templateUrl: "./sep-browser.component.html",
    standalone: false
})
export class SepBrowserComponent {
    @Input() title: string;
    @Input() selectedConfigIdOnInit: string;
    @Input() selectedEntriesOnInit: SepFMObject[];
    @Input() multiselection: boolean;
    @Input() readonly: boolean;
    @Input() rootPath: string = '';
    @Input() showDetails: boolean = true;
    @Output() entriesChanged: EventEmitter<SepFMObject[]> = new EventEmitter<SepFMObject[]>();

    @ViewChild('searchInput') searchInput: ElementRef;

    configs: SepConfiguration[];
    selectedConfig: SepConfiguration;

    path: string = "";

    dirEntries: SepFMObject[];
    selectedEntry: SepFMObject;
    selectedEntryPath: SepFMObject;

    selectedEntries: SepFMObject[] = []; //id of the checked entries

    searchString: string;
    loading: boolean;


    constructor(
        private modalService: NgbModal, 
        private sepiaService: SepiaServices, 
        private basicModals: BasicModalServices, 
        private translateService: TranslateService
    ) {}

    ngOnInit() {
        if (this.readonly) this.multiselection = false;
        if (this.rootPath) this.path = this.rootPath;

        this.selectedEntries = [...(this.selectedEntriesOnInit || [])];

        if (this.selectedEntriesOnInit) {
            this.selectedEntries = [...this.selectedEntriesOnInit];
            this.emitUpdateEntries();
        }

        this.sepiaService.listConfigurations(undefined, SepExtPoints.FileManager).subscribe(
            configs => {
                this.configs = configs;
                if (this.selectedConfigIdOnInit) {
                    this.selectedConfig = this.configs.find(c => c.id == this.selectedConfigIdOnInit);
                }
                if (this.selectedConfig == null) {
                    this.selectedConfig = this.configs[0];
                }
                this.onConfigChanged();
            }
        );

    }

    emitUpdateEntries() {
        let returnData: SepFMObject[];
        if (this.multiselection) {
            returnData = this.selectedEntries;
        } else {
            if (this.selectedEntry != null) {
                returnData = [this.selectedEntry];
            } else {
                returnData = [];
            }

        }
        this.entriesChanged.emit(returnData);
    }

    onConfigChanged() {
        this.listEntries().subscribe();
    }

    levelUp() {
        this.path = this.path.substring(0, this.path.lastIndexOf("/") + 1);
        if (this.path != "") {
            this.path = this.path.substring(0, this.path.length - 1);
        }
        this.listEntries().subscribe();
    }

    listEntries(): Observable<void> {
        this.loading = true;
        return this.sepiaService.listFiles(this.selectedConfig.extensionId, this.selectedConfig.id, this.path).pipe(
            map(entries => {
                this.selectedEntry = null;
                entries.sort((e1, e2) => {
                    if (e1.type === e2.type) {
                        return e1.name.toLocaleLowerCase().localeCompare(e2.name.toLocaleLowerCase());
                    } else if (e1.type === SepFMObjectType.DIRECTORY) {
                        return -1;
                    } else {
                        return 1;
                    }
                });

                this.dirEntries = entries.map(d => {
                    let fullPath: string = this.path;
                    if (fullPath !== "" && !fullPath.endsWith("/")) {
                        fullPath += "/";
                    }
                    fullPath += d.name;
                    if (d.type === SepFMObjectType.DIRECTORY) {
                        fullPath += "/";
                    }
                    d['fullPath'] = fullPath;
                    return d;
                });
                this.computeImplicitlyChecked();
            }),
            finalize(() => {
                this.loading = false;
            })
        );

    }

    // Calculates which files must already be pre-selected
    computeImplicitlyChecked() {
        //Calculates missing fullPaths for selectedEntries
        const getFullPath$ = (entry) => {
            if (!entry['fullPath']) {
                const match = this.dirEntries.find(dirEntry => dirEntry.id == entry.id);
                if (match) {
                    entry['fullPath'] = match['fullPath'];
                } else {
                    this.sepiaService.findPath(entry.id, this.selectedConfig.extensionId, this.selectedConfig.id).subscribe();
                    return this.sepiaService.findPath(entry.id, this.selectedConfig.extensionId, this.selectedConfig.id).pipe(
                        map(path => {
                            entry['fullPath'] = entry.type === SepFMObjectType.DIRECTORY ? path + "/" : path;
                            return entry;
                        })
                    );
                }
            }
            return of(entry);

        };

        forkJoin(this.selectedEntries.map(selEntry => getFullPath$(selEntry)))
            .subscribe(() => {
                // After obtaining all fullPaths, run the implicitlyChecked check on dirEntries
                this.dirEntries.forEach(d => {
                    d['implicitlyChecked'] = this.selectedEntries.some(selEntry =>
                        d['fullPath'].startsWith(selEntry['fullPath']) && d['fullPath'] != selEntry['fullPath']
                    );
                });
            });
    }


    selectEntry(entry: SepFMObject) {
        this.selectedEntry = entry;
    }

    doubleClickEntry(entry: SepFMObject) {
        if (entry.type == SepFMObjectType.DIRECTORY) {
            if (this.path != "") {
                this.path += "/";
            }
            this.path += entry.name;
            this.listEntries().subscribe();
        }
    }

    preventSelection(event: MouseEvent) {
        if (event.detail > 1) { // Check for double click
            event.preventDefault();
        }
    }

    onEntryChecked(entry: SepFMObject) {
        let idx = this.selectedEntries.findIndex(e => e.id == entry.id);
        if (idx != -1) {
            this.removeCheckedEntry(this.selectedEntries[idx]);
        } else {
            this.selectedEntries.push(entry);
            for (let index = this.selectedEntries.length - 1; index >= 0; index--) {
                const value = this.selectedEntries[index];
                if (value['fullPath'].startsWith(entry['fullPath']) && value['fullPath'] !== entry['fullPath']) {
                    this.removeCheckedEntry(value);
                }
            }
        }
        this.emitUpdateEntries();
    }


    removeCheckedEntry(entry: SepFMObject) {
        this.selectedEntries.splice(this.selectedEntries.findIndex(e => e.id == entry.id), 1);
        this.computeImplicitlyChecked();
        if (entry == this.selectedEntryPath) {
            this.selectedEntryPath = null;
        }
    }

    isEntryChecked(entry: SepFMObject) {
        return this.selectedEntries.some(e => e.id == entry.id);
    }


    deleteEntry() {
        this.sepiaService.hasPathFinderCapability().subscribe(
            result => {
                const messageKey = result ? "SEPIA.MESSAGES.DELETE_FILE_CONFIRM" : "SEPIA.MESSAGES.DELETE_FILE_CONFIRM_WITHOUT_PATH_FINDER";
                this.basicModals.confirm({key: "COMMONS.ACTIONS.DELETE_FILE"}, {
                    key: messageKey,
                    params: {type: this.selectedEntry.type.toLowerCase(), name: this.selectedEntry.name}
                }, ModalType.warning).then(
                    () => {
                        this.sepiaService.deleteFileOrDirectory(this.selectedEntry.id, this.selectedConfig.extensionId, this.selectedConfig.id).subscribe(
                            () => {
                                this.removeCheckedEntry(this.selectedEntry);
                                this.listEntries().subscribe();
                            }
                        );
                    },
                    () => {}
                );
            });
    }

    createDir() {
        this.basicModals.prompt("Create folder", {value: "Name"}, null, null, null, true).then(
            name => {
                let newDirPath = this.path;
                if (newDirPath != "" && !newDirPath.endsWith("/")) {
                    newDirPath += "/";
                }
                newDirPath += name;
                this.sepiaService.createDir(this.selectedConfig.extensionId, this.selectedConfig.id, newDirPath).subscribe(
                    () => {
                        this.listEntries().subscribe();
                    }
                );
            }
        );
    }

    uploadFile() {
        this.basicModals.selectFile("Upload file").then(
            (file: File) => {
                let newFilePath = this.path;
                let sanitizedFileName = file.name.replace(new RegExp(" ", 'g'), "_");
                if (newFilePath != "" && !newFilePath.endsWith("/")) {
                    newFilePath += "/";
                }
                newFilePath += sanitizedFileName;
                this.createFile(file, newFilePath).subscribe(
                    (done: boolean) => {
                        if (done) {
                            this.listEntries().subscribe();
                        }
                    }
                );
            },
            () => {
            }
        );
    }

    private createFile(file: File, path: string, overwrite: boolean = false): Observable<boolean> {
        return this.sepiaService.createFile(this.selectedConfig.extensionId, this.selectedConfig.id, path, file, overwrite).pipe(
            map(() => {
                return true;
            }),
            catchError((err: STError) => {
                if (err.name.endsWith("SepiaBackendException") && err.additionalInfo?.sepiaError?.pluginErrorCode?.endsWith("FileAlreadyExistsException")) {
                    return from(
                        this.basicModals.confirm({key: "WIDGETS.STORAGE_MGR.UPLOAD_FILE"}, {key: "MESSAGES.ALREADY_EXISTING_FILE_OVERWRITE_CONFIRM"}, ModalType.warning).then(
                            () => {
                                return this.sepiaService.createFile(this.selectedConfig.extensionId, this.selectedConfig.id, path, file, true).pipe(
                                    map(() => {
                                        return true;
                                    })
                                );
                            },
                            () => {
                                return of(false);
                            }
                        )
                    ).pipe(
                        mergeMap(done => done)
                    );
                } else {
                    return of(false);
                }
            })
        );
    }

    onKeydown(event: KeyboardEvent) {
        if (event.key == "Enter") {
            if (this.searchString && this.searchString.trim() != "") {
                this.search();
            }
        }
    }

    search() {
        this.searchInput.nativeElement.blur();
        this.sepiaService.search(this.searchString, this.selectedConfig.extensionId, this.selectedConfig.id, this.path).subscribe(
            results => {
                if (results.length == 0) {
                    this.basicModals.alert({key: "SEARCH.SEARCH"}, {key: "MESSAGES.NO_RESULTS_FOUND"}, ModalType.warning);
                } else {
                    if (results.length == 1) {
                        //only 1 result, find its path and select the entry
                        this.findAndSelectSearchResult(results[0]);
                    } else {
                        const modalRef: NgbModalRef = this.modalService.open(SepBrowserSearchResultsModalComponent, new ModalOptions('lg'));
                        modalRef.componentInstance.title = TranslationUtils.getTranslatedText({key: "SEARCH.SEARCH"}, this.translateService);
                        modalRef.componentInstance.entries = results;
                        modalRef.componentInstance.config = this.selectedConfig;
                        modalRef.result.then(
                            (selectedResult: SepFMObject) => {
                                this.findAndSelectSearchResult(selectedResult);
                            },
                            () => {
                            }
                        );
                    }
                }
            }
        );
    }

    private findAndSelectSearchResult(selectedResult: SepFMObject) {
        this.sepiaService.findPath(selectedResult.id, this.selectedConfig.extensionId, this.selectedConfig.id).subscribe(
            path => {
                this.path = path.substring(0, path.lastIndexOf("/"));
                this.listEntries().subscribe(
                    () => {
                        let e = this.dirEntries.find(e => e.id == selectedResult.id);
                        this.selectEntry(e);
                    }
                );
            }
        );
    }


    selectDocsetEntry(entry: SepFMObject) {
        this.selectedEntryPath = entry;
    }

}