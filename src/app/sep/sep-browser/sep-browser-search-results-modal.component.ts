import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SepConfiguration, SepFMObject } from '../Sep';
import { SepiaServices } from "../sepiaServices";

@Component({
    selector: "sep-browser-search-results-modal",
    templateUrl: "./sep-browser-search-results-modal.component.html",
    standalone: false
})
export class SepBrowserSearchResultsModalComponent {

    @Input() title: string;
    @Input() entries: SepFMObject[];
    @Input() config: SepConfiguration;


    selectedEntry: SepFMObject;
    entryPath: string;


    constructor(
        public activeModal: NgbActiveModal, 
        private sepiaService: SepiaServices, 
    ) {}

    selectEntry(entry: SepFMObject) {
        this.selectedEntry = entry;
        this.sepiaService.findPath(entry.id, this.config.extensionId, this.config.id).subscribe(
            path => {
                this.entryPath = path;
                if (this.entryPath == "") {
                    this.entryPath += "/";
                }
            }
        );
    }

    ok() {
        this.activeModal.close(this.selectedEntry);

    }

    cancel() {
        this.activeModal.dismiss();
    }

}
