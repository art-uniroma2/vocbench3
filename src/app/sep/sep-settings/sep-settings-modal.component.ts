import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BasicModalServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { SepiaServices } from "../sepiaServices";
import { ResourceUtils } from "src/app/utils/ResourceUtils";
import { ModalType } from "src/app/modal-dialogs/Modals";

@Component({
    selector: "sep-settings-modal",
    templateUrl: "./sep-settings-modal.component.html",
    standalone: false
})
export class SepSettingsModalComponent {

    endpointURL: string;

    constructor(
        public activeModal: NgbActiveModal,
        private sepiaServices: SepiaServices,
        private basicModals: BasicModalServices
    ) { }

    ngOnInit() {
        this.sepiaServices.getSettings().subscribe(
            settings => {
                if (settings) {
                    this.endpointURL = settings.getPropertyValue("endpointURL");
                }
            }
        );
    }

    isOkEnabled() {
        return this.endpointURL && this.endpointURL.trim() != "";
    }

    ok() {
        if (!ResourceUtils.testIRI(this.endpointURL)) {
            this.basicModals.alert({ key: "STATUS.INVALID_VALUE" }, { key: "MESSAGES.INVALID_URL", params: { iri: this.endpointURL } }, ModalType.warning);
            return;
        }
        this.sepiaServices.storeSettings(this.endpointURL).subscribe(
            () => {
                this.activeModal.close();
            }
        );
    }

    cancel() {
        this.activeModal.dismiss();
    }

}
