import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from '../modules/sharedModule';
import { authGuard } from '../utils/CanActivateGuards';
import { CreateDocsetModalComponent } from './create-docset-modal/create-docset-modal.component';
import { DocsetListPanelComponent } from './docset-list/docset-list-panel.component';
import { DocsetListComponent } from './docset-list/docset-list.component';
import { FileDetailsComponent } from './file-list/file-details.component';
import { FileListPanelComponent } from './file-list/file-list-panel.component';
import { FileListComponent } from './file-list/file-list.component';
import { FilesizePipe } from './pipes/filesize.pipe';
import { SepBrowserModalComponent } from './sep-browser/sep-browser-modal.component';
import { SepBrowserSearchResultsModalComponent } from './sep-browser/sep-browser-search-results-modal.component';
import { SepBrowserComponent } from './sep-browser/sep-browser.component';
import { SepComponent } from './sep-component/sep.component';
import { SepSettingsModalComponent } from './sep-settings/sep-settings-modal.component';

export const routes: Routes = [
  { path: "", component: SepComponent, canActivate: [authGuard] },
];
export const sepRouting = RouterModule.forChild(routes);


@NgModule({
  imports: [
    sepRouting,
    CommonModule,
    FormsModule,
    NgbDropdownModule,
    SharedModule,
    TranslateModule
  ],
  declarations: [
    CreateDocsetModalComponent,
    DocsetListComponent,
    DocsetListPanelComponent,
    FileListComponent,
    FileListPanelComponent,
    FileDetailsComponent,
    SepBrowserModalComponent,
    SepBrowserComponent,
    SepBrowserSearchResultsModalComponent,
    SepComponent,
    SepSettingsModalComponent,
    FilesizePipe
  ],
  exports: [
    SepComponent
  ],
  providers: []
})
export class SepModule { }
