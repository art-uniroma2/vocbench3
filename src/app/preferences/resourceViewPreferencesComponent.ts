import { Component } from "@angular/core";
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { ConfigurationComponents } from "../models/Configuration";
import { ExtensionPointID, Scope, STProperties } from "../models/Plugins";
import { ResourceViewPreference, ResourceViewType, ResViewTemplate, SectionFilterPreference, SettingsEnum } from "../models/Properties";
import { OntoLex, SKOS } from "../models/Vocabulary";
import { ResViewSectionFilterDefaultsModalComponent } from "../resource-view/res-view-settings/res-view-section-filter-defaults-modal.component";
import { SettingsServices } from "../services/settings.service";
import { VBContext } from "../utils/VBContext";
import { VBEventHandler } from "../utils/VBEventHandler";
import { VBProperties } from "../utils/VBProperties";
import { BasicModalServices } from "../modal-dialogs/basic-modals/basic-modals.service";
import { ModalOptions, ModalType } from "../modal-dialogs/Modals";
import { LoadConfigurationModalReturnData } from "../modal-dialogs/shared-modals/configuration-store-modal/load-configuration-modal.component";
import { SharedModalServices } from "../modal-dialogs/shared-modals/shared-modals.service";

@Component({
  selector: "res-view-pref",
  templateUrl: "./resourceViewPreferencesComponent.html",
  standalone: false
})
export class ResourceViewPreferencesComponent {

  resViewTabSync: boolean;

  rvConceptTypeSelectorAvailable: boolean;
  rvConceptTypes: ResViewTypeOpt[] = [
    { type: ResourceViewType.resourceView, showTranslationKey: "RESOURCE_VIEW.TYPES.DETAILED", descrTranslationKey: "RESOURCE_VIEW.TYPES.DETAILED_DESCR" },
    { type: ResourceViewType.termView, showTranslationKey: "RESOURCE_VIEW.TYPES.TERMINOLOGIST", descrTranslationKey: "RESOURCE_VIEW.TYPES.TERMINOLOGIST_DESCR" }
  ];
  selectedRvConceptType: ResViewTypeOpt;

  rvLexEntryTypeSelectorAvailable: boolean;
  rvLexEntryTypes: ResViewTypeOpt[] = [
    { type: ResourceViewType.resourceView, showTranslationKey: "RESOURCE_VIEW.TYPES.DETAILED", descrTranslationKey: "RESOURCE_VIEW.TYPES.DETAILED_DESCR" },
    { type: ResourceViewType.lexicographerView, showTranslationKey: "RESOURCE_VIEW.TYPES.LEXICOGRAPHER", descrTranslationKey: "RESOURCE_VIEW.TYPES.LEXICOGRAPHER_DESCR" }
  ];
  selectedRvLexEntryType: ResViewTypeOpt;

  displayImg: boolean;
  showDeprecated: boolean;
  showDatatypeBadge: boolean;
  sortByRendering: boolean;
  forceSearchModeForAddPropertyValue: boolean;


  sectionsFilter: SectionFilterPreference;
  template: ResViewTemplate; //template on which base the sections filter

  constructor(
    private vbProp: VBProperties,
    private settingsService: SettingsServices,
    private basicModals: BasicModalServices,
    private sharedModals: SharedModalServices,
    private modalService: NgbModal,
    private eventHandler: VBEventHandler
  ) { }

  ngOnInit() {
    let rvPrefs: ResourceViewPreference = VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPreferences;
    this.resViewTabSync = rvPrefs.syncTabs;
    this.displayImg = rvPrefs.displayImg;
    this.showDeprecated = rvPrefs.showDeprecated;
    this.showDatatypeBadge = rvPrefs.showDatatypeBadge;
    this.sortByRendering = rvPrefs.sortByRendering;
    this.forceSearchModeForAddPropertyValue = rvPrefs.forceSearchModeForAddPropertyValue;

    this.selectedRvConceptType = this.rvConceptTypes.find(t => t.type == rvPrefs.defaultConceptType);
    this.selectedRvLexEntryType = this.rvLexEntryTypes.find(t => t.type == rvPrefs.defaultLexEntryType);
    //selection of RV type for concept available only in skos and ontolex projects
    this.rvConceptTypeSelectorAvailable = VBContext.getWorkingProject().getModelType() == SKOS.uri;
    //selection of RV type for lex entry available only in ontolex projects
    this.rvLexEntryTypeSelectorAvailable = VBContext.getWorkingProject().getModelType() == OntoLex.uri;

    this.initSectionsFilter();
  }

  onResViewConceptTypeChange() {
    let rvPrefs: ResourceViewPreference = VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPreferences;
    rvPrefs.defaultConceptType = this.selectedRvConceptType.type;
    this.vbProp.setResourceViewPreferences(rvPrefs).subscribe();
  }

  onResViewLexEntryTypeChange() {
    let rvPrefs: ResourceViewPreference = VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPreferences;
    rvPrefs.defaultLexEntryType = this.selectedRvLexEntryType.type;
    this.vbProp.setResourceViewPreferences(rvPrefs).subscribe();
  }

  onTabSyncChange() {
    this.vbProp.setResourceViewTabSync(this.resViewTabSync);
    this.eventHandler.resViewTabSyncChangedEvent.emit(this.resViewTabSync);
  }

  onDisplayImgChange() {
    this.vbProp.setResourceViewDisplayImg(this.displayImg);
  }

  onShowDeprecatedChange() {
    this.vbProp.setShowDeprecatedInResView(this.showDeprecated);
  }

  onShowDatatypeBadge() {
    let pref = VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPreferences;
    pref.showDatatypeBadge = this.showDatatypeBadge;
    this.vbProp.setResourceViewPreferences(pref).subscribe();
  }

  onSortByRenderingChange() {
    this.vbProp.setSortByRendering(this.sortByRendering);
  }

  onForceSearchModeForAddPropertyValueChange() {
    this.vbProp.setForceSearchModeForAddPropertyValue(this.forceSearchModeForAddPropertyValue);
  }



  /**
   * SECTION FILTER - START
   */

  private initSectionsFilter() {
    this.template = VBContext.getWorkingProjectCtx().getProjectSettings().resourceView.templates;
    this.sectionsFilter = VBContext.getWorkingProjectCtx().getProjectPreferences().resViewPartitionFilter;
  }

  loadSectionFilter() {
    this.sharedModals.loadConfiguration({ key: "COMMONS.ACTIONS.LOAD_TEMPLATE" }, ConfigurationComponents.TEMPLATE_STORE, true, false).then(
      (conf: LoadConfigurationModalReturnData) => {
        let templateProp: STProperties = conf.configuration.properties.find(p => p.name == "template");
        if (templateProp != null) {
          this.sectionsFilter = templateProp.value;
          this.updateSectionsFilter();
        }
      }
    );
  }

  storeSectionFilter() {
    let config: { [key: string]: any } = {
      template: this.sectionsFilter
    };
    this.sharedModals.storeConfiguration({ key: "COMMONS.ACTIONS.SAVE_TEMPLATE" }, ConfigurationComponents.TEMPLATE_STORE, config);
  }

  setUserDefault() {
    this.basicModals.confirm({ key: "COMMONS.ACTIONS.SET_AS_DEFAULT" }, { key: "MESSAGES.SET_DEFAULT_TEMPLATE_FOR_ALL_PROJ_CONFIRM" }, ModalType.warning).then(
      () => {
        this.settingsService.storeSettingDefault(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_USER, Scope.USER, SettingsEnum.resViewPartitionFilter, this.sectionsFilter).subscribe();
      },
      () => { }
    );

  }

  /**
   * Reset the preference to the default, namely remove set the PUSettings, so if there is a default it is retrieved through the fallback
   */
  restoreDefault() {
    const modalRef: NgbModalRef = this.modalService.open(ResViewSectionFilterDefaultsModalComponent, new ModalOptions("lg"));
    modalRef.result.then(
      () => {
        this.vbProp.refreshResourceViewSectionFilter().subscribe(
          () => {
            this.initSectionsFilter();
          }
        );
      },
      () => { }
    );
  }

  updateSectionsFilter() {
    this.vbProp.setResViewSectionFilter(this.sectionsFilter).subscribe();
  }

  /**
   * SECTION FILTER - END
   */

}

interface ResViewTypeOpt {
  type: ResourceViewType;
  showTranslationKey: string;
  descrTranslationKey: string;
}