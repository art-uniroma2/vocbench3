import { Component, ElementRef, forwardRef, Input, ViewChild } from "@angular/core";
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { NgbModal, NgbModalRef } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { forkJoin, Observable, of } from "rxjs";
import { ARTResource, ARTURIResource, ResAttribute } from "src/app/models/ARTResources";
import { PrefixMapping } from "src/app/models/Metadata";
import { Project } from "src/app/models/Project";
import { PredLabelMapping } from "src/app/models/Properties";
import { PropertyServices } from "src/app/services/properties.service";
import { ResourcesServices } from "src/app/services/resources.service";
import { STRequestOptions } from "src/app/utils/HttpManager";
import { ResourceUtils } from "src/app/utils/ResourceUtils";
import { ProjectContext } from "src/app/utils/VBContext";
import { VBProperties } from "src/app/utils/VBProperties";
import { BasicModalServices } from "src/app/modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "src/app/modal-dialogs/browsing-modals/browsing-modals.service";
import { ModalOptions, ModalType } from "src/app/modal-dialogs/Modals";
import { SelectVocabulariesModalComponent } from "./select-vocabularies-modal.component";


@Component({
  selector: "pred-label-settings",
  templateUrl: "./predicate-label-settings.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => PredicateLabelSettingsComponent), multi: true,
  }],
  standalone: false
})
export class PredicateLabelSettingsComponent {

  @Input() project: Project;

  private lastBrowsedProjectCtx: ProjectContext;

  translateLangs: string[];

  langs: string[]; //langs for which exists a mapping
  additionalLangs: string[];

  activeTabLang: string;

  langMappings: LangMappingsTab[] = [];
  activeMappingTab: LangMappingsTab;

  properties: ARTURIResource[];

  prefixNsMapping: PrefixMapping[];

  @ViewChild('mappingsContainer') mappingsContainerEl: ElementRef;
  newMapping: { pred: string; label: string };

  constructor(private propertiesService: PropertyServices, private resourcesService: ResourcesServices, private vbProp: VBProperties,
    private basicModals: BasicModalServices, private browsingModals: BrowsingModalServices,
    private modalService: NgbModal, private translate: TranslateService) { }

  ngOnInit() {
    this.translateLangs = this.translate.getLangs();
    this.additionalLangs = this.translateLangs;
  }

  init(setting: { [lang: string]: PredLabelMapping }) {
    this.fromSettingToStruct(setting);
    this.updateLangs();
    if (this.langs.length > 0) {
      this.activateTabLang(this.langs[0]);
    }
    this.annotatePredicates();
  }

  importProperties() {
    const modalRef: NgbModalRef = this.modalService.open(SelectVocabulariesModalComponent, new ModalOptions());
    modalRef.componentInstance.project = this.project;
    modalRef.result.then(
      (graphs: ARTURIResource[]) => {
        this.propertiesService.getFlatProperties(graphs, STRequestOptions.getRequestOptions(new ProjectContext(this.project))).subscribe(
          props => {
            this.properties = props;

            let labelMappings = this.properties.map(p => {
              return {
                annotatedPred: p,
                predIri: p.getURI(),
                label: null,
                visible: true
              };
            });
            if (this.activeMappingTab.labelMappings.length == 0) {
              this.activeMappingTab.labelMappings = labelMappings;
              this.sortMappings(this.activeMappingTab.labelMappings);
            } else {
              labelMappings.forEach(m => {
                if (!this.activeMappingTab.labelMappings.some(mapping => mapping.predIri == m.predIri)) {
                  this.activeMappingTab.labelMappings.push(m);
                }
              });
              this.sortMappings(this.activeMappingTab.labelMappings);
            }
          }
        );
      },
      () => { }
    );
  }

  bootstrapMappings() {
    let predList: ARTURIResource[] = this.activeMappingTab.labelMappings.filter(m => m.label == null).map(m => m.annotatedPred);
    this.propertiesService.getPropertiesLexicalizations(predList, this.activeTabLang, this.activeMappingTab.includeSimpleString, STRequestOptions.getRequestOptions(new ProjectContext(this.project))).subscribe(
      (predLabelMap: { [key: string]: string }) => {
        for (let pred in predLabelMap) {
          let label = predLabelMap[pred];
          if (label != "") { //set the label only if available and not yet set
            let mapping = this.activeMappingTab.labelMappings.find(m => m.predIri == pred);
            if (mapping.label == null) {
              mapping.label = label;
            }
          }
        }
        this.onModelChange();
      }
    );
  }

  clearMapping(mapping: PredLabelMappingEntry) {
    mapping.label = null;
    this.onModelChange();
  }

  private updateLangs() {
    this.langs = this.langMappings.map(m => m.lang);
    this.additionalLangs = this.translateLangs.filter(l => !this.langs.includes(l));
  }

  applyFilters() {
    this.activeMappingTab.labelMappings.forEach(m => {
      if (this.activeMappingTab.filterString && this.activeMappingTab.filterString.trim() != "") {
        let qname = m.annotatedPred.getAdditionalProperty(ResAttribute.QNAME);
        m.visible = m.predIri.toLocaleLowerCase().includes(this.activeMappingTab.filterString.toLocaleLowerCase()) || (qname && qname.toLocaleLowerCase().includes(this.activeMappingTab.filterString.toLocaleLowerCase()));
      } else {
        m.visible = true;
      }
    });
  }

  addMapping() {
    this.newMapping = { pred: "", label: "" };
    this.mappingsContainerEl.nativeElement.scrollTop = 0; //scroll to the top where the new mapping row is located
  }

  cancelNewMapping() {
    this.newMapping = null;
  }

  confirmNewMapping() {
    //check if IRI is valid
    if (!ResourceUtils.testIRI(this.newMapping.pred)) {
      this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "MESSAGES.INVALID_IRI", params: { iri: this.newMapping.pred } }, ModalType.warning);
      return;
    }
    //check if label is valid
    if (!this.newMapping.label || this.newMapping.label.trim() == "") {
      this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "MESSAGES.MISSING_LABEL" }, ModalType.warning);
      return;
    }
    //check if mapping already exists
    if (this.activeMappingTab.labelMappings.some(m => m.predIri == this.newMapping.pred)) {
      this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "PREFERENCES.PRED_LABEL_MAPPING.ALREADY_DEFINED_MAPPING" }, ModalType.warning);
      return;
    }
    this.activeMappingTab.labelMappings.push({
      annotatedPred: null,
      predIri: this.newMapping.pred,
      label: this.newMapping.label,
      visible: true
    });
    this.sortMappings(this.activeMappingTab.labelMappings);
    this.newMapping = null;
    this.onModelChange();
    this.annotatePredicates();
  }

  pickProperty() {
    this.prepareProjectBrowse().subscribe(
      () => {
        this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }, null, null, null, this.lastBrowsedProjectCtx).then(
          prop => {
            this.newMapping.pred = prop.getURI();
          }
        );
      }
    );
  }

  private prepareProjectBrowse(): Observable<any> {
    if (this.lastBrowsedProjectCtx != null && this.lastBrowsedProjectCtx.getProject().getName() == this.project.getName()) {
      //project context was already initialized => do not initialize it again
      return of(null);
    } else { //project context never initialized or initialized for a different project
      this.lastBrowsedProjectCtx = new ProjectContext(this.project);
      let initProjectCtxFn: Observable<void>[] = [
        this.vbProp.initProjectUserBindings(this.lastBrowsedProjectCtx),
        this.vbProp.initUserProjectPreferences(this.lastBrowsedProjectCtx),
        this.vbProp.initProjectSettings(this.lastBrowsedProjectCtx)
      ];
      return forkJoin(initProjectCtxFn);
    }
  }

  /* =====
  TABS
  * ====== */

  activateTabLang(lang: string) {
    this.activeTabLang = lang;
    // this.activeMappingTab.labelMappings = this.langMappings.find(m => m.lang == lang).labelMappings;
    this.activeMappingTab = this.langMappings.find(m => m.lang == lang);
  }

  closeTab(lang: string, event: Event) {
    this.basicModals.confirm({ key: "STATUS.WARNING" }, { key: "PREFERENCES.PRED_LABEL_MAPPING.DELETE_MAPPINGS_WARN", params: { language: lang } }, ModalType.warning).then(
      () => {
        event.preventDefault(); //prevent the refresh of the page
        //remove the tab to close
        this.langMappings = this.langMappings.filter(m => m.lang != lang);
        //update active tab
        if (this.langMappings.length > 0) {
          this.activateTabLang(this.langMappings[0].lang);
        } else {
          this.activeTabLang = null;
          this.activeMappingTab = null;
        }
        this.updateLangs();
        this.onModelChange();
      },
      () => { }
    );
  }

  addLangTab(lang: string) {
    let labelMappings = [];
    //if a mapping for another lang is present, initialize this new one with the same properties
    if (this.langMappings.length > 0) {
      labelMappings = this.langMappings[0].labelMappings.map(m => {
        return {
          annotatedPred: m.annotatedPred,
          predIri: m.predIri,
          label: null,
          visible: true
        };
      });
    }
    this.langMappings.push({
      lang: lang,
      labelMappings: labelMappings,
      includeSimpleString: lang == "en"
    });
    this.updateLangs();
    this.activateTabLang(lang);
  }

  //================

  private annotatePredicates() {
    if (this.project == null) return;

    let toAnnotate: string[] = [];
    this.langMappings.forEach(langMap => {
      let iriToAnnotate = langMap.labelMappings.filter(labelMap => labelMap.annotatedPred == null).map(labelMap => labelMap.predIri);
      toAnnotate = toAnnotate.concat(iriToAnnotate);
    });
    if (toAnnotate.length > 0) {
      this.resourcesService.getResourcesInfo(toAnnotate.map(r => new ARTURIResource(r)), STRequestOptions.getRequestOptions(new ProjectContext(this.project))).subscribe(
        (annotated: ARTResource[]) => {
          let annotatedIRIs: ARTURIResource[] = annotated as ARTURIResource[];
          this.langMappings.forEach(langMap => {
            langMap.labelMappings.forEach(labelMap => {
              if (labelMap.annotatedPred == null) {
                let ann = annotatedIRIs.find(a => a.getURI() == labelMap.predIri);
                if (ann) {
                  labelMap.annotatedPred = ann;
                }
              }
            });
          });
        }
      );
    }
  }

  private sortMappings(mappings: PredLabelMappingEntry[]) {
    mappings.sort((m1, m2) => {
      let pred1: string = m1.annotatedPred ? m1.annotatedPred.getAdditionalProperty(ResAttribute.QNAME) : null;
      if (!pred1) {
        pred1 = m1.predIri;
      }
      let pred2: string = m2.annotatedPred ? m2.annotatedPred.getAdditionalProperty(ResAttribute.QNAME) : null;
      if (!pred2) {
        pred2 = m2.predIri;
      }
      return pred1.localeCompare(pred2);
    });
  }

  private fromSettingToStruct(setting: { [lang: string]: PredLabelMapping }) {
    for (let lang in setting) {
      let predLabelMapPlain = setting[lang];

      let labelMappings: PredLabelMappingEntry[] = [];

      for (let predIri in predLabelMapPlain) {
        let label = predLabelMapPlain[predIri];
        let undefinedMapping = {
          annotatedPred: null,
          predIri: predIri,
          label: label,
          visible: true
        };
        labelMappings.push(undefinedMapping);
      }

      this.sortMappings(labelMappings);

      this.langMappings.push({
        lang: lang,
        labelMappings: labelMappings,
        includeSimpleString: lang == "en"
      });
    }
  }

  private fromStructToSetting(): { [lang: string]: PredLabelMapping } {
    let langMappingsPref: { [lang: string]: PredLabelMapping } = {};
    this.langMappings.forEach(langMap => {
      let mappings: PredLabelMapping = {};
      langMap.labelMappings.filter(labelMap => {
        return labelMap.label && labelMap.label.trim() != "";
      }).forEach(labelMap => {
        mappings[labelMap.predIri] = labelMap.label;
      });
      langMappingsPref[langMap.lang] = mappings;
    });
    //clean empty langs
    for (let lang in langMappingsPref) {
      //empty map in lang? => delete it
      if (langMappingsPref[lang] && Object.keys(langMappingsPref[lang]).length == 0) {
        delete langMappingsPref[lang];
      }
    }
    return langMappingsPref;
  }

  onModelChange() {
    let setting = this.fromStructToSetting();
    this.propagateChange(setting);
  }

  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(obj: { [lang: string]: PredLabelMapping }) {
    if (obj != null) {
      this.init(obj);
    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };

}

interface PredLabelMappingEntry {
  annotatedPred: ARTURIResource;
  predIri: string;
  label: string;
  visible: boolean; //support for filter
}

interface LangMappingsTab {
  lang: string;
  labelMappings: PredLabelMappingEntry[];
  filterString?: string;
  includeSimpleString: boolean;
}