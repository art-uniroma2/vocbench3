import { Component, forwardRef, Input } from "@angular/core";
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DataPanel, DataStructureUtils } from "src/app/models/DataStructure";
import { Project } from "src/app/models/Project";
import { TranslationUtils } from "src/app/utils/TranslationUtils";
import { VBContext } from "src/app/utils/VBContext";

@Component({
  selector: "data-panels-settings",
  templateUrl: "./data-panels-settings.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => DataPanelsSettingsComponent), multi: true,
  }],
  standalone: false
})
export class DataPanelsSettingsComponent {

  @Input() project: Project;

  private hiddenDataPanels: DataPanel[];

  tabsStruct: { tab: DataPanel, translationKey: string, visible: boolean }[];

  onlyOneVisible: boolean; //tells if only one tab is still visible, used for disabling checkbox of last visible tab

  constructor() { }

  ngOnInit() {
    if (this.project == null) {
      this.project = VBContext.getWorkingProject();
    }
  }

  private init() {
    let model = this.project.getModelType();
    let modelPanels: DataPanel[] = DataStructureUtils.modelPanelsMap[model].slice();
    this.tabsStruct = modelPanels.map(tab => {
      return { tab: tab, translationKey: TranslationUtils.dataPanelTranslationMap[tab], visible: !this.hiddenDataPanels.includes(tab) };
    });
    this.onlyOneVisible = this.tabsStruct.filter(t => t.visible).length == 1;
  }

  onTabToggle() {
    this.onlyOneVisible = this.tabsStruct.filter(t => t.visible).length == 1;
    this.onModelChange();
  }

  onModelChange() {
    this.hiddenDataPanels = this.tabsStruct.filter(t => !t.visible).map(t => t.tab);
    this.propagateChange(this.hiddenDataPanels);
  }

  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(obj: DataPanel[]) {
    if (obj) {
      this.hiddenDataPanels = obj.slice();
      this.init();
    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };

}