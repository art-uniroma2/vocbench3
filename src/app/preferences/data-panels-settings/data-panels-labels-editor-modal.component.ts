import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { DataPanel, DataPanelLabelMap, DataPanelLabelSetting, DataStructureUtils } from 'src/app/models/DataStructure';
import { Project } from 'src/app/models/Project';
import { TranslationUtils } from 'src/app/utils/TranslationUtils';
import { BasicModalServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';

@Component({
  selector: 'data-panel-labels-editor-modal',
  templateUrl: './data-panels-labels-editor-modal.component.html',
  standalone: false
})
export class DataPanelLabelsEditorModalComponent {

  @Input() project: Project;
  @Input() setting: DataPanelLabelSetting;
  @Input() customTreeEnabled: boolean;

  panels: DataPanel[];

  translateLangs: string[];

  langs: string[]; //langs for which exists a mapping
  additionalLangs: string[];

  langMappings: LangMappingsTab[] = [];
  activeTab: LangMappingsTab;

  activeTabLang: string;

  constructor(
    public activeModal: NgbActiveModal,
    private basicModals: BasicModalServices,
    private translate: TranslateService,
  ) { }

  ngOnInit() {

    this.translateLangs = this.translate.getLangs();
    this.additionalLangs = this.translateLangs;

    let model = this.project.getModelType();
    this.panels = [DataPanel.cls, DataPanel.concept, DataPanel.skosCollection, DataPanel.conceptScheme,
    DataPanel.limeLexicon, DataPanel.ontolexLexicalEntry, DataPanel.property, DataPanel.dataRange];

    //filter only tabs compliant with model
    this.panels = this.panels.filter(t => DataStructureUtils.modelPanelsMap[model].includes(t));


    if (this.customTreeEnabled) {
      this.panels.push(DataPanel.custom);
    }

    //if a setting is provided, fill the struct
    if (this.setting) {
      for (let lang in this.setting) {
        if (this.translateLangs.includes(lang)) {
          this.addLangTab(lang);
          let langMap = this.langMappings.find(m => m.lang == lang); //gets the newly created mapping
          let panelLabelMappings = this.setting[lang]; //get the lang mapping for the adding lang
          for (let panel in panelLabelMappings) {
            let pm = langMap.panelMappings.find(pm => pm.panel == panel);
            if (pm) {
              pm.label = panelLabelMappings[panel];
            }
          }
        }
      }
      //once restored tabs, activate the first one
      this.activateTabLang(this.langMappings[0].lang);
    }
  }

  private updateLangs() {
    this.langs = this.langMappings.map(m => m.lang);
    this.additionalLangs = this.translateLangs.filter(l => !this.langs.includes(l));
  }

  /* =====
  TABS
  * ====== */

  activateTabLang(lang: string) {
    this.activeTabLang = lang;
    // this.activeMappingTab.labelMappings = this.langMappings.find(m => m.lang == lang).labelMappings;
    this.activeTab = this.langMappings.find(m => m.lang == lang);
  }

  closeTab(lang: string, event: Event) {
    this.basicModals.confirm({ key: "STATUS.WARNING" }, { key: "PREFERENCES.DATA_PANEL_LABELS.DELETE_MAPPINGS_WARN", params: { language: lang } }, ModalType.warning).then(
      () => {
        event.preventDefault(); //prevent the refresh of the page
        //remove the tab to close
        this.langMappings = this.langMappings.filter(m => m.lang != lang);
        //update active tab
        if (this.langMappings.length > 0) {
          this.activateTabLang(this.langMappings[0].lang);
        } else {
          this.activeTabLang = null;
          this.activeTab = null;
        }
        this.updateLangs();
      },
      () => { }
    );
  }

  addLangTab(lang: string) {
    let panelMappings: PanelMappingEntry[] = this.panels.map(p => {
      return {
        panel: p,
        translationKey: TranslationUtils.dataPanelTranslationMap[p],
        label: null
      };
    });
    this.langMappings.push({
      lang: lang,
      panelMappings: panelMappings
    });
    this.updateLangs();
    this.activateTabLang(lang);
  }

  ok() {
    //convert internal structure to the setting value 
    let setting: DataPanelLabelSetting = {};

    this.langMappings.forEach(langMap => {
      let mappings: DataPanelLabelMap = {};
      langMap.panelMappings.filter(m => {
        return m.label && m.label.trim() != "";
      }).forEach(m => {
        mappings[m.panel] = m.label;
      });
      setting[langMap.lang] = mappings;
    });
    //clean empty langs
    for (let lang in setting) {
      //empty map in lang? => delete it
      if (Object.keys(setting[lang]).length == 0) {
        delete setting[lang];
      }
    }
    this.activeModal.close(setting);
  }

  close() {
    this.activeModal.dismiss();
  }

}

interface PanelMappingEntry {
  panel: DataPanel;
  translationKey: string;
  label: string;
}

interface LangMappingsTab {
  lang: string;
  panelMappings: PanelMappingEntry[];
}