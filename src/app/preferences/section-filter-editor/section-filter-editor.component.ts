import { Component, forwardRef, Input, SimpleChanges } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { TranslationUtils } from "src/app/utils/TranslationUtils";
import { RDFResourceRolesEnum } from "../../models/ARTResources";
import { ResViewTemplate, SectionFilterPreference } from "../../models/Properties";
import { ResViewSection } from "../../models/ResourceView";
import { ResourceUtils } from "../../utils/ResourceUtils";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { ModalType } from '../../modal-dialogs/Modals';

@Component({
  selector: "section-filter-editor",
  templateUrl: "./section-filter-editor.component.html",
  host: { class: "vbox" },
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => SectionFilterEditorComponent), multi: true,
  }],
  standalone: false
})
export class SectionFilterEditorComponent {

  @Input() template: ResViewTemplate;
  @Input() readonly: boolean;

  private sectionFilterSetting: SectionFilterPreference;

  roleSectionsStructs: RoleSectionsStruct[];
  selectedRoleSectionsStruct: RoleSectionsStruct;

  constructor(private basicModals: BasicModalServices) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['template'] && changes['template'].currentValue) {
      this.initSectionFilter();
    }
  }

  private initSectionFilter() {
    if (!this.template || !this.sectionFilterSetting) {
      /*
      Initialization of the internal structures needs
      - template: on which to base the editor (provided as @Input)
      - setting: actual value of setting, namely the hidden sections for each role/template (bound with ngModel)
      In on of these is still not provided (e.g. async initialization from parent component), do nothing
      */
      return;
    }
    /*
    Convert the sectionFilterSetting to a RoleSectionStruct list.
    SectionFilterSetting is an object that lists the hidden sections for each roles.
    RoleSectionStruct contains also other info, such as the role or section "show" and the "checked" boolean when a section is visible
    */
    this.roleSectionsStructs = [];
    for (let role in this.template) {
      let sectionsStructs: SectionStruct[] = [];
      let sections: ResViewSection[] = this.template[role];
      sections.forEach(s => {
        //section visible if the role has no filtered-section listed, or the section for the role is not present among the filtered
        let showSection: boolean = this.sectionFilterSetting[role] == null || this.sectionFilterSetting[role].indexOf(s) == -1;
        sectionsStructs.push({
          id: s,
          labelTranslationKey: TranslationUtils.getResViewSectionTranslationKey(s),
          checked: showSection
        });
      });
      this.roleSectionsStructs.push({
        role: { id: role as RDFResourceRolesEnum, show: ResourceUtils.getResourceRoleLabel(role as RDFResourceRolesEnum) },
        sections: sectionsStructs
      });
    }
    this.roleSectionsStructs.sort((p1, p2) => p1.role.show.localeCompare(p2.role.show));

    //reset the selection of the role
    // this.selectedRoleSectionsStruct = null;
    this.selectedRoleSectionsStruct = this.roleSectionsStructs[0];
  }

  selectRoleSectionsStruct(rps: RoleSectionsStruct) {
    this.selectedRoleSectionsStruct = rps;
  }

  /**
   * Checks/Unchecks (according the check parameter) all the sections of the struct provided
   * @param roleSectionsStruct 
   * @param check 
   */
  checkAll(roleSectionsStruct: RoleSectionsStruct, check: boolean) {
    roleSectionsStruct.sections.forEach(p => {
      p.checked = check;
    });
    this.updatePref();
  }

  /**
   * Checks/Unchecks (according the check parameter) the same section (the provided one) for all the roles.
   */
  checkForAllRoles(section: ResViewSection, check: boolean) {
    this.roleSectionsStructs.forEach(rps => {
      rps.sections.forEach(p => {
        if (section == p.id) {
          p.checked = check;
        }
      });
    });
    this.updatePref();
  }

  /**
   * Restore to visible all the sections for all the roles
   */
  reset() {
    this.basicModals.confirm({ key: "COMMONS.ACTIONS.RESET" }, { key: "MESSAGES.ENABLE_ALL_SECTIONS_IN_ALL_TYPES_CONFIRM" }, ModalType.warning).then(
      () => {
        this.roleSectionsStructs.forEach(rps => {
          rps.sections.forEach(p => {
            p.checked = true;
          });
        });
        this.updatePref();
      },
      () => { }
    );
  }

  updatePref() {
    let pref: SectionFilterPreference = {};
    this.roleSectionsStructs.forEach(rps => {
      let sectionsPref: ResViewSection[] = [];
      rps.sections.forEach(p => {
        if (!p.checked) {
          sectionsPref.push(p.id);
        }
      });
      if (sectionsPref.length > 0) {
        pref[rps.role.id + ""] = sectionsPref;
      }
    });
    this.propagateChange(pref);
  }



  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(obj: SectionFilterPreference) {
    if (obj != null) {
      this.sectionFilterSetting = obj;
    } else {
      this.sectionFilterSetting = {};
    }
    this.initSectionFilter();
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };

}

class RoleSectionsStruct {
  role: RoleStruct;
  sections: SectionStruct[];
}
class RoleStruct {
  id: RDFResourceRolesEnum;
  show: string;
}
class SectionStruct {
  id: ResViewSection;
  labelTranslationKey: string;
  checked: boolean;
}