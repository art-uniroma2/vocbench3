import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { ChangeMeasureModalComponent } from '../edoal/change-measure-modal.component';
import { EdoalComponent } from '../edoal/edoal.component';
import { SharedModule } from './sharedModule';
import { TreeAndListModule } from './treeAndListModule';

@NgModule({
    imports: [
        CommonModule,
        DragDropModule,
        FormsModule,
        NgbDropdownModule,
        SharedModule,
        TranslateModule,
        TreeAndListModule
    ],
    declarations: [
        ChangeMeasureModalComponent,
        EdoalComponent,
    ],
    exports: [],
    providers: []
})
export class EdoalModule { }