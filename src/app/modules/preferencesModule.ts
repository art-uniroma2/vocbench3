import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { DataPanelLabelsEditorModalComponent } from '../preferences/data-panels-settings/data-panels-labels-editor-modal.component';
import { DataPanelsSettingsComponent } from '../preferences/data-panels-settings/data-panels-settings.component';
import { LanguagePreferencesComponent } from "../preferences/languagePreferencesComponent";
import { LanguageRenderingComponent } from "../preferences/languages/language-rendering.component";
import { LanguageValueFilterComponent } from "../preferences/languages/language-value-filter.component";
import { LanguageEditingComponent } from "../preferences/languages/languageEditingComponent";
import { NotificationsPreferencesComponent } from '../preferences/notifications/notifications-preferences.component';
import { PredicateLabelSettingsModalComponent } from '../preferences/predicate-label-settings/predicate-label-settings-modal.component';
import { PredicateLabelSettingsComponent } from '../preferences/predicate-label-settings/predicate-label-settings.component';
import { SelectVocabulariesModalComponent } from '../preferences/predicate-label-settings/select-vocabularies-modal.component';
import { ProjectsViewSettingsComponent } from '../preferences/projects-view-settings/projects-view-settings.component';
import { ResViewSectionCustomizationModalComponent } from '../preferences/res-view-section-customization/res-view-section-customization-modal.component';
import { ResViewTemplateEditorComponent } from '../preferences/res-view-template-editor/res-view-template-editor.component';
import { ResourceViewPreferencesComponent } from '../preferences/resourceViewPreferencesComponent';
import { SectionFilterEditorComponent } from '../preferences/section-filter-editor/section-filter-editor.component';
import { VocbenchPreferencesComponent } from "../preferences/vocbenchPreferencesComponent";
import { SharedModule } from './sharedModule';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbDropdownModule,
    NgbTooltipModule,
    SharedModule,
    TranslateModule
  ],
  declarations: [
    DataPanelLabelsEditorModalComponent,
    DataPanelsSettingsComponent,
    LanguageEditingComponent,
    LanguagePreferencesComponent,
    LanguageRenderingComponent,
    LanguageValueFilterComponent,
    NotificationsPreferencesComponent,
    PredicateLabelSettingsComponent,
    PredicateLabelSettingsModalComponent,
    ProjectsViewSettingsComponent,
    ResourceViewPreferencesComponent,
    ResViewSectionCustomizationModalComponent,
    ResViewTemplateEditorComponent,
    SectionFilterEditorComponent,
    SelectVocabulariesModalComponent,
    VocbenchPreferencesComponent,
  ],
  exports: [
    DataPanelsSettingsComponent,
    LanguageValueFilterComponent,
    NotificationsPreferencesComponent,
    PredicateLabelSettingsComponent,
    ProjectsViewSettingsComponent,
    ResourceViewPreferencesComponent,
    ResViewTemplateEditorComponent,
    SectionFilterEditorComponent,
    VocbenchPreferencesComponent,
  ],
  providers: []
})
export class PreferencesModule { }