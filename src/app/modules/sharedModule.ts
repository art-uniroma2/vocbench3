import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CodemirrorModule } from '@ctrl/ngx-codemirror';
import { NgbDropdownModule, NgbPopoverModule, NgbToastModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { QuillModule } from 'ngx-quill';
import { ExplainableTripleComponent } from '../icv/owlConsistencyViolations/explainable-triple.component';
import { DatasetResourceComponent } from '../metadata/metadataRegistry/mdrTree/dataset-resource.component';
import { ProjectListPanelComponent } from '../project/projectListPanel/project-list-panel.component';
import { QueryParameterFormComponent } from '../sparql/queryParameterization/query-parameter-form.component';
import { YasguiComponent } from '../sparql/yasgui.component';
import { BarChartComponent } from '../widget/charts/barChartComponent';
import { LineChartComponent } from '../widget/charts/line-chart.component';
import { PieChartComponent } from '../widget/charts/pieChartComponent';
import { SeriesChartLegendComponent } from '../widget/charts/series-chart-legend.component';
import { HtmlEditorComponent } from '../widget/codemirror/html-editor/html-editor.component';
import { JsonEditorComponent } from '../widget/codemirror/json-editor/json-editor.component';
import { ManchesterEditorComponent } from '../widget/codemirror/manchester-editor/manchester-editor.component';
import { MustacheEditorComponent } from '../widget/codemirror/mustache-editor/mustache-editor.component';
import { NTripleEditorComponent } from '../widget/codemirror/ntriple-editor/ntriple-editor.component';
import { PearlEditorComponent } from "../widget/codemirror/pearl-editor/pearl-editor.component";
import { TurtleEditorComponent } from '../widget/codemirror/turtle-editor/turtle-editor.component';
import { ConverterConfiguratorComponent } from '../widget/converter-configurator/converter-configurator.component';
import { ListParamEditorComponent } from '../widget/converter-configurator/list-param-editor.component';
import { MapParamEditorComponent } from '../widget/converter-configurator/map-param-editor.component';
import { CtrlKeyDirective } from '../widget/directives/ctrl-key.directive';
import { ResizableDirective } from "../widget/directives/resizable.directive";
import { SanitizerDirective } from "../widget/directives/sanitizer.directive";
import { ShiftKeyDirective } from '../widget/directives/shift-key.directive';
import { ExpandableAlertComponent } from '../widget/expandable-alert/expandable-alert.component';
import { ExtensionConfiguratorComponent } from '../widget/extension-configurator/extension-configurator.component';
import { InlineEditableValueComponent } from '../widget/inline-editable-value/inline-editable-value.component';
import { InputEditableComponent } from '../widget/input-editable/input-editable.component';
import { LangStringEditorComponent } from '../widget/lang-string-editor/lang-string-editor.component';
import { LanguageItemComponent } from '../widget/language-item/language-item.component';
import { LeafletMapModalComponent } from '../widget/leaflet-map/leaflet-map-modal.component';
import { LeafletMapComponent } from '../widget/leaflet-map/leaflet-map.component';
import { LoadingDivComponent } from '../widget/loading-div/loading-div.component';
import { LocalizedEditorModalComponent } from '../widget/localized-editor/localized-editor-modal';
import { LocalizedEditorComponent } from '../widget/localized-editor/localized-editor.component';
import { EditableNsInputComponent } from '../modal-dialogs/creation-modals/newResourceModal/editable-ns-input.component';
import { PasswordInputComponent } from '../widget/password-input/password-input.component';
import { DatatypePickerComponent } from '../widget/pickers/datatype-picker/datatype-picker.component';
import { DatetimePickerComponent } from '../widget/pickers/datetime-picker/datetime-picker.component';
import { FilePickerComponent } from '../widget/pickers/file-picker/file-picker.component';
import { InlineResourceListEditorComponent } from '../widget/pickers/inline-resource-list/inline-resource-list-editor.component';
import { LangPickerComponent } from '../widget/pickers/lang-picker/lang-picker.component';
import { LiteralPickerComponent } from '../widget/pickers/value-picker/literal-picker.component';
import { ResourcePickerComponent } from '../widget/pickers/value-picker/resource-picker.component';
import { ValuePickerComponent } from '../widget/pickers/value-picker/value-picker.component';
import { RdfResourceComponent } from '../widget/rdf-resource/rdf-resource.component';
import { ResourceListSelectionComponent } from '../widget/rdf-resource/resource-list-selection.component';
import { ResourceListComponent } from '../widget/rdf-resource/resource-list.component';
import { ResizableLayoutComponent } from '../widget/resizable-layout/resizable-layout.component';
import { DataSizeRendererComponent } from '../widget/settings-renderer/datasize-renderer.component';
import { NestedSettingSetRendererComponent } from '../widget/settings-renderer/nested-settings-renderer.component';
import { SettingEnumerationRendererComponent } from '../widget/settings-renderer/setting-enumeration-renderer.component';
import { SettingSetRendererComponent } from '../widget/settings-renderer/setting-set-renderer.component';
import { SettingValueRendererComponent } from '../widget/settings-renderer/setting-value-renderer.component';
import { SettingConfigurationRendererComponent } from '../widget/settings-renderer/setting-configuration-renderer.component';
import { SettingMapRendererComponent } from '../widget/settings-renderer/setting-map-renderer.component';
import { SettingPropRendererComponent } from '../widget/settings-renderer/setting-prop-renderer.component';
import { SettingsRendererPanelComponent } from '../widget/settings-renderer/settings-renderer-panel.component';
import { SettingsRendererComponent } from '../widget/settings-renderer/settings-renderer.component';
import { ToastsContainerComponent } from '../widget/toast/toast-container.component';
import { ToastService } from '../widget/toast/toast.service';
import { HtmlLiteralInputComponent } from '../widget/typed-literal-input/html-literal-input/html-literal-input.component';
import { TypedLiteralInputComponent } from '../widget/typed-literal-input/typed-literal-input.component';


@NgModule({
  imports: [
    CodemirrorModule,
    CommonModule,
    FormsModule,
    NgbDropdownModule,
    NgbPopoverModule,
    NgbToastModule,
    NgxChartsModule,
    TranslateModule,
    QuillModule,
  ],
  declarations: [
    ConverterConfiguratorComponent,
    DatasetResourceComponent,
    DataSizeRendererComponent,
    DatatypePickerComponent,
    DatetimePickerComponent,
    EditableNsInputComponent,
    ExpandableAlertComponent,
    ExplainableTripleComponent,
    ExtensionConfiguratorComponent,
    FilePickerComponent,
    HtmlEditorComponent,
    HtmlLiteralInputComponent,
    InlineEditableValueComponent,
    InlineResourceListEditorComponent,
    InputEditableComponent,
    JsonEditorComponent,
    LangPickerComponent,
    LangStringEditorComponent,
    LanguageItemComponent,
    ListParamEditorComponent,
    LiteralPickerComponent,
    LoadingDivComponent,
    LocalizedEditorComponent,
    LocalizedEditorModalComponent,
    ManchesterEditorComponent,
    MapParamEditorComponent,
    MustacheEditorComponent,
    NTripleEditorComponent,
    PasswordInputComponent,
    PearlEditorComponent,
    ProjectListPanelComponent,
    QueryParameterFormComponent,
    RdfResourceComponent,
    ResizableDirective,
    ResizableLayoutComponent,
    ResourceListComponent,
    ResourceListSelectionComponent,
    ResourcePickerComponent,
    SettingConfigurationRendererComponent,
    SettingEnumerationRendererComponent,
    SettingMapRendererComponent,
    SettingPropRendererComponent,
    SettingValueRendererComponent,
    SettingSetRendererComponent,
    SettingsRendererComponent,
    NestedSettingSetRendererComponent,
    SettingsRendererPanelComponent,
    ToastsContainerComponent,
    TypedLiteralInputComponent,
    TurtleEditorComponent,
    ValuePickerComponent,
    YasguiComponent,
    BarChartComponent,
    LeafletMapComponent,
    LeafletMapModalComponent,
    LineChartComponent,
    PieChartComponent,
    SeriesChartLegendComponent,

    //directives
    CtrlKeyDirective,
    SanitizerDirective,
    ShiftKeyDirective,
  ],
  exports: [
    ConverterConfiguratorComponent,
    DataSizeRendererComponent,
    DatasetResourceComponent,
    DatatypePickerComponent,
    DatetimePickerComponent,
    EditableNsInputComponent,
    ExpandableAlertComponent,
    ExplainableTripleComponent,
    ExtensionConfiguratorComponent,
    FilePickerComponent,
    HtmlEditorComponent,
    HtmlLiteralInputComponent,
    InlineEditableValueComponent,
    InputEditableComponent,
    InlineResourceListEditorComponent,
    JsonEditorComponent,
    LangPickerComponent,
    LangStringEditorComponent,
    LanguageItemComponent,
    LiteralPickerComponent,
    LoadingDivComponent,
    LocalizedEditorModalComponent,
    ManchesterEditorComponent,
    MustacheEditorComponent,
    NTripleEditorComponent,
    PasswordInputComponent,
    PearlEditorComponent,
    ProjectListPanelComponent,
    QueryParameterFormComponent,
    RdfResourceComponent,
    ResizableDirective,
    ResizableLayoutComponent,
    ResourceListComponent,
    ResourceListSelectionComponent,
    ResourcePickerComponent,
    SettingMapRendererComponent,
    SettingPropRendererComponent,
    SettingValueRendererComponent,
    SettingSetRendererComponent,
    SettingsRendererComponent,
    SettingsRendererPanelComponent,
    ToastsContainerComponent,
    TurtleEditorComponent,
    TypedLiteralInputComponent,
    ValuePickerComponent,
    YasguiComponent,
    BarChartComponent,
    LeafletMapComponent,
    LeafletMapModalComponent,
    LineChartComponent,
    PieChartComponent,

    CtrlKeyDirective,
    SanitizerDirective,
    ShiftKeyDirective,
  ],
  providers: [
    ToastService,
  ]
})
export class SharedModule { }