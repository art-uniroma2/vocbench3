import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from "@angular/router";
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { ChangePasswordModalComponent } from '../user/change-password-modal.component';
import { LoginComponent } from '../user/login.component';
import { RegistrationComponent } from '../user/registration.component';
import { ResetPasswordComponent } from '../user/reset-password.component';
import { UserCreateComponent } from '../user/user-create.component';
import { UserDetailsComponent } from '../user/user-details.component';
import { UserMenuComponent } from '../user/user-menu.component';
import { UserProfileComponent } from '../user/user-profile.component';
import { SharedModule } from './sharedModule';

@NgModule({
  imports: [
    CommonModule,
    DragDropModule,
    FormsModule,
    NgbDropdownModule,
    RouterModule,
    SharedModule,
    TranslateModule
  ],
  declarations: [
    ChangePasswordModalComponent,
    LoginComponent,
    RegistrationComponent,
    ResetPasswordComponent,
    UserCreateComponent,
    UserDetailsComponent,
    UserMenuComponent,
    UserProfileComponent,
  ],
  exports: [
    LoginComponent,
    UserCreateComponent,
    UserDetailsComponent,
    UserMenuComponent
  ],
  providers: []
})
export class UserModule { }