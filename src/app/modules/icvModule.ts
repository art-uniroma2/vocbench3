import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { BrokenAlignmentComponent } from "../icv/broken-alignment/broken-alignment.component";
import { BrokenDefinitionComponent } from "../icv/brokenDefinition/brokenDefinitionComponent";
import { ConflictualLabelComponent } from "../icv/conflictual-label/conflictual-label.component";
import { CyclicConceptComponent } from "../icv/cyclicConcept/cyclicConceptComponent";
import { DanglingConceptComponent } from "../icv/dangling-concept/dangling-concept.component";
import { DanglingXLabelComponent } from "../icv/dangling-xlabel/dangling-xlabel.component";
import { DisjointExactMatchConceptComponent } from "../icv/disjointExactMatchConcept/disjointExactMatchConceptComponent";
import { DisjointRelatedConceptComponent } from "../icv/disjointRelatedConcept/disjointRelatedConceptComponent";
import { ExtraSpaceLabelComponent } from "../icv/extraSpaceLabel/extraSpaceLabelComponent";
import { HierarchicalRedundancyComponent } from "../icv/hierarchicalRedundancy/hierarchicalRedundancyComponent";
import { IcvConfigPanelComponent } from "../icv/icv-config-panel.component";
import { IcvListComponent } from "../icv/icvListComponent";
import { icvRouting } from "../icv/icvRoutes";
import { InvalidUriComponent } from "../icv/invalidUri/invalidUriComponent";
import { MultiplePrefLabelComponent } from "../icv/multiplePrefLabel/multiplePrefLabelComponent";
import { NoDefinitionResourceComponent } from "../icv/noDefinitionResource/noDefinitionResourceComponent";
import { NoLabelResourceComponent } from "../icv/no-label-resource/no-label-resource.component";
import { NoLangLabelComponent } from "../icv/noLangLabel/noLangLabelComponent";
import { NoMandatoryLabelComponent } from "../icv/noMandatoryLabel/noMandatoryLabelComponent";
import { NoSchemeConceptComponent } from "../icv/no-scheme-concept/no-scheme-concept.component";
import { NoTopConceptSchemeComponent } from "../icv/noTopConceptScheme/no-top-concept-scheme.component";
import { OnlyAltLabelResourceComponent } from "../icv/onlyAltLabelResource/onlyAltLabelResourceComponent";
import { OverlappedLabelComponent } from "../icv/overlappedLabel/overlapped-label.component";
import { OwlConsistencyViolationsComponent } from '../icv/owlConsistencyViolations/owlConsistencyViolationsComponent';
import { TopConceptWithBroaderComponent } from "../icv/top-concept-with-broader/top-concept-with-broader.component";
import { SharedModule } from './sharedModule';
import { TreeAndListModule } from "./treeAndListModule";


@NgModule({
    imports: [
        icvRouting,
        CommonModule,
        FormsModule,
        NgbDropdownModule,
        SharedModule,
        TranslateModule,
        TreeAndListModule,
    ],
    declarations: [
        BrokenAlignmentComponent,
        BrokenDefinitionComponent,
        ConflictualLabelComponent,
        CyclicConceptComponent,
        DanglingConceptComponent,
        DanglingXLabelComponent,
        DisjointExactMatchConceptComponent,
        DisjointRelatedConceptComponent,
        ExtraSpaceLabelComponent,
        HierarchicalRedundancyComponent,
        IcvConfigPanelComponent,
        IcvListComponent,
        InvalidUriComponent,
        MultiplePrefLabelComponent,
        NoDefinitionResourceComponent,
        NoLabelResourceComponent,
        NoLangLabelComponent,
        NoMandatoryLabelComponent,
        NoSchemeConceptComponent,
        NoTopConceptSchemeComponent,
        OnlyAltLabelResourceComponent,
        OverlappedLabelComponent,
        OwlConsistencyViolationsComponent,
        TopConceptWithBroaderComponent,
    ],
    exports: [],
    providers: [],
})
export class IcvModule { }