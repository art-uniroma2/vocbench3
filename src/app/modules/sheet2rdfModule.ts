import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { AdvancedGraphApplicationModalComponent } from '../sheet2rdf/s2rdf-modals/advanced-graph-application-modal.component';
import { ContentSanitizationEditorModalComponent } from '../sheet2rdf/s2rdf-modals/content-sanitization-editor-modal.component';
import { ConversionNamespaceEditorComponent } from '../sheet2rdf/s2rdf-modals/conversion-namespace-editor.component';
import { HeaderEditorModalComponent } from '../sheet2rdf/s2rdf-modals/header-editor-modal.component';
import { MemoizationEditorComponent } from '../sheet2rdf/s2rdf-modals/memoization-editor.component';
import { NodeCreationModalComponent } from '../sheet2rdf/s2rdf-modals/node-creation-modal.component';
import { NodeSanitizationEditorComponent } from '../sheet2rdf/s2rdf-modals/node-sanitization-editor.component';
import { Sheet2RdfSettingsModalComponent } from '../sheet2rdf/s2rdf-modals/s2rdf-settings-modal.component';
import { SimpleGraphApplicationModalComponent } from '../sheet2rdf/s2rdf-modals/simple-graph-application-modal.component';
import { SubjectHeaderEditorModalComponent } from '../sheet2rdf/s2rdf-modals/subject-header-editor-modal.component';
import { Sheet2RdfComponent } from '../sheet2rdf/sheet2rdf.component';
import { Sheet2RdfContextService } from '../sheet2rdf/sheet2rdfContext';
import { SheetManagerComponent } from '../sheet2rdf/sheet-manager.component';
import { SharedModule } from './sharedModule';

@NgModule({
    imports: [
        CommonModule,
        DragDropModule,
        FormsModule,
        NgbDropdownModule,
        RouterModule,
        SharedModule,
        TranslateModule,
    ],
    declarations: [
        ConversionNamespaceEditorComponent,
        MemoizationEditorComponent,
        NodeSanitizationEditorComponent,
        Sheet2RdfComponent,
        SheetManagerComponent,
        //modal
        AdvancedGraphApplicationModalComponent,
        ContentSanitizationEditorModalComponent,
        HeaderEditorModalComponent,
        NodeCreationModalComponent,
        Sheet2RdfSettingsModalComponent,
        SimpleGraphApplicationModalComponent,
        SubjectHeaderEditorModalComponent,
    ],
    exports: [
        Sheet2RdfComponent,
    ],
    providers: [
        Sheet2RdfContextService
    ]
})
export class Sheet2RdfModule { }