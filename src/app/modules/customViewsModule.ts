import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { CustomViewsComponent } from '../custom-views/custom-views.component';
import { CustomViewEditorModalComponent } from '../custom-views/editors/custom-view-editor-modal.component';
import { CvAssociationEditorModalComponent } from '../custom-views/editors/cv-association-editor-modal.component';
import { ImportCustomViewModalComponent } from '../custom-views/editors/import-custom-view-modal.component';
import { AdvSingleValueViewEditorComponent } from '../custom-views/editors/views/advSingleValueViewEditorComponent';
import { AreaViewEditorComponent } from '../custom-views/editors/views/areaViewEditorComponent';
import { DynamicVectorViewEditorComponent } from '../custom-views/editors/views/dynamicVectorViewEditorComponent';
import { PointViewEditorComponent } from '../custom-views/editors/views/pointViewEditorComponent';
import { PropertyChainViewEditorComponent } from '../custom-views/editors/views/propertyChainViewEditorComponent';
import { RouteViewEditorComponent } from '../custom-views/editors/views/routeViewEditorComponent';
import { SeriesCollectionViewEditorComponent } from '../custom-views/editors/views/seriesCollectionViewEditorComponent';
import { SeriesViewEditorComponent } from '../custom-views/editors/views/seriesViewEditorComponent';
import { SingleValueEditorComponent } from '../custom-views/editors/views/single-value-editor.component';
import { StaticVectorViewEditorComponent } from '../custom-views/editors/views/staticVectorViewEditorComponent';
import { SuggestFromCfValueSelectionModalComponent } from '../custom-views/editors/views/suggest-from-cf-value-selection-modal.component';
import { SharedModule } from './sharedModule';

@NgModule({
  imports: [
    CommonModule,
    DragDropModule,
    FormsModule,
    NgbDropdownModule,
    SharedModule,
    TranslateModule
  ],
  providers: [],
  declarations: [
    CustomViewsComponent,
    CustomViewEditorModalComponent,
    CvAssociationEditorModalComponent,
    AdvSingleValueViewEditorComponent,
    AreaViewEditorComponent,
    DynamicVectorViewEditorComponent,
    ImportCustomViewModalComponent,
    PointViewEditorComponent,
    PropertyChainViewEditorComponent,
    RouteViewEditorComponent,
    SeriesViewEditorComponent,
    SeriesCollectionViewEditorComponent,
    StaticVectorViewEditorComponent,
    SingleValueEditorComponent,
    SuggestFromCfValueSelectionModalComponent
  ],
  exports: [CustomViewsComponent]
})
export class CustomViewsModule { }