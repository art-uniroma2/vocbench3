import { CommonModule, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { inject, NgModule, provideAppInitializer } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { OAuthModule } from "angular-oauth2-oidc";
import { QuillModule } from 'ngx-quill';
import { AppComponent } from '../app.component';
import { AppRoutingModule } from '../app-routing.module';
import { CustomFormViewPageComponent } from '../custom-views/custom-form-view-page.component';
import { CustomReuseStrategy } from '../CustomReuseStrategy';
import { DataComponent } from '../data/data.component';
import { HomeComponent } from '../home.component';
import { AppConfigService } from "../services/app-config.service";
import { Oauth2Service } from "../services/oauth2.service";
import { SkosDiffingModule } from '../skos-diffing/skosDiffingModule';
import { UndoDirective } from '../undo/undoDirective';
import { UndoHandler } from '../undo/undoHandler';
import { DatatypeValidator } from '../utils/DatatypeValidator';
import { HttpManager } from '../utils/HttpManager';
import { DpopInterceptor } from "../utils/interceptors/dpop.interceptor";
import { RoleActionResolver } from '../utils/RoleActionResolver';
import { StMetadataRegistry } from '../utils/STMetadataRegistry';
import { UserResolver } from '../utils/UserResolver';
import { VBCollaboration } from '../utils/VBCollaboration';
import { VBContext } from "../utils/VBContext";
import { VBEventHandler } from '../utils/VBEventHandler';
import { VBProperties } from '../utils/VBProperties';
import { AdministrationModule } from './administrationModule';
import { AlignmentModule } from './alignmentModule';
import { CollaborationModule } from './collaborationModule';
import { CustomFormModule } from './customFormModule';
import { CustomServicesModule } from './customServicesModule';
import { CustomViewsModule } from './customViewsModule';
import { DatasetCatalogModule } from './datasetCatalogModule';
import { EdoalModule } from './edoalModule';
import { GlobalDataMgmtModule } from './globalDataMgmtModule';
import { GraphModule } from './graphModule';
import { HistoryValidationModule } from './historyValidationModule';
import { IcvModule } from './icvModule';
import { MetadataModule } from './metadataModule';
import { NotificationsModule } from './notificationsModule';
import { PreferencesModule } from './preferencesModule';
import { ProjectModule } from './projectModule';
import { ResourceMetadataModule } from './resourceMetadataModule';
import { ResourceViewModule } from './resourceViewModule';
import { ShaclModule } from './shaclModule';
import { SharedModule } from './sharedModule';
import { Sheet2RdfModule } from './sheet2rdfModule';
import { SparqlModule } from './sparqlModule';
import { STServicesModule } from './stServicesModule';
import { TreeAndListModule } from './treeAndListModule';
import { UserModule } from './userModule';
import { VBModalModule } from './vbModalModule';


// export function HttpLoaderFactory(http: HttpClient) {
//     return new TranslateHttpLoader(http, "./assets/l10n/");
// }
/*
The above is the configuration written in the doc (https://github.com/ngx-translate/core#configuration),
but I had to rewrite it as follow since it gave the eslint issue:
"Expected a function expression. eslint (func-style)"
*/
let HttpLoaderFactory = (http: HttpClient) => {
  return new TranslateHttpLoader(http, "./assets/l10n/");
};


@NgModule({
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    CommonModule,
    FormsModule,
    NgbModule,
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    QuillModule.forRoot(),
    OAuthModule.forRoot(),
    AppRoutingModule,

    AdministrationModule,
    AlignmentModule,
    CollaborationModule,
    CustomFormModule,
    CustomViewsModule,
    CustomServicesModule,
    DatasetCatalogModule,
    EdoalModule,
    GlobalDataMgmtModule,
    GraphModule,
    HistoryValidationModule,
    IcvModule,
    MetadataModule,
    NotificationsModule,
    PreferencesModule,
    ProjectModule,
    ResourceMetadataModule,
    ResourceViewModule,
    ShaclModule,
    SharedModule,
    Sheet2RdfModule,
    SkosDiffingModule,
    SparqlModule,
    STServicesModule,
    TreeAndListModule,
    UserModule,
    VBModalModule
  ],
  providers: [
    AppConfigService,
    Oauth2Service,
    provideAppInitializer(() => { // The functions contained here are executed before bootstrapping the Angular application, that is, before rendering the components.
      // Here it is used to ensure that the app and OAuth2 configuration is complete before the app is fully launched
      const appConfigService = inject(AppConfigService); // It's an Angular function that provides access to services within a context without the need to use the class constructor.
      const oauth2Service = inject(Oauth2Service); // It's an Angular function that provides access to services within a context without the need to use the class constructor.
      return appConfigService.loadConfig().then(isReady => {
        // It's called to load the system settings
        if (isReady && VBContext.getSystemSettings().authService === 'OAuth2') {
          // It's used to configure the OAuth2 system and start the process of authentication.
          return oauth2Service.configure().then(() => {
            // Go on
          }).catch(err => {
            console.error('Error in Oauth2 configuration:', err);
            // Warn the user there is an issue with loading of OAUTH2
            //const notificationService = inject(BasicModalServices);
            //notificationService.alert({ key: "STATUS.WARNING" }, "OAuth2 configuration failed.", ModalType.error);

            // DOES NOT interrupt the bootstrap of the application
            return Promise.resolve();
          });

        }
        return Promise.resolve(); // configuration ended without loading Oauth2 case in which the application starts in Default or SAMl mode
      });
    }),
    DatatypeValidator,
    HttpManager,
    RoleActionResolver,
    StMetadataRegistry,
    UndoHandler,
    UserResolver,
    VBCollaboration,
    VBEventHandler,
    VBProperties,
    { provide: RouteReuseStrategy, useClass: CustomReuseStrategy },
    /** Uses the HashLocationStrategy instead of the default "HTML 5 pushState" PathLocationStrategy.
     * This solves the 404 error problem when reloading a page in a production server
     */
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: DpopInterceptor,
      multi: true,
    }
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    DataComponent,
    CustomFormViewPageComponent,
    UndoDirective
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
