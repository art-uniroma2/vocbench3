import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { HistoryComponent } from '../history-validation/history.component';
import { HistoryFilterComponent } from '../history-validation/history-filter.component';
import { CommitDeltaModalComponent } from '../history-validation/modals/commit-delta-modal.component';
import { HistoryValidationModalServices } from '../history-validation/modals/historyValidationModalServices';
import { OperationParamsModalComponent } from '../history-validation/modals/operation-params-modal.component';
import { OperationSelectModalComponent } from '../history-validation/modals/operation-select-modal.component';
import { ValidationCommentsModalComponent } from '../history-validation/modals/validation-comments-modal.component';
import { ValidationComponent } from '../history-validation/validation.component';
import { SharedModule } from './sharedModule';

@NgModule({
    imports: [
        CommonModule,
        DragDropModule,
        FormsModule,
        NgbDropdownModule,
        SharedModule,
        TranslateModule
    ],
    declarations: [
        HistoryComponent, ValidationComponent, HistoryFilterComponent,
        //modals
        OperationSelectModalComponent, CommitDeltaModalComponent, OperationParamsModalComponent, ValidationCommentsModalComponent
    ],
    exports: [
        HistoryComponent, ValidationComponent
    ],
    providers: [
        HistoryValidationModalServices
    ]
})
export class HistoryValidationModule { }