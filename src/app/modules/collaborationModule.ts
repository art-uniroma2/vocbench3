import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { CollaborationComponent } from '../collaboration/collaborationComponent';
import { CollaborationModalServices } from '../collaboration/collaborationModalService';
import { IssueListComponent } from '../collaboration/issue-list.component';
import { IssueListModalComponent } from '../collaboration/issue-list-modal.component';
import { CollaborationProjectModalComponent } from '../collaboration/modals/collaboration-project-modal.component';
import { CollaborationProjSettingsModalComponent } from '../collaboration/modals/collaboration-proj-settings-modal.component';
import { CollaborationUserSettingsModalComponent } from '../collaboration/modals/collaboration-user-settings-modal.component';
import { CreateIssueModalComponent } from '../collaboration/modals/create-issue-modal.component';
import { SharedModule } from './sharedModule';

@NgModule({
    imports: [
        CommonModule,
        DragDropModule,
        FormsModule,
        NgbDropdownModule,
        SharedModule,
        TranslateModule
    ],
    declarations: [
        CollaborationComponent, IssueListComponent,
        IssueListModalComponent, CollaborationProjectModalComponent, CollaborationProjSettingsModalComponent, CollaborationUserSettingsModalComponent, CreateIssueModalComponent,
    ],
    exports: [CollaborationComponent],
    providers: [CollaborationModalServices]
})
export class CollaborationModule { }