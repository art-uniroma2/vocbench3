import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { ImportPatternModalComponent } from '../resource-metadata/modals/import-pattern-modal.component';
import { MetadataAssociationEditorModalComponent } from '../resource-metadata/modals/metadata-assosiaction-editor-modal.component';
import { MetadataFactoryPatternSelectionModalComponent } from '../resource-metadata/modals/metadata-factory-pattern-selection-modal.component';
import { MetadataPatternEditorModalComponent } from '../resource-metadata/modals/metadata-pattern-editor-modal.component';
import { MetadataPatternLibraryModalComponent } from '../resource-metadata/modals/metadata-pattern-library-modal.component';
import { ResourceMetadataComponent } from '../resource-metadata/resource-metadata.component';
import { SharedModule } from './sharedModule';

@NgModule({
    imports: [
        CommonModule,
        DragDropModule,
        FormsModule,
        NgbDropdownModule,
        SharedModule,
        TranslateModule,
    ],
    providers: [],
    declarations: [
        ImportPatternModalComponent,
        ResourceMetadataComponent,
        MetadataAssociationEditorModalComponent,
        MetadataFactoryPatternSelectionModalComponent,
        MetadataPatternEditorModalComponent,
        MetadataPatternLibraryModalComponent,
    ],
    exports: [
        ResourceMetadataComponent
    ]
})
export class ResourceMetadataModule { }