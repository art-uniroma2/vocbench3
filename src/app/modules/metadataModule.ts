import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { DatasetMetadataComponent } from '../metadata/metadataRegistry/datasetMetadataComponent';
import { LexicalizationSetMetadataComponent } from '../metadata/metadataRegistry/lexicalizationSetsPanel/lexicalizationSetMetadataComponent';
import { LexicalizationSetsPanelComponent } from '../metadata/metadataRegistry/lexicalizationSetsPanel/lexicalizationSetsPanelComponent';
import { LinksetPanelComponent } from '../metadata/metadataRegistry/linksetsPanel/linksetPanelComponent';
import { ConnectToAbsDatasetModalComponent } from '../metadata/metadataRegistry/mdrTree/connect-to-abs-dataset-modal.component';
import { MetadataRegistryTreeComponent } from "../metadata/metadataRegistry/mdrTree/mdrTreeComponent";
import { MetadataRegistryTreeModalComponent } from '../metadata/metadataRegistry/mdrTree/mdr-tree-modal.component';
import { MetadataRegistryTreeNodeComponent } from '../metadata/metadataRegistry/mdrTree/mdrTreeNodeComponent';
import { MetadataRegistryTreePanelComponent } from '../metadata/metadataRegistry/mdrTree/mdrTreePanelComponent';
import { NewDatasetModalComponent } from '../metadata/metadataRegistry/mdrTree/new-dataset-modal.component';
import { MetadataRegistryComponent } from "../metadata/metadataRegistry/metadataRegistryComponent";
import { NewEmbeddedLexicalizationModalComponent } from '../metadata/metadataRegistry/new-embedded-lexicalization-modal.component';
import { ConflictResolverModalComponent } from '../metadata/metadataVocabularies/conflict-resolver-modal.component';
import { MetadataVocabulariesComponent } from "../metadata/metadataVocabularies/metadata-vocabularies.component";
import { ImportFromDatasetCatalogModalComponent } from '../metadata/namespacesAndImports/import-from-dataset-catalog-modal.component';
import { ImportOntologyModalComponent } from '../metadata/namespacesAndImports/import-ontology-modal.component';
import { ImportTreeComponent } from '../metadata/namespacesAndImports/import-tree/import-tree.component';
import { ImportTreeNodeComponent } from '../metadata/namespacesAndImports/import-tree/import-tree-node.component';
import { ImportTreePanelComponent } from '../metadata/namespacesAndImports/import-tree/import-tree-panel.component';
import { NamespacesAndImportsComponent } from "../metadata/namespacesAndImports/namespaces-and-imports.component";
import { OntologyMirrorModalComponent } from '../metadata/namespacesAndImports/ontology-mirror-modal.component';
import { ResourceViewModule } from './resourceViewModule';
import { SharedModule } from './sharedModule';

@NgModule({
    imports: [
        CommonModule,
        DragDropModule,
        FormsModule,
        NgbDropdownModule,
        SharedModule,
        ResourceViewModule,
        TranslateModule
    ],
    declarations: [
        DatasetMetadataComponent,
        ImportTreeComponent,
        ImportTreeNodeComponent,
        ImportTreePanelComponent,
        LexicalizationSetMetadataComponent,
        LexicalizationSetsPanelComponent,
        LinksetPanelComponent,
        MetadataRegistryComponent,
        MetadataRegistryTreeComponent,
        MetadataRegistryTreeModalComponent,
        MetadataRegistryTreeNodeComponent,
        MetadataRegistryTreePanelComponent,
        MetadataVocabulariesComponent,
        NamespacesAndImportsComponent,
        //modals
        ConflictResolverModalComponent,
        ConnectToAbsDatasetModalComponent,
        ImportOntologyModalComponent,
        ImportFromDatasetCatalogModalComponent,
        OntologyMirrorModalComponent,
        NewDatasetModalComponent,
        NewEmbeddedLexicalizationModalComponent
    ],
    exports: [
        MetadataRegistryTreeModalComponent
    ],
    providers: []
})
export class MetadataModule { }