import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { CustomServiceRouterComponent } from '../custom-services/customServiceRouterComponent';
import { CustomOperationComponent } from '../custom-services/custom-services-editor/custom-operation.component';
import { CustomServiceComponent } from '../custom-services/custom-services-editor/custom-service.component';
import { CustomServicesPageComponent } from '../custom-services/custom-services-editor/custom-services-page.component';
import { AuthorizationHelperModalComponent } from '../custom-services/custom-services-editor/modals/authorization-helper-modal.component';
import { CustomOperationEditorModalComponent } from '../custom-services/custom-services-editor/modals/custom-operation-editor-modal.component';
import { CustomOperationModalComponent } from '../custom-services/custom-services-editor/modals/custom-operation-modal.component';
import { CustomServiceEditorModalComponent } from '../custom-services/custom-services-editor/modals/custom-service-editor-modal.component';
import { CustomServiceModalServices } from '../custom-services/custom-services-editor/modals/customServiceModalServices';
import { ImportCustomServiceModalComponent } from '../custom-services/custom-services-editor/modals/import-custom-service-modal.component';
import { OperationTypeEditorComponent } from '../custom-services/custom-services-editor/modals/operation-type-editor.component';
import { InvokableReporterComponent } from '../custom-services/invokable-reporters/invokable-reporter.component';
import { InvokableReportersPageComponent } from '../custom-services/invokable-reporters/invokable-reporters-page.component';
import { ImportInvokableReporterModalComponent } from '../custom-services/invokable-reporters/modals/import-invokable-reporter-modal.component';
import { InvokableReporterEditorModalComponent } from '../custom-services/invokable-reporters/modals/invokable-reporter-editor-modal.component';
import { InvokableReporterModalServices } from '../custom-services/invokable-reporters/modals/invokableReporterModalServices';
import { ReportResultModalComponent } from '../custom-services/invokable-reporters/modals/report-result-modal.component';
import { ServiceInvocationEditorModalComponent } from '../custom-services/invokable-reporters/modals/service-invocation-editor-modal.component';
import { ServiceInvocationComponent } from '../custom-services/invokable-reporters/service-invocation.component';
import { SharedModule } from './sharedModule';


@NgModule({
    imports: [
        CommonModule,
        DragDropModule,
        FormsModule,
        NgbDropdownModule,
        NgbPopoverModule,
        SharedModule,
        TranslateModule,
    ],
    declarations: [
        AuthorizationHelperModalComponent,
        CustomOperationComponent,
        CustomOperationEditorModalComponent,
        CustomOperationModalComponent,
        CustomServicesPageComponent,
        CustomServiceRouterComponent,
        CustomServiceComponent,
        CustomServiceEditorModalComponent,
        ImportCustomServiceModalComponent,
        ImportInvokableReporterModalComponent,
        InvokableReportersPageComponent,
        InvokableReporterComponent,
        InvokableReporterEditorModalComponent,
        ReportResultModalComponent,
        ServiceInvocationComponent,
        ServiceInvocationEditorModalComponent,
        OperationTypeEditorComponent,
    ],
    exports: [
        CustomServiceRouterComponent
    ],
    providers: [
        CustomServiceModalServices, InvokableReporterModalServices
    ]
})
export class CustomServicesModule { }