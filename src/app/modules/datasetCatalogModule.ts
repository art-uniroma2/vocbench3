import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { DataDumpSelectorModalComponent } from '../config/dataManagement/datasetCatalog/data-dump-selector-modal.component';
import { DatasetCatalogModalComponent } from '../config/dataManagement/datasetCatalog/datasetCatalogModal';
import { DatasetDescriptionComponent } from '../config/dataManagement/datasetCatalog/dataset-description.component';
import { SharedModule } from './sharedModule';

@NgModule({
    imports: [
        CommonModule,
        DragDropModule,
        FormsModule,
        SharedModule,
        TranslateModule
    ],
    declarations: [
        DatasetDescriptionComponent,
        //modals
        DatasetCatalogModalComponent, DataDumpSelectorModalComponent
    ],
    exports: [],
    providers: []
})
export class DatasetCatalogModule { }