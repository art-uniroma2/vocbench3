import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CodemirrorModule } from '@ctrl/ngx-codemirror';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { LoadShapesModalComponent } from '../shacl/load-shapes-modal.component';
import { ShaclBatchValidationModalComponent } from '../shacl/shacl-batch-validation-modal.component';
import { ShaclReportEditorComponent } from '../shacl/shaclReportEditorComponent';
import { SharedModule } from './sharedModule';


@NgModule({
    imports: [
        CodemirrorModule,
        CommonModule,
        DragDropModule,
        FormsModule,
        NgbDropdownModule,
        SharedModule,
        TranslateModule,
    ],
    declarations: [
        LoadShapesModalComponent,
        ShaclBatchValidationModalComponent,
        ShaclReportEditorComponent
    ],
    exports: [],
    providers: []
})
export class ShaclModule { }