import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { AlignFromFileComponent } from '../alignment/alignmentValidation/alignFromFileComponent';
import { AlignFromRemoteSystemComponent } from '../alignment/alignmentValidation/align-from-remote-system.component';
import { AlignmentManagementComponent } from '../alignment/alignmentValidation/alignment-management.component';
import { AlignmentValidationComponent } from '../alignment/alignmentValidation/alignment-validation.component';
import { CreateRemoteAlignmentTaskModalComponent } from '../alignment/alignmentValidation/alignmentValidationModals/create-remote-alignment-task-modal.component';
import { MapleDatasetComponent } from '../alignment/alignmentValidation/alignmentValidationModals/mapleDatasetComponent';
import { MaplePairingComponent } from '../alignment/alignmentValidation/alignmentValidationModals/maple-pairing.component';
import { RemoteSystemConfigurationsAdministrationComponent } from '../alignment/alignmentValidation/alignmentValidationModals/remote-system-config-admin-modal.component';
import { RemoteSystemSettingsModalComponent } from '../alignment/alignmentValidation/alignmentValidationModals/remote-system-settings-modal.component';
import { SynonymizerDetailsModalComponent } from '../alignment/alignmentValidation/alignmentValidationModals/synonymizer-details-modal.component';
import { SharedModule } from './sharedModule';

@NgModule({
  imports: [
    CommonModule,
    DragDropModule,
    FormsModule,
    NgbDropdownModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    AlignmentValidationComponent,
    AlignFromFileComponent,
    AlignFromRemoteSystemComponent,
    AlignmentManagementComponent,
    CreateRemoteAlignmentTaskModalComponent,
    MapleDatasetComponent,
    MaplePairingComponent,
    RemoteSystemSettingsModalComponent,
    RemoteSystemConfigurationsAdministrationComponent,
    SynonymizerDetailsModalComponent,
  ],
  exports: [AlignFromRemoteSystemComponent],
  providers: []
})
export class AlignmentModule { }