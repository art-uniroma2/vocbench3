import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { D3Service } from '../graph/d3/d3.services';
import { DraggableDirective } from '../graph/d3/draggable.directive';
import { ZoomableDirective } from '../graph/d3/zoomable.directive';
import { DataGraphPanelComponent } from '../graph/impl/data-graph/data-graph-panel.component';
import { DataGraphComponent } from '../graph/impl/data-graph/data-graph.component';
import { DataLinkComponent } from '../graph/impl/data-graph/data-link.component';
import { DataNodeComponent } from '../graph/impl/data-graph/data-node.component';
import { ModelGraphPanelComponent } from '../graph/impl/model-graph/model-graph-panel.component';
import { ModelGraphComponent } from '../graph/impl/model-graph/model-graph.component';
import { ModelLinkComponent } from '../graph/impl/model-graph/model-link.component';
import { ModelNodeComponent } from '../graph/impl/model-graph/model-node.component';
import { UmlGraphComponent } from '../graph/impl/uml-graph/uml-graph.component';
import { UmlGraphPanelComponent } from '../graph/impl/uml-graph/uml-graph-panel.component';
import { UmlLinkComponent } from '../graph/impl/uml-graph/uml-link.component';
import { UmlNodeComponent } from '../graph/impl/uml-graph/uml-node.component';
import { DataGraphSettingsModalComponent } from '../graph/modals/data-graph-settings-modal.component';
import { GraphModalComponent } from "../graph/modals/graph-modal.component";
import { GraphModalServices } from '../graph/modals/graph-modal.service';
import { LinksFilterModalComponent } from '../graph/modals/links-filter-modal.component';
import { ForceControlPanelComponent } from '../graph/widget/force-control-panel.component';
import { ResourceDetailsPanelComponent } from '../graph/widget/resource-details-panel.component';
import { PreferencesModule } from './preferencesModule';
import { SharedModule } from './sharedModule';

@NgModule({
  imports: [
    CommonModule,
    DragDropModule,
    FormsModule,
    NgbDropdownModule,
    SharedModule,
    PreferencesModule,
    TranslateModule
  ],
  declarations: [
    DataGraphComponent,
    DataGraphPanelComponent,
    DataGraphSettingsModalComponent,
    DataLinkComponent,
    DataNodeComponent,
    DraggableDirective,
    ForceControlPanelComponent,
    GraphModalComponent,
    LinksFilterModalComponent,
    ModelGraphPanelComponent,
    ModelGraphComponent,
    ModelLinkComponent,
    ModelNodeComponent,
    ResourceDetailsPanelComponent,
    UmlGraphComponent,
    UmlGraphPanelComponent,
    UmlLinkComponent,
    UmlNodeComponent,
    ZoomableDirective,
  ],
  exports: [],
  providers: [
    D3Service,
    GraphModalServices
  ]
})
export class GraphModule { }