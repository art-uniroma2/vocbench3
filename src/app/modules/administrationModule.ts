import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule, NgbPopoverModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { AdministrationComponent } from "../administration/administration.component";
import { adminRouting } from "../administration/administrationRoutes";
import { GroupEditorModalComponent } from "../administration/groups-administration/group-editor-modal.component";
import { GroupsAdministrationComponent } from "../administration/groups-administration/groups-administration.component";
import { GroupSelectorModalComponent } from '../administration/groups-administration/group-selector-modal.component';
import { ProjectUsersManagerComponent } from "../administration/projects-administration/project-users-manager.component";
import { ProjectGroupsManagerComponent } from "../administration/projects-administration/projectGroupsManagerComponent";
import { ProjectsAdministrationComponent } from "../administration/projects-administration/projectsAdministrationComponent";
import { ProjectSettingsComponent } from "../administration/projects-administration/project-settings.component";
import { ProjectUsersBindingsTableComponent } from '../administration/projects-administration/projectUsersBindingsTableComponent';
import { ResViewProjectSettingsComponent } from '../administration/projects-administration/res-view-project-settings.component';
import { UserProjBindingModalComponent } from "../administration/projects-administration/user-proj-binding-modal.component";
import { CapabilityEditorModalComponent } from "../administration/roles-administration/capability-editor-modal.component";
import { ImportRoleModalComponent } from "../administration/roles-administration/import-role-modal.component";
import { RoleDescriptionModalComponent } from '../administration/roles-administration/role-description-modal.component';
import { RolesAdministrationComponent } from "../administration/roles-administration/roles-administration.component";
import { RoleSelectorModalComponent } from '../administration/roles-administration/role-selector-modal.component';
import { SettingsMgrConfigComponent } from '../administration/system-configuration/settings-mgr-config.component';
import { SystemConfigurationComponent } from '../administration/system-configuration/systemConfigurationComponent';
import { UserDefaultsEditorComponent } from '../administration/system-configuration/userDefaultsEditorComponent';
import { ForcePasswordModalComponent } from '../administration/users-administration/force-password-modal.component';
import { UserCreateModalComponent } from "../administration/users-administration/user-create-modal.component";
import { UserDetailsPanelComponent } from '../administration/users-administration/user-details-panel.component';
import { UsersAdministrationComponent } from "../administration/users-administration/users-administration.component";
import { UsersListComponent } from '../administration/users-administration/users-list.component';
import { PreferencesModule } from './preferencesModule';
import { SharedModule } from './sharedModule';
import { TreeAndListModule } from './treeAndListModule';
import { UserModule } from './userModule';


@NgModule({
  imports: [
    adminRouting,
    CommonModule,
    DragDropModule,
    FormsModule,
    NgbDropdownModule,
    NgbPopoverModule,
    NgbTooltipModule,
    PreferencesModule,
    SharedModule,
    TranslateModule,
    TreeAndListModule,
    UserModule
  ],
  declarations: [
    AdministrationComponent,
    GroupsAdministrationComponent,
    ProjectGroupsManagerComponent,
    ProjectsAdministrationComponent,
    // ProjectDefaultsEditorComponent,
    ProjectSettingsComponent,
    ProjectUsersBindingsTableComponent,
    ProjectUsersManagerComponent,
    ResViewProjectSettingsComponent,
    RolesAdministrationComponent,
    SettingsMgrConfigComponent,
    SystemConfigurationComponent,
    UsersAdministrationComponent,
    UsersListComponent,
    UserDefaultsEditorComponent,
    UserDetailsPanelComponent,
    //modals
    CapabilityEditorModalComponent,
    ForcePasswordModalComponent,
    GroupEditorModalComponent,
    GroupSelectorModalComponent,
    ImportRoleModalComponent,
    RoleDescriptionModalComponent,
    RoleSelectorModalComponent,
    UserCreateModalComponent,
    UserProjBindingModalComponent,
  ],
  exports: [AdministrationComponent],
  providers: []
})
export class AdministrationModule { }