import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { DatatypeListNodeComponent } from '../structures/lists/datatype/datatype-list-node.component';
import { DatatypeListPanelComponent } from '../structures/lists/datatype/datatype-list-panel.component';
import { DatatypeListComponent } from '../structures/lists/datatype/datatype-list.component';
import { InstanceListNodeComponent } from '../structures/lists/instance/instance-list-node.component';
import { InstanceListPanelComponent } from '../structures/lists/instance/instance-list-panel.component';
import { InstanceListSettingsModalComponent } from '../structures/lists/instance/instance-list-settings-modal.component';
import { InstanceListComponent } from '../structures/lists/instance/instance-list.component';
import { LexicalEntryListNodeComponent } from '../structures/lists/lexical-entry/lexical-entry-list-node.component';
import { LexicalEntryListPanelComponent } from '../structures/lists/lexical-entry/lexical-entry-list-panel.component';
import { LexicalEntryListSettingsModalComponent } from '../structures/lists/lexical-entry/lexical-entry-list-settings-modal.component';
import { LexicalEntryListComponent } from '../structures/lists/lexical-entry/lexical-entry-list.component';
import { LexiconListNodeComponent } from '../structures/lists/lexicon/lexicon-list-node.component';
import { LexiconListPanelComponent } from '../structures/lists/lexicon/lexicon-list-panel.component';
import { LexiconListComponent } from '../structures/lists/lexicon/lexicon-list.component';
import { SchemeListNodeComponent } from '../structures/lists/scheme/scheme-list-node.component';
import { SchemeListPanelComponent } from '../structures/lists/scheme/scheme-list-panel.component';
import { SchemeListComponent } from '../structures/lists/scheme/scheme-list.component';
import { TranslationSetListNodeComponent } from '../structures/lists/translation-set/translationset-list-node.component';
import { TranslationSetListPanelComponent } from '../structures/lists/translation-set/translationset-list-panel.component';
import { TranslationSetListComponent } from '../structures/lists/translation-set/translationset-list.component';
import { MultiSubjectEnrichmentHelper } from '../structures/multiSubjectEnrichmentHelper';
import { AdvancedSearchModalComponent } from '../structures/search-bar/advanced-search-modal.component';
import { CustomSearchModalComponent } from '../structures/search-bar/custom-search-modal.component';
import { LoadCustomSearchModalComponent } from '../structures/search-bar/load-custom-search-modal.component';
import { SearchBarComponent } from '../structures/search-bar/search-bar.component'; //not exported, used just in this module
import { SearchSettingsModalComponent } from '../structures/search-bar/search-settings-modal.component';
import { TabsetPanelComponent } from '../structures/structure-tabset/structure-tabset.component';
import { TreeListSettingsModalComponent } from '../structures/structure-tabset/tree-list-settings-modal.component';
import { ClassIndividualTreePanelComponent } from '../structures/trees/class/class-individual-tree-panel.component';
import { ClassIndividualTreeComponent } from '../structures/trees/class/class-individual-tree.component';
import { ClassTreeNodeComponent } from '../structures/trees/class/class-tree-node.component';
import { ClassTreePanelComponent } from '../structures/trees/class/class-tree-panel.component';
import { ClassTreeSettingsModalComponent } from '../structures/trees/class/class-tree-settings-modal.component';
import { ClassTreeComponent } from '../structures/trees/class/class-tree.component';
import { LexicalSenseSelectorComponent } from '../structures/trees/class/lexical-sense-selector.component';
import { CollectionTreeNodeComponent } from '../structures/trees/collection/collection-tree-node.component';
import { CollectionTreePanelComponent } from '../structures/trees/collection/collection-tree-panel.component';
import { CollectionTreeComponent } from '../structures/trees/collection/collection-tree.component';
import { AddToSchemeModalComponent } from '../structures/trees/concept/add-to-scheme-modal.component';
import { ConceptTreeNodeComponent } from '../structures/trees/concept/concept-tree-node.component';
import { ConceptTreePanelComponent } from '../structures/trees/concept/concept-tree-panel.component';
import { ConceptTreeSettingsModalComponent } from '../structures/trees/concept/concept-tree-settings-modal.component';
import { ConceptTreeComponent } from '../structures/trees/concept/concept-tree.component';
import { OffSchemeSearchResolutionModalComponent } from '../structures/trees/concept/off-scheme-search-resolution-modal.component';
import { CustomTreeNodeComponent } from '../structures/trees/custom/custom-tree-node.component';
import { CustomTreePanelComponent } from '../structures/trees/custom/custom-tree-panel.component';
import { CustomTreeSettingsComponent } from '../structures/trees/custom/custom-tree-settings.component';
import { CustomTreeComponent } from '../structures/trees/custom/custom-tree.component';
import { PropertyTreeNodeComponent } from '../structures/trees/property/property-tree-node.component';
import { PropertyTreePanelComponent } from '../structures/trees/property/property-tree-panel.component';
import { PropertyTreeComponent } from '../structures/trees/property/property-tree.component';
import { LexicalSenseListModalComponent } from '../modal-dialogs/browsing-modals/lexical-sense-list-modal/lexical-sense-list-modal.component';
import { TranslationSetModalComponent } from '../modal-dialogs/browsing-modals/translation-set-modal/translation-set-modal.component';
import { PreferencesModule } from './preferencesModule';
import { SharedModule } from './sharedModule';

@NgModule({
    imports: [
        AutocompleteLibModule,
        CommonModule,
        DragDropModule,
        FormsModule,
        NgbDropdownModule,
        PreferencesModule,
        RouterModule,
        SharedModule,
        TranslateModule
    ],
    declarations: [
        AddToSchemeModalComponent,
        AdvancedSearchModalComponent,
        ClassIndividualTreeComponent,
        ClassIndividualTreePanelComponent,
        ClassTreeComponent,
        ClassTreeNodeComponent,
        ClassTreePanelComponent,
        ClassTreeSettingsModalComponent,
        CollectionTreeComponent,
        CollectionTreeNodeComponent,
        CollectionTreePanelComponent,
        ConceptTreeComponent,
        ConceptTreeNodeComponent,
        ConceptTreePanelComponent,
        ConceptTreeSettingsModalComponent,
        CustomSearchModalComponent,
        CustomTreeComponent,
        CustomTreeNodeComponent,
        CustomTreePanelComponent,
        CustomTreeSettingsComponent,
        DatatypeListComponent,
        DatatypeListNodeComponent,
        DatatypeListPanelComponent,
        InstanceListComponent,
        InstanceListNodeComponent,
        InstanceListPanelComponent,
        InstanceListSettingsModalComponent,
        LexicalEntryListComponent,
        LexicalEntryListNodeComponent,
        LexicalEntryListPanelComponent,
        LexicalEntryListSettingsModalComponent,
        LexicalSenseListModalComponent,
        LexicalSenseSelectorComponent,
        LexiconListComponent,
        LexiconListNodeComponent,
        LexiconListPanelComponent,
        LoadCustomSearchModalComponent,
        OffSchemeSearchResolutionModalComponent,
        PropertyTreeComponent,
        PropertyTreeNodeComponent,
        PropertyTreePanelComponent,
        SchemeListComponent,
        SchemeListNodeComponent,
        SchemeListPanelComponent,
        SearchBarComponent,
        SearchSettingsModalComponent,
        TabsetPanelComponent,
        TranslationSetListComponent,
        TranslationSetListNodeComponent,
        TranslationSetListPanelComponent,
        TranslationSetModalComponent,
        TreeListSettingsModalComponent,
    ],
    exports: [
        ClassIndividualTreeComponent,
        ClassIndividualTreePanelComponent,
        ClassTreeComponent,
        ClassTreePanelComponent,
        CollectionTreeComponent,
        CollectionTreePanelComponent,
        ConceptTreeComponent,
        ConceptTreePanelComponent,
        CustomTreeSettingsComponent,
        DatatypeListComponent,
        DatatypeListPanelComponent,
        InstanceListComponent,
        InstanceListPanelComponent,
        LexicalEntryListComponent,
        LexicalEntryListPanelComponent,
        LexicalSenseSelectorComponent,
        LexiconListComponent,
        LexiconListPanelComponent,
        PropertyTreeComponent,
        PropertyTreePanelComponent,
        SchemeListComponent,
        SchemeListPanelComponent,
        TabsetPanelComponent,
        TranslationSetListPanelComponent,
    ],
    providers: [MultiSubjectEnrichmentHelper]
})
export class TreeAndListModule { }