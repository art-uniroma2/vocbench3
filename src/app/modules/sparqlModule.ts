import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { ExportResultAsRdfModalComponent } from '../sparql/export-as-rdf-modal.component';
import { QueryParameterizerModalComponent } from '../sparql/queryParameterization/query-parameterizer-modal.component';
import { QueryResultsComponent } from '../sparql/query-results.component';
import { SparqlComponent } from '../sparql/sparql-component.component';
import { SparqlTabComponent } from '../sparql/sparql-tab.component';
import { SparqlTabParametrizedComponent } from '../sparql/sparql-tab-parametrized.component';
import { SharedModule } from './sharedModule';

@NgModule({
  imports: [
    CommonModule,
    DragDropModule,
    FormsModule,
    NgbDropdownModule,
    SharedModule,
    TranslateModule,
  ],
  declarations: [
    SparqlComponent,
    SparqlTabComponent,
    SparqlTabParametrizedComponent,
    QueryResultsComponent,
    //modals
    ExportResultAsRdfModalComponent,
    QueryParameterizerModalComponent
  ],
  exports: [SparqlComponent],
  providers: []
})
export class SparqlModule { }