import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { CreateProjectComponent } from "../project/create-project/create-project.component";
import { OpenAllProjReportModalComponent } from '../project/open-projects-report-modal.component';
import { ACLEditorModalComponent } from "../project/projectACL/acl-editor-modal.component";
import { ProjectACLModalComponent } from '../project/projectACL/project-acl-modal.component';
import { ProjectComponent } from "../project/project.component";
import { ProjectListModalComponent } from "../project/project-list-modal.component";
import { ProjectPropertiesModalComponent } from "../project/project-properties-modal.component";
import { ProjSettingsEditorModalComponent } from '../project/project-settings-editor/project-settings-editor-modal.component';
import { ProjectTableConfigModalComponent } from "../project/project-table-config/project-table-config-modal.component";
import { DeleteRemoteRepoModalComponent } from '../project/remoteRepositories/delete-remote-repo-modal.component';
import { DeleteRepositoryReportModalComponent } from '../project/remoteRepositories/delete-repository-report-modal.component';
import { RemoteRepoEditorModalComponent } from "../project/remoteRepositories/remote-repo-editor-modal.component";
import { PreferencesModule } from './preferencesModule';
import { SharedModule } from './sharedModule';

@NgModule({
    imports: [
        CommonModule,
        DragDropModule,
        FormsModule,
        NgbDropdownModule,
        NgbPopoverModule,
        PreferencesModule,
        SharedModule,
        TranslateModule
    ],
    declarations: [
        CreateProjectComponent,
        ProjectComponent,
        //modals
        ACLEditorModalComponent,
        DeleteRemoteRepoModalComponent,
        DeleteRepositoryReportModalComponent,
        OpenAllProjReportModalComponent,
        ProjectACLModalComponent,
        ProjectListModalComponent,
        ProjectPropertiesModalComponent,
        ProjSettingsEditorModalComponent,
        ProjectTableConfigModalComponent,
        RemoteRepoEditorModalComponent,
    ],
    exports: [ProjectComponent],
    providers: []
})
export class ProjectModule { }