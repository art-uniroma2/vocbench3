import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { constituentFeatureRendererComponent } from '../resource-view/lexicographer-view/constituents/constituent-feature-renderer.component';
import { ConstituentListComponent } from '../resource-view/lexicographer-view/constituents/constituent-list.component';
import { ConstituentComponent } from '../resource-view/lexicographer-view/constituents/constituent.component';
import { LexEntryComponent } from '../resource-view/lexicographer-view/lexical-entry/lex-entry.component';
import { LexicalFormComponent } from '../resource-view/lexicographer-view/lexical-form/lexical-form.component';
import { MorphosyntacticPropComponent } from '../resource-view/lexicographer-view/lexical-form/morphosyntactic-prop.component';
import { PhoneticRepComponent } from '../resource-view/lexicographer-view/lexical-form/phonetic-rep.component';
import { CategoryComponent } from '../resource-view/lexicographer-view/lexical-relation/categoryComponent';
import { EntryRelationComponent } from '../resource-view/lexicographer-view/lexical-relation/entry-relation.component';
import { LexicalRelationModalComponent } from '../resource-view/lexicographer-view/lexical-relation/lexical-relation-modal.component';
import { SenseRelationComponent } from '../resource-view/lexicographer-view/lexical-relation/sense-relation.component';
import { ConceptReferenceComponent } from '../resource-view/lexicographer-view/lexical-sense/conceptReferenceComponent';
import { InlineDefinitionComponent } from '../resource-view/lexicographer-view/lexical-sense/inlineDefinitionComponent';
import { LexicalSenseComponent } from '../resource-view/lexicographer-view/lexical-sense/lexicalSenseComponent';
import { SenseReferenceComponent } from '../resource-view/lexicographer-view/lexical-sense/sense-reference.component';
import { LexicographerViewComponent } from '../resource-view/lexicographer-view/lexicographer-view.component';
import { LexViewHelper } from '../resource-view/lexicographer-view/LexViewHelper';
import { LexViewModalService } from '../resource-view/lexicographer-view/lexViewModalService';
import { EntryReferenceComponent } from '../resource-view/lexicographer-view/subterms/entryReferenceComponent';
import { SubtermComponent } from '../resource-view/lexicographer-view/subterms/subterm.component';
import { ResViewSectionFilterDefaultsModalComponent } from '../resource-view/res-view-settings/res-view-section-filter-defaults-modal.component';
import { ResViewSettingsModalComponent } from '../resource-view/res-view-settings/res-view-settings-modal.component';
import { ResourceViewTabContainerComponent } from '../resource-view/resource-view-container/resource-view-container.component';
import { AddManuallyValueModalComponent } from '../resource-view/resource-view-editor/res-view-modals/add-manually-value-modal.component';
import { AddPropertyValueModalComponent } from '../resource-view/resource-view-editor/res-view-modals/add-property-value-modal.component';
import { BrowseExternalResourceModalComponent } from '../resource-view/resource-view-editor/res-view-modals/browse-external-resource-modal.component';
import { ChangePropertyModalComponent } from '../resource-view/resource-view-editor/res-view-modals/change-property-modal.component';
import { ClassListCreatorModalComponent } from '../resource-view/resource-view-editor/res-view-modals/class-list-creator-modal.component';
import { ConstituentListCreatorModalComponent } from '../resource-view/resource-view-editor/res-view-modals/constituent-list-creator-modal.component';
import { CopyLocalesModalComponent } from '../resource-view/resource-view-editor/res-view-modals/copy-locale-modal.component';
import { DataRangeEditorModalComponent } from '../resource-view/resource-view-editor/res-view-modals/data-range-editor-modal.component';
import { DataRangeEditorComponent } from '../resource-view/resource-view-editor/res-view-modals/data-range-editor.component';
import { DatatypeFacetsEditorComponent } from '../resource-view/resource-view-editor/res-view-modals/datatype-facets-editor.component';
import { DataTypeRestrictionsModalComponent } from '../resource-view/resource-view-editor/res-view-modals/datatype-restrictions-modal.component';
import { InstanceListCreatorModalComponent } from '../resource-view/resource-view-editor/res-view-modals/instance-list-creator-modal.component';
import { PropertyChainCreatorModalComponent } from '../resource-view/resource-view-editor/res-view-modals/property-chain-creator-modal.component';
import { RdfsMembersModalComponent } from '../resource-view/resource-view-editor/res-view-modals/rdfs-members-modal.component';
import { ResViewModalServices } from '../resource-view/resource-view-editor/res-view-modals/resViewModalServices';
import { ResourceRenameComponent } from '../resource-view/resource-view-editor/resource-rename.component';
import { ResourceViewContextMenuComponent } from '../resource-view/resource-view-editor/resource-view-ctx-menu.component';
import { ResourceViewEditorComponent } from '../resource-view/resource-view-editor/resource-view-editor.component';
import { BroadersSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/broaders-section-renderer.component';
import { ClassAxiomSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/class-axiom-section-renderer.component';
import { CollectionsSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/collections-section-renderer.component';
import { ConstituentsSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/constituents-section-renderer.component';
import { CustomSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/custom-section-renderer.component';
import { DatatypeDefinitionSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/datatype-definition-section-renderer.component';
import { DenotationsSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/denotations-section-renderer.component';
import { DisjointPropertiesSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/disjoint-properties-section-renderer.component';
import { DomainsSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/domains-section-renderer.component';
import { EquivalentPropertiesSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/equivalent-properties-section-renderer.component';
import { EvokedLexicalConceptsSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/evoked-lexical-concepts-section-renderer.component';
import { FormRepresentationsSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/form-representations-section-renderer.component';
import { ImportSectionPredObjRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/imports-section-pred-obj-renderer.component';
import { ImportsSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/imports-section-renderer.component';
import { LabelRelationsSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/label-relations-section-renderer.component';
import { LexicalFormsSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/lexical-forms-section-renderer.component';
import { LexicalSensesSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/lexical-senses-section-renderer.component';
import { LexicalizationsSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/lexicalizations-section-renderer.component';
import { MembersOrderedSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/members-ordered-section-renderer.component';
import { MembersSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/members-section-renderer.component';
import { NotesSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/notes-section-renderer.component';
import { PropertiesSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/properties-section-renderer.component';
import { PropertyChainRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/property-chain-section-renderer.component';
import { PropertyFacetsSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/property-facets-section-renderer.component';
import { RangesSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/ranges-section-renderer.component';
import { RdfsMembersSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/rdfs-members-section-renderer.component';
import { SchemesSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/schemes-section-renderer.component';
import { SubtermsSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/subterms-section-renderer.component';
import { SuperPropertiesSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/super-properties-section-renderer.component';
import { TopConceptsSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/top-concepts-section-renderer.component';
import { TypesSectionRendererComponent } from '../resource-view/resource-view-editor/section-renderer/impl/types-section-renderer.component';
import { LexicalizationEnrichmentHelper } from '../resource-view/resource-view-editor/section-renderer/lexicalizationEnrichmentHelper';
import { PredicateObjectsRendererComponent } from '../resource-view/resource-view-editor/section-renderer/predicate-objects-renderer.component';
import { TimeMachineModalComponent } from '../resource-view/resource-view-editor/time-machine/time-machine-modal.component';
import { ChartsRendererComponent } from '../resource-view/resource-view-editor/value-renderer/cvRenderer/charts-renderer.component';
import { CustomViewsRendererComponent } from '../resource-view/resource-view-editor/value-renderer/cvRenderer/custom-view-renderer.component';
import { CvValueRendererComponent } from '../resource-view/resource-view-editor/value-renderer/cvRenderer/cv-value-renderer.component';
import { MapRendererComponent } from '../resource-view/resource-view-editor/value-renderer/cvRenderer/map-renderer.component';
import { SingleValueRendererComponent } from '../resource-view/resource-view-editor/value-renderer/cvRenderer/single-value-renderer.component';
import { VectorRendererComponent } from '../resource-view/resource-view-editor/value-renderer/cvRenderer/vector-renderer.component';
import { EditableResourceComponent } from '../resource-view/resource-view-editor/value-renderer/editable-resource.component';
import { ResourceViewValueRendererComponent } from '../resource-view/resource-view-editor/value-renderer/resource-view-value-renderer.component';
import { ResourceViewModalComponent } from '../resource-view/resource-view-modal.component';
import { ResourceViewTabComponent } from '../resource-view/resource-view-tab/resource-view-tab.component';
import { ResourceViewTabsetComponent } from '../resource-view/resource-view-tabset/resource-view-tabset.component';
import { LanguageBoxComponent } from '../resource-view/term-view/language-box/language-box.component';
import { LanguageDefinitionComponent } from '../resource-view/term-view/language-definition/language-definition.component';
import { LanguageTermComponent } from '../resource-view/term-view/language-term/language-term.component';
import { TermViewComponent } from '../resource-view/term-view/term-view.component';
import { ResourceTripleEditorComponent } from '../resource-view/triple-editor/resource-triple-editor.component';
import { PreferencesModule } from './preferencesModule';
import { SharedModule } from './sharedModule';
import { TreeAndListModule } from "./treeAndListModule";

@NgModule({
  imports: [
    CommonModule,
    DragDropModule,
    FormsModule,
    NgbDropdownModule,
    NgbPopoverModule,
    PreferencesModule,
    SharedModule,
    TranslateModule,
    TreeAndListModule,
  ],
  declarations: [
    //CodeView
    ResourceTripleEditorComponent,
    //LexView
    CategoryComponent,
    ConceptReferenceComponent,
    ConstituentComponent,
    constituentFeatureRendererComponent,
    ConstituentListComponent,
    EntryReferenceComponent,
    EntryRelationComponent,
    InlineDefinitionComponent,
    LexEntryComponent,
    LexicalFormComponent,
    LexicalSenseComponent,
    LexicographerViewComponent,
    LexicalRelationModalComponent,
    MorphosyntacticPropComponent,
    PhoneticRepComponent,
    SenseReferenceComponent,
    SenseRelationComponent,
    SubtermComponent,
    //ResourceView
    BroadersSectionRendererComponent,
    ClassAxiomSectionRendererComponent,
    CollectionsSectionRendererComponent,
    ConstituentsSectionRendererComponent,
    CustomSectionRendererComponent,
    DatatypeDefinitionSectionRendererComponent,
    DenotationsSectionRendererComponent,
    DisjointPropertiesSectionRendererComponent,
    DomainsSectionRendererComponent,
    EditableResourceComponent,
    EquivalentPropertiesSectionRendererComponent,
    EvokedLexicalConceptsSectionRendererComponent,
    FormRepresentationsSectionRendererComponent,
    ImportSectionPredObjRendererComponent,
    ImportsSectionRendererComponent,
    LabelRelationsSectionRendererComponent,
    LexicalizationsSectionRendererComponent,
    LexicalFormsSectionRendererComponent,
    LexicalSensesSectionRendererComponent,
    MembersOrderedSectionRendererComponent,
    MembersSectionRendererComponent,
    NotesSectionRendererComponent,
    CustomViewsRendererComponent,
    PredicateObjectsRendererComponent,
    PropertiesSectionRendererComponent,
    PropertyChainRendererComponent,
    PropertyFacetsSectionRendererComponent,
    RangesSectionRendererComponent,
    RdfsMembersSectionRendererComponent,
    ResourceRenameComponent,
    ResourceViewEditorComponent,
    ResourceViewContextMenuComponent,
    ResourceViewModalComponent,
    ResourceViewValueRendererComponent,
    SchemesSectionRendererComponent,
    SubtermsSectionRendererComponent,
    SuperPropertiesSectionRendererComponent,
    TopConceptsSectionRendererComponent,
    TypesSectionRendererComponent,
    //TermView
    LanguageBoxComponent,
    LanguageDefinitionComponent,
    LanguageTermComponent,
    TermViewComponent,
    //misc
    ResourceViewTabsetComponent,
    ResourceViewTabComponent,
    ResourceViewTabContainerComponent,
    //modals
    AddManuallyValueModalComponent,
    AddPropertyValueModalComponent,
    BrowseExternalResourceModalComponent,
    ClassListCreatorModalComponent,
    ConstituentListCreatorModalComponent,
    CopyLocalesModalComponent,
    ChangePropertyModalComponent,
    DataRangeEditorModalComponent,
    DataRangeEditorComponent,
    DataTypeRestrictionsModalComponent,
    DatatypeFacetsEditorComponent,
    InstanceListCreatorModalComponent,
    PropertyChainCreatorModalComponent,
    RdfsMembersModalComponent,
    ResViewSectionFilterDefaultsModalComponent,
    ResViewSettingsModalComponent,
    TimeMachineModalComponent,
    ChartsRendererComponent,
    MapRendererComponent,
    SingleValueRendererComponent,
    VectorRendererComponent,
    CvValueRendererComponent
  ],
  exports: [
    ResourceViewEditorComponent,
    ResourceViewTabsetComponent
  ],
  providers: [
    LexicalizationEnrichmentHelper,
    LexViewHelper,
    LexViewModalService,
    ResViewModalServices,
  ]
})
export class ResourceViewModule { }