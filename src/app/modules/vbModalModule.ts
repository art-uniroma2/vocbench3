import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { MappingPropertySelectionModalComponent } from '../alignment/alignmentValidation/alignmentValidationModals/mapping-property-selection-modal.component';
import { ValidationReportModalComponent } from '../alignment/alignmentValidation/alignmentValidationModals/validation-report-modal.component';
import { ValidationSettingsModalComponent } from '../alignment/alignmentValidation/alignmentValidationModals/validation-settings-modal.component';
import { AssistedSearchModalComponent } from '../alignment/resourceAlignment/assisted-search-modal.component';
import { AssistedSearchResultModalComponent } from '../alignment/resourceAlignment/assisted-search-result-modal.component';
import { ResourceAlignmentModalComponent } from '../alignment/resourceAlignment/resource-alignment-modal.component';
import { InferenceExplanationModalComponent } from '../icv/owlConsistencyViolations/inference-explanation-modal.component';
import { PrefixNamespaceModalComponent } from '../metadata/namespacesAndImports/prefix-namespace-modal.component';
import { ProjectSelectionModalComponent } from '../project/projectListPanel/project-selection-modal.component';
import { HelperModalComponent } from '../widget/codemirror/manchester-editor/modal/helper-modal.component';
import { AlertModalComponent } from '../modal-dialogs/basic-modals/alert-modal/alert-modal.component';
import { BasicModalServices } from '../modal-dialogs/basic-modals/basic-modals.service';
import { ConfirmCheckModalComponent } from '../modal-dialogs/basic-modals/confirm-modal/confirm-check-modal.component';
import { ConfirmModalComponent } from '../modal-dialogs/basic-modals/confirm-modal/confirm-modal.component';
import { DownloadModalComponent } from '../modal-dialogs/basic-modals/download-modal/download-modal.component';
import { FilePickerModalComponent } from '../modal-dialogs/basic-modals/file-picker-modal/file-picker-modal.component';
import { PromptModalComponent } from '../modal-dialogs/basic-modals/prompt-modal/prompt-modal.component';
import { PromptPrefixedModalComponent } from '../modal-dialogs/basic-modals/prompt-modal/prompt-prefixed-modal.component';
import { PromptPropertiesModalComponent } from '../modal-dialogs/basic-modals/prompt-modal/prompt-properties-modal.component';
import { CustomFormSelectionModalComponent } from '../modal-dialogs/basic-modals/selection-modal/custom-form-selection-modal.component';
import { ResourceSelectionModalComponent } from '../modal-dialogs/basic-modals/selection-modal/resource-selection-modal.component';
import { SelectionModalComponent } from '../modal-dialogs/basic-modals/selection-modal/selection-modal.component';
import { BrowsingModalServices } from '../modal-dialogs/browsing-modals/browsing-modals.service';
import { ClassIndividualTreeModalComponent } from '../modal-dialogs/browsing-modals/class-individual-tree-modal/class-individual-tree-modal.component';
import { ClassTreeModalComponent } from '../modal-dialogs/browsing-modals/class-tree-modal/class-tree-modal.component';
import { CollectionTreeModalComponent } from '../modal-dialogs/browsing-modals/collection-tree-modal/collection-tree-modal.component';
import { ConceptTreeModalComponent } from '../modal-dialogs/browsing-modals/concept-tree-modal/concept-tree-modal.component';
import { DatatypeListModalComponent } from '../modal-dialogs/browsing-modals/datatype-list-modal/datatype-list-modal.component';
import { InstanceListModalComponent } from '../modal-dialogs/browsing-modals/instance-list-modal/instance-list-modal.component';
import { LexicalEntryListModalComponent } from '../modal-dialogs/browsing-modals/lexical-entry-list-modal/lexical-entry-list-modal.component';
import { LexiconListModalComponent } from '../modal-dialogs/browsing-modals/lexicon-list-modal/lexicon-list-modal.component';
import { PropertyTreeModalComponent } from '../modal-dialogs/browsing-modals/property-tree-modal/property-tree-modal.component';
import { SchemeListModalComponent } from '../modal-dialogs/browsing-modals/scheme-list-modal/scheme-list-modal.component';
import { CreationModalServices } from '../modal-dialogs/creation-modals/creation-modals.service';
import { NewPlainLiteralModalComponent } from '../modal-dialogs/creation-modals/new-plain-literal-modal/new-plain-literal-modal.component';
import { NewConceptualizationCfModalComponent } from '../modal-dialogs/creation-modals/newResourceModal/ontolex/new-conceptualization-cf-modal.component';
import { NewLexiconCfModalComponent } from '../modal-dialogs/creation-modals/newResourceModal/ontolex/new-lexicon-cf-modal.component';
import { NewLexSenseCfModalComponent } from '../modal-dialogs/creation-modals/newResourceModal/ontolex/new-lex-sense-cf-modal.component';
import { NewOntoLexicalizationCfModalComponent } from '../modal-dialogs/creation-modals/newResourceModal/ontolex/new-ontolexicalization-cf-modal.component';
import { NewResourceCfModalComponent } from '../modal-dialogs/creation-modals/newResourceModal/shared/new-resource-cf-modal.component';
import { NewResourceWithLiteralCfModalComponent } from '../modal-dialogs/creation-modals/newResourceModal/shared/new-resource-with-literal-cf-modal.component';
import { NewConceptCfModalComponent } from '../modal-dialogs/creation-modals/newResourceModal/skos/new-concept-cf-modal.component';
import { NewConceptFromLabelModalComponent } from '../modal-dialogs/creation-modals/newResourceModal/skos/new-concept-from-label-modal.component';
import { NewXLabelModalComponent } from '../modal-dialogs/creation-modals/newResourceModal/skos/new-xlabel-modal.component';
import { SchemeSelectionComponent } from '../modal-dialogs/creation-modals/newResourceModal/skos/scheme-selection.component';
import { NewTypedLiteralModalComponent } from '../modal-dialogs/creation-modals/new-typed-literal-modal/new-typed-literal-modal.component';
import { NewValueModalComponent } from '../modal-dialogs/creation-modals/new-value-modal/new-value-modal.component';
import { LoadConfigurationModalComponent } from '../modal-dialogs/shared-modals/configuration-store-modal/load-configuration-modal.component';
import { StoreConfigurationModalComponent } from '../modal-dialogs/shared-modals/configuration-store-modal/store-configuration-modal.component';
import { ConverterPickerModalComponent } from '../modal-dialogs/shared-modals/converter-picker-modal/converter-picker-modal.component';
import { SignaturePickerModalComponent } from '../modal-dialogs/shared-modals/converter-picker-modal/signature-picker-modal.component';
import { DatetimePickerModalComponent } from '../modal-dialogs/shared-modals/datetime-picker-modal/datetime-picker-modal.component';
import { LanguageSelectorModalComponent } from '../modal-dialogs/shared-modals/languages-selector-modal/languages-selector-modal.component';
import { ManchesterExprModalComponent } from '../modal-dialogs/shared-modals/manchester-expr-modal/manchester-expr-modal.component';
import { PluginConfigModalComponent } from '../modal-dialogs/shared-modals/plugin-config-modal/plugin-config-modal.component';
import { RemoteAccessConfigModalComponent } from '../modal-dialogs/shared-modals/remote-access-config-modal/remote-access-config-modal.component';
import { RemoteRepoSelectionModalComponent } from '../modal-dialogs/shared-modals/remote-repo-selection-modal/remote-repo-selection-modal.component';
import { ResourcePickerModalComponent } from '../modal-dialogs/shared-modals/resource-picker-modal/resource-picker-modal.component';
import { SharedModalServices } from '../modal-dialogs/shared-modals/shared-modals.service';
import { StorageManagerModalComponent } from '../modal-dialogs/shared-modals/storage-manager-modal/storage-manager-modal.component';
import { UserSelectionModalComponent } from '../modal-dialogs/shared-modals/user-selection-modal/user-selection-modal.component';
import { CustomFormModule } from './customFormModule';
import { SharedModule } from './sharedModule';
import { TreeAndListModule } from './treeAndListModule';
import { UserModule } from './userModule';

@NgModule({
  imports: [
    CommonModule,
    CustomFormModule,
    DragDropModule,
    FormsModule,
    NgbDropdownModule,
    SharedModule,
    TranslateModule,
    TreeAndListModule,
    UserModule
  ],
  declarations: [
    AlertModalComponent,
    AssistedSearchModalComponent,
    AssistedSearchResultModalComponent,
    ClassIndividualTreeModalComponent,
    ClassTreeModalComponent,
    CollectionTreeModalComponent,
    ConceptTreeModalComponent,
    ConfirmCheckModalComponent,
    ConfirmModalComponent,
    ConverterPickerModalComponent,
    CustomFormSelectionModalComponent,
    DatatypeListModalComponent,
    DatetimePickerModalComponent,
    DownloadModalComponent,
    FilePickerModalComponent,
    HelperModalComponent,
    InferenceExplanationModalComponent,
    InstanceListModalComponent,
    LanguageSelectorModalComponent,
    LexicalEntryListModalComponent,
    LexiconListModalComponent,
    LoadConfigurationModalComponent,
    ManchesterExprModalComponent,
    MappingPropertySelectionModalComponent,
    NewConceptCfModalComponent,
    NewConceptFromLabelModalComponent,
    NewConceptualizationCfModalComponent,
    NewLexiconCfModalComponent,
    NewLexSenseCfModalComponent,
    NewOntoLexicalizationCfModalComponent,
    NewPlainLiteralModalComponent,
    NewResourceCfModalComponent,
    NewResourceWithLiteralCfModalComponent,
    NewTypedLiteralModalComponent,
    NewValueModalComponent,
    NewXLabelModalComponent,
    PluginConfigModalComponent,
    PrefixNamespaceModalComponent,
    ProjectSelectionModalComponent,
    PromptModalComponent,
    PromptPrefixedModalComponent,
    PromptPropertiesModalComponent,
    PropertyTreeModalComponent,
    RemoteAccessConfigModalComponent,
    RemoteRepoSelectionModalComponent,
    ResourceAlignmentModalComponent,
    ResourcePickerModalComponent,
    ResourceSelectionModalComponent,
    SchemeListModalComponent,
    SchemeSelectionComponent,
    SelectionModalComponent,
    SignaturePickerModalComponent,
    StorageManagerModalComponent,
    StoreConfigurationModalComponent,
    UserSelectionModalComponent,
    ValidationReportModalComponent,
    ValidationSettingsModalComponent
  ],
  exports: [],
  providers: [
    BasicModalServices,
    BrowsingModalServices,
    CreationModalServices,
    SharedModalServices,
  ]
})
export class VBModalModule { }