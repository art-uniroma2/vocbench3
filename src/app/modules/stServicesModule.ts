import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AdministrationServices } from "../services/administration.service";
import { AlignmentServices } from "../services/alignment.service";
import { AuthServices } from "../services/auth.service";
import { ClassesServices } from "../services/classes.service";
import { CODAServices } from "../services/coda.service";
import { CollaborationServices } from "../services/collaboration.service";
import { ConfigurationsServices } from "../services/configurations.service";
import { CustomFormsServices } from "../services/custom-forms.service";
import { CustomServiceServices } from '../services/custom-service.service';
import { CustomTreesServices } from '../services/custom-trees.service';
import { CustomViewsServices } from '../services/custom-views.service';
import { DatasetCatalogsServices } from '../services/dataset-catalogs.service';
import { DatasetMetadataServices } from "../services/dataset-metadata.service";
import { DatatypesServices } from '../services/datatypes.service';
import { EdoalServices } from '../services/edoal.services';
import { ExportServices } from "../services/export.service";
import { ExtensionsServices } from "../services/extensions.service";
import { GraphServices } from '../services/graph.service';
import { HistoryServices } from "../services/history.service";
import { IcvServices } from "../services/icv.service";
import { IndividualsServices } from "../services/individuals.service";
import { InputOutputServices } from "../services/input-output.service";
import { InvokableReportersServices } from '../services/invokable-reporters.service';
import { LexicographerViewServices } from '../services/lexicographer-view.service';
import { ManchesterServices } from "../services/manchester.service";
import { MapleServices } from '../services/maple.service';
import { MetadataRegistryServices } from "../services/metadata-registry.service";
import { MetadataServices } from "../services/metadata.service";
import { NotificationServices } from '../services/notification.service';
import { OntoLexLemonServices } from "../services/ontolex-lemon.service";
import { OntoManagerServices } from "../services/onto-manager.service";
import { ProjectServices } from "../services/projects.service";
import { PropertyServices } from "../services/properties.service";
import { RefactorServices } from "../services/refactor.service";
import { RemoteAlignmentServices } from '../services/remote-alignment.service';
import { RepositoriesServices } from "../services/repositories.service";
import { ResourceMetadataServices } from '../services/resource-metadata.service';
import { ResourcesServices } from "../services/resources.service";
import { ResourceViewServices } from "../services/resource-view.service";
import { SearchServices } from "../services/search.service";
import { ServicesServices } from "../services/services.service";
import { SettingsServices } from "../services/settings.service";
import { ShaclServices } from '../services/shacl.service';
import { Sheet2RDFServices } from "../services/sheet2rdf.service";
import { SkosDiffingServices } from '../services/skos-diffing.service';
import { SkosServices } from "../services/skos.service";
import { SkosxlServices } from "../services/skosxl.service";
import { SparqlServices } from "../services/sparql.service";
import { StorageServices } from '../services/storage.service';
import { UndoServices } from '../services/undo.service';
import { UserServices } from "../services/user.service";
import { UsersGroupsServices } from "../services/users-groups.service";
import { ValidationServices } from "../services/validation.service";
import { VersionsServices } from "../services/versions.service";
import { HttpManager } from '../utils/HttpManager';
import { StMetadataRegistry } from '../utils/STMetadataRegistry';
import { SepiaServices } from '../sep/sepiaServices';
import { SepiaBackendServices } from '../sep/sepiaBackendServices';

@NgModule({ declarations: [],
    exports: [], imports: [], providers: [
        AdministrationServices,
        AlignmentServices,
        AuthServices,
        ClassesServices,
        CODAServices,
        CollaborationServices,
        ConfigurationsServices,
        CustomFormsServices,
        CustomServiceServices,
        CustomTreesServices,
        CustomViewsServices,
        DatatypesServices,
        DatasetCatalogsServices,
        DatasetMetadataServices,
        EdoalServices,
        ExportServices,
        ExtensionsServices,
        GraphServices,
        HistoryServices,
        HttpManager,
        IcvServices,
        IndividualsServices,
        InputOutputServices,
        InvokableReportersServices,
        LexicographerViewServices,
        ManchesterServices,
        MapleServices,
        MetadataServices,
        MetadataRegistryServices,
        NotificationServices,
        OntoLexLemonServices,
        OntoManagerServices,
        ProjectServices,
        PropertyServices,
        RefactorServices,
        RemoteAlignmentServices,
        RepositoriesServices,
        ResourceMetadataServices,
        ResourcesServices,
        ResourceViewServices,
        SearchServices,
        ServicesServices,
        SettingsServices,
        ShaclServices,
        Sheet2RDFServices,
        SkosServices,
        SkosDiffingServices,
        SkosxlServices,
        SparqlServices,
        StMetadataRegistry,
        StorageServices,
        UndoServices,
        UserServices,
        UsersGroupsServices,
        ValidationServices,
        VersionsServices,
        SepiaServices,
        SepiaBackendServices,
        provideHttpClient(withInterceptorsFromDi())
    ] })
export class STServicesModule { }
