import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { ChangeWGraphModalComponent } from '../config/configBar/change-wgraph-modal.component';
import { ConfigBarComponent } from '../config/configBar/config-bar.component';
import { ExportDataComponent } from '../config/dataManagement/exportData/export-data.component';
import { FilterGraphsModalComponent } from '../config/dataManagement/exportData/filter-graphs/filter-graphs-modal.component';
import { LoadDataComponent } from '../config/dataManagement/loadData/loadDataComponent';
import { RefactorComponent } from '../config/dataManagement/refactor/refactor.component';
import { DumpCreationModalComponent } from '../config/dataManagement/versioning/dump-creation-modal.component';
import { VersioningComponent } from '../config/dataManagement/versioning/versioningComponent';
import { SharedModule } from './sharedModule';

@NgModule({
  imports: [
    CommonModule,
    DragDropModule,
    FormsModule,
    NgbDropdownModule,
    RouterModule,
    TranslateModule,
    SharedModule
  ],
  declarations: [
    ChangeWGraphModalComponent,
    ConfigBarComponent,
    DumpCreationModalComponent,
    LoadDataComponent,
    ExportDataComponent,
    FilterGraphsModalComponent,
    RefactorComponent,
    VersioningComponent,
  ],
  exports: [
    ConfigBarComponent,
    LoadDataComponent,
    ExportDataComponent,
    RefactorComponent,
    VersioningComponent,
  ],
  providers: []
})
export class GlobalDataMgmtModule { }