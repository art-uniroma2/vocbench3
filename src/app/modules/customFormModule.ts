import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { CustomFormComponent } from '../custom-forms/custom-form/custom-form.component';
import { CustomFormFieldComponent } from '../custom-forms/custom-form/custom-form-field.component';
import { CustomFormFieldListComponent } from '../custom-forms/custom-form/custom-form-field-list.component';
import { CustomFormModalComponent } from '../custom-forms/custom-form/custom-form-modal.component';
import { CustomFormConfigComponent } from '../custom-forms/custom-form-conf.component';
import { BrokenCFStructReportModalComponent } from '../custom-forms/editors/broken-cf-struct-report-modal.component';
import { CustomFormEditorModalComponent } from '../custom-forms/editors/custom-form-editor-modal.component';
import { CustomFormPickerModalComponent } from '../custom-forms/editors/custom-form-picker-modal.component';
import { AdvancedGraphEditorComponent } from '../custom-forms/editors/customFormWizard/advanced-graph-editor.component';
import { ConstraintValuesSelectorComponent } from '../custom-forms/editors/customFormWizard/constraint-values-selector.component';
import { ConverterConfigModalComponent } from '../custom-forms/editors/customFormWizard/converter-config-modal.component';
import { CustomFormWizardFieldsEditorComponent } from '../custom-forms/editors/customFormWizard/custom-form-wizard-fields-editor.component';
import { CustomFormWizardGraphEditorComponent } from '../custom-forms/editors/customFormWizard/custom-form-wizard-graph-editor.component';
import { CustomFormWizardModalComponent } from '../custom-forms/editors/customFormWizard/custom-form-wizard-modal.component';
import { CustomFormWizardNodesEditorComponent } from '../custom-forms/editors/customFormWizard/custom-form-wizard-nodes-editor.component';
import { RoleSelectorComponent } from '../custom-forms/editors/customFormWizard/role-selector.component';
import { ExtractFromShaclModalComponent } from '../custom-forms/editors/extract-from-shacl-modal.component';
import { FormCollEditorModalComponent } from '../custom-forms/editors/form-coll-editor-modal.component';
import { FormCollMappingModalComponent } from '../custom-forms/editors/form-coll-mapping-modal.component';
import { ImportCfModalComponent } from '../custom-forms/editors/import-cf-modal.component';
import { PearlInferenceValidationModalComponent } from '../custom-forms/editors/pearl-inference-validation-modal.component';
import { BasicModalServices } from "../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../modal-dialogs/browsing-modals/browsing-modals.service";
import { SharedModule } from './sharedModule';
import { Sheet2RdfModule } from './sheet2rdfModule';

@NgModule({
  imports: [
    CommonModule,
    DragDropModule,
    FormsModule,
    NgbDropdownModule,
    SharedModule,
    Sheet2RdfModule,
    TranslateModule
  ],
  providers: [BasicModalServices, BrowsingModalServices],
  declarations: [
    AdvancedGraphEditorComponent,
    BrokenCFStructReportModalComponent,
    ConstraintValuesSelectorComponent,
    CustomFormConfigComponent,
    CustomFormComponent,
    CustomFormEditorModalComponent,
    CustomFormFieldComponent,
    CustomFormFieldListComponent,
    CustomFormModalComponent,
    CustomFormPickerModalComponent,
    CustomFormWizardFieldsEditorComponent,
    CustomFormWizardGraphEditorComponent,
    CustomFormWizardNodesEditorComponent,
    CustomFormWizardModalComponent,
    ExtractFromShaclModalComponent,
    FormCollEditorModalComponent,
    FormCollMappingModalComponent,
    ConverterConfigModalComponent,
    ImportCfModalComponent,
    PearlInferenceValidationModalComponent,
    RoleSelectorComponent,
  ],
  exports: [
    CustomFormConfigComponent,
    CustomFormComponent,
    CustomFormPickerModalComponent,
  ]
})
export class CustomFormModule { }