import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from "../../models/User";
import { AuthServices } from "../../services/auth.service";
import { UserServices } from "../../services/user.service";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";

@Component({
    selector: "force-pwd-modal",
    templateUrl: "./force-password-modal.component.html",
    standalone: false
})
export class ForcePasswordModalComponent {
    @Input() user: User;

    translationParam: { userShow: string };

    password: string;

    constructor(public activeModal: NgbActiveModal, private userService: UserServices, private authService: AuthServices,
        private basicModals: BasicModalServices) {
    }

    ngOnInit() {
        this.translationParam = { userShow: this.user.getShow() };
    }

    isInputValid(): boolean {
        return this.password != undefined && this.password.trim() != "";
    }

    ok() {
        this.userService.forcePassword(this.user.getEmail(), this.password).subscribe(
            () => {
                this.basicModals.alert({ key: "STATUS.OPERATION_DONE" }, { key: "MESSAGES.PASSWORD_CHANGED" }).then(
                    () => {
                        this.activeModal.close();
                    }
                );
            }
        );
    }

    cancel() {
        this.activeModal.dismiss();
    }

}