import { Component, ViewChild } from "@angular/core";
import { SettingsServices } from "src/app/services/settings.service";
import { ConfigurationComponents } from "../../models/Configuration";
import { ExtensionPointID, Scope, SettingsProp } from "../../models/Plugins";
import { ResViewTemplate, SectionFilterPreference, SettingsEnum } from "../../models/Properties";
import { User } from "../../models/User";
import { UserServices } from "../../services/user.service";
import { VBContext } from "../../utils/VBContext";
import { VBProperties } from "../../utils/VBProperties";
import { LoadConfigurationModalReturnData } from "../../modal-dialogs/shared-modals/configuration-store-modal/load-configuration-modal.component";
import { SharedModalServices } from "../../modal-dialogs/shared-modals/shared-modals.service";
import { UsersListComponent } from './users-list.component';

@Component({
  selector: "users-admin-component",
  templateUrl: "./users-administration.component.html",
  host: { class: "pageComponent" },
  standalone: false
})
export class UsersAdministrationComponent {

  @ViewChild(UsersListComponent) userListChild: UsersListComponent;

  selectedUser: User;

  userDetailsAspect: string = "Details";
  rvTemplateAspect: string = "Template";
  aspectSelectors: string[] = [this.userDetailsAspect, this.rvTemplateAspect];
  selectedAspectSelector: string = this.aspectSelectors[0];

  userProjects: string[]; //project assigned to the user

  sectionFilter: SectionFilterPreference;
  template: ResViewTemplate;

  constructor(private userService: UserServices, private settingsService: SettingsServices,
    private vbProp: VBProperties, private sharedModals: SharedModalServices) { }

  ngOnInit() {
    /*
    init the template on which base the section filter. 
    Use the system default template since in this editor the section filter is not project specific.
    */
    this.settingsService.getSettingsDefault(ExtensionPointID.ST_CORE_ID, Scope.PROJECT, Scope.SYSTEM).subscribe(
      settings => {
        this.template = settings.getPropertyValue(SettingsEnum.resourceView).templates;
      }
    );
  }


  /** ==========================
   * Users management
   * =========================== */


  onUserSelected(user: User) {
    this.selectedUser = user;
    if (this.selectedUser != null) {
      this.initTemplate();
      //init project assigned to user user
      if (!this.selectedUser.isAdmin()) {
        this.userService.listProjectsBoundToUser(this.selectedUser.getIri()).subscribe(
          projects => {
            this.userProjects = projects;
            this.userProjects.sort();
          }
        );
      }
    }
  }

  onUserDeleted() {
    this.userListChild.initUserList();
  }

  /** ============================
   * Templates management
   * ============================ */

  private initTemplate() {
    this.settingsService.getPUSettingsUserDefault(ExtensionPointID.ST_CORE_ID, this.selectedUser).subscribe(
      settings => {
        this.sectionFilter = settings.getPropertyValue(SettingsEnum.resViewPartitionFilter, {});
      }
    );
  }

  loadTemplate() {
    this.sharedModals.loadConfiguration({ key: "COMMONS.ACTIONS.LOAD_TEMPLATE" }, ConfigurationComponents.TEMPLATE_STORE).then(
      (conf: LoadConfigurationModalReturnData) => {
        let templateProp: SettingsProp = conf.configuration.properties.find(p => p.name == "template");
        if (templateProp != null) {
          this.sectionFilter = templateProp.value;
          this.updateTemplate();
        }
      }
    );
  }

  storeTemplate() {
    let config: { [key: string]: any } = {
      template: this.sectionFilter
    };
    this.sharedModals.storeConfiguration({ key: "COMMONS.ACTIONS.SAVE_TEMPLATE" }, ConfigurationComponents.TEMPLATE_STORE, config);
  }

  updateTemplate() {
    this.settingsService.storePUSettingUserDefault(ExtensionPointID.ST_CORE_ID, this.selectedUser, SettingsEnum.resViewPartitionFilter, this.sectionFilter).subscribe(
      () => {
        //in case the setting has been changed for the logged user and a project is currently accessed => update its cached PU-settings
        if (VBContext.getWorkingProject() != null && VBContext.getLoggedUser().getEmail() == this.selectedUser.getEmail()) {
          this.vbProp.refreshResourceViewSectionFilter().subscribe();
        }
      }
    );
  }

}