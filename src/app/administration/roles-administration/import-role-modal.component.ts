import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: "import-role-modal",
    templateUrl: "./import-role-modal.component.html",
    standalone: false
})
export class ImportRoleModalComponent {
    
    roleName: string;
    private file: File;

    constructor(public activeModal: NgbActiveModal) {}

    fileChangeEvent(file: File) {
        this.file = file;
    }
    
    ok() {
        this.activeModal.close({file: this.file, name: this.roleName});
    }

    cancel() {
        this.activeModal.dismiss();
    }
    
    isInputValid(): boolean {
        return (this.roleName != null && this.roleName.trim() != "");
    }

}