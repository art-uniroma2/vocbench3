import { Component } from "@angular/core";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from "@ngx-translate/core";
import { Translation } from "src/app/utils/TranslationUtils";
import { ModalOptions } from 'src/app/modal-dialogs/Modals';
import { UsersGroup } from "../../models/User";
import { UsersGroupsServices } from "../../services/users-groups.service";
import { GroupEditorModalComponent } from "./group-editor-modal.component";

@Component({
  selector: "groups-admin-component",
  templateUrl: "./groups-administration.component.html",
  host: { class: "pageComponent" },
  standalone: false
})
export class GroupsAdministrationComponent {

  //Group list panel
  groupList: UsersGroup[];
  selectedGroup: UsersGroup;

  constructor(private groupsSevice: UsersGroupsServices, private modalService: NgbModal, private translateService: TranslateService) { }

  ngOnInit() {
    this.initGroups();
  }

  private initGroups(groupToSelect?: UsersGroup) {
    this.groupsSevice.listGroups().subscribe(
      groups => {
        this.groupList = groups;
        this.selectedGroup = null;

        if (groupToSelect != null) {
          this.groupList.forEach((g: UsersGroup) => {
            if (g.iri.equals(groupToSelect.iri)) {
              this.selectGroup(g);
            }
          });
        }
      }
    );
  }

  selectGroup(group: UsersGroup) {
    this.selectedGroup = group;
  }

  createGroup() {
    this.openGroupEditor({ key: "ADMINISTRATION.ACTIONS.CREATE_GROUP" }).then(
      () => {
        this.initGroups();
      },
      () => { }
    );
  }

  deleteGroup() {
    this.groupsSevice.deleteGroup(this.selectedGroup.iri).subscribe(
      () => {
        this.initGroups();
      }
    );
  }

  editGroup() {
    this.openGroupEditor({ key: "ADMINISTRATION.ACTIONS.EDIT_GROUP" }, this.selectedGroup).then(
      (updatedGroup: UsersGroup) => {
        this.initGroups(updatedGroup);
      },
      () => { }
    );
  }

  private openGroupEditor(title: Translation, group?: UsersGroup) {
    const modalRef: NgbModalRef = this.modalService.open(GroupEditorModalComponent, new ModalOptions());
    modalRef.componentInstance.title = this.translateService.instant(title.key, title.params);
    modalRef.componentInstance.group = group;
    return modalRef.result;
  }


}