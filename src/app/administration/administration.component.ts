import { Component } from "@angular/core";
import { Router } from '@angular/router';
import { AuthorizationEvaluator } from "../utils/AuthorizationEvaluator";
import { VBActionsEnum } from "../utils/VBActions";
import { VBContext } from "../utils/VBContext";

@Component({
    selector: "admin-component",
    templateUrl: "./administration.component.html",
    host: { class: "pageComponent" },
    standalone: false
})
export class AdministrationComponent {

    isAdmin: boolean;

    constructor(private router: Router) { }

    ngOnInit() {
        this.isAdmin = VBContext.getLoggedUser() && VBContext.getLoggedUser().isAdmin();
    }

    isRoleManagementAuthorized(): boolean {
        return AuthorizationEvaluator.isAuthorized(VBActionsEnum.administrationRoleManagement);
    }

    isProjManagementAuthorized(): boolean {
        return (
            AuthorizationEvaluator.isAuthorized(VBActionsEnum.administrationProjectManagement) ||
            AuthorizationEvaluator.isAuthorized(VBActionsEnum.administrationUserRoleManagement) ||
            AuthorizationEvaluator.isAuthorized(VBActionsEnum.administrationUserGroupManagement)
        );
    }

    isSettingsRouteActive(): boolean {
        /**
         * The button group item "Settings" is a dropdown, so I cannot make it active with the usual routerLink+routerLinkActive because:
         * - The active item is the actual dropown item, not the button
         * - If I apply routerLink+routerLinkActive also to the button, when clicked it will redirect to the route
         */
        let url: string = this.router.url.split("?")[0]; //exclude URL args (if any)
        let splittedUrl = url.split("/");
        let active = splittedUrl[splittedUrl.length - 1];
        return active == "UserDefaults" || active == "SettingsMgr";
    }

}