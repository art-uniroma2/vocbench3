import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Project } from "../../models/Project";
import { Role, User } from "../../models/User";
import { AdministrationServices } from "../../services/administration.service";
import { UserServices } from "../../services/user.service";

@Component({
  selector: "up-binding-modal",
  templateUrl: "./user-proj-binding-modal.component.html",
  standalone: false
})
export class UserProjBindingModalComponent {
  @Input() project: Project;
  @Input() usersBound: User[];

  userList: User[] = [];
  selectedUser: User;

  userFilter: string = "";

  roleList: Role[] = [];
  selectedRoles: Role[] = [];

  constructor(public activeModal: NgbActiveModal, public userService: UserServices,
    public adminService: AdministrationServices) {
  }

  ngOnInit() {
    this.userService.listUsers().subscribe(
      users => {
        this.userList = users;
      }
    );
    this.adminService.listRoles(this.project).subscribe(
      roles => {
        this.roleList = roles;
      }
    );
  }

  selectUser(user: User) {
    if (this.isUserAlreadyBound(user)) {
      return;
    }
    if (this.selectedUser == user) {
      this.selectedUser = null;
    } else {
      this.selectedUser = user;
    }
  }

  isUserVisible(user: User): boolean {
    let givenNameCheck: boolean = user.getGivenName().toLocaleLowerCase().includes(this.userFilter.toLocaleLowerCase());
    let familyNameCheck: boolean = user.getFamilyName().toLocaleLowerCase().includes(this.userFilter.toLocaleLowerCase());
    let emailCheck: boolean = user.getEmail().toLocaleLowerCase().includes(this.userFilter.toLocaleLowerCase());
    return givenNameCheck || familyNameCheck || emailCheck;
  }

  isUserAlreadyBound(user: User): boolean {
    for (const u of this.usersBound) {
      if (user.getEmail() == u.getEmail()) {
        return true;
      }
    }
    return false;
  }

  selectRole(role: Role) {
    let idx = this.selectedRoles.indexOf(role);
    if (idx != -1) {
      this.selectedRoles.splice(idx, 1);
    } else {
      this.selectedRoles.push(role);
    }
  }

  isRoleSelected(role: Role): boolean {
    return this.selectedRoles.indexOf(role) != -1;
  }

  ok() {
    let roleList: string[] = [];
    for (const r of this.selectedRoles) {
      roleList.push(r.getName());
    }
    this.activeModal.close({ user: this.selectedUser, roles: roleList });
  }

  cancel() {
    this.activeModal.dismiss();
  }

}