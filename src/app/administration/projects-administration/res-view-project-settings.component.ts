import { ChangeDetectorRef, Component, Input, SimpleChanges, ViewChild } from "@angular/core";
import { ARTURIResource } from "src/app/models/ARTResources";
import { ExtensionPointID, Scope } from "src/app/models/Plugins";
import { ResViewSection } from "src/app/models/ResourceView";
import { ResViewTemplateEditorComponent } from "src/app/preferences/res-view-template-editor/res-view-template-editor.component";
import { SettingsServices } from "src/app/services/settings.service";
import { NTriplesUtil, ResourceUtils } from "src/app/utils/ResourceUtils";
import { BasicModalServices } from "src/app/modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "src/app/modal-dialogs/browsing-modals/browsing-modals.service";
import { ModalType } from "src/app/modal-dialogs/Modals";
import { Project } from "../../models/Project";
import { CustomSection, ResourceViewProjectSettings, ResViewTemplate, SettingsEnum } from "../../models/Properties";
import { VBContext } from "../../utils/VBContext";

@Component({
    selector: "res-view-project-settings",
    templateUrl: "./res-view-project-settings.component.html",
    standalone: false
})
export class ResViewProjectSettingsComponent {

    @Input() project: Project;
    @Input() settings: ResourceViewProjectSettings;

    @ViewChild(ResViewTemplateEditorComponent) rvTemplateEditor: ResViewTemplateEditorComponent;

    isAdmin: boolean; //useful to decide which store/restore defualt action allow
    isActiveProject: boolean; //useful to decide if allow properties addition through properties tree or manually

    private rvSettingTimer: number; //in order to do not fire too much close requests to update rv settings

    //custom sections
    customSections: { [key: string]: ARTURIResource[] }; //ResViewSection -> properties
    customSectionsIDs: string[];

    selectedCustomSection: string;
    selectedManagedProperty: ARTURIResource;

    constructor(
        private settingsService: SettingsServices,
        private basicModals: BasicModalServices,
        private browsingModals: BrowsingModalServices,
        private cdRef: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this.isAdmin = VBContext.getLoggedUser().isAdmin();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['rvSettings'] && changes['rvSettings'].currentValue) {
            this.initResViewSettings();
        }
        if (changes['project'] && changes['project'].currentValue) {
            let workingProj = VBContext.getWorkingProject();
            this.isActiveProject = workingProj != null && workingProj.getName() == this.project.getName();
        }
    }

    private initResViewSettings() {
        this.customSections = {};
        this.customSectionsIDs = [];
        this.selectedCustomSection = null;
        this.selectedManagedProperty = null;

        if (this.settings.customSections != null) {
            for (let sectionId in this.settings.customSections) {
                let cs: CustomSection = this.settings.customSections[sectionId];
                let matchedProperties: ARTURIResource[] = cs.matchedProperties.map(p => new ARTURIResource(p));
                matchedProperties.sort((p1, p2) => p1.getURI().localeCompare(p2.getURI()));
                this.customSections[sectionId] = matchedProperties;
            }
        }
        this.customSectionsIDs = Object.keys(this.customSections);
    }

    /* ================
    * CUSTOM SECTIONS
    * ================= */

    selectCustomSection(cs: string) {
        this.selectedCustomSection = cs;
        this.selectedManagedProperty = null;
    }

    renameCustomSection() {
        this.basicModals.prompt({ key: "ADMINISTRATION.PROJECTS.RES_VIEW.RENAME_CUSTOM_SECTION" }, null, null, this.selectedCustomSection).then(
            sectionName => {
                let newSectionName: ResViewSection = sectionName as ResViewSection;
                if (newSectionName == this.selectedCustomSection) { //not changed
                    return;
                }
                if (Object.keys(this.settings.templates).includes(newSectionName)) { //changed but section with the same name already exists
                    this.basicModals.alert({ key: "STATUS.INVALID_VALUE" }, { key: "MESSAGES.ALREADY_EXISTING_SECTION" }, ModalType.warning);
                    return;
                }
                //move the managed properties to the new section, then remove the old one
                let props: ARTURIResource[] = this.customSections[this.selectedCustomSection];
                this.customSections[newSectionName] = props;
                delete this.customSections[this.selectedCustomSection];
                this.customSectionsIDs = Object.keys(this.customSections);

                //rename the custom section also in the template
                this.rvTemplateEditor.onCustomSectionRenamed(this.selectedCustomSection, newSectionName);

                this.selectedCustomSection = newSectionName;

                //use timeout because the update is automatically fired also by rvTemplateEditor.onCustomSectionRenamed
                this.updateResViewSettingsWithTimeout();
            },
            () => { }
        );
    }

    addCustomSection() {
        this.basicModals.prompt({ key: "ADMINISTRATION.PROJECTS.RES_VIEW.CREATE_CUSTOM_SECTION" }).then(
            customSectionName => {
                if (Object.keys(this.settings.templates).includes(customSectionName)) { //section with the same name already exists
                    this.basicModals.alert({ key: "STATUS.INVALID_VALUE" }, { key: "MESSAGES.ALREADY_EXISTING_SECTION" }, ModalType.warning);
                    return;
                }
                this.customSections[customSectionName] = [];
                this.customSectionsIDs = Object.keys(this.customSections);

                //update templates
                this.rvTemplateEditor.onCustomSectionCreated(customSectionName);

                //use timeout because the update is automatically fired also by rvTemplateEditor.onCustomSectionCreated
                this.updateResViewSettingsWithTimeout();
            },
            () => { }
        );
    }

    deleteCustomSection() {
        delete this.customSections[this.selectedCustomSection];
        this.customSectionsIDs = Object.keys(this.customSections); //update the IDs list

        //remove the custom section from the templates
        this.rvTemplateEditor.onCustomSectionDeleted(this.selectedCustomSection);

        this.selectedCustomSection = null;

        //use timeout because the update is automatically fired also by rvTemplateEditor.onCustomSectionDeleted
        this.updateResViewSettingsWithTimeout();
    }

    selectManagedProperty(prop: ARTURIResource) {
        this.selectedManagedProperty = prop;
    }

    addManagedProperty() {
        if (this.isActiveProject) {
            this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }).then(
                (prop: ARTURIResource) => {
                    if (!this.customSections[this.selectedCustomSection].some(p => p.equals(prop))) { //if not already in
                        this.customSections[this.selectedCustomSection].push(prop);
                        this.updateResViewSettings();
                    }
                },
                () => { }
            );
        } else {
            this.basicModals.prompt({ key: "DATA.ACTIONS.ADD_PROPERTY" }, { value: "IRI" }).then(
                valueIRI => {
                    let prop: ARTURIResource;
                    if (ResourceUtils.testIRI(valueIRI)) { //valid iri (e.g. "http://test")
                        prop = new ARTURIResource(valueIRI);
                    } else { //not an IRI, try to parse as NTriples
                        try {
                            prop = NTriplesUtil.parseURI(valueIRI);
                        } catch { //neither a valid ntriple iri (e.g. "<http://test>")
                            this.basicModals.alert({ key: "STATUS.INVALID_VALUE" }, { key: "MESSAGES.INVALID_IRI", params: { iri: valueIRI } }, ModalType.warning);
                            return;
                        }
                    }
                    if (!this.customSections[this.selectedCustomSection].some(p => p.equals(prop))) { //if not already in
                        this.customSections[this.selectedCustomSection].push(prop);
                        this.updateResViewSettings();
                    }
                },
                () => { }
            );
        }
    }

    deleteManagedProperty() {
        this.customSections[this.selectedCustomSection].splice(this.customSections[this.selectedCustomSection].indexOf(this.selectedManagedProperty), 1);
        this.selectedManagedProperty = null;
        this.updateResViewSettings();
    }

    //================

    /**
     * Store the settings only after a timeout of 1000 in order to prevent too much service invocation
     * when user is editing the templates
     */
    updateResViewSettingsWithTimeout() {
        clearTimeout(this.rvSettingTimer);
        this.rvSettingTimer = window.setTimeout(() => { this.updateResViewSettings(); }, 1000);
    }


    private updateResViewSettings() {
        let rvSettings: ResourceViewProjectSettings = this.getResViewProjectSettings();
        this.settingsService.storeSettingForProjectAdministration(ExtensionPointID.ST_CORE_ID, Scope.PROJECT, SettingsEnum.resourceView, rvSettings, this.project).subscribe(
            () => {
                //in case the edited project is the active one, update the settings stored in VBContext
                if (VBContext.getWorkingProject() != null && VBContext.getWorkingProject().getName() == this.project.getName()) {
                    VBContext.getWorkingProjectCtx().getProjectSettings().resourceView = rvSettings;
                }
            }
        );
    }

    setAsSystemDefault() {
        this.basicModals.confirm({ key: "COMMONS.ACTIONS.SET_AS_SYSTEM_DEFAULT" }, { key: "MESSAGES.CONFIG_SET_SYSTEM_DEFAULT_CONFIRM" }, ModalType.warning).then(
            () => {
                let rvSettings: ResourceViewProjectSettings = this.getResViewProjectSettings();
                this.settingsService.storeSettingDefault(ExtensionPointID.ST_CORE_ID, Scope.PROJECT, Scope.SYSTEM, SettingsEnum.resourceView, rvSettings).subscribe(
                    () => {
                        this.basicModals.alert({ key: "STATUS.OPERATION_DONE" }, { key: "MESSAGES.CONFIG_SYSTEM_DEFAULT_SET" });
                    }
                );
            },
            () => { }
        );
    }

    restoreSystemDefault() {
        this.basicModals.confirm({ key: "COMMONS.ACTIONS.RESTORE_SYSTEM_DEFAULT" }, { key: "MESSAGES.CONFIG_RESTORE_SYSTEM_DEFAULT_CONFIRM" }, ModalType.warning).then(
            () => {
                let rvSettings: ResourceViewProjectSettings = null;
                this.settingsService.storeSettingForProjectAdministration(ExtensionPointID.ST_CORE_ID, Scope.PROJECT, SettingsEnum.resourceView, rvSettings, this.project).subscribe(
                    () => {
                        //update the settings getting them from server in order to exploit the fallback-to-default mechanism
                        this.settingsService.getSettingsForProjectAdministration(ExtensionPointID.ST_CORE_ID, Scope.PROJECT, this.project).subscribe(
                            settings => {
                                this.settings = null;
                                this.cdRef.detectChanges(); //in order to reset template editor
                                this.settings = settings.getPropertyValue(SettingsEnum.resourceView);
                                this.initResViewSettings();
                                this.basicModals.alert({ key: "STATUS.OPERATION_DONE" }, { key: "MESSAGES.CONFIG_SYSTEM_DEFAULT_RESTORED" });
                                //in case the edited project is the active one, update the settings stored in VBContext
                                if (VBContext.getWorkingProject() != null && VBContext.getWorkingProject().getName() == this.project.getName()) {
                                    VBContext.getWorkingProjectCtx().getProjectSettings().resourceView = this.settings;
                                }
                            }
                        );
                    }
                );
            },
            () => { }
        );
    }

    private getResViewProjectSettings(): ResourceViewProjectSettings {
        let customSections: { [key: string]: CustomSection } = {}; //map name -> CustomSection
        for (let csId in this.customSections) {
            customSections[csId] = {
                matchedProperties: this.customSections[csId].map(p => p.toNT())
            };
        }

        let templates: ResViewTemplate = this.settings.templates;

        let rvSettings: ResourceViewProjectSettings = {
            customSections: customSections,
            templates: templates
        };
        return rvSettings;
    }


}