import { Component, Input, SimpleChanges } from "@angular/core";
import { forkJoin, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ExtensionPointID, Scope } from "src/app/models/Plugins";
import { SettingsServices } from "src/app/services/settings.service";
import { ARTURIResource } from "../../models/ARTResources";
import { Project } from "../../models/Project";
import { ConceptTreePreference, SettingsEnum } from "../../models/Properties";
import { UsersGroup } from "../../models/User";
import { OntoLex, SKOS } from "../../models/Vocabulary";
import { PropertyServices } from "../../services/properties.service";
import { ResourcesServices } from "../../services/resources.service";
import { UsersGroupsServices } from "../../services/users-groups.service";
import { ResourceUtils } from "../../utils/ResourceUtils";
import { ProjectContext, VBContext } from "../../utils/VBContext";
import { VBProperties } from "../../utils/VBProperties";
import { BrowsingModalServices } from "../../modal-dialogs/browsing-modals/browsing-modals.service";

@Component({
  selector: "project-groups-manager",
  templateUrl: "./projectGroupsManagerComponent.html",
  standalone: false
})
export class ProjectGroupsManagerComponent {

  @Input() project: Project;
  private lastBrowsedProjectCtx: ProjectContext;
  projectNotAccessed: boolean;

  groups: UsersGroup[]; //list of groups
  selectedGroup: UsersGroup; //group selected in the list of groups

  private concTreePref: ConceptTreePreference;
  baseBroaderProp: string;
  broaderProps: ARTURIResource[] = [];
  narrowerProps: ARTURIResource[] = [];
  includeSubProps: boolean;
  syncInverse: boolean;

  selectedBroader: ARTURIResource;
  selectedNarrower: ARTURIResource;

  ownedSchemes: ARTURIResource[] = [];
  selectedScheme: ARTURIResource;

  translationParam: { projName: string } = { projName: "" };

  constructor(private groupsService: UsersGroupsServices, private settingsService: SettingsServices,
    private resourceService: ResourcesServices, private propService: PropertyServices,
    private browsingModals: BrowsingModalServices, private vbProp: VBProperties) { }

  ngOnInit() {
    this.groupsService.listGroups().subscribe(
      groups => {
        this.groups = groups;
      }
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['project'] && changes['project'].currentValue) {
      this.translationParam = { projName: this.project.getName(true) };
      if (this.selectedGroup) {
        this.initSettings();
      }
    }
  }

  isProjectSkosCompliant() {
    if (this.project != null) {
      let modelType = this.project.getModelType();
      return modelType == SKOS.uri || modelType == OntoLex.uri;
    } else {
      return false;
    }
  }

  private initSettings() {
    this.projectNotAccessed = VBContext.getWorkingProject()?.getName() != this.project.getName();
    if (this.projectNotAccessed) {
      return;
    }

    // this.prepareProjectAccess(); //so that all the getResourcesInfo functions will have the current selected project as ctx_project

    /**
     * array of getResourcesInfo functions collected in the handlers of getSettingsForProjectAdministration (for broader and narrower props)
     * and getProjectGroupBinding (for ownedSchemes) responses.
     */
    let describeFunctions: Observable<any>[] = [];

    let getPGSettingsFn = this.settingsService.getSettingsForProjectAdministration(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_GROUP, this.project, null, this.selectedGroup).pipe(
      map(settings => {
        this.broaderProps = [];
        this.narrowerProps = [];

        this.concTreePref = settings.getPropertyValue(SettingsEnum.conceptTree);

        if (this.concTreePref != null) {
          this.baseBroaderProp = this.concTreePref.baseBroaderProp;
          if (this.baseBroaderProp == null) {
            this.baseBroaderProp = SKOS.broader.getURI();
          }

          let broaderPropsPref: string[] = this.concTreePref.broaderProps;
          if (broaderPropsPref != null) {
            let broaderPropsTemp: ARTURIResource[] = broaderPropsPref.map(b => new ARTURIResource(b));
            if (broaderPropsTemp.length > 0) {
              let describeBroadersFn = this.resourceService.getResourcesInfo(broaderPropsTemp).pipe(
                map(resources => {
                  this.broaderProps = resources as ARTURIResource[];
                })
              );
              describeFunctions.push(describeBroadersFn);
            }
          }

          let narrowerPropsPref: string[] = this.concTreePref.narrowerProps;
          if (narrowerPropsPref != null) {
            let narrowerPropsTemp: ARTURIResource[] = narrowerPropsPref.map(n => new ARTURIResource(n));
            if (narrowerPropsTemp.length > 0) {
              let describeNarrowersFn = this.resourceService.getResourcesInfo(narrowerPropsTemp).pipe(
                map(resources => {
                  this.narrowerProps = resources as ARTURIResource[];
                })
              );
              describeFunctions.push(describeNarrowersFn);
            }
          }

          this.includeSubProps = this.concTreePref.includeSubProps != false; //so that default is true
          this.syncInverse = this.concTreePref.syncInverse != false; //so that default is true
        } else {
          this.concTreePref = new ConceptTreePreference();
        }

      })
    );

    let getPGBindingFn = this.groupsService.getProjectGroupBinding(this.project, this.selectedGroup.iri).pipe(
      map(binding => {
        let ownedSchemesTemp: ARTURIResource[] = [];
        if (binding.ownedSchemes != null) {
          binding.ownedSchemes.forEach((schemeUri: string) => {
            ownedSchemesTemp.push(new ARTURIResource(schemeUri));
          });
        }
        this.ownedSchemes = [];
        if (ownedSchemesTemp.length > 0) {
          let describeOwnedSchemesFn = this.resourceService.getResourcesInfo(ownedSchemesTemp).pipe(
            map(resources => {
              this.ownedSchemes = resources as ARTURIResource[];
            })
          );
          describeFunctions.push(describeOwnedSchemesFn);
        }
      })
    );

    forkJoin([getPGSettingsFn, getPGBindingFn]).subscribe(
      () => {
        if (describeFunctions.length > 0) {
          forkJoin(describeFunctions);
        }
      }
    );

  }

  selectGroup(group: UsersGroup) {
    this.selectedGroup = group;
    if (this.project != null) {
      this.initSettings();
    }
  }


  /**
   * BASE BROADER HANDLERS
   */

  changeBaseBroaderProperty() {
    this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }, [SKOS.broader], null, null, this.lastBrowsedProjectCtx).then(
      (prop: ARTURIResource) => {
        this.baseBroaderProp = prop.getURI();
        this.updateGroupSetting();
      },
      () => { }
    );
  }

  /**
   * BROADER/NARROWER PROPRETIES
   */

  addBroader() {
    this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }, [SKOS.broader], null, null, this.lastBrowsedProjectCtx).then(
      (prop: ARTURIResource) => {
        if (!ResourceUtils.containsNode(this.broaderProps, prop)) {
          this.broaderProps.push(prop);
          this.updateGroupSetting();
          //if synchronization is active sync the lists
          if (this.syncInverse) {
            this.syncInverseOfBroader().subscribe(
              () => {
                this.updateGroupSetting();
              }
            );
          }
        }
      },
      () => { }
    );
  }

  addNarrower() {
    this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }, [SKOS.narrower], null, null, this.lastBrowsedProjectCtx).then(
      (prop: ARTURIResource) => {
        if (!ResourceUtils.containsNode(this.narrowerProps, prop)) {
          this.narrowerProps.push(prop);
          this.updateGroupSetting();
          //if synchronization is active sync the lists
          if (this.syncInverse) {
            this.syncInverseOfNarrower().subscribe(
              () => {
                this.updateGroupSetting();
              }
            );
          }
        }
      },
      () => { }
    );
  }

  removeBroader() {
    this.broaderProps.splice(this.broaderProps.indexOf(this.selectedBroader), 1);
    //if synchronization is active sync the lists
    if (this.syncInverse) {
      this.propService.getInverseProperties([this.selectedBroader]).subscribe(
        (inverseProps: ARTURIResource[]) => {
          let inverseUpdated: boolean = false;
          inverseProps.forEach((narrowProp: ARTURIResource) => {
            let idx = ResourceUtils.indexOfNode(this.narrowerProps, narrowProp);
            if (idx != -1) {
              this.narrowerProps.splice(idx, 1);
              inverseUpdated = true;
            }
          });
          if (inverseUpdated) {
            this.updateGroupSetting();
          }
        }
      );
    } else {
      this.updateGroupSetting();
    }
    this.selectedBroader = null;
  }

  removeNarrower() {
    this.narrowerProps.splice(this.narrowerProps.indexOf(this.selectedNarrower), 1);
    this.updateGroupSetting();
    //if synchronization is active sync the lists
    if (this.syncInverse) {
      // this.prepareProjectAccess();
      this.propService.getInverseProperties([this.selectedNarrower]).subscribe(
        (inverseProps: ARTURIResource[]) => {
          let inverseUpdated: boolean = false;
          inverseProps.forEach((broaderProp: ARTURIResource) => {
            let idx = ResourceUtils.indexOfNode(this.broaderProps, broaderProp);
            if (idx != -1) {
              this.broaderProps.splice(idx, 1);
              inverseUpdated = true;
            }
          });
          if (inverseUpdated) {
            this.updateGroupSetting();
          }
        }
      );
    } else {
      this.updateGroupSetting();
    }
    this.selectedNarrower = null;
  }

  onSyncChange() {
    //if sync inverse properties change from false to true perform a sync
    if (this.syncInverse) {
      this.syncInverseOfBroader().subscribe(
        () => {
          this.syncInverseOfNarrower().subscribe(
            () => {
              this.updateGroupSetting();
            }
          );
        }
      );
    }
  }

  private syncInverseOfBroader(): Observable<any> {
    if (this.broaderProps.length > 0) {
      return this.propService.getInverseProperties(this.broaderProps).pipe(
        map((inverseProps: ARTURIResource[]) => {
          inverseProps.forEach((narrowerProp: ARTURIResource) => {
            if (!ResourceUtils.containsNode(this.narrowerProps, narrowerProp)) { //invers is not already among the narrowerProps
              let broaderPropUri: string = narrowerProp.getAdditionalProperty("inverseOf");
              //add it at the same index of its inverse prop
              let idx: number = ResourceUtils.indexOfNode(this.broaderProps, new ARTURIResource(broaderPropUri));
              this.narrowerProps.splice(idx, 0, narrowerProp);
            }
          });
        })
      );
    } else {
      return of(null);
    }
  }

  private syncInverseOfNarrower(): Observable<any> {
    if (this.narrowerProps.length > 0) {
      return this.propService.getInverseProperties(this.narrowerProps).pipe(
        map((inverseProps: ARTURIResource[]) => {
          inverseProps.forEach((broaderProp: ARTURIResource) => {
            if (!ResourceUtils.containsNode(this.broaderProps, broaderProp)) { //invers is not already among the broaderProps
              let narrowerPropUri: string = broaderProp.getAdditionalProperty("inverseOf");
              //add it at the same index of its inverse prop
              let idx: number = ResourceUtils.indexOfNode(this.narrowerProps, new ARTURIResource(narrowerPropUri));
              this.broaderProps.splice(idx, 0, broaderProp);
            }
          });
        })
      );
    } else {
      return of(null);
    }
  }

  onIncludeSubPropsChange() {
    this.updateGroupSetting();
  }


  /**
   * OWNED SCHEMES PROPRETIES
   */

  addScheme() {
    this.browsingModals.browseSchemeList({ key: "DATA.ACTIONS.SELECT_SCHEME" }, this.lastBrowsedProjectCtx).then(
      (scheme: ARTURIResource) => {
        // this.revokeProjectAccess();
        if (!ResourceUtils.containsNode(this.ownedSchemes, scheme)) {
          this.ownedSchemes.push(scheme);
          this.groupsService.addOwnedSchemeToGroup(this.project, this.selectedGroup.iri, scheme).subscribe();
        }
      },
      () => { }
    );
  }

  removeScheme() {
    this.ownedSchemes.splice(this.ownedSchemes.indexOf(this.selectedScheme), 1);
    this.groupsService.removeOwnedSchemeFromGroup(this.project, this.selectedGroup.iri, this.selectedScheme).subscribe();
    this.selectedScheme = null;
  }


  //--------------------------------------

  private updateGroupSetting() {
    this.concTreePref.baseBroaderProp = this.baseBroaderProp;
    this.concTreePref.broaderProps = this.broaderProps.map(b => b.toNT());
    this.concTreePref.narrowerProps = this.narrowerProps.map(n => n.toNT());
    this.concTreePref.includeSubProps = this.includeSubProps;
    this.concTreePref.syncInverse = this.syncInverse;
    this.settingsService.storeSettingForProjectAdministration(ExtensionPointID.ST_CORE_ID, Scope.PROJECT_GROUP, SettingsEnum.conceptTree, this.concTreePref, this.project, null, this.selectedGroup).subscribe();
  }

}