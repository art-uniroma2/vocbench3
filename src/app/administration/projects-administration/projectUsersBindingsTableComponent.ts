import { Component, Input, SimpleChanges } from "@angular/core";
import { Language, Languages } from "src/app/models/LanguagesCountries";
import { Project } from "../../models/Project";
import { ProjectUserBinding, User } from "../../models/User";
import { AdministrationServices } from "../../services/administration.service";

@Component({
    selector: "project-users-bindings-table",
    templateUrl: "./projectUsersBindingsTableComponent.html",
    standalone: false
})
export class ProjectUsersBindingsTableComponent {

    @Input() project: Project;

    bindings: { user: User, binding: ProjectUserBinding }[];

    constructor(
        private adminService: AdministrationServices
    ) { }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['project'] && changes['project'].currentValue) {
            this.adminService.getProjectUserBindings(this.project).subscribe(
                bindings => {
                    bindings.forEach(b => {
                        //convert lang tag to Language instance to use <lang-item> in the view
                        let languages: Language[] = Languages.fromTagsToLanguages(b.binding.getLanguages());
                        b.binding['languagesEnriched'] = languages;
                    });
                    this.bindings = bindings;
                }
            );
        }
    }

}