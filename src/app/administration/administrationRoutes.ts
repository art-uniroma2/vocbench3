import { RouterModule, Routes } from '@angular/router';
import { adminGuard, authGuard } from "../utils/CanActivateGuards";
import { AdministrationComponent } from "./administration.component";
import { GroupsAdministrationComponent } from './groups-administration/groups-administration.component';
import { ProjectsAdministrationComponent } from "./projects-administration/projectsAdministrationComponent";
import { RolesAdministrationComponent } from "./roles-administration/roles-administration.component";
import { SettingsMgrConfigComponent } from './system-configuration/settings-mgr-config.component';
import { SystemConfigurationComponent } from './system-configuration/systemConfigurationComponent';
import { UserDefaultsEditorComponent } from './system-configuration/userDefaultsEditorComponent';
import { UsersAdministrationComponent } from "./users-administration/users-administration.component";

export const routes: Routes = [
  {
    path: "", component: AdministrationComponent, children: [
      { path: "", redirectTo: "Projects", pathMatch: "full" },
      { path: "Users", component: UsersAdministrationComponent, canActivate: [adminGuard] },
      { path: "UserDefaults", component: UserDefaultsEditorComponent, canActivate: [adminGuard] },
      // { path: "ProjDefaults", component: ProjectDefaultsEditorComponent, canActivate: [adminGuard] },
      { path: "Roles", component: RolesAdministrationComponent, canActivate: [authGuard] },
      { path: "Groups", component: GroupsAdministrationComponent, canActivate: [authGuard] },
      { path: "Projects", component: ProjectsAdministrationComponent, canActivate: [authGuard] },
      { path: "Configuration", component: SystemConfigurationComponent, canActivate: [adminGuard] },
      { path: "SettingsMgr", component: SettingsMgrConfigComponent, canActivate: [adminGuard] },
    ]
  },
];

export const adminRouting = RouterModule.forChild(routes);