import { Component } from '@angular/core';
import { ExtensionPointID, Scope } from 'src/app/models/Plugins';
import { ProjectVisualization } from 'src/app/models/Project';
import { SettingsEnum } from 'src/app/models/Properties';
import { SettingsServices } from 'src/app/services/settings.service';

@Component({
    selector: 'user-defaults-editor',
    templateUrl: './userDefaultsEditorComponent.html',
    host: { class: "vbox" },
    standalone: false
})
export class UserDefaultsEditorComponent {

    projVisualization: ProjectVisualization;

    projectRendering: boolean;


    constructor(private settingsService: SettingsServices) { }

    ngOnInit() {
        /**
         * Defaults here are initialized from server and not by taking them from SettingsManager (cached):
         * - for authorized users the defaults are just used for fallback mechanism (in case of changes refresh cached settings, so to re-exploit fallback on defaults) 
         * - only for visitor users the defaults are cached client side but such users are not allowed to this page
         */
        this.settingsService.getSettingsDefault(ExtensionPointID.ST_CORE_ID, Scope.USER, Scope.SYSTEM).subscribe(
            settings => {
                this.projVisualization = settings.getPropertyValue(SettingsEnum.projectVisualization, new ProjectVisualization());
                this.projectRendering = settings.getPropertyValue(SettingsEnum.projectRendering, false);
            }
        );
    }

    onProjVisualizationChanged() {
        this.settingsService.storeSettingDefault(ExtensionPointID.ST_CORE_ID, Scope.USER, Scope.SYSTEM, SettingsEnum.projectVisualization, this.projVisualization).subscribe();
    }

    onProjRenderingChanged() {
        this.settingsService.storeSettingDefault(ExtensionPointID.ST_CORE_ID, Scope.USER, Scope.SYSTEM, SettingsEnum.projectRendering, this.projectRendering).subscribe();
    }

}