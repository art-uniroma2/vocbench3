import { Component } from "@angular/core";
import { Router } from '@angular/router';
import { HistoryServices } from "../services/history.service";
import { UIUtils } from "../utils/UIUtils";
import { VBEventHandler } from "../utils/VBEventHandler";
import { SharedModalServices } from "../modal-dialogs/shared-modals/shared-modals.service";
import { AbstractHistValidComponent } from "./abstractHistValidComponent";
import { HistoryValidationModalServices } from "./modals/historyValidationModalServices";

@Component({
    selector: "history-component",
    templateUrl: "./history.component.html",
    host: { class: "pageComponent" },
    standalone: false
})
export class HistoryComponent extends AbstractHistValidComponent {

    //paging
    private tipRevisionNumber: number;

    constructor(private historyService: HistoryServices, sharedModals: SharedModalServices, hvModals: HistoryValidationModalServices, eventHandler: VBEventHandler, router: Router) {
        super(sharedModals, hvModals, eventHandler, router);
    }

    ngOnInit() {
        this.init();
    }

    init() {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.page = 0;
        this.commits = [];
        this.historyService.getCommitSummary(this.operations, this.getPerformersIRI(), null, this.resource, true, this.getFormattedFromTime(), this.getFormattedToTime(), this.limit).subscribe(
            stResp => {
                UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
                this.pageCount = stResp.pageCount;
                this.pageSelector = [];
                for (let i = 0; i < this.pageCount; i++) {
                    this.pageSelector.push(i);
                }
                this.tipRevisionNumber = stResp.tipRevisionNumber;
                if (this.tipRevisionNumber != null) {
                    this.listCommits();
                }
            }
        );
    }

    listCommits() {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.historyService.getCommits(this.tipRevisionNumber, this.operations, this.getPerformersIRI(), null, this.resource, true, this.getFormattedFromTime(), this.getFormattedToTime(),
            this.operationSorting.id, this.timeSorting.id, this.page, this.limit).subscribe(
            commits => {
                this.commits = commits;
                UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            }
        );
    }

}