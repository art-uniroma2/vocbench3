import { Injectable } from "@angular/core";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalOptions } from 'src/app/modal-dialogs/Modals';
import { CommitInfo } from "../../models/History";
import { CommitDeltaModalComponent } from "./commit-delta-modal.component";
import { OperationParamsModalComponent } from "./operation-params-modal.component";

@Injectable()
export class HistoryValidationModalServices {

    constructor(private modalService: NgbModal) { }

    inspectParams(item: CommitInfo) {
        const modalRef: NgbModalRef = this.modalService.open(OperationParamsModalComponent, new ModalOptions());
        modalRef.componentInstance.commit = item;
        return modalRef;
    }

    getCommitDelta(item: CommitInfo) {
        const modalRef: NgbModalRef = this.modalService.open(CommitDeltaModalComponent, new ModalOptions('lg'));
        modalRef.componentInstance.commit = item;
        return modalRef;
    }

}