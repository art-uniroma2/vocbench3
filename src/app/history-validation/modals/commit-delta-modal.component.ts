import { Component, ElementRef, Input, ViewChild } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTBNode, ARTLiteral, ARTNode, ARTResource, ARTURIResource } from "../../models/ARTResources";
import { CommitInfo, CommitOperation } from "../../models/History";
import { HistoryServices } from "../../services/history.service";
import { ResourceUtils } from "../../utils/ResourceUtils";
import { UIUtils } from "../../utils/UIUtils";
import { VBContext } from "../../utils/VBContext";
import { SharedModalServices } from "../../modal-dialogs/shared-modals/shared-modals.service";

@Component({
  selector: "commit-modal",
  templateUrl: "./commit-delta-modal.component.html",
  standalone: false
})
export class CommitDeltaModalComponent {
  @Input() commit: CommitInfo;

  @ViewChild('blockingDiv', { static: true }) public blockingDivElement: ElementRef;


  additions: CommitOperation[];
  removals: CommitOperation[];
  truncated: boolean = false; //true if response contains
  messageTruncated: string;

  constructor(public activeModal: NgbActiveModal, private historyService: HistoryServices, private sharedModals: SharedModalServices) {
  }

  ngOnInit() {
    UIUtils.startLoadingDiv(this.blockingDivElement.nativeElement);
    this.historyService.getCommitDelta(this.commit.commit).subscribe(
      delta => {
        this.additions = delta.additions;
        this.removals = delta.removals;
        let additionsTruncated: number = delta.additionsTruncated;
        let removalsTruncated: number = delta.removalsTruncated;
        if (additionsTruncated || removalsTruncated) {
          this.truncated = true;
          if (additionsTruncated) {
            this.messageTruncated = "For performance issue, the additions reported have been limited to " + additionsTruncated + " triples";
            if (removalsTruncated) {
              this.messageTruncated += " as well as the removals";
            }
          } else {
            this.messageTruncated = "For performance issue, the removals reported have been limited to " + removalsTruncated + " triples";
          }
        }
        UIUtils.stopLoadingDiv(this.blockingDivElement.nativeElement);
      }
    );
  }

  getShow(res: ARTNode): string {
    if (res instanceof ARTBNode || res instanceof ARTLiteral) {
      return res.toNT();
    } else if (res instanceof ARTURIResource) {
      let qname: string = ResourceUtils.getQName(res.getURI(), VBContext.getPrefixMappings());
      if (qname != null) {
        return qname;
      } else {
        return res.toNT();
      }
    }
    return null; //not reachable
  }



  openResource(resource: ARTNode) {
    if (resource instanceof ARTResource) {
      this.sharedModals.openResourceView(resource, true);
    }
  }

  ok() {
    this.activeModal.close();
  }

}