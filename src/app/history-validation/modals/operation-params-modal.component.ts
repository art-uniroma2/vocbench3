import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTResource } from "../../models/ARTResources";
import { CommitInfo } from "../../models/History";
import { NTriplesUtil } from "../../utils/ResourceUtils";
import { SharedModalServices } from "../../modal-dialogs/shared-modals/shared-modals.service";

@Component({
  selector: "operation-params-modal",
  templateUrl: "./operation-params-modal.component.html",
  standalone: false
})
export class OperationParamsModalComponent {
  @Input() commit: CommitInfo;

  constructor(public activeModal: NgbActiveModal, private sharedModals: SharedModalServices) {
  }

  openValueResourceView(value: string) {
    try {
      let res: ARTResource;
      if (value.startsWith("<") && value.endsWith(">")) { //uri
        res = NTriplesUtil.parseURI(value);
      } else if (value.startsWith("_:")) { //bnode
        res = NTriplesUtil.parseBNode(value);
      }
      if (res != null) {
        this.sharedModals.openResourceView(res, true);
      }
    } catch {
      //not parseable => not a resource
    }
  }

  ok() {
    this.activeModal.close();
  }

}