import { Component, EventEmitter, Input, Output } from "@angular/core";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ARTURIResource } from "../models/ARTResources";
import { User } from "../models/User";
import { ResourceUtils } from "../utils/ResourceUtils";
import { BasicModalServices } from "../modal-dialogs/basic-modals/basic-modals.service";
import { ModalOptions, ModalType } from '../modal-dialogs/Modals';
import { SharedModalServices } from "../modal-dialogs/shared-modals/shared-modals.service";
import { OperationSelectModalComponent } from "./modals/operation-select-modal.component";

@Component({
    selector: "history-filter",
    templateUrl: "./history-filter.component.html",
    standalone: false
})
export class HistoryFilterComponent {

    @Input() operations: ARTURIResource[];
    @Input() performers: User[];
    @Input() resource: ARTURIResource; //resource involved as S/P/O in commit
    @Input() fromTime: string;
    @Input() toTime: string;
    //Useful in order to hide Performers filter from Validation page when user is not validator (it is forced to see only its commits) */
    @Input() hidePerformers: boolean;

    @Output() apply = new EventEmitter<HistoryFilterData>();

    constructor(private modalService: NgbModal, private sharedModals: SharedModalServices, private basicModals: BasicModalServices) { }

    ngOnInit() {
        if (this.operations == null) {
            this.operations = [];
        }
    }

    selectOperationFilter() {
        this.modalService.open(OperationSelectModalComponent, new ModalOptions()).result.then(
            (operations: any) => {
                //for each operation to add, add it only if not already in operations array
                operations.forEach((op: ARTURIResource) => {
                    if (!ResourceUtils.containsNode(this.operations, op)) {
                        this.operations.push(op);
                    }
                });
            },
            () => { }
        );
    }

    removeOperationFilter(operation: ARTURIResource) {
        this.operations.splice(this.operations.indexOf(operation), 1);
    }

    selectOperationPerformer() {
        this.sharedModals.selectUser({ key: "COMMONS.ACTIONS.SELECT_USER" }, true).then(
            (user: User) => {
                for (const p of this.performers) {
                    if (p.getIri().equals(user.getIri())) {
                        return; //user already in
                    }
                }
                this.performers.push(user);
            },
            () => { }
        );
    }

    removeOperationPerformer(user: User) {
        this.performers.splice(this.performers.indexOf(user), 1);
    }

    applyFilter() {
        let timeRegexp: RegExp = new RegExp("^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}$");
        if (this.fromTime != null && !timeRegexp.test(this.fromTime)) {
            this.basicModals.alert({ key: "STATUS.INVALID_VALUE" }, { key: "MESSAGES.INVALID_FROM_TIME_FORMAT" }, ModalType.error);
            return;
        }
        if (this.toTime != null && !timeRegexp.test(this.toTime)) {
            this.basicModals.alert({ key: "STATUS.INVALID_VALUE" }, { key: "MESSAGES.INVALID_TO_TIME_FORMAT" }, ModalType.error);
            return;
        }
        this.apply.emit({ operations: this.operations, performers: this.performers, resource: this.resource, fromTime: this.fromTime, toTime: this.toTime });
    }

}


export interface HistoryFilterData {
    operations?: ARTURIResource[];
    performers?: User[];
    resource?: ARTURIResource;
    fromTime?: string;
    toTime?: string;
}