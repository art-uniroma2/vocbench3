import { ChangeDetectorRef, Component, forwardRef, Input } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ARTLiteral, ARTNode, ARTURIResource, RDFResourceRolesEnum } from "../../models/ARTResources";
import { AnnotationName, FormField, FormFieldAnnotation } from "../../models/CustomForms";

@Component({
  selector: "custom-form-field",
  templateUrl: "./custom-form-field.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => CustomFormFieldComponent), multi: true,
  }],
  standalone: false
})
export class CustomFormFieldComponent implements ControlValueAccessor {

  @Input() lang: string;

  RDFResourceRolesEnum = RDFResourceRolesEnum;

  field: FormField;

  langString: { value?: string, lang?: string };

  constructor(private changeDetectorRef: ChangeDetectorRef) { }

  private init() {
    if (this.field.getConverter() == 'http://art.uniroma2.it/coda/contracts/langString') {
      let convArg = this.field.getConverterArg();
      if (convArg != null) {
        if (convArg.ph && this.lang) { //if an input language is provided and the field uses the coda:langString converter
          /* 
          the above conditions set visible the lang-picker, so set as default value of the ph argument the @input lang
          but first, check also if the language is not foreseen in case of @oneOfLang annotation restrictions
          */
          let oneOfLang: string[] = this.field['oneOfLang'];
          if (oneOfLang == null || oneOfLang.indexOf(this.lang) != -1) {
            this.field.getConverterArg().ph.value = this.lang;
          }
        }
      } else { //no argument (coda:langString()) is equivalent to literal^^rdf:langString
        this.langString = { lang: this.lang };
      }
    }
    if (this.field.getDatatype() == "http://www.w3.org/1999/02/22-rdf-syntax-ns#langString") {
      this.langString = { lang: this.lang };
    }
  }

  /* 
  Method for supporting strictTemplates: 
  according the annotation type, value of the ann may contain different type of object(s) 
  (see deserialization in CustomFormsServices#getCustomFormRepresentation)
  */
  getAnnValueDataOneOf(ann?: FormFieldAnnotation): ARTLiteral[] {
    if (ann == null) return null;
    //DataOneOf contains list of admitted literal values
    if (ann.name == AnnotationName.DataOneOf) {
      return ann.value as ARTLiteral[];
    } else {
      throw new Error("Invalid annotation type. Expected " + AnnotationName.DataOneOf + ", found" + ann.name);
    }
  }
  getAnnValueObjectOneOf(ann?: FormFieldAnnotation): ARTURIResource[] {
    if (ann == null) return null;
    //ObjectOneOf contains list of admitted IRI values
    if (ann.name == AnnotationName.ObjectOneOf) {
      return ann.value as ARTURIResource[];
    } else {
      throw new Error("Invalid annotation type. Expected " + AnnotationName.ObjectOneOf + ", found" + ann.name);
    }
  }
  getAnnValueRole(ann?: FormFieldAnnotation): RDFResourceRolesEnum[] {
    if (ann == null) return null;
    //Role contains list of admitted roles
    if (ann.name == AnnotationName.Role) {
      return ann.value as RDFResourceRolesEnum[];
    } else {
      throw new Error("Invalid annotation type. Expected " + AnnotationName.Role + ", found" + ann.name);
    }
  }
  getAnnValueRange(ann?: FormFieldAnnotation): ARTURIResource {
    if (ann == null) return null;
    //Range contains a range class
    if (ann.name == AnnotationName.Range) {
      return ann.value as ARTURIResource;
    } else {
      throw new Error("Invalid annotation type. Expected " + AnnotationName.Range + ", found" + ann.name);
    }
  }
  getAnnValueRangeList(ann?: FormFieldAnnotation): ARTURIResource[] {
    if (ann == null) return null;
    //RangeList contains list of range classes
    if (ann.name == AnnotationName.RangeList) {
      return ann.value as ARTURIResource[];
    } else {
      throw new Error("Invalid annotation type. Expected " + AnnotationName.RangeList + ", found" + ann.name);
    }
  }
  getAnnValueForeign(ann?: FormFieldAnnotation): string {
    if (ann == null) return null;
    //Foreign contains a project name
    if (ann.name == AnnotationName.Foreign) {
      return ann.value as string;
    } else {
      throw new Error("Invalid annotation type. Expected " + AnnotationName.Foreign + ", found" + ann.name);
    }
  }

  /**
   * Listener to change of lang-picker used to set the language argument of a formField that
   * has coda:langString as converter
   */
  onCodaLangStringPhLangChange(newLang: string, formFieldConvArgumentPh: FormField) {
    this.changeDetectorRef.detectChanges();
    formFieldConvArgumentPh.value = newLang;
    this.propagateChange(this.field);
  }

  onLangStringChange() {
    if (this.langString.value && this.langString.value.trim() != "") {
      this.field.value = new ARTLiteral(this.langString.value, null, this.langString.lang).toNT();
    } else {
      this.field.value = null;
    }
    this.propagateChange(this.field);
  }

  updateNodeField(res: ARTNode) {
    if (res != null) {
      this.field.value = res.getNominalValue();
    } else {
      this.field.value = null;
    }
    this.propagateChange(this.field);
  }

  onModelChanged() {
    this.propagateChange(this.field);
  }

  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(obj: FormField) {
    if (obj) {
      this.field = obj;
      this.init();
    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };

}