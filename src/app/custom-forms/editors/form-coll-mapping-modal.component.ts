import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { SharedModalServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { ARTURIResource } from "../../models/ARTResources";
import { FormCollection } from "../../models/CustomForms";
import { CustomFormsServices } from "../../services/custom-forms.service";
import { ResourceUtils } from "../../utils/ResourceUtils";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../modal-dialogs/browsing-modals/browsing-modals.service";

@Component({
    selector: "form-coll-mapping-modal",
    templateUrl: "./form-coll-mapping-modal.component.html",
    standalone: false
})
export class FormCollMappingModalComponent {

    formCollectionList: FormCollection[];
    selectedResourceIri: string;
    selectedFormColl: FormCollection;

    constructor(public activeModal: NgbActiveModal, private cfService: CustomFormsServices, private basicModals: BasicModalServices,
        private sharedModals: SharedModalServices,
        private browsingModals: BrowsingModalServices) {
    }

    ngOnInit() {
        this.cfService.getAllFormCollections().subscribe(
            fcList => {
                this.formCollectionList = fcList;
            }
        );
    }

    selectSuggested() {
        this.cfService.getFormCollection(this.selectedFormColl.getId()).subscribe(
            fc => {
                let suggestions: ARTURIResource[] = fc.getSuggestions();
                if (suggestions.length == 0) {
                    this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "MESSAGES.NO_RESOURCE_SUGGESTED_FOR_FORM_COLLECTION" }, ModalType.warning);
                } else {
                    this.sharedModals.selectResource({ key: "COMMONS.ACTIONS.SELECT_RESOURCE" }, null, suggestions).then(
                        (res: ARTURIResource[]) => {
                            this.selectedResourceIri = res[0].getURI();
                        },
                        () => { }
                    );
                }
            }
        );
    }

    selectProperty() {
        this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }).then(
            (res: ARTURIResource) => {
                this.selectedResourceIri = res.getURI();
            },
            () => { }
        );
    }

    selectClass() {
        this.browsingModals.browseClassTree({ key: "DATA.ACTIONS.SELECT_CLASS" }).then(
            (res: ARTURIResource) => {
                this.selectedResourceIri = res.getURI();
            },
            () => { }
        );
    }

    selectFormColl(formColl: FormCollection) {
        this.selectedFormColl = formColl;
    }

    ok() {
        this.selectedResourceIri = this.selectedResourceIri.trim();
        if (!ResourceUtils.testIRI(this.selectedResourceIri)) {
            this.basicModals.alert({ key: "STATUS.INVALID_VALUE" }, { key: "MESSAGES.INVALID_IRI", params: { iri: this.selectedResourceIri } }, ModalType.warning);
            return;
        }
        this.activeModal.close({ resource: new ARTURIResource(this.selectedResourceIri), formCollection: this.selectedFormColl.getId() });
    }

    cancel() {
        this.activeModal.dismiss();
    }
}