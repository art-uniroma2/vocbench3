import { Component, forwardRef, Input, SimpleChanges } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { ARTNode, ARTURIResource, RDFResourceRolesEnum } from "src/app/models/ARTResources";
import { WizardGraphEntry, WizardNode, WizardNodeResource } from "./CustomFormWizard";

@Component({
  selector: "custom-form-wizard-graph-editor",
  templateUrl: "./custom-form-wizard-graph-editor.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => CustomFormWizardGraphEditorComponent), multi: true,
  }],
  host: { class: "vbox" },
  standalone: false
})
export class CustomFormWizardGraphEditorComponent implements ControlValueAccessor {
  @Input() nodes: WizardNode[];

  RDFResourceRolesEnum = RDFResourceRolesEnum;

  graphs: WizardGraphEntry[];

  private resourceNode: WizardNodeResource; //this will be found/initialized only if dealing with CustomRange

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['nodes'] && changes['nodes'].currentValue != null) {
      this.resourceNode = this.nodes.find(n => n instanceof WizardNodeResource);
    }
  }

  addGraph() {
    let g: WizardGraphEntry = new WizardGraphEntry(this.resourceNode);
    this.graphs.push(g);
    this.onModelChange();
  }

  removeGraph(graph: WizardGraphEntry) {
    this.graphs.splice(this.graphs.indexOf(graph), 1);
    this.onModelChange();
  }

  onGraphPredicateChange(graph: WizardGraphEntry, predicate: ARTURIResource) {
    graph.predicate = predicate;
    this.onModelChange();
  }

  onGraphObjectValueChange(graph: WizardGraphEntry, value: ARTNode) {
    graph.object.value = value;
    this.onModelChange();
  }

  onModelChange() {
    this.propagateChange(this.graphs);
  }


  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(obj: WizardGraphEntry[]) {
    if (obj) {
      this.graphs = obj;
    } else {
      this.graphs = [];
    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };


}