import { Component, Input } from "@angular/core";
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalOptions } from 'src/app/modal-dialogs/Modals';
import { CustomForm } from "../../models/CustomForms";
import { CustomFormsServices } from "../../services/custom-forms.service";
import { CustomFormEditorModalComponent } from './custom-form-editor-modal.component';

@Component({
  selector: "cf-picker-modal",
  templateUrl: "./custom-form-picker-modal.component.html",
  standalone: false
})
export class CustomFormPickerModalComponent {
  @Input() title: string;

  customForms: CustomForm[];
  selectedCustomForm: CustomForm;

  constructor(public activeModal: NgbActiveModal, private cfService: CustomFormsServices, private modalService: NgbModal) { }

  ngOnInit() {
    this.cfService.getAllCustomForms().subscribe(
      cForms => {
        this.customForms = cForms;
      }
    );
  }

  showCustomForm() {
    const modalRef: NgbModalRef = this.modalService.open(CustomFormEditorModalComponent, new ModalOptions('xl'));
    modalRef.componentInstance.id = this.selectedCustomForm.getId();
    modalRef.componentInstance.readOnly = true;
  }

  ok() {
    this.activeModal.close(this.selectedCustomForm);
  }

  cancel() {
    this.activeModal.dismiss();
  }

}