import { Component } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BasicModalServices } from "../modal-dialogs/basic-modals/basic-modals.service";
import { UserServices } from "../services/user.service";
import { UIUtils } from "../utils/UIUtils";

@Component({
  selector: "reset-password",
  templateUrl: "./reset-password.component.html",
  host: { class: "pageComponent" },
  standalone: false
})
export class ResetPasswordComponent {

  email: string;
  private token: string;

  constructor(private userService: UserServices, private basicModals: BasicModalServices,
    private router: Router, private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.token = this.activeRoute.snapshot.params['token'];
  }

  isEmailValid(): boolean {
    return (this.email != null && this.email.trim() != "");
  }

  reset() {
    UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
    this.userService.resetPassword(this.email, this.token).subscribe(
      () => {
        UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
        this.basicModals.alert({ key: "STATUS.OPERATION_DONE" }, { key: "MESSAGES.PASSWORD_RESET" }).then(
          () => {
            this.router.navigate(["/Home"]);
          }
        );
      }
    );
  }

}