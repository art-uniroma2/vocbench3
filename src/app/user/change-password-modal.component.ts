import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthServices } from "../services/auth.service";
import { UserServices } from "../services/user.service";
import { VBContext } from "../utils/VBContext";
import { BasicModalServices } from "../modal-dialogs/basic-modals/basic-modals.service";

@Component({
    selector: "change-pwd-modal",
    templateUrl: "./change-password-modal.component.html",
    standalone: false
})
export class ChangePasswordModalComponent {

    oldPwd: string;
    newPwd: string;
    newPwdConfirm: string;

    constructor(public activeModal: NgbActiveModal, private userService: UserServices, private authService: AuthServices,
        private basicModals: BasicModalServices) {
    }

    isInputValid(): boolean {
        return (
            this.oldPwd != undefined && this.oldPwd.trim() != "" &&
            this.newPwd != undefined && this.newPwd.trim() != "" &&
            this.newPwdConfirm != undefined && this.newPwdConfirm.trim() != "" &&
            this.isNewPwdConfirmOk()
        );
    }

    isNewPwdConfirmOk(): boolean {
        return this.newPwd == this.newPwdConfirm;
    }

    ok() {
        this.userService.changePassword(VBContext.getLoggedUser().getEmail(), this.oldPwd, this.newPwd).subscribe(
            () => {
                this.basicModals.alert({ key: "STATUS.OPERATION_DONE" }, { key: "MESSAGES.PASSWORD_CHANGED_LOGGING_OUT" }).then(
                    () => {
                        this.authService.logout().subscribe(
                            () => {
                                this.activeModal.close();
                            }
                        );

                    }
                );

            }
        );

    }

    cancel() {
        this.activeModal.dismiss();
    }

}