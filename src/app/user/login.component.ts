import { Component, EventEmitter, Output } from "@angular/core";
import { AuthServiceMode } from "../models/Properties";
import { User } from "../models/User";
import { AuthServices } from "../services/auth.service";
import { UserServices } from "../services/user.service";
import { HttpManager } from "../utils/HttpManager";
import { UIUtils } from "../utils/UIUtils";
import { VBContext } from "../utils/VBContext";
import { BasicModalServices } from "../modal-dialogs/basic-modals/basic-modals.service";
import { Oauth2Service } from "../services/oauth2.service";

@Component({
  selector: "login",
  templateUrl: "./login.component.html",
  standalone: false
})
export class LoginComponent {

  @Output() loggedIn: EventEmitter<User> = new EventEmitter<User>();

  authServMode: AuthServiceMode;

  private rememberMe: boolean = false;
  email: string;
  password: string;

  samlAction: string;
  samlBtnLabel: string;

  constructor(
    private authService: AuthServices,
    private userService: UserServices,
    private basicModals: BasicModalServices,
    private oauth2Service: Oauth2Service
  ) { }

  ngOnInit() {
    this.authServMode = VBContext.getSystemSettings().authService;
    if (this.authServMode == AuthServiceMode.SAML) {
      let serverhost = HttpManager.getServerHost();
      // this.samlAction = serverhost + "/semanticturkey/it.uniroma2.art.semanticturkey/st-core-services/saml/login";
      this.samlAction = serverhost + "/semanticturkey/saml2/authenticate/st_saml";
    }
    let saml_login_label: string = window['saml_login_label'];
    this.samlBtnLabel = saml_login_label ? saml_login_label : "SAML login";
  }

  onKeydown(event: KeyboardEvent) {
    if (event.key == "Enter") {
      this.login();
    }
  }

  loginOauth2() {
    this.oauth2Service.login();
  }


  login() {
    this.authService.login(this.email, this.password, this.rememberMe).subscribe(
      () => {
        if (VBContext.isLoggedIn()) {
          this.loggedIn.emit();
        }
      }
      //wrong login is already handled in HttpManager#handleError 
    );
  }

  forgotPassword() {
    this.basicModals.prompt({ key: "USER.PASSWORD.FORGOT_PASSWORD" }, { value: "E-mail" }, { key: "MESSAGES.PASSWORD_RESET_INSERT_EMAIL" }).then(
      (email: string) => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.userService.forgotPassword(email.trim()).subscribe(
          () => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            this.basicModals.alert({ key: "USER.PASSWORD.FORGOT_PASSWORD" }, { key: "MESSAGES.PASSWORD_RESET_INSTRUCTION_SENT" });
          }
        );
      },
      () => { }
    );
  }

}