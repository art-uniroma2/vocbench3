import { Directive, HostBinding, HostListener } from "@angular/core";

@Directive({
  selector: '[shiftKey]',
  standalone: false
})
export class ShiftKeyDirective {

  /*
  Most of the occurrences of 
  this.isShiftPressed = false
  are for preventing issues that cuase shift-container class to stay applied
  even after shift key is released
  */

  @HostBinding('class.shift-container') isShiftPressed: boolean = false;

  @HostListener('window:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent) {
    if (event.key === 'Shift') {
      this.isShiftPressed = true;
    }
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(_event: KeyboardEvent) {
    this.isShiftPressed = false;
  }

  @HostListener('mousedown', ['$event'])
  handleMouseDown(event: MouseEvent) {
    if (this.isShiftPressed) {
      event.preventDefault(); // Prevent text selection when Shift is pressed
      this.isShiftPressed = false;
    }
  }

  @HostListener('window:contextmenu', ['$event'])
  handleContextMenu(_event: MouseEvent) {
    this.isShiftPressed = false;
  }

}