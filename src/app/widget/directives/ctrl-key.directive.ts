import { Directive, HostBinding, HostListener } from "@angular/core";

@Directive({
  selector: '[ctrlKey]',
  standalone: false
})
export class CtrlKeyDirective {

  /*
  Most of the occurrences of 
  this.isCtrlPressed = false
  are for preventing issues that cuase ctrl-container class to stay applied
  even after ctrl key is released
  */

  @HostBinding('class.ctrl-container') isCtrlPressed: boolean = false;

  @HostListener('window:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent) {
    if (event.key === 'Control' || event.metaKey) {
      this.isCtrlPressed = true;
    }
  }

  @HostListener('window:keyup', ['$event'])
  handleKeyUp(_event: KeyboardEvent) {
    this.isCtrlPressed = false;
  }

  @HostListener('mousedown', ['$event'])
  handleMouseDown(_event: MouseEvent) {
    if (this.isCtrlPressed) {
      this.isCtrlPressed = false;
    }
  }

  @HostListener('window:contextmenu', ['$event'])
  handleContextMenu(_event: MouseEvent) {
    this.isCtrlPressed = false;
  }

}