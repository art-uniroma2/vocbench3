import { Component, Input, Output, EventEmitter, ViewChild, ElementRef } from "@angular/core";
import { ARTNode } from "../../models/ARTResources";

@Component({
  selector: "resource-list",
  templateUrl: "./resource-list.component.html",
  standalone: false
})
export class ResourceListComponent<T extends ARTNode> {
  @Input() resources: T[];
  @Input() rendering: boolean = true;
  @Output() nodeSelected = new EventEmitter<T>();
  @Output() dblClicked = new EventEmitter<T>();

  @ViewChild('scrollableContainer') scrollableElement: ElementRef;

  resourceSelected: T;

  constructor() { }

  onResourceSelected(resource: T) {
    this.resourceSelected = resource;
    this.nodeSelected.emit(resource);
  }

  onDblClick(resource: T) {
    this.dblClicked.emit(resource);
  }

  //Resource limitation management
  private initialRes: number = 150;
  resLimit: number = this.initialRes;
  private increaseRate: number = this.initialRes / 5;
  onScroll() {
    let scrollElement: HTMLElement = this.scrollableElement.nativeElement;
    if (Math.abs(scrollElement.scrollHeight - scrollElement.offsetHeight - scrollElement.scrollTop) < 2) {
      //bottom reached => increase max range if there are more roots to show
      if (this.resLimit < this.resources.length) {
        this.resLimit += this.increaseRate;
      }
    }
  }

}