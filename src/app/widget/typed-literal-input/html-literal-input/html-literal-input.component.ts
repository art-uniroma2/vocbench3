import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { QuillModules } from 'ngx-quill';

import Quill from 'quill';
import Block from 'quill/blots/block';

Block.tagName = "DIV";
Quill.register(Block, true);

@Component({
  selector: 'html-literal-input',
  templateUrl: './html-literal-input.component.html',
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => HtmlLiteralInputComponent), multi: true,
  }],
  styles: [`
    ::ng-deep .ql-tooltip {
      z-index: 1;
    }
  `],
  standalone: false
})
export class HtmlLiteralInputComponent {

  value: string;

  quillModules: QuillModules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],
      [{ 'list': 'ordered' }, { 'list': 'bullet' }],
      [{ 'color': [] }, { 'background': [] }],
      ['clean'],
      ['link']
    ]
  };

  constructor() { }

  onValueChanged() {
    this.propagateChange(this.value);
  }

  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(obj: string) {
    if (obj) {
      this.value = obj;
    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_fn: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };

}