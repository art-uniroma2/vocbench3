import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "list-param-editor",
  templateUrl: "./list-param-editor.component.html",
  standalone: false
})
export class ListParamEditorComponent {

  @Input() value: any[];

  @Output() valueChanged = new EventEmitter<string[]>();

  list: string[] = [];

  constructor() { }

  ngOnInit() {
    if (this.value == null || this.value.length == 0) {
      this.add();
    } else {
      this.list.push(...this.value);
    }
  }

  add() {
    this.list.push(null);
    this.onModelChange();
  }

  delete(index: number) {
    this.list.splice(index, 1);
    this.onModelChange();
  }

  /**
   * To call each time there is a change on the value array or on one of its member
   * (unless the change on the value array is an "add" of an empty entry)
   */
  onModelChange() {
    this.value = []; //reset value list, populate it from scratch and emit changes
    this.list.forEach(v => {
      //add the value only if it is provided
      if (v != null && v.trim() != "") {
        this.value.push(v);
      }
    });
    this.valueChanged.emit(this.value);
  }

  /**
   * To prevent the view is re-created at every change and the focus on the input field get lost
   * https://stackoverflow.com/q/40314732/5805661
   * @param index 
   * @param obj 
   */
  trackByIndex(index: number, _obj: any): any {
    return index;
  }

}