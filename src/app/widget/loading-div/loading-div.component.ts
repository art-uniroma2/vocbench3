import { Component } from '@angular/core';

@Component({
  selector: 'loading-div',
  templateUrl: './loading-div.component.html',
  styleUrls: ['./loading-div.component.css'],
  standalone: false
})
export class LoadingDivComponent {

  constructor() { }

}