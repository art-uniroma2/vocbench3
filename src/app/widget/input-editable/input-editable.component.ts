import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { UserForm } from "../../models/User";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { ModalType } from '../../modal-dialogs/Modals';

@Component({
  selector: 'input-editable',
  templateUrl: './input-editable.component.html',
  styles: [":host { display: block; }"],
  standalone: false
})
export class InputEditableComponent implements OnInit {
  @Input() value: any;
  @Input() size: string; //xs, sm, lg
  @Input() type: string; //text (default), email, date, number, select
  @Input() options: string[]; //options of select element. Used only if type = "select"
  @Input() min: number; //Useful only if type = "number"
  @Input() max: number; //Useful only if type = "number"
  @Input() step: number; //Useful only if type = "number"
  @Input() allowEmpty: boolean = false; //if true allow the value to be replaced with empty string
  @Input() disabled: boolean = false;
  @Input() readonly: boolean = false; //if true hides the edit button
  @Input() allowPwdToggle: boolean = false; //if true, allow to show/hide password input value (if type=password)

  @Output() valueEdited = new EventEmitter<any>();

  inputClass = "input-group";
  inputType = "text";
  editInProgress: boolean = false;
  private pristineValue: any; //original input value, as backup

  showPwd: boolean = false; //in case @Input type == password

  constructor(private basicModals: BasicModalServices) { }

  ngOnInit() {
    //init class of input field
    if (this.size == "xs" || this.size == "sm" || this.size == "lg") {
      this.inputClass += " input-group-" + this.size;
    }
    //init type of input field
    if (this.type == "email" || this.type == "date" || this.type == "number" || this.type == "password") {
      this.inputType = this.type;
    }

    this.pristineValue = this.value;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['value'] && changes['value'].currentValue) {
      this.pristineValue = this.value;
    }
  }

  edit() {
    this.editInProgress = true;
  }

  confirmEdit() {
    if (this.value == undefined || this.value.trim() == "") {
      if (this.allowEmpty) {
        this.value = null;
      } else {
        this.basicModals.alert({ key: "STATUS.INVALID_VALUE" }, { key: "MESSAGES.INVALID_OR_EMPTY_VALUE" }, ModalType.warning);
        return;
      }
    } else if (this.type == "email" && !UserForm.isValidEmail(this.value)) {
      this.basicModals.alert({ key: "STATUS.INVALID_VALUE" }, { key: "MESSAGES.INVALID_OR_EMPTY_VALUE" }, ModalType.warning);
      return;
    }
    this.editInProgress = false;
    this.pristineValue = this.value;
    this.valueEdited.emit(this.value);
  }

  cancelEdit() {
    this.editInProgress = false;
    this.value = this.pristineValue; //restore initial value
  }

}