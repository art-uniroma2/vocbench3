import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalizedMap } from "./localized-editor.component";

@Component({
  selector: "localized-editor-modal",
  templateUrl: "./localized-editor-modal.html",
  standalone: false
})
export class LocalizedEditorModalComponent {

  @Input() title: string;
  @Input() localizedMap: LocalizedMap;
  @Input() allowEmpty: boolean;

  editingMap: LocalizedMap;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
    this.editingMap = new Map(this.localizedMap);
  }

  isOkEnabled() {
    if (this.allowEmpty) {
      return true;
    } else {
      return Object.entries(this.editingMap).length > 0;
    }
  }

  ok() {
    this.activeModal.close(this.editingMap);
  }

  close() {
    this.activeModal.dismiss();
  }

}