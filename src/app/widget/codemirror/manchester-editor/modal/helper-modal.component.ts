import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'helper-modal',
    templateUrl: "helper-modal.component.html",
    styleUrls: ["./helper-modal.component.css"],
    standalone: false
})

export class HelperModalComponent {

    constructor(public activeModal: NgbActiveModal) {}

    ok() {
        this.activeModal.close();
    }

}