import { ChangeDetectorRef, Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Configuration } from 'src/app/models/Configuration';
import { ConfigurableExtensionFactory, SettingsProp, SettingsPropTypeConstraint } from 'src/app/models/Plugins';
import { ExtensionsServices } from 'src/app/services/extensions.service';
import { ExtensionConfiguratorComponent } from '../extension-configurator/extension-configurator.component';

@Component({
  selector: 'setting-configuration',
  templateUrl: './setting-configuration-renderer.component.html',
  standalone: false
})
export class SettingConfigurationRendererComponent {

  @Input() prop: SettingsProp;
  // @Input() value: Configuration;
  @Input() value: { [key: string]: any }; //configuration prop-value mappings
  @Input() disabled: boolean = false;
  @Input() constraints: SettingsPropTypeConstraint[];
  @Output() valueChanged = new EventEmitter<any>();

  @ViewChild(ExtensionConfiguratorComponent) extConfigurator: ExtensionConfiguratorComponent;

  extPointId: string;
  extensions: ConfigurableExtensionFactory[];

  selectedExtension: ConfigurableExtensionFactory;
  extensionConfig: Configuration;

  constructor(private extensionService: ExtensionsServices, private changeDetectorRef: ChangeDetectorRef) { }

  ngOnInit() {
    if (this.constraints.length > 0) {
      this.extPointId = this.constraints[0].value;
      this.extensionService.getExtensions(this.extPointId).subscribe(
        extensions => {
          /* if a configuration (input value or default) needs to be restored, I need to copy value to this temp variable
          since value will be reset as soon as extensions is initialized and ExtensionConfiguratorComponent
          emits extensionUpdated and configurationUpdated */
          let propValueMapToRestore: any;
          if (this.value) {
            propValueMapToRestore = Object.assign({}, this.value);
          } else if (this.prop.default) {
            propValueMapToRestore = Object.assign({}, this.prop.default);
          }

          this.extensions = extensions as ConfigurableExtensionFactory[];
          if (propValueMapToRestore) { //if a config needs to be restored ....
            this.changeDetectorRef.detectChanges(); //wait that child ExtensionConfiguratorComponent is initialized
            this.extConfigurator.selectExtensionAndConfiguration(this.extPointId, propValueMapToRestore['@type']); //first select the extPoint and the config type
            this.changeDetectorRef.detectChanges(); //wait that child ExtensionConfiguratorComponent select the config and emit the configurationUpdated event which sets extensionConfig
            this.extensionConfig.properties.forEach(prop => { //set the value to restore into the selected Configuration object
              let value = propValueMapToRestore[prop.name];
              if (value != null) {
                prop.value = value;
              }
            });
            this.emitChanges();
          }
        }
      );
    }
  }


  onExtensionChange(selectedExtension: ConfigurableExtensionFactory) {
    this.selectedExtension = selectedExtension;
  }

  onExtensionConfigUpdated(config: Configuration) {
    this.extensionConfig = config;
    this.emitChanges();
  }

  emitChanges() {
    this.value = this.extensionConfig;
    this.valueChanged.emit(this.value.getPropertiesAsMap());
  }

}