import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ConverterContractDescription } from "src/app/models/Coda";
import { RangeType } from "src/app/services/properties.service";
import { ConverterConfigStatus } from "src/app/widget/converter-configurator/converter-configurator.component";
import { ModalType, SelectionOption } from 'src/app/modal-dialogs/Modals';
import { ARTNode, ARTResource, ARTURIResource, RDFResourceRolesEnum } from "../../models/ARTResources";
import { Pair } from "../../models/Shared";
import { CODAConverter, NodeConversion, NodeMemoization, NodeSanitization, S2RDFModel, SimpleHeader } from "../../models/Sheet2RDF";
import { Sheet2RDFServices } from "../../services/sheet2rdf.service";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../modal-dialogs/browsing-modals/browsing-modals.service";
import { Sheet2RdfContextService } from "../sheet2rdfContext";

@Component({
  selector: "subject-header-editor-modal",
  templateUrl: "./subject-header-editor-modal.component.html",
  standalone: false
})
export class SubjectHeaderEditorModalComponent {
  @Input() sheetName: string;

  s2rdfModel: S2RDFModel;

  selectedHeader: SimpleHeader;

  assertType: boolean = false;
  type: ARTResource;

  sanitization: NodeSanitization;
  sheetSanitization: NodeSanitization;

  selectedConverter: CODAConverter;
  memoizeData: NodeMemoization;
  memoizedNodes: NodeConversion[];

  defaultNamespace: string;

  additionalPredObjs: PredObjPair[];

  RDFResourceRolesEnum = RDFResourceRolesEnum;
  RangeType = RangeType;

  constructor(public activeModal: NgbActiveModal, private s2rdfService: Sheet2RDFServices, private s2rdfCtx: Sheet2RdfContextService,
    private basicModals: BasicModalServices, private browsingModals: BrowsingModalServices) {
  }

  ngOnInit() {
    this.s2rdfModel = this.s2rdfCtx.sheetModelMap.get(this.sheetName);
    /**
     * restore the previous subject header choices
     */
    //selected header
    this.s2rdfModel.headers.forEach(h => {
      if (h.id == this.s2rdfModel.subjectHeader.id) {
        this.selectedHeader = h;
      }
    });
    //type + type assertion
    this.type = this.s2rdfModel.subjectHeader.graph.type;
    if (this.type != null) {
      this.assertType = true;
    }

    this.defaultNamespace = this.s2rdfModel.subjectHeader.node.defaultNamespace;

    //sanitization
    this.sanitization = this.s2rdfModel.subjectHeader.node.sanitization;
    this.sheetSanitization = this.s2rdfModel.ruleSanitization;

    //converter
    if (this.s2rdfModel.subjectHeader.node.converter != null) {
      this.selectedConverter = this.s2rdfModel.subjectHeader.node.converter;
      this.memoizeData = this.s2rdfModel.subjectHeader.node.memoization;
    }
    //additional po
    this.additionalPredObjs = [];
    this.s2rdfModel.subjectHeader.additionalGraphs.forEach(g => {
      this.additionalPredObjs.push({ predicate: g.property, object: g.value });
    });

    this.initMemoizedNodes();
  }

  changeType() {
    this.browsingModals.browseClassTree({ key: "DATA.ACTIONS.SELECT_CLASS" }).then(
      (cls: ARTURIResource) => {
        this.type = cls;
      }
    );
  }

  onConverterUpdate(updateStatus: ConverterConfigStatus) {
    this.selectedConverter = updateStatus.converter;
  }

  isConverterRandom() {
    return this.selectedConverter != null && this.selectedConverter.contractUri == ConverterContractDescription.NAMESPACE + "randIdGen";
  }

  private initMemoizedNodes() {
    //collect the nodes that uses the same memoization map
    let sourceNodes: NodeConversion[] = [];
    //from the subject header
    if (this.s2rdfModel.subjectHeader.node.memoization.enabled) {
      if (!sourceNodes.some(sn => sn.nodeId == this.s2rdfModel.subjectHeader.node.nodeId)) { //collect it if not yet in the sourceNodes list
        sourceNodes.push(this.s2rdfModel.subjectHeader.node);
      }
    }
    //from all the other headers
    this.s2rdfModel.headers.forEach(h => {
      h.nodes.forEach(n => {
        if (n.memoization.enabled) {
          if (!sourceNodes.some(sn => sn.nodeId == n.nodeId)) { //collect it if not yet in the sourceNodes list
            sourceNodes.push(n);
          }
        }
      });
    });
    this.memoizedNodes = sourceNodes.length > 0 ? sourceNodes : null;
  }

  selectNodeToBind() {
    let opts: SelectionOption[] = this.memoizedNodes.map(n => {
      return {
        value: n.nodeId,
        description: "(Memoization map ID: " + (n.memoization.id ? n.memoization.id : "Default") + ")"
      };
    });
    this.basicModals.select({ key: "SHEET2RDF.HEADER_EDITOR.COPY_MEMOIZED_NODE_CONVERTER" }, { key: "SHEET2RDF.HEADER_EDITOR.SELECT_MEMOIZED_NODE" }, opts).then(
      (opt: SelectionOption) => {
        let selectedSourceMemoNode: NodeConversion = this.memoizedNodes.find(n => n.nodeId == opt.value);
        this.memoizeData = selectedSourceMemoNode.memoization;
        this.selectedConverter = selectedSourceMemoNode.converter;
      },
      () => { }
    );
  }


  /* ============
  Additional predicate-objects 
  * ============ */

  addAdditionalPredObj() {
    this.additionalPredObjs.push({ predicate: null, object: null });
  }

  onAdditionalPropChanged(po: PredObjPair, prop: ARTURIResource) {
    po.predicate = prop;
  }

  onAdditionalObjChanged(po: PredObjPair, obj: ARTNode) {
    po.object = obj;
  }

  removeAdditionalPredObj(po: PredObjPair) {
    this.additionalPredObjs.splice(this.additionalPredObjs.indexOf(po), 1);
  }

  /**
   * Ok is enabled if
   * - header to use as subject is selected
   * - type assertion is true and a type is selected
   * - converter is selected
   * - all the parameters (if any) of the converter signature are provided
   */
  isOkEnabled() {
    let signatureOk: boolean = true;
    if (this.selectedConverter != null) {
      signatureOk = CODAConverter.isSignatureOk(this.selectedConverter);
    }
    return (
      this.selectedHeader != null &&
      (!this.assertType || this.type) &&
      this.selectedConverter != null &&
      signatureOk
    );
  }

  ok() {
    //check that there are no additional PO pending
    for (let po of this.additionalPredObjs) {
      if (po.predicate == null || po.object == null) {
        this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "MESSAGES.INCOMPLETE_PRED_OBJ_PAIR" }, ModalType.warning);
        return;
      }
    }
    //prepare the additional PO param
    let additionalPOParam: Pair<ARTURIResource, ARTNode>[] = [];
    this.additionalPredObjs.forEach(po => {
      additionalPOParam.push({ first: po.predicate, second: po.object });
    });

    //execute the update
    this.s2rdfService.updateSubjectHeader(this.sheetName, this.selectedHeader.id, this.selectedConverter.contractUri, this.selectedConverter.params,
      this.type, this.memoizeData, this.sanitization, this.defaultNamespace, additionalPOParam).subscribe(
        () => {
          this.activeModal.close();
        }
      );
  }

  cancel() {
    this.activeModal.dismiss();
  }

}

interface PredObjPair {
  predicate: ARTURIResource;
  object: ARTNode;
}