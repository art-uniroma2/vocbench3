import { Component, forwardRef, Input, SimpleChanges } from "@angular/core";
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { ConvertCaseStatus, NodeSanitization } from "../../models/Sheet2RDF";

@Component({
  selector: "node-sanitization-editor",
  templateUrl: "./node-sanitization-editor.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => NodeSanitizationEditorComponent), multi: true,
  }],
  standalone: false
})
export class NodeSanitizationEditorComponent {

  @Input() fallback: NodeSanitization;

  sanitization: NodeSanitization;

  trim: boolean;

  removeDuplicateSpaces: boolean;

  removePunctuationStatus: boolean;
  removePunctuationChars: string;

  convertCase: boolean = false;
  convertCaseStatuses: ConvertCaseStatus[] = [
    { translationKey: "SHEET2RDF.HEADER_EDITOR.SANITIZATION.LOWER_CASE", lowercase: true },
    { translationKey: "SHEET2RDF.HEADER_EDITOR.SANITIZATION.UPPER_CASE", uppercase: true }
  ];
  convertCaseChoice: ConvertCaseStatus = this.convertCaseStatuses[0];

  locked: boolean = false;

  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['fallback'] && changes['fallback'].currentValue) {
      this.init();
    }
  }

  init() {
    if (this.sanitization == null) {
      this.sanitization = new NodeSanitization();
    }
    if (
      this.sanitization.trim == null &&
      this.sanitization.removeDuplicateSpaces == null &&
      this.sanitization.removePunctuation.enabled == null &&
      this.sanitization.lowerCase == null &&
      this.sanitization.upperCase == null
    ) {
      //in case the input sanitization object is empty, the lock is applied only if a fallback sanitization is provided
      //(for example, the default sanitization of the sheet has no fallback, so cannot be unlocked/locked)
      this.locked = this.fallback != null;
      this.restoreInheritedSanitization();
    } else {
      //input sanitization not empty
      //unlock the editor (since it means that the fallback sanitization has been overriden)
      this.locked = false;
      //and init the model for the view
      this.trim = this.sanitization.trim;
      this.removeDuplicateSpaces = this.sanitization.removeDuplicateSpaces;
      this.removePunctuationStatus = this.sanitization.removePunctuation.enabled;
      this.removePunctuationChars = this.sanitization.removePunctuation.chars;
      if (this.sanitization.lowerCase) {
        this.convertCase = true;
        this.convertCaseChoice = this.convertCaseStatuses.find(s => s.lowercase);
      } else if (this.sanitization.upperCase) {
        this.convertCase = true;
        this.convertCaseChoice = this.convertCaseStatuses.find(s => s.uppercase);
      }
    }
  }

  toggleLock() {
    this.locked = !this.locked;
    if (this.locked) {
      this.restoreInheritedSanitization();

    } else {
      this.explicitSanitization();
    }
    this.propagateChange(this.sanitization);
  }

  private explicitSanitization() {
    //restore the fallback sanitization for the view
    this.sanitization.trim = this.trim;
    this.sanitization.removeDuplicateSpaces = this.removeDuplicateSpaces;
    this.sanitization.removePunctuation = {
      enabled: this.removePunctuationStatus == true,
      chars: this.removePunctuationChars
    };
    this.sanitization.lowerCase = this.convertCase && this.convertCaseChoice.lowercase;
    this.sanitization.upperCase = this.convertCase && this.convertCaseChoice.uppercase;
  }

  private restoreInheritedSanitization() {
    //reset node sanitization
    this.sanitization.trim = null;
    this.sanitization.removeDuplicateSpaces = null;
    this.sanitization.removePunctuation = { enabled: null };
    this.sanitization.lowerCase = null;
    this.sanitization.upperCase = null;

    //restore the fallback sanitization for the view
    let defaultSanitization: NodeSanitization = this.fallback ? this.fallback : NodeSanitization.CodaDefaultSanitization;

    this.trim = defaultSanitization.trim != null ? defaultSanitization.trim : true;
    this.removeDuplicateSpaces = defaultSanitization.removeDuplicateSpaces != null ? defaultSanitization.removeDuplicateSpaces : true;
    this.removePunctuationStatus = defaultSanitization.removePunctuation.enabled;
    this.removePunctuationChars = defaultSanitization.removePunctuation.chars;
    if (defaultSanitization.lowerCase) {
      this.convertCase = true;
      this.convertCaseChoice = this.convertCaseStatuses.find(s => s.lowercase);
    } else if (defaultSanitization.upperCase) {
      this.convertCase = true;
      this.convertCaseChoice = this.convertCaseStatuses.find(s => s.uppercase);
    } else {
      this.convertCase = false;
    }
  }


  onModelChange() {
    if (!this.locked) {
      this.sanitization.trim = this.trim;
      if (this.convertCase) {
        this.sanitization.upperCase = this.convertCaseChoice.uppercase;
        this.sanitization.lowerCase = this.convertCaseChoice.lowercase;
      } else {
        this.sanitization.upperCase = false;
        this.sanitization.lowerCase = false;
      }
      this.sanitization.removeDuplicateSpaces = this.removeDuplicateSpaces;
      this.sanitization.removePunctuation = {
        enabled: this.removePunctuationStatus,
        chars: this.removePunctuationChars,
      };
    }
    this.propagateChange(this.sanitization);
  }

  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(obj: NodeSanitization) {
    if (obj) {
      this.sanitization = obj;
    }
    this.init();
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };


}