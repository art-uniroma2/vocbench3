import { Component, forwardRef } from "@angular/core";
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { NodeMemoization } from "src/app/models/Sheet2RDF";
import { BasicModalServices } from "src/app/modal-dialogs/basic-modals/basic-modals.service";
import { Sheet2RdfContextService } from "../sheet2rdfContext";


@Component({
  selector: "memoization-editor",
  templateUrl: "./memoization-editor.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => MemoizationEditorComponent), multi: true,
  }],
  standalone: false
})
export class MemoizationEditorComponent {

  memoizeIds: string[];
  memoizeData: NodeMemoization;

  constructor(private s2rdfCtx: Sheet2RdfContextService, private basicModals: BasicModalServices) { }

  ngOnInit() {
    this.memoizeData = {
      enabled: false,
      ignoreCase: true,
    };
    this.memoizeIds = this.s2rdfCtx.memoizeIdList;
  }

  addMap() {
    this.basicModals.prompt({ key: "SHEET2RDF.HEADER_EDITOR.ADD_MEMOIZE_MAP" }, { value: "ID" }, null, null, false, true).then(
      id => {
        if (!this.memoizeIds.includes(id)) {
          this.memoizeIds.push(id);
        }
        this.memoizeData.id = id;
        this.onModelChange();
      },
      () => { }
    );
  }

  onModelChange() {
    this.propagateChange(this.memoizeData);
  }

  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(obj: NodeMemoization) {
    if (obj) {
      this.memoizeData = obj;
    } else {
      this.memoizeData = {
        enabled: false,
        ignoreCase: true,
      };
    }
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };

}