import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NodeSanitization } from 'src/app/models/Sheet2RDF';

@Component({
    selector: "content-sanitization-modal",
    templateUrl: "./content-sanitization-editor-modal.component.html",
    standalone: false
})
export class ContentSanitizationEditorModalComponent {

    @Input() sanitization: NodeSanitization;

    constructor(public activeModal: NgbActiveModal) { }


    ok() {
        this.activeModal.close(this.sanitization);
    }


    cancel() {
        this.activeModal.dismiss();
    }

}