import { Component, forwardRef } from "@angular/core";
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { VBContext } from 'src/app/utils/VBContext';

@Component({
  selector: "conversion-namespace-editor",
  templateUrl: "./conversion-namespace-editor.component.html",
  providers: [{
    provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => ConversionNamespaceEditorComponent), multi: true,
  }],
  styles: [":host { display: block; }"],
  standalone: false
})
export class ConversionNamespaceEditorComponent {

  defaultNamespace: string;
  namespaceLocked: boolean;

  projectNamespace: string;

  constructor() { }

  ngOnInit() {
    this.projectNamespace = VBContext.getWorkingProject().getDefaultNamespace();
  }

  init() {
    this.namespaceLocked = this.defaultNamespace == null;
  }

  toggleNamespaceLock() {
    this.namespaceLocked = !this.namespaceLocked;
    if (this.namespaceLocked) {
      this.defaultNamespace = null;
    } else {
      this.defaultNamespace = this.projectNamespace;
    }
    this.onModelChange();
  }

  onModelChange() {
    if (this.defaultNamespace == "") {
      this.defaultNamespace = null;
    }
    this.propagateChange(this.defaultNamespace);
  }

  //---- method of ControlValueAccessor and Validator interfaces ----
  /**
   * Write a new value to the element.
   */
  writeValue(obj: string) {
    this.defaultNamespace = obj;
    this.init();
  }
  /**
   * Set the function to be called when the control receives a change event.
   */
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  /**
   * Set the function to be called when the control receives a touch event. Not used.
   */
  registerOnTouched(_: any): void { }

  //--------------------------------------------------

  // the method set in registerOnChange, it is just a placeholder for a method that takes one parameter, 
  // we use it to emit changes back to the parent
  private propagateChange = (_: any) => { };


}