import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { AuthServiceMode } from "../models/Properties";
import { User } from "../models/User";
import { AuthorizationEvaluator } from "../utils/AuthorizationEvaluator";
import { HttpManager, STRequestParams } from "../utils/HttpManager";
import { VBContext } from "../utils/VBContext";
import { VBEventHandler } from '../utils/VBEventHandler';
import { VBProperties } from '../utils/VBProperties';
import { Oauth2Service } from "./oauth2.service";

@Injectable()
export class AuthServices {

    private serviceName = "Auth";

    constructor(private httpMgr: HttpManager, private eventHandler: VBEventHandler, private router: Router, private vbProp: VBProperties, private oauth2Service: Oauth2Service) {
    }

    /**
     * Logs in and registers the logged user in the VBContext
     */
    login(email: string, password: string, rememberMe?: boolean): Observable<User> {
        let params: STRequestParams = {
            email: email,
            password: password,
            _spring_security_remember_me: rememberMe
        };
        return this.httpMgr.doPost(this.serviceName, "login", params).pipe(
            mergeMap(stResp => {
                let loggedUser: User = User.parse(stResp);
                VBContext.setLoggedUser(loggedUser);
                return this.vbProp.initUserSettings().pipe(
                    map(() => {
                        return loggedUser;
                    })
                );
            })
        );

    }



    /**
     * Logs out and removes the logged user from the VBContext
     */
    logout() {
        let logoutFn: Observable<any>;
        if (VBContext.getSystemSettings().authService == AuthServiceMode.OAuth2) {
            logoutFn = of(this.oauth2Service.logout());
        } else {
            let params: STRequestParams = {};
            logoutFn = this.httpMgr.doGet(this.serviceName, "logout", params);
        }
        return logoutFn.pipe(
            map(stResp => {
                this.router.navigate(["/Home"]);
                VBContext.removeLoggedUser();
                VBContext.removeWorkingProject();
                this.eventHandler.themeChangedEvent.emit(); //when quitting current project, reset the style to the default
                AuthorizationEvaluator.reset();
                return stResp;
            })
        );
    }


}