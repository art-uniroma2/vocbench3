import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ARTURIResource } from "../models/ARTResources";
import { CommitDelta, CommitInfo, CommitOperation, HistoryPaginationInfo, SortingDirection } from "../models/History";
import { Deserializer } from "../utils/Deserializer";
import { HttpManager } from "../utils/HttpManager";

@Injectable()
export class HistoryServices {

  private serviceName = "History";

  constructor(private httpMgr: HttpManager) { }

  /**
   * 
   * @param operationFilter 
   * @param timeLowerBound 
   * @param timeUpperBound 
   * @param limit 
   */
  getCommitSummary(operationFilter?: ARTURIResource[], performerFilter?: ARTURIResource[], validatorFilter?: ARTURIResource[],
    resourceFilter?: ARTURIResource, includeObjResourceFilter?: boolean,
    timeLowerBound?: string, timeUpperBound?: string, limit?: number): Observable<HistoryPaginationInfo> {
    let params: any = {
      operationFilter: operationFilter,
      performerFilter: performerFilter,
      validatorFilter: validatorFilter,
      resourceFilter: resourceFilter,
      includeObjResourceFilter: includeObjResourceFilter,
      timeLowerBound: timeLowerBound,
      timeUpperBound: timeUpperBound,
      limit: limit
    };
    return this.httpMgr.doGet(this.serviceName, "getCommitSummary", params).pipe(
      map(stResp => {
        return stResp;
      })
    );
  }

  /**
   * 
   * @param tipRevisionNumber 
   * @param operationFilter 
   * @param timeLowerBound 
   * @param timeUpperBound 
   * @param operationSorting 
   * @param timeSorting 
   * @param page 
   * @param limit 
   */
  getCommits(tipRevisionNumber: number, operationFilter?: ARTURIResource[], performerFilter?: ARTURIResource[], validatorFilter?: ARTURIResource[],
    resourceFilter?: ARTURIResource, includeObjResourceFilter?: boolean,
    timeLowerBound?: string, timeUpperBound?: string, operationSorting?: SortingDirection, timeSorting?: SortingDirection,
    page?: number, limit?: number): Observable<CommitInfo[]> {
    let params: any = {
      tipRevisionNumber: tipRevisionNumber,
      operationFilter: operationFilter,
      performerFilter: performerFilter,
      validatorFilter: validatorFilter,
      resourceFilter: resourceFilter,
      includeObjResourceFilter: includeObjResourceFilter,
      timeLowerBound: timeLowerBound,
      timeUpperBound: timeUpperBound,
      operationSorting: operationSorting,
      timeSorting: timeSorting,
      page: page,
      limit: limit
    };
    return this.httpMgr.doGet(this.serviceName, "getCommits", params).pipe(
      map(stResp => {
        let commits: CommitInfo[] = [];
        for (let commitJson of stResp) {
          commits.push(CommitInfo.parse(commitJson));
        }
        return commits;
      })
    );
  }

  /**
   * Returns the triples added and removed by the given commit
   * @param commit 
   */
  getCommitDelta(commit: ARTURIResource): Observable<CommitDelta> {
    let params: any = {
      commit: commit
    };
    return this.httpMgr.doGet(this.serviceName, "getCommitDelta", params).pipe(
      map(stResp => {
        let additions: CommitOperation[] = [];
        let removals: CommitOperation[] = [];

        let additionsJsonArray: any[] = stResp.additions;
        for (const a of additionsJsonArray) {
          additions.push(
            new CommitOperation(
              Deserializer.createRDFResource(a.subject),
              Deserializer.createURI(a.predicate),
              Deserializer.createRDFNode(a.object),
              Deserializer.createRDFResource(a.context)
            )
          );
        }

        let removalsJsonArray: any[] = stResp.removals;
        for (const r of removalsJsonArray) {
          removals.push(
            new CommitOperation(
              Deserializer.createRDFResource(r.subject),
              Deserializer.createURI(r.predicate),
              Deserializer.createRDFNode(r.object),
              Deserializer.createRDFResource(r.context)
            )
          );
        }

        let additionsTruncated: number = stResp.additionsTruncated;
        let removalsTruncated: number = stResp.removalsTruncated;

        let commitDelta: CommitDelta = {
          additions: additions,
          removals: removals,
          additionsTruncated: additionsTruncated,
          removalsTruncated: removalsTruncated
        };

        return commitDelta;
      })
    );
  }

  getTimeOfOrigin(resource?: ARTURIResource): Observable<Date> {
    let params = {
      resource: resource
    };
    return this.httpMgr.doGet(this.serviceName, "getTimeOfOrigin", params).pipe(
      map(datetime => {
        return new Date(datetime);
      })
    );
  }

}
