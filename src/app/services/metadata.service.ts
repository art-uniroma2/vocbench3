import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ImportStatus, OntologyImport, PrefixMapping, TransitiveImportMethodAllowance } from "../models/Metadata";
import { RDFFormat } from "../models/RDFFormat";
import { HttpManager, STRequestParams, STRequestOptions } from "../utils/HttpManager";
import { VBContext } from "../utils/VBContext";
import { VBEventHandler } from "../utils/VBEventHandler";

@Injectable()
export class MetadataServices {

  private serviceName = "Metadata";

  constructor(private httpMgr: HttpManager, private eventHandler: VBEventHandler) { }

  /**
   * Gets prefix mapping of the currently open project.
   * Returns an array of object with
   * "explicit" (tells if the mapping is explicited by the user or set by default),
   * "namespace" the namespace uri
   * "prefix" the prefix
   */
  getNamespaceMappings(): Observable<PrefixMapping[]> {
    let params: STRequestParams = {};
    return this.httpMgr.doGet(this.serviceName, "getNamespaceMappings", params).pipe(
      map(stResp => {
        let mappings: PrefixMapping[] = [];
        for (const m of stResp) {
          let mapping: PrefixMapping = {
            prefix: m.prefix,
            namespace: m.namespace,
            explicit: m.explicit
          };
          mappings.push(mapping);
        }
        mappings.sort((m1: PrefixMapping, m2: PrefixMapping) => {
          return m1.prefix.localeCompare(m2.prefix);
        });
        VBContext.setPrefixMappings(mappings);
        return mappings;
      })
    );
  }

  /**
   * Adds a prefix namespace mapping
   * @param prefix
   * @param namespace
   */
  setNSPrefixMapping(prefix: string, namespace: string) {
    let params = {
      prefix: prefix,
      namespace: namespace
    };
    let options: STRequestOptions = new STRequestOptions({
      errorHandlers: [
        { className: 'it.uniroma2.art.semanticturkey.ontology.InvalidPrefixException', action: 'skip' },
      ]
    });
    return this.httpMgr.doPost(this.serviceName, "setNSPrefixMapping", params, options);
  }

  /**
   * Removes a prefix namespace mapping
   * @param namespace
   */
  removeNSPrefixMapping(namespace: string) {
    let params = {
      namespace: namespace
    };
    return this.httpMgr.doPost(this.serviceName, "removeNSPrefixMapping", params);
  }

  /**
   * Changes the prefix of a prefix namespace mapping
   * @param prefix
   * @param namespace
   */
  changeNSPrefixMapping(prefix: string, namespace: string) {
    let params = {
      prefix: prefix,
      namespace: namespace
    };
    let options: STRequestOptions = new STRequestOptions({
      errorHandlers: [
        { className: 'it.uniroma2.art.semanticturkey.ontology.InvalidPrefixException', action: 'skip' },
      ]
    });
    return this.httpMgr.doPost(this.serviceName, "changeNSPrefixMapping", params, options);
  }

  /**
   * Get imported ontology.
   * Returns an array of imports, object with:
   * "status": availble values: "FAILED", "OK"
   * "@id": the uri of the ontology
   * "imports": array of recursive imports
   */
  getImports(): Observable<OntologyImport[]> {
    let params: STRequestParams = {};
    return this.httpMgr.doGet(this.serviceName, "getImports", params).pipe(
      map(stResp => {
        let importedOntologies: OntologyImport[] = [];
        for (const i of stResp) {
          importedOntologies.push(this.parseImport(i));
        }
        return importedOntologies;
      })
    );
  }

  private parseImport(importNode: any): OntologyImport {
    let id: string = importNode['@id'];
    let status: ImportStatus = importNode.status;
    let source = importNode.source;
    let imports: OntologyImport[] = [];
    if (importNode.imports != null) {
      for (const i of importNode.imports) {
        imports.push(this.parseImport(i));
      }
    }
    return { id: id, status: status, imports: imports, source: source };
  }

  /**
   * Removes an imported ontology
   * @param baseURI the baseURI that identifies the imported ontology
   */
  removeImport(baseURI: string) {
    let params: STRequestParams = {
      baseURI: baseURI
    };
    return this.httpMgr.doPost(this.serviceName, "removeImport", params).pipe(
      map(stResp => {
        this.eventHandler.refreshDataBroadcastEvent.emit();
        return stResp;
      })
    );
  }

  /**
   * Adds ontology importing it from web. Every time the project is open, the ontology is reimported from web.
   * @param baseURI baseURI of the ontology to import (url of the rdf file works as well)
   * @param altUrl alternative URL (???)
   * @param rdfFormat force the format to read the ontology file to import
   */
  addFromWeb(baseURI: string, transitiveImportAllowance: TransitiveImportMethodAllowance, altUrl?: string, rdfFormat?: RDFFormat) {
    let params: STRequestParams = {
      baseURI: baseURI,
      transitiveImportAllowance: transitiveImportAllowance
    };
    if (altUrl != undefined) {
      params.altUrl = altUrl;
    }
    if (rdfFormat != undefined) {
      params.rdfFormat = rdfFormat.name;
    }
    let options: STRequestOptions = new STRequestOptions({
      errorHandlers: [
        { className: '*', action: 'skip' },
      ]
    });
    return this.httpMgr.doPost(this.serviceName, "addFromWeb", params, options).pipe(
      map(stResp => {
        this.eventHandler.refreshDataBroadcastEvent.emit();
        return stResp;
      })
    );
  }

  /**
   * Adds ontology importing it from web and keep a copy of that in a mirror file.
   * @param baseURI baseURI of the ontology to import (url of the rdf file works as well)
   * @param mirrorFile the name of the mirror file
   * @param altUrl alternative URL (???)
   * @param rdfFormat force the format to read the ontology file to import
   */
  addFromWebToMirror(baseURI: string, mirrorFile: string, transitiveImportAllowance: TransitiveImportMethodAllowance, altUrl?: string, rdfFormat?: RDFFormat) {
    let params: STRequestParams = {
      baseURI: baseURI,
      mirrorFile: mirrorFile,
      transitiveImportAllowance: transitiveImportAllowance
    };
    if (altUrl != undefined) {
      params.altUrl = altUrl;
    }
    if (rdfFormat != undefined) {
      params.rdfFormat = rdfFormat.name;
    }
    let options: STRequestOptions = new STRequestOptions({
      errorHandlers: [
        { className: '*', action: 'skip' },
      ]
    });
    return this.httpMgr.doPost(this.serviceName, "addFromWebToMirror", params, options).pipe(
      map(stResp => {
        this.eventHandler.refreshDataBroadcastEvent.emit();
        return stResp;
      })
    );
  }

  /**
  * Adds ontology importing it from a local file and keep a copy of that in a mirror file.
  * @param baseURI baseURI of the ontology to import
  * @param localFile the file from the local filesystem to import
  * @param mirrorFile the name of the mirror file
  * @param transitiveImportAllowance available values 'web' | 'webFallbackToMirror' | 'mirrorFallbackToWeb' | 'mirror'
  */
  addFromLocalFile(baseURI: string, localFile: File, mirrorFile: string, transitiveImportAllowance: TransitiveImportMethodAllowance) {
    let data = {
      baseURI: baseURI,
      localFile: localFile,
      mirrorFile: mirrorFile,
      transitiveImportAllowance: transitiveImportAllowance
    };
    return this.httpMgr.uploadFile(this.serviceName, "addFromLocalFile", data).pipe(
      map(stResp => {
        this.eventHandler.refreshDataBroadcastEvent.emit();
        return stResp;
      })
    );
  }

  /**
   * Adds ontology importing it from a mirror file.
   * @param baseURI baseURI of the ontology to import
   * @param mirrorFile the name of the mirror file
   * @param transitiveImportAllowance available values 'web' | 'webFallbackToMirror' | 'mirrorFallbackToWeb' | 'mirror'
   */
  addFromMirror(baseURI: string, mirrorFile: string, transitiveImportAllowance: TransitiveImportMethodAllowance) {
    let params = {
      baseURI: baseURI,
      mirrorFile: mirrorFile,
      transitiveImportAllowance: transitiveImportAllowance
    };
    return this.httpMgr.doPost(this.serviceName, "addFromMirror", params).pipe(
      map(stResp => {
        this.eventHandler.refreshDataBroadcastEvent.emit();
        return stResp;
      })
    );
  }

  addFromLocalProject(projectName: string, transitiveImportAllowance: TransitiveImportMethodAllowance) {
    let params = {
      projectName: projectName,
      transitiveImportAllowance: transitiveImportAllowance
    };
    return this.httpMgr.doPost(this.serviceName, "addFromLocalProject", params).pipe(
      map(stResp => {
        this.eventHandler.refreshDataBroadcastEvent.emit();
        return stResp;
      })
    );
  }

  /**
   * Retrieves an ontology that is a failed import from a local file and copies it to the ontology mirror
   * @param baseURI 
   * @param localFile 
   * @param mirrorFile 
   * @param transitiveImportAllowance 
   */
  getFromLocalFile(baseURI: string, localFile: File, transitiveImportAllowance: TransitiveImportMethodAllowance, mirrorFile?: string) {
    let data = {
      baseURI: baseURI,
      localFile: localFile,
      mirrorFile: mirrorFile,
      transitiveImportAllowance: transitiveImportAllowance
    };
    return this.httpMgr.uploadFile(this.serviceName, "getFromLocalFile", data).pipe(
      map(stResp => {
        this.eventHandler.refreshDataBroadcastEvent.emit();
        return stResp;
      })
    );
  }

  /**
   * Retrieves an ontology that is a failed import from a local file and copies it to the ontology mirror
   * @param baseURI 
   * @param transitiveImportAllowance 
   */
  getFromMirror(baseURI: string, transitiveImportAllowance: TransitiveImportMethodAllowance) {
    let data = {
      baseURI: baseURI,
      transitiveImportAllowance: transitiveImportAllowance
    };
    return this.httpMgr.doPost(this.serviceName, "getFromMirror", data).pipe(
      map(stResp => {
        this.eventHandler.refreshDataBroadcastEvent.emit();
        return stResp;
      })
    );
  }

  getFromLocalProject(baseURI: string, projectName: string, transitiveImportAllowance: TransitiveImportMethodAllowance) {
    let data = {
      baseURI: baseURI,
      projectName: projectName,
      transitiveImportAllowance: transitiveImportAllowance
    };
    let options: STRequestOptions = new STRequestOptions({
      errorHandlers: [
        { className: 'it.uniroma2.art.semanticturkey.ontology.OntologyManagerException', action: 'warning' },
      ]
    });
    return this.httpMgr.doPost(this.serviceName, "getFromLocalProject", data, options);
  }


  /**
   * Downloads an ontology that is a failed import from the web
   * @param baseURI 
   * @param transitiveImportAllowance 
   * @param altUrl 
   */
  downloadFromWeb(baseURI: string, transitiveImportAllowance: TransitiveImportMethodAllowance, altUrl?: string, rdfFormat?: RDFFormat) {
    let params: STRequestParams = {
      baseURI: baseURI,
      transitiveImportAllowance: transitiveImportAllowance
    };
    if (altUrl != undefined) {
      params.altUrl = altUrl;
    }
    if (rdfFormat != undefined) {
      params.rdfFormat = rdfFormat.name;
    }
    return this.httpMgr.doPost(this.serviceName, "downloadFromWeb", params).pipe(
      map(stResp => {
        this.eventHandler.refreshDataBroadcastEvent.emit();
        return stResp;
      })
    );
  }

  /**
   * Downloads an ontology that is a failed import from the web to the ontology mirror
   * @param baseURI 
   * @param mirrorFile 
   * @param transitiveImportAllowance 
   * @param altUrl 
   */
  downloadFromWebToMirror(baseURI: string, mirrorFile: string, transitiveImportAllowance: TransitiveImportMethodAllowance, altUrl?: string, rdfFormat?: RDFFormat) {
    let params: STRequestParams = {
      baseURI: baseURI,
      mirrorFile: mirrorFile,
      transitiveImportAllowance: transitiveImportAllowance,
      altUrl: altUrl
    };
    if (rdfFormat != undefined) {
      params.rdfFormat = rdfFormat.name;
    }
    return this.httpMgr.doPost(this.serviceName, "downloadFromWebToMirror", params).pipe(
      map(stResp => {
        this.eventHandler.refreshDataBroadcastEvent.emit();
        return stResp;
      })
    );
  }

  updateFromWeb(baseURI: string, transitiveImportAllowance: TransitiveImportMethodAllowance, altUrl?: string, rdfFormat?: RDFFormat) {
    let params: STRequestParams = {
      baseURI: baseURI,
      transitiveImportAllowance: transitiveImportAllowance,
      altUrl: altUrl
    };
    if (rdfFormat != undefined) {
      params.rdfFormat = rdfFormat.name;
    }
    return this.httpMgr.doPost(this.serviceName, "updateFromWeb", params).pipe(
      map(stResp => {
        this.eventHandler.refreshDataBroadcastEvent.emit();
        return stResp;
      })
    );
  }

  updateFromWebToMirror(baseURI: string, mirrorFile: string, transitiveImportAllowance: TransitiveImportMethodAllowance, altUrl?: string, rdfFormat?: RDFFormat) {
    let params: STRequestParams = {
      baseURI: baseURI,
      mirrorFile: mirrorFile,
      transitiveImportAllowance: transitiveImportAllowance,
      altUrl: altUrl
    };
    if (rdfFormat != undefined) {
      params.rdfFormat = rdfFormat.name;
    }
    return this.httpMgr.doPost(this.serviceName, "updateFromWebToMirror", params).pipe(
      map(stResp => {
        this.eventHandler.refreshDataBroadcastEvent.emit();
        return stResp;
      })
    );
  }

  updateFromLocalFile(baseURI: string, localFile: File, transitiveImportAllowance: TransitiveImportMethodAllowance, mirrorFile?: string) {
    let data = {
      baseURI: baseURI,
      localFile: localFile,
      mirrorFile: mirrorFile,
      transitiveImportAllowance: transitiveImportAllowance
    };
    return this.httpMgr.uploadFile(this.serviceName, "updateFromLocalFile", data).pipe(
      map(stResp => {
        this.eventHandler.refreshDataBroadcastEvent.emit();
        return stResp;
      })
    );
  }


  updateFromMirror(baseURI: string, transitiveImportAllowance: TransitiveImportMethodAllowance) {
    let data = {
      baseURI: baseURI,
      transitiveImportAllowance: transitiveImportAllowance
    };
    return this.httpMgr.doPost(this.serviceName, "updateFromMirror", data);
  }

  updateFromLocalProject(baseURI: string, projectName: string, transitiveImportAllowance: TransitiveImportMethodAllowance) {
    let data = {
      baseURI: baseURI,
      projectName: projectName,
      transitiveImportAllowance: transitiveImportAllowance
    };
    let options: STRequestOptions = new STRequestOptions({
      errorHandlers: [
        { className: 'it.uniroma2.art.semanticturkey.ontology.OntologyManagerException', action: 'warning' },
      ]
    });
    return this.httpMgr.doPost(this.serviceName, "updateFromLocalProject", data, options);
  }


  disconnectFromProject(baseURI: string) {
    let data = {
      baseURI: baseURI,
    };
    return this.httpMgr.doPost(this.serviceName, "disconnectFromProject", data);
  }


  /**
   * Returns the default namespace of the currently open project
   */
  getDefaultNamespace(): Observable<string> {
    let params: STRequestParams = {};
    return this.httpMgr.doGet(this.serviceName, "getDefaultNamespace", params);
  }

  /**
   * Sets default namespace
   * @param namespace
   */
  setDefaultNamespace(namespace: string) {
    let params = {
      namespace: namespace
    };
    return this.httpMgr.doPost(this.serviceName, "setDefaultNamespace", params).pipe(
      map(stResp => {
        VBContext.getWorkingProject().setDefaultNamespace(namespace);
        return stResp;
      })
    );
  }

  /**
   * Returns the baseURI of the currently open project
   */
  getBaseURI(): Observable<string> {
    let params: STRequestParams = {};
    return this.httpMgr.doGet(this.serviceName, "getBaseURI", params);
  }

  /**
   * Returns the URI obtained expanding the given qname
   */
  expandQName(qname: string): Observable<string> {
    let params: STRequestParams = {
      qname: qname
    };
    return this.httpMgr.doGet(this.serviceName, "expandQName", params);
  }

}