import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ARTLiteral, ARTNode, ARTResource, ARTURIResource, ResourcePosition } from "../models/ARTResources";
import { CustomFormValue } from "../models/CustomForms";
import { RDFFormat } from '../models/RDFFormat';
import { RDFS, SKOS } from '../models/Vocabulary';
import { Deserializer } from "../utils/Deserializer";
import { HttpManager, STRequestParams, STRequestOptions } from "../utils/HttpManager";
import { VBEventHandler } from '../utils/VBEventHandler';

@Injectable()
export class ResourcesServices {

  private serviceName = "Resources";

  constructor(private httpMgr: HttpManager, private eventHandler: VBEventHandler) { }

  /**
   * Updates the value of a triple replacing the old value with the new one
   * @param subject
   * @param property
   * @param value
   * @param newValue
   */
  updateTripleValue(subject: ARTResource, property: ARTURIResource, value: ARTNode, newValue: ARTNode) {
    let params: STRequestParams = {
      subject: subject,
      property: property,
      value: value,
      newValue: newValue
    };
    return this.httpMgr.doPost(this.serviceName, "updateTripleValue", params).pipe(
      map(() => {
        if (property.equals(RDFS.subClassOf)) {
          this.eventHandler.superClassUpdatedEvent.emit({ child: subject as ARTURIResource, oldParent: value as ARTURIResource, newParent: newValue as ARTURIResource });
        } else if (property.equals(RDFS.subPropertyOf)) {
          this.eventHandler.superPropertyUpdatedEvent.emit({ child: subject as ARTURIResource, oldParent: value as ARTURIResource, newParent: newValue as ARTURIResource });
        } else if (property.equals(SKOS.broader)) {
          this.eventHandler.broaderUpdatedEvent.emit({ child: subject as ARTURIResource, oldParent: value as ARTURIResource, newParent: newValue as ARTURIResource });
        }
      })
    );
  }

  /**
   * 
   * @param subject 
   * @param property 
   * @param value 
   * @param newValue 
   */
  updateLexicalization(subject: ARTResource, property: ARTURIResource, value: ARTLiteral, newValue: ARTLiteral) {
    let params: STRequestParams = {
      subject: subject,
      property: property,
      value: value,
      newValue: newValue
    };
    return this.httpMgr.doPost(this.serviceName, "updateLexicalization", params);
  }

  updateFlatLexicalizationProperty(subject: ARTResource, property: ARTURIResource, newProperty: ARTURIResource, value: ARTNode) {
    let params: STRequestParams = {
      subject: subject,
      property: property,
      newProperty: newProperty,
      value: value,
    };
    return this.httpMgr.doPost(this.serviceName, "updateFlatLexicalizationProperty", params);
  }

  /**
   * 
   * @param property 
   * @param value 
   * @param newValue 
   */
  updatePredicateObject(property: ARTURIResource, value: ARTNode, newValue: ARTNode) {
    let params: STRequestParams = {
      property: property,
      value: value,
      newValue: newValue
    };
    return this.httpMgr.doPost(this.serviceName, "updatePredicateObject", params);
  }

  /**
   * Remove a triple
   * @param resource
   * @param property
   * @param value
   */
  removeValue(subject: ARTResource, property: ARTURIResource, value: ARTNode) {
    let params: STRequestParams = {
      subject: subject,
      property: property,
      value: value
    };
    return this.httpMgr.doPost(this.serviceName, "removeValue", params);
  }

  updateTriplePredicate(subject: ARTResource, predicate: ARTURIResource, newPredicate: ARTURIResource, value: ARTNode) {
    let params: STRequestParams = {
      subject: subject,
      predicate: predicate,
      value: value,
      newPredicate: newPredicate,
    };
    return this.httpMgr.doPost(this.serviceName, "updateTriplePredicate", params);
  }

  /**
   * 
   * @param property 
   * @param value 
   */
  removePredicateObject(property: ARTURIResource, value: ARTNode) {
    let params: STRequestParams = {
      property: property,
      value: value,
    };
    return this.httpMgr.doPost(this.serviceName, "removePredicateObject", params);
  }

  /**
   * Set a resource as deprecated
   * @param resource
   */
  setDeprecated(resource: ARTResource) {
    let params: STRequestParams = {
      resource: resource,
    };
    return this.httpMgr.doPost(this.serviceName, "setDeprecated", params).pipe(
      map(stResp => {
        this.eventHandler.resourceDeprecatedEvent.emit(resource);
        return stResp;
      })
    );
  }

  /**
   * Add a value to a given subject-property pair
   * @param subject 
   * @param property 
   * @param value 
   */
  addValue(subject: ARTResource, property: ARTURIResource, value: ARTNode | CustomFormValue) {
    let params: STRequestParams = {
      subject: subject,
      property: property,
      value: value
    };
    return this.httpMgr.doPost(this.serviceName, "addValue", params);
  }

  /**
   * Returns the description (nature show and qname) of the given resource
   * @param resource 
   */
  getResourceDescription<T extends ARTResource>(resource: T, options?: STRequestOptions): Observable<T> {
    let params: STRequestParams = {
      resource: resource
    };
    return this.httpMgr.doGet(this.serviceName, "getResourceDescription", params, options).pipe(
      map(stResp => {
        return Deserializer.createRDFResource(stResp) as T;
      })
    );
  }

  /**
   * 
   * @param resource 
   * @param format 
   * @param options 
   */
  getOutgoingTriples(resource: ARTResource, format: RDFFormat): Observable<string> {
    let params: STRequestParams = {
      resource: resource,
      format: format.name
    };
    return this.httpMgr.doGet(this.serviceName, "getOutgoingTriples", params);
  }

  /**
   * 
   * @param resource 
   * @param triples 
   * @param format 
   */
  updateResourceTriplesDescription(resource: ARTResource, triples: string, format: RDFFormat) {
    let params: STRequestParams = {
      resource: resource,
      triples: triples,
      format: format.name
    };
    let options: STRequestOptions = new STRequestOptions({
      errorHandlers: [
        { className: 'java.lang.IllegalArgumentException', action: 'skip' },
      ]
    });
    return this.httpMgr.doPost(this.serviceName, "updateResourceTriplesDescription", params, options);
  }

  /**
   * Returns the description of a set of resources
   * @param resources 
   */
  getResourcesInfo<T extends ARTResource>(resources: T[], options?: STRequestOptions): Observable<T[]> {
    let resourcesIri: string[] = resources.map(r => r.toNT());
    let params: STRequestParams = {
      resources: JSON.stringify(resourcesIri)
    };
    return this.httpMgr.doPost(this.serviceName, "getResourcesInfo", params, options).pipe(
      map(stResp => {
        return Deserializer.createResourceArray(stResp) as T[];
      })
    );
  }

  /**
   * Returns the position (local/remote/unknown) of the given resource
   * @param resource
   */
  getResourcePosition(resource: ARTURIResource): Observable<ResourcePosition> {
    let params: STRequestParams = {
      resource: resource
    };
    return this.httpMgr.doGet(this.serviceName, "getResourcePosition", params).pipe(
      map(stResp => {
        return ResourcePosition.deserialize(stResp);
      })
    );
  }

  /**
   * Returns the position (local/remote/unknown) of the given resources
   * @param resource
   */
  getResourcesPosition(resources: ARTURIResource[]): Observable<{ [key: string]: ResourcePosition }> {
    let resourcesIri: string[] = resources.map(r => r.toNT());
    let params: STRequestParams = {
      resources: JSON.stringify(resourcesIri)
    };
    return this.httpMgr.doPost(this.serviceName, "getResourcesPosition", params).pipe(
      map(stResp => {
        let map: { [key: string]: ResourcePosition } = {};
        for (let res in stResp) {
          map[res] = ResourcePosition.deserialize(stResp[res].position);
        }
        return map;
      })
    );
  }

  /**
   * Checks if a IRIs/QNames list (manually entered by the user) is correct.
   * Returns an exception if it is not a valid IRI list
   * 
   * @param iriList
   * @return
   */
  validateIRIList(iriList: string): Observable<ARTURIResource[]> {
    let params = {
      iriList: iriList
    };
    let options: STRequestOptions = new STRequestOptions({
      errorHandlers: [{
        className: "*", action: 'skip'
      }]
    });
    return this.httpMgr.doGet(this.serviceName, "validateIRIList", params, options).pipe(
      map(stResp => {
        return Deserializer.createURIArray(stResp);
      })
    );
  }

}