import { Injectable } from '@angular/core';
import {lastValueFrom} from "rxjs";
import {VBProperties} from "../utils/VBProperties";

@Injectable()
export class AppConfigService {
  private config: any;
  constructor(private vbProp: VBProperties) {}

  /**
   * Loads the system configuration by initializing the startup system settings.
   *
   * @return {Promise<boolean>} A promise that resolves to true if the configuration was loaded successfully,
   * or false if an error occurs during the process.
   */
  async loadConfig(): Promise<boolean> {
    try {
      this.config = await lastValueFrom(this.vbProp.initStartupSystemSettings());
      return true;
    } catch (error) {
      console.error('Error loading configuration:', error);
      return false;
    }
  }
}