import { Component } from "@angular/core";

@Component({
  selector: "custom-form-view-page",
  templateUrl: "./custom-form-view-page.component.html",
  host: { class: "pageComponent" },
  standalone: false
})
export class CustomFormViewPageComponent {

  constructor() { }

}