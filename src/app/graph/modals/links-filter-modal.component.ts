import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslationUtils } from "src/app/utils/TranslationUtils";
import { ARTPredicateObjects, ARTURIResource } from "../../models/ARTResources";
import { ResViewSection } from "../../models/ResourceView";

@Component({
    selector: "links-filter-modal",
    templateUrl: "./links-filter-modal.component.html",
    standalone: false
})
export class LinksFilterModalComponent {
    @Input() predObjListMap: { [section: string]: ARTPredicateObjects[] };

    filters: LinkFilter[];
    totalObjCount: number = 0;

    constructor(public activeModal: NgbActiveModal) {}

    ngOnInit() {
        this.filters = [];
        for (let p in this.predObjListMap) {
            let polList: ARTPredicateObjects[] = this.predObjListMap[p];
            let predicates: { res: ARTURIResource, checked: boolean, count: number }[] = [];
            polList.forEach(pol => {
                predicates.push({ res: pol.getPredicate(), checked: true, count: pol.getObjects().length });
                this.totalObjCount += pol.getObjects().length;
            });
            if (predicates.length > 0) {
                this.filters.push({
                    section: { id: p as ResViewSection, labelTranslationKey: TranslationUtils.getResViewSectionTranslationKey(p as ResViewSection) },
                    predicates: predicates 
                });
            }
        }
    }

    checkAll(filter: LinkFilter, check: boolean) {
        filter.predicates.forEach(p => {
            p.checked = check;
        });
    }

    getSectionCount(filter: LinkFilter): number {
        let count = 0;
        filter.predicates.forEach(p => {
            if (p.checked) count += p.count;
        });
        return count;
    }

    getVisibleCount(): number {
        let count = 0;
        this.filters.forEach(f => {
            f.predicates.forEach(p => {
                if (p.checked) count += p.count;
            });
        });
        return count;
    }

    ok() {
        let predicatesToHide: ARTURIResource[] = [];
        this.filters.forEach(f => {
            f.predicates.forEach(p => {
                if (!p.checked) predicatesToHide.push(p.res);
            });
        });
        this.activeModal.close(predicatesToHide);
    }

    cancel() {
        this.activeModal.dismiss();
    }

}

class LinkFilter {
    section: { id: ResViewSection, labelTranslationKey: string };
    predicates: { res: ARTURIResource, checked: boolean, count: number }[];
}