import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalOptions } from 'src/app/modal-dialogs/Modals';
import { ARTBNode, ARTLiteral, ARTNode, ARTPredicateObjects, ARTResource, ARTURIResource } from '../../models/ARTResources';
import { DataGraphContext } from '../../models/Graphs';
import { GraphResultBindings, QueryResultBinding } from '../../models/Sparql';
import { ResourcesServices } from '../../services/resources.service';
import { ResourceUtils } from '../../utils/ResourceUtils';
import { GraphMode } from '../abstract-graph';
import { D3Service } from '../d3/d3.services';
import { DataLink } from '../model/DataLink';
import { DataNode } from '../model/DataNode';
import { ForceDirectedGraph } from '../model/ForceDirectedGraph';
import { GraphUtils } from '../model/GraphUtils';
import { Link } from '../model/Link';
import { ModelNode } from '../model/ModelNode';
import { Node } from '../model/Node';
import { GraphModalComponent } from './graph-modal.component';
import { LinksFilterModalComponent } from './links-filter-modal.component';

@Injectable()
export class GraphModalServices {

  constructor(private modalService: NgbModal, private d3Service: D3Service, private resourceService: ResourcesServices) { }

  openGraphQuertyResult(result: GraphResultBindings[]) {
    //creates nodes and links
    let nodes: DataNode[] = [];
    let links: DataLink[] = [];
    result.forEach(binding => {
      let subj: ARTResource = this.convertBindingToValue(binding.subj) as ARTResource;
      let pred: ARTURIResource = this.convertBindingToValue(binding.pred) as ARTURIResource;
      let obj: ARTNode = this.convertBindingToValue(binding.obj);

      let nodeSubj: DataNode = GraphUtils.getNodeOfValue(nodes, subj);
      let nodeObj: DataNode = GraphUtils.getNodeOfValue(nodes, obj);
      if (nodeSubj == null) {
        nodeSubj = new DataNode(subj);
        nodes.push(nodeSubj);
      }
      if (nodeObj == null) {
        nodeObj = new DataNode(obj);
        nodes.push(nodeObj);
      }
      links.push(new DataLink(nodeSubj, nodeObj, pred));
    });

    //replaces "generic" resources in nodes and links with the annotated resource
    let annotatedRes: ARTURIResource[] = [];
    nodes.forEach(n => {
      if (n.res instanceof ARTURIResource) {
        annotatedRes.push(n.res);
      }
    });
    links.forEach(l => {
      if (!ResourceUtils.containsNode(annotatedRes, l.res)) {
        annotatedRes.push(l.res);
      }
    });
    this.resourceService.getResourcesInfo(annotatedRes).subscribe(
      resources => {
        resources.forEach(r => {
          GraphUtils.getLinksWithPredicate(links, r).forEach(l => {
            l.res = r;
          });
          let n = GraphUtils.getNodeOfValue(nodes, r);
          if (n != null) {
            n.res = r;
          }
        });
        let graph: ForceDirectedGraph<Node, Link<Node>> = this.d3Service.getForceDirectedGraph(nodes, links, false);
        const modalRef: NgbModalRef = this.modalService.open(GraphModalComponent, new ModalOptions('full'));
        modalRef.componentInstance.graph = graph;
        modalRef.componentInstance.mode = GraphMode.dataOriented;
        modalRef.componentInstance.rendering = true;
        modalRef.componentInstance.role = null;
        modalRef.componentInstance.context = DataGraphContext.sparql;
        return modalRef.result;
      }
    );
  }

  private convertBindingToValue(binding: QueryResultBinding): ARTNode {
    if (binding.type == "uri") {
      return new ARTURIResource(binding.value);
    } else if (binding.type == "bnode") {
      return new ARTBNode("_:" + binding.value);
    } else { //literal
      return new ARTLiteral(binding.value, binding.datatype, binding["xml:lang"]);
    }
  }

  openDataGraph(resource: ARTResource, rendering: boolean) {
    let rootNode: DataNode = new DataNode(resource);
    rootNode.root = true; //so it cannot be close in case of loop.
    let graph: ForceDirectedGraph<Node, Link<Node>> = this.d3Service.getForceDirectedGraph([rootNode], []);
    const modalRef: NgbModalRef = this.modalService.open(GraphModalComponent, new ModalOptions('full'));
    modalRef.componentInstance.graph = graph;
    modalRef.componentInstance.mode = GraphMode.dataOriented;
    modalRef.componentInstance.rendering = rendering;
    modalRef.componentInstance.role = resource.getRole();
    return modalRef.result;
  }

  /**
   * Open a model-oriented graph. If a resource is provided, the exploration in incremental, otherwise the graph
   * will show the entire model-graph
   * @param resource
   */
  openModelGraph(resource: ARTResource, rendering: boolean) {
    let nodes: Node[] = [];
    if (resource != null) {
      let rootNode: ModelNode = new ModelNode(resource);
      rootNode.root = true; //so it cannot be close in case of loop.
      nodes.push(rootNode);
    }
    let graph: ForceDirectedGraph<Node, Link<Node>> = this.d3Service.getForceDirectedGraph(nodes, []);
    const modalRef: NgbModalRef = this.modalService.open(GraphModalComponent, new ModalOptions('full'));
    modalRef.componentInstance.graph = graph;
    modalRef.componentInstance.mode = GraphMode.modelOriented;
    modalRef.componentInstance.rendering = rendering;
    return modalRef.result;
  }

  openUmlGraph(rendering: boolean) {
    let graph = new ForceDirectedGraph([], [], rendering);
    const modalRef: NgbModalRef = this.modalService.open(GraphModalComponent, new ModalOptions('full'));
    modalRef.componentInstance.graph = graph;
    modalRef.componentInstance.mode = GraphMode.umlOriented;
    modalRef.componentInstance.rendering = rendering;
    return modalRef.result;
  }

  filterLinks(predObjListMap: { [section: string]: ARTPredicateObjects[] }) {
    const modalRef: NgbModalRef = this.modalService.open(LinksFilterModalComponent, new ModalOptions());
    modalRef.componentInstance.predObjListMap = predObjListMap;
    return modalRef.result;
  }

}