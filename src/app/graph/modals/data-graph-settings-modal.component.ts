import { Component } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ResViewTemplate, SectionFilterPreference } from "../../models/Properties";
import { VBContext } from "../../utils/VBContext";
import { VBProperties } from "../../utils/VBProperties";

@Component({
    selector: "data-graph-settings-modal",
    templateUrl: "./data-graph-settings-modal.component.html",
    standalone: false
})
export class DataGraphSettingsModalComponent {

    graphFilter: SectionFilterPreference;

    hideLiteralNodes: boolean;

    template: ResViewTemplate; //template on which base the graph sections filter

    constructor(public activeModal: NgbActiveModal, private vbProp: VBProperties) {
    }

    ngOnInit() {
        this.graphFilter = VBContext.getWorkingProjectCtx().getProjectPreferences().graphViewSectionFilter;
        this.template = VBContext.getWorkingProjectCtx().getProjectSettings().resourceView.templates;
        this.hideLiteralNodes = VBContext.getWorkingProjectCtx().getProjectPreferences().hideLiteralGraphNodes;
    }

    updateFilter() {
        this.vbProp.setGraphViewSectionFilter(this.graphFilter).subscribe();
    }

    onHideLiteralChange() {
        this.vbProp.setHideLiteralGraphNodes(this.hideLiteralNodes).subscribe();
    }

    ok() {
        this.activeModal.close();
    }

}