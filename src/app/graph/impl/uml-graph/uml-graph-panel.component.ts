import { Component, ViewChild } from "@angular/core";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalOptions } from 'src/app/modal-dialogs/Modals';
import { ARTNode, ARTURIResource } from '../../../models/ARTResources';
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../../modal-dialogs/browsing-modals/browsing-modals.service";
import { AbstractGraphPanel } from "../../abstract-graph-panel";
import { DataGraphSettingsModalComponent } from "../../modals/data-graph-settings-modal.component";
import { UmlLink } from "../../model/UmlLink";
import { UmlNode } from '../../model/UmlNode';
import { UmlGraphComponent } from './uml-graph.component';

@Component({
  selector: 'uml-graph-panel',
  templateUrl: "./uml-graph-panel.component.html",
  standalone: false
})
export class UmlGraphPanelComponent extends AbstractGraphPanel<UmlNode, UmlLink> {

  @ViewChild(UmlGraphComponent) viewChildGraph: UmlGraphComponent;

  resourceToDescribe: ARTNode;
  isHideArrows: boolean = false;
  activeRemove: boolean = false;

  constructor(basicModals: BasicModalServices, browsingModals: BrowsingModalServices, private modalService: NgbModal) {
    super(basicModals, browsingModals);
  }


  openSettings() {
    const modalRef: NgbModalRef = this.modalService.open(DataGraphSettingsModalComponent, new ModalOptions('lg'));
    return modalRef.result;
  }



  addNode() {
    this.browsingModals.browseClassTree({ key: "COMMONS.ACTIONS.ADD_NODE" }).then(
      (cls: ARTURIResource) => {
        this.viewChildGraph.addNode(cls);
      },
      () => { }
    );
  }

  removeNode() {
    this.viewChildGraph.removeNode(this.selectedNode);
  }


  onElementSelected(element: any) {
    this.activeRemove = false;
    this.resourceToDescribe = null;
    if (element != null) {
      if (element instanceof UmlNode) {
        this.selectedNode = element;
        this.activeRemove = true;
        this.resourceToDescribe = element.res;
      } else if (element instanceof UmlLink) {
        this.selectedLink = element;
        this.resourceToDescribe = element.res;
      } else {
        this.resourceToDescribe = element.property;
      }
    }
  }

  updateArrows() {
    this.isHideArrows = !this.isHideArrows;
  }

}