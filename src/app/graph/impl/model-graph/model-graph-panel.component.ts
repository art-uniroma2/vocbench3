import { Component, ViewChild } from "@angular/core";
import { ARTURIResource } from "../../../models/ARTResources";
import { GraphClassAxiomFilter } from "../../../models/Graphs";
import { OWL, RDFS } from "../../../models/Vocabulary";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../../modal-dialogs/browsing-modals/browsing-modals.service";
import { AbstractGraphPanel } from "../../abstract-graph-panel";
import { ModelLink } from "../../model/ModelLink";
import { ModelNode } from "../../model/ModelNode";
import { ModelGraphComponent } from "./model-graph.component";

@Component({
  selector: 'model-graph-panel',
  templateUrl: "./model-graph-panel.component.html",
  standalone: false
})
export class ModelGraphPanelComponent extends AbstractGraphPanel<ModelNode, ModelLink> {

  @ViewChild(ModelGraphComponent) viewChildGraph: ModelGraphComponent;

  axiomFilters: GraphClassAxiomFilter[] = [
    { property: OWL.complementOf, show: false },
    { property: OWL.disjointWith, show: false },
    { property: OWL.equivalentClass, show: false },
    { property: RDFS.subClassOf, show: true }
  ];

  constructor(basicModals: BasicModalServices, browsingModals: BrowsingModalServices) {
    super(basicModals, browsingModals);
  }

  onFilterChange(filter: GraphClassAxiomFilter) {
    this.viewChildGraph.applyFilter(filter);
  }

  addNode() {
    this.browsingModals.browseClassTree({ key: "COMMONS.ACTIONS.ADD_NODE" }).then(
      (cls: ARTURIResource) => {
        this.viewChildGraph.addNode(cls);
      },
      () => { }
    );
  }

  removeNode() {
    this.viewChildGraph.removeNode(this.selectedNode);
  }

}