import { Component, Input, ViewChild } from "@angular/core";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalOptions } from 'src/app/modal-dialogs/Modals';
import { ARTURIResource, RDFResourceRolesEnum } from "../../../models/ARTResources";
import { DataGraphContext } from "../../../models/Graphs";
import { ResourceUtils } from "../../../utils/ResourceUtils";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from "../../../modal-dialogs/browsing-modals/browsing-modals.service";
import { AbstractGraphPanel } from "../../abstract-graph-panel";
import { DataGraphSettingsModalComponent } from "../../modals/data-graph-settings-modal.component";
import { DataLink } from "../../model/DataLink";
import { DataNode } from "../../model/DataNode";
import { DataGraphComponent } from "./data-graph.component";


@Component({
  selector: 'data-graph-panel',
  templateUrl: "./data-graph-panel.component.html",
  standalone: false
})
export class DataGraphPanelComponent extends AbstractGraphPanel<DataNode, DataLink> {

  @Input() role: RDFResourceRolesEnum;
  @Input() context: DataGraphContext;
  @ViewChild(DataGraphComponent) viewChildGraph: DataGraphComponent;

  activeRemove: boolean = false;

  constructor(basicModals: BasicModalServices, browsingModals: BrowsingModalServices, private modalService: NgbModal) {
    super(basicModals, browsingModals);
  }

  openSettings() {
    const modalRef: NgbModalRef = this.modalService.open(DataGraphSettingsModalComponent, new ModalOptions('lg'));
    return modalRef.result;
  }

  addNode() {
    let browsePromise: Promise<ARTURIResource>;
    if (ResourceUtils.roleSubsumes(RDFResourceRolesEnum.property, this.role)) {
      browsePromise = this.browsingModals.browsePropertyTree({ key: "COMMONS.ACTIONS.ADD_NODE" });
    } else if (this.role == RDFResourceRolesEnum.cls) {
      browsePromise = this.browsingModals.browseClassTree({ key: "COMMONS.ACTIONS.ADD_NODE" });
    } else if (this.role == RDFResourceRolesEnum.concept) {
      browsePromise = this.browsingModals.browseConceptTree({ key: "COMMONS.ACTIONS.ADD_NODE" });
    } else if (this.role == RDFResourceRolesEnum.conceptScheme) {
      browsePromise = this.browsingModals.browseSchemeList({ key: "COMMONS.ACTIONS.ADD_NODE" });
    } else if (this.role == RDFResourceRolesEnum.dataRange) {
      browsePromise = this.browsingModals.browseDatatypeList({ key: "COMMONS.ACTIONS.ADD_NODE" });
    } else if (this.role == RDFResourceRolesEnum.individual) {
      browsePromise = this.browsingModals.browseClassIndividualTree({ key: "COMMONS.ACTIONS.ADD_NODE" });
    } else if (this.role == RDFResourceRolesEnum.limeLexicon) {
      browsePromise = this.browsingModals.browseLexiconList({ key: "COMMONS.ACTIONS.ADD_NODE" });
    } else if (this.role == RDFResourceRolesEnum.ontolexLexicalEntry) {
      browsePromise = this.browsingModals.browseLexicalEntryList({ key: "COMMONS.ACTIONS.ADD_NODE" });
    } else if (ResourceUtils.roleSubsumes(RDFResourceRolesEnum.skosCollection, this.role)) {
      browsePromise = this.browsingModals.browseCollectionTree({ key: "COMMONS.ACTIONS.ADD_NODE" });
    }
    browsePromise.then(
      res => {
        this.viewChildGraph.addNode(res);
      },
      () => { }
    );
  }

  removeNode() {
    this.viewChildGraph.removeNode(this.selectedNode);
  }

  isExpandEnabled(): boolean {
    if (this.selectedNode) {
      return (
        this.selectedNode.res instanceof ARTURIResource &&
        (
          this.selectedNode.res.getRole() == RDFResourceRolesEnum.cls ||
          this.selectedNode.res.getRole() == RDFResourceRolesEnum.concept ||
          this.selectedNode.res.getRole() == RDFResourceRolesEnum.skosCollection ||
          ResourceUtils.roleSubsumes(RDFResourceRolesEnum.property, this.selectedNode.res.getRole())
        )
      );
    } else {
      return false;
    }
  }

  expandSubResources() {
    this.viewChildGraph.expandSub();
  }
  expandSuperResources() {
    this.viewChildGraph.expandSuper();
  }

}