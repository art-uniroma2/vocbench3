import { ChangeDetectorRef, Component, Input } from '@angular/core';
import { GraphMode } from '../../abstract-graph';
import { AbstractGraphNode } from '../../abstract-graph-node';
import { DataNode } from '../../model/DataNode';

@Component({
  selector: '[dataNode]',
  templateUrl: "./data-node.component.html",
  styleUrls: ['../../graph.css'],
  standalone: false
})
export class DataNodeComponent extends AbstractGraphNode<DataNode> {

  @Input('dataNode') node: DataNode;

  graphMode = GraphMode.dataOriented;

  stripePercentage: number; //percentage of the rect height to dedicate to the top stripe
  stripeHeight: number; //height (in px) of the top stripe

  constructor(protected changeDetectorRef: ChangeDetectorRef) {
    super(changeDetectorRef);
  }

  ngOnInit() {
    this.initNode();
    this.initMeasures();
  }

  private initMeasures() {
    let fontSize: number = 11;
    let padding: number = 2;
    this.stripeHeight = fontSize + 2 * padding;
    this.stripePercentage = Math.ceil(this.stripeHeight * 100 / this.measures.height);
  }

}