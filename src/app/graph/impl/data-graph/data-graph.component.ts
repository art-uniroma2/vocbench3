import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, Output } from "@angular/core";
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { ARTNode, ARTPredicateObjects, ARTResource, ARTURIResource, ResAttribute } from "../../../models/ARTResources";
import { GraphModelRecord } from "../../../models/Graphs";
import { ResViewSection, ResViewUtils } from "../../../models/ResourceView";
import { GraphServices } from "../../../services/graph.service";
import { ResourceViewServices } from "../../../services/resource-view.service";
import { Deserializer } from "../../../utils/Deserializer";
import { ResourceUtils } from "../../../utils/ResourceUtils";
import { UIUtils } from "../../../utils/UIUtils";
import { VBContext } from "../../../utils/VBContext";
import { BasicModalServices } from "../../../modal-dialogs/basic-modals/basic-modals.service";
import { AbstractGraph, GraphMode } from "../../abstract-graph";
import { D3Service } from "../../d3/d3.services";
import { GraphModalServices } from "../../modals/graph-modal.service";
import { DataLink } from "../../model/DataLink";
import { DataNode } from "../../model/DataNode";

@Component({
  selector: 'data-graph',
  templateUrl: "./data-graph.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['../../graph.css'],
  standalone: false
})
export class DataGraphComponent extends AbstractGraph<DataNode, DataLink> {

  @Output() elementSelected = new EventEmitter<DataNode | DataLink>();

  protected mode = GraphMode.dataOriented;

  private linkLimit: number = 50;

  private rvSections: ResViewSection[] = ResViewUtils.orderedResourceViewSections;

  constructor(protected d3Service: D3Service, protected elementRef: ElementRef, protected ref: ChangeDetectorRef, protected basicModals: BasicModalServices,
    private resViewService: ResourceViewServices, private graphService: GraphServices, private graphModals: GraphModalServices) {
    super(d3Service, elementRef, ref, basicModals);
  }

  ngOnInit() {
    //if the graph has only one node, expand it (so avoid to expand the first nodes in case the graph is opened from a SPARQL graph query result)
    if (this.graph.getNodes().length == 1) {
      this.expandNode(this.graph.getNodes()[0], true);
    }
  }

  addNode(res: ARTURIResource) {
    if (this.graph.getNode(res)) {
      this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "MESSAGES.ALREADY_EXISTING_GRAPH_NODE_FOR_RESOURCE", params: { resource: res.getShow() } }, ModalType.warning);
      return;
    }

    let node: DataNode = new DataNode(res);
    node.root = true;
    this.initNodePosition(node);
    this.graph.addNode(node);
    this.graph.update();
    this.expandNode(node, true);
  }

  removeNode(node: DataNode) {
    let linksToRemove: DataLink[] = this.graph.getLinksFrom(node);
    this.graph.getLinksTo(node).forEach(l => {
      //add only if not already in (if there are loops, the same link will be returned by getLinksFrom and getLinksTo)
      if (linksToRemove.indexOf(l) == -1) {
        linksToRemove.push(l);
      }
    });
    linksToRemove.forEach(l => {
      this.graph.removeLink(l);

    });
    this.graph.removeNode(node);
    this.selectedNode = null;
    this.elementSelected.emit(this.selectedNode);
    this.graph.update();
  }

  expandSub() {
    UIUtils.startLoadingDiv(this.blockingDivElement.nativeElement);
    this.graphService.expandSubResources(this.selectedNode.res as ARTURIResource).subscribe(
      (graphModel: GraphModelRecord[]) => {
        UIUtils.stopLoadingDiv(this.blockingDivElement.nativeElement);
        let links: DataLink[] = this.convertModelToLinks(graphModel);
        this.appendLinks(this.selectedNode, links);
      }
    );
  }

  expandSuper() {
    UIUtils.startLoadingDiv(this.blockingDivElement.nativeElement);
    this.graphService.expandSuperResources(this.selectedNode.res as ARTURIResource).subscribe(
      (graphModel: GraphModelRecord[]) => {
        UIUtils.stopLoadingDiv(this.blockingDivElement.nativeElement);
        let links: DataLink[] = this.convertModelToLinks(graphModel);
        this.appendLinks(this.selectedNode, links);
      }
    );
  }

  protected expandNode(node: DataNode, selectOnComplete?: boolean) {
    if (!node.res.isResource()) {
      return;
    }
    UIUtils.startLoadingDiv(this.blockingDivElement.nativeElement);
    this.resViewService.getResourceView(node.res as ARTResource).subscribe(
      rv => {
        UIUtils.stopLoadingDiv(this.blockingDivElement.nativeElement);
        let filteredSections: ResViewSection[] = VBContext.getWorkingProjectCtx().getProjectPreferences().graphViewSectionFilter[(node.res as ARTResource).getRole()];
        //create the predicate-object lists for each section (skip the filtered section)
        let predObjListMap: { [section: string]: ARTPredicateObjects[] } = {};
        this.rvSections.forEach(section => {
          if (filteredSections != null && filteredSections.indexOf(section) != -1) {
            return; //if the current section is among the sections filtered for the resource role => skip the parsing
          }
          //parse section
          let sectionJson = rv[section];
          if (section == ResViewSection.facets && sectionJson != null) { //dedicated handler for facets section
            sectionJson = sectionJson.inverseOf;
          }
          if (sectionJson != null) {
            let poList: ARTPredicateObjects[] = Deserializer.createPredicateObjectsList(sectionJson);
            predObjListMap[section] = poList;
          }
        });
        //filter the objects according the filter-value languages
        for (let section in predObjListMap) {
          this.filterValueLanguageFromPrefObjList(predObjListMap[section]);
        }
        //count number of objects
        let linkCount: number = 0;
        for (let section in predObjListMap) {
          predObjListMap[section].forEach(pol => { //for each pol of a section
            linkCount += pol.getObjects().length; //a link for each object (subject ---predicate---> object)
          });
        }
        //check if the relations that it is going to add are too much
        if (linkCount > this.linkLimit) {
          this.graphModals.filterLinks(predObjListMap).then(
            (predicatesToHide: ARTURIResource[]) => {
              let links: DataLink[] = this.convertPredObjListMapToLinks(node, predObjListMap, predicatesToHide);
              this.appendLinks(node, links);
              if (selectOnComplete) {
                this.onNodeClicked(node);
              }
            },
            () => { }
          );
        } else {
          let links: DataLink[] = this.convertPredObjListMapToLinks(node, predObjListMap, []);
          this.appendLinks(node, links);
          if (selectOnComplete) {
            this.onNodeClicked(node);
          }
        }
      }
    );
    node.open = true;
  }

  protected closeNode(node: DataNode) {
    this.deleteSubtree(node);

    let sourceDataNode = node;
    if (sourceDataNode.isPending()) {
      this.graph.removeNode(sourceDataNode); //remove the node from the graph
    }

    this.graph.update();
    node.open = false;
  }


  private appendLinks(expandedNode: DataNode, links: DataLink[]) {
    links.forEach(l => {

      if (this.graph.getLink(l.source.res, l.res, l.target.res) != null) {
        return;
      }

      let sourceNode = this.retrieveNode(l.source, expandedNode);
      l.source = sourceNode;
      let targetNode = this.retrieveNode(l.target, expandedNode);
      l.target = targetNode;

      if (expandedNode == sourceNode) {
        targetNode.openBy.push(expandedNode);
      }
      this.graph.addLink(l);
    });

    this.graph.update();
  }


  /**
   * Delete the subtree rooted on the given node. Useful when closing a node.
   * @param node 
   */
  private deleteSubtree(node: DataNode) {
    let recursivelyClosingNodes: DataNode[] = []; //nodes target in the removed links that needs to be closed in turn
    let linksFromNode: DataLink[] = this.graph.getLinksFrom(node);
    if (linksFromNode.length > 0) {
      //removes links with the node as source
      linksFromNode.forEach(l => {
        //remove the source node from the openBy nodes of the target
        let targetDataNode = l.target;
        targetDataNode.removeOpenByNode(l.source);
        //remove the link
        this.graph.removeLink(l);
        //if now the openBy list of the target is empty, it means that the node would be detached from the graph
        if (targetDataNode.isPending()) {
          this.graph.removeNode(targetDataNode); //remove the node from the graph
          recursivelyClosingNodes.push(targetDataNode); //add to the list of nodes to recursively close
        }
      });
      //call recursively the deletion of the subtree for the deleted node)
      recursivelyClosingNodes.forEach(n => {
        this.deleteSubtree(n);
      });
    }
  }

  /* ================== UTILS ================== */

  /**
   * Converts the map between sections and predicate-objects lists to a list of links.
   * Filters the convertion according the list of predicates to hide
   * @param sourceNode source node of the links
   * @param predObjListMap 
   * @param predicatesToHide 
   */
  private convertPredObjListMapToLinks(sourceNode: DataNode, predObjListMap: { [section: string]: ARTPredicateObjects[] }, predicatesToHide: ARTURIResource[]): DataLink[] {
    let hideLiteralNodes: boolean = VBContext.getWorkingProjectCtx().getProjectPreferences().hideLiteralGraphNodes;
    let links: DataLink[] = [];
    for (let section in predObjListMap) {
      predObjListMap[section].forEach(pol => { //for each pol of a section
        let pred: ARTURIResource = pol.getPredicate();
        if (!ResourceUtils.containsNode(predicatesToHide, pred)) {
          let objs: ARTNode[] = pol.getObjects();
          objs.forEach(o => { //for each object/value
            if (hideLiteralNodes && o.isLiteral()) {
              return; //if the literal should be hidden and the object is literal, skip the link
            }
            links.push(new DataLink(sourceNode, new DataNode(o), pred));
          });
        }
      });
    }
    return links;
  }

  /**
   * Converts the GraphModelRecord(s) (returned by getGraphModel() and expandGraphModelNode() services) into a list of nodes and links
   */
  private convertModelToLinks(graphModel: GraphModelRecord[]): DataLink[] {
    let links: DataLink[] = [];
    //set the nodes and the links according the model
    graphModel.forEach(record => {
      let nodeSource: DataNode = new DataNode(record.source);
      let nodeTarget: DataNode = new DataNode(record.target);
      links.push(new DataLink(nodeSource, nodeTarget, record.link, record.classAxiom));
    });
    return links;
  }

  /**
   * Filters the objects list according the value-filter languages
   * @param predObjList 
   */
  private filterValueLanguageFromPrefObjList(predObjList: ARTPredicateObjects[]) {
    let valueFilterLangEnabled = VBContext.getWorkingProjectCtx().getProjectPreferences().filterValueLang.enabled;
    if (valueFilterLangEnabled) {
      let valueFilterLanguages = VBContext.getWorkingProjectCtx().getProjectPreferences().filterValueLang.languages;
      if (valueFilterLanguages != null && valueFilterLanguages.length != 0) { //if no languages are set, it means all languages are used
        for (let i = 0; i < predObjList.length; i++) {
          let objList: ARTNode[] = predObjList[i].getObjects();
          for (let j = 0; j < objList.length; j++) {
            let lang = objList[j].getAdditionalProperty(ResAttribute.LANG);
            //remove the object if it has a language not in the languages list of the filter
            if (lang != null && valueFilterLanguages.indexOf(lang) == -1) {
              objList.splice(j, 1);
              j--;
            }
          }
          //after filtering the objects list, if the predicate has no more objects, remove it from predObjList
          if (objList.length == 0) {
            predObjList.splice(i, 1);
            i--;
          }
        }
      }
    }
  }

  /**
   * Look for a node to retrieve, if it exists returns it, otherwise creates and returns it
   * @param node 
   * @param expandedNode used to initialize the position of the new nodes placing them near the expanded one
   */
  private retrieveNode(node: DataNode, expandedNode: DataNode): DataNode {
    let graphNode: DataNode;
    graphNode = this.graph.getNode(node.res);
    if (graphNode == null) { //node for the given resource not yet created => create it and add to the graph
      graphNode = node;
      this.initNodePosition(graphNode, expandedNode);
      this.graph.addNode(graphNode);
    }
    return graphNode;
  }


  /* ================== EVENT HANDLER ================== */

  protected onNodeDblClicked(node: DataNode) {
    if (!this.graph.dynamic) return; //if graph is not dynamic, do nothing
    if (node.open) {
      this.closeNode(node);
    } else {
      this.expandNode(node);
    }
  }

}