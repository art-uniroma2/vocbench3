import { ARTURIResource } from 'src/app/models/ARTResources';
import { Link } from './Link';
import { ModelNode } from './ModelNode';

export class ModelLink extends Link<ModelNode> {

  res: ARTURIResource;

}