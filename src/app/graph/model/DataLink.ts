import { ARTURIResource } from 'src/app/models/ARTResources';
import { DataNode } from './DataNode';
import { Link } from './Link';

export class DataLink extends Link<DataNode> {

  res: ARTURIResource;

}