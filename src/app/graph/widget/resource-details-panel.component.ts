import { Component, Input } from '@angular/core';
import { ARTNode, ARTResource } from '../../models/ARTResources';
import { SharedModalServices } from '../../modal-dialogs/shared-modals/shared-modals.service';

@Component({
  selector: 'resource-details-panel',
  templateUrl: './resource-details-panel.component.html',
  host: { class: "vbox" },
  standalone: false
})
export class ResourceDetailsPanelComponent {

  @Input() resource: ARTNode;

  constructor(private sharedModals: SharedModalServices) { }

  showResourceView() {
    this.sharedModals.openResourceView(this.resource as ARTResource, false);
  }

}