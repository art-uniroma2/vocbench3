import { Component, ViewChild } from "@angular/core";
import { forkJoin } from "rxjs";
import { map } from "rxjs/operators";
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { ARTURIResource } from "../../models/ARTResources";
import { PrefixMapping } from "../../models/Metadata";
import { ResourceViewEditorComponent } from "../../resource-view/resource-view-editor/resource-view-editor.component";
import { MetadataServices } from "../../services/metadata.service";
import { RefactorServices } from "../../services/refactor.service";
import { AuthorizationEvaluator } from "../../utils/AuthorizationEvaluator";
import { ResourceUtils } from "../../utils/ResourceUtils";
import { UIUtils } from "../../utils/UIUtils";
import { VBActionsEnum } from "../../utils/VBActions";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";
import { SharedModalServices } from "../../modal-dialogs/shared-modals/shared-modals.service";
import { ImportTreePanelComponent } from './import-tree/import-tree-panel.component';
import { DatatypeValidator } from "src/app/utils/DatatypeValidator";

@Component({
  selector: "namespaces-imports",
  templateUrl: "./namespaces-and-imports.component.html",
  host: { class: "pageComponent" },
  standalone: false
})
export class NamespacesAndImportsComponent {

  @ViewChild(ResourceViewEditorComponent) viewChildResView: ResourceViewEditorComponent;
  @ViewChild(ImportTreePanelComponent) viewChildImportTree: ImportTreePanelComponent;

  // baseURI namespace section
  private pristineBaseURI: string;
  private pristineNamespace: string;
  baseURI: string;
  namespace: string;
  bind: boolean = true; //keep bound the baseURI and the namespace
  nsBaseURISubmitted: boolean = false; //tells if changes on namespace or baseURI have been submitted

  // namespace prefix section
  nsPrefMappingList: PrefixMapping[];
  selectedMapping: any; //the currently selected mapping {namespace: string, prefix: string}

  //Ontology res view
  baseUriRes: ARTURIResource;

  constructor(
    private metadataService: MetadataServices,
    private refactorService: RefactorServices,
    private dtValidator: DatatypeValidator,
    private basicModals: BasicModalServices,
    private sharedModals: SharedModalServices
  ) { }

  ngOnInit() {
    this.refreshBaseURI();
    this.refreshDefaultNamespace();
    this.refreshNSPrefixMappings();
  }

  //inits or refreshes baseURI
  private refreshBaseURI() {
    this.metadataService.getBaseURI().subscribe(
      baseURI => {
        this.pristineBaseURI = baseURI;
        this.baseURI = baseURI;
        this.baseUriRes = new ARTURIResource(this.baseURI);
      }
    );
  }

  //inits or refreshes default namespace
  private refreshDefaultNamespace() {
    this.metadataService.getDefaultNamespace().subscribe(
      ns => {
        this.pristineNamespace = ns;
        this.namespace = ns;
      }
    );
  }

  //inits or refreshes imports
  private refreshImports() {
    this.viewChildImportTree.initTree();
  }

  //inits or refreshes namespace prefix mappings
  private refreshNSPrefixMappings() {
    this.metadataService.getNamespaceMappings().subscribe(
      mappings => {
        this.nsPrefMappingList = mappings;
        this.selectedMapping = null;
      }
    );
  }

  //======= NAMESPACE AND BASEURI MANAGEMENT =======

  /**
   * Bind/unbind namespace baseURI.
   * If they're bound, changing one of them will change the other
   */
  bindNamespaceBaseURI() {
    this.bind = !this.bind;
  }

  /**
   * When baseURI changes updates the namespace if they are bound 
   */
  onBaseURIChanged(newBaseURI: string) {
    this.baseURI = newBaseURI;
    if (this.bind) {
      this.namespace = this.baseURI;
      if (!(this.baseURI.endsWith("/") || this.baseURI.endsWith("#"))) {
        this.namespace += "#";
      }
    }
  }

  /**
   * When namespace changes updates the baseURI if they are bound
   */
  onNamespaceChanged(newNamespace: string) {
    this.namespace = newNamespace;
    if (this.bind) {
      if (this.namespace.endsWith("#")) {
        this.baseURI = this.namespace.slice(0, -1);
      } else {
        this.baseURI = this.namespace;
      }
    }
  }

  /**
   * Tells if namespace or baseURI have been changed
   */
  areNamespaceBaseURIChanged() {
    return (this.baseURI != this.pristineBaseURI || this.namespace != this.pristineNamespace);
  }

  /**
   * Tells if baseURI is valid. BaseURI is valid if startsWith http://
   */
  isBaseURIValid() {
    return (this.baseURI && ResourceUtils.testIRI(this.baseURI));
  }

  /**
   * Tells if namespace is valid. Namespace is valid if starts with http:// and ends with #
   */
  isNamespaceValid() {
    return (this.namespace && ResourceUtils.testIRI(this.namespace) && (this.namespace.endsWith("#") || this.namespace.endsWith("/")));
  }

  /**
   * Updates the namespace and baseURI
   */
  applyNamespaceBaseURI() {
    this.nsBaseURISubmitted = true;
    if (this.isBaseURIValid() && this.isNamespaceValid()) {
      let updateNsFn = this.metadataService.setDefaultNamespace(this.namespace).pipe(
        map(() => {
          this.refreshDefaultNamespace();
        })
      );
      let updateBaseUriFn = this.refactorService.replaceBaseURI(this.baseURI).pipe(
        map(() => {
          this.refreshBaseURI();
        })
      );

      if (this.baseURI != this.pristineBaseURI && this.namespace != this.pristineNamespace) { //changed both baseURI and namespace
        this.basicModals.confirm({ key: "DATA_MANAGEMENT.REFACTOR.REFACTOR" }, { key: "MESSAGES.SAVE_BASEURI_NS_CHANGE_CONFIRM" }, ModalType.warning).then(
          () => {
            UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
            forkJoin([updateNsFn, updateBaseUriFn]).subscribe(
              () => {
                UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
                this.basicModals.alert({ key: "DATA_MANAGEMENT.REFACTOR.REFACTOR" }, { key: "MESSAGES.BASEURI_AND_NAMESPACE_UPDATED" });
                this.nsBaseURISubmitted = true;
              }
            );
          },
          () => {
            //restore both
            this.namespace = this.pristineNamespace;
            this.baseURI = this.pristineBaseURI;
            this.nsBaseURISubmitted = true;
          }
        );
      } else if (this.baseURI != this.pristineBaseURI) { //changed only baseURI
        this.basicModals.confirm({ key: "DATA_MANAGEMENT.REFACTOR.REFACTOR" }, { key: "MESSAGES.SAVE_BASEURI_CHANGE_CONFIRM" }, ModalType.warning).then(
          () => {
            UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
            updateBaseUriFn.subscribe(
              () => {
                UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
                this.basicModals.alert({ key: "DATA_MANAGEMENT.REFACTOR.REFACTOR" }, { key: "MESSAGES.BASEURI_UPDATED" });
                this.nsBaseURISubmitted = true;
              }
            );
          },
          () => {
            //restore baseURI
            this.baseURI = this.pristineBaseURI;
            this.nsBaseURISubmitted = true;
          }
        );
      } else if (this.namespace != this.pristineNamespace) { //changed only namespace
        this.basicModals.confirm({ key: "COMMONS.ACTIONS.SAVE_CHANGES" }, { key: "MESSAGES.SAVE_NS_CHANGE_CONFIRM" }, ModalType.warning).then(
          () => {
            updateNsFn.subscribe(
              () => {
                this.basicModals.alert({ key: "DATA_MANAGEMENT.REFACTOR.REFACTOR" }, { key: "MESSAGES.NAMESPACE_UPDATED" });
                this.nsBaseURISubmitted = true;
              }
            );
          },
          () => {
            //restore namespace
            this.namespace = this.pristineNamespace;
            this.nsBaseURISubmitted = true;
          }
        );
      }
    } else {
      this.basicModals.alert({ key: "STATUS.ERROR" }, { key: "MESSAGES.INVALID_NAMESPACE_AND_BASEURI" }, ModalType.warning);
    }
  }

  //======= PREFIX NAMESPACE MAPPINGS MANAGEMENT =======

  /**
   * Set the given prefix namespace mapping as selected
   */
  selectMapping(mapping: any) {
    if (this.selectedMapping == mapping) {
      this.selectedMapping = null;
    } else {
      this.selectedMapping = mapping;
    }
  }

  /**
   * Adds a new prefix namespace mapping
   */
  addMapping() {
    this.sharedModals.prefixNamespace({ key: "COMMONS.ACTIONS.ADD_PREFIX_NAMESPACE_MAPPING" }, null, null, false, true).then(
      () => {
        this.refreshNSPrefixMappings();
      },
      () => { }
    );
  }

  /**
   * Removes the selected mapping
   */
  removeMapping() {
    this.metadataService.removeNSPrefixMapping(this.selectedMapping.namespace).subscribe(
      () => {
        this.refreshNSPrefixMappings();
      }
    );
  }

  /**
   * Changes the prefix of a prefix namespace mapping
   */
  changeMapping() {
    this.sharedModals.prefixNamespace({ key: "COMMONS.ACTIONS.EDIT_MAPPING" }, this.selectedMapping.prefix, this.selectedMapping.namespace, true, true).then(
      () => {
        this.refreshNSPrefixMappings();
      },
      () => { }
    );
  }

  //======= IMPORTS MANAGEMENT =======

  onImportsUpdate() {
    this.refreshNSPrefixMappings();
    this.refreshBaseUriResView();
    //import might have added/removed DT restrictions, so update them
    this.dtValidator.initDatatypeRestrictions().subscribe();
  }


  //======= BASE URI RES VIEW =======

  private refreshBaseUriResView() {
    this.viewChildResView.buildResourceView(this.baseUriRes);
  }

  private ontoInitialized: boolean = false;
  onOntologyUpdated() {
    //when the ResView of the ontology is initialized, emits an "update" event.
    //This check is in order to prevent the refresh of imports and prefix-ns mappings (that is already done in ngOnInit) when ontology ResView in initialized
    if (!this.ontoInitialized) {
      this.ontoInitialized = true;
      return;
    }
    this.refreshImports();
    this.refreshNSPrefixMappings();
  }

  //Authorizations

  isAddNsPrefixMappingAuthorized(): boolean {
    return AuthorizationEvaluator.isAuthorized(VBActionsEnum.metadataSetNsPrefixMapping);
  }
  isRemoveNsPrefixMappingAuthorized(): boolean {
    return AuthorizationEvaluator.isAuthorized(VBActionsEnum.metadataRemoveNsPrefixMapping);
  }
  isChangeNsPrefixMappingAuthorized(): boolean {
    return AuthorizationEvaluator.isAuthorized(VBActionsEnum.metadataChangeNsPrefixMapping);
  }
  isBaseuriNsEditAuthorized(): boolean {
    return (
      AuthorizationEvaluator.isAuthorized(VBActionsEnum.metadataSetDefaultNs) &&
      AuthorizationEvaluator.isAuthorized(VBActionsEnum.refactorReplaceBaseUri));
  }

}