import { Component, Input } from "@angular/core";
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { DatasetCatalogModalComponent, DatasetCatalogModalReturnData } from "../../config/dataManagement/datasetCatalog/datasetCatalogModal";
import { TransitiveImportMethodAllowance, TransitiveImportUtils } from "../../models/Metadata";
import { RDFFormat } from "../../models/RDFFormat";
import { InputOutputServices } from "../../services/input-output.service";
import { OntoManagerServices } from "../../services/onto-manager.service";
import { BasicModalServices } from "../../modal-dialogs/basic-modals/basic-modals.service";

@Component({
  selector: "import-datset-catalog-modal",
  templateUrl: "./import-from-dataset-catalog-modal.component.html",
  standalone: false
})
export class ImportFromDatasetCatalogModalComponent {
  @Input() title: string;

  preloadCatalog: string; //id-title of the datasetCatalog

  ontologyIRI: string;
  dataDump: string;
  useDataDump: boolean = false;

  lockFormat: boolean = true;
  formats: RDFFormat[];
  rdfFormat: RDFFormat;

  importAllowances: { allowance: TransitiveImportMethodAllowance, showTranslationKey: string }[] = TransitiveImportUtils.importAllowancesList;
  selectedImportAllowance: TransitiveImportMethodAllowance = this.importAllowances[1].allowance;

  constructor(public activeModal: NgbActiveModal, public basicModals: BasicModalServices, private modalService: NgbModal,
    public ontoMgrService: OntoManagerServices, public inOutService: InputOutputServices) {
  }

  openDatasetCatalog() {
    const modalRef: NgbModalRef = this.modalService.open(DatasetCatalogModalComponent, new ModalOptions('full'));
    modalRef.result.then(
      (data: DatasetCatalogModalReturnData) => {
        this.preloadCatalog = data.dataset.id + " - " + data.dataset.getPreferredTitle().getValue() + " @" + data.dataset.getPreferredTitle().getLang();
        this.ontologyIRI = (data.dataset.ontologyIRI != null) ? data.dataset.ontologyIRI.getURI() : null;
        this.dataDump = data.dataDump.accessURL;
        if (this.dataDump != null) {
          let ext: string = this.dataDump.substring(this.dataDump.lastIndexOf(".") + 1);
          this.formats.forEach(f => {
            if (f.defaultFileExtension == ext) {
              this.rdfFormat = f;
            }
          });
        }
      },
      () => { }
    );
  }

  ngOnInit() {
    this.inOutService.getInputRDFFormats().subscribe(
      formats => {
        this.formats = formats;
        for (const f of this.formats) {
          if (f.name == "RDF/XML") { //select RDF/XML as default
            this.rdfFormat = f;
            break;
          }
        }
      }
    );
  }

  ok() {
    if (this.ontologyIRI == null) {
      this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "MESSAGES.CANNOT_IMPORT_DATASET_MISSING_ONTO_IRI" }, ModalType.warning);
      return;
    }


    let dataDumpPar = (this.useDataDump) ? this.dataDump : null;
    let formatPar: RDFFormat = dataDumpPar != null ? this.rdfFormat : null;

    let returnData: ImportFromDatasetCatalogModalReturnData = {
      ontologyIRI: this.ontologyIRI,
      dataDump: dataDumpPar,
      rdfFormat: formatPar,
      transitiveImportAllowance: this.selectedImportAllowance
    };
    this.activeModal.close(returnData);
  }

  cancel() {
    this.activeModal.dismiss();
  }

}

export class ImportFromDatasetCatalogModalReturnData {
  ontologyIRI: string;
  dataDump: string;
  rdfFormat: RDFFormat;
  transitiveImportAllowance: TransitiveImportMethodAllowance;
}