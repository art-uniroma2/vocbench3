import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Project } from 'src/app/models/Project';
import { ProjectServices } from 'src/app/services/projects.service';
import { VBContext } from 'src/app/utils/VBContext';
import { ImportAction, ImportSource, OntologyMirror, TransitiveImportMethodAllowance, TransitiveImportUtils } from "../../models/Metadata";
import { RDFFormat } from "../../models/RDFFormat";
import { ExportServices } from "../../services/export.service";
import { OntoManagerServices } from "../../services/onto-manager.service";

@Component({
  selector: "import-ontology-modal",
  templateUrl: "./import-ontology-modal.component.html",
  standalone: false
})
export class ImportOntologyModalComponent {
  @Input() title: string;
  @Input() importSource: ImportSource;
  @Input() importAction: ImportAction;
  @Input() baseUriInput?: string; //baseURI of the imported ontology (provided only when repairing)
  @Input() projectNameInput?: string; //project name to lock (in case source is from project)


  baseURIOptional: boolean;
  baseURICheck: boolean; //used for type "fromLocalFile" (its value is computed in the method ngOnInit())
  baseURI: string; //used for type "fromWeb", "fromLocalFile"

  private localFile: File; //used for type "fromLocalFile"

  mirrorFileCheck: boolean; //used for type "fromWeb", "fromLocalFile" (its value is computed in the method ngOnInit())
  mirrorFile: string; //used for type "fromWeb", "fromLocalFile"

  altURLCheck: boolean = false; //used for type "fromWeb"
  altURL: string; //used for type "fromWeb"

  forceFormatCheck: boolean = false; //used for type "fromWeb"
  formats: RDFFormat[];
  rdfFormat: RDFFormat; //used for type "fromWeb"

  mirrorList: OntologyMirror[]; //used for type "fromOntologyMirror"
  selectedMirror: OntologyMirror; //used for type "fromOntologyMirror"
  mirrorListDisabled: boolean;
  mirrorListDisabledMsgKey: string;

  projects: Project[];
  selectedProject: Project;
  projectListDisabledMsgKey: string;

  importAllowances: { allowance: TransitiveImportMethodAllowance, showTranslationKey: string }[] = TransitiveImportUtils.importAllowancesList;
  selectedImportAllowance: TransitiveImportMethodAllowance = this.importAllowances[1].allowance;

  constructor(private activeModal: NgbActiveModal, private ontoMgrService: OntoManagerServices, private exportService: ExportServices, private projectService: ProjectServices) {
  }

  ngOnInit() {
    this.initAsyncStuff().subscribe(
      () => {
        if (this.importAction == ImportAction.repair || this.importAction == ImportAction.update) {
          //base uri in input provided only when repairing or updating an import
          this.baseURI = this.baseUriInput;
          // in case it is a repair from Mirror, force the selection of the mirror with the same input URI
          if (this.importSource == ImportSource.fromOntologyMirror) {
            let mirrorToSelect = this.mirrorList.find(m => m.baseURI == this.baseURI);
            if (mirrorToSelect == null) {
              this.mirrorListDisabledMsgKey = "METADATA.NAMESPACES_AND_IMPORTS.MESSAGES.NO_MIRROR_FOR_GIVEN_IMPORT";
            } else {
              this.selectedMirror = mirrorToSelect;
            }
            this.mirrorListDisabled = true; //blocked either if mirror is found or not
          }
          if (this.importSource == ImportSource.fromLocalProject) {
            this.projects = this.projects.filter(p => p.getBaseURI() == this.baseURI);
            if (this.projects.length == 0) {
              this.projectListDisabledMsgKey = "METADATA.NAMESPACES_AND_IMPORTS.MESSAGES.NO_PROJECT_FOR_GIVEN_IMPORT";
            }
          }
        }
        if (this.projectNameInput && this.projects) {
          this.selectedProject = this.projects.find(p => p.getName() == this.projectNameInput);
        }
        //baseURI is always mandatory, except in import from local file
        this.baseURIOptional = this.importAction == ImportAction.import && this.importSource == ImportSource.fromLocalFile;
      }
    );
  }

  private initAsyncStuff(): Observable<any> {
    let asyncFn: Observable<any>[] = [];
    if (this.importSource == ImportSource.fromOntologyMirror) {
      //init mirror list if modal import type is fromOntologyMirror
      asyncFn.push(
        this.ontoMgrService.getOntologyMirror().pipe(
          map(mirrors => {
            this.mirrorList = mirrors;
            if (this.mirrorList.length == 0) {
              this.mirrorListDisabled = true;
              this.mirrorListDisabledMsgKey = "METADATA.NAMESPACES_AND_IMPORTS.MESSAGES.NO_ONTOLOGIES_MIRRORED";
            }
          })
        )
      );
    }
    if (this.importSource == ImportSource.fromLocalProject) {
      asyncFn.push(
        this.projectService.listProjects(VBContext.getWorkingProject(), true).pipe(
          map(projects => {
            this.projects = projects.filter(p => p.getName() != VBContext.getWorkingProject().getName());
          })
        )
      );
    }
    if (this.importSource == ImportSource.fromWeb) {
      //init list of rdfFormats if import type is fromWeb
      asyncFn.push(
        this.exportService.getOutputFormats().pipe(
          map(formats => {
            this.formats = formats;
            //select RDF/XML as default
            for (const f of this.formats) {
              if (f.name == "RDF/XML") {
                this.rdfFormat = f;
                break;
              }
            }
          })
        )
      );
    }
    if (asyncFn.length > 0) {
      return forkJoin(asyncFn);
    } else {
      return of(null);
    }
  }

  fileChangeEvent(file: File) {
    this.localFile = file;
  }

  onProjectSelected() {
    this.baseURI = this.selectedProject.getBaseURI();
  }

  onMirrorChanged() {
    this.baseURI = this.selectedMirror.baseURI;
  }

  isOkClickable() {
    /* 
    in the following checks, selectedImportAllowance is never checked since (even it is mandatory) it is automatically set through the combobox.
    Moreover, some checks may be the same for both import and repair (e.g. in fromWeb), 
    but I prefer to keep them separated (using if-else clause) so if the optional/mandatory parameters changes for the given scenario, 
    it will be easier to fix the conditions.
    */
    if (this.importSource == ImportSource.fromWeb) {
      if (this.importAction == ImportAction.import) {
        //baseURI required, other params optional
        return this.baseURI != null && this.baseURI.trim() != "" && //baseURI valid
          (!this.altURLCheck || this.altURL != null && this.altURL.trim() != "") && //altURL not checked, or checked and valid
          (!this.forceFormatCheck || this.rdfFormat != null) && //format not checked or checked and selected
          (!this.mirrorFileCheck || this.mirrorFile != null && this.mirrorFile.trim() != ""); //mirror not checked or checked and valid
      } else { //repair/update
        //baseURI required, other params optional
        return this.baseURI != null && this.baseURI.trim() != "" && //baseURI valid
          (!this.altURLCheck || this.altURL != null && this.altURL.trim() != "") && //altURL not checked, or checked and valid
          (!this.forceFormatCheck || this.rdfFormat != null) && //format not checked or checked and selected
          (!this.mirrorFileCheck || this.mirrorFile != null && this.mirrorFile.trim() != ""); //mirror not checked or checked and valid
      }
    } else if (this.importSource == ImportSource.fromLocalFile) {
      if (this.importAction == ImportAction.import) {
        //local file required, other param optional
        return this.localFile != null && //localFile provided
          (!this.baseURICheck || this.baseURI != null && this.baseURI.trim() != "") && //baseURI not checked, or checked and valid
          (!this.mirrorFileCheck || this.mirrorFile != null && this.mirrorFile.trim() != ""); //mirrorFile not checked, or checked and valid
      } else {
        //baseURI and localFile required, other optional
        return this.baseURI != null && this.baseURI.trim() != "" && //baseURI valid
          this.localFile != null && //localFile provided
          (!this.mirrorFileCheck || this.mirrorFile != null && this.mirrorFile.trim() != ""); //mirrorFile not checked, or checked and valid
      }
    } else if (this.importSource == ImportSource.fromOntologyMirror) {
      //available only import (not repair)
      //baseURI and mirror required (both from selectedMirror)
      return this.selectedMirror != null;
    } else if (this.importSource == ImportSource.fromLocalProject) {
      return this.selectedProject != null || this.projectNameInput != null;
    }
    return false; //should not be reached, all cases have been taken in account
  }

  ok() {
    if (this.importSource == ImportSource.fromWeb) {
      if (this.importAction == ImportAction.import) {
        let returnData: ImportFromWebData = {
          baseURI: this.baseURI,
          altURL: this.altURLCheck ? this.altURL : null,
          rdfFormat: this.forceFormatCheck ? this.rdfFormat : null,
          transitiveImportAllowance: this.selectedImportAllowance,
          mirrorFile: this.mirrorFileCheck ? this.mirrorFile : null
        };
        this.activeModal.close(returnData);
      } else { //repair
        let returnData: UpdateFromWebData = {
          baseURI: this.baseURI,
          altURL: this.altURLCheck ? this.altURL : null,
          rdfFormat: this.forceFormatCheck ? this.rdfFormat : null,
          transitiveImportAllowance: this.selectedImportAllowance,
          mirrorFile: this.mirrorFileCheck ? this.mirrorFile : null
        };
        this.activeModal.close(returnData);
      }
    } else if (this.importSource == ImportSource.fromLocalFile) {
      if (this.importAction == ImportAction.import) {
        let returnData: ImportFromLocalFileData = {
          baseURI: this.baseURICheck ? this.baseURI : null,
          localFile: this.localFile,
          mirrorFile: this.mirrorFileCheck ? this.mirrorFile : null,
          transitiveImportAllowance: this.selectedImportAllowance
        };
        this.activeModal.close(returnData);
      } else { //repair
        let returnData: UpdateFromLocalFileData = {
          baseURI: this.baseURI,
          localFile: this.localFile,
          mirrorFile: this.mirrorFileCheck ? this.mirrorFile : null,
          transitiveImportAllowance: this.selectedImportAllowance
        };
        this.activeModal.close(returnData);
      }
    } else if (this.importSource == ImportSource.fromOntologyMirror) {
      if (this.importAction == ImportAction.import) {
        let returnData: ImportFromMirrorData = {
          mirror: this.selectedMirror,
          transitiveImportAllowance: this.selectedImportAllowance
        };
        this.activeModal.close(returnData);
      } else { //repair
        let returnData: UpdateImportReturnData = {
          baseURI: this.baseURI,
          transitiveImportAllowance: this.selectedImportAllowance
        };
        this.activeModal.close(returnData);
      }
    } else if (this.importSource == ImportSource.fromLocalProject) {
      if (this.importAction == ImportAction.import) {
        let returnData: ImportFromLocalProjectData = {
          project: this.projectNameInput ? new Project(this.projectNameInput) : this.selectedProject,
          transitiveImportAllowance: this.selectedImportAllowance
        };
        this.activeModal.close(returnData);
      } else { //repair
        let returnData: UpdateFromLocalProjectData = {
          baseURI: this.baseURI,
          project: this.projectNameInput ? new Project(this.projectNameInput) : this.selectedProject,
          transitiveImportAllowance: this.selectedImportAllowance
        };
        this.activeModal.close(returnData);
      }
    }
  }

  cancel() {
    this.activeModal.dismiss();
  }

}


/** data when importig ontology */

export interface ImportOntologyReturnData {
  transitiveImportAllowance: TransitiveImportMethodAllowance;
}

export interface ImportFromLocalFileData extends ImportOntologyReturnData {
  baseURI?: string;
  localFile: File;
  mirrorFile?: string;
}

export interface ImportFromLocalProjectData extends ImportOntologyReturnData {
  project: Project;
}


export interface ImportFromMirrorData extends ImportOntologyReturnData {
  mirror: OntologyMirror; //contains both file and baseUri
}

export interface ImportFromWebData extends ImportOntologyReturnData {
  baseURI: string;
  altURL?: string;
  rdfFormat?: RDFFormat;
  mirrorFile?: string;
}

/** data when repairing/updating import */

export interface UpdateImportReturnData {
  baseURI: string; //will be the same of the one provided/forced in input
  transitiveImportAllowance: TransitiveImportMethodAllowance;
}

export interface UpdateFromLocalProjectData extends UpdateImportReturnData {
  project: Project;
}

export interface UpdateFromWebData extends UpdateImportReturnData {
  altURL?: string;
  rdfFormat?: RDFFormat;
  mirrorFile: string;
}

export interface UpdateFromLocalFileData extends UpdateImportReturnData {
  localFile: File;
  mirrorFile?: string;
}
