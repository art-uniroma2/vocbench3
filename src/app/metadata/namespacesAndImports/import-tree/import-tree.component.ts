import { Component, EventEmitter, Output } from "@angular/core";
import { MetadataServices } from 'src/app/services/metadata.service';
import { OntologyImport } from "../../../models/Metadata";

@Component({
  selector: "import-tree",
  templateUrl: "./import-tree.component.html",
  standalone: false
})
export class ImportTreeComponent {
  @Output() nodeRemoved = new EventEmitter<OntologyImport>();
  @Output() update = new EventEmitter();

  imports: OntologyImport[];

  constructor(private metadataService: MetadataServices) { }

  ngOnInit() {
    this.initTree();
  }

  initTree() {
    this.metadataService.getImports().subscribe(
      importedOntologies => {
        this.imports = importedOntologies;
      }
    );
  }

  onNodeRemoved(node: OntologyImport) {
    this.metadataService.removeImport(node.id).subscribe(
      () => {
        //Refreshes the imports and the namespace prefix mapping
        this.initTree();
        this.update.emit();
      }
    );
  }

  onUpdate() {
    this.initTree();
  }

}