import { Component, EventEmitter, Input, Output } from "@angular/core";
import { Observable } from 'rxjs';
import { SharedModalServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { ImportAction, ImportSource, OntologyImport } from "../../../models/Metadata";
import { MetadataServices } from "../../../services/metadata.service";
import { AuthorizationEvaluator } from "../../../utils/AuthorizationEvaluator";
import { UIUtils } from "../../../utils/UIUtils";
import { VBActionsEnum } from "../../../utils/VBActions";
import { ImportFromDatasetCatalogModalReturnData } from '../import-from-dataset-catalog-modal.component';
import { UpdateFromLocalFileData, UpdateFromLocalProjectData, UpdateFromWebData, UpdateImportReturnData } from "../import-ontology-modal.component";

@Component({
  selector: "import-tree-node",
  templateUrl: "./import-tree-node.component.html",
  standalone: false
})
export class ImportTreeNodeComponent {
  @Input() import: OntologyImport;
  @Input() root: boolean;
  @Output() remove = new EventEmitter<OntologyImport>();
  @Output() update = new EventEmitter();

  open: boolean = true;

  deleteImportAuthorized: boolean;
  updateImportAuthorized: boolean;

  constructor(private metadataService: MetadataServices, private sharedModals: SharedModalServices) { }

  ngOnInit() {
    this.deleteImportAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.metadataRemoveImport);
    this.updateImportAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.metadataAddImport);
  }

  removeImport() {
    this.remove.emit(this.import);
  }

  updateFromWeb() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.UPDATE_DATA_FROM_WEB" }, ImportSource.fromWeb, ImportAction.update, this.import.id).then(
      (data: UpdateFromWebData) => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        let importFn: Observable<any> = this.metadataService.updateFromWeb(data.baseURI, data.transitiveImportAllowance, data.altURL, data.rdfFormat);
        if (data.mirrorFile != null) {
          importFn = this.metadataService.updateFromWebToMirror(data.baseURI, data.mirrorFile, data.transitiveImportAllowance, data.altURL, data.rdfFormat);
        }
        importFn.subscribe(
          () => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            this.update.emit();
          }
        );
      },
      () => { }
    );
  }

  updateFromLocalFile() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.UPDATE_DATA_FROM_LOCAL_FILE" }, ImportSource.fromLocalFile, ImportAction.update, this.import.id).then(
      (data: UpdateFromLocalFileData) => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.metadataService.updateFromLocalFile(data.baseURI, data.localFile, data.transitiveImportAllowance, data.mirrorFile).subscribe(
          () => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            this.update.emit();
          }
        );
      },
      () => { }
    );
  }

  updateFromMirror() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.UPDATE_DATA_FROM_ONTOLOGY_MIRROR" }, ImportSource.fromOntologyMirror, ImportAction.update, this.import.id).then(
      (data: UpdateImportReturnData) => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.metadataService.updateFromMirror(data.baseURI, data.transitiveImportAllowance).subscribe(
          () => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            this.update.emit();
          }
        );
      },
      () => { }
    );
  }

  updateFromDatasetCatalog() {
    this.sharedModals.importFromDatasetCatalog({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.UPDATE_DATA_FROM_DATASET_CATALOG" }).then(
      (data: ImportFromDatasetCatalogModalReturnData) => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.metadataService.updateFromWeb(this.import.id, data.transitiveImportAllowance, data.dataDump, data.rdfFormat).subscribe(
          () => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            this.update.emit();
          }
        );
      },
      () => { }
    );

  }

  updateFromLinkedProject() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.UPDATE_DATA_FROM_LINKED_PROJ" }, ImportSource.fromLocalProject, ImportAction.update, this.import.id, this.import.source.project).then(
      (data: UpdateFromLocalProjectData) => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.metadataService.updateFromLocalProject(this.import.id, this.import.source.project, data.transitiveImportAllowance).subscribe(
          () => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            this.update.emit();
          }
        );
      },
      () => { }
    );
  }

  changeSourceProject() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.CHANGE_SOURCE_TO_PROJ" }, ImportSource.fromLocalProject, ImportAction.update, this.import.id).then(
      (data: UpdateFromLocalProjectData) => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.metadataService.updateFromLocalProject(data.baseURI, data.project.getName(), data.transitiveImportAllowance).subscribe(
          () => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            this.update.emit();
          }
        );
      },
      () => { }
    );
  }

  connectToProject() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.CONNECT_TO_PROJECT" }, ImportSource.fromLocalProject, ImportAction.update, this.import.id).then(
      (data: UpdateFromLocalProjectData) => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.metadataService.updateFromLocalProject(data.baseURI, data.project.getName(), data.transitiveImportAllowance).subscribe(
          () => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            this.update.emit();
          }
        );
      },
      () => { }
    );
  }

  disconnectFromProject() {
    UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
    this.metadataService.disconnectFromProject(this.import.id).subscribe(
      () => {
        UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
        this.update.emit();
      }
    );
  }

  repairFromLocalFile() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.REPAIR_FROM_LOCAL_FILE" }, ImportSource.fromLocalFile, ImportAction.repair, this.import.id).then(
      (data: UpdateFromLocalFileData) => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.metadataService.getFromLocalFile(data.baseURI, data.localFile, data.transitiveImportAllowance, data.mirrorFile).subscribe(
          () => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            this.update.emit();
          }
        );
      },
      () => { }
    );
  }

  repairFromWeb() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.REPAIR_FROM_WEB" }, ImportSource.fromWeb, ImportAction.repair, this.import.id).then(
      (data: UpdateFromWebData) => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        let importFn: Observable<any> = this.metadataService.downloadFromWeb(data.baseURI, data.transitiveImportAllowance, data.altURL, data.rdfFormat);
        if (data.mirrorFile != null) {
          importFn = this.metadataService.downloadFromWebToMirror(data.baseURI, data.mirrorFile, data.transitiveImportAllowance, data.altURL, data.rdfFormat);
        }
        importFn.subscribe(
          () => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            this.update.emit();
          }
        );
      },
      () => { }
    );
  }

  repairFromMirror() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.REPAIR_FROM_ONTOLOGY_MIRROR" }, ImportSource.fromOntologyMirror, ImportAction.repair, this.import.id).then(
      (data: UpdateImportReturnData) => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.metadataService.getFromMirror(data.baseURI, data.transitiveImportAllowance).subscribe(
          () => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            this.update.emit();
          }
        );
      },
      () => { }
    );
  }

  repairFromDatasetCatalog() {
    this.sharedModals.importFromDatasetCatalog({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.REPAIR_FROM_DATASET_CATALOG" }).then(
      (data: ImportFromDatasetCatalogModalReturnData) => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.metadataService.downloadFromWeb(this.import.id, data.transitiveImportAllowance, data.dataDump, data.rdfFormat).subscribe(
          () => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            this.update.emit();
          }
        );
      },
      () => { }
    );
  }

  repairFromLocalProject() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.REPAIR_FROM_LOCAL_PROJECT" }, ImportSource.fromLocalProject, ImportAction.repair, this.import.id).then(
      (data: UpdateFromLocalProjectData) => {
        UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
        this.metadataService.getFromLocalProject(data.baseURI, data.project.getName(), data.transitiveImportAllowance).subscribe(
          () => {
            UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
            this.update.emit();
          }
        );
      },
      () => { }
    );
  }

  onNodeRemoved(node: OntologyImport) {
    this.remove.emit(node);
  }

  onUpdate() {
    this.update.emit();
  }

}