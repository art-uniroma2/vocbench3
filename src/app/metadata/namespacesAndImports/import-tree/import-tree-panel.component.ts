import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { ImportAction, ImportSource } from 'src/app/models/Metadata';
import { MetadataServices } from 'src/app/services/metadata.service';
import { AuthorizationEvaluator } from 'src/app/utils/AuthorizationEvaluator';
import { UIUtils } from 'src/app/utils/UIUtils';
import { VBActionsEnum } from 'src/app/utils/VBActions';
import { BasicModalServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalOptions, ModalType } from 'src/app/modal-dialogs/Modals';
import { SharedModalServices } from 'src/app/modal-dialogs/shared-modals/shared-modals.service';
import { ImportFromDatasetCatalogModalReturnData } from '../import-from-dataset-catalog-modal.component';
import { ImportFromLocalFileData, ImportFromLocalProjectData, ImportFromMirrorData, ImportFromWebData } from '../import-ontology-modal.component';
import { OntologyMirrorModalComponent } from '../ontology-mirror-modal.component';
import { ImportTreeComponent } from './import-tree.component';

@Component({
  selector: "import-tree-panel",
  templateUrl: "./import-tree-panel.component.html",
  host: { class: "position-relative vbox" },
  standalone: false
})
export class ImportTreePanelComponent {

  @Output() update = new EventEmitter();

  @ViewChild(ImportTreeComponent) viewChildTree: ImportTreeComponent;
  @ViewChild('blockingDiv', { static: true }) public blockingDivElement: ElementRef;

  addImportAuthorized: boolean;

  constructor(private metadataService: MetadataServices, private modalService: NgbModal,
    private basicModals: BasicModalServices, private sharedModals: SharedModalServices,
    private translateService: TranslateService) { }

  ngOnInit() {
    this.addImportAuthorized = AuthorizationEvaluator.isAuthorized(VBActionsEnum.metadataAddImport);
  }

  initTree() {
    this.viewChildTree.initTree();
  }

  /**
   * Opens a modal to import an ontology from web,
   * once done refreshes the imports list and the namespace prefix mapping
   */
  importFromWeb() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.IMPORT_FROM_WEB" }, ImportSource.fromWeb, ImportAction.import).then(
      (data: ImportFromWebData) => {
        UIUtils.startLoadingDiv(this.blockingDivElement.nativeElement);
        let importFn: Observable<any> = this.metadataService.addFromWeb(data.baseURI, data.transitiveImportAllowance, data.altURL, data.rdfFormat);
        if (data.mirrorFile != null) {
          importFn = this.metadataService.addFromWebToMirror(data.baseURI, data.mirrorFile, data.transitiveImportAllowance, data.altURL, data.rdfFormat);
        }
        importFn.subscribe({
          next: () => {
            UIUtils.stopLoadingDiv(this.blockingDivElement.nativeElement);
            this.initTree();
            this.update.emit();
          },
          error: (err: Error) => { this.handleImportFromWebFail(err); }
        });
      },
      () => { }
    );
  }


  /**
   * Opens a modal to import an ontology from a local file and copies it to a mirror file,
   * once done refreshes the imports list and the namespace prefix mapping
   */
  importFromLocalFile() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.IMPORT_FROM_LOCAL_FILE" }, ImportSource.fromLocalFile, ImportAction.import).then(
      (data: ImportFromLocalFileData) => {
        UIUtils.startLoadingDiv(this.blockingDivElement.nativeElement);
        this.metadataService.addFromLocalFile(data.baseURI, data.localFile, data.mirrorFile, data.transitiveImportAllowance).subscribe(
          () => {
            UIUtils.stopLoadingDiv(this.blockingDivElement.nativeElement);
            //Refreshes the imports and the namespace prefix mapping
            this.initTree();
            this.update.emit();
          }
        );
      },
      () => { }
    );
  }

  /**
   * Opens a modal to import an ontology from a mirror file,
   * once done refreshes the imports list and the namespace prefix mapping
   */
  importFromOntologyMirror() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.IMPORT_FROM_ONTOLOGY_MIRROR" }, ImportSource.fromOntologyMirror, ImportAction.import).then(
      (data: ImportFromMirrorData) => {
        UIUtils.startLoadingDiv(this.blockingDivElement.nativeElement);
        this.metadataService.addFromMirror(data.mirror.baseURI, data.mirror.file, data.transitiveImportAllowance).subscribe(
          () => {
            UIUtils.stopLoadingDiv(this.blockingDivElement.nativeElement);
            //Refreshes the imports and the namespace prefix mapping
            this.initTree();
            this.update.emit();
          }
        );
      },
      () => { }
    );
  }

  /**
   * Opens a modal to import an ontology from the dataset catalog. This uses the addFromWeb import.
   * Once done refreshes the imports list and the namespace prefix mapping
   */
  importFromDatasetCatalog() {
    this.sharedModals.importFromDatasetCatalog({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.IMPORT_FROM_DATASET_CATALOG" }).then(
      (data: ImportFromDatasetCatalogModalReturnData) => {
        UIUtils.startLoadingDiv(this.blockingDivElement.nativeElement);
        this.metadataService.addFromWeb(data.ontologyIRI, data.transitiveImportAllowance, data.dataDump, data.rdfFormat).subscribe(
          () => {
            UIUtils.stopLoadingDiv(this.blockingDivElement.nativeElement);
            //Refreshes the imports and the namespace prefix mapping
            this.initTree();
            this.update.emit();
          },
          (err: Error) => { this.handleImportFromWebFail(err); }
        );
      },
      () => { }
    );
  }

  /**
   * Opens a modal to import an ontology from a local file and copies it to a mirror file,
   * once done refreshes the imports list and the namespace prefix mapping
   */
  importFromLocalProject() {
    this.sharedModals.importOntology({ key: "METADATA.NAMESPACES_AND_IMPORTS.ACTIONS.IMPORT_FROM_LOCAL_PROJECT" }, ImportSource.fromLocalProject, ImportAction.import).then(
      (data: ImportFromLocalProjectData) => {
        UIUtils.startLoadingDiv(this.blockingDivElement.nativeElement);
        this.metadataService.addFromLocalProject(data.project.getName(), data.transitiveImportAllowance).subscribe(
          () => {
            UIUtils.stopLoadingDiv(this.blockingDivElement.nativeElement);
            //Refreshes the imports and the namespace prefix mapping
            this.initTree();
            this.update.emit();
          }
        );
      },
      () => { }
    );
  }

  openOntologyMirror() {
    this.modalService.open(OntologyMirrorModalComponent, new ModalOptions()).result.then(
      (changed: boolean) => {
        if (changed) {
          this.initTree();
        }
      },
      () => { }
    );
  }

  onUpdate() {
    this.update.emit();
  }


  private handleImportFromWebFail(err: Error) {
    let msg = this.translateService.instant("METADATA.NAMESPACES_AND_IMPORTS.MESSAGES.UNABLE_TO_IMPORT_ONTOLOGY") + ":\n" + err.message;
    this.basicModals.alert({ key: "STATUS.WARNING" }, msg, ModalType.warning);
  }



}