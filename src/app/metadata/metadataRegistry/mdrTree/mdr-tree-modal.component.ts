import { Component, ElementRef, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CatalogRecord2, DatasetNature } from 'src/app/models/Metadata';
import { UIUtils } from 'src/app/utils/UIUtils';
import { BasicModalServices } from 'src/app/modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from 'src/app/modal-dialogs/Modals';
import { MdrTreeContext } from './mdrTreeComponent';

@Component({
    selector: "mdr-tree-modal",
    templateUrl: "./mdr-tree-modal.component.html",
    standalone: false
})
export class MetadataRegistryTreeModalComponent {

    @Input() title: string;
    @Input() context: MdrTreeContext;

    selectedRecord: CatalogRecord2;

    constructor(public activeModal: NgbActiveModal, private basicModals: BasicModalServices, private elementRef: ElementRef) { }

    ngAfterViewInit() {
        UIUtils.setFullSizeModal(this.elementRef);
    }

    ok() {
        if (this.selectedRecord.dataset.nature == DatasetNature.ABSTRACT) {
            this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "You need to select a concrete dataset" }, ModalType.warning);
            return;
        }
        this.activeModal.close(this.selectedRecord);
    }

    cancel() {
        this.activeModal.dismiss();
    }

}
