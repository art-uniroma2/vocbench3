import { Component, Input } from "@angular/core";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import FileSaver from 'file-saver';
import { ConfigurableExtensionFactory, ExtensionPointID, Settings, TransformationStep } from "../models/Plugins";
import { RDFFormat } from "../models/RDFFormat";
import { ExportServices } from "../services/export.service";
import { ExtensionsServices } from "../services/extensions.service";
import { SparqlServices } from "../services/sparql.service";
import { UIUtils } from "../utils/UIUtils";
import { BasicModalServices } from "../modal-dialogs/basic-modals/basic-modals.service";
import { ModalType } from '../modal-dialogs/Modals';
import { SharedModalServices } from "../modal-dialogs/shared-modals/shared-modals.service";

@Component({
  selector: "export-as-rdf-modal",
  templateUrl: "./export-as-rdf-modal.component.html",
  standalone: false
})
export class ExportResultAsRdfModalComponent {
  @Input() query: string;
  @Input() inferred: boolean;

  exportFormats: RDFFormat[];
  selectedExportFormat: RDFFormat;

  applyFilter: boolean = false;

  //export filter management
  private filters: ConfigurableExtensionFactory[];
  filtersChain: FilterChainElement[] = [];
  selectedFilterChainElement: FilterChainElement;

  constructor(public activeModal: NgbActiveModal,
    private exportService: ExportServices, private extensionServices: ExtensionsServices, private sparqlService: SparqlServices,
    private basicModals: BasicModalServices, private sharedModals: SharedModalServices) {
  }

  ngOnInit() {
    this.exportService.getOutputFormats().subscribe(
      formats => {
        this.exportFormats = formats;
        //select RDF/XML as default
        for (const f of this.exportFormats) {
          if (f.name == "RDF/XML") {
            this.selectedExportFormat = f;
            return;
          }
        }
      }
    );

    this.extensionServices.getExtensions(ExtensionPointID.RDF_TRANSFORMERS_ID).subscribe(
      extensions => {
        this.filters = extensions as ConfigurableExtensionFactory[];
      }
    );
  }

  ok() {
    let filteringPipelineParam: string;
    if (this.applyFilter) {
      //check if every filter has been configured
      for (const c of this.filtersChain) {
        if (this.requireConfiguration(c)) {
          this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "MESSAGES.EXPORT_FILTER_NOT_CONFIGURED", params: { filterName: c.selectedFactory.id } },
            ModalType.warning);
          return;
        }
      }
      let filteringPipeline: any[] = [];
      for (const c of this.filtersChain) {
        filteringPipeline.push(c.convertToFilteringPipelineStep());
      }
      filteringPipelineParam = JSON.stringify(filteringPipeline);
    }
    UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
    this.sparqlService.exportGraphQueryResultAsRdf(this.query, this.selectedExportFormat, this.inferred, filteringPipelineParam).subscribe(
      blob => {
        UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
        FileSaver.saveAs(blob, "sparql_export." + this.selectedExportFormat.defaultFileExtension);
      }
    );

    this.activeModal.close();
  }

  cancel() {
    this.activeModal.dismiss();
  }


  /** =====================================
   * =========== FILTER CHAIN =============
   * =====================================*/
  selectFilterChainElement(filterChainEl: FilterChainElement) {
    if (this.selectedFilterChainElement == filterChainEl) {
      this.selectedFilterChainElement = null;
    } else {
      this.selectedFilterChainElement = filterChainEl;
    }
  }
  isSelectedFilterFirst(): boolean {
    return (this.selectedFilterChainElement == this.filtersChain[0]);
  }
  isSelectedFilterLast(): boolean {
    return (this.selectedFilterChainElement == this.filtersChain[this.filtersChain.length - 1]);
  }

  appendFilter() {
    this.filtersChain.push(new FilterChainElement(this.filters));
  }
  removeFilter() {
    this.filtersChain.splice(this.filtersChain.indexOf(this.selectedFilterChainElement), 1);
    this.selectedFilterChainElement = null;
  }
  moveFilterDown() {
    let prevIndex = this.filtersChain.indexOf(this.selectedFilterChainElement);
    this.filtersChain.splice(prevIndex, 1); //remove from current position
    this.filtersChain.splice(prevIndex + 1, 0, this.selectedFilterChainElement);
  }
  moveFilterUp() {
    let prevIndex = this.filtersChain.indexOf(this.selectedFilterChainElement);
    this.filtersChain.splice(prevIndex, 1); //remove from current position
    this.filtersChain.splice(prevIndex - 1, 0, this.selectedFilterChainElement);
  }

  onExtensionUpdated(filterChainEl: FilterChainElement, ext: ConfigurableExtensionFactory) {
    filterChainEl.selectedFactory = ext;
  }
  onConfigurationUpdated(filterChainEl: FilterChainElement, config: Settings) {
    filterChainEl.selectedConfiguration = config;
  }

  /**
   * Returns true if a plugin of the filter chain require edit of the configuration and it is not configured
   */
  requireConfiguration(filterChainEl: FilterChainElement): boolean {
    let conf: Settings = filterChainEl.selectedConfiguration;
    if (conf != null && conf.requireConfiguration()) { //!= null required because selectedConfiguration could be not yet initialized
      return true;
    }
    return false;
  }

}


//Utility model classes

class FilterChainElement {
  public availableFactories: ConfigurableExtensionFactory[];
  public selectedFactory: ConfigurableExtensionFactory;
  public selectedConfiguration: Settings;

  constructor(availableFactories: ConfigurableExtensionFactory[]) {
    //clone the available factories, so changing the configuration of one of them, doesn't change the default of the others
    let availableFactClone: ConfigurableExtensionFactory[] = [];
    for (const f of availableFactories) {
      availableFactClone.push(f.clone());
    }
    this.availableFactories = availableFactClone;
  }

  convertToFilteringPipelineStep(): TransformationStep {
    let filterStep: TransformationStep = { filter: null };
    //filter: factoryId and properties
    let filter: { factoryId: string, configuration: any } = {
      factoryId: this.selectedFactory.id,
      configuration: null
    };
    let selectedConf: Settings = this.selectedConfiguration;

    filter.configuration = selectedConf.getPropertiesAsMap();
    filterStep.filter = filter;
    return filterStep;
  }
}