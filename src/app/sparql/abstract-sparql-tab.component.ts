import { ChangeDetectorRef, Directive, EventEmitter, Output, ViewChild } from "@angular/core";
import { finalize, Observable } from 'rxjs';
import { Configuration, ConfigurationProperty } from "../models/Configuration";
import { PrefixMapping } from "../models/Metadata";
import { QueryChangedEvent, QueryMode } from "../models/Sparql";
import { SparqlServices } from "../services/sparql.service";
import { AuthorizationEvaluator } from "../utils/AuthorizationEvaluator";
import { UIUtils } from "../utils/UIUtils";
import { VBActionsEnum } from "../utils/VBActions";
import { VBContext } from "../utils/VBContext";
import { BasicModalServices } from '../modal-dialogs/basic-modals/basic-modals.service';
import { ModalType } from "../modal-dialogs/Modals";
import { SharedModalServices } from '../modal-dialogs/shared-modals/shared-modals.service';
import { YasguiComponent } from "./yasgui.component";

@Directive()
export abstract class AbstractSparqlTabComponent {

  @ViewChild(YasguiComponent, { static: false }) viewChildYasgui: YasguiComponent;

  @Output() updateName: EventEmitter<string> = new EventEmitter<string>();
  @Output() savedStatus: EventEmitter<boolean> = new EventEmitter<boolean>();

  query: string;
  queryCache: string; //contains the last query submitted (useful to invoke the export excel)
  protected queryMode: QueryMode = QueryMode.query;
  inferred: boolean = false;
  storedQueryReference: string;

  private sampleQuery: string = "SELECT * WHERE {\n    ?s ?p ?o .\n} LIMIT 10";

  queryInProgress: boolean = false;
  queryValid: boolean = true;
  queryTime: string;

  queryResultResp: any;

  protected sparqlService: SparqlServices;
  protected basicModals: BasicModalServices;
  protected sharedModals: SharedModalServices;
  protected changeDetectorRef: ChangeDetectorRef;
  constructor(sparqlService: SparqlServices, basicModals: BasicModalServices, sharedModals: SharedModalServices, changeDetectorRef: ChangeDetectorRef) {
    this.sparqlService = sparqlService;
    this.basicModals = basicModals;
    this.sharedModals = sharedModals;
    this.changeDetectorRef = changeDetectorRef;
  }

  ngOnInit() {
    //collect the prefix namespace mappings
    let mappings: PrefixMapping[] = VBContext.getPrefixMappings();
    let prefixImports: string = "";
    for (const m of mappings) {
      prefixImports += "PREFIX " + m.prefix + ": <" + m.namespace + ">\n";
    }
    //set them as suffix of sampleQuery
    this.sampleQuery = prefixImports + "\n" + this.sampleQuery;

    this.query = this.sampleQuery;
  }

  doQuery() {
    this.queryTime = null;
    let initTime = new Date().getTime();
    this.queryCache = this.query; //stored the submitted query

    let doQueryFn: Observable<any>;
    if (this.queryMode == QueryMode.query) {
      if (!AuthorizationEvaluator.isAuthorized(VBActionsEnum.sparqlEvaluateQuery)) {
        this.basicModals.alert({ key: "STATUS.OPERATION_DENIED" }, { key: "MESSAGES.UNAUTHORIZED_SPARQL_QUERY" });
        return;
      }
      doQueryFn = this.evaluateQueryImpl();
    } else { //queryMode "update"
      if (!AuthorizationEvaluator.isAuthorized(VBActionsEnum.sparqlExecuteUpdate)) {
        this.basicModals.alert({ key: "STATUS.OPERATION_DENIED" }, { key: "MESSAGES.UNAUTHORIZED_SPARQL_UPDATE" });
        return;
      }
      doQueryFn = this.executeUpdateImpl();
    }
    UIUtils.startLoadingDiv(UIUtils.blockDivFullScreen);
    doQueryFn.pipe(
      finalize(() => {
        UIUtils.stopLoadingDiv(UIUtils.blockDivFullScreen);
      })
    ).subscribe({
      next: stResp => {
        this.sparqlResponseHandler(stResp, initTime);
      },
      error: (err: Error) => {
        if (err.name.endsWith("MalformedQueryException")) {
          this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "SPARQL.MESSAGES.MALFORMED_QUERY_ERR" }, ModalType.warning, err.message);
        }
      }
    });
  }

  abstract evaluateQueryImpl(): Observable<any>;
  abstract executeUpdateImpl(): Observable<any>;

  private sparqlResponseHandler(stResp: any, initTime: number) {
    this.queryResultResp = stResp;
    //calculates the time spent in query
    let finishTime = new Date().getTime();
    let diffTime = finishTime - initTime;
    this.queryTime = this.getPrettyPrintTime(diffTime);
  }

  /**
   * Listener of event querychange, emitted from YasquiComponent.
   * Event is an object {query: string, valid: boolean, mode} where
   * query is the code written in the textarea
   * valid tells wheter the query is syntactically correct
   * mode tells the query mode (query/update) 
   */
  onQueryChange(event: QueryChangedEvent) {
    this.query = event.query;
    this.queryValid = event.valid;
    this.queryMode = event.mode;
    this.savedStatus.emit(false);
  }

  clear() {
    this.queryTime = null;
    this.queryResultResp = null;
  }

  private getPrettyPrintTime(time: number) {
    if (time < 1000) {
      return time + " millisec";
    } else {
      let sec = Math.floor(time / 1000);
      let millisec: any = time % 1000;
      if (millisec < 10) {
        millisec = "00" + millisec;
      } else if (millisec < 100) {
        millisec = "0" + millisec;
      }
      return sec + "," + millisec + " sec";
    }
  }

  //LOAD/SAVE/PARAMETERIZE QUERY

  /**
   * Loads a configuration (stored query or parameterized query)
   */
  abstract loadConfiguration(): void;
  /**
   * Stores a configuration (stored query or parameterized query)
   */
  abstract saveConfiguration(): void;

  /**
   * Set the query after the load of a stored query
   * @param conf 
   */
  setLoadedQueryConf(conf: Configuration) {
    let query: string;
    let includeInferred: boolean = false;
    let confProps: ConfigurationProperty[] = conf.properties;
    for (const confProp of confProps) {
      if (confProp.name == "sparql") {
        query = confProp.value;
      } else if (confProp.name == "includeInferred") {
        includeInferred = confProp.value;
      }
    }
    this.query = query;
    this.inferred = includeInferred;
    //in order to detect the change of @Input query in the child YasguiComponent
    this.changeDetectorRef.detectChanges();
    this.viewChildYasgui.forceContentUpdate();
    this.savedStatus.emit(true);
  }

}