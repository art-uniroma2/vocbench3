import { ChangeDetectorRef, Component } from "@angular/core";
import { Observable } from 'rxjs';
import { ConfigurationComponents } from "../models/Configuration";
import { SparqlServices } from "../services/sparql.service";
import { BasicModalServices } from '../modal-dialogs/basic-modals/basic-modals.service';
import { LoadConfigurationModalReturnData } from "../modal-dialogs/shared-modals/configuration-store-modal/load-configuration-modal.component";
import { SharedModalServices } from '../modal-dialogs/shared-modals/shared-modals.service';
import { AbstractSparqlTabComponent } from "./abstract-sparql-tab.component";

@Component({
  selector: "sparql-tab",
  templateUrl: "./sparql-tab.component.html",
  standalone: false
})
export class SparqlTabComponent extends AbstractSparqlTabComponent {

  constructor(sparqlService: SparqlServices, basicModals: BasicModalServices, sharedModals: SharedModalServices, changeDetectorRef: ChangeDetectorRef) {
    super(sparqlService, basicModals, sharedModals, changeDetectorRef);
  }

  evaluateQueryImpl(): Observable<any> {
    return this.sparqlService.evaluateQuery(this.query, this.inferred);
  }

  executeUpdateImpl(): Observable<any> {
    return this.sparqlService.executeUpdate(this.query, this.inferred);
  }

  //LOAD/SAVE/PARAMETERIZE QUERY

  loadConfiguration() {
    this.sharedModals.loadConfiguration({ key: "SPARQL.ACTIONS.LOAD_SPARQL_QUERY" }, ConfigurationComponents.SPARQL_STORE).then(
      (data: LoadConfigurationModalReturnData) => {
        let relativeRef = data.reference.relativeReference;
        this.storedQueryReference = relativeRef;
        this.updateName.emit(relativeRef.substring(relativeRef.indexOf(":") + 1));
        this.setLoadedQueryConf(data.configuration);
        this.savedStatus.emit(true);
      },
      () => { }
    );
  }

  saveConfiguration() {
    let queryConfig: { [key: string]: any } = {
      sparql: this.query,
      type: this.queryMode,
      includeInferred: this.inferred
    };
    this.sharedModals.storeConfiguration({ key: "SPARQL.ACTIONS.SAVE_SPARQL_QUERY" }, ConfigurationComponents.SPARQL_STORE, queryConfig, this.storedQueryReference).then(
      (relativeRef: string) => {
        this.basicModals.alert({ key: "STATUS.OPERATION_DONE" }, { key: "SPARQL.MESSAGES.QUERY_SAVED" });
        this.storedQueryReference = relativeRef;
        this.updateName.emit(relativeRef.substring(relativeRef.indexOf(":") + 1));
        this.savedStatus.emit(true);
      },
      () => { }
    );
  }


}
