import { Component, ElementRef, ViewChild } from "@angular/core";
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { forkJoin, Observable, of } from 'rxjs';
import { finalize, map, mergeMap } from 'rxjs/operators';
import { AlignmentRelationSymbol, Correspondence } from "../models/Alignment";
import { ARTResource, ARTURIResource, RDFResourceRolesEnum } from "../models/ARTResources";
import { Project } from "../models/Project";
import { EdoalServices } from "../services/edoal.services";
import { ProjectServices } from "../services/projects.service";
import { ResourcesServices } from "../services/resources.service";
import { NodeSelectEvent } from "../structures/abstractNode";
import { TabsetPanelComponent } from "../structures/structure-tabset/structure-tabset.component";
import { HttpServiceContext } from "../utils/HttpManager";
import { ResourceUtils } from "../utils/ResourceUtils";
import { UIUtils } from "../utils/UIUtils";
import { ProjectContext, VBContext } from "../utils/VBContext";
import { VBProperties } from "../utils/VBProperties";
import { BasicModalServices } from "../modal-dialogs/basic-modals/basic-modals.service";
import { BrowsingModalServices } from '../modal-dialogs/browsing-modals/browsing-modals.service';
import { ModalOptions, ModalType } from '../modal-dialogs/Modals';
import { SharedModalServices } from "../modal-dialogs/shared-modals/shared-modals.service";
import { ChangeMeasureModalComponent } from "./change-measure-modal.component";

@Component({
  selector: "edoal-component",
  templateUrl: "./edoal.component.html",
  host: { class: "pageComponent" },
  standalone: false
})
export class EdoalComponent {

  @ViewChild('leftTabset') leftTabset: TabsetPanelComponent;
  @ViewChild('rightTabset') rightTabset: TabsetPanelComponent;

  @ViewChild('blockingDiv', { static: false }) private blockingDivElement: ElementRef;

  /**
   * Projects and tabs
   */
  contextInitialized: boolean = false;
  leftProjCtx: ProjectContext;
  rightProjCtx: ProjectContext;

  /**
   * Alignments
   */
  private alignemnts: ARTResource[]; //nodes of alignments

  correspondences: Correspondence[];
  selectedCorrespondence: Correspondence;

  leftSelectedResource: ARTURIResource = null;
  rightSelectedResource: ARTURIResource = null;
  relations: AlignmentRelationSymbol[] = AlignmentRelationSymbol.getDefaultRelations();
  selectedRelation: AlignmentRelationSymbol;
  measure: number = 1.0;

  //pagination
  page: number = 0;
  totPage: number;
  private pageSize: number = 50;

  pageSelector: number[] = [];
  pageSelectorOpt: number;

  constructor(private edoalService: EdoalServices, private projectService: ProjectServices, private resourcesService: ResourcesServices,
    private vbProp: VBProperties, private basicModals: BasicModalServices, private browsingModals: BrowsingModalServices, private sharedModals: SharedModalServices, private modalService: NgbModal) { }

  ngOnInit() {
    this.initProjects();
  }

  initProjects() {
    this.edoalService.getAlignedProjects().subscribe(
      projects => {
        let leftProjectName: string = projects[0];
        let rightProjectName: string = projects[1];

        let leftProject: Project = new Project(leftProjectName);
        let rightProject: Project = new Project(rightProjectName);

        let describeProjFn: Observable<any>[] = [
          this.projectService.getProjectInfo(leftProjectName).pipe(
            map(proj => {
              leftProject = proj;
            })
          ),
          this.projectService.getProjectInfo(rightProjectName).pipe(
            map(proj => {
              rightProject = proj;
            })
          )
        ];

        forkJoin(describeProjFn).subscribe(
          () => {
            this.leftProjCtx = new ProjectContext(leftProject);
            let initLeftProjectCtxFn: Observable<void>[] = [
              this.vbProp.initProjectUserBindings(this.leftProjCtx),
              this.vbProp.initUserProjectPreferences(this.leftProjCtx),
              this.vbProp.initProjectSettings(this.leftProjCtx)
            ];

            this.rightProjCtx = new ProjectContext(rightProject);
            let initRightProjectCtxFn: Observable<void>[] = [
              this.vbProp.initProjectUserBindings(this.rightProjCtx),
              this.vbProp.initUserProjectPreferences(this.rightProjCtx),
              this.vbProp.initProjectSettings(this.rightProjCtx)
            ];

            let initFn: Observable<any>[] = initLeftProjectCtxFn.concat(initRightProjectCtxFn);
            forkJoin(initFn).subscribe(
              () => {
                this.contextInitialized = true;
                this.listAlignments();
              }
            );
          }
        );
      }
    );
  }

  listAlignments() {
    this.ensureExistingAlignment().subscribe(
      () => {
        //paging handler
        let totCorrespondences = this.alignemnts[0].getAdditionalProperty('correspondences');
        this.totPage = Math.floor(totCorrespondences / this.pageSize);
        if (totCorrespondences % this.pageSize > 0) {
          this.totPage++;
        }

        this.pageSelector = [];
        for (let i = 0; i < this.totPage; i++) {
          this.pageSelector.push(i);
        }

        //after a init/refresh, if the current selected page is greater than the tot page, reset it to the first page
        if (this.page > this.totPage) {
          this.page = 0;
        }

        this.listCorrespondences();
      }
    );
  }

  private ensureExistingAlignment(): Observable<void> {
    return this.edoalService.getAlignments().pipe(
      mergeMap(alignments => {
        if (alignments.length > 0) {
          this.alignemnts = alignments;
          return of(null);
        } else {
          return this.edoalService.createAlignment().pipe(
            mergeMap(() => {
              return this.edoalService.getAlignments().pipe(
                map(alignments => {
                  this.alignemnts = alignments;
                })
              );
            })
          );
        }
      })
    );
  }

  /** ======================
   * Correspondence handlers
   * ====================== */

  private listCorrespondences() {
    UIUtils.startLoadingDiv(this.blockingDivElement.nativeElement);
    this.edoalService.getCorrespondences(this.alignemnts[0], this.page, this.pageSize).subscribe({
      next: correspondences => {
        UIUtils.stopLoadingDiv(this.blockingDivElement.nativeElement);
        this.correspondences = correspondences;
        //try to set qname of mapping property
        this.correspondences.forEach(c => {
          c.mappingProperty.forEach(mp => {
            mp.setShow(ResourceUtils.getQName(mp.getURI(), VBContext.getPrefixMappings()));
          });
        });
        this.renderCorrespondences();
        this.selectedCorrespondence = null;
      },
      error: (err: Error) => {
        if (err.name.endsWith("IndexingLanguageNotFound")) {
          this.basicModals.alert({ key: "STATUS.WARNING" }, { key: "MESSAGES.NO_LANG_DETECTED_IN_EDOAL_ALIGNMENT", params: { projName: this.leftProjCtx.getProject().getName() } },
            ModalType.warning);
        }
      }
    });
  }

  private renderCorrespondences() {
    let leftEntities: ARTResource[] = [];
    let rightEntities: ARTResource[] = [];
    this.correspondences.forEach(c => {
      leftEntities.push(c.leftEntity[0]);
      rightEntities.push(c.rightEntity[0]);
    });
    if (leftEntities.length > 0 || rightEntities.length > 0) {
      HttpServiceContext.setContextProject(this.leftProjCtx.getProject());
      this.resourcesService.getResourcesInfo(leftEntities).pipe(
        finalize(() => HttpServiceContext.removeContextProject())
      ).subscribe(
        resources => {
          resources.forEach(r => {
            this.correspondences.forEach(c => {
              if (c.leftEntity[0].equals(r)) {
                c.leftEntity[0] = r;
              }
            });
          });
        }
      );
      HttpServiceContext.setContextProject(this.rightProjCtx.getProject());
      this.resourcesService.getResourcesInfo(rightEntities).pipe(
        finalize(() => HttpServiceContext.removeContextProject())
      ).subscribe(
        resources => {
          resources.forEach(r => {
            this.correspondences.forEach(c => {
              if (c.rightEntity[0].equals(r)) {
                c.rightEntity[0] = r;
              }
            });
          });
        }
      );
    }
  }

  selectCorrespondence(correspondece: Correspondence) {
    if (this.selectedCorrespondence == correspondece) {
      this.selectedCorrespondence = null;
    } else {
      this.selectedCorrespondence = correspondece;
    }
  }

  addCorrespondence() {
    if (this.measure < 0 || this.measure > 1) {
      this.basicModals.alert({ key: "STATUS.INVALID_VALUE" }, { key: "MESSAGES.INVALID_ALIGNMENT_MEASURE" }, ModalType.warning);
      return;
    }
    this.edoalService.createCorrespondence(this.alignemnts[0], this.leftSelectedResource, this.rightSelectedResource, this.selectedRelation.relation, this.measure).subscribe(
      () => {
        this.selectedRelation = null;
        this.measure = 1.0;
        this.listCorrespondences();
      }
    );
  }

  deleteCorrespondece() {
    this.edoalService.deleteCorrespondence(this.selectedCorrespondence.identity).subscribe(
      () => {
        this.correspondences.splice(this.correspondences.indexOf(this.selectedCorrespondence), 1);
        this.selectedCorrespondence = null;
      }
    );
  }

  syncCorrespondece() {
    this.leftTabset.syncResource(this.selectedCorrespondence.leftEntity[0], true);
    this.rightTabset.syncResource(this.selectedCorrespondence.rightEntity[0], true);
  }

  changeRelation(correspondence: Correspondence, relation: AlignmentRelationSymbol) {
    this.edoalService.setRelation(correspondence.identity, relation.relation).subscribe(
      () => {
        this.listCorrespondences();
      }
    );
  }

  changeMeasure(correspondence: Correspondence) {
    const modalRef: NgbModalRef = this.modalService.open(ChangeMeasureModalComponent, new ModalOptions());
    modalRef.componentInstance.value = correspondence.measure[0].getShow() as unknown as number;
    return modalRef.result.then(
      newMeasure => {
        this.edoalService.setMeasure(correspondence.identity, newMeasure).subscribe(
          () => {
            this.listCorrespondences();
          }
        );
      },
      () => { }
    );
  }

  changeMappingProperty(correspondence: Correspondence, property: ARTURIResource) {
    this.edoalService.setMappingProperty(correspondence.identity, property).subscribe(
      () => {
        this.listCorrespondences();
      }
    );
  }

  selectCustomProperty(correspondence: Correspondence) {
    this.browsingModals.browsePropertyTree({ key: "DATA.ACTIONS.SELECT_PROPERTY" }).then(
      prop => {
        this.changeMappingProperty(correspondence, prop);
      },
      () => { }
    );
  }

  /**
   * Called when user click on menu to change the mapping property.
   * Useful to populate the menu of the mapping properties.
   * Retrieves the suggested mapping properties and set them to the alignment cell.
   */
  initSuggestedMappingProperties(correspondence: Correspondence) {
    //call the service only if suggested properties for the given cell is not yet initialized
    if (correspondence['suggestedMappingProperties'] == null) {
      //mention is not a valid role in ST, so get the role with fallback to individual
      let role = correspondence.leftEntity[0].getRole();
      if (role == RDFResourceRolesEnum.mention) {
        role = RDFResourceRolesEnum.individual;
      }
      this.edoalService.getSuggestedProperties(this.alignemnts[0], role, correspondence.relation[0].getShow()).subscribe(
        props => {
          correspondence['suggestedMappingProperties'] = props;
        }
      );
    }
  }

  /**
   * Paging
   */

  prevPage() {
    this.page--;
    this.listCorrespondences();
  }

  nextPage() {
    this.page++;
    this.listCorrespondences();
  }

  goToPage() {
    if (this.page != this.pageSelectorOpt) {
      this.page = this.pageSelectorOpt;
      this.listCorrespondences();
    }
  }

  /** ======================
   * Tabset handlers
   * ====================== */

  openLeftResourceView(res: ARTResource) {
    this.sharedModals.openResourceView(res, true, this.leftProjCtx);
  }
  openRightResourceView(res: ARTResource) {
    this.sharedModals.openResourceView(res, true, this.rightProjCtx);
  }

  onLeftResourceSelected(event?: NodeSelectEvent) {
    this.leftSelectedResource = event?.value as ARTURIResource;
  }

  onRightResourceSelected(event?: NodeSelectEvent) {
    this.rightSelectedResource = event?.value as ARTURIResource;
  }

}